-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2017 at 06:29 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nhaphang365`
--

-- --------------------------------------------------------

--
-- Table structure for table `accountbalances`
--

CREATE TABLE `accountbalances` (
  `AccountBalanceId` int(11) NOT NULL,
  `BankAccountId` smallint(6) NOT NULL,
  `RechargeId` int(11) NOT NULL,
  `Balance` decimal(10,2) NOT NULL,
  `IsLast` tinyint(4) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accountbalances`
--

INSERT INTO `accountbalances` (`AccountBalanceId`, `BankAccountId`, `RechargeId`, `Balance`, `IsLast`, `CrDateTime`) VALUES
(1, 1, 1, '1000.00', 0, '2017-07-17 17:39:52'),
(2, 1, 2, '3500.00', 0, '2017-07-17 17:40:35'),
(3, 1, 3, '4000.00', 0, '2017-07-17 17:42:49'),
(4, 2, 4, '1.00', 1, '2017-07-25 20:53:05'),
(5, 1, 5, '4050.18', 0, '2017-07-25 20:54:08'),
(6, 1, 6, '3960.18', 0, '2017-07-25 20:54:52'),
(7, 1, 7, '3958.18', 1, '2017-08-08 22:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `ActionId` smallint(6) NOT NULL,
  `ActionName` varchar(250) NOT NULL,
  `ActionUrl` varchar(250) NOT NULL,
  `ParentActionId` smallint(6) DEFAULT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `DisplayOrder` tinyint(4) NOT NULL,
  `FontAwesome` varchar(20) DEFAULT NULL,
  `ActionLevel` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`ActionId`, `ActionName`, `ActionUrl`, `ParentActionId`, `StatusId`, `DisplayOrder`, `FontAwesome`, `ActionLevel`) VALUES
(1, 'Cài đặt', '', NULL, 2, 20, 'fa-cog', 1),
(2, 'Tỉnh/ Thành phố', 'province', 1, 0, 100, NULL, 2),
(3, 'Đổi thứ tự Tỉnh/ Thành phố', 'province/changeDisplayOrder', 2, 0, 0, NULL, 3),
(4, 'Chi nhánh', 'branch', 1, 2, 0, NULL, 2),
(5, 'Cập nhật Chi nhánh', 'branch/update', 4, 2, 0, NULL, 3),
(6, 'Xóa Chi nhánh', 'branch/delete', 4, 2, 0, NULL, 3),
(7, 'Khách hàng', '', NULL, 2, 100, 'fa-user', 1),
(8, 'Danh sách Khách hàng', 'user/customer', 7, 2, 100, NULL, 2),
(9, 'Thêm khách hàng', 'user/add/customer', 7, 2, 90, NULL, 2),
(10, 'Nhân viên', '', NULL, 2, 90, 'fa-user', 1),
(11, 'Danh sách Nhân viên', 'user/staff', 10, 2, 100, NULL, 2),
(12, 'Thêm Nhân viên', 'user/add/staff', 10, 2, 90, NULL, 2),
(13, 'Xóa Khách hàng', 'user/deleteCustomer', 8, 2, 0, NULL, 3),
(14, 'Sửa Khách hàng', 'user/editCustomer', 7, 2, 0, NULL, 2),
(15, 'Xóa Nhân viên', 'user/deleteStaff', 11, 2, 0, NULL, 3),
(16, 'Sửa Nhân viên', 'user/editStaff', 10, 2, 0, NULL, 2),
(17, 'Cấp quyền truy cập', 'useraction', 11, 2, 0, NULL, 3),
(18, 'Cấp quyền từng phân cấp', 'role', 1, 2, 50, NULL, 2),
(19, 'Thuộc tính sản phẩm', 'attribute', 1, 0, 70, NULL, 2),
(20, 'Cập nhật Thuộc tính sản phẩm', 'attribute/update', 19, 0, 0, NULL, 3),
(21, 'Xóa Thuộc tính sản phẩm', 'attribute/delete', 19, 0, 0, NULL, 3),
(22, 'Đơn hàng', '', NULL, 2, 80, 'fa-shopping-cart', 1),
(23, 'Xem thông tin Khách hàng', 'user/viewCustomer', 8, 2, 0, NULL, 3),
(24, 'Xem thông tin Nhân viên', 'user/viewStaff', 11, 2, 0, NULL, 3),
(25, 'Xem thông tin Đơn hàng', 'order/view', 28, 2, 0, NULL, 3),
(26, 'Cấu hình hệ thống', 'config', 1, 2, 30, NULL, 2),
(27, 'Cập nhật Cấu hình hệ thống', 'config/update', 26, 2, 0, NULL, 3),
(28, 'Danh sách Đơn hàng', 'order', 22, 2, 100, NULL, 2),
(29, 'Thêm Đơn hàng', 'order/add', 22, 2, 90, NULL, 2),
(30, 'Xóa Đơn hàng', 'order/delete', 28, 1, 0, NULL, 3),
(31, 'Đổi trạng thái Đơn hàng', 'order/changeStatus', 28, 1, 0, NULL, 3),
(32, 'Sửa Đơn hàng', 'order/edit', 28, 1, 0, NULL, 3),
(33, 'Khiếu nại', 'complaint', 7, 2, 80, '', 2),
(34, 'Danh sách Khiếu nại', 'complaint', 33, 0, 100, NULL, 2),
(35, 'Thêm Khiếu nại', 'complaint/add', 34, 0, 99, NULL, 2),
(36, 'Sửa Khiếu nại', 'complaint/edit', 34, 0, 0, NULL, 3),
(37, 'Xóa Khiếu nại', 'complaint/delete', 33, 2, 0, NULL, 3),
(38, 'Tỉ giá', 'exchangerate', 1, 1, 50, NULL, 2),
(39, 'Cập nhật Tỉ giá', 'exchangerate/update', 38, 1, 0, NULL, 3),
(40, 'Đổi thông tin Địa chỉ nhận hàng', 'order/changeRecieveAddress', 28, 1, 0, NULL, 3),
(41, 'Cập nhật sản phẩm trong đơn hàng', 'order/updateProduct', 28, 1, 0, NULL, 3),
(42, 'Xóa sản phẩm trong đơn hàng', 'order/deleteProduct', 28, 1, 0, NULL, 3),
(43, 'Đổi trạng thái Sản phẩm', 'order/changeProductStatus', 28, 1, 0, NULL, 3),
(44, 'Phí dịch vụ', 'servicefee', 1, 2, 100, NULL, 2),
(45, 'Cập nhật Phí dịch vụ', 'servicefee/update', 44, 2, 0, NULL, 3),
(46, 'Xóa Phí dịch vụ', 'servicefee/delete', 44, 2, 0, NULL, 3),
(47, 'Danh sách Kho', 'warehouse', 1, 2, 90, NULL, 2),
(48, 'Cập nhật Kho', 'warehouse/update', 47, 2, 0, NULL, 3),
(49, 'Xóa Kho', 'warehouse/delete', 47, 2, 0, NULL, 3),
(50, 'Chat', 'user/chat', NULL, 2, 40, NULL, 1),
(51, 'Nguồn tiền', 'moneysource', 1, 2, 80, NULL, 2),
(52, 'Cập nhật Nguồn tiền', 'moneysource/update', 51, 2, 0, NULL, 3),
(53, 'Xóa Nguồn tiền', 'moneysource/delete', 21, 2, 0, NULL, 3),
(54, 'Tài chính', '', NULL, 2, 70, NULL, 1),
(55, 'Phiếu thu NoName chưa duyệt', 'transaction/noName', 54, 2, 100, NULL, 2),
(56, 'Danh sách Phiếu thu', 'transaction/invoice', 54, 2, 90, NULL, 2),
(57, 'Danh sách Phiếu chi', 'transaction/expense', 54, 2, 80, NULL, 2),
(58, 'Xóa Phiếu thu NoName', 'transaction/deleteNoName', 55, 2, 0, NULL, 3),
(59, 'Thêm Phiếu thu', 'transaction/add/invoice', 56, 2, 0, NULL, 3),
(60, 'Thêm Phiếu chi', 'transaction/add/expense', 57, 2, 0, NULL, 3),
(61, 'Sửa Phiếu thu', 'transaction/editInvoice', 56, 2, 0, NULL, 3),
(62, 'Sửa Phiếu chi', 'transaction/editExpense', 57, 2, 0, NULL, 3),
(63, 'Xem thông tin Phiếu thu', 'transaction/viewInvoice', 56, 2, 0, NULL, 3),
(64, 'Xem thông tin Phiếu chi', 'transaction/viewExpense', 57, 2, 0, NULL, 3),
(65, 'CMS', '', NULL, 2, 10, NULL, 1),
(66, 'Bình luận - Đánh giá', 'cms/comment', 65, 2, 100, NULL, 2),
(67, 'Thêm Bình luận - Đánh giá', 'cms/comment/add', 66, 2, 0, NULL, 3),
(68, 'Sửa Bình luận - Đánh giá', 'cms/comment/edit', 66, 2, 0, NULL, 3),
(69, 'Xóa Bình luận - Đánh giá', 'cms/comment/delete', 66, 2, 0, NULL, 3),
(70, 'Slider trang chủ', 'cms/slider', 65, 2, 95, NULL, 2),
(71, 'Trang tĩnh', 'cms/article/page', 65, 2, 90, NULL, 2),
(72, 'Sửa trang, bài viết', 'cms/article/edit', 71, 2, 0, NULL, 3),
(73, 'Chuyên mục sản phẩm', 'cms/category/product', 65, 2, 85, NULL, 2),
(74, 'Cập nhật Chuyên mục sản phẩm', 'cms/category/update', 73, 0, 0, NULL, 3),
(75, 'Xóa Chuyên mục sản phẩm', 'cms/category/delete', 73, 0, 0, NULL, 3),
(76, 'Thay đổi sản phẩm', 'product', 54, 1, 70, NULL, 2),
(77, 'Tài chính khách hàng', 'transaction/customer', 7, 2, 0, NULL, 2),
(78, 'Duyệt Đơn hàng + Tài chính', 'transaction/verify', 54, 2, 50, NULL, 2),
(79, 'Video trang chủ', 'cms/article/video', 65, 0, 87, NULL, 2),
(80, 'Chuyên mục sản phẩm', 'cms/category/shop', 1, 2, 60, NULL, 2),
(81, 'Thêm phiếu thu Vận chuyển', 'transport/add', 82, 2, 0, NULL, 3),
(82, 'Danh sách phiếu thu Vận chuyển', 'transport', 54, 2, 70, NULL, 2),
(83, 'Phí vận chuyển', 'transportfee', 1, 2, 95, NULL, 2),
(84, 'Cập nhật Phí vận chuyển', 'transportfee/update', 83, 2, 0, NULL, 3),
(85, 'Xóa Phí vận chuyển', 'transportfee/delete', 83, 2, 0, NULL, 3),
(86, 'Tài khoản Đặt hàng', 'orderaccount', 1, 2, 75, NULL, 2),
(87, 'Cập nhật Tài khoản Đặt hàng', 'orderaccount/update', 86, 2, 0, NULL, 3),
(88, 'Xóa Tài khoản Đặt hàng', 'orderaccount/delete', 86, 2, 0, NULL, 3),
(89, 'Đơn đặt hàng', 'order/addOrderUser', 22, 2, 80, NULL, 2),
(90, 'Phương thức giao hàng', 'transporttype', 1, 2, 70, NULL, 2),
(91, 'Cập nhật Phương thức giao hàng', 'transporttype/update', 90, 2, 0, NULL, 3),
(92, 'Xóa Phương thức giao hàng', 'transporttype/delete', 90, 2, 0, NULL, 3),
(93, 'Nhân viên giao hàng', 'transportuser', 1, 2, 65, NULL, 2),
(94, 'Cập nhật Nhân viên giao hàng', 'transportuser/update', 93, 2, 0, NULL, 3),
(95, 'Xóa Nhân viên giao hàng', 'transportuser/delete', 93, 2, 0, NULL, 3),
(96, 'Tiền trình Vận chuyển', 'transportprocess', 22, 1, 50, NULL, 2),
(97, 'Sửa phiếu thu Vận chuyển', 'transport/edit', 82, 2, 0, NULL, 3),
(98, 'Sửa Tiền trình Vận chuyển', 'transportprocess/edit', 96, 1, 0, NULL, 3),
(99, 'Xóa Tiền trình Vận chuyển', 'transportprocess/delete', 96, 1, 0, NULL, 3),
(100, 'Mã vận đơn lấy về', 'trackingcrawl', 22, 2, 70, NULL, 2),
(101, 'Shop cần thanh toán', 'shop/paid', 22, 2, 60, NULL, 2),
(102, 'Shop đã thanh toán', 'shop/paidDone', 22, 2, 55, NULL, 2),
(103, 'Tài khoản ngân hàng', 'bankaccount', 1, 2, 77, NULL, 2),
(104, 'Cập nhật Tài khoản ngân hàng', 'bankaccount/update', 103, 2, 0, '', 3),
(105, 'Xóa Tài khoản ngân hàng', 'bankaccount/delete', 103, 2, 0, NULL, 3),
(106, 'Đòi tiền', 'shop/isreturn/0', 22, 2, 50, NULL, 2),
(107, 'Tài khoản Tệ', 'recharge', 54, 2, 45, NULL, 2),
(108, 'Nạp tệ', 'recharge/add', 107, 2, 0, NULL, 3),
(109, 'Doanh thu của CSKH', 'revenue/staff', 10, 2, 80, NULL, 2),
(110, 'Chuyên mục bài viết', 'cms/category', 65, 2, 80, NULL, 2),
(111, 'Danh sách bài viết', 'cms/article', 65, 2, 87, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `ArticleId` int(11) NOT NULL,
  `ArticleTitle` varchar(250) NOT NULL,
  `ArticleLead` text,
  `ArticleSlug` varchar(250) DEFAULT NULL,
  `ArticleContent` text NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `ArticleTypeId` tinyint(4) NOT NULL,
  `DisplayOrder` smallint(6) NOT NULL,
  `ArticleImage` varchar(250) DEFAULT NULL,
  `PublishDateTime` datetime NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`ArticleId`, `ArticleTitle`, `ArticleLead`, `ArticleSlug`, `ArticleContent`, `StatusId`, `ArticleTypeId`, `DisplayOrder`, `ArticleImage`, `PublishDateTime`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 'Hướng dẫn đặt hàng', NULL, 'huong-dan-dat-hang', '', 2, 1, 1, NULL, '2017-09-13 00:00:00', 1, '2017-09-13 00:00:00', NULL, NULL),
(2, 'Quy định đặt hàng', NULL, 'quy-dinh-dat-hang', '', 2, 1, 1, NULL, '2017-09-13 00:00:00', 1, '2017-09-13 00:00:00', NULL, NULL),
(3, 'Bảng giá', NULL, 'bang-gia', '', 2, 1, 1, NULL, '2017-09-13 00:00:00', 1, '2017-09-13 00:00:00', NULL, NULL),
(4, 'Giới thiệu', NULL, 'gioi-thieu', '', 2, 1, 1, NULL, '2017-09-13 00:00:00', 1, '2017-09-13 00:00:00', NULL, NULL),
(5, 'Tuyển dụng', NULL, 'tuyen-dung', '', 2, 1, 1, NULL, '2017-09-13 00:00:00', 1, '2017-09-13 00:00:00', NULL, NULL),
(6, 'Liên hệ', NULL, 'lien-he', '', 2, 1, 1, NULL, '2017-09-13 00:00:00', 1, '2017-09-13 00:00:00', NULL, NULL),
(7, 'Lựa chọn nguồn hàng Quảng Châu - những bước đầu tiên cần biết', '<p><a alt="Lựa chọn nguồn hàng Quảng Châu - những bước đầu tiên cần biết" href="http://www.nhaphang247.com/p136/lua-chon-nguon-hang-quang-chau-nhung-buoc-dau-tien-can-biet" title="Lựa chọn nguồn hàng Quảng Châu - những bước đầu tiên cần biết">Lựa chọn nguồn h&agrave;ng Quảng Ch&acirc;u - những bước đầu ti&ecirc;n cần biết</a></p>', 'lua-chon-nguon-hang-quang-chau-nhung-buoc-dau-tien-can-biet', '<p><a alt="Lựa chọn nguồn hàng Quảng Châu - những bước đầu tiên cần biết" href="http://www.nhaphang247.com/p136/lua-chon-nguon-hang-quang-chau-nhung-buoc-dau-tien-can-biet" title="Lựa chọn nguồn hàng Quảng Châu - những bước đầu tiên cần biết">Lựa chọn nguồn h&agrave;ng Quảng Ch&acirc;u - những bước đầu ti&ecirc;n cần biết</a></p>', 2, 2, 1, 'hoanmuada.jpg', '2017-09-14 00:00:00', 1, '2017-09-14 22:18:51', NULL, NULL),
(8, 'Kinh nghiệm buôn lấy sỉ giày dép từ Trung Quốc', '<p><a alt="Kinh nghiệm buôn lấy sỉ giày dép từ Trung Quốc" href="http://www.nhaphang247.com/p135/kinh-nghiem-buon-lay-si-giay-dep-tu-trung-quoc" title="Kinh nghiệm buôn lấy sỉ giày dép từ Trung Quốc">Kinh nghiệm bu&ocirc;n lấy sỉ gi&agrave;y d&eacute;p từ Trung Quốc</a></p>', 'kinh-nghiem-buon-lay-si-giay-dep-tu-trung-quoc', '<p><a alt="Kinh nghiệm buôn lấy sỉ giày dép từ Trung Quốc" href="http://www.nhaphang247.com/p135/kinh-nghiem-buon-lay-si-giay-dep-tu-trung-quoc" title="Kinh nghiệm buôn lấy sỉ giày dép từ Trung Quốc">Kinh nghiệm bu&ocirc;n lấy sỉ gi&agrave;y d&eacute;p từ Trung Quốc</a></p>', 2, 2, 1, 'hoanmuada.jpg', '2017-09-14 00:00:00', 1, '2017-09-14 22:23:34', NULL, NULL),
(9, 'Những xu hướng street style cho các nàng điệu đà năm 2017', '<p><a alt="Những xu hướng street style cho các nàng điệu đà năm 2017" href="http://www.nhaphang247.com/p134/nhung-xu-huong-street-style-cho-cac-nang-dieu-da-nam-2017" title="Những xu hướng street style cho các nàng điệu đà năm 2017">Những xu hướng street style cho c&aacute;c n&agrave;ng điệu đ&agrave; năm 2017</a></p>', 'nhung-xu-huong-street-style-cho-cac-nang-dieu-da-nam-2017', '<p><a alt="Những xu hướng street style cho các nàng điệu đà năm 2017" href="http://www.nhaphang247.com/p134/nhung-xu-huong-street-style-cho-cac-nang-dieu-da-nam-2017" title="Những xu hướng street style cho các nàng điệu đà năm 2017">Những xu hướng street style cho c&aacute;c n&agrave;ng điệu đ&agrave; năm 2017</a></p>', 2, 2, 1, 'hoanmuada.jpg', '2017-09-14 00:00:00', 1, '2017-09-14 22:24:33', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bankaccounts`
--

CREATE TABLE `bankaccounts` (
  `BankAccountId` smallint(6) NOT NULL,
  `BankAccountName` varchar(250) NOT NULL,
  `StatusId` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bankaccounts`
--

INSERT INTO `bankaccounts` (`BankAccountId`, `BankAccountName`, `StatusId`) VALUES
(1, 'Bùi Tuấn Vũ', 2),
(2, 'Cao Hương', 2);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `CategoryId` smallint(6) NOT NULL,
  `CategoryName` varchar(250) NOT NULL,
  `CategoryNameTQ` varchar(250) DEFAULT NULL,
  `ParentCategoryId` smallint(6) NOT NULL,
  `CategoryLevel` tinyint(4) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `CategoryImage` varchar(250) DEFAULT NULL,
  `CategoryTypeId` tinyint(4) NOT NULL,
  `DisplayOrder` smallint(6) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`CategoryId`, `CategoryName`, `CategoryNameTQ`, `ParentCategoryId`, `CategoryLevel`, `StatusId`, `CategoryImage`, `CategoryTypeId`, `DisplayOrder`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 'Blog', 'blog', 0, 1, 2, NULL, 2, 1, 1, '2017-09-13 22:12:04', NULL, NULL),
(2, 'Kinh nghiệm nhập hàng', 'kinh-nghiem-nhap-hang', 1, 1, 2, NULL, 2, 1, 1, '2017-09-14 22:06:03', NULL, NULL),
(3, 'Tìm nguồn hàng', 'tim-nguon-hang', 1, 1, 2, NULL, 2, 2, 1, '2017-09-14 22:06:29', NULL, NULL),
(4, 'Bài học kinh doanh', 'bai-hoc-kinh-doanh', 1, 1, 2, NULL, 2, 3, 1, '2017-09-14 22:07:31', NULL, NULL),
(5, 'Xu hướng thị trường', 'xu-huong-thi-truong', 1, 1, 2, NULL, 2, 4, 1, '2017-09-14 22:08:03', NULL, NULL),
(6, 'Marketing Online', 'marketing-online', 1, 1, 2, NULL, 2, 5, 1, '2017-09-14 22:08:58', NULL, NULL),
(7, 'Thông báo', 'thong-bao', 1, 1, 2, NULL, 2, 6, 1, '2017-09-14 22:11:39', NULL, NULL),
(8, 'Quảng cáo Facebook', 'quang-cao-facebook', 6, 1, 2, NULL, 2, 1, 1, '2017-09-14 22:12:11', NULL, NULL),
(9, 'Quảng cáo Google', 'quang-cao-google', 6, 1, 2, NULL, 2, 2, 1, '2017-09-14 22:13:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categoryarticles`
--

CREATE TABLE `categoryarticles` (
  `CategoryArticleId` int(11) NOT NULL,
  `CategoryId` smallint(6) NOT NULL,
  `ArticleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categoryarticles`
--

INSERT INTO `categoryarticles` (`CategoryArticleId`, `CategoryId`, `ArticleId`) VALUES
(1, 1, 7),
(2, 2, 7),
(3, 1, 8),
(4, 2, 8),
(5, 1, 9),
(6, 2, 9);

-- --------------------------------------------------------

--
-- Table structure for table `chats`
--

CREATE TABLE `chats` (
  `ChatId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `StaffId` int(11) NOT NULL,
  `Message` varchar(650) NOT NULL,
  `FileUrl` varchar(250) NOT NULL,
  `IsCustomerSend` tinyint(4) NOT NULL,
  `IsCustomerRead` tinyint(4) NOT NULL,
  `IsStaffRead` tinyint(4) NOT NULL,
  `IpAddress` varchar(15) DEFAULT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chats`
--

INSERT INTO `chats` (`ChatId`, `CustomerId`, `StaffId`, `Message`, `FileUrl`, `IsCustomerSend`, `IsCustomerRead`, `IsStaffRead`, `IpAddress`, `CrDateTime`) VALUES
(1, 5, 2, '2ndcn', '', 1, 1, 1, NULL, '2016-10-31 17:22:54'),
(2, 5, 2, '2ndcn1', '', 0, 1, 1, NULL, '2016-10-31 17:22:58'),
(4, 5, 2, 'abc', '', 0, 1, 1, NULL, '2016-10-31 18:43:08'),
(5, 5, 2, 'ncnmc', '', 1, 1, 1, NULL, '2016-10-31 18:43:15'),
(6, 5, 2, 'mcdnmdn', '', 0, 1, 1, NULL, '2016-10-31 18:43:17'),
(7, 5, 2, 'alo', '', 1, 1, 1, NULL, '2016-11-02 12:03:55'),
(8, 5, 2, 'hello', '', 1, 1, 1, NULL, '2016-11-02 12:08:35'),
(9, 5, 2, '', 'hoanmuada.jpg', 1, 1, 1, NULL, '2016-11-02 13:00:19'),
(10, 5, 2, 'hdndbn', '', 1, 1, 1, NULL, '2016-11-02 13:17:58'),
(11, 5, 2, '', 'no-product.png', 1, 1, 1, NULL, '2016-11-02 13:18:09'),
(12, 5, 2, '', 'hoanmuada.jpg', 1, 1, 1, NULL, '2016-11-02 13:19:33'),
(13, 5, 2, '', 'no-product.png', 1, 1, 1, NULL, '2016-11-02 13:19:39'),
(14, 5, 2, '', 'hoanmuada.jpg', 1, 1, 1, NULL, '2016-11-02 13:22:22'),
(15, 5, 2, 'nmdnm', '', 1, 1, 1, NULL, '2016-11-02 13:48:13'),
(16, 5, 2, 'nncmc', '', 1, 1, 1, NULL, '2016-11-02 13:48:17'),
(17, 4, 2, 'asaaa', '', 1, 1, 1, NULL, '2016-11-02 13:48:17'),
(18, 4, 2, 'bbbb', '', 1, 1, 1, NULL, '2016-11-02 13:48:17'),
(19, 5, 2, 'banb', '', 1, 1, 1, NULL, '2016-11-02 22:43:17'),
(20, 5, 2, 'xuyc', '', 1, 1, 1, NULL, '2016-11-02 22:43:34'),
(21, 5, 2, 'abx', '', 1, 1, 1, NULL, '2016-11-02 22:48:37'),
(22, 5, 2, '1', '', 1, 1, 1, NULL, '2016-11-02 22:48:53'),
(23, 5, 2, '2', '', 1, 1, 1, NULL, '2016-11-02 22:50:01'),
(24, 5, 2, '3', '', 1, 1, 1, NULL, '2016-11-02 22:50:29'),
(25, 5, 2, 'x', '', 1, 1, 1, NULL, '2016-11-03 09:46:32'),
(26, 5, 2, 'jdjdckjjjjjjjjjjjjjjjjj', '', 0, 1, 1, NULL, '2016-11-03 09:46:43'),
(27, 5, 2, 'mcjkkkkkkkkkkkkkkkkkc', '', 0, 1, 1, NULL, '2016-11-03 09:46:47'),
(28, 5, 2, 'â', '', 0, 1, 1, NULL, '2016-11-03 09:47:01'),
(29, 5, 2, 'âjaj', '', 1, 1, 1, NULL, '2016-11-03 09:47:44'),
(30, 5, 2, 'hello', '', 0, 1, 1, NULL, '2016-11-03 09:58:25'),
(31, 4, 2, 'aaa', '', 1, 1, 1, NULL, '2016-11-03 10:01:36'),
(32, 5, 2, 'bbbbb', '', 0, 1, 1, NULL, '2016-11-03 10:01:54'),
(33, 5, 2, 'kldd', '', 0, 1, 1, NULL, '2016-11-03 10:04:01'),
(34, 4, 2, 'mdfmdf', '', 1, 1, 1, NULL, '2016-11-03 10:04:14'),
(35, 5, 2, '', 'no-product.png', 0, 1, 1, NULL, '2016-11-03 10:04:49'),
(36, 5, 2, 'hello', '', 1, 1, 1, NULL, '2016-11-03 10:06:44'),
(37, 5, 2, '', 'no-product.png', 1, 1, 1, NULL, '2016-11-03 10:06:51'),
(38, 5, 2, 'kf,f', '', 0, 1, 1, NULL, '2016-11-03 10:06:56'),
(39, 5, 2, 'mfmf', '', 0, 1, 1, NULL, '2016-11-03 10:46:14'),
(40, 4, 2, 'hjhdsd', '', 1, 1, 1, NULL, '2016-11-03 10:46:27'),
(41, 4, 2, 'jkjj', '', 1, 1, 1, NULL, '2016-11-03 10:53:20'),
(42, 4, 2, 'aaaaa', '', 1, 1, 1, NULL, '2016-11-03 10:53:40'),
(43, 4, 2, 'bbbbb', '', 1, 1, 1, NULL, '2016-11-03 10:54:18'),
(44, 4, 2, 'ddddddd', '', 1, 1, 1, NULL, '2016-11-03 10:55:28'),
(45, 4, 2, 'aaaaaaaa', '', 1, 1, 1, NULL, '2016-11-03 10:56:40'),
(46, 4, 2, 'aaaaaaaa', '', 1, 1, 1, NULL, '2016-11-03 10:56:48'),
(47, 4, 2, 'ffffff', '', 1, 1, 1, NULL, '2016-11-03 11:01:22'),
(48, 4, 2, '', 'no-product.png', 1, 1, 1, NULL, '2016-11-03 11:01:40'),
(49, 4, 2, '', 'no-product.png', 1, 1, 1, NULL, '2016-11-03 11:03:19'),
(50, 5, 2, 'xxxxxxx', '', 1, 1, 1, NULL, '2016-11-03 11:03:24'),
(51, 4, 2, 'aaaaa;', '', 1, 1, 1, NULL, '2016-11-03 11:12:49'),
(52, 4, 2, 'bbbbbbb', '', 1, 1, 1, NULL, '2016-11-03 12:06:09'),
(53, 5, 2, 'ANKD', '', 0, 1, 1, NULL, '2016-11-03 12:09:50'),
(54, 5, 2, 'NDN', '', 1, 1, 1, NULL, '2016-11-03 12:09:57'),
(55, 5, 2, 'NDND', '', 0, 1, 1, NULL, '2016-11-03 12:10:09'),
(56, 5, 2, 'EEEEEEE', '', 1, 1, 1, NULL, '2016-11-03 12:10:20'),
(57, 4, 2, 'FDDD', '', 1, 1, 1, NULL, '2016-11-03 12:10:59'),
(58, 5, 2, 'hhh', '', 1, 1, 1, NULL, '2016-11-03 12:12:40'),
(59, 5, 2, 'ccx', '', 0, 1, 1, NULL, '2016-11-04 14:48:35'),
(60, 5, 2, 'bgdhbd', '', 1, 1, 1, NULL, '2016-11-04 15:48:21'),
(61, 5, 2, 'mjnsjsj', '', 1, 1, 1, NULL, '2016-11-04 15:48:23'),
(62, 5, 2, 'ndjhds', '', 1, 1, 1, NULL, '2016-11-04 15:48:25'),
(63, 5, 2, 'njn', '', 1, 1, 1, NULL, '2016-11-04 16:51:04'),
(64, 5, 2, 'xxx', '', 1, 1, 1, NULL, '2016-11-04 17:03:11'),
(65, 5, 2, 'tgttr', '', 0, 1, 1, NULL, '2016-11-04 17:03:26'),
(66, 5, 2, 'jjjjj', '', 1, 1, 1, NULL, '2016-11-04 17:16:44'),
(67, 5, 2, 'iuuiuy', '', 1, 1, 1, NULL, '2016-11-04 17:16:47'),
(68, 5, 2, 'kiu', '', 1, 1, 1, NULL, '2016-11-04 17:16:48'),
(69, 5, 2, 'jjh', '', 1, 1, 1, NULL, '2016-11-04 17:18:18'),
(70, 5, 2, 'jj', '', 1, 1, 1, NULL, '2016-11-04 17:18:19'),
(71, 5, 2, 'yyt6', '', 1, 1, 1, NULL, '2016-11-04 17:18:20'),
(72, 5, 2, 'jjjj', '', 1, 1, 1, NULL, '2016-11-04 17:24:55'),
(73, 5, 2, 'sdddd', '', 1, 1, 1, NULL, '2016-11-04 17:26:16'),
(74, 5, 2, 'ndmdm', '', 1, 1, 1, NULL, '2016-11-04 17:27:02'),
(75, 5, 2, 'jdj', '', 1, 1, 1, NULL, '2016-11-04 17:27:59'),
(76, 5, 2, 'test', '', 0, 1, 1, NULL, '2016-11-05 11:34:48'),
(77, 5, 2, 'ndnd', '', 0, 1, 1, NULL, '2016-11-05 20:35:52'),
(78, 5, 2, ',f,djd', '', 0, 1, 1, NULL, '2016-11-05 20:36:26'),
(79, 5, 2, 'hhđgd', '', 1, 1, 1, NULL, '2016-11-05 20:41:33'),
(80, 5, 2, 'xin chào', '', 1, 1, 1, NULL, '2016-11-12 13:11:47'),
(83, 5, 2, 'ok', '', 0, 1, 1, NULL, '2016-12-03 16:21:22'),
(90, 5, 2, 'anan', '', 1, 1, 1, NULL, '2016-12-10 10:34:16'),
(91, 5, 2, 'nmmd', '', 0, 1, 1, NULL, '2016-12-10 10:34:34'),
(92, 5, 2, 'a', '', 1, 1, 1, NULL, '2017-01-14 10:39:38'),
(93, 5, 2, 'a', '', 1, 1, 1, NULL, '2017-01-14 10:39:39'),
(94, 5, 2, 'a', '', 1, 1, 1, NULL, '2017-01-14 10:39:39'),
(95, 5, 2, 'a', '', 1, 1, 1, NULL, '2017-01-14 10:39:40'),
(96, 5, 2, 'a', '', 1, 1, 1, NULL, '2017-01-14 10:39:40'),
(97, 5, 2, 'a', '', 1, 1, 1, NULL, '2017-01-14 10:39:41'),
(98, 5, 2, 'a', '', 1, 1, 1, NULL, '2017-01-14 10:39:42'),
(99, 5, 2, 'v', '', 1, 1, 1, NULL, '2017-01-14 10:39:43'),
(100, 5, 2, 'b', '', 1, 1, 1, NULL, '2017-01-14 10:39:43'),
(101, 5, 2, 'v', '', 1, 1, 1, NULL, '2017-01-14 10:39:44'),
(102, 5, 2, 'd', '', 1, 1, 1, NULL, '2017-01-14 10:39:45'),
(103, 5, 2, 'd', '', 1, 1, 1, NULL, '2017-01-14 10:39:45'),
(104, 5, 2, 'd', '', 1, 1, 1, NULL, '2017-01-14 10:39:46'),
(119, 5, 2, 'ân', '', 1, 1, 1, NULL, '2017-01-14 13:54:32'),
(120, 5, 2, 'ơi', '', 0, 1, 1, NULL, '2017-01-14 14:30:37'),
(121, 5, 2, 'abc', '', 1, 1, 1, NULL, '2017-01-14 14:30:53'),
(122, 5, 2, 'zxuz', '', 0, 1, 1, NULL, '2017-01-14 14:30:59'),
(126, 5, 2, 'abc', '', 1, 1, 1, NULL, '2017-01-16 09:46:40'),
(128, 5, 2, 'abc', '', 0, 1, 1, NULL, '2017-03-06 15:54:25'),
(130, 5, 2, 'abcq', '', 0, 1, 1, NULL, '2017-03-06 15:54:44'),
(131, 5, 2, 'avc', '', 1, 1, 1, NULL, '2017-03-06 19:31:45'),
(132, 5, 2, 'xyz', '', 0, 1, 1, NULL, '2017-03-06 19:32:02'),
(133, 5, 2, 'abc', '', 1, 1, 1, NULL, '2017-03-06 19:32:09'),
(134, 5, 2, 'alo', '', 0, 1, 1, NULL, '2017-04-01 15:13:54'),
(135, 5, 2, 'bạn check giúp mình <a href="https://docs.google.com/spreadsheets/d/1EYPCI1c1_Rc7LxlC-P91gbZVTiXCrYFFIaHe4vIlnHY/edit#gid=672533831" target="_blank">https://docs.google.com/spreadsheets/d/1EYPCI1c1_Rc7LxlC-P91gbZVTiXCrYFFIaHe4vIlnHY/edit#gid=672533831</a>', '', 0, 1, 1, NULL, '2017-04-01 15:14:17'),
(136, 5, 2, 'check <a href="https://docs.google.com/spreadsheets/d/1EYPCI1c1_Rc7LxlC-P91gbZVTiXCrYFFIaHe4vIlnHY/edit#gid=672533831" target="_blank">https://docs.google.com/spreadsheets/d/1EYPCI1c1_Rc7LxlC-P91gbZVTiXCrYFFIaHe4vIlnHY/edit#gid=672533831</a>', '', 0, 1, 1, NULL, '2017-04-01 15:21:14'),
(145, 5, 2, 'fnf', '', 0, 1, 1, '127.0.0.1', '2017-05-02 19:23:01'),
(146, 5, 2, 'dmd', '', 0, 1, 1, '127.0.0.1', '2017-05-02 19:43:06'),
(147, 5, 2, 'gfdhdf', '', 0, 1, 1, '127.0.0.1', '2017-05-02 19:47:15'),
(148, 5, 2, 'jrjre', '', 0, 1, 1, '127.0.0.1', '2017-05-02 19:47:19'),
(149, 5, 2, 'j5rr', '', 0, 1, 1, '127.0.0.1', '2017-05-02 19:47:23'),
(150, 4, 2, 'aaa', '', 1, 1, 1, '127.0.0.1', '2017-05-11 09:38:16'),
(151, 4, 2, 'nbb', '', 1, 1, 1, '127.0.0.1', '2017-05-11 09:45:37'),
(152, 4, 2, 'vvv', '', 1, 1, 1, '127.0.0.1', '2017-05-11 09:47:27'),
(153, 4, 2, 'nnn', '', 1, 1, 1, '127.0.0.1', '2017-05-11 09:47:37'),
(154, 4, 2, 'bnhh', '', 1, 1, 1, '127.0.0.1', '2017-05-11 09:48:29'),
(155, 4, 2, 'ggg', '', 1, 1, 1, '127.0.0.1', '2017-05-11 09:50:30'),
(156, 4, 2, 'nnn', '', 1, 1, 1, '127.0.0.1', '2017-05-11 09:54:03'),
(157, 4, 2, 'hh', '', 1, 1, 1, '127.0.0.1', '2017-05-11 10:10:40'),
(158, 4, 2, 'mdjd', '', 1, 1, 1, '127.0.0.1', '2017-05-11 10:10:56'),
(159, 4, 2, 'abc', '', 0, 1, 1, '127.0.0.1', '2017-05-11 13:52:38'),
(160, 4, 2, 'fff', '', 1, 0, 1, '127.0.0.1', '2017-05-11 17:14:41'),
(178, 3, 2, 'mfkf', '', 1, 0, 1, '127.0.0.1', '2017-05-12 16:43:17'),
(179, 5, 2, 'ábcv', '', 0, 1, 1, '127.0.0.1', '2017-05-13 19:15:02'),
(180, 5, 2, 'lao', '', 0, 1, 1, '127.0.0.1', '2017-05-14 14:28:36'),
(181, 5, 15, 'd', '', 1, 1, 0, '127.0.0.1', '2017-05-14 14:28:57'),
(182, 5, 2, 'yy', '', 1, 1, 1, '127.0.0.1', '2017-05-14 14:29:25'),
(183, 5, 2, 'gfggg', '', 0, 1, 1, '127.0.0.1', '2017-05-14 14:29:35'),
(184, 5, 2, 'fmdjd', '', 0, 1, 1, '127.0.0.1', '2017-05-14 15:42:11'),
(185, 5, 2, 'vhbhjh', '', 0, 1, 1, '127.0.0.1', '2017-05-14 15:44:08'),
(186, 5, 2, 'n', '', 0, 1, 1, '127.0.0.1', '2017-05-14 16:57:22'),
(187, 5, 2, 'jj', '', 1, 1, 1, '127.0.0.1', '2017-05-14 17:16:34'),
(188, 5, 2, 'kkkj', '', 0, 1, 1, '127.0.0.1', '2017-05-14 17:16:43'),
(189, 5, 2, 'iii', '', 0, 1, 1, '127.0.0.1', '2017-05-14 17:17:01'),
(190, 5, 2, 'jj', '', 0, 1, 1, '127.0.0.1', '2017-05-14 17:38:58'),
(191, 5, 2, 'uuu', '', 0, 1, 1, '127.0.0.1', '2017-05-14 17:39:02'),
(192, 5, 2, 'kk', '', 0, 1, 1, '127.0.0.1', '2017-05-14 17:40:14'),
(193, 5, 2, 'nn', '', 0, 1, 1, '127.0.0.1', '2017-05-14 17:40:16'),
(194, 5, 2, 'vv', '', 0, 1, 1, '127.0.0.1', '2017-05-14 17:44:23'),
(195, 5, 2, 'bbv', '', 0, 1, 1, '127.0.0.1', '2017-05-14 17:44:36'),
(196, 5, 2, 'bbb', '', 1, 1, 1, '127.0.0.1', '2017-05-14 17:44:54'),
(197, 5, 2, 'hhd', '', 0, 1, 1, '127.0.0.1', '2017-05-18 15:21:42'),
(198, 5, 2, 'kêj', '', 0, 1, 1, '127.0.0.1', '2017-05-18 15:21:45'),
(199, 6, 2, 'jjjj', '', 0, 1, 1, '127.0.0.1', '2017-05-18 15:23:32'),
(200, 6, 2, 'jnhhgtg', '', 0, 1, 1, '127.0.0.1', '2017-05-18 15:23:33'),
(201, 5, 2, 'mvmnv', '', 0, 1, 1, '127.0.0.1', '2017-05-21 16:04:53'),
(202, 5, 2, 'ggg', '', 0, 1, 1, '127.0.0.1', '2017-05-21 16:05:37'),
(203, 5, 2, 'dffdmf', '', 1, 1, 1, '127.0.0.1', '2017-05-21 16:07:34'),
(204, 5, 2, '],fg,mf', '', 0, 1, 1, '127.0.0.1', '2017-05-21 16:07:40'),
(205, 5, 2, 'mfmf', '', 1, 0, 1, '127.0.0.1', '2017-05-21 16:07:48'),
(206, 5, 2, 'nmcnc', '', 1, 0, 1, '127.0.0.1', '2017-05-21 16:09:05'),
(207, 5, 2, 'dfcnc', '', 1, 0, 1, '127.0.0.1', '2017-05-21 16:10:15'),
(208, 5, 2, 'nmjn', '', 1, 0, 0, '127.0.0.1', '2017-05-21 16:10:55'),
(209, 5, 2, 'vvvvvvvvvvvvv', '', 1, 0, 0, '127.0.0.1', '2017-05-21 16:12:30'),
(210, 6, 2, 'kfkfk', '', 0, 1, 0, '127.0.0.1', '2017-05-26 11:30:02'),
(211, 6, 2, 'k,fdjdfh', '', 0, 1, 0, '127.0.0.1', '2017-05-26 11:30:04'),
(212, 6, 2, 'mfjfj', '', 0, 1, 0, '127.0.0.1', '2017-05-26 11:30:05');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `CommentId` int(11) NOT NULL,
  `ArticleId` int(11) DEFAULT NULL,
  `Comment` text NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `CommentStarId` tinyint(4) NOT NULL,
  `UserAgent` varchar(250) NOT NULL,
  `IpAddress` varchar(15) NOT NULL,
  `FullName` varchar(250) DEFAULT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`CommentId`, `ArticleId`, `Comment`, `StatusId`, `CommentStarId`, `UserAgent`, `IpAddress`, `FullName`, `CrUserId`, `CrDateTime`) VALUES
(1, NULL, 'Mọi người rất nhiệt tình, hàng về nhanh, về đến đâu báo đến đó, đây là điều mình rất ưng về dịch vụ của Hoàng Vũ. Tài chính cũng minh bạch. Sẽ tiếp tục hợp tác với bên mình tiếp ạ.', 2, 5, 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '::1', NULL, 5, '2017-01-15 10:24:41'),
(2, NULL, 'Mọi người rất nhiệt tình, hành về nhanh, về đến đâu báo đến đó, đây là điều mình rất ưng về dịch vụ của Hoàng Vũ.', 2, 5, 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '::1', NULL, 5, '2017-01-15 10:25:28'),
(3, NULL, 'Mọi người rất nhiệt tình, hành về nhanh, về đến đâu báo đến đó, đây là điều mình rất ưng về dịch vụ của Hoàng Vũ.', 3, 5, 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '::1', NULL, 5, '2017-01-15 10:25:43'),
(4, NULL, 'Rất tôt', 3, 4, 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '::1', NULL, 5, '2017-01-15 12:18:11'),
(5, NULL, 'Test', 1, 1, 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '::1', NULL, 3, '2017-01-15 15:18:23'),
(6, NULL, 'Test', 1, 2, 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '::1', NULL, 5, '2017-01-15 15:18:53'),
(7, NULL, 'nfnfnmfn', 3, 3, 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36', '::1', 'Vân Anh', 0, '2017-05-20 17:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `complaintmsgs`
--

CREATE TABLE `complaintmsgs` (
  `ComplaintMsgId` int(11) NOT NULL,
  `ComplaintId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Message` varchar(250) NOT NULL,
  `ComplaintMsgTypeId` tinyint(4) NOT NULL,
  `IsAccept` tinyint(4) NOT NULL DEFAULT '0',
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complaintmsgs`
--

INSERT INTO `complaintmsgs` (`ComplaintMsgId`, `ComplaintId`, `UserId`, `Message`, `ComplaintMsgTypeId`, `IsAccept`, `CrDateTime`) VALUES
(1, 1, 1, 'aaaaaaaa', 1, 0, '2016-12-06 00:00:00'),
(2, 1, 2, 'bbb', 7, 0, '2016-12-06 00:00:00'),
(3, 1, 1, 'cccc', 3, 0, '2016-12-25 16:55:13'),
(4, 1, 1, 'ddd', 3, 0, '2016-12-25 16:59:52'),
(5, 1, 1, 'eeeeeeeee', 3, 0, '2016-12-25 17:02:34'),
(6, 1, 5, 'ok', 8, 0, '2016-12-31 17:08:06'),
(7, 1, 5, 'ok1', 8, 0, '2016-12-31 17:08:14'),
(8, 1, 5, 'ok2', 8, 1, '2017-01-01 09:34:38'),
(9, 2, 1, 'QL ko duyet 1', 7, 0, '2017-04-29 16:43:13');

-- --------------------------------------------------------

--
-- Table structure for table `complaints`
--

CREATE TABLE `complaints` (
  `ComplaintId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `ComplaintStatusId` tinyint(4) NOT NULL,
  `ComplaintTypes` varchar(45) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `CareStaffId` int(11) NOT NULL,
  `OrderUserId` int(11) NOT NULL,
  `ProductImage` varchar(650) NOT NULL,
  `WaybillCodeImage` varchar(650) NOT NULL,
  `Comment` varchar(250) NOT NULL,
  `ComplaintOwnerId` tinyint(4) NOT NULL,
  `ComplaintSolutions` varchar(45) NOT NULL,
  `ExchangeRate` int(11) NOT NULL,
  `ExchangeCostTQ` decimal(10,2) NOT NULL,
  `ReduceCostTQ` decimal(10,2) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complaints`
--

INSERT INTO `complaints` (`ComplaintId`, `OrderId`, `ProductId`, `ComplaintStatusId`, `ComplaintTypes`, `CustomerId`, `CareStaffId`, `OrderUserId`, `ProductImage`, `WaybillCodeImage`, `Comment`, `ComplaintOwnerId`, `ComplaintSolutions`, `ExchangeRate`, `ExchangeCostTQ`, `ReduceCostTQ`, `CrDateTime`) VALUES
(1, 22, 316, 1, '["1"]', 6, 2, 0, '["ao-so-mi-5885bd4f64274-58e0b3004362d.jpg"]', '["no-product.png"]', 'zassssss', 1, '', 3456, '0.00', '0.00', '2017-05-02 21:03:26'),
(2, 33, 357, 1, '["1"]', 6, 2, 0, '[]', '[]', 'mnnnn', 1, '', 3456, '0.00', '0.00', '2017-05-27 08:26:45');

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `ConfigId` tinyint(4) NOT NULL,
  `ConfigCode` varchar(45) NOT NULL,
  `ConfigName` varchar(100) NOT NULL,
  `ConfigValue` text NOT NULL,
  `AutoLoad` tinyint(4) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`ConfigId`, `ConfigCode`, `ConfigName`, `ConfigValue`, `AutoLoad`, `CrUserId`, `CrDateTime`) VALUES
(1, 'EMAIL_COMPANY', 'Email', 'nhaphang365@gmail.com.vn', 1, 1, '2017-08-27 23:11:28'),
(2, 'COMPANY_NAME', 'Tên công ty', 'Nhập hàng 365', 1, 1, '2017-08-27 23:11:28'),
(3, 'SITE_NAME', 'Tên trang web', 'Nhập hàng 365', 1, 1, '2017-08-27 23:11:28'),
(4, 'EXCHAGE_RATE_CN', 'Tỉ giá Tệ hiện tại', '3456', 0, 1, '2016-09-05 11:19:29'),
(5, 'PHONE_CARE', 'Phone CSKH', '0123456789', 1, 1, '2017-09-12 20:37:19'),
(6, 'FACEBOOK', 'Facebook', 'https://www.facebook.com/Nhaphang365/?fref=ts', 1, 1, '2017-08-27 23:11:28');

-- --------------------------------------------------------

--
-- Table structure for table `customerbalances`
--

CREATE TABLE `customerbalances` (
  `CustomerBalanceId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `TransactionId` int(11) NOT NULL,
  `Balance` int(11) NOT NULL,
  `IsLast` tinyint(4) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customerbalances`
--

INSERT INTO `customerbalances` (`CustomerBalanceId`, `CustomerId`, `TransactionId`, `Balance`, `IsLast`, `CrDateTime`) VALUES
(1, 6, 2, -1099008, 0, '2017-05-04 11:01:42'),
(2, 6, 3, 992, 0, '2017-05-04 11:03:28'),
(3, 6, 4, 35552, 0, '2017-05-07 17:58:40'),
(4, 5, 5, -60000, 0, '2017-05-25 11:15:51'),
(5, 5, 6, 0, 0, '2017-05-25 11:15:51'),
(6, 6, 19, 135552, 0, '2017-05-27 08:28:50'),
(7, 6, 18, 235552, 0, '2017-05-27 08:29:13'),
(8, 6, 17, 335552, 0, '2017-05-27 08:49:47'),
(9, 6, 19, 435552, 0, '2017-05-27 09:04:07'),
(10, 6, 19, 535552, 0, '2017-05-27 09:07:04'),
(11, 6, 18, 1535552, 0, '2017-05-27 09:15:57'),
(12, 6, 19, 1635552, 0, '2017-05-27 09:48:12'),
(13, 6, 18, 2635552, 0, '2017-05-27 09:49:13'),
(14, 6, 17, 2735552, 0, '2017-05-27 09:49:26'),
(15, 3, 20, -130000, 0, '2017-05-29 10:58:38'),
(16, 3, 21, -155500, 0, '2017-05-29 11:00:37'),
(17, 3, 22, -405500, 0, '2017-05-29 11:01:40'),
(18, 6, 23, 2549654, 0, '2017-05-29 17:49:04'),
(19, 6, 24, 2363756, 0, '2017-06-01 12:13:34'),
(20, 6, 25, 2549654, 0, '2017-06-01 12:18:34'),
(21, 6, 26, 2735552, 0, '2017-06-01 12:26:13'),
(22, 6, 27, 2921450, 0, '2017-06-01 12:28:07'),
(23, 6, 28, 3107348, 0, '2017-06-01 12:32:09'),
(24, 6, 29, 2921450, 0, '2017-06-01 22:36:11'),
(25, 6, 30, -2381011, 0, '2017-06-02 22:37:23'),
(26, 6, 31, -7683472, 1, '2017-06-02 22:40:29'),
(27, 3, 38, -155500, 0, '2017-06-06 15:48:42'),
(28, 3, 39, -130000, 0, '2017-06-06 15:59:57'),
(29, 3, 40, 0, 0, '2017-06-06 16:00:37'),
(30, 3, 41, -29000, 0, '2017-06-06 17:45:21'),
(31, 5, 47, -6600027, 0, '2017-06-17 11:23:07'),
(32, 5, 50, -7139854, 1, '2017-06-28 13:58:46'),
(33, 3, 54, -360353, 1, '2017-07-03 16:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `moneysources`
--

CREATE TABLE `moneysources` (
  `MoneySourceId` smallint(6) NOT NULL,
  `MoneySourceName` varchar(250) NOT NULL,
  `StatusId` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `moneysources`
--

INSERT INTO `moneysources` (`MoneySourceId`, `MoneySourceName`, `StatusId`) VALUES
(1, 'Tiền mặt', 2),
(2, 'VCB 123', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `NotificationId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `RoleId` tinyint(4) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `ProductId` int(11) NOT NULL,
  `ComplaintId` int(11) NOT NULL,
  `Message` varchar(650) NOT NULL,
  `IsCustomerSend` tinyint(4) NOT NULL,
  `NotificationStatusId` tinyint(4) NOT NULL,
  `LinkTo` varchar(250) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`NotificationId`, `CustomerId`, `UserId`, `RoleId`, `OrderId`, `ProductId`, `ComplaintId`, `Message`, `IsCustomerSend`, `NotificationStatusId`, `LinkTo`, `CrDateTime`) VALUES
(1, 6, 2, 1, 22, 0, 0, 'Sản phẩm bvv - Màu sắc: cd - Kích thước: f - hết hàng', 0, 2, 'chi-tiet-don-hang-22', '2017-04-17 23:02:59'),
(2, 6, 1, 3, 22, 0, 0, 'Sản phẩm bvv - Màu sắc: cd - Kích thước: f - hết hàng', 0, 2, 'chi-tiet-don-hang-22', '2017-04-17 23:09:19'),
(3, 6, 1, 3, 22, 0, 0, 'Sản phẩm bvv - Màu sắc: cd - Kích thước: f - hết hàng', 0, 2, 'chi-tiet-don-hang-22', '2017-04-17 23:13:51'),
(4, 6, 1, 3, 22, 0, 0, 'Sản phẩm b - Màu sắc: c - Kích thước: d - đang chờ Khách hàng đồng ý tăng giá vào lúc 18/04/2017 08:47. Lí do: tang gia', 0, 2, 'chi-tiet-don-hang-22', '2017-04-18 08:47:28'),
(5, 6, 1, 3, 22, 0, 0, 'Sản phẩm b - Màu sắc: c - Kích thước: d - đang chờ Khách hàng đồng ý tăng giá vào lúc 18/04/2017 08:55. Lí do: Tăng giá', 0, 2, 'chi-tiet-don-hang-22', '2017-04-18 08:55:45'),
(6, 6, 2, 1, 22, 0, 0, 'Sản phẩm b - Màu sắc: c - Kích thước: d - được Khách hàng đồng ý tăng giá vào lúc 18/04/2017 20:57. Xác nhận: đồng ý tăng giá', 1, 2, 'chi-tiet-don-hang-22', '2017-04-18 20:57:15'),
(7, 6, 1, 3, 22, 0, 0, 'Sản phẩm b - Màu sắc: c - Kích thước: d - được Khách hàng đồng ý tăng giá vào lúc 18/04/2017 20:59. Xác nhận: đồng ý tăng giá', 1, 2, 'chi-tiet-don-hang-22', '2017-04-18 20:59:37'),
(8, 6, 2, 1, 24, 0, 0, 'Đơn hàng D290410024 đã được nhân viên CSKH To check.', 0, 2, 'chi-tiet-don-hang-24', '2017-05-26 08:32:17'),
(9, 6, 1, 1, 18, 0, 0, 'Đơn hàng  nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-18', '2017-05-26 10:36:03'),
(10, 6, 1, 1, 18, 0, 0, 'Đơn hàng  nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-18', '2017-05-26 10:38:09'),
(11, 6, 1, 1, 18, 0, 0, 'Đơn hàng  nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-18', '2017-05-26 10:41:20'),
(12, 6, 1, 1, 18, 0, 0, 'Đơn hàng  nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-18', '2017-05-26 10:42:15'),
(13, 6, 1, 1, 18, 0, 0, 'Đơn hàng D020410018 nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-18', '2017-05-26 10:46:14'),
(14, 6, 2, 1, 28, 0, 0, 'Đơn hàng D010510028 đã được nhân viên CSKH To check.', 0, 2, 'chi-tiet-don-hang-28', '2017-05-27 08:11:57'),
(15, 6, 2, 1, 28, 0, 0, 'Đơn hàng D010510028 nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-28', '2017-05-27 08:15:16'),
(16, 6, 2, 1, 31, 0, 0, 'Khách hàng  đã gửi đơn hàng D270510031 cần check.', 1, 2, 'order/edit/31', '2017-05-27 08:16:49'),
(17, 6, 2, 1, 32, 0, 0, 'Khách hàng  đã gửi đơn hàng D270510032 cần check.', 1, 2, 'order/edit/32', '2017-05-27 08:17:29'),
(18, 6, 2, 1, 33, 0, 0, 'Khách hàng Đoàn Xuân Phúc đã gửi đơn hàng D270510033 cần check.', 1, 2, 'order/edit/33', '2017-05-27 08:21:18'),
(19, 6, 2, 1, 33, 0, 0, 'Đơn hàng D270510033 đã được nhân viên CSKH To check.', 0, 2, 'chi-tiet-don-hang-33', '2017-05-27 08:22:46'),
(20, 6, 2, 1, 33, 0, 0, 'Đơn hàng D270510033 đã được chốt.', 1, 2, 'order/edit/33', '2017-05-27 08:22:52'),
(21, 6, 2, 1, 33, 357, 2, 'Đơn hàng D270510033 khách có khiếu nại.', 1, 2, 'order/edit/33/2', '2017-05-27 08:26:45'),
(22, 6, 2, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 100,000 VNĐ.', 0, 2, 'customer/transaction', '2017-05-27 08:49:47'),
(24, 6, 2, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 100,000 VNĐ.', 0, 2, 'customer/transaction', '2017-05-27 09:04:07'),
(26, 6, 2, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 100,000 VNĐ.', 0, 2, 'customer/transaction', '2017-05-27 09:07:04'),
(28, 6, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 1,000,000 VNĐ.', 0, 2, 'customer/transaction', '2017-05-27 09:15:57'),
(30, 6, 1, 3, 33, 0, 0, 'Hệ thống đã nhận được khoản tiền 100,000 VNĐ.', 0, 2, 'customer/transaction', '2017-05-27 09:48:12'),
(31, 6, 2, 1, 33, 0, 0, 'Phiếu thu #19 gắn với khách Đoàn Xuân Phúc đã được duyệt.', 1, 2, 'transaction/edit/19', '2017-05-27 09:48:12'),
(32, 6, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 1,000,000 VNĐ.', 0, 2, 'customer/transaction', '2017-05-27 09:49:13'),
(34, 6, 1, 3, 28, 0, 0, 'Hệ thống đã nhận được khoản tiền 100,000 VNĐ.', 0, 2, 'customer/transaction', '2017-05-27 09:49:26'),
(35, 6, 2, 1, 28, 0, 0, 'Phiếu thu #17 gắn với khách Đoàn Xuân Phúc đã được duyệt.', 1, 2, 'transaction/edit/17', '2017-05-27 09:49:26'),
(36, 6, 2, 1, 33, 0, 0, 'Khách hàng đã lựa chọn nhân viên khác check đơn D270510033', 1, 2, 'order/edit/33', '2017-05-29 17:33:03'),
(37, 6, 14, 1, 33, 0, 0, 'Khách hàng Đoàn Xuân Phúc đã gửi đơn hàng D270510033 cần check.', 1, 2, 'order/edit/33', '2017-05-29 17:33:03'),
(38, 6, 2, 1, 32, 0, 0, 'Khách hàng đã lựa chọn nhân viên khác check đơn D270510032', 1, 2, 'order/edit/32', '2017-05-29 17:42:21'),
(39, 6, 14, 1, 32, 0, 0, 'Khách hàng Đoàn Xuân Phúc đã gửi đơn hàng D270510032 cần check.', 1, 2, 'order/edit/32', '2017-05-29 17:42:21'),
(40, 6, 1, 1, 34, 0, 0, 'Đơn hàng D010610034 đã được chốt.', 1, 2, 'order/edit/34', '2017-06-01 09:21:48'),
(41, 6, 1, 3, 32, 0, 0, 'Hoàn tiền đơn hàng D270510032.', 0, 2, 'chi-tiet-don-hang-32', '2017-06-01 12:26:13'),
(42, 6, 1, 3, 32, 0, 0, 'Hoàn tiền đơn hàng D270510032.', 0, 2, 'chi-tiet-don-hang-32', '2017-06-01 12:28:07'),
(43, 6, 1, 3, 32, 0, 0, 'Hoàn tiền đơn hàng D270510032.', 0, 2, 'chi-tiet-don-hang-32', '2017-06-01 12:32:09'),
(44, 6, 1, 1, 35, 0, 0, 'Đơn hàng D020610035 đã được chốt.', 1, 2, 'order/edit/35', '2017-06-02 22:36:53'),
(45, 6, 1, 3, 35, 380, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm tăng giá với lý do "Dạ anh/ chị ơi, sản phẩm này shop báo tăng giá, anh/ chị có đặt không ạ?", quý khách xin vui lòng xác nhận.', 0, 2, 'chi-tiet-don-hang-35', '2017-06-04 14:12:33'),
(46, 6, 2, 1, 35, 380, 0, 'Đơn hàng D020610035 được khách hàng chấp nhận tăng giá - Xác nhận: "Đồng ý"', 1, 2, 'order/edit/35', '2017-06-04 16:46:26'),
(47, 6, 1, 3, 35, 379, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-35', '2017-06-04 16:54:30'),
(48, 6, 1, 3, 35, 379, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-35', '2017-06-04 17:00:56'),
(49, 6, 1, 3, 35, 379, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-35', '2017-06-04 17:12:07'),
(50, 3, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 29,000 VNĐ.', 0, 2, 'customer/transaction', '2017-06-06 14:43:34'),
(51, 3, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 250,000 VNĐ.', 0, 2, 'customer/transaction', '2017-06-06 15:48:42'),
(52, 3, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 250,000 VNĐ.', 0, 2, 'customer/transaction', '2017-06-06 15:48:42'),
(53, 3, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 25,500 VNĐ.', 0, 2, 'customer/transaction', '2017-06-06 15:59:57'),
(54, 3, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 25,500 VNĐ.', 0, 2, 'customer/transaction', '2017-06-06 15:59:57'),
(55, 3, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 130,000 VNĐ.', 0, 2, 'customer/transaction', '2017-06-06 16:00:37'),
(56, 3, 1, 3, 0, 0, 0, 'Hệ thống đã nhận được khoản tiền 130,000 VNĐ.', 0, 2, 'customer/transaction', '2017-06-06 16:00:37'),
(57, 6, 1, 3, 33, 357, 0, 'Đơn hàng D270510033 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-33-shop-D270510033', '2017-06-08 16:38:05'),
(58, 6, 0, 6, 33, 357, 0, 'Đơn hàng D270510033 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 1, 2, 'order/edit/33', '2017-06-08 16:38:05'),
(59, 6, 1, 3, 35, 381, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-35-shop-D020610035', '2017-06-08 17:10:01'),
(60, 6, 0, 6, 35, 381, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 1, 2, 'chi-tiet-don-hang-35-shop-D020610035', '2017-06-08 17:10:01'),
(61, 6, 1, 3, 35, 378, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 2, 'chi-tiet-don-hang-35-shop-HV_13', '2017-06-08 17:17:18'),
(62, 6, 0, 6, 35, 378, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 1, 2, 'chi-tiet-don-hang-35-shop-HV_13', '2017-06-08 17:17:18'),
(63, 6, 1, 3, 35, 0, 0, 'Dạ anh/ chị ơi, sản phẩm này shop HV_15 báo tăng phí ship, anh/ chị có đặt không ạ?', 1, 2, 'order/edit/35/0/0HV_15', '2017-06-11 09:23:49'),
(64, 6, 1, 3, 35, 0, 0, 'Dạ anh/ chị ơi, đơn hàng D020610035 có shop HV_15 báo tăng phí ship, quý khách xin vui lòng xác nhận.', 0, 2, 'chi-tiet-don-hang-35-shop-HV_15', '2017-06-11 09:51:55'),
(65, 5, 1, 1, 36, 0, 0, 'Đơn hàng D110610036 nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 1, 'chi-tiet-don-hang-36', '2017-06-11 14:16:06'),
(66, 6, 2, 1, 35, 0, 0, 'Đơn hàng D020610035 được khách hàng chấp nhận tăng giá shop HV_15 - Xác nhận: "ok"', 1, 1, 'order/edit/35/0/0HV_15', '2017-06-11 19:45:44'),
(67, 6, 0, 6, 35, 0, 0, 'Đơn hàng D020610035 được khách hàng chấp nhận tăng giá shop HV_15 - Xác nhận: "ok"', 1, 1, 'order/edit/35/0/0HV_15', '2017-06-11 19:45:44'),
(68, 6, 1, 3, 35, 0, 0, 'Dạ anh/ chị ơi, đơn hàng D020610035 có shop HV_7 báo tăng phí ship, quý khách xin vui lòng xác nhận.', 0, 2, 'chi-tiet-don-hang-35-shop-HV_7', '2017-06-11 19:49:16'),
(69, 6, 2, 1, 35, 0, 0, 'Đơn hàng D020610035 khách hàng KHÔNG chấp nhận tăng giá shop HV_7 - Xác nhận: "ko ok"', 1, 1, 'order/edit/35/0/0HV_7', '2017-06-11 20:07:36'),
(70, 6, 0, 6, 35, 0, 0, 'Đơn hàng D020610035 khách hàng KHÔNG chấp nhận tăng giá shop HV_7 - Xác nhận: "ko ok"', 1, 1, 'order/edit/35/0/0HV_7', '2017-06-11 20:07:36'),
(71, 6, 2, 1, 35, 0, 0, 'Đơn hàng D020610035 khách hàng KHÔNG chấp nhận tăng giá shop HV_7 - Xác nhận: "ko ok"', 1, 1, 'order/edit/35/0/0HV_7', '2017-06-11 20:18:42'),
(72, 6, 0, 6, 35, 0, 0, 'Đơn hàng D020610035 khách hàng KHÔNG chấp nhận tăng giá shop HV_7 - Xác nhận: "ko ok"', 1, 1, 'order/edit/35/0/0HV_7', '2017-06-11 20:18:42'),
(73, 5, 1, 3, 36, 402, 0, 'Đơn hàng D110610036 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 1, 'chi-tiet-don-hang-36-sp-402', '2017-06-20 10:08:13'),
(74, 5, 0, 6, 36, 402, 0, 'Đơn hàng D110610036 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 1, 1, 'chi-tiet-don-hang-36-sp-402', '2017-06-20 10:08:13'),
(75, 5, 1, 3, 36, 402, 0, 'Đơn hàng D110610036 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 1, 'chi-tiet-don-hang-36-sp-402', '2017-06-20 10:25:28'),
(76, 5, 0, 6, 36, 402, 0, 'Đơn hàng D110610036 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 1, 1, 'chi-tiet-don-hang-36-sp-402', '2017-06-20 10:25:28'),
(77, 3, 1, 1, 39, 0, 0, 'Đơn hàng D210610039 đã được chốt.', 1, 1, 'order/edit/39', '2017-06-21 22:07:00'),
(78, 5, 21, 8, 5, 95, 0, 'Đơn hàng D291010005 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 1, 'chi-tiet-don-hang-5-sp-95', '2017-06-28 14:20:11'),
(79, 5, 0, 6, 5, 95, 0, 'Đơn hàng D291010005 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 1, 1, 'chi-tiet-don-hang-5-sp-95', '2017-06-28 14:20:11'),
(82, 6, 21, 8, 35, 377, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 1, 'chi-tiet-don-hang-35-sp-377', '2017-06-30 16:24:11'),
(83, 6, 0, 6, 35, 377, 0, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 1, 1, 'chi-tiet-don-hang-35-sp-377', '2017-06-30 16:24:11'),
(84, 3, 1, 1, 42, 0, 0, 'Đơn hàng D030710042 đã được chốt.', 1, 1, 'order/edit/42', '2017-07-03 16:33:00'),
(85, 3, 1, 1, 42, 0, 0, 'Khách hàng đã lựa chọn nhân viên khác check đơn D030710042', 1, 1, 'order/edit/42', '2017-07-03 16:53:52'),
(86, 3, 2, 1, 42, 0, 0, 'Khách hàng Cao Xuân Hùng đã gửi đơn hàng D030710042 cần check.', 1, 1, 'order/edit/42', '2017-07-03 16:53:52'),
(87, 3, 21, 8, 42, 444, 0, 'Đơn hàng D030710042 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 0, 1, 'chi-tiet-don-hang-42-sp-444', '2017-07-03 16:56:12'),
(88, 3, 0, 6, 42, 444, 0, 'Đơn hàng D030710042 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', 1, 1, 'chi-tiet-don-hang-42-sp-444', '2017-07-03 16:56:12'),
(89, 6, 2, 1, 23, 0, 0, 'Đơn hàng D290410023 đã được nhân viên CSKH To check.', 0, 1, 'chi-tiet-don-hang-23', '2017-07-31 10:01:25'),
(90, 6, 2, 1, 17, 0, 0, 'Đơn hàng D020410017 đã được nhân viên CSKH To check.', 0, 1, 'chi-tiet-don-hang-17', '2017-07-31 10:01:34'),
(91, 6, 2, 1, 16, 0, 0, 'Đơn hàng D190310016 đã được nhân viên CSKH To check.', 0, 1, 'chi-tiet-don-hang-16', '2017-07-31 10:01:41'),
(92, 6, 2, 1, 15, 0, 0, 'Đơn hàng D190310015 đã được nhân viên CSKH To check.', 0, 1, 'chi-tiet-don-hang-15', '2017-07-31 10:01:50'),
(93, 6, 2, 1, 14, 0, 0, 'Đơn hàng D190310014 đã được nhân viên CSKH To check.', 0, 1, 'chi-tiet-don-hang-14', '2017-07-31 10:01:56'),
(94, 6, 2, 1, 10, 0, 0, 'Đơn hàng D021210010 đã được nhân viên CSKH To check.', 0, 1, 'chi-tiet-don-hang-10', '2017-07-31 10:01:59'),
(95, 6, 2, 1, 28, 0, 0, 'Đơn hàng D010510028 nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 1, 'chi-tiet-don-hang-28', '2017-07-31 10:02:58'),
(96, 6, 2, 1, 28, 0, 0, 'Đơn hàng D010510028 nhân viên đã check xong, bạn vui lòng kiểm tra.', 0, 1, 'chi-tiet-don-hang-28', '2017-07-31 10:03:05');

-- --------------------------------------------------------

--
-- Table structure for table `orderaccounts`
--

CREATE TABLE `orderaccounts` (
  `OrderAccountId` smallint(6) NOT NULL,
  `OrderAccountName` varchar(250) NOT NULL,
  `StatusId` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderaccounts`
--

INSERT INTO `orderaccounts` (`OrderAccountId`, `OrderAccountName`, `StatusId`) VALUES
(1, 'hoanmuada', 2);

-- --------------------------------------------------------

--
-- Table structure for table `orderdraffs`
--

CREATE TABLE `orderdraffs` (
  `OrderDraffId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL DEFAULT '0',
  `FullName` varchar(250) DEFAULT NULL,
  `PhoneNumber` varchar(15) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Address` varchar(250) DEFAULT NULL,
  `CareStaffId` int(11) NOT NULL,
  `WarehouseId` smallint(6) NOT NULL,
  `ExchangeRate` int(11) NOT NULL,
  `Comment` varchar(250) DEFAULT NULL,
  `ProductJson` longtext,
  `IpAddress` varchar(15) DEFAULT NULL,
  `UserAgent` varchar(250) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orderdraffs`
--

INSERT INTO `orderdraffs` (`OrderDraffId`, `UserId`, `FullName`, `PhoneNumber`, `Email`, `Address`, `CareStaffId`, `WarehouseId`, `ExchangeRate`, `Comment`, `ProductJson`, `IpAddress`, `UserAgent`, `CrDateTime`) VALUES
(2, 0, 'Phạm Thị Thu', '0962774396', 'laptrinhphamthu@gmail.com', 'Hà Đông, Hà Nội', 0, 1, 3456, NULL, '[]', '::1', 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) coc_coc_browser/64.4.146 Chrome/58.4.3029.146 Safari/537.36', '2017-08-20 15:19:09'),
(3, 1, NULL, NULL, NULL, NULL, 1, 1, 3456, NULL, '[{"ShopCode":"lenanianfeng","ShopName":"lena\\u5e74\\u4e30\\u4e13\\u5356\\u5e97","ShopUrl":"https:\\/\\/lenanianfeng.world.tmall.com\\/","CategoryId":0,"IsTransportSlow":0,"ShopComment":"","ShopFeedback":"","ProductImage":"http:\\/\\/img.alicdn.com\\/bao\\/uploaded\\/i2\\/2894755708\\/TB2raCFjYBnpuFjSZFGXXX51pXa_!!2894755708.jpg","ProductLink":"https:\\/\\/world.tmall.com\\/item\\/539070176646.htm?skuId=3221410980475","Color":"\\u6a31\\u82b1\\u7c89","Size":"","Quantity":"1","Cost":"69.9","ShipTQ":0,"ProductStatusId":2,"Comment":"","FeedBack":""},{"ShopCode":"lenanianfeng","ShopName":"lena\\u5e74\\u4e30\\u4e13\\u5356\\u5e97","ShopUrl":"https:\\/\\/lenanianfeng.world.tmall.com\\/","CategoryId":0,"IsTransportSlow":0,"ShopComment":"","ShopFeedback":"","ProductImage":"http:\\/\\/img.alicdn.com\\/bao\\/uploaded\\/i2\\/2894755708\\/TB2raCFjYBnpuFjSZFGXXX51pXa_!!2894755708.jpg","ProductLink":"https:\\/\\/world.tmall.com\\/item\\/539070176646.htm?spm=a220m.1000858.0.0.d8117976jOh0W&id=539070176646&skuId=3221410980475&is_b=1&cat_id=2&q=%BE%ED%B7%A2%B0%F4+%B4%F3%BE%ED+%B5%E7%BE%ED%B0%F4+%CC%CC%B7%A2%C6%F7","Color":"\\u6a31\\u82b1\\u7c89","Size":"","Quantity":5,"Cost":"69.9","ShipTQ":0,"ProductStatusId":2,"Comment":"ttgg","FeedBack":""}]', '::1', 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', '2017-07-25 19:33:26'),
(4, 6, 'Đoàn Xuân Phúc', '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 0, 0, 3456, NULL, '[{"ShopCode":"shop60859944","ShopName":"\\u950b\\u72c2\\u4e16\\u7eaa","ShopUrl":"https:\\/\\/shop60859944.world.taobao.com\\/","CategoryId":0,"IsTransportSlow":0,"ShopComment":"","ShopFeedback":"","ProductImage":"http:\\/\\/gd1.alicdn.com\\/bao\\/uploaded\\/i1\\/386213293\\/TB2_IyDwlNkpuFjy0FaXXbRCVXa_!!386213293.jpg","ProductLink":"https:\\/\\/world.taobao.com\\/item\\/554873620431.htm?fromSite=main&spm=a21wu.241046-global.4691948847.1.4519fd50VMf0pA&scm=1007.15423.84311.100200300000005&pvid=f703356c-20ef-49ae-ac99-527f71f5f6d6","Color":"","Size":"","Quantity":"1","Cost":"78","ShipTQ":0,"ProductStatusId":2,"Comment":"","FeedBack":""}]', '::1', 'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36', '2017-07-27 11:50:07');

-- --------------------------------------------------------

--
-- Table structure for table `orderlogs`
--

CREATE TABLE `orderlogs` (
  `OrderLogId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `PhoneNumber` varchar(15) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Address` varchar(250) NOT NULL,
  `CareStaffId` int(11) NOT NULL,
  `WarehouseId` smallint(6) NOT NULL,
  `ExchangeRate` int(11) NOT NULL,
  `OrderStatusId` tinyint(4) NOT NULL,
  `ServicePercent` tinyint(4) NOT NULL DEFAULT '0',
  `IsFixPercent` tinyint(4) NOT NULL DEFAULT '0',
  `OtherCost` int(11) NOT NULL DEFAULT '0',
  `Comment` varchar(250) DEFAULT NULL,
  `CustomerOkDate` datetime DEFAULT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `OrderUserId` int(11) DEFAULT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `OrderId` int(11) NOT NULL,
  `OrderCode` varchar(15) DEFAULT NULL,
  `CustomerId` int(11) NOT NULL,
  `PhoneNumber` varchar(15) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Address` varchar(250) NOT NULL,
  `CareStaffId` int(11) NOT NULL,
  `WarehouseId` smallint(6) NOT NULL,
  `ExchangeRate` int(11) NOT NULL,
  `OrderStatusId` tinyint(4) NOT NULL,
  `ServicePercent` tinyint(4) NOT NULL DEFAULT '0',
  `IsFixPercent` tinyint(4) NOT NULL DEFAULT '0',
  `OtherCost` int(11) NOT NULL DEFAULT '0',
  `Comment` varchar(250) DEFAULT NULL,
  `CustomerOkDate` datetime DEFAULT NULL,
  `OrderDate` datetime DEFAULT NULL,
  `TrackingStatusId` tinyint(4) NOT NULL DEFAULT '1',
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `OrderUserId` int(11) NOT NULL DEFAULT '0',
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`OrderId`, `OrderCode`, `CustomerId`, `PhoneNumber`, `Email`, `Address`, `CareStaffId`, `WarehouseId`, `ExchangeRate`, `OrderStatusId`, `ServicePercent`, `IsFixPercent`, `OtherCost`, `Comment`, `CustomerOkDate`, `OrderDate`, `TrackingStatusId`, `CrUserId`, `CrDateTime`, `OrderUserId`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 'D101010001', 5, '0962774396', 'laptrinhphamthu@gmail.com', 'Hà Đông, Hà Nội', 1, 1, 3456, 1, 0, 0, 0, NULL, NULL, NULL, 1, 5, '2016-10-10 21:42:14', 2, 5, '2016-10-29 12:29:31'),
(2, 'D111010002', 5, '0962774396', 'laptrinhphamthu@gmail.com', 'Hà Đông, Hà Nội', 1, 1, 3456, 1, 0, 0, 0, NULL, '2016-10-29 00:00:00', NULL, 1, 5, '2016-10-11 08:11:59', 0, 1, '2017-03-26 19:45:29'),
(3, 'D171010003', 5, '0962774396', 'laptrinhphamthu@gmail.com', 'Hà Đông, Hà Nội', 0, 1, 3456, 1, 10, 0, 10000, NULL, NULL, NULL, 1, 5, '2016-10-17 16:59:16', 0, 1, '2017-03-27 13:33:06'),
(4, 'D291010004', 5, '0962774396', 'laptrinhphamthu@gmail.com', 'Hà Đông, Hà Nội', 1, 1, 3456, 1, 0, 0, 0, NULL, NULL, NULL, 1, 5, '2016-10-29 23:00:04', 0, 1, '2016-11-09 19:52:21'),
(5, 'D291010005', 5, '0962774396', 'laptrinhphamthu@gmail.com', 'Hà Đông, Hà Nội', 0, 1, 3456, 5, 10, 1, 0, NULL, NULL, '2017-06-28 13:58:46', 2, 5, '2016-10-29 23:02:04', 0, 1, '2017-06-28 13:58:46'),
(6, 'D121110006', 5, '0962774396', 'laptrinhphamthu@gmail.com', 'Hà Đông, Hà Nội', 0, 1, 3397, 1, 0, 0, 0, NULL, NULL, NULL, 1, 1, '2016-11-12 15:36:41', 0, NULL, NULL),
(7, 'D141110007', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 0, 1, 3399, 1, 0, 0, 100000, NULL, NULL, NULL, 1, 1, '2016-11-14 13:15:37', 0, NULL, NULL),
(8, 'D141110008', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 0, 1, 3399, 1, 0, 0, 0, NULL, NULL, NULL, 1, 1, '2016-11-14 20:45:54', 0, NULL, NULL),
(9, 'D021210009', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 1, 1, 3399, 1, 0, 0, 0, NULL, NULL, NULL, 1, 6, '2016-12-02 15:39:48', 0, NULL, NULL),
(10, 'D021210010', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3399, 2, 5, 0, 0, NULL, NULL, NULL, 1, 6, '2016-12-02 15:45:48', 0, 2, '2017-07-31 10:01:59'),
(11, 'D021210011', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 1, 1, 3399, 1, 5, 0, 0, NULL, NULL, NULL, 1, 6, '2016-12-02 15:46:06', 0, 1, '2017-05-04 10:33:47'),
(12, 'D021210012', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 1, 1, 3399, 1, 5, 0, 100000, NULL, NULL, NULL, 1, 6, '2016-12-02 15:49:13', 0, 1, '2017-01-10 16:13:25'),
(13, 'D160210013', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 1, 1, 3456, 1, 5, 0, 0, NULL, '2017-02-16 12:27:34', NULL, 1, 1, '2017-02-16 12:27:34', 0, 1, '2017-02-18 16:57:18'),
(14, 'D190310014', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3399, 2, 5, 0, 0, NULL, NULL, NULL, 1, 1, '2017-03-19 15:51:43', 0, 2, '2017-07-31 10:01:56'),
(15, 'D190310015', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3399, 2, 5, 0, 0, NULL, NULL, NULL, 1, 1, '2017-03-19 15:53:26', 0, 2, '2017-07-31 10:01:50'),
(16, 'D190310016', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3399, 2, 5, 0, 0, NULL, NULL, NULL, 1, 1, '2017-03-19 15:54:21', 0, 2, '2017-07-31 10:01:41'),
(17, 'D020410017', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 2, 5, 0, 0, 'abc abc', NULL, NULL, 1, 6, '2017-04-02 15:58:08', 0, 2, '2017-07-31 10:01:34'),
(18, 'D020410018', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 3, 10, 0, 0, 'ghi chú đơn hàng', NULL, NULL, 1, 6, '2017-04-08 09:18:10', 0, 1, '2017-05-26 10:46:14'),
(19, 'D080410019', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 1, 10, 0, 0, 'aaa', NULL, NULL, 1, 6, '2017-04-08 16:52:14', 0, NULL, NULL),
(22, 'D120410022', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 2, 11, 1, 0, 'ghi chus', '2017-04-28 16:19:55', NULL, 1, 6, '2017-04-12 10:32:51', 0, 2, '2017-05-17 16:42:10'),
(23, 'D290410023', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 2, 10, 0, 0, 'abc', NULL, NULL, 1, 6, '2017-04-29 08:55:01', 0, 2, '2017-07-31 10:01:25'),
(24, 'D290410024', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 2, 10, 0, 0, 'abc', NULL, NULL, 1, 6, '2017-04-29 08:55:29', 0, 2, '2017-05-26 08:32:17'),
(25, 'D290410025', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 4, 5, 0, 0, 'abc', '2017-05-04 13:28:37', NULL, 1, 6, '2017-04-29 08:57:19', 0, 2, '2017-05-04 13:28:37'),
(28, 'D010510028', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 3, 10, 0, 0, 'TEST 0105', NULL, NULL, 1, 6, '2017-05-01 10:03:09', 0, 2, '2017-07-31 10:03:05'),
(29, 'D070510029', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 0, 1, 3456, 3, 1, 1, 0, 'abc', NULL, NULL, 1, 1, '2017-05-07 08:49:46', 0, 6, '2017-05-19 17:23:47'),
(30, 'D230510030', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 1, 1, 3347, 3, 5, 0, 0, NULL, NULL, NULL, 1, 1, '2017-05-23 20:00:37', 0, NULL, NULL),
(31, 'D270510031', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3456, 5, 10, 0, 0, NULL, NULL, NULL, 1, 6, '2017-05-27 08:16:49', 19, 1, '2017-06-03 15:06:31'),
(32, 'D270510032', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 14, 1, 3456, 8, 10, 0, 0, NULL, NULL, NULL, 1, 6, '2017-05-27 08:17:29', 0, 1, '2017-06-01 12:32:09'),
(33, 'D270510033', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 14, 1, 3456, 6, 10, 0, 0, NULL, NULL, NULL, 1, 6, '2017-05-27 08:21:18', 0, 1, '2017-05-31 17:48:50'),
(34, 'D010610034', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 1, 1, 3456, 7, 5, 0, 0, NULL, '2017-06-01 09:21:48', NULL, 1, 1, '2017-06-01 09:21:48', 0, 1, '2017-06-01 09:35:08'),
(35, 'D020610035', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 2, 1, 3347, 10, 2, 1, 10000, 'Ghi chu don hang', '2017-06-28 00:00:00', '2017-06-28 00:00:00', 1, 1, '2017-06-02 22:36:53', 19, 21, '2017-06-17 16:49:31'),
(36, 'D110610036', 5, '0962774396', 'laptrinhphamthu@gmail.com', 'Hà Đông, Hà Nội', 1, 1, 3456, 5, 5, 1, 158000, 'ccvc', NULL, '2017-06-17 11:23:07', 1, 1, '2017-06-11 14:11:27', 0, 1, '2017-06-17 11:23:07'),
(37, 'D140610037', 3, '0903210339', '0903210339@gmail.com', 'Khách qua lấy,Hà Nội', 1, 1, 3347, 3, 7, 0, 0, NULL, NULL, NULL, 1, 1, '2017-06-14 00:00:00', 0, NULL, NULL),
(38, 'D150610038', 3, '0903210339', '0903210339@gmail.com', 'Khách qua lấy,Hà Nội', 1, 1, 3347, 3, 7, 0, 0, NULL, NULL, NULL, 1, 1, '2017-06-15 00:00:00', 0, NULL, NULL),
(39, 'D210610039', 3, '0903210339', '0903210339@gmail.com', 'Khách qua lấy,Hà Nội', 1, 1, 3347, 4, 5, 0, 0, NULL, '2017-06-21 22:07:00', NULL, 1, 1, '2017-06-21 22:07:00', 0, NULL, NULL),
(41, 'D300610041', 3, '0903210339', '0903210339@gmail.com', 'Khách qua lấy,Hà Nội', 21, 1, 3347, 3, 7, 0, 0, NULL, NULL, NULL, 1, 21, '2017-06-30 14:41:00', 0, NULL, NULL),
(42, 'D030710042', 3, '0903210339', '0903210339@gmail.com', 'Khách qua lấy,Hà Nội', 2, 1, 3347, 10, 10, 1, 0, NULL, NULL, '2017-07-03 16:53:42', 2, 1, '2017-07-03 16:33:00', 21, 21, '2017-07-05 09:55:28'),
(43, 'D070710043', 3, '0903210339', '0903210339@gmail.com', 'Khách qua lấy,Hà Nội', 1, 1, 3347, 3, 10, 0, 0, NULL, NULL, NULL, 1, 1, '2017-07-07 11:44:00', 0, NULL, NULL),
(44, 'D200710044', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 14, 1, 3456, 1, 10, 0, 0, NULL, NULL, NULL, 1, 6, '2017-07-20 17:42:11', 0, NULL, NULL),
(45, 'D200710045', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 14, 1, 3456, 1, 10, 0, 0, NULL, NULL, NULL, 1, 6, '2017-07-20 17:44:41', 0, NULL, NULL),
(46, 'D200710046', 6, '0985867245', 'phucdx16@wru.vn', 'Ngõ 22 Tôn Thất Tùng', 14, 1, 3456, 1, 10, 0, 0, NULL, NULL, NULL, 1, 6, '2017-07-20 18:59:04', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ProductId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `ProductLink` varchar(250) NOT NULL,
  `ProductImage` varchar(250) DEFAULT NULL,
  `Color` varchar(100) DEFAULT NULL,
  `Size` varchar(100) DEFAULT NULL,
  `Quantity` int(11) NOT NULL,
  `Cost` decimal(10,2) NOT NULL,
  `ProductStatusId` tinyint(4) NOT NULL,
  `ShopId` int(11) NOT NULL DEFAULT '0',
  `Comment` varchar(250) DEFAULT NULL,
  `Feedback` varchar(250) DEFAULT NULL,
  `QuantityOldNew` int(11) DEFAULT NULL,
  `CostOldNew` decimal(10,2) DEFAULT NULL,
  `RemoveReason` varchar(250) DEFAULT NULL,
  `RemoveConfirm` varchar(250) DEFAULT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ProductId`, `OrderId`, `ProductLink`, `ProductImage`, `Color`, `Size`, `Quantity`, `Cost`, `ProductStatusId`, `ShopId`, `Comment`, `Feedback`, `QuantityOldNew`, `CostOldNew`, `RemoveReason`, `RemoveConfirm`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 1, 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'Xanh', 'S', 1, '1.00', 0, 0, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-10 21:42:14', 5, '2016-10-25 12:43:34'),
(2, 1, 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'http://sanphamsong.com/sites/default/files/img_0204.jpg', 'Đen', 'M', 2, '1.00', 0, 0, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-10 21:42:14', 5, '2016-10-25 12:43:34'),
(3, 1, 'http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg', 'http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg', 'Đỏ', 'XL', 1, '1.00', 0, 0, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-10 21:42:14', 5, '2016-10-25 12:43:34'),
(4, 1, 'http://img.yeah1.com/upload/news/02032015/1425271738_img20141009164458394-6620-1414121918_(1).jpg', 'http://img.yeah1.com/upload/news/02032015/1425271738_img20141009164458394-6620-1414121918_(1).jpg', 'Trắng', 'XXL', 1, '1.00', 0, 0, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-10 21:42:14', 5, '2016-10-25 12:43:34'),
(5, 2, 'quần bò', '', 'Đen', '27', 1, '1.00', 0, 1, 'The Tooltip plugin is small pop-up box that appears when the user moves the mouse pointer over an element', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-11 08:11:59', 5, '2016-10-29 12:28:48'),
(6, 2, 'quần bò', '', 'Xanh', '26', 1, '10.00', 0, 1, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-11 08:11:59', 5, '2016-10-29 12:28:48'),
(7, 2, 'quần bò', '', 'Trắng', '25', 1, '1.00', 0, 1, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-11 08:11:59', 5, '2016-10-29 12:28:48'),
(8, 2, 'Áo thun', '', 'Xanh', 'S', 1, '1.00', 0, 1, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-11 08:11:59', 5, '2016-10-29 12:28:48'),
(9, 2, 'Giày cao gót', '', 'Sữa', '37', 1, '1.00', 0, 0, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-11 08:11:59', 5, '2016-10-29 12:28:48'),
(10, 2, 'Giày cao gót', '', 'Đen', '35', 1, '1.00', 0, 0, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-11 08:11:59', 5, '2016-10-29 12:28:48'),
(11, 2, 'Áo phao', '', 'Đen', 'XXL', 1, '1.00', 0, 0, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-11 08:11:59', 5, '2016-10-29 12:28:48'),
(12, 2, 'Áo phao', '', 'Sữa', 'XXL', 1, '1.00', 0, 0, '', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-11 08:11:59', 5, '2016-10-29 12:28:48'),
(13, 3, '1', '', '1', '1', 1, '1.00', 0, 0, 'aaa', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-17 16:59:16', 1, '2017-03-27 13:33:06'),
(14, 3, '2', '', '2', '23', 1, '0.01', 0, 0, 'aaa', NULL, NULL, NULL, NULL, NULL, 5, '2016-10-17 16:59:16', 1, '2017-03-27 13:33:06'),
(15, 3, '1', '', '1', '1', 1, '1.00', 0, 0, 'aaa', '', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:20:46', 1, '2017-03-27 13:33:06'),
(16, 3, '2', '', '2', '23', 1, '0.01', 0, 0, 'aaa', '', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:20:46', 1, '2017-03-27 13:33:06'),
(17, 3, '1', '', '1', '1', 1, '1.00', 0, 0, 'aaa', '', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:21:04', 1, '2017-03-27 13:33:06'),
(18, 3, '2', '', '2', '23', 1, '0.01', 0, 0, 'aaa', '', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:21:04', 1, '2017-03-27 13:33:06'),
(19, 3, '1', '', '1', '1', 1, '1.00', 0, 0, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:21:39', 1, '2017-03-27 13:33:06'),
(20, 3, '2', '', '2', '23', 1, '0.01', 0, 0, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:21:39', 1, '2017-03-27 13:33:06'),
(21, 3, '1', '', '1', '1', 1, '1.00', 0, 0, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:21:53', 1, '2017-03-27 13:33:06'),
(22, 3, '2', '', '2', '23', 1, '0.01', 0, 0, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:21:53', 1, '2017-03-27 13:33:06'),
(23, 3, '1', '', '1', '1', 1, '1.00', 0, 2, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:22:30', 1, '2017-03-27 13:33:06'),
(24, 3, '2', '', '2', '23', 1, '0.01', 0, 3, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:22:30', 1, '2017-03-27 13:33:06'),
(25, 3, '1', '', '1', '1', 1, '1.00', 0, 2, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:25:11', 1, '2017-03-27 13:33:06'),
(26, 3, '2', '', '2', '23', 1, '0.01', 0, 3, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:25:11', 1, '2017-03-27 13:33:06'),
(27, 3, '1', '', '1', '1', 1, '1.00', 0, 2, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:27:44', 1, '2017-03-27 13:33:06'),
(28, 3, '2', '', '2', '23', 1, '0.01', 0, 3, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:27:44', 1, '2017-03-27 13:33:06'),
(29, 3, '2', '', '2', '23', 1, '0.01', 0, 3, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:36:35', 1, '2017-03-27 13:33:06'),
(30, 3, '1', '', '1', '1', 1, '1.00', 0, 2, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:36:35', 1, '2017-03-27 13:33:06'),
(31, 3, '2', '', '2', '23', 1, '0.01', 0, 3, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:37:31', 1, '2017-03-27 13:33:06'),
(32, 3, '1', '', '1', '1', 1, '1.00', 0, 2, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-17 17:37:31', 1, '2017-03-27 13:33:06'),
(33, 3, '2', '', '2', '23', 2, '10.00', 0, 3, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-20 13:52:06', 1, '2017-03-27 13:33:06'),
(34, 3, '1', '', '1', '1', 1, '1.00', 0, 2, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-20 13:52:06', 1, '2017-03-27 13:33:06'),
(35, 3, '2', '', '2', '23', 2, '10.00', 0, 3, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2016-10-20 13:54:35', 1, '2017-03-27 13:33:06'),
(36, 3, '1', '', '1', '1', 1, '1.00', 0, 2, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 1, '2016-10-20 13:54:35', 1, '2017-03-27 13:33:06'),
(37, 2, 'quần bò', '', 'Đen', '27', 1, '1.00', 0, 1, 'The Tooltip plugin is small pop-up box that appears when the user moves the mouse pointer over an element', 'The Tooltip plugin is small pop-up box that appears when the user moves the mouse pointer over an element', NULL, NULL, NULL, NULL, 5, '2016-10-25 10:13:05', 5, '2016-10-29 12:28:48'),
(38, 2, 'quần bò', '', 'Xanh', '26', 1, '10.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 10:13:05', 5, '2016-10-29 12:28:48'),
(39, 2, 'quần bò', '', 'Trắng', '25', 1, '1.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 10:13:05', 5, '2016-10-29 12:28:48'),
(40, 2, 'Áo thun', '', 'Xanh', 'S', 1, '1.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 10:13:05', 5, '2016-10-29 12:28:48'),
(41, 2, 'Áo phao', '', 'Đen', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 10:13:05', 5, '2016-10-29 12:28:48'),
(42, 2, 'Áo phao', '', 'Sữa', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 10:13:05', 5, '2016-10-29 12:28:48'),
(43, 2, 'Giày cao gót', '', 'Sữa', '37', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 10:13:05', 5, '2016-10-29 12:28:48'),
(44, 2, 'Giày cao gót', '', 'Đen', '35', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 10:13:05', 5, '2016-10-29 12:28:48'),
(45, 1, 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'Xanh', 'S', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 12:43:17', 5, '2016-10-25 12:43:34'),
(46, 1, 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'http://sanphamsong.com/sites/default/files/img_0204.jpg', 'Đen', 'M', 2, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 12:43:17', 5, '2016-10-25 12:43:34'),
(47, 1, 'http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg', 'http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg', 'Đỏ', 'XL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 12:43:17', 5, '2016-10-25 12:43:34'),
(48, 1, 'http://img.yeah1.com/upload/news/02032015/1425271738_img20141009164458394-6620-1414121918_(1).jpg', 'http://img.yeah1.com/upload/news/02032015/1425271738_img20141009164458394-6620-1414121918_(1).jpg', 'Trắng', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-25 12:43:17', 5, '2016-10-25 12:43:34'),
(49, 1, 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'Xanh', 'S', 1, '1.00', 2, 1, 'a', 'a', NULL, NULL, NULL, NULL, 5, '2016-10-25 12:43:34', NULL, NULL),
(50, 1, 'http://tienganh247.com.vn/wp-content/uploads/2016/06/tu-vung-tieng-anh-chu-de-quan-ao.png', 'http://sanphamsong.com/sites/default/files/img_0204.jpg', 'Đen', 'M', 2, '1.00', 2, 1, 'b', 'b', NULL, NULL, 'test xóa 2', NULL, 5, '2016-10-25 12:43:34', 1, '2017-02-11 16:09:43'),
(51, 1, 'http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg', 'http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg', 'Đỏ', 'XL', 1, '1.00', 2, 2, 'c', 'c', NULL, NULL, 'xoa', 'Ko ok', 5, '2016-10-25 12:43:34', 1, '2017-02-11 20:09:11'),
(52, 1, 'http://img.yeah1.com/upload/news/02032015/1425271738_img20141009164458394-6620-1414121918_(1).jpg', 'http://img.yeah1.com/upload/news/02032015/1425271738_img20141009164458394-6620-1414121918_(1).jpg', 'Trắng', 'XXL', 1, '1.00', 2, 3, 'd', 'd', NULL, NULL, NULL, NULL, 5, '2016-10-25 12:43:34', NULL, NULL),
(53, 3, '2', '', '2', '23', 2, '10.00', 0, 3, 'aaa', 'bbbbbbb', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:23:05', 1, '2017-03-27 13:33:06'),
(54, 3, '1', '', '1', '1', 1, '1.00', 0, 2, 'aaa', 'qaaa', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:23:05', 1, '2017-03-27 13:33:06'),
(55, 2, 'quần bò', '', 'Đen', '27', 1, '1.00', 0, 1, '', 'The Tooltip plugin is small pop-up box that appears when the user moves the mouse pointer over an element', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:25:49', 5, '2016-10-29 12:28:48'),
(56, 2, 'quần bò', '', 'Xanh', '26', 1, '10.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:25:49', 5, '2016-10-29 12:28:48'),
(57, 2, 'quần bò', '', 'Trắng', '25', 1, '1.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:25:49', 5, '2016-10-29 12:28:48'),
(58, 2, 'Áo thun', '', 'Xanh', 'S', 1, '1.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:25:49', 5, '2016-10-29 12:28:48'),
(59, 2, 'Áo phao', '', 'Đen', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:25:49', 5, '2016-10-29 12:28:48'),
(60, 2, 'Áo phao', '', 'Sữa', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:25:49', 5, '2016-10-29 12:28:48'),
(61, 2, 'Giày cao gót', '', 'Sữa', '37', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:25:49', 5, '2016-10-29 12:28:48'),
(62, 2, 'Giày cao gót', '', 'Đen', '35', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:25:49', 5, '2016-10-29 12:28:48'),
(63, 2, 'quần bò', '', 'Đen', '27', 1, '1.00', 0, 1, 'aqaaaaaaaaaaa', 'The Tooltip plugin is small pop-up box that appears when the user moves the mouse pointer over an element', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:26:19', 5, '2016-10-29 12:28:48'),
(64, 2, 'quần bò', '', 'Xanh', '26', 1, '10.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:26:19', 5, '2016-10-29 12:28:48'),
(65, 2, 'quần bò', '', 'Trắng', '25', 1, '1.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:26:19', 5, '2016-10-29 12:28:48'),
(66, 2, 'Áo thun', '', 'Xanh', 'S', 1, '1.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:26:19', 5, '2016-10-29 12:28:48'),
(67, 2, 'Áo phao', '', 'Đen', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:26:19', 5, '2016-10-29 12:28:48'),
(68, 2, 'Áo phao', '', 'Sữa', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:26:19', 5, '2016-10-29 12:28:48'),
(69, 2, 'Giày cao gót', '', 'Sữa', '37', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:26:19', 5, '2016-10-29 12:28:48'),
(70, 2, 'Giày cao gót', '', 'Đen', '35', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:26:19', 5, '2016-10-29 12:28:48'),
(71, 2, 'quần bò', '', 'Đen', '27', 1, '1.00', 0, 1, 'aqaaaaaaaaaaa', 'The Tooltip plugin is small pop-up box that appears when the user moves the mouse pointer over an element', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:27:10', 5, '2016-10-29 12:28:48'),
(72, 2, 'quần bò', '', 'Xanh', '26', 1, '10.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:27:10', 5, '2016-10-29 12:28:48'),
(73, 2, 'quần bò', '', 'Trắng', '25', 1, '1.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:27:10', 5, '2016-10-29 12:28:48'),
(74, 2, 'Áo thun', '', 'Xanh', 'S', 1, '1.00', 0, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:27:10', 5, '2016-10-29 12:28:48'),
(75, 2, 'Áo phao', '', 'Đen', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:27:10', 5, '2016-10-29 12:28:48'),
(76, 2, 'Áo phao', '', 'Sữa', 'XXL', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:27:10', 5, '2016-10-29 12:28:48'),
(77, 2, 'Giày cao gót', '', 'Sữa', '37', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:27:10', 5, '2016-10-29 12:28:48'),
(78, 2, 'Giày cao gót', '', 'Đen', '35', 1, '1.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:27:10', 5, '2016-10-29 12:28:48'),
(79, 2, 'quần bò', '', 'Đen', '27', 1, '1.00', 2, 1, '', 'The Tooltip plugin is small pop-up box that appears when the user moves the mouse pointer over an element', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:28:48', NULL, NULL),
(80, 2, 'quần bò', '', 'Xanh', '26', 1, '10.00', 2, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:28:48', NULL, NULL),
(81, 2, 'quần bò', '', 'Trắng', '25', 1, '1.00', 2, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:28:48', NULL, NULL),
(82, 2, 'Áo thun', '', 'Xanh', 'S', 1, '1.00', 2, 1, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:28:48', NULL, NULL),
(83, 2, 'Áo phao', '', 'Đen', 'XXL', 1, '1.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:28:48', NULL, NULL),
(84, 2, 'Áo phao', '', 'Sữa', 'XXL', 1, '1.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:28:48', NULL, NULL),
(85, 2, 'Giày cao gót', '', 'Sữa', '37', 1, '1.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:28:48', NULL, NULL),
(86, 2, 'Giày cao gót', '', 'Đen', '35', 1, '1.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 12:28:48', NULL, NULL),
(87, 3, '2', '', '2', '23', 2, '10.00', 0, 3, '', 'bbbbbbb', NULL, NULL, NULL, NULL, 5, '2016-10-29 14:10:50', 1, '2017-03-27 13:33:06'),
(88, 3, '1', '', '1', '1', 1, '1.00', 0, 2, '', 'qaaa', NULL, NULL, NULL, NULL, 5, '2016-10-29 14:10:50', 1, '2017-03-27 13:33:06'),
(89, 3, '2', '', '2', '23', 2, '10.00', 0, 3, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2016-10-29 23:00:04', 1, '2017-03-27 13:33:06'),
(90, 3, '1', '', '1', '1', 1, '1.00', 0, 2, NULL, NULL, NULL, NULL, NULL, NULL, 5, '2016-10-29 23:00:04', 1, '2017-03-27 13:33:06'),
(92, 5, '2', 'ao-so-mi-5885bd4f64274-58e0b3004362d.jpg', '2', '23', 2, '10.00', 2, 10, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 23:02:04', 1, '2017-06-28 13:58:46'),
(93, 5, '1', '', '1', '1', 1, '1.00', 2, 10, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 23:02:04', 1, '2017-06-28 13:58:46'),
(94, 5, '2', '', '2', '23', 2, '10.00', 2, 10, '', '', NULL, NULL, NULL, NULL, 5, '2016-10-29 23:02:04', 1, '2017-06-28 13:58:46'),
(95, 5, '1', '', '1', '1', 0, '1.00', 1, 95, '', '', 1, NULL, 'Sản phẩm hết hàng', NULL, 5, '2016-10-29 23:02:04', 21, '2017-06-28 14:20:11'),
(96, 6, 'Link 1', 'Link 1', 'Đỏ', 'XL', 10, '100.50', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2016-11-12 15:36:41', NULL, NULL),
(97, 6, 'Link 2', 'Link 2', 'Xanh', 'XXL', 5, '12.05', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2016-11-12 15:36:41', NULL, NULL),
(98, 7, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, '', 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(99, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(100, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(101, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(102, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(103, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(104, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(105, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(106, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(107, 7, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(108, 7, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(109, 7, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 13:15:37', NULL, NULL),
(110, 8, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, '', 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(111, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(112, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(113, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(114, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(115, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(116, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(117, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(118, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(119, 8, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(120, 8, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(121, 8, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, '', 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, 1, '2016-11-14 20:45:54', NULL, NULL),
(122, 9, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(123, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(124, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(125, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(126, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(127, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(128, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(129, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(130, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(131, 9, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(132, 9, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(133, 9, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:39:48', NULL, NULL),
(134, 10, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 0, 0, 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(135, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(136, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(137, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(138, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(139, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(140, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(141, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(142, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(143, 10, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(144, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(145, 10, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:45:48', 1, '2017-03-19 16:20:51'),
(146, 11, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 0, 0, 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(147, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(148, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(149, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(150, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(151, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(152, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(153, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(154, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(155, 11, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 5, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, 'Tăng giá sản phẩm', NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(156, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '100.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, 'Giảm giá', NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(157, 11, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, 6, '2016-12-02 15:46:06', 1, '2017-04-01 11:19:09'),
(158, 12, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 0, 0, 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(159, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(160, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(161, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(162, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(163, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(164, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(165, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(166, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(167, 12, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(168, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(169, 12, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', NULL, NULL, NULL, NULL, NULL, 6, '2016-12-02 15:49:13', 1, '2017-01-10 16:13:25'),
(170, 12, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 0, 0, 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(171, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(172, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(173, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(174, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(175, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(176, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(177, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(178, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(179, 12, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(180, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(181, 12, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 09:56:17', 1, '2017-01-10 16:13:25'),
(182, 12, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(183, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(184, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(185, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(186, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(187, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL);
INSERT INTO `products` (`ProductId`, `OrderId`, `ProductLink`, `ProductImage`, `Color`, `Size`, `Quantity`, `Cost`, `ProductStatusId`, `ShopId`, `Comment`, `Feedback`, `QuantityOldNew`, `CostOldNew`, `RemoveReason`, `RemoveConfirm`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(188, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(189, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(190, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(191, 12, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(192, 12, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(193, 12, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-01-10 16:13:25', NULL, NULL),
(194, 13, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(195, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(196, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(197, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(198, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(199, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(200, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(201, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(202, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(203, 13, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(204, 13, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(205, 13, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, '', '<div style="border:1px solid #990000;padding-left:20px;margin:0 0 10px 0;">\n\n<h4>A PHP Error was encountered</h4>\n\n<p>Severity: Notice</p>\n<p>Message:  Undefined index: Feedback</p>\n<p>Filename: order/add.php</p>\n<p>Line Number: 164</p>\n\n\n	<p>Backtra', NULL, NULL, NULL, NULL, 1, '2017-02-16 12:27:34', NULL, NULL),
(206, 14, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(207, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(208, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(209, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(210, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(211, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(212, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(213, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(214, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(215, 14, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(216, 14, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(217, 14, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:51:43', NULL, NULL),
(218, 15, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(219, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(220, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(221, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(222, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(223, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(224, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(225, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(226, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(227, 15, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(228, 15, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(229, 15, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:53:26', NULL, NULL),
(230, 16, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(231, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(232, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(233, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(234, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(235, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(236, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(237, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(238, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(239, 16, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(240, 16, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(241, 16, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 15:54:21', NULL, NULL),
(242, 10, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(243, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(244, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(245, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(246, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(247, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(248, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(249, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(250, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(251, 10, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(252, 10, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '128.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(253, 10, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221', 'https://gd1.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old đỏ trắng', '40', 1, '168.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-19 16:20:51', NULL, NULL),
(254, 3, '2', '', '2', '23', 2, '10.00', 2, 3, '', 'bbbbbbb', NULL, NULL, NULL, NULL, 1, '2017-03-27 13:33:06', NULL, NULL),
(255, 3, '2', '', '2', '23', 2, '10.00', 2, 3, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-27 13:33:06', NULL, NULL),
(256, 3, '1', '', '1', '1', 1, '1.00', 2, 2, '', 'qaaa', NULL, NULL, NULL, NULL, 1, '2017-03-27 13:33:06', NULL, NULL),
(257, 3, '1', '', '1', '1', 1, '1.00', 2, 2, '', '', NULL, NULL, NULL, NULL, 1, '2017-03-27 13:33:06', NULL, NULL),
(258, 11, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 0, 0, 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(259, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(260, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(261, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(262, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(263, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(264, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(265, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(266, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(267, 11, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 5, '128.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(268, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '100.00', 0, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:18:52', 1, '2017-04-01 11:19:09'),
(269, 11, 'https://item.taobao.com/item.htm?spm=a230r.1.14.62.GWUMSK&id=541270599655&ns=1&abbucket=11#detail', 'https://gd2.alicdn.com/imgextra/i1/3019132405/TB2H.i6bIaK.eBjSspjXXXL.XXa_!!3019132405.jpg_400x400.jpg', 'stan smith gót xanh lá', '40.5', 1, '288.00', 2, 0, 'tem made in china, phông chữ đậm lùn như ảnh. Không giống như ảnh trên web trả hàng', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(270, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.61.SkqVHf&id=536128597139', 'https://gd1.alicdn.com/imgextra/i2/0/TB1mV4FJFXXXXX2XFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all white', '42', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(271, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '42', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(272, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '37', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(273, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '41', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(274, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.52.SkqVHf&id=536092410234', 'https://gd4.alicdn.com/imgextra/i4/0/TB1y6NLJFXXXXa8XpXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'slip on all black', '40', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(275, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.115.5G9BbR&id=536021859221', 'https://gd2.alicdn.com/imgextra/i4/0/TB1clo2JVXXXXXiXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'old trắng đỏ', '40', 1, '168.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(276, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.80.1WBWma&id=536021543727', 'https://gd4.alicdn.com/imgextra/i4/0/TB17tdDJFXXXXaYXFXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans era đen trắng', '41', 1, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(277, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.70.FdGAda&id=541264175506', 'https://gd2.alicdn.com/imgextra/i2/2904836445/TB2Bi4dck1M.eBjSZPiXXawfpXa_!!2904836445.png_400x400.jpg_.webp', 'old xanh min', '36', 1, '168.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(278, 11, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619', 'https://gd4.alicdn.com/imgextra/i3/0/TB14slpKpXXXXaYXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'checker', '43', 5, '128.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(279, 11, 'https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374', 'https://gd3.alicdn.com/imgextra/i3/0/TB1qNlUJFXXXXaKXXXXXXXXXXXX_!!0-item_pic.jpg_400x400.jpg', 'vans authentic đỏ', '40.5', 1, '100.00', 2, 0, 'yêu cầu đóng thêm thùng để bảo về hộp giày', '', NULL, NULL, NULL, NULL, 1, '2017-04-01 11:19:09', NULL, NULL),
(280, 17, 'https://world.tmall.com/item/529692780933.htm?spm=a21bp.7806943.topsale_XX.2.VR1NfG&skuId=3155674653010', 'http://img.alicdn.com/bao/uploaded/i4/2085955364/TB2KraGmVXXXXc1XXXXXXXXXXXX_!!2085955364.jpg_150x150q90.jpg', '黑色', '35', 3, '289.00', 0, 0, 'abv', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 15:58:08', 1, '2017-04-02 17:09:01'),
(281, 17, 'https://world.taobao.com/item/542027378770.htm?spm=a312a.7728556.2014080708.6.TCDEuK', 'https://gd3.alicdn.com/bao/uploaded/i3/2246360148/TB2wITjd8yN.eBjSZFkXXb8YFXa_!!2246360148.jpg_150x150.jpg', '灰黑单裤6101-2', '28', 8, '75.00', 0, 0, 'bdsbsh', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 15:58:08', 1, '2017-04-02 17:09:01'),
(282, 17, 'https://world.taobao.com/item/542027378770.htm?spm=a312a.7728556.2014080708.6.TCDEuK', 'https://gd3.alicdn.com/bao/uploaded/i3/2246360148/TB2kHnndZCO.eBjSZFzXXaRiVXa_!!2246360148.jpg_150x150.jpg', '中蓝单裤6101-1', '27', 3, '75.00', 0, 0, 'bfnfn', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 15:58:08', 1, '2017-04-02 17:09:01'),
(283, 17, 'https://world.taobao.com/item/542027378770.htm?spm=a312a.7728556.2014080708.6.TCDEuK', 'https://gd3.alicdn.com/bao/uploaded/i3/2246360148/TB2wITjd8yN.eBjSZFkXXb8YFXa_!!2246360148.jpg_150x150.jpg', '灰黑单裤6101-2', '28', 8, '75.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 16:54:59', 1, '2017-04-02 17:09:01'),
(284, 17, 'https://world.taobao.com/item/542027378770.htm?spm=a312a.7728556.2014080708.6.TCDEuK', 'https://gd3.alicdn.com/bao/uploaded/i3/2246360148/TB2kHnndZCO.eBjSZFzXXaRiVXa_!!2246360148.jpg_150x150.jpg', '中蓝单裤6101-1', '27', 4, '75.00', 0, 0, 'mua them', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 16:54:59', 1, '2017-04-02 17:09:01'),
(285, 17, 'https://world.tmall.com/item/529692780933.htm?spm=a21bp.7806943.topsale_XX.2.VR1NfG&skuId=3155674653010', 'http://img.alicdn.com/bao/uploaded/i4/2085955364/TB2KraGmVXXXXc1XXXXXXXXXXXX_!!2085955364.jpg_150x150q90.jpg', '黑色', '35', 3, '289.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 16:54:59', 1, '2017-04-02 17:09:01'),
(286, 17, 'https://world.taobao.com/item/542027378770.htm?spm=a312a.7728556.2014080708.6.TCDEuK', 'https://gd3.alicdn.com/bao/uploaded/i3/2246360148/TB2wITjd8yN.eBjSZFkXXb8YFXa_!!2246360148.jpg_150x150.jpg', '灰黑单裤6101-2', '28', 8, '75.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 16:56:06', 1, '2017-04-02 17:09:01'),
(287, 17, 'https://world.taobao.com/item/542027378770.htm?spm=a312a.7728556.2014080708.6.TCDEuK', 'https://gd3.alicdn.com/bao/uploaded/i3/2246360148/TB2kHnndZCO.eBjSZFzXXaRiVXa_!!2246360148.jpg_150x150.jpg', '中蓝单裤6101-1', '27', 4, '75.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 16:56:06', 1, '2017-04-02 17:09:01'),
(288, 17, 'https://world.tmall.com/item/529692780933.htm?spm=a21bp.7806943.topsale_XX.2.VR1NfG&skuId=3155674653010', 'http://img.alicdn.com/bao/uploaded/i4/2085955364/TB2KraGmVXXXXc1XXXXXXXXXXXX_!!2085955364.jpg_150x150q90.jpg', '黑色', '35', 4, '289.00', 0, 0, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-02 16:56:06', 1, '2017-04-02 17:09:01'),
(289, 17, 'https://world.taobao.com/item/542027378770.htm?spm=a312a.7728556.2014080708.6.TCDEuK', 'https://gd3.alicdn.com/bao/uploaded/i3/2246360148/TB2wITjd8yN.eBjSZFkXXb8YFXa_!!2246360148.jpg_150x150.jpg', '灰黑单裤6101-2', '28', 8, '75.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-04-02 17:09:01', NULL, NULL),
(290, 17, 'https://world.taobao.com/item/542027378770.htm?spm=a312a.7728556.2014080708.6.TCDEuK', 'https://gd3.alicdn.com/bao/uploaded/i3/2246360148/TB2kHnndZCO.eBjSZFzXXaRiVXa_!!2246360148.jpg_150x150.jpg', '中蓝单裤6101-1', '27', 4, '75.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-04-02 17:09:01', NULL, NULL),
(291, 17, 'https://world.tmall.com/item/529692780933.htm?spm=a21bp.7806943.topsale_XX.2.VR1NfG&skuId=3155674653010', 'http://img.alicdn.com/bao/uploaded/i4/2085955364/TB2KraGmVXXXXc1XXXXXXXXXXXX_!!2085955364.jpg_150x150q90.jpg', '黑色', '35', 4, '289.00', 2, 0, '', '', NULL, NULL, NULL, NULL, 1, '2017-04-02 17:09:01', NULL, NULL),
(292, 18, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.150x150.jpg', '粉色', '', 1, '8.50', 2, 29, 'ghi chú 1', '', NULL, NULL, NULL, NULL, 6, '2017-04-08 09:18:10', 1, '2017-05-26 10:46:14'),
(293, 18, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.150x150.jpg', '紫色', '', 1, '8.50', 2, 29, 'ghi chú 2', '', NULL, NULL, NULL, NULL, 6, '2017-04-08 09:18:10', 1, '2017-05-26 10:46:14'),
(294, 18, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.150x150.jpg', '深蓝色', '', 1, '8.50', 2, 29, 'ghi chú 3', '', NULL, NULL, NULL, NULL, 6, '2017-04-08 09:18:10', 1, '2017-05-26 10:46:14'),
(295, 18, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.150x150.jpg', '绿色', '', 1, '8.50', 2, 29, 'ghi chú 4', '', NULL, NULL, NULL, NULL, 6, '2017-04-08 09:18:10', 1, '2017-05-26 10:46:14'),
(296, 19, 'https://world.taobao.com/item/527425930379.htm?spm=a21bp.7915460.179246.11.vVgH7L', 'https://gd3.alicdn.com/bao/uploaded/i3/2708499720/TB2fkYkkFXXXXadXXXXXXXXXXXX_!!2708499720.jpg_150x150.jpg', '腰靠', '', 3, '58.00', 2, 0, 'ghi chus', '', NULL, NULL, NULL, NULL, 6, '2017-04-08 16:52:14', NULL, NULL),
(297, 19, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.150x150.jpg', '粉色', '', 1, '8.50', 2, 0, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-08 16:52:14', NULL, NULL),
(298, 19, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.150x150.jpg', '紫色', '', 1, '8.50', 2, 0, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-08 16:52:14', NULL, NULL),
(299, 19, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.150x150.jpg', '深蓝色', '', 2, '8.50', 2, 0, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-08 16:52:14', NULL, NULL),
(315, 22, 'b', '', 'c', 'd', 10, '20.00', 5, 2, '', '', NULL, '10.00', 'Tăng giá', 'đồng ý tăng giá', 6, '2017-04-17 21:23:35', 6, '2017-04-28 16:19:55'),
(316, 22, 'x', '', 'v', 'g', 10, '5.00', 2, 2, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-17 21:23:35', 6, '2017-04-28 16:19:55'),
(317, 22, 'a', '', 'b', 'c', 10, '1.00', 2, 1, '', '', NULL, NULL, 'aaaaaaaa', 'bbbbbbb', 6, '2017-04-17 21:23:35', 6, '2017-04-28 16:19:55'),
(318, 22, 'a', '', 'c', 'd', 10, '11.00', 2, 1, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-17 21:23:35', 6, '2017-04-28 16:19:55'),
(319, 22, 'bvv', '', 'sd', 'f', 45, '200.00', 2, 1, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-17 21:23:35', 6, '2017-04-28 16:19:55'),
(320, 22, 'x', '', 'á', 'd', 10, '10.00', 2, 2, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-17 21:23:35', 6, '2017-04-28 16:19:55'),
(321, 22, 'bvv', '', 'cd', 'f', 10, '5.00', 1, 1, '', '', NULL, NULL, 'Sản phẩm hết hàng', NULL, 0, '0000-00-00 00:00:00', 6, '2017-04-28 16:19:55'),
(322, 25, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.jpg', '紫色', '', 9, '8.50', 2, 3, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-29 08:55:01', 2, '2017-05-04 13:28:37'),
(323, 25, 'https://item.taobao.com/item.htm?id=547649894351&toSite=main#detail', 'https://gd2.alicdn.com/imgextra/i2/2892063454/TB2CXhQjR0lpuFjSszdXXcdxFXa_!!2892063454.jpg_150x150.jpg', '钢制脚', '滑轮款 黑框橙色', 30, '93.00', 2, 4, 'aa', '', NULL, NULL, NULL, NULL, 6, '2017-04-29 08:55:01', 2, '2017-05-04 13:28:37'),
(324, 25, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.jpg', '紫色', '', 9, '8.50', 2, 3, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-29 08:55:29', 2, '2017-05-04 13:28:37'),
(325, 25, 'https://item.taobao.com/item.htm?id=547649894351&toSite=main#detail', 'https://gd2.alicdn.com/imgextra/i2/2892063454/TB2CXhQjR0lpuFjSszdXXcdxFXa_!!2892063454.jpg_150x150.jpg', '钢制脚', '滑轮款 黑框橙色', 3, '93.00', 2, 4, 'aa', 'c', NULL, NULL, NULL, NULL, 6, '2017-04-29 08:55:29', 2, '2017-05-04 13:28:37'),
(326, 25, 'https://detail.1688.com/offer/543041215704.html?position=top&nhpid=347669&productid=543041215704', 'https://cbu01.alicdn.com/img/ibank/2017/746/784/3878487647_487771459.jpg', '紫色', '', 9, '8.50', 2, 3, '', '', NULL, NULL, NULL, NULL, 6, '2017-04-29 08:57:19', 2, '2017-05-04 13:28:37'),
(327, 25, 'https://item.taobao.com/item.htm?id=547649894351&toSite=main#detail', 'https://gd2.alicdn.com/imgextra/i2/2892063454/TB2CXhQjR0lpuFjSszdXXcdxFXa_!!2892063454.jpg_150x150.jpg', '钢制脚', '滑轮款 黑框橙色', 3, '93.00', 2, 4, '', 'v', NULL, NULL, NULL, NULL, 6, '2017-04-29 08:57:19', 2, '2017-05-04 13:28:37'),
(328, 28, 'https://world.tmall.com/item/529692780933.htm?spm=a21bp.7806943.topsale_XX.2.VR1NfG&skuId=3163877946961', 'https://img.alicdn.com/bao/uploaded/i2/2085955364/TB2AF9CpVXXXXXsXXXXXXXXXXXX_!!2085955364.jpg', '黑色', '34', 1, '289.00', 2, 9, 'aaaaaa', '', NULL, NULL, NULL, NULL, 6, '2017-05-01 10:03:09', 2, '2017-07-31 10:03:05'),
(329, 29, 'https://world.tmall.com/item/529692780933.htm?spm=a21bp.7806943.topsale_XX.2.VR1NfG&skuId=3163877946960', 'http://img.alicdn.com/bao/uploaded/i4/2085955364/TB25KqkmVXXXXcyXpXXXXXXXXXX_!!2085955364.jpg', '白色', '34', 1, '289.00', 2, 11, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-07 08:49:46', 1, '2017-05-09 15:12:23'),
(331, 29, 'https://world.tmall.com/item/39448266376.htm?spm=a312a.7700718.0.da321h.9Mn8xf&id=39448266376&skuId=62654823146', 'http://img.alicdn.com/bao/uploaded/i3/2094190888/TB23AlDdmiK.eBjSZFyXXaS4pXa_!!2094190888.jpg', '官方标配', '红色', 1, '49.90', 2, 12, '', 'bndfbndbncb', NULL, NULL, NULL, NULL, 1, '2017-05-19 16:49:49', NULL, NULL),
(332, 29, 'https://world.tmall.com/item/522066817896.htm?spm=a312a.7700718.1998025129.9.iwzQwo&abtest=_AB-LR32-PR32&pvid=1a5702ee-351d-4260-bfe2-5eaaa25f830a&pos=5&abbucket=_AB-M32_B17&acm=03054.1003.1.1539344&id=522066817896&scm=1007.12144.81309.23864_0&skuId=', 'https:img.alicdn.com/imgextra/i1/2094190888/TB27LGQcCOI.eBjSspmXXatOVXa_!!2094190888.jpg', '官方标配', '深灰色', 1, '48.90', 2, 12, 'abc', '', NULL, NULL, NULL, NULL, 6, '2017-05-19 17:23:47', NULL, NULL),
(333, 30, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.81.IVjEF6&id=550552245247', 'https://gd3.alicdn.com/imgextra/i3/TB1clJ9RXXXXXb_XVXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '55.00', 2, 13, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(334, 30, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.69.aVzT7n&id=551232371934', 'https://gd2.alicdn.com/imgextra/i3/TB1AXQfRXXXXXabaXXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '90.00', 2, 13, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(335, 30, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.120.B1wmiJ&id=547643024907', 'https://gd2.alicdn.com/imgextra/i2/TB1p1yxQpXXXXbhXXXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '60.00', 2, 13, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(336, 30, 'https://item.taobao.com/item.htm?spm=a230r.1.999.1.jTPGnk&id=549625529635&ns=1#detail', 'https://gd3.alicdn.com/imgextra/i2/0/TB1uafAQpXXXXcKXVXXXXXXXXXX_!!0-item_pic.jpg', 'nhu link', 'L. XL', 2, '39.90', 2, 14, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(337, 30, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-16330272333.85.c2BTH2&id=550975143766', 'https://gd1.alicdn.com/imgextra/i3/1133972013/TB2DBcfsElnpuFjSZFjXXXTaVXa_!!1133972013.jpg', 'nhu link', 'M', 1, '158.00', 2, 15, '', 'giuc shop cho chi', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(338, 30, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd2.alicdn.com/imgextra/i2/1052419246/TB2089DmiRnpuFjSZFCXXX2DXXa_!!1052419246.jpg', 'cam', '', 1, '27.80', 2, 16, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(339, 30, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd1.alicdn.com/imgextra/i1/1052419246/TB2jEqhiwJkpuFjSszcXXXfsFXa_!!1052419246.jpg', 'xanh', '', 2, '27.80', 2, 16, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(340, 30, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd4.alicdn.com/imgextra/i3/1052419246/TB2OkeOirtlpuFjSspoXXbcDpXa_!!1052419246.jpg_400x400.jpg', 'đen', '', 3, '27.80', 2, 16, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(341, 30, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd3.alicdn.com/imgextra/i2/1052419246/TB2jCCuiwNlpuFjy0FfXXX3CpXa_!!1052419246.jpg_400x400.jpg', 'trắng', '', 1, '27.80', 2, 16, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(342, 30, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.67.UfbOEd&id=549319618501', 'https://gd1.alicdn.com/imgextra/i1/196173223/TB269CBphtmpuFjSZFqXXbHFpXa_!!196173223.jpg', 'nhu link', 'L', 1, '159.00', 2, 17, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(343, 30, 'https://item.taobao.com/item.htm?spm=2013.1.20141001.1.hCbBH2&id=534241084889&scm=1007.10115.36023.100200300000000&pvid=b8a9bcc7-7c6e-4d84-a54f-4ffcaef06a52&idnum=0', 'https://gd1.alicdn.com/imgextra/i2/47883575/TB2qiN9qVXXXXbuXpXXXXXXXXXX_!!47883575.jpg', 'xanh', '38', 1, '32.90', 2, 18, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(344, 30, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.93.rxgNUK&id=549734000014', 'https://gd4.alicdn.com/imgextra/i4/196173223/TB23o2UoB8lpuFjSspaXXXJKpXa_!!196173223.jpg', 'nâu nhạt như link', 'S, M', 2, '69.00', 2, 19, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(345, 30, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.93.rxgNUK&id=549734000014', 'https://gd4.alicdn.com/imgextra/i4/196173223/TB2I0rikhRDOuFjSZFzXXcIipXa_!!196173223.jpg', 'nâu đậm như link', 'M', 1, '69.00', 2, 19, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(346, 30, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.2.7jzsSk&id=21810287147&scm=1007.12144.81309.42296_0&pvid=11ef7063-a2ae-429d-8d32-c74b95516198', 'https://gd2.alicdn.com/imgextra/i3/182560466/TB2C3w3rVXXXXbTXXXXXXXXXXXX_!!182560466.jpg_400x400.jpg', 'nhu link', '', 1, '55.00', 2, 20, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(347, 30, 'https://item.taobao.com/item.htm?spm=a230r.1.999.24.XJsBEB&id=550861198344&ns=1#detail', 'https://gd4.alicdn.com/imgextra/i3/2135453531/TB2rM0JsrJmpuFjSZFwXXaE4VXa_!!2135453531.jpg', 'nhu link', '', 1, '45.00', 2, 21, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(348, 30, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.1.Zj30dR&id=547122540440&scm=1007.12144.81309.42296_0&pvid=e9dedb3b-f99d-4895-9b99-a44db246bd40', 'https://gd2.alicdn.com/imgextra/i2/825236775/TB2aAatkrJmpuFjSZFwXXaE4VXa_!!825236775.jpg', 'nhu link', '', 1, '69.00', 2, 22, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(349, 30, 'https://item.taobao.com/item.htm?spm=2013.1.20141003.6.kLBO0j&scm=1007.10011.70203.100200300000001&id=548785739054&pvid=996dfa79-fbe3-4109-8afb-e735983b579e', 'https://gd4.alicdn.com/imgextra/i4/1874382357/TB2sXtaom0mpuFjSZPiXXbssVXa_!!1874382357.jpg', 'đen', '40', 1, '78.00', 2, 23, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL);
INSERT INTO `products` (`ProductId`, `OrderId`, `ProductLink`, `ProductImage`, `Color`, `Size`, `Quantity`, `Cost`, `ProductStatusId`, `ShopId`, `Comment`, `Feedback`, `QuantityOldNew`, `CostOldNew`, `RemoveReason`, `RemoveConfirm`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(350, 30, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.3.W960DT&id=550479040142&scm=1007.12144.81309.42296_0&pvid=7dd34783-4e79-4d05-88a5-3839d698dd70', 'https://img.alicdn.com/imgextra/i3/1990942755/TB2AlZBrSVmpuFjSZFFXXcZApXa_!!1990942755.png', 'hông', 'S', 1, '38.61', 2, 24, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(351, 30, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.3.W960DT&id=550479040142&scm=1007.12144.81309.42296_0&pvid=7dd34783-4e79-4d05-88a5-3839d698dd70', 'https://img.alicdn.com/imgextra/i2/1990942755/TB2XFRnrYBmpuFjSZFAXXaQ0pXa_!!1990942755.png', 'nhu link', 'S', 1, '45.54', 2, 25, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(352, 30, 'https://detail.1688.com/offer/539633685822.html?spm=a261y.7663282.1998411376.2.J09kT3', '', 'nhu ảnh', 'L', 1, '48.00', 2, 26, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(353, 30, 'https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w13675275-16254649721.38.ENtt2m&id=534106481988', '', 'đen', 'M', 2, '25.00', 2, 27, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(354, 30, 'https://item.taobao.com/item.htm?spm=2013.1.20141002.10.0rv1E6&scm=1007.10009.70205.100200300000001&id=548142068176&pvid=83374539-9a78-4f7b-986f-4d0db958437c', 'https://gd4.alicdn.com/imgextra/i3/0/TB1s3wfQpXXXXb3XVXXXXXXXXXX_!!0-item_pic.jpg', 'đen', '', 1, '29.80', 2, 28, '', '', NULL, NULL, NULL, NULL, 1, '2017-05-23 20:00:37', NULL, NULL),
(355, 31, 'https://world.tmall.com/item/522066817896.htm?spm=a312a.7700718.1998025129.9.iwzQwo&abtest=_AB-LR32-PR32&pvid=1a5702ee-351d-4260-bfe2-5eaaa25f830a&pos=5&abbucket=_AB-M32_B17&acm=03054.1003.1.1539344&id=522066817896&scm=1007.12144.81309.23864_0&skuId=', 'https:img.alicdn.com/imgextra/i1/2094190888/TB27LGQcCOI.eBjSspmXXatOVXa_!!2094190888.jpg', '官方标配', '深灰色', 1, '48.90', 2, 30, 'bbbbbbbbbbb', '', NULL, NULL, NULL, NULL, 6, '2017-05-27 08:16:49', 1, '2017-06-01 22:36:11'),
(356, 32, 'https://world.tmall.com/item/522066817896.htm?spm=a312a.7700718.1998025129.9.iwzQwo&abtest=_AB-LR32-PR32&pvid=1a5702ee-351d-4260-bfe2-5eaaa25f830a&pos=5&abbucket=_AB-M32_B17&acm=03054.1003.1.1539344&id=522066817896&scm=1007.12144.81309.23864_0&skuId=', 'https:img.alicdn.com/imgextra/i1/2094190888/TB27LGQcCOI.eBjSspmXXatOVXa_!!2094190888.jpg', '官方标配', '深灰色', 1, '48.90', 2, 31, 'bbbbbbbbbbb', '', NULL, NULL, NULL, NULL, 6, '2017-05-27 08:17:29', 1, '2017-06-01 12:13:34'),
(357, 33, 'https://world.tmall.com/item/522066817896.htm?spm=a312a.7700718.1998025129.9.iwzQwo&abtest=_AB-LR32-PR32&pvid=1a5702ee-351d-4260-bfe2-5eaaa25f830a&pos=5&abbucket=_AB-M32_B17&acm=03054.1003.1.1539344&id=522066817896&scm=1007.12144.81309.23864_0&skuId=', 'https:img.alicdn.com/imgextra/i1/2094190888/TB27LGQcCOI.eBjSspmXXatOVXa_!!2094190888.jpg', '官方标配', '深灰色', 1, '48.90', 2, 32, 'bbbbbbbbbbb', '', 1, NULL, 'Dạ anh/ chị ơi, sản phẩm này shop báo hết hàng', NULL, 6, '2017-05-27 08:21:18', 1, '2017-06-08 16:38:05'),
(358, 34, 'https://world.taobao.com/item/527425930379.htm?spm=a21bp.7915460.179246.11.vVgH7L', 'https:gd3.alicdn.com/bao/uploaded/i3/2708499720/TB2fkYkkFXXXXadXXXXXXXXXXXX_!!2708499720.jpg', '腰靠', '', 3, '58.00', 2, 33, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-01 09:21:48', NULL, NULL),
(359, 34, 'https://world.taobao.com/item/545792874379.htm?spm=a312a.7728556.2014080708.6.vu5Z8C#', 'http://gd4.alicdn.com/bao/uploaded/i4/TB1A.Y0KFXXXXXUXVXXXXXXXXXX_!!0-item_pic.jpg', '', '', 10, '458.00', 2, 34, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-01 09:21:48', NULL, NULL),
(360, 35, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.81.IVjEF6&id=550552245247', 'https://gd3.alicdn.com/imgextra/i3/TB1clJ9RXXXXXb_XVXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '55.00', 2, 35, 'ĐIỀU GÌ ĐỢI BẠN Ở CUỐI CHẶNG ĐUA THÈM LÀ NHÍCH?ĐIỀU GÌ ĐỢI BẠN Ở CUỐI CHẶNG ĐUA THÈM LÀ NHÍCH?ĐIỀU GÌ ĐỢI BẠN Ở CUỐI CHẶNG ĐUA THÈM LÀ NHÍCH?ĐIỀU GÌ ĐỢI BẠN Ở CUỐI CHẶNG ĐUA THÈM LÀ NHÍCH? ĐIỀU GÌ ĐỢI BẠN Ở CUỐI CHẶNG ĐUA THÈM LÀ NHÍCH? ĐIỀU GÌ ĐỢI B', 'abc1', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(361, 35, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.69.aVzT7n&id=551232371934', 'https://gd2.alicdn.com/imgextra/i3/TB1AXQfRXXXXXabaXXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '90.00', 2, 35, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(362, 35, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.120.B1wmiJ&id=547643024907', 'https://gd2.alicdn.com/imgextra/i2/TB1p1yxQpXXXXbhXXXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '60.00', 2, 35, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(363, 35, 'https://item.taobao.com/item.htm?spm=a230r.1.999.1.jTPGnk&id=549625529635&ns=1#detail', 'https://gd3.alicdn.com/imgextra/i2/0/TB1uafAQpXXXXcKXVXXXXXXXXXX_!!0-item_pic.jpg', 'nhu link', 'L. XL', 2, '39.90', 2, 36, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(364, 35, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-16330272333.85.c2BTH2&id=550975143766', 'https://gd1.alicdn.com/imgextra/i3/1133972013/TB2DBcfsElnpuFjSZFjXXXTaVXa_!!1133972013.jpg', 'nhu link', 'M', 1, '158.00', 2, 37, 'giuc shop cho chi', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(365, 35, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd2.alicdn.com/imgextra/i2/1052419246/TB2089DmiRnpuFjSZFCXXX2DXXa_!!1052419246.jpg', 'cam', '', 1, '27.80', 2, 38, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(366, 35, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd1.alicdn.com/imgextra/i1/1052419246/TB2jEqhiwJkpuFjSszcXXXfsFXa_!!1052419246.jpg', 'xanh', '', 2, '27.80', 2, 38, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(367, 35, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd4.alicdn.com/imgextra/i3/1052419246/TB2OkeOirtlpuFjSspoXXbcDpXa_!!1052419246.jpg_400x400.jpg', 'đen', '', 3, '27.80', 2, 38, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(368, 35, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd3.alicdn.com/imgextra/i2/1052419246/TB2jCCuiwNlpuFjy0FfXXX3CpXa_!!1052419246.jpg_400x400.jpg', 'trắng', '', 1, '27.80', 2, 38, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(369, 35, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.67.UfbOEd&id=549319618501', 'https://gd1.alicdn.com/imgextra/i1/196173223/TB269CBphtmpuFjSZFqXXbHFpXa_!!196173223.jpg', 'nhu link', 'L', 1, '159.00', 2, 39, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(370, 35, 'https://item.taobao.com/item.htm?spm=2013.1.20141001.1.hCbBH2&id=534241084889&scm=1007.10115.36023.100200300000000&pvid=b8a9bcc7-7c6e-4d84-a54f-4ffcaef06a52&idnum=0', 'https://gd1.alicdn.com/imgextra/i2/47883575/TB2qiN9qVXXXXbuXpXXXXXXXXXX_!!47883575.jpg', 'xanh', '38', 1, '32.90', 2, 40, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(371, 35, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.93.rxgNUK&id=549734000014', 'https://gd4.alicdn.com/imgextra/i4/196173223/TB23o2UoB8lpuFjSspaXXXJKpXa_!!196173223.jpg', 'nâu nhạt như link', 'S, M', 0, '69.00', 1, 41, '', '', 2, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 6, '2017-06-11 20:18:42'),
(372, 35, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.93.rxgNUK&id=549734000014', 'https://gd4.alicdn.com/imgextra/i4/196173223/TB2I0rikhRDOuFjSZFzXXcIipXa_!!196173223.jpg', 'nâu đậm như link', 'M', 0, '69.00', 1, 41, '', '', 1, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 6, '2017-06-11 20:18:42'),
(373, 35, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.2.7jzsSk&id=21810287147&scm=1007.12144.81309.42296_0&pvid=11ef7063-a2ae-429d-8d32-c74b95516198', 'https://gd2.alicdn.com/imgextra/i3/182560466/TB2C3w3rVXXXXbTXXXXXXXXXXXX_!!182560466.jpg_400x400.jpg', 'nhu link', '', 1, '55.00', 2, 42, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(374, 35, 'https://item.taobao.com/item.htm?spm=a230r.1.999.24.XJsBEB&id=550861198344&ns=1#detail', 'https://gd4.alicdn.com/imgextra/i3/2135453531/TB2rM0JsrJmpuFjSZFwXXaE4VXa_!!2135453531.jpg', 'nhu link', '', 1, '45.00', 2, 43, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(375, 35, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.1.Zj30dR&id=547122540440&scm=1007.12144.81309.42296_0&pvid=e9dedb3b-f99d-4895-9b99-a44db246bd40', 'https://gd2.alicdn.com/imgextra/i2/825236775/TB2aAatkrJmpuFjSZFwXXaE4VXa_!!825236775.jpg', 'nhu link', '', 1, '69.00', 2, 44, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(376, 35, 'https://item.taobao.com/item.htm?spm=2013.1.20141003.6.kLBO0j&scm=1007.10011.70203.100200300000001&id=548785739054&pvid=996dfa79-fbe3-4109-8afb-e735983b579e', 'https://gd4.alicdn.com/imgextra/i4/1874382357/TB2sXtaom0mpuFjSZPiXXbssVXa_!!1874382357.jpg', 'đen', '40', 1, '78.00', 2, 45, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(377, 35, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.3.W960DT&id=550479040142&scm=1007.12144.81309.42296_0&pvid=7dd34783-4e79-4d05-88a5-3839d698dd70', 'https://img.alicdn.com/imgextra/i3/1990942755/TB2AlZBrSVmpuFjSZFFXXcZApXa_!!1990942755.png', 'hông', 'S', 0, '38.61', 1, 46, '', '', 1, NULL, 'Sản phẩm hết hàng', NULL, 1, '2017-06-02 22:36:53', 21, '2017-06-30 16:24:11'),
(378, 35, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.3.W960DT&id=550479040142&scm=1007.12144.81309.42296_0&pvid=7dd34783-4e79-4d05-88a5-3839d698dd70', 'https://img.alicdn.com/imgextra/i2/1990942755/TB2XFRnrYBmpuFjSZFAXXaQ0pXa_!!1990942755.png', 'nhu link', 'S', 0, '45.54', 1, 47, '', '', 1, NULL, 'Dạ anh/ chị ơi, sản phẩm này shop báo hết hàng', NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-08 17:17:18'),
(379, 35, 'https://detail.1688.com/offer/539633685822.html?spm=a261y.7663282.1998411376.2.J09kT3', '', 'nhu ảnh', 'L', 0, '48.00', 1, 48, '', '', 1, NULL, 'Sản phẩm hết hàng', NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-04 17:12:07'),
(380, 35, 'https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w13675275-16254649721.38.ENtt2m&id=534106481988', '', 'đen', 'M', 2, '30.00', 5, 49, '', '', 2, '25.00', 'Dạ anh/ chị ơi, sản phẩm này shop báo tăng giá, anh/ chị có đặt không ạ?', 'Đồng ý', 1, '2017-06-02 22:36:53', 1, '2017-06-04 14:12:33'),
(381, 35, 'https://item.taobao.com/item.htm?spm=2013.1.20141002.10.0rv1E6&scm=1007.10009.70205.100200300000001&id=548142068176&pvid=83374539-9a78-4f7b-986f-4d0db958437c', 'https://gd4.alicdn.com/imgextra/i3/0/TB1s3wfQpXXXXb3XVXXXXXXXXXX_!!0-item_pic.jpg', 'đen', '', 0, '29.80', 1, 50, '', '', 1, NULL, 'Dạ anh/ chị ơi, sản phẩm này shop báo hết hàng', NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-08 17:10:01'),
(382, 36, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.81.IVjEF6&id=550552245247', 'https://gd3.alicdn.com/imgextra/i3/TB1clJ9RXXXXXb_XVXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '55.00', 2, 51, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(383, 36, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.69.aVzT7n&id=551232371934', 'https://gd2.alicdn.com/imgextra/i3/TB1AXQfRXXXXXabaXXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '90.00', 2, 51, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(384, 36, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.120.B1wmiJ&id=547643024907', 'https://gd2.alicdn.com/imgextra/i2/TB1p1yxQpXXXXbhXXXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '60.00', 2, 51, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(385, 36, 'https://item.taobao.com/item.htm?spm=a230r.1.999.1.jTPGnk&id=549625529635&ns=1#detail', 'https://gd3.alicdn.com/imgextra/i2/0/TB1uafAQpXXXXcKXVXXXXXXXXXX_!!0-item_pic.jpg', 'nhu link', 'L. XL', 2, '39.90', 2, 52, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(386, 36, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-16330272333.85.c2BTH2&id=550975143766', 'https://gd1.alicdn.com/imgextra/i3/1133972013/TB2DBcfsElnpuFjSZFjXXXTaVXa_!!1133972013.jpg', 'nhu link', 'M', 1, '158.00', 2, 53, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(387, 36, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd2.alicdn.com/imgextra/i2/1052419246/TB2089DmiRnpuFjSZFCXXX2DXXa_!!1052419246.jpg', 'cam', '', 1, '27.80', 2, 54, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(388, 36, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd1.alicdn.com/imgextra/i1/1052419246/TB2jEqhiwJkpuFjSszcXXXfsFXa_!!1052419246.jpg', 'xanh', '', 2, '27.80', 2, 54, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(389, 36, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd4.alicdn.com/imgextra/i3/1052419246/TB2OkeOirtlpuFjSspoXXbcDpXa_!!1052419246.jpg_400x400.jpg', 'đen', '', 3, '27.80', 2, 54, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(390, 36, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd3.alicdn.com/imgextra/i2/1052419246/TB2jCCuiwNlpuFjy0FfXXX3CpXa_!!1052419246.jpg_400x400.jpg', 'trắng', '', 1, '27.80', 2, 54, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(391, 36, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.67.UfbOEd&id=549319618501', 'https://gd1.alicdn.com/imgextra/i1/196173223/TB269CBphtmpuFjSZFqXXbHFpXa_!!196173223.jpg', 'nhu link', 'L', 1, '159.00', 2, 55, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(392, 36, 'https://item.taobao.com/item.htm?spm=2013.1.20141001.1.hCbBH2&id=534241084889&scm=1007.10115.36023.100200300000000&pvid=b8a9bcc7-7c6e-4d84-a54f-4ffcaef06a52&idnum=0', 'https://gd1.alicdn.com/imgextra/i2/47883575/TB2qiN9qVXXXXbuXpXXXXXXXXXX_!!47883575.jpg', 'xanh', '38', 1, '32.90', 2, 56, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(393, 36, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.93.rxgNUK&id=549734000014', 'https://gd4.alicdn.com/imgextra/i4/196173223/TB23o2UoB8lpuFjSspaXXXJKpXa_!!196173223.jpg', 'nâu nhạt như link', 'S, M', 2, '69.00', 2, 57, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(394, 36, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.93.rxgNUK&id=549734000014', 'https://gd4.alicdn.com/imgextra/i4/196173223/TB2I0rikhRDOuFjSZFzXXcIipXa_!!196173223.jpg', 'nâu đậm như link', 'M', 1, '69.00', 2, 57, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(395, 36, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.2.7jzsSk&id=21810287147&scm=1007.12144.81309.42296_0&pvid=11ef7063-a2ae-429d-8d32-c74b95516198', 'https://gd2.alicdn.com/imgextra/i3/182560466/TB2C3w3rVXXXXbTXXXXXXXXXXXX_!!182560466.jpg_400x400.jpg', 'nhu link', '', 1, '55.00', 2, 58, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(396, 36, 'https://item.taobao.com/item.htm?spm=a230r.1.999.24.XJsBEB&id=550861198344&ns=1#detail', 'https://gd4.alicdn.com/imgextra/i3/2135453531/TB2rM0JsrJmpuFjSZFwXXaE4VXa_!!2135453531.jpg', 'nhu link', '', 1, '45.00', 2, 59, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(397, 36, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.1.Zj30dR&id=547122540440&scm=1007.12144.81309.42296_0&pvid=e9dedb3b-f99d-4895-9b99-a44db246bd40', 'https://gd2.alicdn.com/imgextra/i2/825236775/TB2aAatkrJmpuFjSZFwXXaE4VXa_!!825236775.jpg', 'nhu link', '', 1, '69.00', 2, 60, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(398, 36, 'https://item.taobao.com/item.htm?spm=2013.1.20141003.6.kLBO0j&scm=1007.10011.70203.100200300000001&id=548785739054&pvid=996dfa79-fbe3-4109-8afb-e735983b579e', 'https://gd4.alicdn.com/imgextra/i4/1874382357/TB2sXtaom0mpuFjSZPiXXbssVXa_!!1874382357.jpg', 'đen', '40', 1, '78.00', 2, 61, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(399, 36, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.3.W960DT&id=550479040142&scm=1007.12144.81309.42296_0&pvid=7dd34783-4e79-4d05-88a5-3839d698dd70', 'https://img.alicdn.com/imgextra/i3/1990942755/TB2AlZBrSVmpuFjSZFFXXcZApXa_!!1990942755.png', 'hông', 'S', 1, '38.61', 2, 62, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(400, 36, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.3.W960DT&id=550479040142&scm=1007.12144.81309.42296_0&pvid=7dd34783-4e79-4d05-88a5-3839d698dd70', 'https://img.alicdn.com/imgextra/i2/1990942755/TB2XFRnrYBmpuFjSZFAXXaQ0pXa_!!1990942755.png', 'nhu link', 'S', 1, '45.54', 2, 63, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(401, 36, 'https://detail.1688.com/offer/539633685822.html?spm=a261y.7663282.1998411376.2.J09kT3', '', 'nhu ảnh', 'L', 1, '48.00', 2, 64, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(402, 36, 'https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w13675275-16254649721.38.ENtt2m&id=534106481988', '', 'đen', 'M', 0, '25.00', 1, 65, '', '', 2, NULL, 'Sản phẩm hết hàng', NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-20 10:25:28'),
(403, 36, 'https://item.taobao.com/item.htm?spm=2013.1.20141002.10.0rv1E6&scm=1007.10009.70205.100200300000001&id=548142068176&pvid=83374539-9a78-4f7b-986f-4d0db958437c', 'https://gd4.alicdn.com/imgextra/i3/0/TB1s3wfQpXXXXb3XVXXXXXXXXXX_!!0-item_pic.jpg', 'đen', '', 1, '29.80', 2, 66, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(404, 37, 'https://item.taobao.com/item.htm?spm=2013.1.0.0.ugpbiL&id=548040625509&scm=1007.11962.70328.100200300000000&pvid=98e7283f-281c-4950-a2c8-076eda4e91ea&idnum=0', 'https://img.alicdn.com/imgextra/i2/169642122/TB2tPrWmstnpuFjSZFKXXalFFXa_!!169642122.jpg', 'màu xanh', 'size M', 1, '108.00', 2, 67, 'e xem link ảnh đúng chưa nha', '', NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(405, 37, 'https://item.taobao.com/item.htm?spm=a230r.1.999.65.ELFyjg&id=546834482090&ns=1#detail', '', 'màu đen', '', 1, '39.00', 2, 68, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(406, 37, 'https://item.taobao.com/item.htm?spm=a230r.1.999.110.9VdDN9&id=551114895566&ns=1#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19074020_805761696252353_931584641_n.jpg?oh=c39d935d2a02e2d58d8f7d144f9151ef&oe=5940FCE3', 'màu xanh', 'size 38', 1, '228.00', 2, 69, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(407, 37, 'https://item.taobao.com/item.htm?spm=a230r.1.999.1.VhkB8p&id=548859836912&ns=1#detail', 'https://gd1.alicdn.com/imgextra/i3/TB1dyHcQFXXXXbIXVXXYXGcGpXX_M2.SS2_400x400.jpg', 'như hình', 'size 41', 1, '198.00', 2, 70, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(408, 37, 'https://item.taobao.com/item.htm?spm=a230r.1.999.208.svqSA6&id=551773361944&ns=1#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19113297_806258002869389_499790015_n.jpg?oh=6437d0480d2030e8158ad81de987b6f8&oe=59404301', 'màu đỏ như hình', 'size 41', 1, '168.00', 2, 71, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(409, 37, 'https://item.taobao.com/item.htm?spm=a230r.1.14.100.Jv1abT&id=552368076344&ns=1&abbucket=4#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19113395_806261219535734_964412708_n.jpg?oh=17645eac4ae2058b07e226373fea8fe2&oe=5940FCED', 'màu bạc mã số S76004 như ảnh', 'size 40', 1, '138.00', 2, 72, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(410, 38, 'https://item.taobao.com/item.htm?spm=2013.1.0.0.ugpbiL&id=548040625509&scm=1007.11962.70328.100200300000000&pvid=98e7283f-281c-4950-a2c8-076eda4e91ea&idnum=0', 'https://img.alicdn.com/imgextra/i2/169642122/TB2tPrWmstnpuFjSZFKXXalFFXa_!!169642122.jpg', 'màu xanh', 'size M', 1, '108.00', 2, 73, 'e xem link ảnh đúng chưa nha', '', NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(411, 38, 'https://item.taobao.com/item.htm?spm=a230r.1.999.65.ELFyjg&id=546834482090&ns=1#detail', '', 'màu đen', '', 1, '39.00', 2, 74, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(412, 38, 'https://item.taobao.com/item.htm?spm=a230r.1.999.110.9VdDN9&id=551114895566&ns=1#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19074020_805761696252353_931584641_n.jpg?oh=c39d935d2a02e2d58d8f7d144f9151ef&oe=5940FCE3', 'màu xanh', 'size 38', 1, '228.00', 2, 75, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(413, 38, 'https://item.taobao.com/item.htm?spm=a230r.1.999.1.VhkB8p&id=548859836912&ns=1#detail', 'https://gd1.alicdn.com/imgextra/i3/TB1dyHcQFXXXXbIXVXXYXGcGpXX_M2.SS2_400x400.jpg', 'như hình', 'size 41', 1, '198.00', 2, 76, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(414, 38, 'https://item.taobao.com/item.htm?spm=a230r.1.999.208.svqSA6&id=551773361944&ns=1#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19113297_806258002869389_499790015_n.jpg?oh=6437d0480d2030e8158ad81de987b6f8&oe=59404301', 'màu đỏ như hình', 'size 41', 1, '168.00', 2, 77, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(415, 38, 'https://item.taobao.com/item.htm?spm=a230r.1.14.100.Jv1abT&id=552368076344&ns=1&abbucket=4#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19113395_806261219535734_964412708_n.jpg?oh=17645eac4ae2058b07e226373fea8fe2&oe=5940FCED', 'màu bạc mã số S76004 như ảnh', 'size 40', 1, '138.00', 2, 78, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(416, 39, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.81.IVjEF6&id=550552245247', 'https://gd3.alicdn.com/imgextra/i3/TB1clJ9RXXXXXb_XVXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '55.00', 2, 79, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(417, 39, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.69.aVzT7n&id=551232371934', 'https://gd2.alicdn.com/imgextra/i3/TB1AXQfRXXXXXabaXXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '90.00', 2, 79, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(418, 39, 'https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-1713366655.120.B1wmiJ&id=547643024907', 'https://gd2.alicdn.com/imgextra/i2/TB1p1yxQpXXXXbhXXXXYXGcGpXX_M2.SS2', 'nhu link', 'S', 1, '60.00', 2, 79, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(419, 39, 'https://item.taobao.com/item.htm?spm=a230r.1.999.1.jTPGnk&id=549625529635&ns=1#detail', 'https://gd3.alicdn.com/imgextra/i2/0/TB1uafAQpXXXXcKXVXXXXXXXXXX_!!0-item_pic.jpg', 'nhu link', 'L. XL', 2, '39.90', 2, 80, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(420, 39, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-16330272333.85.c2BTH2&id=550975143766', 'https://gd1.alicdn.com/imgextra/i3/1133972013/TB2DBcfsElnpuFjSZFjXXXTaVXa_!!1133972013.jpg', 'nhu link', 'M', 1, '158.00', 2, 81, 'giuc shop cho chi', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(421, 39, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd2.alicdn.com/imgextra/i2/1052419246/TB2089DmiRnpuFjSZFCXXX2DXXa_!!1052419246.jpg', 'cam', '', 1, '27.80', 2, 82, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(422, 39, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd1.alicdn.com/imgextra/i1/1052419246/TB2jEqhiwJkpuFjSszcXXXfsFXa_!!1052419246.jpg', 'xanh', '', 2, '27.80', 2, 82, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(423, 39, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd4.alicdn.com/imgextra/i3/1052419246/TB2OkeOirtlpuFjSspoXXbcDpXa_!!1052419246.jpg_400x400.jpg', 'đen', '', 3, '27.80', 2, 82, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(424, 39, 'https://item.taobao.com/item.htm?spm=2013.1.1998246701.3.0rv1E6&scm=1007.10152.33975.1p0&id=547067793131&pvid=7ab395cd-1e43-4752-98d1-019dd91bfd6a', 'https://gd3.alicdn.com/imgextra/i2/1052419246/TB2jCCuiwNlpuFjy0FfXXX3CpXa_!!1052419246.jpg_400x400.jpg', 'trắng', '', 1, '27.80', 2, 82, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(425, 39, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.67.UfbOEd&id=549319618501', 'https://gd1.alicdn.com/imgextra/i1/196173223/TB269CBphtmpuFjSZFqXXbHFpXa_!!196173223.jpg', 'nhu link', 'L', 1, '159.00', 2, 83, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(426, 39, 'https://item.taobao.com/item.htm?spm=2013.1.20141001.1.hCbBH2&id=534241084889&scm=1007.10115.36023.100200300000000&pvid=b8a9bcc7-7c6e-4d84-a54f-4ffcaef06a52&idnum=0', 'https://gd1.alicdn.com/imgextra/i2/47883575/TB2qiN9qVXXXXbuXpXXXXXXXXXX_!!47883575.jpg', 'xanh', '38', 1, '32.90', 2, 84, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(427, 39, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.93.rxgNUK&id=549734000014', 'https://gd4.alicdn.com/imgextra/i4/196173223/TB23o2UoB8lpuFjSspaXXXJKpXa_!!196173223.jpg', 'nâu nhạt như link', 'S, M', 2, '69.00', 2, 85, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(428, 39, 'https://item.taobao.com/item.htm?spm=a1z10.3-c-s.w4002-15337467954.93.rxgNUK&id=549734000014', 'https://gd4.alicdn.com/imgextra/i4/196173223/TB2I0rikhRDOuFjSZFzXXcIipXa_!!196173223.jpg', 'nâu đậm như link', 'M', 1, '69.00', 2, 85, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(429, 39, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.2.7jzsSk&id=21810287147&scm=1007.12144.81309.42296_0&pvid=11ef7063-a2ae-429d-8d32-c74b95516198', 'https://gd2.alicdn.com/imgextra/i3/182560466/TB2C3w3rVXXXXbTXXXXXXXXXXXX_!!182560466.jpg_400x400.jpg', 'nhu link', '', 1, '55.00', 2, 86, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(430, 39, 'https://item.taobao.com/item.htm?spm=a230r.1.999.24.XJsBEB&id=550861198344&ns=1#detail', 'https://gd4.alicdn.com/imgextra/i3/2135453531/TB2rM0JsrJmpuFjSZFwXXaE4VXa_!!2135453531.jpg', 'nhu link', '', 1, '45.00', 2, 87, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(431, 39, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.1.Zj30dR&id=547122540440&scm=1007.12144.81309.42296_0&pvid=e9dedb3b-f99d-4895-9b99-a44db246bd40', 'https://gd2.alicdn.com/imgextra/i2/825236775/TB2aAatkrJmpuFjSZFwXXaE4VXa_!!825236775.jpg', 'nhu link', '', 1, '69.00', 2, 88, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(432, 39, 'https://item.taobao.com/item.htm?spm=2013.1.20141003.6.kLBO0j&scm=1007.10011.70203.100200300000001&id=548785739054&pvid=996dfa79-fbe3-4109-8afb-e735983b579e', 'https://gd4.alicdn.com/imgextra/i4/1874382357/TB2sXtaom0mpuFjSZPiXXbssVXa_!!1874382357.jpg', 'đen', '40', 1, '78.00', 2, 89, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(433, 39, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.3.W960DT&id=550479040142&scm=1007.12144.81309.42296_0&pvid=7dd34783-4e79-4d05-88a5-3839d698dd70', 'https://img.alicdn.com/imgextra/i3/1990942755/TB2AlZBrSVmpuFjSZFFXXcZApXa_!!1990942755.png', 'hông', 'S', 1, '38.61', 2, 90, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(434, 39, 'https://item.taobao.com/item.htm?ft=t&spm=2013.1.20141001.3.W960DT&id=550479040142&scm=1007.12144.81309.42296_0&pvid=7dd34783-4e79-4d05-88a5-3839d698dd70', 'https://img.alicdn.com/imgextra/i2/1990942755/TB2XFRnrYBmpuFjSZFAXXaQ0pXa_!!1990942755.png', 'nhu link', 'S', 1, '45.54', 2, 91, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(435, 39, 'https://detail.1688.com/offer/539633685822.html?spm=a261y.7663282.1998411376.2.J09kT3', '', 'nhu ảnh', 'L', 1, '48.00', 2, 92, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(436, 39, 'https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w13675275-16254649721.38.ENtt2m&id=534106481988', '', 'đen', 'M', 2, '25.00', 2, 93, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(437, 39, 'https://item.taobao.com/item.htm?spm=2013.1.20141002.10.0rv1E6&scm=1007.10009.70205.100200300000001&id=548142068176&pvid=83374539-9a78-4f7b-986f-4d0db958437c', 'https://gd4.alicdn.com/imgextra/i3/0/TB1s3wfQpXXXXb3XVXXXXXXXXXX_!!0-item_pic.jpg', 'đen', '', 1, '29.80', 2, 94, '', '', NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(438, 41, 'https://item.taobao.com/item.htm?spm=2013.1.0.0.ugpbiL&id=548040625509&scm=1007.11962.70328.100200300000000&pvid=98e7283f-281c-4950-a2c8-076eda4e91ea&idnum=0', 'https://img.alicdn.com/imgextra/i2/169642122/TB2tPrWmstnpuFjSZFKXXalFFXa_!!169642122.jpg', 'màu xanh', 'size M', 1, '108.00', 2, 96, 'e xem link ảnh đúng chưa nha', '', NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(439, 41, 'https://item.taobao.com/item.htm?spm=a230r.1.999.65.ELFyjg&id=546834482090&ns=1#detail', '', 'màu đen', '', 1, '39.00', 2, 97, '', '', NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(440, 41, 'https://item.taobao.com/item.htm?spm=a230r.1.999.110.9VdDN9&id=551114895566&ns=1#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19074020_805761696252353_931584641_n.jpg?oh=c39d935d2a02e2d58d8f7d144f9151ef&oe=5940FCE3', 'màu xanh', 'size 38', 1, '228.00', 2, 98, '', '', NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(441, 41, 'https://item.taobao.com/item.htm?spm=a230r.1.999.1.VhkB8p&id=548859836912&ns=1#detail', 'https://gd1.alicdn.com/imgextra/i3/TB1dyHcQFXXXXbIXVXXYXGcGpXX_M2.SS2_400x400.jpg', 'như hình', 'size 41', 1, '198.00', 2, 99, '', '', NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(442, 41, 'https://item.taobao.com/item.htm?spm=a230r.1.999.208.svqSA6&id=551773361944&ns=1#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19113297_806258002869389_499790015_n.jpg?oh=6437d0480d2030e8158ad81de987b6f8&oe=59404301', 'màu đỏ như hình', 'size 41', 1, '168.00', 2, 100, '', '', NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(443, 41, 'https://item.taobao.com/item.htm?spm=a230r.1.14.100.Jv1abT&id=552368076344&ns=1&abbucket=4#detail', 'https://scontent.fhan2-2.fna.fbcdn.net/v/t34.0-12/19113395_806261219535734_964412708_n.jpg?oh=17645eac4ae2058b07e226373fea8fe2&oe=5940FCED', 'màu bạc mã số S76004 như ảnh', 'size 40', 1, '138.00', 2, 101, '', '', NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(444, 42, 'https://world.taobao.com/item/546230751270.htm?fromSite=main#detail', '', '', '', 0, '8.00', 1, 102, 'bnnnnnnnnnn', '', 10, NULL, 'Sản phẩm hết hàng', NULL, 1, '2017-07-03 16:33:00', 21, '2017-07-03 16:56:12'),
(445, 42, 'https://world.taobao.com/item/546230751270.htm?fromSite=main#detail', '', 'https://world.taobao.com/item/549827996602.htm?fromSite=main&ali_refid=a3_430618_1006:1121810257:N:%', '', 1, '1.00', 2, 103, 'bbbbbbbbbbbb', '', NULL, NULL, NULL, NULL, 1, '2017-07-03 16:33:00', 1, '2017-07-03 16:53:42'),
(446, 43, 'https://world.taobao.com/item/546230751270.htm?fromSite=main#detail', '', '', '', 10, '8.00', 2, 104, '', '', NULL, NULL, NULL, NULL, 1, '2017-07-07 11:44:00', NULL, NULL),
(447, 43, 'https://world.taobao.com/item/546230751270.htm?fromSite=main#detail', '', 'https://world.taobao.com/item/549827996602.htm?fromSite=main&ali_refid=a3_430618_1006:1121810257:N:%', '', 1, '1.00', 2, 105, '', '', NULL, NULL, NULL, NULL, 1, '2017-07-07 11:44:00', NULL, NULL),
(448, 45, 'https://world.tmall.com/item/522066817896.htm?spm=a312a.7700718.1998025129.9.iwzQwo&abtest=_AB-LR32-PR32&pvid=1a5702ee-351d-4260-bfe2-5eaaa25f830a&pos=5&abbucket=_AB-M32_B17&acm=03054.1003.1.1539344&id=522066817896&scm=1007.12144.81309.23864_0&skuId=', 'https:img.alicdn.com/imgextra/i1/2094190888/TB27LGQcCOI.eBjSspmXXatOVXa_!!2094190888.jpg', '官方标配', '深灰色', 1, '48.90', 2, 32, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-07-20 17:44:41', NULL, NULL),
(449, 46, 'https://world.tmall.com/item/522066817896.htm?spm=a312a.7700718.1998025129.9.iwzQwo&abtest=_AB-LR32-PR32&pvid=1a5702ee-351d-4260-bfe2-5eaaa25f830a&pos=5&abbucket=_AB-M32_B17&acm=03054.1003.1.1539344&id=522066817896&scm=1007.12144.81309.23864_0&skuId=', 'https:img.alicdn.com/imgextra/i1/2094190888/TB27LGQcCOI.eBjSspmXXatOVXa_!!2094190888.jpg', '官方标配', '深灰色', 1, '48.90', 2, 108, NULL, NULL, NULL, NULL, NULL, NULL, 6, '2017-07-20 18:59:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `ProvinceId` tinyint(4) NOT NULL,
  `ProvinceName` varchar(45) NOT NULL,
  `DisplayOrder` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`ProvinceId`, `ProvinceName`, `DisplayOrder`) VALUES
(1, 'An Giang', 3),
(2, 'Bà Rịa - Vũng Tàu', 3),
(3, 'Bắc Giang', 3),
(4, 'Bắc Kạn', 3),
(5, 'Bạc Liêu', 3),
(6, 'Bắc Ninh', 3),
(7, 'Bến Tre', 3),
(8, 'Bình Định', 3),
(9, 'Bình Dương', 3),
(10, 'Bình Phước', 3),
(11, 'Bình Thuận', 3),
(12, 'Cà Mau', 3),
(13, 'Cao Bằng', 3),
(14, 'Cần Thơ', 3),
(15, 'Đà Nẵng', 3),
(16, 'Đắk Lắk', 3),
(17, 'Đắk Nông', 3),
(18, 'Điện Biên', 3),
(19, 'Đồng Nai', 3),
(20, 'Đồng Tháp', 3),
(21, 'Gia Lai', 3),
(22, 'Hà Giang', 3),
(23, 'Hà Nam', 3),
(24, 'Hà Nội', 1),
(25, 'Hà Tĩnh', 3),
(26, 'Hải Dương', 3),
(27, 'Hải Phòng', 3),
(28, 'Hậu Giang', 3),
(29, 'Hòa Bình', 3),
(30, 'Hưng Yên', 3),
(31, 'Khánh Hòa', 3),
(32, 'Kiên Giang', 3),
(33, 'Kon Tum', 3),
(34, 'Lai Châu', 3),
(35, 'Lâm Đồng', 3),
(36, 'Lạng Sơn', 3),
(37, 'Lào Cai', 3),
(38, 'Long An', 3),
(39, 'Nam Định', 3),
(40, 'Nghệ An', 3),
(41, 'Ninh Bình', 3),
(42, 'Ninh Thuận', 3),
(43, 'Phú Thọ', 3),
(44, 'Quảng Bình', 3),
(45, 'Quảng Nam', 3),
(46, 'Quảng Ngãi', 3),
(47, 'Quảng Ninh', 3),
(48, 'Quảng Trị', 3),
(49, 'Sóc Trăng', 3),
(50, 'Sơn La', 3),
(51, 'Tây Ninh', 3),
(52, 'Thái Bình', 3),
(53, 'Thái Nguyên', 3),
(54, 'Thanh Hóa', 3),
(55, 'Thừa Thiên Huế', 3),
(56, 'Tiền Giang', 3),
(57, 'TP Hồ Chí Minh', 3),
(58, 'Trà Vinh', 3),
(59, 'Tuyên Quang', 3),
(60, 'Vĩnh Long', 3),
(61, 'Vĩnh Phúc', 3),
(62, 'Yên Bái', 3),
(63, 'Phú Yên', 3);

-- --------------------------------------------------------

--
-- Table structure for table `recharges`
--

CREATE TABLE `recharges` (
  `RechargeId` int(11) NOT NULL,
  `BankAccountId` smallint(6) NOT NULL,
  `ShopNumberId` int(11) NOT NULL,
  `PaidTQ` decimal(10,2) NOT NULL,
  `ExchangeRate` int(11) NOT NULL,
  `RechargeTypeId` tinyint(4) NOT NULL,
  `Balance` decimal(10,2) NOT NULL,
  `RechargeDate` datetime NOT NULL,
  `Comment` varchar(650) DEFAULT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recharges`
--

INSERT INTO `recharges` (`RechargeId`, `BankAccountId`, `ShopNumberId`, `PaidTQ`, `ExchangeRate`, `RechargeTypeId`, `Balance`, `RechargeDate`, `Comment`, `CrUserId`, `CrDateTime`) VALUES
(1, 1, 0, '1000.00', 3456, 2, '1000.00', '0000-00-00 00:00:00', 'Napm lanf 1', 1, '2017-07-17 17:39:52'),
(2, 1, 0, '2500.00', 3456, 2, '3500.00', '0000-00-00 00:00:00', 'Nap lan 2', 1, '2017-07-17 17:40:35'),
(3, 1, 0, '500.00', 3347, 2, '4000.00', '2017-07-17 17:42:49', 'Nap lan 3', 1, '2017-07-17 17:42:49'),
(4, 2, 15, '1.00', 0, 1, '1.00', '2017-07-25 20:53:05', 'Đòi lại tiền STT STT1', 21, '2017-07-25 20:53:05'),
(5, 1, 3, '50.18', 0, 1, '4050.18', '2017-07-25 20:54:08', 'Đòi lại tiền STT Stt1', 21, '2017-07-25 20:54:08'),
(6, 1, 3, '90.00', 0, 3, '3960.18', '2017-07-25 20:54:52', 'Thanh toán STT Stt1', 21, '2017-07-25 20:54:52'),
(7, 1, 5, '2.00', 0, 3, '3958.18', '2017-08-08 22:27:40', 'Thanh toán STT STT2', 1, '2017-08-08 22:27:40');

-- --------------------------------------------------------

--
-- Table structure for table `roleactions`
--

CREATE TABLE `roleactions` (
  `RoleActionId` int(11) NOT NULL,
  `RoleId` tinyint(4) NOT NULL,
  `ActionId` smallint(6) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roleactions`
--

INSERT INTO `roleactions` (`RoleActionId`, `RoleId`, `ActionId`, `CrUserId`, `CrDateTime`) VALUES
(1, 1, 1, 1, '2016-10-05 14:48:54'),
(2, 1, 4, 1, '2016-10-05 14:48:54'),
(3, 1, 5, 1, '2016-10-05 14:48:54'),
(4, 1, 6, 1, '2016-10-05 14:48:54'),
(5, 1, 7, 1, '2016-10-05 14:48:54'),
(6, 1, 8, 1, '2016-10-05 14:48:54'),
(7, 1, 9, 1, '2016-10-05 14:48:54'),
(8, 1, 10, 1, '2016-10-05 14:48:54'),
(9, 1, 11, 1, '2016-10-05 14:48:54'),
(10, 1, 12, 1, '2016-10-05 14:48:54'),
(11, 1, 13, 1, '2016-10-05 14:48:54'),
(12, 1, 14, 1, '2016-10-05 14:48:54'),
(13, 1, 15, 1, '2016-10-05 14:48:54'),
(14, 1, 16, 1, '2016-10-05 14:48:54'),
(15, 1, 17, 1, '2016-10-05 14:48:54'),
(16, 1, 18, 1, '2016-10-05 14:48:54'),
(17, 1, 22, 1, '2016-10-05 14:48:54'),
(18, 1, 23, 1, '2016-10-05 14:48:54'),
(19, 1, 24, 1, '2016-10-05 14:48:54'),
(20, 1, 25, 1, '2016-10-05 14:48:54'),
(21, 1, 26, 1, '2016-10-05 14:48:54'),
(22, 1, 27, 1, '2016-10-05 14:48:54'),
(23, 1, 28, 1, '2016-10-05 14:48:54'),
(24, 1, 29, 1, '2016-10-05 14:48:54'),
(25, 1, 30, 1, '2016-10-05 14:48:54'),
(26, 1, 31, 1, '2016-10-05 14:48:54'),
(27, 1, 32, 1, '2016-10-05 14:48:54'),
(28, 1, 33, 1, '2016-10-05 14:48:54'),
(29, 1, 34, 1, '2016-10-05 14:48:54'),
(30, 1, 35, 1, '2016-10-05 14:48:54'),
(31, 1, 36, 1, '2016-10-05 14:48:54'),
(32, 1, 37, 1, '2016-10-05 14:48:54'),
(33, 1, 40, 1, '2016-10-05 14:48:54'),
(34, 1, 41, 1, '2016-10-05 14:48:54'),
(35, 1, 42, 1, '2016-10-05 14:48:54'),
(36, 1, 43, 1, '2016-10-05 14:48:54'),
(37, 2, 54, 1, '2017-04-25 15:30:20'),
(38, 2, 55, 1, '2017-04-25 15:30:20'),
(39, 2, 56, 1, '2017-04-25 15:30:20'),
(40, 2, 57, 1, '2017-04-25 15:30:20'),
(41, 2, 59, 1, '2017-04-25 15:30:20'),
(42, 2, 60, 1, '2017-04-25 15:30:20'),
(43, 2, 63, 1, '2017-04-25 15:30:20'),
(44, 2, 64, 1, '2017-04-25 15:30:20'),
(125, 3, 7, 1, '2017-07-25 11:19:03'),
(126, 3, 8, 1, '2017-07-25 11:19:03'),
(127, 3, 13, 1, '2017-07-25 11:19:03'),
(128, 3, 23, 1, '2017-07-25 11:19:03'),
(129, 3, 9, 1, '2017-07-25 11:19:03'),
(130, 3, 33, 1, '2017-07-25 11:19:03'),
(131, 3, 37, 1, '2017-07-25 11:19:03'),
(132, 3, 14, 1, '2017-07-25 11:19:03'),
(133, 3, 77, 1, '2017-07-25 11:19:03'),
(134, 3, 10, 1, '2017-07-25 11:19:03'),
(135, 3, 11, 1, '2017-07-25 11:19:03'),
(136, 3, 15, 1, '2017-07-25 11:19:03'),
(137, 3, 17, 1, '2017-07-25 11:19:03'),
(138, 3, 24, 1, '2017-07-25 11:19:03'),
(139, 3, 12, 1, '2017-07-25 11:19:03'),
(140, 3, 109, 1, '2017-07-25 11:19:03'),
(141, 3, 16, 1, '2017-07-25 11:19:03'),
(142, 3, 22, 1, '2017-07-25 11:19:03'),
(143, 3, 28, 1, '2017-07-25 11:19:03'),
(144, 3, 25, 1, '2017-07-25 11:19:03'),
(145, 3, 29, 1, '2017-07-25 11:19:03'),
(146, 3, 89, 1, '2017-07-25 11:19:03'),
(147, 3, 100, 1, '2017-07-25 11:19:03'),
(148, 3, 101, 1, '2017-07-25 11:19:03'),
(149, 3, 102, 1, '2017-07-25 11:19:03'),
(150, 3, 106, 1, '2017-07-25 11:19:03'),
(151, 3, 54, 1, '2017-07-25 11:19:03'),
(152, 3, 55, 1, '2017-07-25 11:19:03'),
(153, 3, 58, 1, '2017-07-25 11:19:03'),
(154, 3, 56, 1, '2017-07-25 11:19:03'),
(155, 3, 59, 1, '2017-07-25 11:19:03'),
(156, 3, 61, 1, '2017-07-25 11:19:03'),
(157, 3, 63, 1, '2017-07-25 11:19:03'),
(158, 3, 57, 1, '2017-07-25 11:19:03'),
(159, 3, 60, 1, '2017-07-25 11:19:03'),
(160, 3, 62, 1, '2017-07-25 11:19:03'),
(161, 3, 64, 1, '2017-07-25 11:19:03'),
(162, 3, 82, 1, '2017-07-25 11:19:03'),
(163, 3, 81, 1, '2017-07-25 11:19:03'),
(164, 3, 97, 1, '2017-07-25 11:19:03'),
(165, 3, 78, 1, '2017-07-25 11:19:03'),
(166, 3, 107, 1, '2017-07-25 11:19:03'),
(167, 3, 108, 1, '2017-07-25 11:19:03'),
(168, 3, 50, 1, '2017-07-25 11:19:03'),
(169, 3, 1, 1, '2017-07-25 11:19:03'),
(170, 3, 44, 1, '2017-07-25 11:19:03'),
(171, 3, 45, 1, '2017-07-25 11:19:03'),
(172, 3, 46, 1, '2017-07-25 11:19:03'),
(173, 3, 83, 1, '2017-07-25 11:19:03'),
(174, 3, 84, 1, '2017-07-25 11:19:03'),
(175, 3, 85, 1, '2017-07-25 11:19:03'),
(176, 3, 47, 1, '2017-07-25 11:19:03'),
(177, 3, 48, 1, '2017-07-25 11:19:03'),
(178, 3, 49, 1, '2017-07-25 11:19:03'),
(179, 3, 51, 1, '2017-07-25 11:19:03'),
(180, 3, 52, 1, '2017-07-25 11:19:03'),
(181, 3, 103, 1, '2017-07-25 11:19:03'),
(182, 3, 104, 1, '2017-07-25 11:19:03'),
(183, 3, 105, 1, '2017-07-25 11:19:03'),
(184, 3, 86, 1, '2017-07-25 11:19:03'),
(185, 3, 87, 1, '2017-07-25 11:19:03'),
(186, 3, 88, 1, '2017-07-25 11:19:03'),
(187, 3, 90, 1, '2017-07-25 11:19:03'),
(188, 3, 91, 1, '2017-07-25 11:19:03'),
(189, 3, 92, 1, '2017-07-25 11:19:03'),
(190, 3, 93, 1, '2017-07-25 11:19:03'),
(191, 3, 94, 1, '2017-07-25 11:19:03'),
(192, 3, 95, 1, '2017-07-25 11:19:03'),
(193, 3, 80, 1, '2017-07-25 11:19:03'),
(194, 3, 18, 1, '2017-07-25 11:19:03'),
(195, 3, 26, 1, '2017-07-25 11:19:03'),
(196, 3, 27, 1, '2017-07-25 11:19:03'),
(197, 3, 4, 1, '2017-07-25 11:19:03'),
(198, 3, 5, 1, '2017-07-25 11:19:03'),
(199, 3, 6, 1, '2017-07-25 11:19:03'),
(200, 3, 65, 1, '2017-07-25 11:19:03'),
(201, 3, 66, 1, '2017-07-25 11:19:03'),
(202, 3, 67, 1, '2017-07-25 11:19:03'),
(203, 3, 68, 1, '2017-07-25 11:19:03'),
(204, 3, 69, 1, '2017-07-25 11:19:03'),
(205, 3, 70, 1, '2017-07-25 11:19:03'),
(206, 3, 71, 1, '2017-07-25 11:19:03'),
(207, 3, 72, 1, '2017-07-25 11:19:03'),
(208, 3, 79, 1, '2017-07-25 11:19:03'),
(209, 3, 73, 1, '2017-07-25 11:19:03'),
(210, 3, 74, 1, '2017-07-25 11:19:03'),
(211, 3, 75, 1, '2017-07-25 11:19:03');

-- --------------------------------------------------------

--
-- Table structure for table `salemilestones`
--

CREATE TABLE `salemilestones` (
  `SaleMilestoneId` smallint(6) NOT NULL,
  `BeginCost` int(11) NOT NULL,
  `EndCost` int(11) NOT NULL,
  `SalePercent` tinyint(4) NOT NULL,
  `BonusCost` int(11) NOT NULL,
  `TotalCost` int(11) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `servicefeelogs`
--

CREATE TABLE `servicefeelogs` (
  `ServiceFeeLogId` int(11) NOT NULL,
  `ServiceFeeId` smallint(6) NOT NULL,
  `BeginCost` smallint(6) NOT NULL,
  `EndCost` smallint(6) NOT NULL,
  `Percent` decimal(3,1) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `servicefeelogs`
--

INSERT INTO `servicefeelogs` (`ServiceFeeLogId`, `ServiceFeeId`, `BeginCost`, `EndCost`, `Percent`, `StatusId`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 1, 0, 1, '10.0', 2, 1, '2016-10-05 16:43:28', 1, '2016-11-05 09:39:04');

-- --------------------------------------------------------

--
-- Table structure for table `servicefees`
--

CREATE TABLE `servicefees` (
  `ServiceFeeId` smallint(6) NOT NULL,
  `BeginCost` smallint(6) NOT NULL,
  `EndCost` smallint(6) NOT NULL,
  `Percent` decimal(3,1) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `servicefees`
--

INSERT INTO `servicefees` (`ServiceFeeId`, `BeginCost`, `EndCost`, `Percent`, `StatusId`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 0, 2, '10.0', 2, 1, '2016-10-05 16:43:28', 1, '2016-11-05 09:39:04'),
(2, 2, 5, '7.0', 2, 1, '2016-10-05 16:46:25', NULL, NULL),
(3, 5, 50, '5.0', 2, 1, '2016-10-05 16:46:51', NULL, NULL),
(4, 50, 100, '4.0', 2, 1, '2016-10-05 16:47:20', NULL, NULL),
(5, 100, 300, '2.0', 2, 1, '2016-10-05 16:48:42', NULL, NULL),
(6, 300, 500, '1.0', 2, 1, '2016-10-05 16:48:56', NULL, NULL),
(7, 500, 10000, '1.0', 2, 1, '2016-10-05 16:50:06', 1, '2016-10-20 14:11:05');

-- --------------------------------------------------------

--
-- Table structure for table `shopnumbers`
--

CREATE TABLE `shopnumbers` (
  `ShopNumberId` int(11) NOT NULL,
  `ShopId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `ShopNo` varchar(45) NOT NULL,
  `ShopCost` decimal(10,2) NOT NULL,
  `ShopCostAdmin` decimal(10,2) NOT NULL,
  `ReturnCost` decimal(10,2) NOT NULL,
  `IsReturn` tinyint(4) NOT NULL,
  `OrderAccountId` smallint(6) NOT NULL DEFAULT '0',
  `ShopPaidStatusId` tinyint(4) NOT NULL DEFAULT '1',
  `BankAccountId` smallint(6) NOT NULL,
  `Reason` varchar(250) DEFAULT NULL,
  `Comment` varchar(250) DEFAULT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shopnumbers`
--

INSERT INTO `shopnumbers` (`ShopNumberId`, `ShopId`, `OrderId`, `ShopNo`, `ShopCost`, `ShopCostAdmin`, `ReturnCost`, `IsReturn`, `OrderAccountId`, `ShopPaidStatusId`, `BankAccountId`, `Reason`, `Comment`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(3, 50, 35, 'Stt1', '100.00', '90.00', '50.18', 1, 1, 3, 1, 'anc lui dfo', 'vcvc', 21, '2017-06-30 12:16:02', 21, '2017-07-25 20:54:52'),
(5, 48, 35, 'STT2', '2.00', '2.00', '2.00', 0, 1, 0, 1, '2', '', 1, '2017-06-15 15:42:45', 1, '2017-08-08 22:27:40'),
(7, 48, 35, 'STT4', '2.00', '1.00', '1.00', 0, 1, 2, 0, 'gggggggggggg', '', 1, '2017-06-15 15:48:05', 1, '2017-07-08 12:35:22'),
(10, 47, 35, 'STT13', '10.00', '10.00', '5.00', 0, 1, 2, 0, 'abc', NULL, 21, '2017-06-30 10:30:22', NULL, NULL),
(11, 47, 35, 'STT11', '10.00', '0.00', '0.00', 0, 1, 2, 0, '', NULL, 21, '2017-06-30 10:30:22', NULL, NULL),
(12, 46, 35, 'Stt12', '10.00', '0.00', '5.00', 0, 1, 2, 0, '', '', 21, '2017-06-30 10:31:36', 1, '2017-07-08 12:36:51'),
(13, 45, 35, 'STT11', '20.00', '0.00', '10.00', 0, 1, 2, 0, '', NULL, 21, '2017-06-30 10:31:36', NULL, NULL),
(14, 44, 35, 'STT10', '10.00', '10.00', '5.00', 0, 1, 2, 0, 'lys do', NULL, 21, '2017-06-30 10:32:35', 21, '2017-06-30 17:58:53'),
(15, 103, 42, 'STT1', '1.00', '0.50', '5000.00', 1, 1, 3, 2, 'vv', 'bbbb', 21, '2017-07-05 09:55:28', 21, '2017-07-25 20:53:05'),
(16, 103, 42, 'STT2', '1.00', '1.00', '0.00', 0, 1, 0, 0, 'fvv', '', 21, '2017-07-06 13:24:59', 1, '2017-07-07 11:00:22');

-- --------------------------------------------------------

--
-- Table structure for table `shops`
--

CREATE TABLE `shops` (
  `ShopId` int(11) NOT NULL,
  `ShopCode` varchar(45) NOT NULL,
  `ShopName` varchar(100) NOT NULL,
  `ShopUrl` varchar(250) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `CategoryId` smallint(6) NOT NULL,
  `ShipTQ` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ShipTQOldNew` decimal(10,2) NOT NULL DEFAULT '0.00',
  `ShopStatusId` tinyint(4) NOT NULL DEFAULT '2',
  `IsTransportSlow` tinyint(4) NOT NULL DEFAULT '0',
  `Comment` varchar(250) DEFAULT NULL,
  `Feedback` text,
  `ShipTQReason` varchar(250) DEFAULT NULL,
  `ShipTQConfirm` varchar(250) DEFAULT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shops`
--

INSERT INTO `shops` (`ShopId`, `ShopCode`, `ShopName`, `ShopUrl`, `OrderId`, `CategoryId`, `ShipTQ`, `ShipTQOldNew`, `ShopStatusId`, `IsTransportSlow`, `Comment`, `Feedback`, `ShipTQReason`, `ShipTQConfirm`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 'shop1', 'shop1', '', 22, 1, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(2, 'shop2', 'shop2', '', 22, 2, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(3, 'ab26287087', '廖灿锋', 'https://ab26287087.1688.com/', 25, 28, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(4, 'HV_25', 'HV_25', 'javascript:void(0)', 25, 26, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(5, 'ab26287087', '廖灿锋', 'https://ab26287087.1688.com/', 25, 28, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(6, 'HV_25', 'HV_25', '', 25, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(7, 'ab26287087', '廖灿锋', 'https://ab26287087.1688.com/', 25, 28, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(8, 'HV_25', 'HV_25', 'javascript:void(0)', 25, 26, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(9, 'oulaikanx', '欧莱卡旗舰店', 'https://oulaikanx.world.tmall.com/', 28, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', 2, '2017-07-31 10:03:05'),
(10, 'HV_5', 'HV_5', 'javascript:void(0)', 5, 0, '100.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '0000-00-00 00:00:00', 1, '2017-06-28 13:58:46'),
(11, 'oulaikanx', '欧莱卡旗舰店', 'https://oulaikanx.world.tmall.com/', 29, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(12, 'sandiskyb', '闪迪亚百专卖店', 'https://sandiskyb.world.tmall.com/', 29, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(13, 'HV_1', 'HV_1', 'javascript:void(0)', 30, 28, '18.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(14, 'HV_2', 'HV_2', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(15, 'HV_3', 'HV_3', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(16, 'HV_4', 'HV_4', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(17, 'HV_5', 'HV_5', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(18, 'HV_6', 'HV_6', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(19, 'HV_7', 'HV_7', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(20, 'HV_8', 'HV_8', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(21, 'HV_9', 'HV_9', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(22, 'HV_10', 'HV_10', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(23, 'HV_11', 'HV_11', 'javascript:void(0)', 30, 0, '15.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(24, 'HV_12', 'HV_12', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(25, 'HV_13', 'HV_13', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(26, 'HV_14', 'HV_14', 'javascript:void(0)', 30, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(27, 'HV_15', 'HV_15', 'javascript:void(0)', 30, 0, '12.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(28, 'HV_16', 'HV_16', 'javascript:void(0)', 30, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(29, 'HV_18', 'HV_18', 'javascript:void(0)', 18, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(30, 'sandiskyb', '闪迪亚百专卖店', 'https://sandiskyb.world.tmall.com/', 31, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(31, 'sandiskyb', '闪迪亚百专卖店', 'https://sandiskyb.world.tmall.com/', 32, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(32, 'sandiskyb', '闪迪亚百专卖店', 'https://sandiskyb.world.tmall.com/', 33, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(33, 'shop142273661', '龟宿奢品', 'https://shop142273661.world.taobao.com/', 34, 28, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(34, 'shop154915417', '村头鲜', 'https://shop154915417.world.taobao.com/', 34, 26, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00', NULL, NULL),
(35, 'HV_1', 'HV_1', 'javascript:void(0)', 35, 0, '18.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(36, 'HV_2', 'HV_2', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(37, 'HV_3', 'HV_3', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(38, 'HV_4', 'HV_4', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(39, 'HV_5', 'HV_5', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(40, 'HV_6', 'HV_6', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(41, 'HV_7', 'HV_7', 'javascript:void(0)', 35, 0, '0.00', '0.00', 7, 0, NULL, NULL, 'Dạ anh/ chị ơi, sản phẩm này shop báo tăng phí ship, anh/ chị có đặt không ạ?', 'ko ok', 1, '2017-06-02 22:36:53', 6, '2017-06-11 20:18:42'),
(42, 'HV_8', 'HV_8', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(43, 'HV_9', 'HV_9', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-06-02 22:40:29'),
(44, 'HV_10', 'HV_10', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 21, '2017-06-30 10:32:35'),
(45, 'HV_11', 'HV_11', 'javascript:void(0)', 35, 0, '15.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 21, '2017-06-30 10:31:36'),
(46, 'HV_12', 'HV_12', 'javascript:void(0)', 35, 0, '0.00', '0.00', 7, 0, NULL, NULL, 'Shop hết sản phẩm', NULL, 1, '2017-06-02 22:36:53', 6, '2017-06-30 16:24:11'),
(47, 'HV_13', 'HV_13', 'javascript:void(0)', 35, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 21, '2017-06-30 10:30:22'),
(48, 'HV_14', 'HV_14', 'javascript:void(0)', 35, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 21, '2017-06-17 20:45:01'),
(49, 'HV_15', 'HV_15', 'javascript:void(0)', 35, 26, '12.00', '15.00', 3, 1, NULL, NULL, '', '', 1, '2017-06-02 22:36:53', 6, '2017-06-11 19:45:44'),
(50, 'HV_16', 'HV_16', 'javascript:void(0)', 35, 28, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-02 22:36:53', 1, '2017-07-24 20:24:48'),
(51, 'HV_1', 'HV_1', 'javascript:void(0)', 36, 0, '18.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(52, 'HV_2', 'HV_2', 'javascript:void(0)', 36, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(53, 'HV_3', 'HV_3', 'javascript:void(0)', 36, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(54, 'HV_4', 'HV_4', 'javascript:void(0)', 36, 0, '50.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(55, 'HV_5', 'HV_5', 'javascript:void(0)', 36, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(56, 'HV_6', 'HV_6', 'javascript:void(0)', 36, 0, '40.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(57, 'HV_7', 'HV_7', 'javascript:void(0)', 36, 0, '20.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(58, 'HV_8', 'HV_8', 'javascript:void(0)', 36, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(59, 'HV_9', 'HV_9', 'javascript:void(0)', 36, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(60, 'HV_10', 'HV_10', 'javascript:void(0)', 36, 0, '5.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(61, 'HV_11', 'HV_11', 'javascript:void(0)', 36, 0, '15.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(62, 'HV_12', 'HV_12', 'javascript:void(0)', 36, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(63, 'HV_13', 'HV_13', 'javascript:void(0)', 36, 0, '50.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(64, 'HV_14', 'HV_14', 'javascript:void(0)', 36, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(65, 'HV_15', 'HV_15', 'javascript:void(0)', 36, 0, '0.00', '12.00', 7, 0, NULL, NULL, 'Shop hết sản phẩm', NULL, 1, '2017-06-11 14:11:27', 5, '2017-06-20 10:25:28'),
(66, 'HV_16', 'HV_16', 'javascript:void(0)', 36, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-11 14:11:27', 1, '2017-06-17 11:23:07'),
(67, 'HV_1', 'HV_1', 'javascript:void(0)', 37, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(68, 'HV_2', 'HV_2', 'javascript:void(0)', 37, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(69, 'HV_3', 'HV_3', 'javascript:void(0)', 37, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(70, 'HV_4', 'HV_4', 'javascript:void(0)', 37, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(71, 'HV_5', 'HV_5', 'javascript:void(0)', 37, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(72, 'HV_6', 'HV_6', 'javascript:void(0)', 37, 0, '12.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-14 00:00:00', NULL, NULL),
(73, 'HV_1', 'HV_1', 'javascript:void(0)', 38, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(74, 'HV_2', 'HV_2', 'javascript:void(0)', 38, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(75, 'HV_3', 'HV_3', 'javascript:void(0)', 38, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(76, 'HV_4', 'HV_4', 'javascript:void(0)', 38, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(77, 'HV_5', 'HV_5', 'javascript:void(0)', 38, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(78, 'HV_6', 'HV_6', 'javascript:void(0)', 38, 0, '12.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-15 00:00:00', NULL, NULL),
(79, 'HV_1', 'HV_1', 'javascript:void(0)', 39, 0, '18.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(80, 'HV_2', 'HV_2', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(81, 'HV_3', 'HV_3', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(82, 'HV_4', 'HV_4', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(83, 'HV_5', 'HV_5', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(84, 'HV_6', 'HV_6', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(85, 'HV_7', 'HV_7', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(86, 'HV_8', 'HV_8', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(87, 'HV_9', 'HV_9', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(88, 'HV_10', 'HV_10', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(89, 'HV_11', 'HV_11', 'javascript:void(0)', 39, 0, '15.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(90, 'HV_12', 'HV_12', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(91, 'HV_13', 'HV_13', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(92, 'HV_14', 'HV_14', 'javascript:void(0)', 39, 0, '10.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(93, 'HV_15', 'HV_15', 'javascript:void(0)', 39, 0, '12.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(94, 'HV_16', 'HV_16', 'javascript:void(0)', 39, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-06-21 22:07:00', NULL, NULL),
(95, 'HV_6', 'HV_6', 'javascript:void(0)', 5, 0, '0.00', '10.00', 7, 0, NULL, NULL, 'Shop hết sản phẩm', NULL, 1, '2017-06-28 00:00:00', 5, '2017-06-28 14:20:11'),
(96, 'HV_1', 'HV_1', 'javascript:void(0)', 41, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(97, 'HV_2', 'HV_2', 'javascript:void(0)', 41, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(98, 'HV_3', 'HV_3', 'javascript:void(0)', 41, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(99, 'HV_4', 'HV_4', 'javascript:void(0)', 41, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(100, 'HV_5', 'HV_5', 'javascript:void(0)', 41, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(101, 'HV_6', 'HV_6', 'javascript:void(0)', 41, 0, '12.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 21, '2017-06-30 14:41:00', NULL, NULL),
(102, 'HV_1', 'HV_1', 'javascript:void(0)', 42, 0, '0.00', '8.00', 7, 0, NULL, NULL, 'Shop hết sản phẩm', NULL, 1, '2017-07-03 16:33:00', 3, '2017-07-03 16:56:12'),
(103, 'HV_2', 'HV_2', 'javascript:void(0)', 42, 0, '1.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-07-03 16:33:00', 21, '2017-07-05 09:55:28'),
(104, 'HV_1', 'HV_1', 'javascript:void(0)', 43, 0, '8.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-07-07 11:44:00', NULL, NULL),
(105, 'HV_2', 'HV_2', 'javascript:void(0)', 43, 0, '1.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 1, '2017-07-07 11:44:00', NULL, NULL),
(106, 'sandiskyb', '闪迪亚百专卖店', 'https://sandiskyb.world.tmall.com/', 44, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '2017-07-20 17:42:11', NULL, NULL),
(107, 'sandiskyb', '闪迪亚百专卖店', 'https://sandiskyb.world.tmall.com/', 45, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '2017-07-20 17:44:41', NULL, NULL),
(108, 'sandiskyb', '闪迪亚百专卖店', 'https://sandiskyb.world.tmall.com/', 46, 0, '0.00', '0.00', 2, 0, NULL, NULL, NULL, NULL, 0, '2017-07-20 18:59:04', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `shoptrackings`
--

CREATE TABLE `shoptrackings` (
  `ShopTrackingId` int(11) NOT NULL,
  `ShopId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `Tracking` varchar(45) NOT NULL,
  `IsReturn` tinyint(4) NOT NULL,
  `Comment` varchar(250) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shoptrackings`
--

INSERT INTO `shoptrackings` (`ShopTrackingId`, `ShopId`, `OrderId`, `Tracking`, `IsReturn`, `Comment`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(7, 10, 5, '01', 0, '', 21, '2017-06-28 14:23:24', NULL, NULL),
(8, 10, 5, '01', 0, '', 21, '2017-06-28 14:24:03', NULL, NULL),
(9, 50, 35, 'MVD1', 1, 'yhyh', 21, '2017-06-29 20:57:54', 21, '2017-06-29 20:57:57'),
(11, 103, 42, 'MVD2', 0, '', 21, '2017-07-03 16:57:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `SliderId` smallint(6) NOT NULL,
  `SliderName` varchar(250) DEFAULT NULL,
  `SliderImage` varchar(250) NOT NULL,
  `SliderLink` varchar(250) DEFAULT NULL,
  `DisplayOrder` smallint(6) NOT NULL,
  `SliderTypeId` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`SliderId`, `SliderName`, `SliderImage`, `SliderLink`, `DisplayOrder`, `SliderTypeId`) VALUES
(1, 'Nhập hàng 365', 'new_banner1-59b9326e5fb64.png', '', 1, 1),
(2, 'Nhập hàng 365', 'new_banner2-59b932c79769f.png', '', 2, 1),
(3, 'Nhập hàng 365', 'new_banner3-59b93337be6c6.png', '', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sysnotices`
--

CREATE TABLE `sysnotices` (
  `SysNoticeId` int(11) NOT NULL,
  `RoleId` tinyint(4) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `Notice` varchar(650) NOT NULL,
  `NotifyStatusId` tinyint(4) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sysnotices`
--

INSERT INTO `sysnotices` (`SysNoticeId`, `RoleId`, `CustomerId`, `OrderId`, `Notice`, `NotifyStatusId`, `CrDateTime`) VALUES
(1, 1, 6, 11, 'Sản phẩm XYZ đã bị xóa do ABC', 1, '2017-02-09 16:35:52'),
(2, 1, 6, 11, 'Sản phẩm , màu sắc:  - Kích thước:  - Số lượng:  - Đơn giá:  tệ - Tổng tệ: 0.00 - Tỉ giá:  - Thành tiền: 0 tệ đâng chờ duyệt xóa vào lúc 10/02/2017 11:08. Lí do xóa: Xóa lần 2', 1, '2017-02-10 11:08:47'),
(3, 1, 6, 11, 'Sản phẩm , màu sắc:  - Kích thước:  - Số lượng:  - Đơn giá:  tệ - Tổng tệ: 0.00 - Tỉ giá:  - Thành tiền: 0 tệ đâng chờ duyệt xóa vào lúc 10/02/2017 11:09. Lí do xóa: Xóa lần 2', 1, '2017-02-10 11:09:40'),
(4, 1, 6, 11, 'Sản phẩm , màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 168 tệ - Tổng tệ: 168.00 - Tỉ giá: 3399 - Thành tiền: 571032 tệ đâng chờ duyệt xóa vào lúc 10/02/2017 11:11. Lí do xóa: Xóa lần 2', 1, '2017-02-10 11:11:39'),
(5, 1, 6, 11, 'Sản phẩm , màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 168 tệ - Tổng tệ: 168.00 - Tỉ giá: 3399 - Thành tiền: 571032 tệ đâng chờ duyệt xóa vào lúc 10/02/2017 11:12. Lí do xóa: Xóa lần 2', 2, '2017-02-10 11:12:01'),
(6, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221, màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 168 tệ - Tổng tệ: 168.00 - Tỉ giá: 3399 - Thành tiền: 571032 tệ đâng chờ duyệt xóa vào lúc 10/02/2017 11:12. Lí do xóa: Xóa lần 2', 1, '2017-02-10 11:12:36'),
(7, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221 - Màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 168 tệ - Tổng tệ: 168.00 - Tỉ giá: 3399 - Thành tiền: 571032 tệ - đang chờ duyệt xóa vào lúc 10/02/2017 11:22. Lí do xóa: Ko còn hàng', 1, '2017-02-10 11:22:47'),
(8, 1, 5, 1, 'Sản phẩm http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg - Màu sắc: Đỏ - Kích thước: XL - Số lượng: 1 - Đơn giá: 1 tệ - Ship TQ: 20 -  Tổng tệ: 21.00 - Tỉ giá: 3456 - Thành tiền: 72576 tệ - đang chờ duyệt xóa vào lúc 11/02/2017 10:48. Lí do xóa: SP lỗi', 1, '2017-02-11 10:48:24'),
(9, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221 - Màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 168 tệ - Ship TQ: 10 -  Tổng tệ: 178.00 - Tỉ giá: 3399 - Thành tiền: 605022 tệ - đang chờ duyệt xóa vào lúc 11/02/2017 16:05. Lí do xóa: tesst xóa 1', 1, '2017-02-11 16:05:13'),
(10, 1, 5, 1, 'Sản phẩm  - Màu sắc: Đen - Kích thước: M - Số lượng: 2 - Đơn giá: 1 tệ - Ship TQ: 0 -  Tổng tệ: 2.00 - Tỉ giá: 3456 - Thành tiền: 6912 tệ - đang chờ duyệt xóa vào lúc 11/02/2017 16:09. Lí do xóa: test xóa 2', 1, '2017-02-11 16:09:43'),
(11, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221 - Màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 168 tệ - Ship TQ: 0 -  Tổng tệ: 168.00 - Tỉ giá: 3399 - Thành tiền: 571032 tệ - đang chờ duyệt xóa vào lúc 11/02/2017 19:51. Lí do xóa: Xoas', 1, '2017-02-11 19:51:37'),
(12, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221 - Màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 168 tệ - Ship TQ: 10 -  Tổng tệ: 178.00 - Tỉ giá: 3399 - Thành tiền: 605022 tệ - đang chờ duyệt xóa vào lúc 11/02/2017 19:58. Lí do xóa: xoa', 1, '2017-02-11 19:58:20'),
(13, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374 - Màu sắc: vans authentic đỏ - Kích thước: 40.5 - Số lượng: 1 - Đơn giá: 128 tệ - Ship TQ: 0 -  Tổng tệ: 128.00 - Tỉ giá: 3399 - Thành tiền: 435072 tệ - đang chờ duyệt xóa vào lúc 11/02/2017 20:03. Lí do xóa: xoa', 1, '2017-02-11 20:03:48'),
(14, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221 - Màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 0 tệ - Ship TQ: 10 -  Tổng tệ: 10.00 - Tỉ giá: 3399 - Thành tiền: 33990 tệ - đã được duyệt xóa vào lúc 11/02/2017 20:04. Lí do xóa: xoa', 1, '2017-02-11 20:04:39'),
(15, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374 - Màu sắc: vans authentic đỏ - Kích thước: 40.5 - Số lượng: 1 - Đơn giá: 0 tệ - Ship TQ: 0 -  Tổng tệ: 0.00 - Tỉ giá: 3399 - Thành tiền: 0 tệ', 1, '2017-02-11 20:08:31'),
(16, 1, 5, 1, 'Sản phẩm http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg - Màu sắc: Đỏ - Kích thước: XL - Số lượng: 1 - Đơn giá: 1 tệ - Ship TQ: 20 -  Tổng tệ: 21.00 - Tỉ giá: 3456 - Thành tiền: 72576 tệ - đang chờ duyệt xóa vào lúc 11/02/2017 20:09. Lí do xóa: xoa', 1, '2017-02-11 20:09:11'),
(17, 3, 5, 1, 'Sản phẩm http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg - Màu sắc: Đỏ - Kích thước: XL - Số lượng: 1 - Đơn giá: 0 tệ - Ship TQ: 20 -  Tổng tệ: 20.00 - Tỉ giá: 3456 - Thành tiền: 69120 tệ - đã được duyệt xóa vào lúc 11/02/2017 20:09. Lí do xóa: xoa', 2, '2017-02-11 20:09:40'),
(18, 3, 5, 1, 'Sản phẩm http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg - Màu sắc: Đỏ - Kích thước: XL - Số lượng: 1 - Đơn giá: 0 tệ - Ship TQ: 20 -  Tổng tệ: 20.00 - Tỉ giá: 3456 - Thành tiền: 69120 tệ - đã được Khách hàng duyệt xóa vào lúc 13/02/2017 12:15. Lí do xóa: xoa', 1, '2017-02-13 12:15:37'),
(19, 3, 5, 1, 'Sản phẩm http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg - Màu sắc: Đỏ - Kích thước: XL - Số lượng: 1 - Đơn giá: 0 tệ - Ship TQ: 20 -  Tổng tệ: 20.00 - Tỉ giá: 3456 - Thành tiền: 69120 tệ', 1, '2017-02-13 12:17:56'),
(20, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221 - Màu sắc: old đỏ trắng - Kích thước: 40 - Số lượng: 1 - Đơn giá: 168 tệ - Ship TQ: 10 -  Tổng tệ: 178.00 - Tỉ giá: 3399 - Thành tiền: 605022 tệ - đang chờ Khách hàng duyệt xóa vào lúc 19/02/2017 16:14. Lí do xóa: xóa', 1, '2017-02-19 16:14:07'),
(21, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619 - Màu sắc: checker - Kích thước: 43 - đang chờ Khách hàng đồng ý tăng giá vào lúc 25/02/2017 10:16. Lí do: Tăng giá sản phẩm', 1, '2017-02-25 10:16:11'),
(22, 1, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374 - Màu sắc: vans authentic đỏ - Kích thước: 40.5 - đang chờ Khách hàng đồng ý giảm giá vào lúc 25/02/2017 10:19. Lí do: Giảm giá', 1, '2017-02-25 10:19:24'),
(23, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374 - Màu sắc: vans authentic đỏ - Kích thước: 40.5 - được Khách hàng đồng ý giảm giá vào lúc 25/02/2017 14:08. Xác nhận: quá tốt', 1, '2017-02-25 14:08:50'),
(24, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374 - Màu sắc: vans authentic đỏ - Kích thước: 40.5 - được Khách hàng đồng ý giảm giá vào lúc 25/02/2017 14:44. Xác nhận: quá tốt', 1, '2017-02-25 14:44:58'),
(25, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374 - Màu sắc: vans authentic đỏ - Kích thước: 40.5 - được Khách hàng đồng ý giảm giá vào lúc 25/02/2017 15:04. Xác nhận: đồng ý', 1, '2017-02-25 15:04:15'),
(26, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374 - Màu sắc: vans authentic đỏ - Kích thước: 40.5 - được Khách hàng đồng ý giảm giá vào lúc 25/02/2017 15:11. Xác nhận: tốt quá', 1, '2017-02-25 15:11:52'),
(27, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a312a.7700824.w4002-14484666517.64.FdGAda&id=536021727374 - Màu sắc: vans authentic đỏ - Kích thước: 40.5 - được Khách hàng đồng ý giảm giá vào lúc 25/02/2017 15:20. Xác nhận: good', 1, '2017-02-25 15:20:09'),
(28, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.109.DqG4Jw&id=536021859221 - Màu sắc: old đỏ trắng - Kích thước: 40 - được Khách hàng đồng ý xóa vào lúc 25/02/2017 15:21. Xác nhận: ok', 1, '2017-02-25 15:21:39'),
(29, 3, 6, 11, 'Sản phẩm https://item.taobao.com/item.htm?spm=a1z10.3-c.w4002-14484666517.49.DqG4Jw&id=536166736619 - Màu sắc: checker - Kích thước: 43 - bị Khách hàng không đồng ý tăng giá vào lúc 25/02/2017 15:26. Xác nhận: ko đồng ý', 1, '2017-02-25 15:26:39'),
(30, 1, 6, 22, 'Sản phẩm bvv - Màu sắc: cd - Kích thước: f - hết hàng', 1, '2017-04-17 23:02:59'),
(31, 1, 6, 22, 'Sản phẩm bvv - Màu sắc: cd - Kích thước: f - hết hàng', 1, '2017-04-17 23:09:19'),
(32, 1, 6, 22, 'Sản phẩm bvv - Màu sắc: cd - Kích thước: f - hết hàng', 1, '2017-04-17 23:13:51'),
(33, 1, 6, 22, 'Sản phẩm b - Màu sắc: c - Kích thước: d - đang chờ Khách hàng đồng ý tăng giá vào lúc 18/04/2017 08:47. Lí do: tang gia', 1, '2017-04-18 08:47:28'),
(34, 1, 6, 22, 'Sản phẩm b - Màu sắc: c - Kích thước: d - đang chờ Khách hàng đồng ý tăng giá vào lúc 18/04/2017 08:55. Lí do: Tăng giá', 1, '2017-04-18 08:55:45'),
(35, 1, 6, 22, 'Sản phẩm b - Màu sắc: c - Kích thước: d - được Khách hàng đồng ý tăng giá vào lúc 18/04/2017 20:57. Xác nhận: đồng ý tăng giá', 1, '2017-04-18 20:57:15'),
(36, 1, 6, 22, 'Sản phẩm b - Màu sắc: c - Kích thước: d - được Khách hàng đồng ý tăng giá vào lúc 18/04/2017 20:59. Xác nhận: đồng ý tăng giá', 1, '2017-04-18 20:59:37');

-- --------------------------------------------------------

--
-- Table structure for table `trackingcrawls`
--

CREATE TABLE `trackingcrawls` (
  `TrackingCrawlId` int(11) NOT NULL,
  `Tracking` varchar(45) NOT NULL,
  `PackageCount` varchar(45) NOT NULL,
  `ProductTypeName` varchar(45) NOT NULL,
  `TrackingCost` varchar(45) NOT NULL,
  `DateTQ` datetime DEFAULT NULL,
  `DateVN` datetime DEFAULT NULL,
  `DateTQStr` varchar(45) DEFAULT NULL,
  `DateVNStr` varchar(45) DEFAULT NULL,
  `StatusName` varchar(45) NOT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trackingcrawls`
--

INSERT INTO `trackingcrawls` (`TrackingCrawlId`, `Tracking`, `PackageCount`, `ProductTypeName`, `TrackingCost`, `DateTQ`, `DateVN`, `DateTQStr`, `DateVNStr`, `StatusName`, `CrDateTime`) VALUES
(1, '70444903443631', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(2, '71146442117147', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(3, '71158902829706', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(4, '9890789361860', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(5, '1901843492662', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(6, '3999043396195', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(7, '3331152480931', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(8, '3991961671775', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(9, '3987960705689', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(10, '3830881471818', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(11, '402469218264', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(12, '3900188545746', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(13, '3331118535415', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(14, '3985911149103', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59'),
(15, '3982385773452', '1', '', '', '2017-06-08 00:00:00', '0000-00-00 00:00:00', '2017-6-8', '', '已提货', '2017-06-08 12:42:59');

-- --------------------------------------------------------

--
-- Table structure for table `transactionlogs`
--

CREATE TABLE `transactionlogs` (
  `TransactionLogId` int(11) NOT NULL,
  `TransactionId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `ComplaintId` int(11) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `TransactionTypeId` tinyint(4) NOT NULL,
  `TransactionRangeId` tinyint(4) NOT NULL,
  `WarehouseId` smallint(6) NOT NULL,
  `MoneySourceId` smallint(6) NOT NULL,
  `Comment` varchar(250) DEFAULT NULL,
  `Confirm` varchar(650) DEFAULT NULL,
  `PaidTQ` decimal(10,2) DEFAULT NULL,
  `ExchangeRate` int(11) NOT NULL,
  `PaidVN` int(11) NOT NULL,
  `PaidDateTime` datetime NOT NULL,
  `Balance` int(11) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactionlogs`
--

INSERT INTO `transactionlogs` (`TransactionLogId`, `TransactionId`, `CustomerId`, `OrderId`, `ComplaintId`, `StatusId`, `TransactionTypeId`, `TransactionRangeId`, `WarehouseId`, `MoneySourceId`, `Comment`, `Confirm`, `PaidTQ`, `ExchangeRate`, `PaidVN`, `PaidDateTime`, `Balance`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(3, 1, 0, 0, 0, 1, 1, 1, 1, 2, 'ghi chú', NULL, NULL, 3456, 345600000, '2017-01-08 16:25:38', 0, 1, '2017-01-08 16:25:38', 1, '2017-01-08 16:39:10'),
(4, 2, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, '100.00', 3456, 345600, '2017-01-08 16:41:13', 0, 1, '2017-01-08 16:41:13', 1, '2017-01-08 16:41:27'),
(5, 2, 0, 0, 0, 2, 1, 1, 1, 1, NULL, NULL, '100.00', 3456, 345600, '2017-01-08 16:41:13', 0, 1, '2017-01-08 16:41:13', 1, '2017-01-08 16:41:49'),
(6, 3, 5, 1, 1, 1, 2, 3, 1, 1, 'đền tiền khiếu nại', NULL, '20.00', 3456, 69120, '2017-02-08 09:15:42', 0, 1, '2017-02-08 09:15:42', 1, '2017-02-08 10:03:25'),
(7, 4, 5, 1, 0, 1, 2, 3, 1, 1, 'Sản phẩm http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg - Màu sắc: Đỏ - Kích thước: XL - Số lượng: 1 - Đơn giá: 0 tệ - Ship TQ: 20 -  Tổng tệ: 20.00 - Tỉ giá: 3456 - Thành tiền: 69120 tệ - đã được duyệt xóa vào lúc 11/02/2017 20:09. Lí ', NULL, NULL, 3456, 69120, '2017-02-11 20:09:40', 0, 1, '2017-02-11 20:09:40', 1, '2017-02-11 20:13:01'),
(8, 4, 5, 1, 0, 2, 2, 3, 1, 1, 'Sản phẩm http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg - Màu sắc: Đỏ - Kích thước: XL - Số lượng: 1 - Đơn giá: 0 tệ - Ship TQ: 20 -  Tổng tệ: 20.00 - Tỉ giá: 3456 - Thành tiền: 69120 tệ - đã được duyệt xóa vào lúc 11/02/2017 20:09. Lí ', NULL, NULL, 3456, 69120, '2017-02-11 20:09:40', 0, 1, '2017-02-11 20:09:40', 1, '2017-02-11 20:13:21'),
(9, 4, 5, 1, 0, 1, 2, 3, 1, 1, 'Sản phẩm http://vanta.vn/wp-content/uploads/2015/08/thoi-trang-nu.jpg - Màu sắc: Đỏ - Kích thước: XL - Số lượng: 1 - Đơn giá: 0 tệ - Ship TQ: 20 -  Tổng tệ: 20.00 - Tỉ giá: 3456 - Thành tiền: 69120 tệ - đã được duyệt xóa vào lúc 11/02/2017 20:09. Lí ', NULL, NULL, 3456, 69120, '2017-02-11 20:09:40', 0, 1, '2017-02-11 20:09:40', 1, '2017-02-11 20:46:38'),
(10, 8, 0, 0, 0, 1, 1, 1, 1, 1, 'Thu tien', NULL, NULL, 1, 1000000, '2017-03-18 09:39:04', 0, 1, '2017-03-18 09:39:04', 1, '2017-03-18 15:41:55'),
(11, 9, 5, 2, 0, 1, 1, 1, 1, 1, 'Thu tien don hang #2', NULL, NULL, 1, 2000000, '2017-03-26 18:46:25', 0, 1, '2017-03-26 18:46:25', 1, '2017-03-26 19:28:02'),
(12, 9, 5, 2, 0, 1, 1, 1, 1, 1, 'Thu tien don hang #2', NULL, NULL, 1, 2000000, '2017-03-26 18:46:25', 0, 1, '2017-03-26 18:46:25', 1, '2017-03-26 19:38:27'),
(13, 9, 5, 2, 0, 1, 1, 1, 1, 1, 'Thu tien don hang #2', NULL, NULL, 1, 2000000, '2017-03-26 18:46:25', 0, 1, '2017-03-26 18:46:25', 1, '2017-03-26 19:44:33'),
(14, 11, 6, 22, 0, 1, 1, 1, 1, 1, 'don 22', NULL, NULL, 1, 2145000, '2017-04-17 22:36:03', 0, 1, '2017-04-17 22:36:03', 1, '2017-04-17 22:41:50'),
(15, 16, 5, 0, 0, 1, 1, 1, 1, 1, 'AA', NULL, NULL, 1, 10000, '2017-04-26 12:40:38', 0, 1, '2017-04-26 12:40:38', 1, '2017-04-26 12:42:43'),
(16, 3, 6, 28, 0, 1, 1, 1, 1, 1, 'Thanh toan #28', NULL, NULL, 1, 1100000, '2017-05-04 11:03:20', 0, 1, '2017-05-04 11:03:20', 1, '2017-05-04 11:03:28'),
(17, 9, 0, 0, 0, 1, 1, 1, 1, 1, 'abvx', NULL, NULL, 1, 1000000, '2017-05-25 16:32:17', 0, 1, '2017-05-25 16:32:17', 2, '2017-05-25 16:36:55'),
(18, 9, 3, 0, 0, 1, 1, 1, 0, 0, NULL, NULL, NULL, 1, 100000, '2017-05-25 16:32:17', 0, 1, '2017-05-25 16:32:17', 2, '2017-05-25 16:56:26'),
(19, 9, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 100000, '2017-05-25 16:32:17', 0, 1, '2017-05-25 16:32:17', 2, '2017-05-25 16:57:33'),
(20, 19, 6, 0, 0, 1, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 0, 2, '2017-05-27 08:28:32', 2, '2017-05-27 08:28:50'),
(21, 18, 6, 33, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 100000, '2017-05-27 08:27:45', 0, 2, '2017-05-27 08:27:45', 2, '2017-05-27 08:29:13'),
(22, 17, 6, 28, 0, 1, 1, 1, 1, 1, 'xxxxxxxxxxxxxx', NULL, NULL, 1, 100000, '2017-05-26 14:03:23', 0, 2, '2017-05-26 14:03:23', 2, '2017-05-27 08:49:47'),
(23, 19, 6, 0, 0, 1, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 135552, 2, '2017-05-27 08:28:32', 2, '2017-05-27 09:04:07'),
(24, 19, 6, 0, 0, 2, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 435552, 2, '2017-05-27 08:28:32', 2, '2017-05-27 09:04:13'),
(25, 19, 6, 0, 0, 1, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 0, 2, '2017-05-27 08:28:32', 2, '2017-05-27 09:07:04'),
(26, 18, 6, 33, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 100000, '2017-05-27 08:27:45', 235552, 2, '2017-05-27 08:27:45', 1, '2017-05-27 09:15:57'),
(27, 19, 6, 0, 0, 1, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 535552, 2, '2017-05-27 08:28:32', 1, '2017-05-27 09:29:48'),
(28, 19, 6, 33, 0, 1, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 0, 2, '2017-05-27 08:28:32', 1, '2017-05-27 09:37:23'),
(29, 19, 6, 33, 0, 1, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 0, 2, '2017-05-27 08:28:32', 1, '2017-05-27 09:39:06'),
(30, 19, 6, 33, 0, 1, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 0, 2, '2017-05-27 08:28:32', 1, '2017-05-27 09:45:35'),
(31, 19, 6, 33, 0, 1, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', NULL, NULL, 1, 100000, '2017-05-27 08:28:32', 0, 2, '2017-05-27 08:28:32', 1, '2017-05-27 09:48:12'),
(32, 18, 6, 0, 0, 1, 1, 1, 1, 1, 'ff', 'gv', NULL, 1, 1000000, '2017-05-27 08:27:45', 1535552, 2, '2017-05-27 08:27:45', 1, '2017-05-27 09:49:13'),
(33, 17, 6, 28, 0, 1, 1, 1, 1, 1, 'xxxxxxxxxxxxxx', NULL, NULL, 1, 100000, '2017-05-26 14:03:23', 335552, 2, '2017-05-26 14:03:23', 1, '2017-05-27 09:49:19'),
(34, 17, 6, 28, 0, 1, 1, 1, 1, 1, 'xxxxxxxxxxxxxx', NULL, NULL, 1, 100000, '2017-05-26 14:03:23', 0, 2, '2017-05-26 14:03:23', 1, '2017-05-27 09:49:26'),
(35, 9, 3, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 100000, '2017-05-25 16:32:17', 0, 1, '2017-05-25 16:32:17', 1, '2017-06-01 09:40:32'),
(36, 14, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1100, '2017-05-26 13:53:09', 0, 2, '2017-05-26 13:53:09', 1, '2017-06-02 12:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `TransactionId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `OrderId` int(11) NOT NULL,
  `ComplaintId` int(11) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `TransactionTypeId` tinyint(4) NOT NULL,
  `TransactionRangeId` tinyint(4) NOT NULL,
  `WarehouseId` smallint(6) NOT NULL,
  `MoneySourceId` smallint(6) NOT NULL,
  `Comment` varchar(650) DEFAULT NULL,
  `Confirm` varchar(650) DEFAULT NULL,
  `PaidTQ` decimal(10,2) DEFAULT NULL,
  `ExchangeRate` int(11) NOT NULL,
  `PaidVN` int(11) NOT NULL,
  `PaidDateTime` datetime NOT NULL,
  `Balance` int(11) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`TransactionId`, `CustomerId`, `OrderId`, `ComplaintId`, `StatusId`, `TransactionTypeId`, `TransactionRangeId`, `WarehouseId`, `MoneySourceId`, `Comment`, `Confirm`, `PaidTQ`, `ExchangeRate`, `PaidVN`, `PaidDateTime`, `Balance`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(2, 6, 28, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng #28', '', '289.00', 3456, 1099008, '2017-05-04 11:01:42', -1099008, 1, '2017-05-04 11:01:42', NULL, NULL),
(3, 6, 28, 0, 2, 1, 1, 1, 1, 'Thanh toan #28', NULL, NULL, 1, 1100000, '2017-05-04 11:03:20', 992, 1, '2017-05-04 11:03:20', 1, '2017-05-04 11:03:28'),
(4, 6, 28, 0, 2, 2, 3, 1, 1, 'Giam gia', NULL, '10.00', 3456, 34560, '2017-05-07 17:58:40', 35552, 1, '2017-05-07 17:58:40', NULL, NULL),
(5, 5, 0, 0, 2, 3, 2, 1, 1, 'Ghi nợ tiền vận chuyển #1', NULL, NULL, 1, 60000, '2017-05-25 11:15:51', -60000, 1, '2017-05-25 11:15:51', NULL, NULL),
(6, 5, 0, 0, 2, 1, 2, 1, 1, 'Thanh toán tiền vận chuyển #1', NULL, NULL, 1, 60000, '2017-05-25 11:22:54', 0, 1, '2017-05-25 11:15:51', NULL, NULL),
(7, 5, 0, 0, 2, 3, 2, 1, 1, 'Ghi nợ tiền vận chuyển #2', '', '0.00', 1, 20000, '2017-05-25 11:25:44', -20000, 1, '2017-05-25 11:25:44', NULL, NULL),
(8, 5, 0, 0, 2, 1, 2, 1, 1, 'Thanh toàn tiền vận chuyển #2', '', '0.00', 1, 20000, '2017-05-25 11:25:44', 0, 1, '2017-05-25 11:25:44', NULL, NULL),
(9, 3, 0, 0, 3, 1, 1, 1, 1, NULL, NULL, NULL, 1, 100000, '2017-05-25 16:32:17', 0, 1, '2017-05-25 16:32:17', 1, '2017-06-01 09:40:32'),
(10, 3, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 1000, '2017-05-26 13:48:46', 0, 2, '2017-05-26 13:48:46', NULL, NULL),
(11, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 4111, '2017-05-26 13:50:35', 0, 2, '2017-05-26 13:50:35', NULL, NULL),
(12, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 410000, '2017-05-26 13:52:46', 0, 2, '2017-05-26 13:52:46', NULL, NULL),
(13, 0, 0, 0, 1, 1, 1, 1, 1, NULL, NULL, NULL, 1, 410000, '2017-05-26 13:52:54', 0, 2, '2017-05-26 13:52:54', NULL, NULL),
(17, 6, 28, 0, 2, 1, 1, 1, 1, 'xxxxxxxxxxxxxx', NULL, NULL, 1, 100000, '2017-05-26 14:03:23', 2735552, 2, '2017-05-26 14:03:23', 1, '2017-05-27 09:49:26'),
(18, 6, 0, 0, 2, 1, 1, 1, 1, 'ff', 'gv', NULL, 1, 1000000, '2017-05-27 08:27:45', 2635552, 2, '2017-05-27 08:27:45', 1, '2017-05-27 09:49:13'),
(19, 6, 33, 0, 2, 1, 1, 1, 1, 'aaaaaaaaaaaaaaaa', 'gbbbbbbbbb', NULL, 1, 100000, '2017-05-27 08:28:32', 1635552, 2, '2017-05-27 08:28:32', 1, '2017-05-27 09:48:12'),
(20, 3, 0, 0, 2, 3, 2, 1, 1, 'Ghi nợ tiền vận chuyển #3', NULL, NULL, 1, 130000, '2017-05-29 10:58:38', -130000, 1, '2017-05-29 10:58:38', NULL, NULL),
(21, 3, 0, 0, 2, 3, 2, 1, 1, 'Ghi nợ tiền vận chuyển #4', NULL, NULL, 1, 25500, '2017-05-29 11:00:37', -155500, 1, '2017-05-29 11:00:37', NULL, NULL),
(22, 3, 0, 0, 2, 3, 2, 1, 1, 'Ghi nợ tiền vận chuyển #5', NULL, NULL, 1, 250000, '2017-05-29 11:01:40', -405500, 1, '2017-05-29 11:01:40', NULL, NULL),
(23, 6, 33, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng #33', '', '0.00', 3456, 185898, '2017-05-29 17:49:04', 2549654, 1, '2017-05-29 17:49:04', NULL, NULL),
(24, 6, 32, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng #32', '', '0.00', 3456, 185898, '2017-06-01 12:13:34', 2363756, 1, '2017-06-01 12:13:34', NULL, NULL),
(25, 6, 32, 0, 2, 1, 1, 1, 1, 'Ghi nợ đơn hàng #32', '', '0.00', 3456, 185898, '2017-06-01 12:18:34', 2549654, 1, '2017-06-01 12:18:34', NULL, NULL),
(26, 6, 32, 0, 2, 1, 1, 1, 1, 'Hoàn tiền đơn hàng ', '', '0.00', 3456, 185898, '2017-06-01 12:26:13', 2735552, 1, '2017-06-01 12:26:13', NULL, NULL),
(27, 6, 32, 0, 2, 1, 1, 1, 1, 'Hoàn tiền đơn hàng ', '', '0.00', 3456, 185898, '2017-06-01 12:28:07', 2921450, 1, '2017-06-01 12:28:07', NULL, NULL),
(28, 6, 32, 0, 2, 1, 1, 1, 1, 'Hoàn tiền đơn hàng D270510032.', '', '0.00', 3456, 185898, '2017-06-01 12:32:09', 3107348, 1, '2017-06-01 12:32:09', NULL, NULL),
(29, 6, 31, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng D270510031', '', '0.00', 3456, 185898, '2017-06-01 22:36:11', 2921450, 1, '2017-06-01 22:36:11', NULL, NULL),
(30, 6, 35, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng D020610035', '', '0.00', 3347, 5302461, '2017-06-02 22:37:23', -2381011, 1, '2017-06-02 22:37:23', NULL, NULL),
(31, 6, 35, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng D020610035', '', '0.00', 3347, 5302461, '2017-06-02 22:40:29', -7683472, 1, '2017-06-02 22:40:29', NULL, NULL),
(32, 6, 35, 0, 1, 2, 4, 1, 1, 'Đơn hàng D020610035 được khách hàng chấp nhận tăng giá - Xác nhận: "Đồng ý"', NULL, '10.00', 3347, 34140, '2017-06-04 16:46:26', 0, 6, '2017-06-04 16:46:26', NULL, NULL),
(33, 6, 35, 0, 1, 1, 3, 1, 1, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '58.00', 3347, 198009, '2017-06-04 16:54:30', 0, 1, '2017-06-04 16:54:30', NULL, NULL),
(34, 6, 35, 0, 1, 1, 3, 1, 1, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '58.00', 3347, 198009, '2017-06-04 17:00:56', 0, 1, '2017-06-04 17:00:56', NULL, NULL),
(35, 6, 35, 0, 1, 1, 3, 1, 1, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '58.00', 3347, 198009, '2017-06-04 17:12:07', 0, 1, '2017-06-04 17:12:07', NULL, NULL),
(36, 3, 0, 0, 2, 3, 2, 1, 1, 'Ghi nợ tiền vận chuyển #6', '', '0.00', 1, 29000, '2017-06-06 14:43:34', -434500, 1, '2017-06-06 14:43:34', NULL, NULL),
(37, 3, 0, 0, 2, 1, 2, 1, 1, 'Thanh toàn tiền vận chuyển #6', '', '0.00', 1, 29000, '2017-06-06 14:43:34', -405500, 1, '2017-06-06 14:43:34', NULL, NULL),
(38, 3, 0, 0, 2, 1, 2, 1, 2, 'Thanh toán tiền vận chuyển #5', NULL, NULL, 1, 250000, '2017-06-06 15:48:42', -155500, 1, '2017-06-06 15:48:42', NULL, NULL),
(39, 3, 0, 0, 2, 1, 2, 1, 2, 'Thanh toán tiền vận chuyển #4', NULL, NULL, 1, 25500, '2017-06-06 15:59:57', -130000, 1, '2017-06-06 15:59:57', NULL, NULL),
(40, 3, 0, 0, 2, 1, 2, 1, 1, 'Thanh toán tiền vận chuyển #3', NULL, NULL, 1, 130000, '2017-06-06 16:00:37', 0, 1, '2017-06-06 16:00:37', NULL, NULL),
(41, 3, 0, 0, 2, 3, 2, 1, 4, 'Ghi nợ tiền vận chuyển #7', NULL, NULL, 1, 29000, '2017-06-06 17:45:21', -29000, 1, '2017-06-06 17:45:21', NULL, NULL),
(42, 6, 33, 0, 1, 1, 3, 1, 1, 'Đơn hàng D270510033 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '48.90', 3456, 185899, '2017-06-08 16:38:05', 0, 1, '2017-06-08 16:38:05', NULL, NULL),
(43, 6, 35, 0, 1, 1, 3, 1, 1, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '29.80', 3347, 101736, '2017-06-08 17:10:01', 0, 1, '2017-06-08 17:10:01', NULL, NULL),
(44, 6, 35, 0, 1, 1, 3, 1, 1, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '45.54', 3347, 155472, '2017-06-08 17:17:18', 0, 1, '2017-06-08 17:17:18', NULL, NULL),
(45, 6, 35, 0, 1, 2, 4, 1, 1, 'Đơn hàng D020610035 được khách hàng chấp nhận tăng giá shop HV_15 - Xác nhận: "ok"', NULL, '3.00', 3347, 10242, '2017-06-11 19:45:44', 0, 6, '2017-06-11 19:45:44', NULL, NULL),
(46, 6, 35, 0, 1, 1, 3, 1, 1, 'Đơn hàng D020610035 khách hàng KHÔNG chấp nhận tăng giá shop HV_7 - Xác nhận: "ko ok"', NULL, '207.00', 3347, 706686, '2017-06-11 20:18:42', 0, 6, '2017-06-11 20:18:42', NULL, NULL),
(47, 5, 36, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng D110610036', '', '0.00', 3456, 6600027, '2017-06-17 11:23:07', -6600027, 1, '2017-06-17 11:23:07', NULL, NULL),
(48, 5, 36, 0, 1, 1, 3, 1, 1, 'Đơn hàng D110610036 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '62.00', 3456, 224986, '2017-06-20 10:08:13', 0, 1, '2017-06-20 10:08:13', NULL, NULL),
(49, 5, 36, 0, 1, 1, 3, 1, 1, 'Đơn hàng D110610036 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '62.00', 3456, 224986, '2017-06-20 10:25:28', 0, 1, '2017-06-20 10:25:28', NULL, NULL),
(50, 5, 5, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng D291010005', '', '0.00', 3456, 539827, '2017-06-28 13:58:46', -7139854, 1, '2017-06-28 13:58:46', NULL, NULL),
(51, 5, 5, 0, 1, 1, 3, 1, 1, 'Đơn hàng D291010005 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '11.00', 3456, 41818, '2017-06-28 14:20:11', 0, 21, '2017-06-28 14:20:11', NULL, NULL),
(53, 6, 35, 0, 1, 1, 3, 1, 1, 'Đơn hàng D020610035 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '38.61', 3347, 131813, '2017-06-30 16:24:11', 0, 21, '2017-06-30 16:24:11', NULL, NULL),
(54, 3, 42, 0, 2, 3, 1, 1, 1, 'Ghi nợ đơn hàng D030710042', '', '0.00', 3347, 331353, '2017-07-03 16:53:42', -360353, 1, '2017-07-03 16:53:42', NULL, NULL),
(55, 3, 42, 0, 1, 1, 3, 1, 1, 'Đơn hàng D030710042 của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.', NULL, '88.00', 3347, 323990, '2017-07-03 16:56:12', 0, 21, '2017-07-03 16:56:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transportfeelogs`
--

CREATE TABLE `transportfeelogs` (
  `TransportFeeLogId` int(11) NOT NULL,
  `TransportFeeId` smallint(6) NOT NULL,
  `WarehouseId` smallint(6) NOT NULL,
  `BeginWeight` smallint(6) NOT NULL,
  `EndWeight` smallint(6) NOT NULL,
  `WeightCost` int(11) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transportfeelogs`
--

INSERT INTO `transportfeelogs` (`TransportFeeLogId`, `TransportFeeId`, `WarehouseId`, `BeginWeight`, `EndWeight`, `WeightCost`, `StatusId`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 1, 1, 1, 5, 29000, 2, 1, '2017-05-29 12:57:44', 1, '2017-05-29 12:59:58'),
(2, 1, 1, 1, 5, 29000, 2, 1, '2017-05-29 12:57:44', 1, '2017-05-29 13:01:09'),
(3, 1, 1, 1, 5, 29000, 2, 1, '2017-05-29 12:57:44', 1, '2017-05-29 13:02:01'),
(4, 4, 1, 200, 1000, 21000, 2, 1, '2017-05-29 13:04:31', 1, '2017-08-01 20:58:30'),
(5, 5, 2, 1, 5, 45000, 2, 1, '2017-05-29 13:04:54', 1, '2017-08-01 20:58:48');

-- --------------------------------------------------------

--
-- Table structure for table `transportfees`
--

CREATE TABLE `transportfees` (
  `TransportFeeId` smallint(6) NOT NULL,
  `WarehouseId` smallint(6) NOT NULL,
  `BeginWeight` smallint(6) NOT NULL,
  `EndWeight` smallint(6) NOT NULL,
  `WeightCost` int(11) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transportfees`
--

INSERT INTO `transportfees` (`TransportFeeId`, `WarehouseId`, `BeginWeight`, `EndWeight`, `WeightCost`, `StatusId`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 1, 1, 5, 29000, 2, 1, '2017-05-29 12:57:44', 1, '2017-05-29 13:02:01'),
(2, 1, 5, 20, 27000, 2, 1, '2017-05-29 13:01:51', NULL, NULL),
(3, 1, 20, 200, 23000, 2, 1, '2017-05-29 13:03:27', NULL, NULL),
(4, 1, 200, 200, 21000, 2, 1, '2017-05-29 13:04:31', 1, '2017-08-01 20:58:30'),
(5, 2, 10, 500, 45000, 2, 1, '2017-05-29 13:04:54', 1, '2017-08-01 20:58:48'),
(6, 2, 5, 20, 43000, 2, 1, '2017-05-29 13:05:17', NULL, NULL),
(7, 2, 20, 200, 39000, 2, 1, '2017-05-29 13:05:38', NULL, NULL),
(8, 2, 200, 1000, 37000, 2, 1, '2017-05-29 13:06:01', NULL, NULL),
(9, 1, 32767, 5435, 5345345, 2, 1, '2017-08-01 12:15:17', NULL, NULL),
(10, 1, 4324, 32767, 432423, 2, 1, '2017-08-01 15:33:31', NULL, NULL),
(11, 1, 4324, 423, 423432, 2, 1, '2017-08-01 15:33:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transports`
--

CREATE TABLE `transports` (
  `TransportId` int(11) NOT NULL,
  `CustomerId` int(11) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `WarehouseId` smallint(6) NOT NULL,
  `MoneySourceId` smallint(6) NOT NULL,
  `PackageCount` smallint(6) NOT NULL,
  `Weight` decimal(10,2) NOT NULL,
  `WeightCost` int(11) NOT NULL,
  `OtherCost` int(11) NOT NULL,
  `TransportReasonId` tinyint(4) NOT NULL,
  `PaidVN` int(11) NOT NULL,
  `TransportUserId` smallint(6) NOT NULL DEFAULT '0',
  `TransportTypeId` smallint(6) NOT NULL DEFAULT '0',
  `Comment` varchar(650) DEFAULT NULL,
  `PaidDateTime` datetime NOT NULL,
  `CrUserId` int(11) NOT NULL,
  `CrDateTime` datetime NOT NULL,
  `UpdateUserId` int(11) DEFAULT NULL,
  `UpdateDateTime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transports`
--

INSERT INTO `transports` (`TransportId`, `CustomerId`, `StatusId`, `WarehouseId`, `MoneySourceId`, `PackageCount`, `Weight`, `WeightCost`, `OtherCost`, `TransportReasonId`, `PaidVN`, `TransportUserId`, `TransportTypeId`, `Comment`, `PaidDateTime`, `CrUserId`, `CrDateTime`, `UpdateUserId`, `UpdateDateTime`) VALUES
(1, 5, 2, 1, 1, 10, '2.00', 25000, 10000, 1, 60000, 0, 0, 'Thu tien VC', '2017-05-25 11:15:51', 1, '2017-05-25 11:15:51', 1, '2017-05-25 11:22:54'),
(2, 5, 2, 1, 1, 1, '1.00', 20000, 0, 1, 20000, 0, 0, 'VC 2', '2017-05-25 11:25:44', 1, '2017-05-25 11:25:44', NULL, NULL),
(3, 3, 2, 1, 1, 10, '2.00', 20000, 100000, 1, 130000, 0, 0, 'bgbbv', '2017-05-29 10:58:38', 1, '2017-05-29 10:58:38', 1, '2017-06-06 16:00:37'),
(4, 3, 2, 1, 2, 5, '5.00', 5000, 500, 4, 25500, 2, 0, 'bvvvvvvvvvvv', '2017-05-29 11:00:37', 1, '2017-05-29 11:00:37', 1, '2017-06-06 16:00:20'),
(5, 3, 2, 1, 2, 10, '2.50', 100000, 0, 3, 250000, 2, 0, 'hdcccccccccccc', '2017-05-29 11:01:40', 1, '2017-05-29 11:01:40', 1, '2017-06-06 15:53:46'),
(6, 3, 2, 1, 4, 10, '1.00', 29000, 0, 3, 29000, 2, 2, 'fvvv', '2017-06-06 15:39:25', 1, '2017-06-06 15:39:25', 1, '2017-06-06 17:44:37'),
(7, 3, 1, 1, 4, 1, '1.00', 29000, 0, 2, 29000, 2, 4, 'fgfg', '2017-06-06 17:45:21', 1, '2017-06-06 17:45:21', 1, '2017-06-06 17:45:51');

-- --------------------------------------------------------

--
-- Table structure for table `transporttypes`
--

CREATE TABLE `transporttypes` (
  `TransportTypeId` smallint(6) NOT NULL,
  `TransportTypeName` varchar(250) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `ParentTransportTypeId` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transporttypes`
--

INSERT INTO `transporttypes` (`TransportTypeId`, `TransportTypeName`, `StatusId`, `ParentTransportTypeId`) VALUES
(1, 'Chuyển phát nhanh CPN', 2, 0),
(2, 'Gửi xe', 2, 0),
(3, 'Tự giao', 2, 0),
(4, 'Viettel post', 2, 1),
(5, 'Pro ship', 2, 1),
(6, 'Gửi tại bến', 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `transportusers`
--

CREATE TABLE `transportusers` (
  `TransportUserId` smallint(6) NOT NULL,
  `TransportUserName` varchar(250) NOT NULL,
  `StatusId` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `transportusers`
--

INSERT INTO `transportusers` (`TransportUserId`, `TransportUserName`, `StatusId`) VALUES
(1, 'Hoàn Mưa Đá', 0),
(2, 'NV Giao hàng', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `UserPass` varchar(100) NOT NULL,
  `FullName` varchar(250) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `RoleId` tinyint(4) NOT NULL,
  `GenderId` tinyint(4) NOT NULL,
  `StatusId` tinyint(4) NOT NULL,
  `ProvinceId` tinyint(4) NOT NULL,
  `Address` varchar(250) NOT NULL,
  `PhoneNumber` varchar(15) NOT NULL,
  `Avatar` varchar(100) DEFAULT NULL,
  `Token` varchar(15) DEFAULT NULL,
  `Configs` text,
  `Comment` varchar(250) DEFAULT NULL,
  `CareStaffId` int(11) DEFAULT '0',
  `CrUserId` int(11) DEFAULT NULL,
  `CrDateTime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserId`, `UserName`, `UserPass`, `FullName`, `Email`, `RoleId`, `GenderId`, `StatusId`, `ProvinceId`, `Address`, `PhoneNumber`, `Avatar`, `Token`, `Configs`, `Comment`, `CareStaffId`, `CrUserId`, `CrDateTime`) VALUES
(1, 'hoanmuada', '1ec78e9264de8633d9455cf9c521fb32', 'Lê Văn Hoàn', 'levanhoanhtt@gmail.com', 3, 1, 2, 54, 'Hà Trung', '01669136318', 'hoanmuada.jpg', NULL, '{"BankAccountNumber":"","BankName1":"","BankAccountName":"","BankName2":""}', NULL, 0, 1, '2017-09-12 20:57:56'),
(2, 'cskh', 'e10adc3949ba59abbe56e057f20f883e', 'CSKH To', '0963482242@gmail.com', 1, 1, 2, 24, '1/69 Nguyễn Phúc Lai,Ô chợ dừa đống đa hà nội,Hà Nội', '0963482242', 'no-image.jpg', NULL, NULL, NULL, 0, 1, '2016-10-02 16:50:36'),
(3, '0903210339', 'e10adc3949ba59abbe56e057f20f883e', 'Cao Xuân Hùng', '0903210339@gmail.com', 4, 1, 2, 24, 'Khách qua lấy,Hà Nội', '0903210339', 'no-image.jpg', NULL, NULL, NULL, 2, 1, '2016-10-02 16:56:23'),
(4, '0945426663', 'e10adc3949ba59abbe56e057f20f883e', 'Nguyễn Ánh Tuyết', '0945426663@gmail.com', 4, 2, 2, 24, '155B Hoàng Văn Thái, HN,Hà Nội', '0945426663', 'no-image.jpg', NULL, NULL, NULL, 0, 1, '2016-10-02 16:59:34'),
(5, '0962774396', 'e10adc3949ba59abbe56e057f20f883e', 'Phạm Thị Thu', 'laptrinhphamthu@gmail.com', 4, 2, 2, 24, 'Hà Đông, Hà Nội', '0962774396', 'no-image.png', NULL, '{"BankAccountNumber":"","BankName1":"VCB","BankAccountName":"Ph\\u1ea1m Th\\u1ecb Thu","BankName2":"VCB","FullName0":""}', 'ghi chú', 2, 5, '2017-05-12 10:12:08'),
(6, '0985867245', 'e10adc3949ba59abbe56e057f20f883e', 'Đoàn Xuân Phúc', 'phucdx16@wru.vn', 4, 1, 2, 24, 'Ngõ 22 Tôn Thất Tùng', '0985867245', NULL, NULL, NULL, NULL, 0, NULL, '2016-11-14 13:15:37'),
(7, '789', 'e10adc3949ba59abbe56e057f20f883e', 'Tesst 01', '', 4, 1, 2, 24, 'Ha Noi', '789', 'hoanmuada.jpg', NULL, '{"PhoneNumber1":"123","PhoneNumber2":"456"}', 'ghi chus', 0, 1, '2016-12-03 11:09:11'),
(8, '1234', 'e10adc3949ba59abbe56e057f20f883e', 'Test 02', '', 4, 1, 2, 24, 'Ha Noi', '1234', NULL, NULL, NULL, NULL, 2, 0, '2016-12-03 12:45:45'),
(9, '12345888', 'e10adc3949ba59abbe56e057f20f883e', 'Tesst 03', '', 4, 1, 2, 24, 'cv', '12345888', 'no-image.jpg', NULL, NULL, NULL, 0, 1, '2016-12-06 16:46:37'),
(10, '123456', 'e10adc3949ba59abbe56e057f20f883e', 'Test 04', '123456@gmail.com', 4, 1, 2, 54, 'Nguyen Thai Hoc Street, Ha Trung', '123456', NULL, 'ad85e38d61c4d60', NULL, NULL, 2, 0, '2017-01-14 19:18:09'),
(13, '0123456789', 'e10adc3949ba59abbe56e057f20f883e', 'NV Vận Chuyển', '', 2, 1, 2, 24, 'Ha Noi', '0123456789', 'no-image.png', NULL, NULL, NULL, 0, 1, '2017-04-25 15:29:36'),
(14, 'cskh1', 'e10adc3949ba59abbe56e057f20f883e', 'CSKH 1', '', 1, 1, 2, 24, 'Ha Noi', '1', NULL, NULL, NULL, NULL, 0, 1, '2017-04-25 15:29:36'),
(15, 'cskh2', 'e10adc3949ba59abbe56e057f20f883e', 'CSKH 2', '', 1, 1, 2, 24, 'Ha Noi', '1', NULL, NULL, NULL, NULL, 0, 1, '2017-04-25 15:29:36'),
(16, 'cskh3', 'e10adc3949ba59abbe56e057f20f883e', 'CSKH 3', '', 1, 1, 2, 24, 'Ha Noi', '1', NULL, NULL, NULL, NULL, 0, 1, '2017-04-25 15:29:36'),
(17, 'cskh4', 'e10adc3949ba59abbe56e057f20f883e', 'CSKH 4', '', 1, 1, 2, 24, 'Ha Noi', '1', NULL, NULL, NULL, NULL, 0, 1, '2017-04-25 15:29:36'),
(18, 'cskh5', 'e10adc3949ba59abbe56e057f20f883e', 'CSKH 5', '', 1, 1, 2, 24, 'Ha Noi', '1', NULL, NULL, NULL, NULL, 0, 1, '2017-04-25 15:29:36'),
(19, 'nvdh1', 'e10adc3949ba59abbe56e057f20f883e', 'NVDH1', '', 5, 1, 2, 24, 'Hà Nội', 'nvdh1', 'no-image.png', NULL, NULL, NULL, 0, 1, '2017-06-03 14:50:10'),
(20, 'ketoan', 'e10adc3949ba59abbe56e057f20f883e', 'Kế toán', '', 6, 1, 2, 24, 'dcfd', 'ketoan', 'no-image.png', NULL, NULL, NULL, 0, 1, '2017-06-08 16:35:11'),
(21, 'qldh', 'e10adc3949ba59abbe56e057f20f883e', 'qldh', '', 8, 1, 2, 24, 'fg,f', '0123456788', 'no-image.png', NULL, NULL, NULL, 0, 1, '2017-06-17 16:14:00'),
(22, '0976653494', 'e10adc3949ba59abbe56e057f20f883e', 'Tạ Duy Hoàng', 'duyhoangptit@gmail.com', 4, 1, 2, 24, 'HH', '0976653494', 'no-image.png', NULL, NULL, NULL, 0, 0, '2017-08-19 23:13:49');

-- --------------------------------------------------------

--
-- Table structure for table `warehouses`
--

CREATE TABLE `warehouses` (
  `WarehouseId` smallint(6) NOT NULL,
  `WarehouseName` varchar(250) NOT NULL,
  `StatusId` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `warehouses`
--

INSERT INTO `warehouses` (`WarehouseId`, `WarehouseName`, `StatusId`) VALUES
(1, 'Hà Nội', 2),
(2, 'Sài Gòn', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accountbalances`
--
ALTER TABLE `accountbalances`
  ADD PRIMARY KEY (`AccountBalanceId`);

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`ActionId`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`ArticleId`);

--
-- Indexes for table `bankaccounts`
--
ALTER TABLE `bankaccounts`
  ADD PRIMARY KEY (`BankAccountId`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`CategoryId`);

--
-- Indexes for table `categoryarticles`
--
ALTER TABLE `categoryarticles`
  ADD PRIMARY KEY (`CategoryArticleId`);

--
-- Indexes for table `chats`
--
ALTER TABLE `chats`
  ADD PRIMARY KEY (`ChatId`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`CommentId`);

--
-- Indexes for table `complaintmsgs`
--
ALTER TABLE `complaintmsgs`
  ADD PRIMARY KEY (`ComplaintMsgId`);

--
-- Indexes for table `complaints`
--
ALTER TABLE `complaints`
  ADD PRIMARY KEY (`ComplaintId`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`ConfigId`);

--
-- Indexes for table `customerbalances`
--
ALTER TABLE `customerbalances`
  ADD PRIMARY KEY (`CustomerBalanceId`);

--
-- Indexes for table `moneysources`
--
ALTER TABLE `moneysources`
  ADD PRIMARY KEY (`MoneySourceId`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`NotificationId`);

--
-- Indexes for table `orderaccounts`
--
ALTER TABLE `orderaccounts`
  ADD PRIMARY KEY (`OrderAccountId`);

--
-- Indexes for table `orderdraffs`
--
ALTER TABLE `orderdraffs`
  ADD PRIMARY KEY (`OrderDraffId`);

--
-- Indexes for table `orderlogs`
--
ALTER TABLE `orderlogs`
  ADD PRIMARY KEY (`OrderLogId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderId`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductId`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`ProvinceId`);

--
-- Indexes for table `recharges`
--
ALTER TABLE `recharges`
  ADD PRIMARY KEY (`RechargeId`);

--
-- Indexes for table `roleactions`
--
ALTER TABLE `roleactions`
  ADD PRIMARY KEY (`RoleActionId`);

--
-- Indexes for table `salemilestones`
--
ALTER TABLE `salemilestones`
  ADD PRIMARY KEY (`SaleMilestoneId`);

--
-- Indexes for table `servicefeelogs`
--
ALTER TABLE `servicefeelogs`
  ADD PRIMARY KEY (`ServiceFeeLogId`);

--
-- Indexes for table `servicefees`
--
ALTER TABLE `servicefees`
  ADD PRIMARY KEY (`ServiceFeeId`);

--
-- Indexes for table `shopnumbers`
--
ALTER TABLE `shopnumbers`
  ADD PRIMARY KEY (`ShopNumberId`);

--
-- Indexes for table `shops`
--
ALTER TABLE `shops`
  ADD PRIMARY KEY (`ShopId`);

--
-- Indexes for table `shoptrackings`
--
ALTER TABLE `shoptrackings`
  ADD PRIMARY KEY (`ShopTrackingId`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`SliderId`);

--
-- Indexes for table `sysnotices`
--
ALTER TABLE `sysnotices`
  ADD PRIMARY KEY (`SysNoticeId`);

--
-- Indexes for table `trackingcrawls`
--
ALTER TABLE `trackingcrawls`
  ADD PRIMARY KEY (`TrackingCrawlId`);

--
-- Indexes for table `transactionlogs`
--
ALTER TABLE `transactionlogs`
  ADD PRIMARY KEY (`TransactionLogId`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`TransactionId`);

--
-- Indexes for table `transportfeelogs`
--
ALTER TABLE `transportfeelogs`
  ADD PRIMARY KEY (`TransportFeeLogId`);

--
-- Indexes for table `transportfees`
--
ALTER TABLE `transportfees`
  ADD PRIMARY KEY (`TransportFeeId`);

--
-- Indexes for table `transports`
--
ALTER TABLE `transports`
  ADD PRIMARY KEY (`TransportId`);

--
-- Indexes for table `transporttypes`
--
ALTER TABLE `transporttypes`
  ADD PRIMARY KEY (`TransportTypeId`);

--
-- Indexes for table `transportusers`
--
ALTER TABLE `transportusers`
  ADD PRIMARY KEY (`TransportUserId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `warehouses`
--
ALTER TABLE `warehouses`
  ADD PRIMARY KEY (`WarehouseId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accountbalances`
--
ALTER TABLE `accountbalances`
  MODIFY `AccountBalanceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `ArticleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `bankaccounts`
--
ALTER TABLE `bankaccounts`
  MODIFY `BankAccountId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `CategoryId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `categoryarticles`
--
ALTER TABLE `categoryarticles`
  MODIFY `CategoryArticleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `chats`
--
ALTER TABLE `chats`
  MODIFY `ChatId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `CommentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `complaintmsgs`
--
ALTER TABLE `complaintmsgs`
  MODIFY `ComplaintMsgId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `complaints`
--
ALTER TABLE `complaints`
  MODIFY `ComplaintId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `customerbalances`
--
ALTER TABLE `customerbalances`
  MODIFY `CustomerBalanceId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `moneysources`
--
ALTER TABLE `moneysources`
  MODIFY `MoneySourceId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `NotificationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `orderaccounts`
--
ALTER TABLE `orderaccounts`
  MODIFY `OrderAccountId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `orderdraffs`
--
ALTER TABLE `orderdraffs`
  MODIFY `OrderDraffId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `orderlogs`
--
ALTER TABLE `orderlogs`
  MODIFY `OrderLogId` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `OrderId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ProductId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=450;
--
-- AUTO_INCREMENT for table `recharges`
--
ALTER TABLE `recharges`
  MODIFY `RechargeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `roleactions`
--
ALTER TABLE `roleactions`
  MODIFY `RoleActionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=212;
--
-- AUTO_INCREMENT for table `salemilestones`
--
ALTER TABLE `salemilestones`
  MODIFY `SaleMilestoneId` smallint(6) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `servicefeelogs`
--
ALTER TABLE `servicefeelogs`
  MODIFY `ServiceFeeLogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `servicefees`
--
ALTER TABLE `servicefees`
  MODIFY `ServiceFeeId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `shopnumbers`
--
ALTER TABLE `shopnumbers`
  MODIFY `ShopNumberId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `shops`
--
ALTER TABLE `shops`
  MODIFY `ShopId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT for table `shoptrackings`
--
ALTER TABLE `shoptrackings`
  MODIFY `ShopTrackingId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `SliderId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sysnotices`
--
ALTER TABLE `sysnotices`
  MODIFY `SysNoticeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `trackingcrawls`
--
ALTER TABLE `trackingcrawls`
  MODIFY `TrackingCrawlId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `transactionlogs`
--
ALTER TABLE `transactionlogs`
  MODIFY `TransactionLogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `TransactionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `transportfeelogs`
--
ALTER TABLE `transportfeelogs`
  MODIFY `TransportFeeLogId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `transportfees`
--
ALTER TABLE `transportfees`
  MODIFY `TransportFeeId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `transports`
--
ALTER TABLE `transports`
  MODIFY `TransportId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `transporttypes`
--
ALTER TABLE `transporttypes`
  MODIFY `TransportTypeId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `transportusers`
--
ALTER TABLE `transportusers`
  MODIFY `TransportUserId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `warehouses`
--
ALTER TABLE `warehouses`
  MODIFY `WarehouseId` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
