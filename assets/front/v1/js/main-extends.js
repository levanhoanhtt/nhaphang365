(function ($) {
    "use strict";
    $(document).ready(function () {
        if ($('#home-slider').length > 0 && $('#slide-background').length > 0) {
            $('#slide-background').bxSlider(
                {
                    nextText: '<i class="fa fa-angle-right"></i>',
                    prevText: '<i class="fa fa-angle-left"></i>',
                    auto: true,
                    onSliderLoad: function(){
                        var slideHeight = $('.content-slide').outerHeight();
                        $('.box-sidebar').css('min-height', slideHeight);
                        $('.header-banner').css('min-height', slideHeight);
                        $('.content-other').slimScroll({
                            height: (parseInt(slideHeight) - 62) + 'px',
                            alwaysVisible: false,
                            wheelStep: 20,
                            touchScrollStep: 500
                        });
                    }
                }
            );
        }
        /* Owl Carousel */
        $(".owl-carousel").each(function (index, el) {
            var config = $(this).data();
            config.navText = ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'];
            config.smartSpeed = "300";
            $(this).owlCarousel(config);
        });
        /* Scroll Top */
        $(document).on('click', '.scroll_top', function () {
            $('body,html').animate({ scrollTop: 0 }, 400);
            return false;
        });
        $(document).on('click', '.link_top', function () {
            $('body,html').animate({ scrollTop: 0 }, 400);
            return false;
        });

        $('.list-site li').on('click', function () {
            $('input#shopId').val($(this).attr('data-id'));
            var text = $('.selected-site').html();
            var html = text.split('(');
            $('.selected-site').html(html[0]);
        });
        /* Popup Login */
        $(document).on('click', '#link-modal-login', function () {
            $('#forgotPass_Modal').hide();
            $('#login_Modal').show();
            return false;
        });
        $(document).on('click', '#link-modal-sign-up', function () {
            $('#login_Modal').hide();
            $('#register_Modal').show();
            if($('select#provinceId option').length == 0){
                $.ajax({
                    type: "POST",
                    url: $('input#getListProvinceUrl').val(),
                    data: {},
                    success: function (response) {
                        var json = $.parseJSON(response);
                        var html = '';
                        for (var i = 0; i < json.length; i++) html += '<option value="' + json[i].ProvinceId + '">' + json[i].ProvinceName + '</option>';
                        $('select#provinceId').html(html);
                    },
                    error: function (response) {
                    }
                });
            }
            return false;
        });
        $(document).on('click', '.link-forgot', function () {
            $('#login_Modal').hide();
            $('#forgotPass_Modal').show();
            return false;
        });
        /* Input Check */
        $('input.iCheck').iCheck({
            checkboxClass: 'icheckbox_square-red',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });

        /* Chat */
        $(document).on('click', '.maximizechat', function () {
            $(this).hide();
            $(this).closest('.chat-box').find('.direct-chat').slideDown();
            $('.direct-chat').slideDown();
        });
        $(document).on('click', '.chat-box .btn-box-tool .fa-minus', function () {
            $('.maximizechat').show(1200);
            $('.chat-wrapper .direct-chat').hide();
        });
        $(document).on('click', '.chat-box .btn-tool-close', function () {
            $('.maximizechat').show(1200);
            $('.chat-wrapper .direct-chat').hide();
        });
        $(document).on('click', '.btn-chat-toggle', function () {
            $('.chat-wrapper .direct-chat').toggleClass('direct-chat-contacts-open');
            if($('.chat-wrapper .direct-chat').hasClass('direct-chat-contacts-open')) $('.btn-chat-toggle').text('Thu gọn');
            else  $('.btn-chat-toggle').text('Mở rộng');
        });

        //Hoan Mua Da
        $('#navbar-customer li:first-child, #navbar-customer li:last-child').hide();
        $('#aFacebook1').attr('href', $('#aFacebook').attr('href'));
        /* Chat */
        $(document).on('click', '.maximizechat', function () {
            $(this).hide();
            $(this).closest('.chat-box').find('.direct-chat').slideDown();
            $('.direct-chat').slideDown();
        });
        $(document).on('click', '.chat-box .btn-box-tool .fa-minus', function () {
            $('.maximizechat').show(1200);
            $('.chat-wrapper .direct-chat').hide();
        });
        $(document).on('click', '.chat-box .btn-tool-close', function () {
            $('.maximizechat').show(1200);
            $('.chat-wrapper .direct-chat').hide();
        });
        $(document).on('click', '.btn-chat-toggle', function () {
            $('.chat-wrapper .direct-chat').toggleClass('direct-chat-contacts-open');
            if($('.chat-wrapper .direct-chat').hasClass('direct-chat-contacts-open')) $('.btn-chat-toggle').text('Thu gọn');
            else  $('.btn-chat-toggle').text('Mở rộng');
        });

        //link extension
        if(navigator.userAgent.indexOf("Chrome") != -1 ) $('.aLinkTool').attr('href', 'https://chrome.google.com/webstore/detail/%C4%91%E1%BA%B7t-h%C3%A0ng-86/ggoeafjbhhihmlkmkgcneliicpldahgk?hl=vi');
        else if(navigator.userAgent.indexOf("Firefox") != -1 ) $('.aLinkTool').attr('href', 'https://chrome.google.com/webstore/detail/%C4%91%E1%BA%B7t-h%C3%A0ng-86/ggoeafjbhhihmlkmkgcneliicpldahgk?hl=vi');
        else{
            $('.aLinkTool').attr('href', 'javascript:void(0)');
            $('.aLinkTool').attr('onclick', '$("#userAgent_Modal").modal("show");return false;');
            $('.aLinkTool').removeAttr('target');
        }
        //product image
        $('img.lazy').lazyload();
        $('.box-product-head ul.nav-tab li a').click(function(){
            $('div' + $(this).attr('href')).find('.lazy').each(function() {
                var imageSrc = $(this).attr('data-original');
                $(this).attr('src', imageSrc).removeClass('lazy');
            });
        });
        //review & comment
        $('#btnSendComment').click(function(){
            var comment = $('#txtComment').val().trim();
            if(comment == ''){
                showNotification('Đánh giá không được bỏ trống', 0);
                return false;
            }
            var crUserId = parseInt($('input#userLoginId').val());
            if(crUserId == 0){
                showNotification('Vui lòng đăng nhập để đánh giá', 0);
                return false;
            }
            var commentTypeId = parseInt($('select#commentTypeId').val());
            $.ajax({
                type: "POST",
                url: $('input#updateCommentUrl').val(),
                data: {
                    CommentId: 0,
                    ArticleId: 0,
                    Comment: comment,
                    StatusId: 1,
                    CommentStarId: (commentTypeId == 1) ? 5 : 2,
                    CrUserId: crUserId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        showNotification('Cảm ơn bạn đã viết đánh giá', 1);
                        $('select#commentTypeId').val('1');
                        $('#txtComment').val('');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
            return false;
        });

        $("#slShopId").click(function() {
            var shopId = $('.slShopId').val();
            $('input#ShopId').val(shopId);
        });

        $('.span-star, #aReview').click(function(){
            $('input#commentStarId').val('5');
            $('p#starDesc').text('Xuất sắc');
            $('#review_Modal i.fa-star').css('color', '#4081ff');
            $('#review_Modal').show();
            return false;
        });
        $('#review_Modal i.fa-star').hover(function(){
            var id = parseInt($(this).attr('data-id'));
            $('p#starDesc').text($(this).attr('data-text'));
            $('#review_Modal i.fa-star').each(function(){
                if(id < parseInt($(this).attr('data-id'))) $(this).css('color', '#333');
                else $(this).css('color', '#4081ff');
            });
        }, function(){
            var id = $('input#commentStarId').val();
            $('p#starDesc').text($('#review_Modal i.fa-star[data-id="' + id + '"]').attr('data-text'));
            $('#review_Modal i.fa-star').each(function(){
                if(id < parseInt($(this).attr('data-id'))) $(this).css('color', '#333');
                else $(this).css('color', '#4081ff');
            });
        });
        $('#review_Modal i.fa-star').click(function(){
            var id = parseInt($(this).attr('data-id'));
            $('input#commentStarId').val(id);
            $('p#starDesc').text($(this).attr('data-text'));
            $('#review_Modal i.fa-star').each(function(){
                if(id < parseInt($(this).attr('data-id'))) $(this).css('color', '#333');
                else $(this).css('color', '#4081ff');
            });
        });
        $('#btnSendReview').click(function(){
            var comment = $('#txtReview').val().trim();
            if(comment == ''){
                showNotification('Đánh giá không được bỏ trống', 0);
                return false;
            }
            var crUserId = parseInt($('input#userLoginId').val());
            if(crUserId == 0){
                showNotification('Vui lòng đăng nhập để đánh giá', 0);
                return false;
            }
            $.ajax({
                type: "POST",
                url: $('input#updateCommentUrl').val(),
                data: {
                    CommentId: 0,
                    ArticleId: 0,
                    Comment: comment,
                    StatusId: 1,
                    CommentStarId: $('input#commentStarId').val(),
                    CrUserId: crUserId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        showNotification('Cảm ơn bạn đã viết đánh giá', 1);
                        $('#txtReview').val('');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
            return false;
        });
        getListNotification();
        setInterval(getListNotification, 60000);
        // getListChatHeader();
        $('#ulNotificationHeader').on("click", "a.link_to", function(){
            var url = $(this).attr('href');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusNotificationUrl').val(),
                data: {
                    NotificationId: $(this).attr('data-id'),
                    NotificationStatusId: 2
                },
                success: function (response) {
                    if(url != 'javascript:void(0)') window.location.href = url;
                },
                error: function (response) {
                    if(url != 'javascript:void(0)') window.location.href = url;
                }
            });
            return false;
        });

        $('#ulChatHeader').on('click', 'a', function (){
            var staffId = parseInt($(this).attr('data-id'));
            if(staffId > 0){
                $('.message_icon').removeClass('open');
                $('.maximizechat').hide();
                $('.maximizechat').closest('.chat-box').find('.direct-chat').slideDown();
                $('.direct-chat').slideDown();
                var staffName = $(this).attr('data-name');
                var staffAvatar = $(this).attr('data-avatar');
                $('input#staffId').val(staffId);
                $('input#staffName').val(staffName);
                $('input#staffAvatar').val(staffAvatar);
                $('.direct-chat h3.box-title').html(staffName);
                $('p#staffPhone').text($('#spanSysPhone').text());
                $('p#staffEmail').text($('#aSysEmail').text());
                $('span#countChatUnRead').text('1').attr('data-original-title', '1 Tin Nhắn chưa đọc');
                $('.direct-chat').removeClass('direct-chat-contacts-open');
                $('input#startChatPagging').val('0');
                // getListChat('');
            }
            return false;
        });


        //login
        $(document).on('submit', '#loginForm', function () {
            if (validatePart('#loginForm')) {
                var form = $('#loginForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            $('#link-modal-sign-up, #link-modal-login').hide();
                            $('#aAvatar, #aLogout').show();
                            $('input#userLoginId').val(json.data.User.UserId);
                            $('input#fullNameLoginId').val(json.data.User.FullName);
                            $('span.fullName').text(json.data.User.FullName);
                            $('input#avatarLoginId').val(json.data.User.Avatar);
                            $('#aAvartar').show();
                            $('img.userAvatar').attr('src', $('input#userImagePath').val() + json.data.User.Avatar);
                            $('#aCurentBalance').text(json.data.Balance + ' VNĐ');
                            $('#login_Modal').hide();
                            $('input#startChatPagging').val('0');
                            getListNotification();
                            // getListChatHeader();
                            // changeStateUser(json.data.User.UserId, 4, 1);
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
            return false;
        });


        //logout
        $('#aLogout').click(function(){
            $.ajax({
                type: "POST",
                url: $(this).attr('data-url'),
                data: {},
                success: function (response) {
                    // changeStateUser($('input#userLoginId').val(), 4, 0);
                    var json = $.parseJSON(response);
                    $('#aBalance, #aLogout').hide();
                    $('#link-modal-login,#link-modal-sign-up').show();
                    $('input#userLoginId').val('0');
                    $('input#fullNameLoginId').val(json.FullName);
                    $('span.fullName').text(json.FullName);
                    $('input#avatarLoginId').val(json.Avatar);
                    $('#aAvartar').hide();
                    $('img.userAvatar').hide();
                    $('#aCurentBalance').text('0 VNĐ');
                    $('#listChat').html('');
                    $('span.countNotification').text('0');
                    $('#ulNotificationHeader').html('');
                    $('#ulChatHeader').html('');
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
            return false;
        });

        //register
        $(document).on('submit', '#registerForm', function () {
            if (validatePart('#registerForm')) {
                if ($('input#newPass').val() != $('input#rePass').val()) {
                    showNotification('Mật khẩu không trùng', 0);
                    return false;
                }
                var phoneNumber = $('input#phoneNumber').val().trim();
                if(phoneNumber.indexOf(' ') >= 0){
                    showNotification('Số điện thoại không được có khoảng trằng', 0);
                    $('input#phoneNumber').focus();
                    return false;
                }
                if(phoneNumber.length == 10 || phoneNumber.length == 11){
                    var filter = /^[0-9-+]+$/;
                    if(!filter.test(phoneNumber)) {
                        showNotification('Số điện thoại không hợp lệ', 0);
                        $('input#phoneNumber').focus();
                        return false;
                    }
                }
                else{
                    showNotification('Số điện thoại chỉ gồm 10 hoặc 11 số', 0);
                    $('input#phoneNumber').focus();
                    return false;
                }
                var form = $('#registerForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            $('#register_Modal').hide();
                            showNotification('Đămg ký thành công. Vui lòng đăng nhập', 1);
                            $('#loginForm input[name="UserName"]').val(form.find('input[name="PhoneNumber"]').val());
                            $('#loginForm input[name="UserPass"]').val($('input#newPass').val());
                            $('#login_Modal').show();
                            form.trigger('reset');
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
            return false;
        });
        //forgot pass
        $(document).on('submit', '#forgotForm', function () {
            if (validatePart('#forgotForm')) {
                var form = $('#forgotForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1){
                            form.trigger('reset');
                            $('#forgotPass_Modal').hide();
                        }
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
            return false;
        });
        //article modal
        var articleId = parseInt($('input#articleId').val());
        if(articleId > 0) getArticle(articleId);
        $('a.showModal').click(function(){
            articleId = $(this).attr('data-id');
            if($('input#isGetArticle' + articleId).val() == '0') getArticle(articleId);
            else $('#article_Modal_' + id).show();
            return false;
        });
        //link product
        var redirectProductUrl = $('input#redirectProductUrl').val() + '/?';
        setLinkProduct(redirectProductUrl, 1);
        $('ul.box-tabs-shop .aShop').click(function(){
            $('.box-tabs-shop > li').removeClass('active');
            $(this).parent('li').addClass('active');
            setLinkProduct(redirectProductUrl, $(this).attr('data-id'));
        });
    });
    $(window).scroll(function () {
        /* Fixed Header Meny Top */
        var h = $(window).scrollTop();
        var h_header = $('.top-header .col-content').height();

        var width = $(window).width();
        if ( width > 767 ) {
            if (h > h_header) $('.list-support').addClass('fixed_support');
            else $('.list-support').removeClass('fixed_support');
        }
        if (width > 992) {
            if (h > 35) $('.sidebar-box').addClass('sidebar-fixed');
            else $('.sidebar-box').removeClass('sidebar-fixed');
        }
        /* Show hide scroll top button */
        if ($(window).scrollTop() == 0) $('.scroll_top').stop(false, true).fadeOut(600);
        else $('.scroll_top').stop(false, true).fadeIn(600);

        /* Scroll Top Right */
        if ($(window).scrollTop() == 0) $('.link_top').stop(false, true).fadeOut(600);
        else $('.link_top').stop(false, true).fadeIn(600);

    });
})(jQuery);

function getArticle(articleId){
    $.ajax({
        type: "POST",
        url: $('input#getArticleContentUrl').val(),
        data: {
            ArticleId: articleId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1){
                $('#article_Modal_' + articleId).find('div.articleContent').html(json.data);
                $('#article_Modal_' + articleId).show();
                $('input#isGetArticle' + articleId).val('1');
            }
        },
        error: function (response) {}
    });
}

function setLinkProduct(redirectProductUrl, shopId){
    $('.aProduct').each(function(){
        $(this).attr('href', redirectProductUrl + 'ProductNameTQ=' + $(this).attr('data-text') + '&ShopId=' + shopId);
    });
}

function appendCountToPageTitle(){}

function getListNotification(){
    var userLoginId = parseInt($('input#userLoginId').val());
    if(userLoginId > 0){
        $.ajax({
            type: "POST",
            url: $('input#getListNotificationHeaderUrl').val(),
            data: {
                UserId: userLoginId,
                RoleId: 4
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1) {
                    var html = '';
                    var data = json.data;
                    $('span.countNotification').text(data.length);
                    var userImagePath = $('input#userImagePath').val();
                    var notificationPageUrl = $('#aViewAllNotification').attr('href');
                    for (var i = 0; i < data.length; i++) {
                        html += '<li><a href="' + ((data[i].LinkTo == '') ? notificationPageUrl : data[i].LinkTo) + '" class="link_to" data-id="' + data[i].NotificationId + '"><div class="pull-left">';
                        html += '<img src="assets/vendor/dist/img/logo.png" class="img-circle" alt="Đặt hàng 86">';
                        html += '</div><h4>Đặt hàng 86<small>';
                        html += '<i class="fa fa-clock-o"></i> ' + data[i].CrTime + '<br>' + data[i].CrDate + '</small></h4>';
                        html += '<p>' + data[i].Message + '</p></a></li>';
                    }
                    $('#ulNotificationHeader').html(html);
                    if(html != '') {
                        $('#ulNotificationHeader').slimScroll({
                            height: '185px',
                            alwaysVisible: false,
                            wheelStep: 20,
                            touchScrollStep: 500
                        });
                        $.playSound('assets/vendor/dist/chat');
                    }
                }
            },
            error: function (response) {}
        });
    }
}

function validatePart(container){
    var flag = true;
    $(container + ' .hmdrequired').each(function(){
        if($(this).val().trim() == ''){
            showNotification($(this).attr('data-field') + ' không được bỏ trống', 0);
            $(this).focus();
            flag = false;
            return false;
        }
    });
    return flag;
}

function showNotification(msg, type){
    var typeText = 'error';
    if(type == 1) typeText = 'success';
    var notice = new PNotify({
        title: 'Thông báo',
        text: msg,
        type: typeText,
        delay: 2000,
        addclass: 'stack-bottomright',
        stack: {"dir1": "up", "dir2": "left", "firstpos1": 15, "firstpos2": 15}
    });
}

//chat
function scrollChat(isOpenChat){
    if(isOpenChat && $('.maximizechat').length > 0){
        $('.maximizechat').hide();
        $('.maximizechat').closest('.chat-box').find('.direct-chat').slideDown();
        $('.direct-chat').slideDown();
    }
    var height = 250;
    if($('div#chatToCustomerPage').length > 0) height = 350;
    else if($('div#chatToStaffPage').length > 0) height = 350;
    $('#listChat').slimScroll({
        height: height + 'px',
        alwaysVisible: true,
        wheelStep: 20,
        touchScrollStep: 500,
        start: 'bottom',
        scrollTo : $('#listChat').prop('scrollHeight') + 'px'
    });
}

function getCurentDateTime(){
    var now = new Date();
    var date = now.getDate();
    var month = now.getMonth() + 1;
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if(date < 10) date = '0' + date;
    if(month < 10) month = '0' + month;
    if(hour < 10) hour = '0' + hour;
    if(minute < 10) minute = '0' + minute;
    if(second < 10) second = '0' + second;
    return date + "/" + month + "/" + now.getFullYear() + " " + hour + ":" + minute + ":" + second;
}