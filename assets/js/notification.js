$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    var curentPathName = window.location.pathname;
    var rootPath = $('#roorPath').val();
    curentPathName = curentPathName.replace(rootPath, '');
    var hostname = window.location.hostname;
    $('#ulNotificationStatus li a').each(function() {
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-default');
        var pageLink = $(this).attr("href");
        pageLink = pageLink.replace('https://', '');
        pageLink = pageLink.replace('http://', '');
        pageLink = pageLink.replace(hostname, '');
        pageLink = pageLink.replace(rootPath, '');
        if(pageLink == curentPathName){
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
            return false;
        }
    });
    $('#aReadAll').click(function(){
        $.ajax({
            type: "POST",
            url: $('input#changeAllStatusUrl').val(),
            data: {
                NotificationStatusId: 2
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1){
                    $('tr.success').removeClass('success');
                    $('td.actions').html('');
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
    $('#tbodyNotification').on("click", "a.link_to", function(){
        var url = $(this).attr('href');
        if($(this).attr('data-status') == '1' && $(this).attr('data-check') == '1'){
            changeStatus($(this).attr('data-id'), function(){
                if(url != 'javascript:void(0)') window.location.href = url;
            });
        }
        else window.location.href = url;
       return false;
    });
    $('#tbodyNotification').on("click", "a.link_check", function(){
        var id = $(this).attr('data-id');
        changeStatus(id, function(){
            var tr = $('tr#notification_' + id);
            tr.removeClass('success');
            tr.find('a.link_to').attr('data-status', '2').attr('data-check', '0');
            tr.find('td.actions').html('');
        });
        return false;
    });
});

function changeStatus(id, fnSuccess){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusNotificationUrl').val(),
        data: {
            NotificationId: id,
            NotificationStatusId: 2
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1) fnSuccess();
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}