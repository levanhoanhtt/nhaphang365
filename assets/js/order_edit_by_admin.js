$(document).ready(function(){
    $('.match-height').matchHeight();
    if($('input#canEdit').val() == '0'){
        $('select#warehouseId').prop('disabled', true);
        $('input#costFee').prop('disabled', true);
        $('input#exchangeRate').prop('disabled', true);
        $('#servicePercent').prop('disabled', true);
        $('.tbodyProduct input, .tbodyProduct textarea').prop('disabled', true);
        $('.btn-add-product').hide();
        $('input#cbIsFixPercent').iCheck('disable');
    }

    //global variable========================================
    var orderStatusId = parseInt($('input#orderStatusId').val());
    var noProductImage = $('input#noProductImage').val();
    var shipTQType = parseInt($('input#shipTQType').val());

    if(orderStatusId == 5 || orderStatusId == 10) $('.tdShipTQ').addClass('tdShipTQ2').prepend('<i class="fa fa-level-up ship_action" data-status="3" title="Tăng giá"></i><i class="fa fa-level-down ship_action" data-status="4" title="Giảm giá"></i>');
    if($('input#canEdit').val() == '1'){
        if(orderStatusId == 5 || orderStatusId == 10) $('.tbodyProduct textarea.feedback').prop('disabled', false);
        else if(orderStatusId < 4){
            $('.tbodyProduct .tr_product').each(function(){
                $(this).find('td').last().html('<i class="fa fa-trash-o link_delete_product" title="Xóa"></i>');
            });
        }
        else{
            $('.tbodyProduct .tr_product').each(function(){
                $(this).find('td').last().html('');
            });
        }
    }

    //delete product normal===================================
    deleteProduct(orderStatusId, shipTQType);
    //product action  with teransaction==========================
    productAction(orderStatusId, shipTQType);
    //shop action  with teransaction==========================
    shopAction(orderStatusId, shipTQType);

    //update staff=============================================
    $('#btnUpdateStaff').click(function(){
        $('#btnUpdateStaff').prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#updateOrderStaffUrl').val(),
            data: {
                OrderId: $('input#orderId').val(),
                CareStaffId: $('select#careStaffId1').val(),
                OrderUserId: $('select#orderUserId').val(),
                OldCareStaffId: $('input#careStaffId').val(),
                CustomerId: $('input#customerId').val(),
                OrderCode: $('#tdOrderCode').text()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if(json.code == 1) $('input#careStaffId').val($('select#careStaffId1').val());
                $('#btnUpdateStaff').prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('#btnUpdateStaff').prop('disabled', true);
            }
        });
    });

    //Revenue=============================================
    orderRevenue();
    //complaint===========================================
    complaintAction();

    //save order===============================================
    $('.btnSubmitOrder').click(function(){
        saveOrder(3, noProductImage, shipTQType, function(){
            orderStatusId = 3;
        });
    });
    //comfirm order============================================
    $('.btnComfirmOrder').click(function(){
        saveOrder(5, noProductImage, shipTQType, function(){
            orderStatusId = 5;
        });
    });
    //not comfirm order============================================
    $('.btnConfirmNotOk').click(function(){
        changeOrderStatus(7, function(){
            orderStatusId = 7;
        });
    });
    //cancer order============================================
    $('.btnCancelOrder').click(function(){
        changeOrderStatus(8, function(){
            orderStatusId = 8;
        });
    });
    //change shop category===================================
    changeShopCategory(orderStatusId);
    //order==================================================
    $('input.cbIsTransportSlow').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
});

function saveOrder(orderStatusId, noProductImage, shipTQType, fnSuccess){
    if(validate() && validateCost() && validateCost1()) {
        var listProducts = getListProducts(noProductImage, false, shipTQType);
        if (listProducts.length > 0) {
            $('.btnSubmitOrder, .btnComfirmOrder, .btnConfirmNotOk, .btnCancelOrder').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateOrderUrl').val(),
                data: {
                    CustomerId: $('input#customerId').val(),
                    FullName: $('input#fullName').val().trim(),
                    PhoneNumber: $('input#phoneNumber').val().trim(),
                    Email: $('input#email').val().trim(),
                    Address: $('input#address').val().trim(),
                    CareStaffId: $('input#careStaffId').val(),
                    WarehouseId: $('select#warehouseId').val(),
                    ExchangeRate: replaceCost($('input#exchangeRate').val()),
                    OrderStatusId: orderStatusId,
                    ServicePercent: $('#servicePercent').val(),
                    IsFixPercent: $('input#isFixPercent').val(),
                    OtherCost: replaceCost($('input#costFee').val()),
                    OrderUserId: $('select#orderUserId').val(),
                    Comment: $('#aOrderComment').attr('data-original-title'),
                    ProductJson: JSON.stringify(listProducts),
                    IsFromAdmin: 1,
                    OrderId: $('input#orderId').val(),
                    OrderCode: $('#tdOrderCode').text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        if(orderStatusId == 3) $('#btnCurent').text('Đã check');
                        else if(orderStatusId == 5) $('#btnCurent').text('Đặt hàng');
                        $('input#orderStatusId').val(orderStatusId);
                        fnSuccess();
                    }
                    else $('.btnSubmitOrder, .btnComfirmOrder, .btnConfirmNotOk, .btnCancelOrder').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('.btnSubmitOrder, .btnComfirmOrder, .btnConfirmNotOk, .btnCancelOrder').prop('disabled', false);
                }
            });
        }
    }
}

function changeOrderStatus(orderStatusId, fnSuccess){
    var flag = true;
    if(orderStatusId == 8) flag = confirm('Bạn có thực sự muốn hủy đơn hàng ?');
    if(flag){
        $('.btnSubmitOrder, .btnConfirmNotOk, .btnCancelOrder').prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#changeStatusOrderUrl').val(),
            data: {
                OrderId: $('input#orderId').val(),
                OrderStatusId: orderStatusId,
                CustomerId: $('input#customerId').val(),
                CareStaffId: $('input#careStaffId').val(),
                OrderCode: $('#tdOrderCode').text()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) {
                    var statusName = '';
                    if(orderStatusId == 5) statusName = 'Đặt hàng';
                    else if(orderStatusId == 7) statusName = 'Không được duyệt';
                    else if(orderStatusId == 8) statusName = 'Đã hủy';
                    $('#btnCurent').text(statusName);
                    $('input#orderStatusId').val(orderStatusId);
                    fnSuccess();
                }
                else $('.btnSubmitOrder, .btnConfirmNotOk, .btnCancelOrder').prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('.btnSubmitOrder, .btnConfirmNotOk, .btnCancelOrder').prop('disabled', false);
            }
        });
    }
}