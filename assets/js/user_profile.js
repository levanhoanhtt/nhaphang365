$(document).ready(function(){
    if($('#tbodyContact').length > 0) {
        $('input.iCheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
        var flag = 0, i;
        for(i = 2; i < 4; i++){
            if($('input#fullName' + i).val() == '' && $('input#phoneNumber' + i).val() == '' && $('input#address' + i).val() == ''){
                $('tr#contact_' + i).hide();
                flag++;
            }
        }
        if(flag == 0) $('#btnAddContact').hide();
        $('#btnAddContact').click(function(){
            flag = 0;
            for(i = 1; i < 4; i++){
                if($('tr#contact_' + i).is(':hidden')){
                    $('tr#contact_' + i).show();
                    flag = i;
                    break;
                }
            }
            if(flag > 0){
                for(i = 1; i < 4; i++) {
                    if ($('tr#contact_' + i).is(':hidden')) {
                        flag = 0;
                        break;
                    }
                }
                if(flag > 0) $('#btnAddContact').hide();
            }
        });
        $('#tbodyContact a.link_delete').click(function(){
            var id = $(this).attr('data-id');
            if($('tr#contact_' + id).find('div.iradio_square-blue').hasClass('checked')) showNotification('Địa chỉ mặc định không được xóa', 0);
            else{
                $('tr#contact_' + id).hide();
                $('tr#contact_' + id).find('input').val('');
                $('#btnAddContact').show();
            }
            return false;
        });
        $('input.iCheck').on('ifChecked', function(e){
            var id = $(this).val();
            $('#tbodyContact tr').removeClass('bg-info');
            $('input.phoneNumberContact').removeClass('phoneNumberCurent');
            $('input.addressContact').removeClass('addressCurent');
            $('select.select2').removeClass('provinceCurent');
            var trContact = $('tr#contact_' + id);
            trContact.addClass('bg-info');
            trContact.find('input.phoneNumberContact').addClass('phoneNumberCurent');
            trContact.find('input.addressContact').addClass('addressCurent');
            trContact.find('select.select2').addClass('provinceCurent');
            $('input#phoneNumber').val(trContact.find('input.phoneNumberContact').val());
            $('input#address').val(trContact.find('input.addressContact').val());
            $('input#provinceId').val(trContact.find('select.select2').val());
        });
        $('#tbodyContact').on('keyup', 'input.phoneNumberCurent', function () {
            $('input#phoneNumber').val($(this).val());
        });
        $('#tbodyContact').on('keyup', 'input.addressCurent', function () {
            $('input#address').val($(this).val());
        });
        $('#tbodyContact').on('change', 'select.provinceCurent', function () {
            $('input#provinceId').val($(this).val());
        });
        $('input#phoneNumber').keyup(function(){
            $('#tbodyContact input.phoneNumberCurent').val($(this).val());
        });
    }
    $('.chooseImage').click(function(){
        var finder = new CKFinder();
        finder.resourceType = 'Users';
        finder.selectActionFunction = function(fileUrl) {
            $('input#avatar').val(fileUrl);
            $('img#imgAvatar').attr('src', fileUrl);
        };
        finder.popup();
    });
    $(document).on('submit','#profileForm',function (){
        if(validate()) {
            if ($('input#newPass').val().length > 0 || $('input#rePass').val().length > 0) {
                if ($('input#newPass').val() != $('input#rePass').val()) {
                    showNotification('Mật khẩu không trùng', 0);
                    return false;
                }
            }
            if($('input#userName').val().trim().indexOf(' ') >= 0){
                showNotification('Tên đăng nhập không được có khoảng trằng', 0);
                $('input#userName').focus();
                return false;
            }
            if($('#roleId').val() == '4') {
                var phoneNumber = $('input#phoneNumber').val().trim();
                if (phoneNumber.indexOf(' ') >= 0) {
                    showNotification('Số điện thoại không được có khoảng trằng', 0);
                    $('input#phoneNumber').focus();
                    return false;
                }
                if (phoneNumber.length == 10 || phoneNumber.length == 11) {
                    var filter = /^[0-9-+]+$/;
                    if (!filter.test(phoneNumber)) {
                        showNotification('Số điện thoại không hợp lệ', 0);
                        $('input#phoneNumber').focus();
                        return false;
                    }
                }
                else {
                    showNotification('Số điện thoại chỉ gồm 10 hoặc 11 số', 0);
                    $('input#phoneNumber').focus();
                    return false;
                }
            }
            var form = $('#profileForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('input#userPass').val('');
                        $('input#newPass').val('');
                        $('input#rePass').val('');
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});