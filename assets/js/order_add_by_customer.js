$(document).ready(function () {
    if ($(window).width() < 500) $('div#logo').removeClass('pull-left');
    else $('div#logo').addClass('pull-left');
    $(window).resize(function () {
        if ($(window).width() < 500) $('div#logo').removeClass('pull-left');
        else $('div#logo').addClass('pull-left');
    });

    //global variable========================================
    var exchangeRate = replaceCost($('input#exchangeRate').val());
    var noProductImage = $('input#noProductImage').val();
    var shipTQType = parseInt($('input#shipTQType').val());

    //save order===============================================
    $('#btnSaveOrder').click(function(){
        saveOrderDraff(noProductImage, replaceCost($('input#exchangeRate').val()), shipTQType, true);
    });
    $('#btnSubmitOrder').click(function () {
        if(validate() && validateCost() && validateCost1()) {
            var careStaffId = $('select#careStaffId').val();
            if(careStaffId != '0') {
                var listProducts = getListProducts(noProductImage, false, shipTQType);
                if (listProducts.length > 0) {
                    $('#btnSubmitOrder').prop('disabled', true);
                    var customerId = parseInt($('input#userLoginId').val());
                    $.ajax({
                        type: "POST",
                        url: $('input#updateOrderUrl').val(),
                        data: {
                            CustomerId: customerId,
                            FullName: $('input#fullName').val().trim(),
                            PhoneNumber: $('input#phoneNumber').val().trim(),
                            Email: $('input#email').val().trim(),
                            Address: $('input#address').val().trim(),
                            CareStaffId: careStaffId,
                            WarehouseId: $('select#warehouseId').val(),
                            ExchangeRate: exchangeRate,
                            OrderStatusId: 1,
                            ServicePercent: $('#servicePercent').val(),
                            IsFixPercent: $('input#isFixPercent').val(),
                            OtherCost: 0,//replaceCost($('input#costFee').val()),
                            Comment: $('textarea#comment').val().trim(),
                            OrderUserId: 0,
                            ProductJson: JSON.stringify(listProducts),
                            IsFromAdmin: 0,
                            OrderId: 0
                        },
                        success: function (response) {
                            var json = $.parseJSON(response);
                            showNotification(json.message, json.code);
                            if (json.code == 1) {
                                if (customerId > 0) redirect(false, $('input#listOrderUrl').val());
                                else {
                                    $('#btnSubmitOrder, #btnMergeOrederDraff').hide();
                                    $('#modalLoginMsg').modal('show');
                                }
                            }
                            else $('#btnSubmitOrder').prop('disabled', false);
                        },
                        error: function (response) {
                            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                            $('#btnSubmitOrder').prop('disabled', false);
                        }
                    });
                }
                else showNotification('Xin vui lòng thêm link sản phẩm để gửi đơn hàng', 0);
            }
            else showNotification('Xin vui lòng chọn Nhân viên CSKH', 0);
        }
    });
    //save draff to exist order================================
    $('#btnMergeOrederDraff').click(function(){
        var customerId = parseInt($('input#userLoginId').val());
        if(customerId > 0){
            if (validate() && validateCost() && validateCost1()) {
                $.ajax({
                    type: "POST",
                    url: $('input#getListOrderUrl').val(),
                    data: {
                        CustomerId: customerId,
                        OrderStatusIds: '1,2,3'
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) {
                            var html = '<option value="0">Vui lòng chọn</option>';
                            var data = json.data;
                            for (var i = 0; i < data.length; i++) {
                                html += '<option value="' + data[i].OrderId + '">' + data[i].OrderCode + '</option>';
                            }
                            $('select#mergeOrderId1').html(html);
                            $('#modalMergeOrderDraff').modal('show');
                        }
                        else showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Xin vui lòng đăng nhập để gộp đơn hàng', 0);
    });
    $('#btnMergeOrder1').click(function(){
        var orderId = parseInt($('select#mergeOrderId1').val());
        if(orderId > 0){
            var listProducts = getListProducts(noProductImage, false, shipTQType);
            if (listProducts.length > 0) {
                $('#btnMergeOrder1').prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#mergeOrderDraffUrl').val(),
                    data: {
                        OrderId: orderId,
                        ProductJson: JSON.stringify(listProducts),
                        CustomerName: $('input#fullName').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1) redirect(false, $('input#listOrderUrl').val());
                        else $('#btnMergeOrder1').prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        $('#btnMergeOrder1').prop('disabled', false);
                    }
                });
            }
            else showNotification('Xin vui lòng thêm link sản phẩm để gộp đơn hàng', 0);
        }
        else showNotification('Xin vui lòng chọn đơn gộp cùng', 0);
    });
});