$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $("#tbodyArticle").on("click", "a.link_delete", function(){
        if($('input#changeStatus').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) changeStatus($(this).attr('data-id'), 0);
        }
        else showNotification('Bạn không có quyền xóa bài viết', 0);
        return false;
    });
    $("#tbodyArticle").on("click", "a.link_status", function(){
        if($('input#changeStatus').val() == '1'){
            var id = $(this).attr('data-id');
            var statusId = $(this).attr('data-status');
            if(statusId != $('input#statusId_' + id).val()) changeStatus(id, statusId);
        }
        else showNotification('Bạn không có quyền cập nhật trạng thái bài viết', 0);
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });
});

function changeStatus(id, statusId){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusUrl').val(),
        data: {
            ArticleId: id,
            StatusId: statusId,
            ArticleTypeName: 'Bài viết'
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1){
                if(statusId == 0) $('tr#article_' + id).remove();
                else{
                    $('td#statusName_' + id).html(json.data.StatusName);
                    $('input#statusId_' + id).val(statusId);
                }
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}
