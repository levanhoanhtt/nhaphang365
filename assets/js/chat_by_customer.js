$(document).ready(function(){
    var noticeAudioUrl = 'assets/vendor/dist/chat';
    //thong bao notice
    if($('#ulChatHeader').length > 0) getListChatHeader();

    var socket = io($('input#chatServerUrl').val());
    var flag = 0;
    //form chat to
    if($('div#chatToStaffPage').length > 0){
        flag = 1;
        getListStaffChat(socket);
        $('#seachStaffChatForm').submit(function (e) {
            getListStaffChat(socket);
            e.preventDefault();
        });
        //customer on/off
        socket.on('user state', function (data) {
            if(data.roleId == 1){
                if(data.state == 1) $('.box-comments .box-comment[data-id="' + data.userId + '"] span.offline').removeClass('offline').addClass('online');
                else $('.box-comments .box-comment[data-id="' + data.userId + '"] span.online').removeClass('online').addClass('offline');
            }
        });
        //chon nhan vien CSKH de chat
        $('div.box-comments').on('click', 'div.box-comment', function () {
            var staffId = $(this).attr('data-id');
            if ($('input#staffId').val() != staffId) {
                $('div.box-comment').removeClass('active');
                $('div.box-comment[data-id="' + staffId + '"]').addClass('active');
                var staffName = $(this).attr('data-name');
                var staffAvatar = $(this).attr('data-avatar');
                $('input#staffId').val(staffId);
                $('input#staffName').val(staffName);
                $('input#staffAvatar').val(staffAvatar);
                $('.direct-chat h3.box-title').html(staffName + ' - ' + $('#spanSysPhone').text() + ' - ' + $('#aSysEmail').text());
                $('input#startChatPagging').val(0);
                $('input#totalChatMsg').val(0);
                getListChat('');
            }
        });
    }
    //form chat popup
    else if($('div#boxChatCustomer').length > 0) {
        flag = 2;
        //lay danh sach nhan vien CSKH - on/off
        getListStaff(socket);
        socket.on('user state', function (data) {
            if(data.roleId == 1){
                if(data.state == 1) $('ul#chat-staff-care-list').find('li a[data-id="' + data.userId + '"] span.offline').removeClass('offline').addClass('online');
                else $('ul#chat-staff-care-list').find('li a[data-id="' + data.userId + '"] span.online').removeClass('online').addClass('offline');
            }
        });
        //chon nhan vien CSKH de chat
        $('ul#chat-staff-care-list').on('click', 'a', function () {
            var staffId = $(this).attr('data-id');
            if ($('input#staffId').val() != staffId) {
                var staffName = $(this).attr('data-name');
                var staffAvatar = $(this).attr('data-avatar');
                $('input#staffId').val(staffId);
                $('input#staffName').val(staffName);
                $('input#staffAvatar').val(staffAvatar);
                $('.direct-chat h3.box-title').html(staffName);
                $('p#staffPhone').text($('#spanSysPhone').text());
                $('p#staffEmail').text($('#aSysEmail').text());
                var count = $(this).find('span.text-muted').attr('data-count');
                $('span#countChatUnRead').text(count).attr('data-original-title', count + ' Tin Nhắn chưa đọc');
                $('.direct-chat').removeClass('direct-chat-contacts-open');
                var chatData = {
                    customerId: $('input#userLoginId').val(),
                    customerName: $('input#fullNameLoginId').val(),
                    customerAvatar: $('input#avatarLoginId').val(),
                    staffId: staffId,
                    staffName: staffName,
                    staffAvatar: staffAvatar
                };
                socket.emit('chat begin', chatData);
            }
            return false;
        });
    }

    if(flag > 0) {
        socket.on('chat begin', function (data) {
            if ($('input#userLoginId').val() == data.customerId) {
                var userImagePath = $('input#userImagePath').val();
                var html = '<div class="direct-chat-msg">';
                html += '<div class="direct-chat-info clearfix">';
                html += '<span class="direct-chat-name pull-left">' + data.staffName + '</span>';
                html += '<span class="direct-chat-timestamp pull-right">' + getCurentDateTime() + '</span>';
                html += '</div><img class="direct-chat-img" src="' + userImagePath + data.staffAvatar + '" alt="">';
                html += '<div class="direct-chat-text">' + data.msg + '</div></div>';
                $('input#startChatPagging').val(0);
                $('input#totalChatMsg').val(0);
                getListChat(html);
            }
        });

        socket.on('new message', function (data) {
            if ($('input#userLoginId').val() == data.customerId) {
                if (flag == 1) {
                    if ($('input#staffId').val() == data.staffId) {
                        var userImagePath = $('input#userImagePath').val();
                        var imagePath = $('input#imagePath').val();
                        var divClass = ' unread';
                        if (data.isCustomerSend == 1) divClass = ' right';
                        var html = '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + ((data.isCustomerSend == 1) ? data.customerName : data.staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data.crDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + ((data.isCustomerSend == 1) ? data.customerAvatar : data.staffAvatar) + '" alt="">';
                        if (data.msg != '') html += '<div class="direct-chat-text">' + data.msg + '</div></div>';
                        else if (data.fileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data.fileUrl + '" target="_blank"><img src="' + imagePath + data.fileUrl + '" alt=""></a></div></div>';
                        $('#listChat').append(html);
                        scrollChat(true);
                        if (data.isCustomerSend == 0) {
                            $.playSound(noticeAudioUrl);
                            prependCustomerChat(data);
                        }
                        else $('#listChatStaff div.box-comment[data-id="' + data.staffId + '"]').find('span.lastMsgChat').text('You: ' + data.msg);
                    }
                    else {
                        if (data.isCustomerSend == 0) {
                            $.playSound(noticeAudioUrl);
                            if ($('#listChatStaff div.box-comment[data-id="' + data.staffId + '"]').length > 0) {
                                prependCustomerChat(data);
                            }
                        }
                    }
                }
                else {
                    if ($('input#staffId').val() == data.staffId) {
                        var userImagePath = $('input#userImagePath').val();
                        var imagePath = $('input#imagePath').val();
                        var divClass = ' unread';
                        if (data.isCustomerSend == 1) divClass = ' right';
                        var html = '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + ((data.isCustomerSend == 1) ? data.customerName : data.staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data.crDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + ((data.isCustomerSend == 1) ? data.customerAvatar : data.staffAvatar) + '" alt="">';
                        if (data.msg != '') html += '<div class="direct-chat-text">' + data.msg + '</div></div>';
                        else if (data.fileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data.fileUrl + '" target="_blank"><img src="' + imagePath + data.fileUrl + '" alt=""></a></div></div>';
                        $('#listChat').append(html);
                        scrollChat(true);
                        if (data.isCustomerSend == 0) {
                            $.playSound(noticeAudioUrl);
                            //cap nhat so tin chua doc
                            var count = parseInt($('span#countChatUnRead').text()) + 1;
                            $('span#countChatUnRead').text(count).attr('data-original-title', count + ' Tin Nhắn chưa đọc');
                        }
                    }
                    else {
                        if (data.isCustomerSend == 0){
                            $.playSound(noticeAudioUrl);
                            if($('.direct-chat').css('display') == 'none'){
                                $('.message_icon').removeClass('open');
                                $('.maximizechat').hide();
                                $('.maximizechat').closest('.chat-box').find('.direct-chat').slideDown();
                                $('.direct-chat').slideDown();
                                var staffName = data.staffName;
                                var staffAvatar = data.staffAvatar;
                                $('input#staffId').val(data.staffId);
                                $('input#staffName').val(staffName);
                                $('input#staffAvatar').val(staffAvatar);
                                $('.direct-chat h3.box-title').html(staffName);
                                $('p#staffPhone').text($('#spanSysPhone').text());
                                $('p#staffEmail').text($('#aSysEmail').text());
                                $('span#countChatUnRead').text('1').attr('data-original-title', '1 Tin Nhắn chưa đọc');
                                $('.direct-chat').removeClass('direct-chat-contacts-open');
                                $('input#startChatPagging').val('0');
                                getListChat('');
                            }
                            else{
                                var msg = '';
                                if (data.msg != '') msg = data.msg;
                                else if (data.fileUrl != '') msg = 'Gửi ảnh...';
                                showNotification(data.staffName + ': ' + msg , 1);
                            }
                        }
                    }
                }
            }
        });

        $('#chatForm').submit(function (e) {
            var customerId = $('input#userLoginId').val();
            if (customerId != '0') {
                var staffId = $('input#staffId').val();
                if (staffId != '0') {
                    var chatMsg = $('input#chatMsg').val().trim();
                    if (chatMsg != '') {
                        $('input#chatMsg').val('');
                        var chatData = {
                            customerId: customerId,
                            customerName: $('input#fullNameLoginId').val(),
                            customerAvatar: $('input#avatarLoginId').val(),
                            staffId: staffId,
                            staffName: $('input#staffName').val(),
                            staffAvatar: $('input#staffAvatar').val(),
                            msg: chatMsg,
                            fileUrl: '',
                            isCustomerSend: 1
                        };
                        socket.emit('new message', chatData);
                    }
                }
                else showNotification('Vui lòng chọn 1 nhân viên Chăm sóc Khách hàng để chat', 0);
            }
            else {
                showNotification('Vui lòng đăng nhập để chat', 0);
                if ($('#login_Modal').length > 0) $('#login_Modal').modal('show');
            }
            e.preventDefault();
        });

        //send file in chat
        //truoc mat la image
        $('#btnChatSendFile').click(function () {
            var customerId = $('input#userLoginId').val();
            if (customerId != '0') {
                var staffId = $('input#staffId').val();
                if (staffId != '0') {
                    var finder = new CKFinder();
                    finder.resourceType = 'Products';
                    finder.selectActionFunction = function (fileUrl) {
                        var chatData = {
                            customerId: customerId,
                            customerName: $('input#fullNameLoginId').val(),
                            customerAvatar: $('input#avatarLoginId').val(),
                            staffId: staffId,
                            staffName: $('input#staffName').val(),
                            staffAvatar: $('input#staffAvatar').val(),
                            msg: '',
                            fileUrl: fileUrl,
                            isCustomerSend: 1
                        };
                        socket.emit('new message', chatData);
                    };
                    finder.popup();
                }
                else showNotification('Vui lòng chọn 1 nhân viên Chăm sóc Khách hàng để chat', 0);
            }
            else {
                showNotification('Vui lòng đăng nhập để chat', 0);
                if ($('#login_Modal').length > 0) $('#login_Modal').modal('show');
            }
        });

        //phan trang chat
        $("#listChat").scroll(function () {
            if ($(this).scrollTop() == 0) {
                var totalChatMsg = parseInt($('input#totalChatMsg').val());
                if (totalChatMsg % 20 == 0) {
                    $('input#startChatPagging').val(20 + parseInt($('input#startChatPagging').val()));
                    getListChat('');
                }
            }
        });

        $('input#chatMsg').focus(function () {
            var staffId = parseInt($('input#staffId').val());
            if (staffId > 0) {
                var count = '0';
                if (flag == 1) count = $('#listChatStaff .box-comment[data-id="' + staffId + '"] span.text-muted').attr('data-count');
                else count = $('span#countChatUnRead').text();
                if (count != '0') {
                    $.ajax({
                        type: "POST",
                        url: $('input#updateCountChatUnReadUrl').val(),
                        data: {
                            CustomerId: $('input#userLoginId').val(),
                            StaffId: staffId,
                            IsStaff: 0
                        },
                        success: function (response) {
                            if (response == 1) {
                                $('#listChat .direct-chat-msg').removeClass('unread');
                                if (flag == 1) $('#listChatStaff .box-comment[data-id="' + staffId + '"] span.text-muted').text('').attr('data-count', '0');
                                else $('span#countChatUnRead').text('0').attr('data-original-title', '0 Tin Nhắn chưa đọc');
                                getListChatHeader();
                            }
                        },
                        error: function (response) {}
                    });
                }
            }
        });
    }
});

function getListChatHeader(){
    var customerId = $('input#userLoginId').val();
    if(customerId != '0'){
        $.ajax({
            type: "POST",
            url: $('input#getListChatUnReadUrl').val(),
            data: {
                UserId: customerId,
                IsStaff: 0
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var n = data.length;
                    $('span.countMsgChat').text(n);
                    var userImagePath = $('input#userImagePath').val();
                    var chatPageUrl = $('a#aViewAllChat').attr('href');
                    for (var i = 0; i < n; i++) {
                        html += '<li><a href="' + chatPageUrl + '" data-id="' + data[i].StaffId + '" data-name="' + data[i].FullName + '" data-avatar="' + data[i].Avatar + '"><div class="pull-left">';
                        html += '<img src="' + userImagePath + data[i].Avatar + '" class="img-circle" alt="' + data[i].FullName + '">';
                        html += '</div><h4>' + data[i].FullName + '<small><i class="fa fa-clock-o"></i> ' + data[i].CrTime + '<br>' + data[i].CrDate + '</small></h4>';
                        html += '<p>' + data[i].Message.replace(/<\/?[^>]+(>|$)/g, "") + '</p></a></li>';
                    }
                    $('#ulChatHeader').html(html);
                    appendCountToPageTitle();
                }
            },
            error: function (response) {}
        });
    }
}

function getListStaffChat(socket){
    var customerId = $('input#userLoginId').val();
    if(customerId != '0'){
        var eleCustomer = $('#listChatStaff');
        eleCustomer.html('<img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter">');
        socket.emit('user online', {
            roleId: 1
        });
        $.ajax({
            type: "POST",
            url: $('#seachStaffChatForm').attr('action'),
            data: {
                CustomerId: customerId,
                StaffName: $('input#txtStaffChatName').val().trim()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var userImagePath = $('input#userImagePath').val();
                    for (var i = 0; i < data.length; i++) {
                        if(data[i].StaffId > 0 && data[i].CustomerId > 0) {
                            html += '<div class="box-comment' + ((data[i].CountMsg != '0') ? ' unread' : '') + '" data-id="' + data[i].StaffId + '" data-name="' + data[i].StaffName + '" data-avatar="' + data[i].StaffAvatar + '">';
                            html += '<img class="img-circle img-sm" src="' + userImagePath + data[i].StaffAvatar + '" alt="' + data[i].StaffName + '">';
                            html += '<div class="comment-text"><span class="username">' + data[i].StaffName + '<span class="offline"></span>';
                            html += '<span class="text-muted pull-right" data-count="' + data[i].CountMsg + '">' + ((data[i].CountMsg != '0') ? (data[i].CountMsg + ' Tin nhắn') : '') + '</span>';
                            if (data[i].IsCustomerSend == 0) html += '</span><span class="lastMsgChat">' + data[i].Message + '</span></div></div>';
                            else html += '</span><span class="lastMsgChat">You: ' + data[i].Message + '</span></div></div>';
                        }
                    }
                    eleCustomer.html(html);
                    eleCustomer.slimScroll({
                        height: '400px',
                        alwaysVisible: true,
                        wheelStep: 20,
                        touchScrollStep: 500
                    });
                    socket.on('staff online', function (staffOnlines) {
                        for(var i = 0; i < staffOnlines.length; i++){
                            $('.box-comments .box-comment[data-id="' + staffOnlines[i] + '"] span.offline').removeClass('offline').addClass('online');
                        }
                    });
                }
            },
            error: function (response) {}
        });
    }
}

function getListChat(htmlGreet){
    var customerId = $('input#userLoginId').val();
    var staffId = $('input#staffId').val();
    if(staffId != '0' && customerId != '0'){
        var startChatPagging = $('input#startChatPagging').val();
        $.ajax({
            type: "POST",
            url: $('input#getChatUrl').val(),
            data: {
                CustomerId: customerId,
                StaffId: staffId,
                Limit: 20,
                Start: startChatPagging
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var n = data.length;
                    $('input#totalChatMsg').val(n + parseInt($('input#totalChatMsg').val()));
                    var divClass = '';
                    var userImagePath = $('input#userImagePath').val();
                    var imagePath = $('input#imagePath').val();
                    var staffName = $('input#staffName').val();
                    var staffAvatar = $('input#staffAvatar').val();
                    var customerName = $('input#fullNameLoginId').val();
                    var customerAvatar = $('input#avatarLoginId').val();
                    for (var i = 0; i < n; i++) {
                        if(data[i].IsCustomerSend == 1) divClass = ' right';
                        else if(data[i].IsCustomerRead == 0) divClass = ' unread';
                        else divClass = '';
                        html += '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + ((data[i].IsCustomerSend == 1) ? customerName : staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data[i].CrDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + ((data[i].IsCustomerSend == 1) ? customerAvatar : staffAvatar) + '" alt="">';
                        if(data[i].Message != '') html += '<div class="direct-chat-text">' + data[i].Message + '</div></div>';
                        else if(data[i].FileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data[i].FileUrl + '" target="_blank"><img src="' + imagePath + data[i].FileUrl + '" alt=""></a></div></div>';
                    }
                    if(startChatPagging == '0'){
                        if(htmlGreet != '' && html == '') html += htmlGreet;
                        $('#listChat').html(html);
                        scrollChat(false);
                    }
                    else $('#listChat').prepend(html);
                }
            },
            error: function (response) {}
        });
    }
}

function getListStaff(socket){
    var customerId = $('input#userLoginId').val();
    //if(customerId != '0'){
        socket.emit('user online', {
            roleId: 1
        });
        $.ajax({
            type: "POST",
            url: $('input#getListStaffUrl').val(),
            data: {
                CustomerId: customerId,
                StaffName: ''
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var userImagePath = $('input#userImagePath').val();
                    var email = $('#aSysEmail').text();
                    var phoneNumber = $('#spanSysPhone').text();
                    for (var i = 0; i < data.length; i++) {
                        //if(data[i].StaffId > 0 && data[i].CustomerId > 0) {
                        if(data[i].StaffId > 0){
                            html += '<li><a href="javascript:void(0)" data-id="' + data[i].StaffId + '" data-name="' + data[i].StaffName + '" data-avatar="' + data[i].StaffAvatar + '">';
                            html += '<img class="contacts-list-img" src="' + userImagePath + data[i].StaffAvatar +'" alt="' + data[i].StaffName + '">';
                            html += '<div class="contacts-list-info">';
                            html += '<span class="contacts-list-name">' + data[i].StaffName + '<br/>' + phoneNumber + '<span class="offline"></span></span>';
                            html += '<span class="text-muted pull-right" data-count="' + data[i].CountMsg + '">' + ((data[i].CountMsg != '0') ? (data[i].CountMsg + ' Tin nhắn') : '') + '</span>';
                            html += '<span class="contacts-list-msg">' + email + '</span>';
                            html += '</div></a></li>';
                        }
                    }
                    var ulStaffs = $('ul#chat-staff-care-list');
                    ulStaffs.html(html);
                    ulStaffs.slimScroll({
                        height: '250px',
                        alwaysVisible: true,
                        wheelStep: 20,
                        touchScrollStep: 500
                    });
                    socket.on('staff online', function (staffOnlines) {
                        for(var i = 0; i < staffOnlines.length; i++){
                            ulStaffs.find('li a[data-id="' + staffOnlines[i] + '"] span.offline').removeClass('offline').addClass('online');
                        }
                    });
                }
            },
            error: function (response) {}
        });
    //}
}

function prependCustomerChat(data){
    var eleBoxComment = $('#listChatStaff div.box-comment[data-id="' + data.staffId + '"]');
    var span = eleBoxComment.find('span.text-muted');
    var count = parseInt(span.attr('data-count')) + 1;
    span.text(count + ' Tin nhắn').attr('data-count', count);
    var msg = '';
    if (data.msg != '') msg = data.msg;
    else if (data.fileUrl != '') msg = 'Gửi ảnh...';
    eleBoxComment.find('span.lastMsgChat').text(msg);
    eleBoxComment.addClass('unread');
    var html = eleBoxComment[0].outerHTML;
    eleBoxComment.remove();
    $('#listChatStaff').prepend(html);
}

function changeStateUser(userId, roleId, state){
    var socket = io($('input#chatServerUrl').val());
    socket.emit('user state', {
        userId: userId,
        roleId: roleId,
        state: state
    });
}