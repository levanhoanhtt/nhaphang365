$(document).ready(function () {
    //global variable
    var serviceFees = getServiceFees();
    var exchangeRate = replaceCost($('input#exchangeRate').val());
    var noProductImage = $('input#noProductImage').val();
    var shipTQType = parseInt($('input#shipTQType').val());
    /*
     shipTQType = 0: ShipTQ la ?, KH ko dc sua - customer/index
     shipTQType = 1: NV dc sua - order/add
     shipTQType = 2: NV dc sua, thong bao het hang, tang gia (giao dien cua NV sua don hang) - order/edit...
     shipTQType = 3: KH sua don hang
    */
    var unitHeight = (shipTQType == 0 || shipTQType == 1) ? 82 : 105;

    //scroll shop======================================
    var liShop = $('.box-order-shop ul li');
    if(liShop.length > 0) {
        liShop.tooltip();
        $('.box-order-shop').css('height', (22 * liShop.length) + 'px');
        $('.box-order-shop').show();
        setToolTip();
        mergeByLink(shipTQType);
        if(shipTQType == 2 || shipTQType == 3) formatCostPart('cost4');
        setTotalPrice(exchangeRate, serviceFees, shipTQType);
    }
    else $('.box-order-shop').hide();
    $('.box-order-shop').on('click', 'li', function(){
        scrollTo('#li_' + $(this).attr('data-id'));
    });
    var objIdScroll = parseInt($('input#productIdScroll').val());
    if(objIdScroll > 0){
        var tr = $('.tr_product[data-product-id="' + objIdScroll + '"]');
        if(tr.length > 0){
            $('html, body').animate({
                scrollTop: tr.offset().top - 700
            }, 1000);
        }
    }
    objIdScroll = $('input#shopCodeScroll').val();
    if(objIdScroll != '' && objIdScroll != 'shopcode'){
        $('.box-order-shop ul li').each(function(){
            if($(this).attr('data-id') == objIdScroll){
                $('html, body').animate({
                    scrollTop: $('#li_' + objIdScroll).offset().top - 300
                }, 1000);
                return false;
            }
        });
    }

    //tootip order comment=============================
    if($('#aOrderComment').length > 0) $('#aOrderComment').tooltip({placement: 'bottom'});

    //iCheck==========================================
    if($('input.iCheck').length > 0) {
        $('input.iCheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
        $('input#cbIsFixPercent').on('ifToggled', function(e){
            if(e.currentTarget.checked){
                $('#servicePercent').prop('disabled', false);
                $('input#isFixPercent').val('1');
            }
            else{
                $('#servicePercent').prop('disabled', true);
                $('input#isFixPercent').val('0');
                setTotalPrice(exchangeRate, serviceFees, shipTQType);
            }
        });
    }

    commonAction(exchangeRate, serviceFees, shipTQType, noProductImage, unitHeight);

    //keyup exchangeRate=====================================
    $('input#exchangeRate').keyup(function(){
        exchangeRate = replaceCost($(this).val());
        setTotalPrice(exchangeRate, serviceFees, shipTQType);
    });

    //keyup service=====================================
    $('#servicePercent').change(function(){
        var servicePercent = replaceCost($(this).val());
        if(servicePercent <= 0) servicePercent = 0;
        if(servicePercent >= 100) servicePercent = 100;
        if(servicePercent == 0 || servicePercent == 100) $(this).val(servicePercent);
        var costTQ = replaceCost1($('#costTQ').val());
        var costVN = Math.ceil(costTQ * exchangeRate);
        var serviceFee = Math.ceil(costVN * servicePercent/100);
        $('input#serviceFee').val(serviceFee);
        var costFee = replaceCost($('input#costFee').val());
        var sumCost = costVN + serviceFee + costFee;
        $('input#sumCost').val(sumCost);
        formatCost();
    });

    //keyup cost fee=====================================
    $('input#costFee').keyup(function(){
        var costTQ = replaceCost1($('input#costTQ').val());
        var serviceFee = replaceCost($('input#serviceFee').val());
        var sumCost = Math.ceil(costTQ * exchangeRate) + serviceFee + replaceCost($(this).val());
        $('input#sumCost').val(sumCost);
        formatCost();
    });

    //merge order========================================
    $('#aMergeOreder').click(function(){
        var orderStatusId = parseInt($('input#orderStatusId').val());
        if(orderStatusId < 4) {
            $.ajax({
                type: "POST",
                url: $('input#getListOrderUrl').val(),
                data: {
                    CustomerId: $('input#customerId').val(),
                    OrderStatusIds: '1,2,3'
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        var html = '<option value="0">Vui lòng chọn</option>';
                        var orderId = $('input#orderId').val();
                        var data = json.data;
                        for (var i = 0; i < data.length; i++){
                            if(orderId != data[i].OrderId) html += '<option value="' + data[i].OrderId + '">' + data[i].OrderCode + '</option>';
                        }
                        $('select#mergeOrderId').html(html);
                        $('#modalMergeOrder').modal('show');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else showNotification('Đơn hàng này không gộp được', 0);
        return false;
    });
    $('#btnMergeOrder').click(function(){
        var orderId = parseInt($('input#orderId').val());
        var mergeOrderId = parseInt($('select#mergeOrderId').val());
        if(orderId > 0 && mergeOrderId > 0){
            $('#btnMergeOrder').prop('disabled', true);
            var careStaffId = 0;
            if($('input#careStaffId').length > 0) careStaffId = $('input#careStaffId').val();
            else if($('select#careStaffId').length > 0) careStaffId = $('select#careStaffId').val();
            $.ajax({
                type: "POST",
                url: $('input#mergeOrderUrl').val(),
                data: {
                    OrderId: orderId,
                    MergeOrderId: mergeOrderId,
                    CareStaffId: careStaffId,
                    CustomerId: $('input#customerId').val(),
                    CustomerName: $('input#fullName').val(),
                    OrderCode: $('#tdOrderCode').text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                    else $('#btnMergeOrder').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('#btnMergeOrder').prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn đơn hàng gộp cùng', 0);
    });
});

function getServiceFees() {
    var retVal = [];
    var serviceFee = {};
    $('input.serviceFee').each(function () {
        serviceFee = {
            min: parseInt($(this).attr('data-min')),
            max: parseInt($(this).attr('data-max')),
            rate: parseFloat($(this).val())
        };
        retVal.push(serviceFee);
    });
    return retVal;
}

function mergeByLink(shipTQType){
    var tbodyProductId = '';
    var dataProductOld = '0';
    var dataProduct = 0;
    var rowspan = 1;
    var heightInput = 0;
    var html = '';
    var quantity = 0;
    var cost = 0;
    var shipTQ = 0;
    $('ul.timeline  li').each(function(){
        var liShop = $(this);
        tbodyProductId = '';
        dataProductOld = '0';
        dataProduct = 0;
        rowspan = 1;
        heightInput = 0;
        quantity = 0;
        cost = 0;
        shipTQ = 0;
        liShop.find('.tbodyProduct .tr_product').each(function(){
            dataProduct =  $(this).attr('data-product');
            tbodyProductId = '#tbodyProduct_' + $(this).attr('data-shop');
            if(shipTQType == 0) shipTQ = '?';
            else shipTQ = replaceCost1($(tbodyProductId + ' .shipTQ').val().trim());
            if(dataProduct != dataProductOld) {
                dataProductOld = dataProduct;
                rowspan = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').length;
                if(rowspan > 1){
                    var trFirst = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').first();
                    trFirst.find('.tdProductName').attr('rowspan', rowspan);
                    $(tbodyProductId + ' tr[data-product="' + dataProduct + '"] td[rowspan="1"]').remove();
                    heightInput =  trFirst.find('.tdProductName').height();
                    trFirst.find('.productName').css('height', heightInput);
                }
            }
            quantity += replaceCost($(this).find('input.soluong').val().trim());
            cost += replaceCost1($(this).find('input.thanhtien').val().trim());
        });
        //add tr sum bottom each shop=================================
        html = '<tr class="tr_product_sun"><td></td><td>Tổng</td><td></td><td></td>';
        html += '<td class="td_sum_quantity">' + formatDecimal(quantity.toString()) + '</td><td></td>';
        html += '<td class="td_sum_cost">' + formatDecimal(cost.toFixed(2).toString()) + '</td>';
        html += '<td class="td_sum_ship">' + formatDecimal(shipTQ.toString()) + '</td>';
        var sumCost = cost;
        if(shipTQ != '?') sumCost += shipTQ;
        html += '<td class="td_sum_cost_ship">' + formatDecimal(sumCost.toFixed(2).toString()) + '</td><td></td></tr>';
        liShop.find('.tbodyProduct').append(html);
    });
}

function commonAction(exchangeRate, serviceFees, shipTQType, noProductImage, unitHeight ){
    //popup anh========================================
    $('.tbodyProduct').on('click', 'img.productImage', function () {
        var imageRow = $(this).attr('data-id');
        var imageShop = $(this).attr('data-shop');
        $('input#imageRow').val(imageRow);
        $('input#imageShop').val(imageShop);
        var imageSrc = $(this).attr('src');
        $('img#imgProduct').attr('src', imageSrc);
        $('#modalProductImage').modal('show');
    });
    $('#btnUploadImage').click(function () {
        var finder = new CKFinder();
        finder.resourceType = 'Products';
        finder.selectActionFunction = function (fileUrl) {
            $('input#productImageUrl').val(fileUrl);
            $('img#imgProduct').attr('src', fileUrl);
        };
        finder.popup();
    });
    $('input#productImageUrl').blur(function () {
        var fileUrl = $(this).val().trim();
        if (fileUrl != '') $('img#imgProduct').attr('src', fileUrl);
    });
    $('#btnUpdateImage').click(function () {
        var productImageUrl = $('input#productImageUrl').val().trim();
        if (productImageUrl == '') productImageUrl = noProductImage;
        var imageRow = $('input#imageRow').val();
        var imageShop = $('input#imageShop').val();
        $('.tbodyProduct').find('img#productImage_' + imageRow + '_' + imageShop).attr('src', productImageUrl);
        $('input#productImageUrl').val('');
        $('img#imgProduct').attr('src', noProductImage);
        $('input#imageRow').val('0');
        $('input#imageShop').val('');
        $('#modalProductImage').modal('hide');
    });

    //link product=======================================
    $('.tbodyProduct').on('keyup', 'tr td.tdProductName .productName', function(){
        $(this).parent().find('.linkproductName').text('');
    });
    $('.tbodyProduct').on('change', 'tr td.tdProductName .productName', function(){
        var link = $(this).val();
        $(this).val('');
        $(this).parent().find('.linkproductName').attr('href', link);
        $(this).parent().find('.linkproductName').text(link);
    });

    //add same product link===============================
    $('.tbodyProduct').on('click', 'i.link_add_product', function () {
        var tr = $(this).parent().parent();
        var dataProduct = tr.attr('data-product');
        var shopCode = tr.attr('data-shop');
        var tbodyProductId = '#tbodyProduct_' + shopCode;
        var rowImage = $(tbodyProductId + ' .tr_product').length + 1;

        var html = '';
        html += '<tr data-product="' + dataProduct + '" data-shop="' + shopCode + '" data-product-id="0" data-status="2" class="tr_product tr_product_status_2">';
        html += '<td><img class="productImage" id="productImage_' + rowImage + '_' + shopCode + '" src="' + noProductImage + '" data-id="' + rowImage + '" data-shop="' + shopCode + '"></td>';
        html += '<td class="tdAttr"><input type="text" class="form-control productColor "><i class="fa fa-plus link_add_product" ></i></td>';
        html += '<td class="tdAttr"><input type="text" class="form-control productSize"><i class="fa fa-plus link_add_product" ></i></td>';
        html += '<td class="tdQuantity"><input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="0"></td>';
        html += '<td class="tdCost"><input type="text" class="form-control cost1 gia hmdrequired hmdrequiredCost1" data-field="Giá sản phẩm" value="0" ></td>';
        html += '<td><input type="text" class="form-control cost1 thanhtien" value="0" disabled></td>';
        if(shipTQType == 1) html += '<td><textarea rows="3" class="form-control feedback"></textarea></td>';
        else if(shipTQType == 2){
            html += '<td><p><b>Khách</b>: <span class="comment cmt" data-title=""></span><i class="fa fa-pencil" title=""></i></p>';
            html += '<b>Hệ thống</b>:<textarea rows="2" class="form-control feedback cmt"></textarea></td>';
        }
        else if(shipTQType == 3){
            html += '<td><p><b>Hệ thống</b>: <span class="feedback cmt" data-title=""></span><i class="fa fa-pencil" title=""></i></p>';
            html += '<b>Khách</b>:<textarea rows="2" class="form-control comment cmt"></textarea></td>';
        }
        else html += '<td><textarea rows="3" class="form-control comment"></textarea></td>';
        html += '<td><i class="fa fa-trash-o link_delete"></i></td>';
        html += '</tr>';

        var htmlTrSum = $(tbodyProductId).find('.tr_product_sun').html();
        $(tbodyProductId).find('.tr_product_sun').remove();
        $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').last().after(html);

        var rowspan = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').length;
        var trFirst = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').first();
        trFirst.find('.tdProductName').attr('rowspan', rowspan);

        var heightInput = rowspan * unitHeight;
        trFirst.find('.productName').css('height', heightInput);

        trFirst = $(tbodyProductId + ' .tr_product').first();
        rowspan = $(tbodyProductId + ' .tr_product').length;
        heightInput = rowspan * unitHeight + (rowspan - 1);
        trFirst.find('.tdShipTQ').attr('rowspan', rowspan);
        trFirst.find('.shipTQ').css('height', heightInput);

        formatCost();
        $(tbodyProductId).append('<tr class="tr_product_sun">' + htmlTrSum + '</tr>');
    });

    //add product=============================
    $('.timeline-body').on('click', '.btn-add-product', function(){
        var shopCode = $(this).attr('data-id');
        var tbodyProductId = '#tbodyProduct_' + shopCode;
        var rowImage = $(tbodyProductId + ' .tr_product').length + 1;
        var dataProduct = $(tbodyProductId + ' .tdProductName').length + 1;
        var html = '';
        html += '<tr data-product="' + dataProduct + '" data-shop="' + shopCode + '" data-product-id="0" data-status="2" class="tr_product tr_product_status_2">';
        html += '<td><img class="productImage" id="productImage_' + rowImage + '_' + shopCode + '" src="' + noProductImage + '" data-id="' + rowImage + '" data-shop="' + shopCode + '"></td>';
        html += '<td class="tdProductName" rowspan="1"><a target="_blank" href="javascript:void(0)" class="linkproductName"></a><textarea rows="3" class="form-control productName"></textarea></td>';
        html += '<td class="tdAttr"><input type="text" class="form-control productColor "><i class="fa fa-plus link_add_product" ></i></td>';
        html += '<td class="tdAttr"><input type="text" class="form-control productSize"><i class="fa fa-plus link_add_product" ></i></td>';
        html += '<td class="tdQuantity"><input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="0"></td>';
        html += '<td class="tdCost"><input type="text" class="form-control cost1 gia hmdrequired hmdrequiredCost1" data-field="Giá sản phẩm" value="0" ></td>';
        html += '<td><input type="text" class="form-control cost1 thanhtien" value="0" disabled></td>';
        if(rowImage == 1){
            if(shipTQType == 0) html += '<td class="tdShipTQ" rowspan="1"><input type="text" disabled value="?" class="form-control cost1 shipTQ"></td>';
            else if(shipTQType == 1 || shipTQType == 2 || shipTQType == 3) html += '<td class="tdShipTQ" rowspan="1"><input type="text" value="0" class="form-control cost1 shipTQ"></td>';
            else html += '<td class="tdShipTQ" rowspan="1"><input type="text" disabled value="?" class="form-control cost1 shipTQ"></td>';
        }
        if(shipTQType == 1) html += '<td><textarea rows="3" class="form-control comment"></textarea></td>';
        else if(shipTQType == 2){
            html += '<td><p><b>Khách</b>: <span class="comment cmt" data-title=""></span><i class="fa fa-pencil" title=""></i></p>';
            html += '<b>Hệ thống</b>:<textarea rows="2" class="form-control feedback cmt"></textarea></td>';
        }
        else if(shipTQType == 3){
            html += '<td><p><b>Hệ thống</b>: <span class="feedback cmt" data-title=""></span><i class="fa fa-pencil" title=""></i></p>';
            html += '<b>Khách</b>:<textarea rows="2" class="form-control comment cmt"></textarea></td>';
        }
        else html += '<td><textarea rows="3" class="form-control comment"></textarea></td>';
        html += '<td><i class="fa fa-trash-o link_delete"></i></td>';
        html += '</tr>';
        var htmlTrSum = $(tbodyProductId).find('.tr_product_sun').html();
        $(tbodyProductId).find('.tr_product_sun').remove();
        $(tbodyProductId).append(html);
        if(rowImage > 1) {
            var trFirst = $(tbodyProductId + ' .tr_product').first();
            var rowspan = parseInt(trFirst.find('.tdShipTQ').attr('rowspan'));
            var heightInput = (rowspan + 1) * unitHeight + rowspan;
            trFirst.find('.tdShipTQ').attr('rowspan', rowspan + 1);
            trFirst.find('.shipTQ').css('height', heightInput);
        }
        formatCost();
        $(tbodyProductId).append('<tr class="tr_product_sun">' + htmlTrSum + '</tr>');
    });

    //delete product============================================
    $('.tbodyProduct').on('click', '.link_delete', function () {
        var tr = $(this).parent().parent();
        var dataProduct = tr.attr('data-product');
        var shopCode = tr.attr('data-shop');
        var tbodyProductId = '#tbodyProduct_' + shopCode;
        var htmlTrSum = $(tbodyProductId).find('.tr_product_sun').html();
        $(tbodyProductId).find('.tr_product_sun').remove();
        var rowspan = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').length - 1;
        var trFirst = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').first();
        if (trFirst.is(tr)) {
            var productName = tr.find('.tdProductName .linkproductName').text();
            var shipTQ = '?';
            if(shipTQType == 1 || shipTQType == 2 || shipTQType == 3) shipTQ = $(tbodyProductId).find('.shipTQ').val();
            tr.remove();
            var trFirst1 = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').first();
            trFirst1.find('td:nth-child(2)').before('<td class="tdProductName" rowspan="' + rowspan + '"><a target="_blank" href="' + productName + '" class="linkproductName">' + productName + '</a><textarea rows="3" class="form-control productName"></textarea></td>');
        }
        else {
            tr.remove();
            trFirst.find('.tdProductName').attr('rowspan', rowspan);
        }
        var heightInput = trFirst.find('.tdProductName').height();
        heightInput = heightInput/(rowspan+1)*rowspan;
        trFirst.find('.productName').css('height', heightInput);

        rowspan = $(tbodyProductId + ' .tr_product').length;
        trFirst = $(tbodyProductId + ' .tr_product').first();
        if(trFirst.find('.tdShipTQ').length == 0){
            if(shipTQType == 0) trFirst.find('td:nth-child(8)').before('<td class="tdShipTQ" rowspan="' + rowspan + '"><input type="text" disabled value="' + shipTQ + '" class="form-control cost1 shipTQ"></td>');
            else if(shipTQType == 1 || shipTQType == 2 || shipTQType == 3) trFirst.find('td:nth-child(8)').before('<td class="tdShipTQ" rowspan="' + rowspan + '"><input type="text" value="' + shipTQ + '" class="form-control cost1 shipTQ"></td>');
            else trFirst.find('td:nth-child(8)').before('<td class="tdShipTQ" rowspan="' + rowspan + '"><input type="text" disabled value="' + shipTQ + '" class="form-control cost1 shipTQ"></td>');
        }
        else trFirst.find('.tdShipTQ').attr('rowspan', rowspan);
        heightInput = rowspan * unitHeight + rowspan;
        trFirst.find('.shipTQ').css('height', heightInput);

        setTotalPrice(exchangeRate, serviceFees, shipTQType);
        $(tbodyProductId).append('<tr class="tr_product_sun">' + htmlTrSum + '</tr>');
        setProductSum($(tbodyProductId));
        if(shipTQType == 0 || shipTQType == 1) saveOrderDraff(noProductImage, exchangeRate, shipTQType, false);
    });

    //keyup quantity, price, shipTQ==========================
    keyUpInput(exchangeRate, serviceFees, shipTQType);
}

function keyUpInput(exchangeRate, serviceFees, shipTQType){
    $('.tbodyProduct').on('keyup', 'input.soluong, input.gia', function () {
        var soluong = 0;
        var gia = 0;
        var thanhtien = 0;
        $('.tbodyProduct .tr_product').each(function () {
            soluong = replaceCost($(this).find('input.soluong').val());
            gia = replaceCost1($(this).find('input.gia').val());
            thanhtien = soluong * gia;
            if(thanhtien.toString().indexOf('.') > 0) thanhtien = thanhtien.toFixed(2);
            $(this).find('input.thanhtien').val(thanhtien);
        });
        setProductSum($(this).parent().parent().parent());
        setTotalPrice(exchangeRate, serviceFees, shipTQType);
    });
    if(shipTQType == 1 || shipTQType == 2 || shipTQType == 3){
        $('.tbodyProduct').on('keyup', 'input.shipTQ', function(){
            var tbody = $(this).parent().parent().parent();
            tbody.find('.td_sum_ship').text(formatDecimal($(this).val()));
            setProductSum(tbody);
            setTotalPrice(exchangeRate, serviceFees, shipTQType);
        });
    }
}

function setProductSum(tbody){
    var soluong = 0;
    var thanhtien = 0;
    tbody.find('.tr_product').each(function(){
        soluong += replaceCost($(this).find('input.soluong').val());
        thanhtien += replaceCost1($(this).find('input.thanhtien').val());
    });
    tbody.find('.td_sum_quantity').text(formatDecimal(soluong.toString()));
    tbody.find('.td_sum_cost').text(formatDecimal(thanhtien.toFixed(2).toString()));
    tbody.find('.td_sum_cost_ship').text(formatDecimal((thanhtien + replaceCost1(tbody.find('input.shipTQ').val())).toFixed(2).toString()));
}

function setTotalPrice(exchangeRate, serviceFees, shipTQType) {
    var costTQ = 0;
    $('.tbodyProduct .tr_product').each(function () {
        costTQ += replaceCost1($(this).find('input.thanhtien').val());
    });
    if(shipTQType == 1 || shipTQType == 2 || shipTQType == 3){
        $('.tbodyProduct .shipTQ').each(function () {
            costTQ += replaceCost1($(this).val());
        });
    }
    if(costTQ.toString().indexOf('.') > 0) costTQ = costTQ.toFixed(2);
    $('#costTQ').val(costTQ);
    var costVN = Math.ceil(costTQ * exchangeRate);
    $('#costVN').val(costVN);
    var serviceFee = 0;
    var isFixPercent = $('input#isFixPercent').val();
    if(isFixPercent == '0') {
        for (var i = 0; i < serviceFees.length; i++) {
            if (costVN >= serviceFees[i].min && costVN < serviceFees[i].max) {
                $('#servicePercent').val(Math.floor(serviceFees[i].rate * 100));
                serviceFee = Math.ceil(costVN * serviceFees[i].rate);
                break;
            }
        }
    }
    else serviceFee = Math.ceil(costVN * parseInt($('#servicePercent').val()) / 100);
    $('#serviceFee').val(serviceFee);
    $('#sumCost').val(costVN + serviceFee + replaceCost($('input#costFee').val()));
    formatCost();
}

function setToolTip(){
    $('.tbodyProduct i.fa-pencil').each(function(){
        if($(this).attr('title') != '') $(this).css('color', 'red');
    });
    $('.tbodyProduct i.fa-pencil').tooltip();
}

function getListProducts(noProductImage, noCheck, shipTQType) {
    var retVal = [];
    var product = {};
    var shopCode = '';
    var shopName = '';
    var shopUrl = '';
    var categoryId = 0;
    var productLink = '';
    var productImage = '';
    var productColor = '';
    var productSize = '';
    var quantity = 0;
    var cost = 0;
    var shipTQ = 0;
    var comment = '';
    var feedBack = '';
    var id = 0;
    var productId = 0;
    var productStatusId = 2;
    var tbodyProductId = '';
    $('.tbodyProduct .tr_product').each(function () {
        id = $(this).attr('data-product');
        productId = parseInt($(this).attr('data-product-id'));
        productStatusId = parseInt($(this).attr('data-status'));
        shopCode = $(this).attr('data-shop');
        shopName = $('span#shopName_' + shopCode).text();
        shopUrl = $('a#shopUrl_' + shopCode).attr('href');
        categoryId = parseInt($('select#categoryId_' + shopCode).val());
        tbodyProductId = '#tbodyProduct_' + shopCode;
        if(shopCode == 'NoShopHMD') shopCode = '';
        //var trFirst = $(tbodyProductId + ' tr[data-product="' + id + '"]').first();
        productLink = $(tbodyProductId + ' tr[data-product="' + id + '"]').first().find('.tdProductName .linkproductName').text();
        productImage = $(this).find('img.productImage').attr('src');
        if (productImage == noProductImage) productImage = '';
        productColor = $(this).find('input.productColor').val().trim();
        productSize = $(this).find('input.productSize').val().trim();
        quantity = replaceCost($(this).find('input.soluong').val().trim());
        cost = replaceCost1($(this).find('input.gia').val().trim());
        if(shipTQType == 0){
            shipTQ = 0;
            comment = $(this).find('textarea.comment').val().trim();
            feedBack = '';
        }
        else if(shipTQType == 1){
            shipTQ = replaceCost1($(tbodyProductId).find('.shipTQ').val().trim());
            comment = $(this).find('textarea.comment').val().trim();
            feedBack = '';
        }
        else if(shipTQType == 2){
            shipTQ = replaceCost1($(tbodyProductId).find('.shipTQ').val().trim());
            comment = $(this).find('span.comment').attr('data-title');
            feedBack = $(this).find('textarea.feedback').val().trim();
        }
        else if(shipTQType == 3){
            shipTQ = replaceCost1($(tbodyProductId).find('.shipTQ').val().trim());
            comment = $(this).find('textarea.comment').val().trim();
            feedBack = $(this).find('span.feedback').attr('data-title');
        }
        if (productLink != '' && quantity > 0 && cost >= 0) {
            product = {
                ProductId: productId,
                ShopCode: shopCode,
                ShopName: shopName,
                ShopUrl: shopUrl,
                CategoryId: categoryId,
                ShopComment: '',
                ShopFeedback: '',
                ProductImage: productImage,
                ProductLink: productLink,
                Color: productColor,
                Size: productSize,
                Quantity: quantity,
                Cost: cost,
                ShipTQ: shipTQ,
                ProductStatusId: productStatusId,
                Comment: comment,
                FeedBack: feedBack
            };
            retVal.push(product);
        }
        else if(noCheck){
            product = {
                ProductId: productId,
                ShopCode: shopCode,
                ShopName: shopName,
                ShopUrl: shopUrl,
                CategoryId: categoryId,
                ShopComment: '',
                ShopFeedback: '',
                ProductImage: productImage,
                ProductLink: productLink,
                Color: productColor,
                Size: productSize,
                Quantity: quantity,
                Cost: cost,
                ShipTQ: shipTQ,
                ProductStatusId: productStatusId,
                Comment: comment,
                FeedBack: feedBack
            };
            retVal.push(product);
        }
    });
    return retVal;
}

function saveOrderDraff(noProductImage, exchangeRate, shipTQType, isShowNotification){
    var careStaffId = 0;
    if($('input#careStaffId').length > 0) careStaffId = $('input#careStaffId').val();
    else if($('select#careStaffId').length > 0) careStaffId = $('select#careStaffId').val();
    var listProducts = getListProducts(noProductImage, true, shipTQType);
    $.ajax({
        type: "POST",
        url: $('input#saveOrderDraffUrl').val(),
        data: {
            UserId: $('input#userLoginId').val(),
            FullName: $('input#fullName').val().trim(),
            PhoneNumber: $('input#phoneNumber').val().trim(),
            Email: $('input#email').val().trim(),
            Address: $('input#address').val().trim(),
            CareStaffId: careStaffId,
            WarehouseId: $('select#warehouseId').val(),
            ExchangeRate: exchangeRate,
            Comment: $('textarea#comment').val().trim(),
            ProductJson: JSON.stringify(listProducts)
        },
        success: function (response) {
            if(isShowNotification){
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            }
        },
        error: function (response) {}
    });
}

function complaintAction(){
    var userRoleId = parseInt($('input#roleLoginId').val());
    if(userRoleId == 1 || userRoleId == 5) $('select#complaintOwnerId option[value="3"]').hide();
    if($('input#complaintId').val() != '0') showComplaint($('input#complaintId').val(), userRoleId, true);
    $('.tbodyProduct').on('click', 'span.complaint', function () {
        showComplaint($(this).attr('data-id'), userRoleId, true);
    });
    $('select#complaintOwnerId').change(function(){
        if(userRoleId == 3) $('input#complaintMsgTypeId').val('7');
        var complaintOwnerId = $(this).val();
        if(complaintOwnerId == '1'){
            if(userRoleId == 1){
                $('#divComplaintSolution').show();
                $('#btnUpdateComplaint').text('Gửi quản lý duyệt');
                $('input#complaintStatusId').val('3');
                $('input#complaintMsgTypeId').val('3');
            }
            else if(userRoleId == 5 || userRoleId == 3){
                $('#btnUpdateComplaint').text('Gửi chăm sóc xử lý');
                $('input#complaintStatusId').val('8');
                var flag = true;
                $('#divComplaintSolution div.icheckbox_square-blue').each(function(){
                    if($(this).hasClass('checked')) flag = false;
                });
                if(parseFloat($('input#exchangeCostTQ').val()) == 0 && parseFloat($('input#reduceCostTQ').val()) == 0 && flag) {
                    $('#divComplaintSolution').hide();
                    $('input.cbComplaintSolution').iCheck('uncheck');
                    $('#divComplaintSolutionMoney .form-group').hide();
                }
                if(userRoleId == 5) $('input#complaintMsgTypeId').val('2');
            }
        }
        else if(complaintOwnerId == '2'){
            if(userRoleId == 1 || userRoleId == 3) {
                $('#btnUpdateComplaint').text('Gửi đặt hàng xử lý');
                $('input#complaintStatusId').val('2');
                var flag = true;
                $('#divComplaintSolution div.icheckbox_square-blue').each(function(){
                    if($(this).hasClass('checked')) flag = false;
                });
                if(parseFloat($('input#exchangeCostTQ').val()) == 0 && parseFloat($('input#reduceCostTQ').val()) == 0  && flag) {
                    $('#divComplaintSolution').hide();
                    $('input.cbComplaintSolution').iCheck('uncheck');
                    $('#divComplaintSolutionMoney .form-group').hide();
                }
                if(userRoleId == 1) $('input#complaintMsgTypeId').val('1');
            }
            else if(userRoleId == 5){
                $('#divComplaintSolution').show();
                $('#btnUpdateComplaint').text('Gửi quản lý duyệt');
                $('input#complaintStatusId').val('3');
                $('input#complaintMsgTypeId').val('4');
            }
        }
        else if(complaintOwnerId == '3'){
            $('#divComplaintSolution').show();
            $('#btnUpdateComplaint').text('Cập nhật');
            $('input#complaintStatusId').val('4');
        }
    });
    $('input.cbComplaintSolution').on('ifToggled', function(e){
        if(e.currentTarget.value == 1){
            if(e.currentTarget.checked) $('#complaintSolution1').show();
            else{
                $('#complaintSolution1').hide();
                $('input#exchangeCostTQ').val('0');
            }
        }
        else if(e.currentTarget.value == 4){
            if(e.currentTarget.checked) $('#complaintSolution4').show();
            else{
                $('#complaintSolution4').hide();
                $('input#reduceCostTQ').val('0');
            }
        }
    });
    $('input.cbComplaintAgree').on('ifToggled', function(e){
        if(e.currentTarget.checked){
            if(e.currentTarget.value == 0) $('input#complaintStatusId').val('4');
            else if(e.currentTarget.value == 1) $('input#complaintStatusId').val('5');
        }
    });
    $('#divComplaintSolutionMoney').on('keyup', '.cost3', function(){
        formatCostPart('cost3');
    });
    $('#btnUpdateComplaint').click(function(){
        var curentMsgComplaint = $('#curentMsgComplaint').val().trim();
        if(curentMsgComplaint == ''){
            showNotification('Bạn chưa trả lời khiếu nại', 0);
            return false;
        }
        var complaintSolutions = [];
        if(!$('#divComplaintSolution').is(':hidden')){
            $('#divComplaintSolution div.icheckbox_square-blue').each(function(){
                if($(this).hasClass('checked')) complaintSolutions.push($(this).find('input.cbComplaintSolution').val());
            });
            if(complaintSolutions.length == 0){
                showNotification('Bạn chưa chọn Phương án xử lý', 0);
                return false;
            }
            for(var i = 0; i < complaintSolutions.length; i++){
                if(complaintSolutions[i] == 1){
                    if(parseFloat($('input#exchangeCostTQ').val()) == 0){
                        showNotification('Số tệ đền hàng phải lớn hơn 0', 0);
                        return false;
                    }
                }
                else if(complaintSolutions[i] == 4){
                    if(parseFloat($('input#reduceCostTQ').val()) == 0) {
                        showNotification('Số tệ giảm trừ phải lớn hơn 0', 0);
                        return false;
                    }
                }
            }
        }
        var complaintId = parseInt($('input#complaintId').val());
        if(complaintId > 0) {
            var isAccept = 0;
            $('#complaintCurentMsg div.iradio_square-blue').each(function () {
                if ($(this).hasClass('checked')) isAccept = $(this).find('input.cbComplaintAgree').val();
            });
            var careStaffId = 0;
            if($('input#careStaffId').length > 0) careStaffId = $('input#careStaffId').val();
            else if($('select#careStaffId').length > 0) careStaffId = $('select#careStaffId').val();
            $('#btnUpdateComplaint').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateComplaintUrl').val(),
                data: {
                    ComplaintId: complaintId,
                    OrderId: $('input#orderId').val(),
                    ProductId: $('input#productIdComplaint2').val(),
                    CustomerId: $('input#customerId').val(),
                    CareStaffId: careStaffId,
                    ComplaintStatusId: $('input#complaintStatusId').val(),
                    ComplaintOwnerId: $('select#complaintOwnerId').val(),
                    ComplaintSolutions: JSON.stringify(complaintSolutions),
                    ExchangeRate: exchangeRate,
                    ExchangeCostTQ: $('input#exchangeCostTQ').val(),
                    ReduceCostTQ: $('input#reduceCostTQ').val(),
                    UserId: $('input#userLoginId').val(),
                    Message: curentMsgComplaint,
                    ComplaintMsgTypeId: $('input#complaintMsgTypeId').val(),
                    IsAccept: isAccept,
                    OrderCode: $('#tdOrderCode').text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('#curentMsgComplaint').val('');
                        showComplaint(complaintId, userRoleId, false);
                    }
                    $('#btnUpdateComplaint').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('#btnUpdateComplaint').prop('disabled', false);
                }
            });
        }
    });
}

function showComplaint(id, userRoleId, isOpenModal){
    $('input.cbComplaintType').iCheck('uncheck');
    $('input.cbComplaintSolution').iCheck('uncheck');
    $('#divComplaintSolutionMoney .form-group').hide();
    $('#divComplaintSolutionMoney input').val('0');
    $('input#complaintStatusId').val('1');
    $('#curentMsgComplaint').val('');
    $.ajax({
        type: "POST",
        url: $('input#getComplaintDetailUrl').val(),
        data: {
            ComplaintId: id
        },
        success: function (response) {
            var json = JSON.parse(response);
            if(json.code == 1) {
                $('#customerNameComplaint').text($('input#fullName').val().trim());
                var data = json.data.Product;
                $('input#productIdComplaint2').val(data.ProductId);
                $('a#productLinkComplaint2').attr('href', data.ProductLink);
                $('a#productLinkComplaint2').text(data.ProductLink);
                $('td#productColorComplaint2').text(data.Color);
                $('td#productSizeComplaint2').text(data.Size);
                $('td#productCostComplaint2').text(data.Cost);
                $('td#productQuantityComplaint2').text(data.Quantity);
                data = json.data.Complaint;
                $('input#complaintId').val(data.ComplaintId);
                $('#crDateTimeComplaint').text(data.CrDateTime);
                //$('input#complaintStatusId').val(data.ComplaintStatusId);
                $('#commentComplaint').val(data.Comment);
                $('#spanCrDateTimeComplaint').text('Đã gửi lúc ' + data.CrDateTime + ' - ' + $('input#fullName').val().trim());
                var i, html = '', image = '';
                var imagePath = $('input#imagePath').val();
                var complaintTypes = data.ComplaintTypes;
                if(complaintTypes != ''){
                    complaintTypes = JSON.parse(complaintTypes);
                    for(i = 0; i < complaintTypes.length; i++) $('input.cbComplaintType[value="' + complaintTypes[i] + '"]').iCheck('check');
                }
                var productImages = data.ProductImage;
                if(productImages != ''){
                    html = '';
                    productImages = JSON.parse(productImages);
                    for(i = 0; i < productImages.length; i++){
                        image = imagePath + productImages[i];
                        html += '<li><a href="' + image + '" target="_blank"><img src="' + image + '"></a></li>';
                    }
                    $('#ulProduct1Complaint').html(html);
                }
                var waybillCodeImages = data.WaybillCodeImage;
                if(waybillCodeImages != ''){
                    html = '';
                    waybillCodeImages = JSON.parse(waybillCodeImages);
                    for(i = 0; i < waybillCodeImages.length; i++){
                        image = imagePath + waybillCodeImages[i];
                        html += '<li><a href="' + image + '" target="_blank"><img src="' + image + '"></a></li>';
                    }
                    $('#ulProduct2Complaint').html(html);
                }
                var complaintStatusId = data.ComplaintStatusId;
                if(complaintStatusId == 5){
                    $('#complaintCurentMsg').hide();
                    $('#btnUpdateComplaint').hide();
                }
                else if(userRoleId == 1 && (complaintStatusId == 1 || complaintStatusId == 4 || complaintStatusId == 6 || complaintStatusId == 8)) $('#labelComplaintCurentMsg').text('Chăm sóc trả lời: ');
                else if(userRoleId == 5 && (complaintStatusId == 1 || complaintStatusId == 2 || complaintStatusId == 4 || complaintStatusId == 6)) $('#labelComplaintCurentMsg').text('Đặt hàng trả lời: ');
                else if(userRoleId == 3){
                    $('#labelComplaintCurentMsg').text('Quản lý: ');
                    $('#btnUpdateComplaint').text('Cập nhật');
                    $('#complaintCurentMsg .complaintAgree').css('display', 'inline');
                }
                var complaintOwnerId = data.ComplaintOwnerId;
                $('select#complaintOwnerId').val(complaintOwnerId);
                if(userRoleId == 1){
                    if(complaintOwnerId == 1){
                        $('input#complaintStatusId').val('3');
                        $('input#complaintMsgTypeId').val('3');
                    }
                    else if(complaintOwnerId == 2){
                        $('input#complaintStatusId').val('2');
                        $('input#complaintMsgTypeId').val('1');
                    }
                }
                else if(userRoleId == 5){
                    if(complaintOwnerId == 1){
                        $('input#complaintStatusId').val('8');
                        $('input#complaintMsgTypeId').val('2');
                    }
                    else if(complaintOwnerId == 2){
                        $('input#complaintStatusId').val('3');
                        $('input#complaintMsgTypeId').val('4');
                    }
                }
                else if(userRoleId == 3){
                    $('input#complaintMsgTypeId').val('7');
                    if(complaintOwnerId == 1) $('input#complaintStatusId').val('8');
                    else if(complaintOwnerId == 2) $('input#complaintStatusId').val('2');
                    else if(complaintOwnerId == 3) $('input#complaintStatusId').val('3');
                }
                var complaintSolutions = data.ComplaintSolutions;
                if(complaintSolutions != ''){
                    complaintSolutions = JSON.parse(complaintSolutions);
                    for(i = 0; i < complaintSolutions.length; i++){
                        $('input.cbComplaintSolution[value="' + complaintSolutions[i] + '"]').iCheck('check');
                        if(complaintSolutions[i] == 1){
                            $('input#exchangeCostTQ').val(data.ExchangeCostTQ);
                            $('#complaintSolution1').show();
                        }
                        else if(complaintSolutions[i] == 4){
                            $('input#reduceCostTQ').val(data.ReduceCostTQ);
                            $('#complaintSolution4').show();
                        }
                    }
                }
                var listComplaintMsgs = json.data.ComplaintMsgs;
                if(listComplaintMsgs.length > 0){
                    html = '';
                    var divClass = '', labelUser = '';
                    for(i = 0; i < listComplaintMsgs.length; i++){
                        divClass = (i % 2 == 0) ? 'div-left' : 'div-right';
                        if(listComplaintMsgs[i].ComplaintMsgTypeId == 1 || listComplaintMsgs[i].ComplaintMsgTypeId == 3 || listComplaintMsgs[i].ComplaintMsgTypeId == 5) labelUser = 'Chăm sóc trả lời';
                        else if(listComplaintMsgs[i].ComplaintMsgTypeId == 2 || listComplaintMsgs[i].ComplaintMsgTypeId == 4 || listComplaintMsgs[i].ComplaintMsgTypeId == 6) labelUser = 'Đặt hàng trả lời';
                        else if(listComplaintMsgs[i].ComplaintMsgTypeId == 8) labelUser = 'Khách hàng phản hồi';
                        else labelUser = 'Quản lý duyệt';
                        html += '<div class="form-group ' + divClass + '">';
                        html += '<label>' + labelUser + ': </label>';
                        if(listComplaintMsgs[i].ComplaintMsgTypeId == 7){
                            html += '<div class="complaintAgree">';
                            html += '<label><input type="radio" class="iCheck1" disabled' + ((listComplaintMsgs[i].IsAccept == 1) ? ' checked' : '') + '> Duyệt</label>';
                            html += '<label><input type="radio" class="iCheck1" disabled' + ((listComplaintMsgs[i].IsAccept == 0) ? ' checked' : '') + '> Không Duyệt</label></div>';
                        }
                        else if(listComplaintMsgs[i].ComplaintMsgTypeId == 8){
                            html += '<div class="complaintAgree">';
                            html += '<label><input type="radio" class="iCheck1" disabled' + ((listComplaintMsgs[i].IsAccept == 1) ? ' checked' : '') + '> Đồng ý</label>';
                            html += '<label><input type="radio" class="iCheck1" disabled' + ((listComplaintMsgs[i].IsAccept == 0) ? ' checked' : '') + '> Không Đồng ý</label></div>';
                        }
                        html += '<span class="padding15">Đã gửi lúc ' + listComplaintMsgs[i].CrDateTime + ' - ' + listComplaintMsgs[i].FullName + '</span>';
                        html += '<textarea class="form-control" rows="2" disabled>' + listComplaintMsgs[i].Message + '</textarea></div>';
                    }
                    $('#listComplaintMsgs').html(html);
                    $('input.iCheck1').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%'
                    });
                }
                formatCostPart('cost2');
                if(isOpenModal) $('#modalUpdateComplaint').modal('show');
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function deleteProduct(orderStatusId, shipTQType){
    $('.tbodyProduct').on('click', 'i.link_delete_product', function () {
        var tr = $(this).parent().parent();
        var productId = parseInt(tr.attr('data-product-id'));
        if (productId > 0 && orderStatusId < 4) {
            if (confirm('Bạn đã chắc chắn muốn xóa sản phẩm này?')) {
                $.ajax({
                    type: "POST",
                    url: $('input#deleteProductUrl').val(),
                    data: {
                        ProductId: productId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) {
                            var dataProduct = tr.attr('data-product');
                            var shopCode = tr.attr('data-shop');
                            var tbodyProductId = '#tbodyProduct_' + shopCode;
                            var htmlTrSum = $(tbodyProductId).find('.tr_product_sun').html();
                            $(tbodyProductId).find('.tr_product_sun').remove();
                            var rowspan = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').length - 1;
                            var trFirst = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').first();
                            if (trFirst.is(tr)) {
                                var productName = tr.find('.tdProductName .linkproductName').text();
                                var shipTQ = $(tbodyProductId).find('.shipTQ').val();
                                tr.remove();
                                var trFirst1 = $(tbodyProductId + ' tr[data-product="' + dataProduct + '"]').first();
                                trFirst1.find('td:nth-child(2)').before('<td class="tdProductName" rowspan="' + rowspan + '"><a target="_blank" href="' + productName + '" class="linkproductName">' + productName + '</a><textarea rows="3" class="form-control productName"></textarea></td>');
                            }
                            else {
                                tr.remove();
                                trFirst.find('.tdProductName').attr('rowspan', rowspan);
                            }
                            var heightInput = trFirst.find('.tdProductName').height();
                            heightInput = heightInput/(rowspan+1)*rowspan;
                            trFirst.find('.productName').css('height', heightInput);

                            rowspan = $(tbodyProductId + ' tr').length;
                            trFirst = $(tbodyProductId + ' tr').first();
                            if(trFirst.find('.tdShipTQ').length == 0){
                                if(shipTQType == 0) trFirst.find('td:nth-child(8)').before('<td class="tdShipTQ" rowspan="' + rowspan + '"><input type="text" disabled value="' + shipTQ + '" class="form-control cost1 shipTQ"></td>');
                                else if(shipTQType == 1 || shipTQType == 2 || shipTQType == 3) trFirst.find('td:nth-child(8)').before('<td class="tdShipTQ" rowspan="' + rowspan + '"><input type="text" value="' + shipTQ + '" class="form-control cost1 shipTQ"></td>');
                                else trFirst.find('td:nth-child(8)').before('<td class="tdShipTQ" rowspan="' + rowspan + '"><input type="text" disabled value="' + shipTQ + '" class="form-control cost1 shipTQ"></td>');
                            }
                            else trFirst.find('.tdShipTQ').attr('rowspan', rowspan);
                            heightInput = rowspan * 105 + rowspan;
                            trFirst.find('.shipTQ').css('height', heightInput);

                            setTotalPrice(replaceCost($('input#exchangeRate').val()), getServiceFees(), shipTQType);
                            $(tbodyProductId).append('<tr class="tr_product_sun">' + htmlTrSum + '</tr>');
                            setProductSum($(tbodyProductId));
                        }
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
    });
}

function productAction(orderStatusId, shipTQType){
    //out off stock
    $('.tbodyProduct').on('click', 'i.product_action_delete', function () {
        var tr = $(this).parent().parent();
        var productId = parseInt(tr.attr('data-product-id'));
        if (productId > 0) {
            if (confirm('Bạn đã chắc chắn sản phẩm này hết hàng?')) {
                var color = tr.find('input.productColor').val().trim();
                var size = tr.find('input.productSize').val().trim();
                var quantity = replaceCost(tr.find('input.soluong').val().trim());
                var costOld = replaceCost1(tr.find('input.gia').val().trim());
                var costNew = costOld;
                var shipTQ = 0;
                var shopCode = tr.attr('data-shop');
                var tbodyProductId = '#tbodyProduct_' + shopCode;
                var data_product = tr.attr('data-product');
                var trFirst = $(tbodyProductId + ' tr[data-product="' + data_product + '"]').first();
                var productLink = trFirst.find('.tdProductName .linkproductName').text();
                if ($(tbodyProductId + ' tr[data-status="2"]').length == 1) shipTQ = replaceCost1($(tbodyProductId).find('.shipTQ').val().trim());
                changeProductStatus(productId, 1, 'Sản phẩm hết hàng', productLink, color, size, quantity, quantity, costOld, costNew, shipTQ, shopCode, parseInt($('#servicePercent').val()), function () {
                    tr.attr('data-status', '1');
                    for(var i = 1; i < 9; i++) tr.removeClass('tr_product_status_' + i);
                    tr.addClass('tr_product_status_1');
                    if(tr.find('.tdQuantity').length > 0) {
                        var html = '<span class="label1">SL mới</span><span class="label2">SL cũ</span>';
                        html += '<input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" disabled data-field="Số lượng sản phẩm" value="0">';
                        html += '<input type="text" class="form-control cost soluong1" disabled data-field="Số lượng sản phẩm" value="' + quantity + '">';
                        tr.find('.tdQuantity').addClass('tdQuantity2').removeClass('tdQuantity').html(html);
                    }
                    else tr.find('input.soluong').val('0');
                    tr.find('.thanhtien').val('0');
                    tr.find('td').last().html('');
                    if(shipTQ > 0){
                        var rowspan = $('#tbodyProduct_' + shopCode + ' .tr_product').length;
                        var height = rowspan * 105 / 2;
                        html = '<span class="label1">Giá mới</span><span class="label2" style="top: ' + height + 'px">Giá cũ</span>';
                        html += '<input type="text" value="0" class="form-control cost1 shipTQ" disabled style="height: ' + height + 'px;">';
                        html += '<input type="text" value="' + formatDecimal(shipTQ.toString()) + '" class="form-control cost1 shipTQ1" disabled style="height: ' + height + 'px;">';
                        tr.find('.tdShipTQ2').removeClass('tdShipTQ').removeClass('tdShipTQ2').addClass('tdShipTQ3').html(html);
                        $(tbodyProductId + ' .td_sum_ship').text('0');
                    }
                    setTotalPrice(replaceCost($('input#exchangeRate').val()), getServiceFees(), shipTQType);
                    setProductSum($(tbodyProductId));
                });
            }
        }
    });
    //product action==========================================
    $('.tbodyProduct').on('click', 'i.product_action', function () {
        if(orderStatusId == 5 || orderStatusId == 6 || orderStatusId == 10) {
            $('input#productId').val('0');
            $('input#productRow').val('0');
            $('input#productShop').val('');
            var statusId = parseInt($(this).attr('data-status'));
            var tr = $(this).parent().parent();
            var shopCode = tr.attr('data-shop');
            var data_product = tr.attr('data-product');
            var tbodyProductId = '#tbodyProduct_' + shopCode;
            var trFirst = $(tbodyProductId + ' tr[data-product="' + data_product + '"]').first();
            var productId = parseInt(tr.attr('data-product-id'));
            if (productId > 0) {
                $('input#productId').val(productId);
                $('input#productRow').val(tr.find('img.productImage').attr('data-id'));
                $('input#productShop').val(shopCode);
                $('td#productShopDelete').text(shopCode);
                $('td#productColorDelete').text(tr.find('input.productColor').val());
                $('td#productSizeDelete').text(tr.find('input.productSize').val());
                var quantity = tr.find('input.soluong').val();
                $('input#productQuantityDelete').val(quantity);
                $('input#newProductQuantity').val(quantity);
                $('input#productCostDelete, input#newProductCost').val(tr.find('input.gia').val());
                var productName = trFirst.find('.tdProductName .linkproductName').text();
                var shipTQ = $(tbodyProductId).find('.shipTQ').val();
                $('a#productLinkDelete').text(productName);
                $('a#productLinkDelete').attr('href', productName);
                $('td#productShipTQDelete').text(shipTQ);
                $('input#differenceCostTQ, input#differenceCostVN').val('0');
                $('input#imageDelete').val(tr.find('img.productImage').attr('src'));
                $('input#commentDelete').val(tr.find('span.comment').attr('data-title'));
                //$('input#feedbackDelete').val(tr.find('textarea.feedback').val().trim());
                $('input#productStatusId').val(statusId);
                var removeReason = 'Dạ anh/ chị ơi, đơn hàng ' + $('#tdOrderCode').text() + ' có sản phẩm shop báo ' + (statusId == 3 ? 'tăng' : 'giảm') + ' giá, quý khách xin vui lòng xác nhận.';
                $('#removeReason').val(removeReason);
                if (statusId == 3) {
                    $('#modalDeleteProduct h4').text('Sản phẩm tăng giá');
                    $('input#newProductCost').prop('disabled', false);
                }
                else if (statusId == 4) {
                    $('#modalDeleteProduct h4').text('Sản phẩm giảm giá');
                    $('input#newProductCost').prop('disabled', false);
                }
                $('#modalDeleteProduct').modal('show');
            }
        }
        else showNotification('Thay đổi trực tiếp trên bảng', 0);
    });

    //keyup price change======================================
    $('#modalDeleteProduct').on('keyup', '.cost3', function(){
        var quantityOld = replaceCost($('input#productQuantityDelete').val());
        var quantityNew = replaceCost($('input#newProductQuantity').val());
        var costOld = replaceCost1($('input#productCostDelete').val());
        var costNew = replaceCost1($('input#newProductCost').val());
        var differenceCostTQ = quantityNew * costNew - quantityOld * costOld;
        if(differenceCostTQ.toString().indexOf('.') > 0) differenceCostTQ = differenceCostTQ.toFixed(2);
        $('input#differenceCostTQ').val(differenceCostTQ);
        var differenceCostVN = Math.ceil(differenceCostTQ * replaceCost($('input#exchangeRate').val()));
        differenceCostVN += Math.ceil(differenceCostVN * parseInt($('#servicePercent').val()) / 100);
        $('input#differenceCostVN').val(differenceCostVN);
        formatCostPart('cost3');
    });

    //update product===========================================
    $('#btnChangeProduct').click(function(){
        var productId = parseInt($('input#productId').val());
        var productRow = parseInt($('input#productRow').val());
        var removeReason = $('#removeReason').val().trim();
        if(productId > 0 && productRow > 0 && removeReason != ''){
            $('#btnChangeProduct').prop('disabled', true);
            var costOld = replaceCost1($('input#productCostDelete').val());
            var costNew = replaceCost1($('input#newProductCost').val());
            var quantityOld = replaceCost($('input#productQuantityDelete').val());
            var quantityNew = replaceCost($('input#newProductQuantity').val());
            var statusId = parseInt($('input#productStatusId').val());
            if(statusId == 3){
                if(quantityOld * costOld >= quantityNew * costNew){
                    showNotification('Giá mới phải lớn hơn giá cũ', 0);
                    return false;
                }
            }
            else if(statusId == 4){
                if(quantityNew * costNew >= quantityOld * costOld){
                    showNotification('Giá mới phải nhỏ hơn giá cũ', 0);
                    return  false;
                }
            }
            var productLink = $('a#productLinkDelete').text();
            var color = $('td#productColorDelete').text();
            var size = $('td#productSizeDelete').text();
            var shopCode = $('input#productShop').val();
            var shipTQ = 0;
            var tbodyProductId = '#tbodyProduct_' + shopCode;
            if($(tbodyProductId + ' tr[data-status="2"]').length == 1) shipTQ = replaceCost1($('td#productShipTQDelete').text());
            changeProductStatus(productId, statusId, removeReason, productLink, color, size, quantityOld, quantityNew, costOld, costNew, shipTQ, shopCode, parseInt($('#servicePercent').val()), function(){
                $('#modalDeleteProduct').modal('hide');
                var tr = $(tbodyProductId + ' tr[data-product-id="' + productId + '"]');
                tr.attr('data-status', statusId);
                for(var i = 1; i < 9; i++) tr.removeClass('tr_product_status_' + i);
                tr.addClass('tr_product_status_' + statusId);
                //tr.find('textarea.feedback').val('Dạ anh/ chị ơi, sản phẩm này shop báo ' + ((statusId == 3) ? 'tăng' : 'giảm') + ' giá, anh/ chị có đặt không ạ?');
                tr.find('.tdAttr input, .soluong').prop('disabled', true);
                var html = '<span class="label1">Giá cần KH duyệt</span><span class="label2">Giá cũ</span>';
                html += '<input type="text" class="form-control cost4" disabled value="' + costOld + '">';
                html += '<input type="text" class="form-control cost1 gia cost4 hmdrequired hmdrequiredCost1" disabled data-field="Giá sản phẩm" value="' + costNew + '">';
                tr.find('.tdCost').addClass('tdCost2').removeClass('tdCost').html(html);
                html = '<span class="label1">SL cần KH duyệt</span><span class="label2">SL cũ</span>';
                html += '<input type="text" class="form-control cost soluong1" disabled data-field="Số lượng sản phẩm" value="' + quantityNew + '">';
                html += '<input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" disabled data-field="Số lượng sản phẩm" value="' + quantityOld + '">';
                tr.find('.tdQuantity').addClass('tdQuantity2').removeClass('tdQuantity').html(html);
                tr.find('td').last().html('');
                formatCostPart('cost4');
            });
        }
        else showNotification('Lí do không được bỏ trống', 0);
    });
}

function changeProductStatus(productId, statusId, removeReason, productLink, color, size, quantity, quantityNew, costOld, costNew, shipTQ, shopCode, servicePercent, fnSuccess){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusProductUrl').val(),
        data: {
            UserId: $('input#userLoginId').val(),
            RoleId: $('input#roleLoginId').val(),
            CareStaffId: $('#careStaffId').val(),
            OrderUserId: $('#orderUserId').val(),
            CustomerId: $('input#customerId').val(),
            OrderId: $('input#orderId').val(),
            OrderCode: $('#tdOrderCode').text(),
            ProductId: productId,
            ProductStatusId: statusId,
            RemoveReason: removeReason,
            RemoveConfirm: '',
            ProductLink: productLink,
            Color: color,
            Size: size,
            Quantity: quantity,
            QuantityNew: quantityNew,
            Cost: costOld,
            CostNew: costNew,
            ShipTQ: shipTQ,
            ShopCode: shopCode,
            ExchangeRate: replaceCost($('input#exchangeRate').val()),
            ServicePercent: servicePercent
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(statusId == 3 || statusId == 4) $('#btnChangeProduct').prop('disabled', false);
            if (json.code == 1) fnSuccess();
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            if(statusId == 3 || statusId == 4) $('#btnChangeProduct').prop('disabled', false);
        }
    });
}

function shopAction(orderStatusId, shipTQType){
    $('.tbodyProduct').on('click', 'i.ship_action', function () {
        if(orderStatusId == 5 || orderStatusId == 6 || orderStatusId == 10) {
            $('input#shipTQShopCode').val('');
            var tr = $(this).parent().parent();
            var shopCode = tr.attr('data-shop');
            var tbodyProductId = '#tbodyProduct_' + shopCode;
            var productCount = parseInt($(tbodyProductId + ' .td_sum_quantity').text());
            if (productCount > 0) {
                var shipTQ = replaceCost1($(tbodyProductId).find('.shipTQ').val());
                var statusId = parseInt($(this).attr('data-status'));
                if (statusId == 4 && shipTQ == 0) showNotification('Shop này không giảm được', 0);
                else {
                    $('input#shopStatusId').val(statusId);
                    if (statusId == 3) $('#modalChangeShipTQ h4').text('Tăng phí Ship TQ');
                    else $('#modalChangeShipTQ h4').text('Giảm phí Ship TQ');
                    var shipTQReason = 'Dạ anh/ chị ơi, đơn hàng ' + $('#tdOrderCode').text() + ' có shop ' +  shopCode + ' báo ' + (statusId == 3 ? 'tăng' : 'giảm') + ' phí ship, quý khách xin vui lòng xác nhận.';
                    $('#shipTQReason').val(shipTQReason);
                    $('input#shipTQShopCode').val(shopCode);
                    $('input#shipTQShopName').val($('span#shopName_' + shopCode).text());
                    $('input#shipTQProductCount').val(productCount);
                    $('input#shipTQOld, input#shipTQNew').val(formatDecimal(shipTQ.toString()));
                    $('input#differenceShipTQ').val('0');
                    $('input#differenceShipVN').val('0');
                    $('#modalChangeShipTQ').modal('show');
                }
            }
            else showNotification('Shop này đã hết hàng', 0);
        }
        else showNotification('Thay đổi trực tiếp trên bảng', 0);
    });
    $('#modalChangeShipTQ').on('keyup', 'input#shipTQNew', function(){
        $(this).val(formatDecimal($(this).val()));
        var shipTQOld = replaceCost1($('input#shipTQOld').val());
        var shipTQNew = replaceCost1($(this).val());
        var differenceShipTQ = shipTQNew - shipTQOld;
        $('input#differenceShipTQ').val(formatDecimal(differenceShipTQ.toString()));
        var differenceShipVN = Math.ceil(differenceShipTQ * replaceCost($('input#exchangeRate').val()));
        differenceShipVN += Math.ceil(differenceShipVN * parseInt($('#servicePercent').val()) / 100);
        $('input#differenceShipVN').val(formatDecimal(differenceShipVN.toString()));
    });
    $('#btnChangeShipTQ').click(function(){
        var shopCode = $('input#shipTQShopCode').val();
        var shipTQReason = $('#shipTQReason').val().trim();
        if(shopCode != '' && shipTQReason != ''){
            var shipTQOld = replaceCost1($('input#shipTQOld').val());
            var shipTQNew = replaceCost1($('input#shipTQNew').val());
            var statusId = parseInt($('input#shopStatusId').val());
            if(statusId == 3){
                if(shipTQOld >= shipTQNew){
                    showNotification('Giá mới phải lớn hơn giá cũ', 0);
                    return  false;
                }
            }
            else{
                if(shipTQNew >= shipTQOld){
                    showNotification('Giá mới phải nhỏ hơn giá cũ', 0);
                    return  false;
                }
            }
            $('#btnChangeShipTQ').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#changeStatusShopUrl').val(),
                data: {
                    UserId: $('input#userLoginId').val(),
                    RoleId: $('input#roleLoginId').val(),
                    CareStaffId: $('#careStaffId').val(),
                    OrderUserId: $('#orderUserId').val(),
                    CustomerId: $('input#customerId').val(),
                    OrderId: $('input#orderId').val(),
                    OrderCode: $('#tdOrderCode').text(),
                    ShopCode: shopCode,
                    ShopStatusId: statusId,
                    ShipTQ: shipTQOld,
                    ShipTQNew: shipTQNew,
                    PaidTQ: replaceCost1($('input#differenceShipTQ').val()),
                    ExchangeRate: replaceCost($('input#exchangeRate').val()),
                    PaidVN: replaceCost1($('input#differenceShipVN').val()),
                    ShipTQReason: shipTQReason,
                    ShipTQConfirm: ''
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('#modalChangeShipTQ').modal('hide');
                        var tr = $('#tbodyProduct_' + shopCode + ' .tr_product').first();
                        var rowspan = $('#tbodyProduct_' + shopCode + ' .tr_product').length;
                        var height = rowspan * 105 / 2;
                        var html = '<span class="label1">Giá cần duyệt</span><span class="label2" style="top: ' + height + 'px">Giá cũ</span>';
                        html += '<input type="text" value="' + formatDecimal(shipTQNew.toString()) + '" class="form-control cost1 shipTQ1" disabled style="height: ' + height + 'px;">';
                        html += '<input type="text" value="' + formatDecimal(shipTQOld.toString()) + '" class="form-control cost1 shipTQ" disabled style="height: ' + height + 'px;">';
                        tr.find('.tdShipTQ2').removeClass('tdShipTQ').removeClass('tdShipTQ2').addClass('tdShipTQ3').html(html);
                    }
                    $('#btnChangeShipTQ').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('#btnChangeShipTQ').prop('disabled', false);
                }
            });
        }
        else showNotification('Lí do không được bỏ trống', 0);
    });
}

function orderRevenue(){
    $('select#transactionRangeId option[value="1"], select#transactionRangeId option[value="2"]').hide();
    $('.aCurrency').click(function(){
        $('span#curentCurrency').text($(this).text());
        $('#divCurrency').removeClass('open');
        var id = $(this).attr('data-id');
        $('input#currencyId').val(id);
        if(id == '1'){
            $('input#exchangeRateRevenue').val('1');
            $('input#paidTQRevenue').val('0');
            $('input#paidVNRevenue').val($('input#paidCostRevenue').val());
            $('.divTQ').hide();
        }
        else{
            var paidCost = $('input#paidCostRevenue').val();
            var exchangeRate = $('input#exchangeRate').val();
            $('input#paidTQRevenue').val(paidCost);
            $('input#exchangeRateRevenue').val(exchangeRate);
            $('input#paidVNRevenue').val(Math.ceil(replaceCost1(paidCost) * replaceCost(exchangeRate)));
            formatPaidVNRevenue();
            $('.divTQ').show();
        }
        return false;
    });
    $('#modalRevenue').on('keyup', 'input#paidCostRevenue', function(){
        formatCostPart('cost5');
        if($('input#currencyId').val() == '1'){
            $('input#paidTQRevenue').val('0');
            $('input#paidVNRevenue').val($(this).val());
        }
        else{
            $('input#paidTQRevenue').val($(this).val());
            $('input#paidVNRevenue').val(Math.ceil(replaceCost1($(this).val()) * replaceCost($('input#exchangeRateRevenue').val())));
        }
        formatPaidVNRevenue();
    });
    $('#modalRevenue').on('keyup', 'input#exchangeRateRevenue', function(){
        $('input#paidVNRevenue').val(Math.ceil(replaceCost1($('input#paidCostRevenue').val()) * replaceCost($(this).val())));
        formatPaidVNRevenue();
    });
    $('#btnRevenue').click(function(){
        $('#modalRevenue').modal('show');
    });
    $('#btnAddRevenue').click(function(){
        var paidCostRevenue = replaceCost1($('input#paidCostRevenue').val());
        if(paidCostRevenue > 0){
            $('#btnAddRevenue').prop('disabled', true);
            var statusId = 1;
            if($('input#roleLoginId').val() == '3') statusId = 2;
            var transactionRangeId = parseInt($('select#transactionRangeId').val());
            var transactionTypeId = 3;
            if(transactionRangeId == 3) transactionTypeId = 2;
            $.ajax({
                type: "POST",
                url: $('input#updateTransactionUrl').val(),
                data: {
                    CustomerId: $('input#customerId').val(),
                    OrderId: $('input#orderId').val(),
                    ComplaintId: 0,
                    StatusId: statusId,
                    TransactionTypeId: transactionTypeId,
                    TransactionRangeId: transactionRangeId,
                    WarehouseId: $('select#warehouseId1').val(),
                    MoneySourceId: $('select#moneySourceId').val(),
                    Comment: $('#commentRevenue').val().trim(),
                    Confirm: '',
                    PaidTQ: replaceCost1($('input#paidTQRevenue').val()),
                    ExchangeRate: replaceCost($('input#exchangeRateRevenue').val()),
                    PaidVN: replaceCost($('input#paidVNRevenue').val()),
                    TransactionId: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('input#paidCostRevenue').val('0');
                        $('input#paidVNRevenue').val('0');
                        $('input#paidTQRevenue').val('0');
                        $('#commentRevenue').val('');
                        $('#modalRevenue').modal('hide');
                    }
                    $('#btnAddRevenue').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('#btnAddRevenue').prop('disabled', false);
                }
            });
        }
        else{
            showNotification('Số tiền phải lơn hơn 0', 0);
            $('input#paidCostRevenue').focus();
        }
    });
}

function formatPaidVNRevenue(){
    $('input#paidVNRevenue').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: ','
    });
}

function changeShopCategory(orderStatusId){
    if(orderStatusId == 4 || orderStatusId == 5 || orderStatusId == 6 || orderStatusId == 9 || orderStatusId == 10){
        $('.timeline-header select.select2').change(function(){
            if($(this).val() != '0') {
                $.ajax({
                    type: "POST",
                    url: $('input#changeCategoryShopUrl').val(),
                    data: {
                        OrderId: $('input#orderId').val(),
                        ShopCode: $(this).attr('name').replace('CategoryId_', ''),
                        CategoryId: $(this).val()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        });
    }
}