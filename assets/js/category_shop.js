$(document).ready(function(){
    $("#tbodyCategory").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#categoryId').val(id);
        $('select#displayOrder').val($('select#displayOrder_' + id).val());
        $('input#categoryName').val($('td#categoryName_' + id).text());
        scrollTo('select#displayOrder');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#categoryForm').trigger("reset");
        return false;
    });
    $("#tbodyCategory").on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteCategoryUrl').val(),
                data: {
                    CategoryId: id,
                    IsCheck: 0
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#category_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_update').click(function(){
        if (validate()) {
            var form = $('#categoryForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(true, '');
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});
function changeDisplayOrder(select, categoryId){
    $.ajax({
        type: "POST",
        url: $('input#changeDisplayOrderUrl').val(),
        data: {
            CategoryId: categoryId,
            CategoryTypeId: $('input#categoryTypeId').val(),
            ParentCategoryId: $('input#parentCategoryId').val(),
            DisplayOrder: select.value
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1) redirect(true, '');
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}