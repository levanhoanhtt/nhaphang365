$(document).ready(function(){
    $('select#commentStarId option').each(function(){
        if($(this).val() != '0') $(this).text($(this).val() + ' - ' + $(this).text());
    });
    if($('select#crUserId').val() != '0') $('input#fullName').val('').prop('disabled', true);
    $('select#crUserId').change(function(){
        if($(this).val() == '0') $('input#fullName').prop('disabled', false);
        else $('input#fullName').val('').prop('disabled', true);
    });
    $(document).on('submit','#commentForm',function (){
        if(validate()) {
            var form = $('#commentForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1 && $('input#commentId').val() == '0') redirect(false, $('input#commentEditUrl').val() + '/' + json.data);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});