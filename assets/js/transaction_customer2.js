$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('select#customerId').change(function(){
        if($(this).val() != '0') window.location.href = $('input#transactionCustomerUrl').val() + '/' + $(this).val();
    });
    $('#btnOrder').click(function(){
        $('#paggingOrder .box-tools').html('');
        $('#paggingOrder').hide();
        getListOrder(1, true);
    });
    $('#tbodyTransaction').on('keyup', 'input.balance', function(){
        $(this).val(formatDecimal($(this).val()));
    });
    $('#tbodyTransaction').on('click', '.link_delete', function(){
        if (confirm('Bạn có thực sự muốn xóa ?')){
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    TransactionId: id,
                    StatusId: 0,
                    OrderId: $('input#orderId_' + id).val(),
                    CustomerId: $('input#customerId_' + id).val(),
                    PaidVN: $('input#paidVN_' + id).val(),
                    Balance: replaceCost1($('input#balance_' + id).val()),
                    TransactionTypeId: $('input#transactionTypeId_' + id).val(),
                    TransactionTypeName: $('td#transactionTypeName_' + id).text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) $('tr#transaction_' + id).remove();
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('#tbodyTransaction').on('click', '.link_update', function(){
        var id = $(this).attr('data-id');
        var paidDateTime = $('input#paidDateTime_' + id).val().trim();
        if(paidDateTime != ''){
            var paidDateTimes = paidDateTime.split(' ');
            if(paidDateTimes.length == 2){
                var paidDates = paidDateTimes[0].split('/');
                if(paidDates.length == 3){
                    if(paidDates[0] > 0 && paidDates[0] < 32 && paidDates[1] > 0 && paidDates[1] < 13 && paidDates[2] > 2016){
                        var paidDateTimeStr = paidDates[2] + '-' + paidDates[1] + '-' + paidDates[0];
                        paidDates = paidDateTimes[1].split(':');
                        if(paidDates[0] > 0 && paidDates[0] < 24 && paidDates[1] > 0 && paidDates[1] < 60){
                            paidDateTimeStr += ' ' + paidDates[0] + ':' + paidDates[1];
                            $.ajax({
                                type: "POST",
                                url: $('input#updateBalanceUrl').val(),
                                data: {
                                    TransactionId: id,
                                    PaidDateTime: paidDateTimeStr,
                                    Balance: replaceCost1($('input#balance_' + id).val().trim()),
                                    TransactionTypeName: $('td#transactionTypeName_' + id).text()
                                },
                                success: function (response) {
                                    var json = $.parseJSON(response);
                                    showNotification(json.message, json.code);
                                    if (json.code == 1) redirect(true, '');
                                },
                                error: function (response) {
                                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                                }
                            });
                        }
                        else showNotificationPaidDateTime(id);
                    }
                    else showNotificationPaidDateTime(id);
                }
                else showNotificationPaidDateTime(id);
            }
            else showNotificationPaidDateTime(id);
        }
        else showNotificationPaidDateTime(id);
        return false;
    });
});

function paggingOrder(pageId){
    getListOrder(pageId, false);
}

function getListOrder(pageId, isOpenModal){
    $.ajax({
        type: "POST",
        url: $('input#getListOrderUrl').val(),
        data: {
            CustomerId: $('input#customerId').val(),
            PageId: pageId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1) {
                var data = json.data;
                if(data.PaggingHtml != ''){
                    $('#paggingOrder .box-tools').html(data.PaggingHtml);
                    $('#paggingOrder').show();
                }
                var html = '';
                data = data.ListOrders;
                for (var i = 0; i < data.length; i++){
                    html += '<tr><td>' + (i + 1) + '</td>';
                    html += '<td>' + data[i].OrderLinkHtml + '</td>';
                    html += '<td>' + data[i].CrDateTime + '</td>';
                    html += '<td>' + data[i].OrderStatusHtml + '</td>';
                    html += '<td>' + data[i].TotalCost + '</td></tr>';
                }
                $('#tbodyOrder').html(html);
                if(isOpenModal) $('#modalOrder').modal('show');
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}

function showNotificationPaidDateTime(id){
    showNotification('Thời gian không đúng định dạng', 0);
    $('input#paidDateTime_' + id).focus();
}