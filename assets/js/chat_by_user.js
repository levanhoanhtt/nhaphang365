$(document).ready(function() {
    var userLoginId = parseInt($('input#userLoginId').val());
    if(userLoginId > 0) {
        var noticeAudioUrl = 'assets/vendor/dist/chat';
        //thong bao notice
        if ($('#ulChatHeader').length > 0) getListChatHeader(userLoginId);
        //chat nodejs
        var socket = io($('input#chatServerUrl').val());
        //form chat cho CSKH
        if ($('div#chatToCustomerPage').length > 0) {
            //lay danh sach tin nhan cua KH
            getListCustomerChat(userLoginId, 1, socket);
            $('#seachCustomerChatForm').submit(function (e) {
                getListCustomerChat(userLoginId, 2, socket);
                e.preventDefault();
            });
            $('#seachCustomerChatForm2').submit(function (e) {
                getListCustomerChat(userLoginId, 3, socket);
                e.preventDefault();
            });
            //customer on/off
            socket.on('user state', function (data) {
                if (data.roleId == 4) {
                    if (data.state == 1) $('.box-comments .box-comment[data-id="' + data.userId + '"] span.offline').removeClass('offline').addClass('online');
                    else $('.box-comments .box-comment[data-id="' + data.userId + '"] span.online').removeClass('online').addClass('offline');
                }
            });
            //chon KH de chat
            $('div.box-comments').on('click', 'div.box-comment', function () {
                var customerId = $(this).attr('data-id');
                if ($('input#customerId').val() != customerId) {
                    $('div.box-comment').removeClass('active');
                    $('div.box-comment[data-id="' + customerId + '"]').addClass('active');
                    $('input#customerId').val(customerId);
                    var customerName = $(this).attr('data-name');
                    $('input#customerName').val(customerName);
                    if ($(this).attr('data-phone') != '') customerName += ' - ' + $(this).attr('data-phone');
                    if ($(this).attr('data-email') != '') customerName += ' - ' + $(this).attr('data-email');
                    $('h3.box-title').text(customerName);
                    $('input#customerAvatar').val($(this).find('img.img-circle').attr('src').replace($('input#userImagePath').val(), ''));
                    $('input#startChatPagging').val(0);
                    $('input#totalChatMsg').val(0);
                    getListChat(userLoginId);
                }
            });
            //On/ off chat
            $('input#cbOnChat').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
            $('input#cbOnChat').on('ifToggled', function (e) {
                if (e.currentTarget.checked) $('#lbOnChat').css('font-weight', 'bold').text('Online');
                else $('#lbOnChat').css('font-weight', 'normal').text('Offline');
                socket.emit('user state', {
                    userId: userLoginId,
                    roleId: parseInt($('input#roleLoginId').val()),
                    state: (e.currentTarget.checked) ? 1 : 0
                });
            });
            //check staff on/off when reload
            socket.emit('user online', {
                roleId: 1
            });
            socket.on('staff online', function (staffOnlines) {
                if (staffOnlines.indexOf(userLoginId) >= 0) $('input#cbOnChat').iCheck('check');
                else $('input#cbOnChat').iCheck('uncheck');
            });
        }
        //chat popup
        else if ($('.maximizechat').length > 0) {
            //lay danh sach KH
            getListCustomer(userLoginId, socket);
            //customer on/off
            socket.on('user state', function (data) {
                if (data.roleId == 4) {
                    if (data.state == 1) $('ul#chat-staff-care-list li a[data-id="' + data.userId + '"] span.offline').removeClass('offline').addClass('online');
                    else $('ul#chat-staff-care-list li a[data-id="' + data.userId + '"] span.online').removeClass('online').addClass('offline');
                }
            });
            //chon KH de chat
            $('ul#chat-staff-care-list').on('click', 'a', function () {
                var customerId = $(this).attr('data-id');
                if ($('input#customerId').val() != customerId) {
                    var customerName = $(this).attr('data-name');
                    $('input#customerId').val(customerId);
                    $('input#customerName').val(customerName);
                    $('input#customerAvatar').val($(this).attr('data-avatar'));
                    $('.direct-chat h3.box-title').html(customerName);
                    $('p#customerPhone').text($(this).attr('data-phone'));
                    $('p#customerEmail').text($(this).attr('data-email'));
                    $('input#startChatPagging').val(0);
                    $('input#totalChatMsg').val(0);
                    getListChat(userLoginId);
                    getCountChatUnRead(userLoginId);
                }
                $('.direct-chat').removeClass('direct-chat-contacts-open');
                return false;
            });
        }

        $('#chatForm').submit(function (e) {
            var customerId = $('input#customerId').val();
            if (customerId != '0') {
                var chatMsg = $('input#chatMsg').val().trim();
                if (chatMsg != '') {
                    $('input#chatMsg').val('');
                    var chatData = {
                        customerId: customerId,
                        customerName: $('input#customerName').val(),
                        customerAvatar: $('input#customerAvatar').val(),
                        staffId: userLoginId,
                        staffName: $('input#fullNameLoginId').val(),
                        staffAvatar: $('input#avatarLoginId').val(),
                        msg: chatMsg,
                        fileUrl: '',
                        isCustomerSend: 0
                    };
                    socket.emit('new message', chatData);
                }
            }
            else showNotification('Vui lòng chọn 1 khách hàng để chat', 0);
            e.preventDefault();
        });
        //send file in chat
        //truoc mat la image
        $('#btnChatSendFile').click(function () {
            var customerId = $('input#customerId').val();
            if (customerId != '0') {
                var finder = new CKFinder();
                finder.resourceType = 'Products';
                finder.selectActionFunction = function (fileUrl) {
                    var chatData = {
                        customerId: customerId,
                        customerName: $('input#customerName').val(),
                        customerAvatar: $('input#customerAvatar').val(),
                        staffId: userLoginId,
                        staffName: $('input#fullNameLoginId').val(),
                        staffAvatar: $('input#avatarLoginId').val(),
                        msg: '',
                        fileUrl: fileUrl,
                        isCustomerSend: 0
                    };
                    socket.emit('new message', chatData);
                };
                finder.popup();
            }
            else showNotification('Vui lòng chọn 1 khách hàng để chat', 0);
        });
        //phan trang chat
        $("#listChat").scroll(function () {
            if ($(this).scrollTop() == 0) {
                var totalChatMsg = parseInt($('input#totalChatMsg').val());
                if (totalChatMsg % 20 == 0) {
                    $('input#startChatPagging').val(20 + parseInt($('input#startChatPagging').val()));
                    getListChat(userLoginId);
                }
            }
        });
        //nhan chat
        socket.on('new message', function (data) {
            var userImagePath = $('input#userImagePath').val();
            if (userLoginId == data.staffId) {
                //hien thi tren page chat
                if ($('div#chatToCustomerPage').length > 0) {
                    if ($('input#customerId').val() == data.customerId) {
                        var imagePath = $('input#imagePath').val();
                        var divClass = '';
                        if (data.isCustomerSend == 1) divClass = ' right unread';
                        var html = '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + ((data.isCustomerSend == 1) ? data.customerName : data.staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data.crDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + ((data.isCustomerSend == 1) ? data.customerAvatar : data.staffAvatar) + '" alt="">';
                        if (data.msg != '') html += '<div class="direct-chat-text">' + data.msg + '</div></div>';
                        else if (data.fileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data.fileUrl + '" target="_blank"><img src="' + imagePath + data.fileUrl + '" alt=""></a></div></div>';
                        $('#listChat').append(html);
                        scrollChat(true);
                        if (data.isCustomerSend == 1) {
                            $.playSound(noticeAudioUrl);
                            prependCustomerChat(data);
                        }
                        else $('#listChatCustomer div.box-comment[data-id="' + data.customerId + '"]').find('span.lastMsgChat').text('You: ' + data.msg);
                    }
                    else {
                        if (data.isCustomerSend == 1) {
                            $.playSound(noticeAudioUrl);
                            if ($('div.box-comment[data-id="' + data.customerId + '"]').length > 0) {
                                prependCustomerChat(data);
                            }
                        }
                    }
                }
                //chat popup
                else if ($('.maximizechat').length > 0) {
                    if ($('input#customerId').val() == data.customerId) {
                        var imagePath = $('input#imagePath').val();
                        var divClass = '';
                        if (data.isCustomerSend == 1) divClass = ' right unread';
                        var html = '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + ((data.isCustomerSend == 1) ? data.customerName : data.staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data.crDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + ((data.isCustomerSend == 1) ? data.customerAvatar : data.staffAvatar) + '" alt="">';
                        if (data.msg != '') html += '<div class="direct-chat-text">' + data.msg + '</div></div>';
                        else if (data.fileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data.fileUrl + '" target="_blank"><img src="' + imagePath + data.fileUrl + '" alt=""></a></div></div>';
                        $('#listChat').append(html);
                        scrollChat(true);
                        if (data.isCustomerSend == 1) {
                            $.playSound(noticeAudioUrl);
                            //cap nhat so tin chua doc
                            var count = parseInt($('span#countChatUnRead').text()) + 1;
                            $('span#countChatUnRead').text(count).attr('data-original-title', count + ' Tin Nhắn chưa đọc');
                        }
                    }
                    else if (data.isCustomerSend == 1) {//hien thi tren header chat
                        $.playSound(noticeAudioUrl);
                        var msg = '';
                        if (data.msg != '') msg = data.msg;
                        else if (data.fileUrl != '') msg = 'Gửi ảnh...';
                        showNotification(data.customerName + ': ' + msg, 1);
                        getListChatHeader(userLoginId);
                    }
                }
                else if (data.isCustomerSend == 1) {//hien thi tren header chat
                    $.playSound(noticeAudioUrl);
                    var msg = '';
                    if (data.msg != '') msg = data.msg;
                    else if (data.fileUrl != '') msg = 'Gửi ảnh...';
                    showNotification(data.customerName + ': ' + msg, 1);
                    getListChatHeader(userLoginId);
                }
            }
        });
        //set da doc
        $('input#chatMsg').focus(function () {
            var customerId = $('input#customerId').val();
            if (customerId != '0') {
                var count = '0';
                if ($('div#chatToCustomerPage').length > 0) count = $('#listChatCustomer div.box-comment[data-id="' + customerId + '"] span.text-muted').attr('data-count');
                else count = $('span#countChatUnRead').text();
                if (count != '0') {
                    $.ajax({
                        type: "POST",
                        url: $('input#updateCountChatUnReadUrl').val(),
                        data: {
                            StaffId: userLoginId,
                            CustomerId: customerId,
                            IsStaff: 1
                        },
                        success: function (response) {
                            if (response == 1) {
                                $('#listChat .direct-chat-msg').removeClass('unread');
                                if ($('div#chatToCustomerPage').length > 0) {
                                    $('#listChatCustomer div.box-comment[data-id="' + customerId + '"] span.text-muted').text('').attr('data-count', '0');
                                    $('div.box-comment[data-id="' + customerId + '"]').removeClass('unread');
                                    getListChatHeader(userLoginId);
                                }
                                else $('span#countChatUnRead').text('0').attr('data-count', '0');
                            }
                        },
                        error: function (response) {
                        }
                    });
                }
            }
        });
    }
});

/*
 customerTypeId = 1: get all
 customerTypeId = 2: get user chat
 customerTypeId = 3: get cusstomer sidebar
*/
function getListCustomerChat(staffId, customerTypeId, socket){
    var eleCustomer1 = $('#listChatCustomer');
    var eleCustomer2 = $('#listChatCustomer2');
    if(customerTypeId != 3) eleCustomer1.html('<img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter">');
    if(customerTypeId != 2) eleCustomer2.html('<img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter">');
    socket.emit('user online', {
        roleId: 4
    });
    var customerName = (customerTypeId != 3) ? $('input#txtCustomerChatName').val().trim() : $('input#txtCustomerChatName2').val().trim();
    $.ajax({
        type: "POST",
        url: $('#seachCustomerChatForm').attr('action'),
        data: {
            StaffId: staffId,
            CustomerName: customerName,
            CustomerTypeId: customerTypeId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1) {
                var html = '';
                var html2 = '';
                var data = json.data;
                var userImagePath = $('input#userImagePath').val();
                for (var i = 0; i < data.length; i++) {
                    if(data[i].StaffId > 0 && data[i].CustomerId > 0) {
                        if(customerTypeId != 3 && data[i].CustomerTypeId == 2) {
                            html += '<div class="box-comment' + ((data[i].CountMsg != '0') ? ' unread' : '') + '" data-id="' + data[i].CustomerId + '" data-name="' + data[i].CustomerName + '" data-email="' + data[i].CustomerEmail + '" data-phone="' + data[i].CustomerPhone + '">';
                            html += '<img class="img-circle img-sm" src="' + userImagePath + data[i].CustomerAvatar + '" alt="' + data[i].CustomerName + '">';
                            html += '<div class="comment-text"><span class="username">' + data[i].CustomerName + '<span class="offline"></span>';
                            html += '<span class="text-muted pull-right" data-count="' + data[i].CountMsg + '">' + ((data[i].CountMsg != '0') ? (data[i].CountMsg + ' Tin nhắn') : '') + '</span>';
                            if (data[i].IsCustomerSend == 1) html += '</span><span class="lastMsgChat">' + data[i].Message + '</span></div></div>';
                            else html += '</span><span class="lastMsgChat">You: ' + data[i].Message + '</span></div></div>';
                        }
                        if(customerTypeId != 2){
                            html2 += '<div class="box-comment" data-id="' + data[i].CustomerId + '" data-name="' + data[i].CustomerName + '" data-email="' + data[i].CustomerEmail + '" data-phone="' + data[i].CustomerPhone + '">';
                            html2 += '<img class="img-circle img-sm" src="' + userImagePath + data[i].CustomerAvatar + '" alt="' + data[i].CustomerName + '">';
                            html2 += '<div class="comment-text"><span class="username">' + data[i].CustomerName + '<span class="offline"></span>';
                            html2 += '<span class="text-muted pull-right" data-count="0" style="display: none;"></span>';
                            html2 += '</span><span class="lastMsgChat" style="display: none;"></span></div></div>';
                        }
                    }
                }
                if(customerTypeId != 3) {
                    eleCustomer1.html(html);
                    eleCustomer1.slimScroll({
                        height: '400px',
                        alwaysVisible: true,
                        wheelStep: 20,
                        touchScrollStep: 500
                    });
                }
                if(customerTypeId != 2){
                    eleCustomer2.html(html2);
                    eleCustomer2.slimScroll({
                        height: '400px',
                        alwaysVisible: true,
                        wheelStep: 20,
                        touchScrollStep: 500
                    });
                }
                socket.on('customer online', function (customerOnlines) {
                    for(var i = 0; i < customerOnlines.length; i++){
                        $('.box-comments .box-comment[data-id="' + customerOnlines[i] + '"] span.offline').removeClass('offline').addClass('online');
                    }
                });
            }
        },
        error: function (response) {}
    });
}

function prependCustomerChat(data){
    var eleBoxComment = $('div.box-comments div.box-comment[data-id="' + data.customerId + '"]');
    var span = eleBoxComment.find('span.text-muted');
    var count = parseInt(span.attr('data-count')) + 1;
    span.text(count + ' Tin nhắn').attr('data-count', count);
    var msg = '';
    if (data.msg != '') msg = data.msg;
    else if (data.fileUrl != '') msg = 'Gửi ảnh...';
    eleBoxComment.find('span.lastMsgChat').text(msg);
    var html = eleBoxComment[0].outerHTML;
    $('#listChatCustomer div.box-comment[data-id="' + data.customerId + '"]').remove();
    $('#listChatCustomer').prepend(html);
    var eleBoxComment2 = $('#listChatCustomer div.box-comment[data-id="' + data.customerId + '"]');
    eleBoxComment2.addClass('unread');
    eleBoxComment2.find('span.text-muted').show();
    eleBoxComment2.find('span.lastMsgChat').show();
}

function getListChat(staffId){
    var customerId = $('input#customerId').val();
    if(staffId > 0 && customerId != '0'){
        var startChatPagging = $('input#startChatPagging').val();
        $.ajax({
            type: "POST",
            url: $('input#getChatUrl').val(),
            data: {
                CustomerId: customerId,
                StaffId: staffId,
                Limit: 20,
                Start: startChatPagging
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var n = data.length;
                    $('input#totalChatMsg').val(n + parseInt($('input#totalChatMsg').val()));
                    var divClass = '';
                    var userImagePath = $('input#userImagePath').val();
                    var imagePath = $('input#imagePath').val();
                    var staffName = $('input#fullNameLoginId').val();
                    var staffAvatar = $('input#avatarLoginId').val();
                    var customerName = $('input#customerName').val();
                    var customerAvatar = $('input#customerAvatar').val();
                    for (var i = 0; i < n; i++) {
                        if(data[i].IsCustomerSend == 1){
                            divClass = ' right';
                            if(data[i].IsStaffRead == 0) divClass += ' unread';
                        }
                        else divClass = '';
                        html += '<div class="direct-chat-msg' + divClass + '">';
                        html += '<div class="direct-chat-info clearfix">';
                        html += '<span class="direct-chat-name pull-left">' + ((data[i].IsCustomerSend == 1) ? customerName : staffName) + '</span>';
                        html += '<span class="direct-chat-timestamp pull-right">' + data[i].CrDateTime + '</span>';
                        html += '</div><img class="direct-chat-img" src="' + userImagePath + ((data[i].IsCustomerSend == 1) ? customerAvatar : staffAvatar) + '" alt="">';
                        if(data[i].Message != '') html += '<div class="direct-chat-text">' + data[i].Message + '</div></div>';
                        else if(data[i].FileUrl != '') html += '<div class="direct-chat-text"><a href="' + imagePath + data[i].FileUrl + '" target="_blank"><img src="' + imagePath + data[i].FileUrl + '" alt=""></a></div></div>';
                    }
                    if(startChatPagging == '0'){
                        $('#listChat').html(html);
                        scrollChat(false);
                    }
                    else $('#listChat').prepend(html);
                }
            },
            error: function (response) {}
        });
    }
}

function getListChatHeader(staffId){
    $.ajax({
        type: "POST",
        url: $('input#getListChatUnReadUrl').val(),
        data: {
            UserId: staffId,
            IsStaff: 1
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1) {
                var html = '';
                var data = json.data;
                var n = data.length;
                $('span.countMsgChat').text(n);
                var userImagePath = $('input#userImagePath').val();
                var chatPageUrl = $('a#aViewAllChat').attr('href');
                for (var i = 0; i < n; i++) {
                    html += '<li><a href="' + chatPageUrl + '"><div class="pull-left">';
                    html += '<img src="' + userImagePath + data[i].Avatar + '" class="img-circle" alt="' + data[i].FullName + '">';
                    html += '</div><h4>' + data[i].FullName + '<small><i class="fa fa-clock-o"></i> ' + data[i].CrTime + '<br>' + data[i].CrDate + '</small></h4>';
                    html += '<p>' + data[i].Message.replace(/<\/?[^>]+(>|$)/g, "") + '</p></a></li>';
                }
                $('#ulChatHeader').html(html);
                appendCountToPageTitle();
            }
        },
        error: function (response) {}
    });
}

function getListCustomer(staffId, socket){
    socket.emit('user online', {
        roleId: 4
    });
    $.ajax({
        type: "POST",
        url: $('input#getCustomerUrl').val(),
        data: {
            StaffId: staffId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1) {
                var data = json.data;
                var userImagePath = $('input#userImagePath').val();
                var html = '';
                for(var i = 0; i < data.length; i++){
                    html += '<li><a href="javascript:void(0)" data-id="' + data[i].UserId + '" data-name="' + data[i].FullName + '" data-avatar="' + data[i].Avatar + '" data-email="' + data[i].Email + '" data-phone="' + data[i].PhoneNumber + '">';
                    html += '<img class="contacts-list-img" src="' + userImagePath + data[i].Avatar +'" alt="' + data[i].FullName + '">';
                    html += '<div class="contacts-list-info">';
                    html += '<span class="contacts-list-name">' + data[i].FullName + '<br/>' + data[i].PhoneNumber + '<span class="offline"></span></span>';
                    html += '<span class="contacts-list-msg">' + data[i].Email + '</span>';
                    html += '</div></a></li>';
                }
                var eleCustomer = $('ul#chat-staff-care-list');
                eleCustomer.html(html);
                eleCustomer.slimScroll({
                    height: '250px',
                    alwaysVisible: true,
                    wheelStep: 20,
                    touchScrollStep: 500
                });
                socket.on('customer online', function (customerOnlines) {
                    for(var i = 0; i < customerOnlines.length; i++){
                        eleCustomer.find('li a[data-id="' + customerOnlines[i] + '"] span.offline').removeClass('offline').addClass('online');
                    }
                });
            }
        },
        error: function (response) {}
    });
}

function getCountChatUnRead(staffId){
    var customerId = $('input#customerId').val();
    if(staffId > 0 && customerId != '0'){
        $.ajax({
            type: "POST",
            url: $('input#getCountChatUnReadByStaffUrl').val(),
            data: {
                CustomerId: customerId,
                StaffId: staffId
            },
            success: function (response) {
                $('span#countChatUnRead').text(response);
                $('span#countChatUnRead').attr('data-original-title', response + ' Tin Nhắn chưa đọc');
            },
            error: function (response) {}
        });
    }
}