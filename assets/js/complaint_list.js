$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    var curentPathName = window.location.pathname;
    var rootPath = $('#roorPath').val();
    curentPathName = curentPathName.replace(rootPath, '');
    var hostname = window.location.hostname;
    $('#ulComplaintStatus li a').each(function() {
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-default');
        var pageLink = $(this).attr("href");
        pageLink = pageLink.replace('https://', '');
        pageLink = pageLink.replace('http://', '');
        pageLink = pageLink.replace(hostname, '');
        pageLink = pageLink.replace(rootPath, '');
        if(pageLink == curentPathName){
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
            return false;
        }
    });
    $("#tbodyComplaint").on("click", "a.link_delete", function(){
        if($('input#deleteComplaint').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')){
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deleteComplaintUrl').val(),
                    data: {
                        ComplaintId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) $('tr#complaint_' + id).remove();
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa Khiếu nại', 0);
        return false;
    });
});