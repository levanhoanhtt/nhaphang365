$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'mm/yyyy',
        autoclose: true
    });
    $('input#submit').click(function(){
        //var staffId = $('select#staffId').val();
        var beginDate = $('input#beginDate').val();
        var endDate = $('input#endDate').val();
        if(beginDate != '' && endDate != ''){
            var beginDates = beginDate.split('/');
            var endDates = endDate.split('/');
            if(beginDates.length == 2 && endDates.length == 2){
                if(endDates[1] >= beginDates[1]){
                    var flag = false;
                    if(endDates[1] == beginDates[1]){
                        if(endDates[0] >= beginDates[0]) flag = true;
                        else showNotification('Tháng kết thúc không được bé hơn tháng bắt đầu', 0);
                    }
                    else flag = true;
                    if(flag){
                        console.log('ok');
                    }
                }
                else showNotification('Năm kết thúc không được bé hơn năm bắt đầu', 0);
            }
            else showNotification('Tháng bắt đầu và kết thúc không đúng định dạng', 0);
        }
        else showNotification('Vui lòng chọn tháng bắt đầu và kết thúc', 0);
    });
});