$(document).ready(function(){
    $('.match-height').matchHeight();
    $('select#warehouseId').prop('disabled', true);
    $('input#costFee').prop('disabled', true);
    $('input#exchangeRate').prop('disabled', true);
    $('#servicePercent').prop('disabled', true);
    $('.tbodyProduct input, .tbodyProduct textarea').prop('disabled', true);
    $('.btn-add-product').hide();
    $('input#cbIsFixPercent').iCheck('disable');

    //global variable========================================
    var orderStatusId = parseInt($('input#orderStatusId').val());
    var noProductImage = $('input#noProductImage').val();
    var shipTQType = parseInt($('input#shipTQType').val());

    if(orderStatusId == 5 || orderStatusId == 10) $('.tdShipTQ').addClass('tdShipTQ2').prepend('<i class="fa fa-level-up ship_action" data-status="3" title="Tăng giá"></i><i class="fa fa-level-down ship_action" data-status="4" title="Giảm giá"></i>');

    //shop info handle========================================
    //shop Number=============================================
    var orderAccounts = [];
    $('input.orderAccount').each(function(){
        orderAccounts.push({
            OrderAccountId: $(this).attr('data-id'),
            OrderAccountName: $(this).val()
        });
    });
    $('input.cbShop, input.cbIsTransportSlow').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    keyupShopCostNumber();
    $('input.cbIsTransportSlow').on('ifToggled', function(e){
        $.ajax({
            type: "POST",
            url: $('input#changeIsTransportSlowUrl').val(),
            data: {
                ShopCode: e.currentTarget.value,
                OrderId: $('input#orderId').val(),
                IsTransportSlow: e.currentTarget.checked ? 1 : 0
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    });
    var roleId = parseInt($('input#roleLoginId').val());
    $('.btnAddShopNumber').click(function(){
        var shopCode = $(this).attr('data-id');
        var userLoginId = parseInt($('input#userLoginId').val());
        var html = '<tr class="trShopNumber trShopNumber_0" data-shop="' + shopCode + '">';
        html += '<td><input type="text" class="form-control shopNo" value=""></td>';
        html += '<td><input type="text" class="form-control shopCostNumber shopCost" value="0"></td>';
        if(roleId == 8) html += '<td><input type="text" class="form-control shopCostNumber shopCostAdmin" value="0"></td>';
        html += '<td><select class="form-control orderAccountId">';
        for(var i = 0; i < orderAccounts.length; i++){
            if(userLoginId == 103){ //bich phuong
                if(orderAccounts[i].OrderAccountId == 2) html += '<option value="' + orderAccounts[i].OrderAccountId + '" selected="selected">' + orderAccounts[i].OrderAccountName + '</option>';
                else html += '<option value="' + orderAccounts[i].OrderAccountId + '">' + orderAccounts[i].OrderAccountName + '</option>';
            }
            else if(userLoginId == 104){ //cao huong
                if(orderAccounts[i].OrderAccountId == 3) html += '<option value="' + orderAccounts[i].OrderAccountId + '" selected="selected">' + orderAccounts[i].OrderAccountName + '</option>';
                else html += '<option value="' + orderAccounts[i].OrderAccountId + '">' + orderAccounts[i].OrderAccountName + '</option>';
            }
            else html += '<option value="' + orderAccounts[i].OrderAccountId + '">' + orderAccounts[i].OrderAccountName + '</option>';
        }
        html += '</select></td>';
        html += '<td class="tdPadding"><span class="label label-default">Chưa thanh toán</span></td>';
        html += '<td><input type="text" class="form-control returnCost" value="0"></td>';
        html += '<td class="tdPadding"><input type="checkbox" class="cbShop shopReturn"></td>';
        html += '<td><input type="text" class="form-control shopNumberReason" value=""></td>';
        html += '<td><input type="text" class="form-control shopNumberComment" value=""></td>';
        html += '<td class="actions"><i class="fa fa-trash-o link_delete_shop_number" title="Xóa" data-id="0"></i></td></tr>';
        $('#tbodyShopNumber_' + shopCode).append(html);
        keyupShopCostNumber();
        $('input.cbShop').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
    $('.tblShopNumber').on('click', '.link_delete_shop_number', function(){
        var tr = $(this).parent().parent();
        var id = parseInt($(this).attr('data-id'));
        if(id > 0){
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                $.ajax({
                    type: "POST",
                    url: $('input#deleteShopNumberUrl').val(),
                    data: {
                        ShopNumberId: id,
                        OrderId: $('input#orderId').val()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) tr.remove();
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else tr.remove();
    });
    $('.tblShopNumber').on('click', '.link_update_shop_number', function(){
        var id = parseInt($(this).attr('data-id'));
        if(id > 0){
            var tr = $('#trShopNumber_' + id);
            var shopNo = tr.find('input.shopNo').val().trim();
            var shopCost = replaceCost1(tr.find('input.shopCost').val().trim());
            var shopCostAdmin = 0;
            if(roleId == 8) shopCostAdmin = replaceCost1(tr.find('input.shopCostAdmin').val().trim());
            var isReturn = 0;
            if(tr.find('input.shopReturn').parent('div').hasClass('checked')) isReturn = 1;
            $.ajax({
                type: "POST",
                url: $('input#updateShopNumberUrl').val(),
                data: {
                    ShopNumberId: id,
                    OrderId: $('input#orderId').val(),
                    ShopNo: shopNo,
                    ShopCost: shopCost,
                    ShopCostAdmin: shopCostAdmin,
                    ReturnCost: replaceCost1(tr.find('input.returnCost').val().trim()),
                    IsReturn: isReturn,
                    OrderAccountId: tr.find('select.orderAccountId').val(),
                    ShopPaidStatusId: $('input#shopPaidStatusId_' + id).val(),
                    BankAccountId: $('input#bankAccountId_' + id).val(),
                    Reason: tr.find('input.shopNumberReason').val().trim(),
                    Comment: tr.find('input.shopNumberComment').val().trim(),
                    OrderStatusId: orderStatusId,
                    UpdateTypeId: 1
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        orderStatusId = 10;
                        $('input#orderStatusId').val(orderStatusId);
                        if(isReturn == 1){
                            tr.find('input.returnCost').prop('disabled', true);
                            tr.find('input.shopReturn').iCheck('disable');
                        }
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
    $('#btnUpdateShopNumber').click(function(){
        var shopNumbers = [];
        var shopCostAdmin = 0;
        var isReturn = 0;
        var n = 0;
        $('.tblShopNumber .trShopNumber_0').each(function(){
            n++;
            if(roleId == 8) shopCostAdmin = replaceCost1($(this).find('input.shopCostAdmin').val().trim());
            if($(this).find('input.shopReturn').parent('div').hasClass('checked')) isReturn = 1;
            else isReturn = 0;
            shopNumbers.push({
                ShopCode: $(this).attr('data-shop'),
                ShopNo: $(this).find('input.shopNo').val().trim(),
                ShopCost: replaceCost1($(this).find('input.shopCost').val().trim()),
                ShopCostAdmin: shopCostAdmin,
                ReturnCost: replaceCost1($(this).find('input.returnCost').val().trim()),
                IsReturn: isReturn,
                OrderAccountId: $(this).find('select.orderAccountId').val(),
                Reason: $(this).find('input.shopNumberReason').val().trim(),
                Comment: $(this).find('input.shopNumberComment').val().trim()
            });
        });
        if(n > 0){
            var flag = true;
            for(var i = 0; i < n; i++){
                if(shopNumbers[i].ShopNo == '' || shopNumbers[i].ShopCost <= 0){
                    showNotification('Nhập Số thứ tự hợp lệ ở shop ' + shopNumbers[i].ShopCode, 0);
                    flag = false;
                    break;
                }
            }
            if(flag){
                $('#btnUpdateShopNumber').prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#insertShopNumberUrl').val(),
                    data: {
                        OrderId: $('input#orderId').val(),
                        ShopNumberJson: JSON.stringify(shopNumbers),
                        OrderStatusId: orderStatusId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) redirect(true, '');
                        else $('#btnUpdateShopNumber').prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        $('#btnUpdateShopNumber').prop('disabled', false);
                    }
                });
            }
        }
        else showNotification('Không có shop để thanh toán', 0);
    });
    $('#btnShopInfo').click(function(){
        $.ajax({
            type: "POST",
            url: $('input#getShopNumberUrl').val(),
            data: {
                OrderId: $('input#orderId').val()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1){
                    var data = json.data;
                    var html = '';
                    var shopCost = 0;
                    var returnCost = 0;
                    var orderAccountName = '';
                    var sumShopCostReal = 0;
                    var sumShopCost = 0;
                    var sumReturnCost = 0;
                    var i, j;
                    for (var shopCode in data){
                        shopCost = 0;
                        returnCost = 0;
                        html += '<tr>';
                        html += '<td>' + shopCode + '</td>';
                        html += '<td>';
                        for(i = 0; i < data[shopCode].length; i++){
                            shopCost += parseFloat(data[shopCode][i].ShopCost);
                            returnCost += parseFloat(data[shopCode][i].ReturnCost);
                            orderAccountName = '';
                            for(j = 0; j < orderAccounts.length; j++){
                                if(orderAccounts[j].OrderAccountId == data[shopCode][i].OrderAccountId){
                                    orderAccountName = orderAccounts[j].OrderAccountName;
                                    break;
                                }
                            }
                            html += '<p>' + data[shopCode][i].ShopNo + ' - ' + orderAccountName + ' - ' + formatDecimal(data[shopCode][i].ShopCost) + '</p>'
                        }
                        sumShopCost += shopCost;
                        sumReturnCost += returnCost;
                        html += '</td>';
                        html += '<td>' + $('#tbodyProduct_' + shopCode + ' .td_sum_cost_ship').text() + '</td>';
                        sumShopCostReal += replaceCost1($('#tbodyProduct_' + shopCode + ' .td_sum_cost_ship').text());
                        html += '<td>' + formatDecimal(shopCost.toFixed(2).toString()) + '</td>';
                        html += '<td>' + formatDecimal(returnCost.toFixed(2).toString()) + '</td>';
                        html += '</tr>';
                    }
                    html += '<tr class="info"><td></td><td>Tổng</td><td>' + formatDecimal(sumShopCostReal.toFixed(2).toString()) + '</td><td>' + formatDecimal(sumShopCost.toFixed(2).toString()) + '</td><td>' + formatDecimal(sumReturnCost.toFixed(2).toString()) + '</td></tr>';
                    $('#tbodyShopNumberInfo').html(html);
                    $('#modalShopNumberInfo').modal('show');
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('#btnUpdateShopNumber').prop('disabled', false);
            }
        });
    });
    //shop tracking======================================================
    $('.btnShopTracking').click(function(){
        var shopCode = $(this).attr('data-id');
        $('input#shopCodeCurent').val(shopCode);
        $('#modalShopTracking h4').text('Cập nhật Mã vận đơn cho shop ' + shopCode);
        $('#modalShopTracking .trShopTracking').remove();
        var html = '';
        var id = '';
        $('.shopTracking_' + shopCode).each(function(){
            id = $(this).attr('data-id');
            html += '<tr class="trShopTracking" id="trShopTracking_' + id + '">';
            html += '<td id="tracking_' + id + '">' + $(this).val() + '</td>';
            html += '<td id="trackingReturnName_' + id + '">';
            if($(this).attr('data-is-return') == '1') html += '<span class="label label-success">Đã về</span></td>';
            else html += '<span class="label label-default">Chưa về</span></td>';
            html += '<td id="trackingComment_' + id + '">' + $(this).attr('data-comment') + '</td>';
            html += '<td class="actions">' +
                '<a href="javascript:void(0)" class="link_edit_shopTracking" data-id="' + id + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                '<a href="javascript:void(0)" class="link_delete_shopTracking" data-id="' + id + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                '<input type="text" hidden="hidden" id="trackingReturn_' + id + '" value="' + $(this).attr('data-is-return') + '">' +
                '</td>';
            html += '</tr>';
        });
        $('#tbodyShopTracking').prepend(html);
        $('#modalShopTracking').modal('show');
    });
    $("#tbodyShopTracking").on("click", "a.link_edit_shopTracking", function(){
        var id = $(this).attr('data-id');
        $('input#shopTrackingId').val(id);
        $('input#tracking').val($('td#tracking_' + id).text());
        $('input#trackingComment').val($('td#trackingComment_' + id).text());
        if($('input#trackingReturn_' + id).val() == '1') $('input#trackingReturn').iCheck('check');
        else $('input#trackingReturn').iCheck('uncheck');
        scrollTo('input#tracking');
        return false;
    });
    $('a#link_cancel_shopTracking').click(function(){
        $('#shopTrackingForm').trigger("reset");
        $('input#trackingReturn').iCheck('uncheck');
        return false;
    });
    $("#tbodyShopTracking").on("click", "a.link_delete_shopTracking", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteShopTrackingUrl').val(),
                data: {
                    ShopTrackingId: id,
                    OrderId: $('input#orderId').val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('#trShopTracking_' + id).remove();
                        $('input#shopTracking_' + id).remove();
                        var shopCode = $('input#shopCodeCurent').val();
                        var n = $('input.shopTracking_' + shopCode).length;
                        $('span#countShopTracking_' + shopCode).text(n);
                        if(n == 0) $('input#shopTrackingCurent_' + shopCode).val('');
                        else $('input#shopTrackingCurent_' + shopCode).val($('input.shopTracking_' + shopCode).first().val());
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_update_shopTracking').click(function(){
        if (validatePart('#shopTrackingForm')) {
            var form = $('#shopTrackingForm');
            var id = parseInt($('input#shopTrackingId').val());
            var shopCode = $('input#shopCodeCurent').val();
            var isReturn = 0;
            if($('input#trackingReturn').parent('div').hasClass('checked')) isReturn = 1;
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: {
                    ShopTrackingId: id,
                    ShopCode: shopCode,
                    OrderId: $('input#orderId').val(),
                    Tracking: $('input#tracking').val().trim(),
                    IsReturn: isReturn,
                    Comment: $('input#trackingComment').val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        form.trigger("reset");
                        $('input#trackingReturn').iCheck('uncheck');
                        var data = json.data;
                        if(data.IsAdd == 1){
                            id = data.ShopTrackingId;
                            var html = '<tr class="trShopTracking" id="trShopTracking_' + id + '">';
                            html += '<td id="tracking_' + id + '">' + data.Tracking + '</td>';
                            html += '<td id="trackingReturnName_' + id + '">';
                            if(isReturn == 1) html += '<span class="label label-success">Đã về</span></td>';
                            else html += '<span class="label label-default">Chưa về</span></td>';
                            html += '<td id="trackingComment_' + id + '">' + data.Comment + '</td>';
                            html += '<td class="actions">' +
                                '<a href="javascript:void(0)" class="link_edit_shopTracking" data-id="' + id + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                '<a href="javascript:void(0)" class="link_delete_shopTracking" data-id="' + id + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                '<input type="text" hidden="hidden" id="trackingReturn_' + id + '" value="' + isReturn + '">' +
                                '</td>';
                            html += '</tr>';
                            $('#tbodyShopTracking').prepend(html);
                        }
                        else{
                            $('input#shopTracking_' + id).remove();
                            $('td#tracking_' + id).text(data.Tracking);
                            $('td#trackingComment_' + id).text(data.Comment);
                            if(isReturn == 1) $('td#trackingReturnName_' + id).html('<span class="label label-success">Đã về</span>');
                            else $('td#trackingReturnName_' + id).html('<span class="label label-default">Chưa về</span>');
                        }
                        $('#divShopTracking_' + shopCode).append('<input type="text" class="shopTracking_' + shopCode + '" id="shopTracking_' + id + '" data-id="' + id + '" data-is-return="' + isReturn + '" data-comment="' + data.Comment + '" value="' + data.Tracking + '">');
                        var n = $('input.shopTracking_' + shopCode).length;
                        $('span#countShopTracking_' + shopCode).text(n);
                        if(n == 0) $('input#shopTrackingCurent_' + shopCode).val('');
                        else $('input#shopTrackingCurent_' + shopCode).val($('input.shopTracking_' + shopCode).first().val());
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });

    //da dat hang===============================================
    $('#btnOrderDone').click(function(){
        $('#btnOrderDone').prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#changeStatusOrderUrl').val(),
            data: {
                OrderId: $('input#orderId').val(),
                OrderStatusId: 6,
                CustomerId: $('input#customerId').val(),
                CareStaffId: $('input#careStaffId').val(),
                OrderCode: $('#tdOrderCode').text()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) {
                    orderStatusId = 6;
                    $('#btnCurent').text('Đã đặt hàng');
                    $('input#orderStatusId').val(orderStatusId);
                }
                else $('#btnOrderDone').prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('#btnOrderDone').prop('disabled', false);
            }
        });
    });

    //product action  with teransaction==========================
    productAction(orderStatusId, shipTQType);
    //shop action  with teransaction==========================
    shopAction(orderStatusId, shipTQType);

    //Revenue=============================================
    orderRevenue();
    //complaint===========================================
    complaintAction();
});

function keyupShopCostNumber(){
    $('.tblShopNumber').on('keyup', 'input.shopCostNumber', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
}