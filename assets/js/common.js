$(document).ready(function() {
    var siteName = $('input#siteName').val();
    if(siteName != ''){
        siteName = $('title').text() + ' - ' + siteName;
        $('title').text(siteName);
        $('input#siteName').val(siteName);
    }
    if($('select.select2').length > 0) $('select.select2').select2();
    if($('div.divTable').length > 0) {
        if ($(window).width() > 760) $('div.divTable').removeClass('table-responsive');
        else $('div.divTable').addClass('table-responsive');
        $(window).resize(function () {
            if ($(window).width() > 760) $('div.divTable').removeClass('table-responsive');
            else $('div.divTable').addClass('table-responsive');
        });
    }
    $(document).ajaxStart(function() { Pace.restart(); });

    var curentPathName = window.location.pathname;
    var rootPath = $('#roorPath').val();
    curentPathName = curentPathName.replace(rootPath, '');
    var hostname = window.location.hostname;
    //admin menu
    if($('.sidebar-menu').length > 0){
        $('.sidebar-menu li a').each(function () {
            var pageLink = $(this).attr('href');
            if (pageLink != undefined) {
                pageLink = pageLink.replace('https://', '');
                pageLink = pageLink.replace('http://', '');
                pageLink = pageLink.replace(hostname, '');
                pageLink = pageLink.replace(rootPath, '');
                if (pageLink == curentPathName) {
                    $(this).parent('li').addClass('active');
                    var ul = $(this).parent('li').parent('ul').parent('li').parent('ul');
                    if (ul.length > 0) {
                        $(this).parent('li').parent('ul').css('display', 'block');
                        $(this).parent('li').parent('ul').parent('li').addClass('active');
                        ul.css('display', 'block');
                        ul.parent('li').addClass('active');
                    }
                    else $(this).parent('li').parent('ul').parent('li').addClass('active');
                    return false;
                }
            }
        });
    }
    //cusstomer menu
    if($('#navbar-customer').length > 0){
        $('ul#navbar-customer li a').each(function() {
            var pageLink = $(this).attr("href");
            pageLink = pageLink.replace('https://', '');
            pageLink = pageLink.replace('http://', '');
            pageLink = pageLink.replace(hostname, '');
            pageLink = pageLink.replace(rootPath, '');
            if(pageLink == curentPathName){
                $(this).parent('li').addClass('active');
                return false;
            }
        });
    }
    //notification header
    if($('#ulNotificationHeader').length > 0){
        getListNotificationHeader();
        setInterval(getListNotificationHeader, 60000);
        $('#ulNotificationHeader').on("click", "a.link_to", function(){
            var url = $(this).attr('href');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusNotificationUrl').val(),
                data: {
                    NotificationId: $(this).attr('data-id'),
                    NotificationStatusId: 2
                },
                success: function (response) {
                    if(url != 'javascript:void(0)') window.location.href = url;
                },
                error: function (response) {
                    if(url != 'javascript:void(0)') window.location.href = url;
                }
            });
            return false;
        });
    }
    //customer balance
    if($('.spanBalance').length > 0){
        $.ajax({
            type: "POST",
            url: $('input#getBalanceUrl').val(),
            data: {
                CustomerId: $('input#userLoginId').val()
            },
            success: function (response) {
                $('.spanBalance').text(response);
            },
            error: function (response) {}
        });
    }
    $('#aBalance').click(function(){
        if($('input#isGetRechargeText').val() == '0'){
            $.ajax({
                type: "POST",
                url: $('input#getArticleContentUrl').val(),
                data: {
                    ArticleId: 1
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1){
                        $('#modalBalance div.modal-body').html(json.data);
                        $('#modalBalance').modal('show');
                        $('input#isGetRechargeText').val('1');
                    }
                },
                error: function (response) {}
            });
        }
        else $('#modalBalance').modal('show');
        return false;
    });
    //chat
    $(document).on('click', '.maximizechat', function () {
        $(this).hide();
        $(this).closest('.chat-box').find('.direct-chat').slideDown();
    });
    $(document).on('click', '.chat-box .btn-tool-close', function () {
        $('.maximizechat').show(1200);
    });
    $(document).on('click', '.btn-chat-toggle', function () {
        $('.chat-wrapper .direct-chat').toggleClass('direct-chat-contacts-open');
        if($('.chat-wrapper .direct-chat').hasClass('direct-chat-contacts-open')) $('.btn-chat-toggle').text('Thu gọn');
        else  $('.btn-chat-toggle').text('Mở rộng');
    });
});

function getListNotificationHeader(){
    var roleLoginId = parseInt($('input#roleLoginId').val());
    if(roleLoginId > 0) {
        $.ajax({
            type: "POST",
            url: $('input#getListNotificationHeaderUrl').val(),
            data: {
                UserId: $('input#userLoginId').val(),
                RoleId: roleLoginId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1) {
                    var html = '';
                    var data = json.data;
                    var n = data.length;
                    $('span.countNotification').text(n);
                    var userImagePath = $('input#userImagePath').val();
                    var notificationPageUrl = $('#aViewAllNotification').attr('href');
                    for (var i = 0; i < n; i++) {
                        html += '<li><a href="' + ((data[i].LinkTo == '') ? notificationPageUrl : data[i].LinkTo) + '" class="link_to" data-id="' + data[i].NotificationId + '"><div class="pull-left">';
                        if (roleLoginId == 4) {
                            html += '<img src="assets/vendor/dist/img/logo.png" class="img-circle" alt="Đặt hàng 86">';
                            html += '</div><h4>Đặt hàng 86<small>';
                        }
                        else {
                            html += '<img src="' + userImagePath + data[i].CustomerAvatar + '" class="img-circle" alt="' + data[i].CustomerName + '">';
                            html += '</div><h4>' + data[i].CustomerName + '<small>';
                        }
                        html += '<i class="fa fa-clock-o"></i> ' + data[i].CrTime + '<br>' + data[i].CrDate + '</small></h4>';
                        html += '<p>' + data[i].Message + '</p></a></li>';
                    }
                    $('#ulNotificationHeader').html(html);
                    appendCountToPageTitle();
                    if(n > 0) $.playSound('assets/vendor/dist/chat');
                }
            },
            error: function (response) {}
        });
    }
}

/* type = 1 - success
other - error
*/
function showNotification(msg, type){
    var typeText = 'error';
    if(type == 1) typeText = 'success';
    var notice = new PNotify({
        title: 'Thông báo',
        text: msg,
        type: typeText,
        delay: 2000,
        addclass: 'stack-bottomright',
        stack: {"dir1": "up", "dir2": "left", "firstpos1": 15, "firstpos2": 15}
    });
}

function redirect(reload, url){
    if(reload){
        window.setTimeout(function () {
            window.location.reload(true);
        }, 2000);
    }
    else{
        window.setTimeout(function() {
            window.location.href = url;
        }, 2000);
    }
}

//set count to title page
function appendCountToPageTitle(){
    var count = $('#ulNotificationHeader li').length + $('#ulChatHeader li').length;
    if(count > 0) $('title').text('(' + count + ') ' + $('input#siteName').val());
}

//validate
function validate(){
    var flag = true;
    $('.hmdrequired').each(function(){
        if($(this).val().trim() == ''){
            showNotification($(this).attr('data-field') + ' không được bỏ trống', 0);
            $(this).focus();
            flag = false;
            return false;
        }
    });
    return flag;
}

function validateCost(){
    var flag = true;
    $('.hmdrequiredCost').each(function(){
        if(parseInt($(this).val().trim()) <= 0){
            showNotification($(this).attr('data-field') + ' phải lơn hơn 0', 0);
            $(this).focus();
            flag = false;
            return false;
        }
    });
    return flag;
}

function validateCost1(){
    var flag = true;
    $('.hmdrequiredCost1').each(function(){
        if(parseFloat($(this).val().trim()) <= 0){
            showNotification($(this).attr('data-field') + ' phải lơn hơn 0', 0);
            $(this).focus();
            flag = false;
            return false;
        }
    });
    return flag;
}

function validatePart(container){
    var flag = true;
    $(container + ' .hmdrequired').each(function(){
        if($(this).val().trim() == ''){
            showNotification($(this).attr('data-field') + ' không được bỏ trống', 0);
            $(this).focus();
            flag = false;
            return false;
        }
    });
    return flag;
}

function checkEmptyEditor(text){
    text = text.replace(/\&nbsp;/g, '').replace(/\<p>/g, '').replace(/\<\/p>/g, '').trim();
    return text.length > 0;
}

//price format
function replaceCost(cost) {
    cost = cost.replace(/\,/g, '');
    if(cost == '') cost = 0;
    return parseInt(cost);
}

function replaceCost1(cost) {
    cost = cost.replace(/\,/g, '');
    if(cost == '') cost = 0;
    return parseFloat(cost);
}

function formatCost() {
    $('input.cost').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: ','
    });
    $('input.cost1').each(function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
}

function formatCostPart(inputClass) {
    $('input.' + inputClass).each(function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
}

function formatDecimal(value){
    value = value.replace(/\,/g, '');
    while(value.length > 1 && value[0] == '0' && value[1] != '.') value = value.substring(1);
    if(value != '' && value != '0'){
        if(value[value.length - 1] != '.') {
            if(value.indexOf('.00') > 0) value = value.substring(0, value.length - 3);
            value = addCommas(value);
            return value;
        }
        else return value;
    }
    else return 0;
}

function addCommas(nStr){
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function scrollTo(eleId){
    $('html, body').animate({
        scrollTop: $(eleId).offset().top - 200
    }, 1000);
    $(eleId).focus();
}

function makeSkug(str){
    var slug = str.toLowerCase();
    // change vietnam character
    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');
    // remove special character
    slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
    // change space to -
    slug = slug.replace(/ /gi, "-");
    slug = slug.replace(/\-\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-\-/gi, '-');
    slug = slug.replace(/\-\-\-/gi, '-');
    slug = slug.replace(/\-\-/gi, '-');
    slug = '@' + slug + '@';
    slug = slug.replace(/\@\-|\-\@|\@/gi, '');
    return slug;
}

//chat
function scrollChat(isOpenChat){
    if(isOpenChat && $('.maximizechat').length > 0){
        $('.maximizechat').hide();
        $('.maximizechat').closest('.chat-box').find('.direct-chat').slideDown();
        $('.direct-chat').slideDown();
    }
    var height = 250;
    if($('div#chatToCustomerPage').length > 0) height = 350;
    else if($('div#chatToStaffPage').length > 0) height = 350;
    $('#listChat').slimScroll({
        height: height + 'px',
        alwaysVisible: true,
        wheelStep: 20,
        touchScrollStep: 500,
        start: 'bottom',
        scrollTo : $('#listChat').prop('scrollHeight') + 'px'
    });
}

function getCurentDateTime(){
    var now = new Date();
    var date = now.getDate();
    var month = now.getMonth() + 1;
    var hour = now.getHours();
    var minute = now.getMinutes();
    var second = now.getSeconds();
    if(date < 10) date = '0' + date;
    if(month < 10) month = '0' + month;
    if(hour < 10) hour = '0' + hour;
    if(minute < 10) minute = '0' + minute;
    if(second < 10) second = '0' + second;
    return date + "/" + month + "/" + now.getFullYear() + " " + hour + ":" + minute + ":" + second;
}

//pagging
function pagging(pageId){
    $('input#pageId').val(pageId);
    $('input#submit').trigger('click');
}