$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $("#tbodyTransport").on("click", "a.link_status", function(){
        if($('input#changeStatus').val() == '1'){
            if (confirm('Bạn đã chắc chắn ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#changeStatusUrl').val(),
                    data: {
                        TransportId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) {
                            $('td#statusName_' + id).html('<span class="label label-success">Đã thu tiền</span>');
                            $('tr#transport_' + id + ' td.actions').html('');
                        }
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật trạng thái phiếu thu Vận chuyển', 0);
        return false;
    });    
});