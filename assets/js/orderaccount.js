$(document).ready(function(){
    $("#tbodyOrderAccount").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#orderAccountId').val(id);
        $('input#orderAccountName').val($('td#orderAccountName_' + id).text());
        scrollTo('input#orderAccountName');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#orderAccountForm').trigger("reset");
        return false;
    });
    $("#tbodyOrderAccount").on("click", "a.link_delete", function(){
        if($('input#deleteOrderAccount').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deleteOrderAccountUrl').val(),
                    data: {
                        OrderAccountId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) $('tr#orderAccount_' + id).remove();
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa Tài khoản đặt hàng', 0);
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#updateOrderAccount').val() == '1') {
            if (validate()) {
                var form = $('#orderAccountForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            form.trigger("reset");
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr id="orderAccount_' + data.OrderAccountId + '">';
                                html += '<td id="orderAccountName_' + data.OrderAccountId + '">' + data.OrderAccountName + '</td>';
                                html += '<td class="actions">' +
                                    '<a href="javascript:void(0)" class="link_edit" data-id="' + data.OrderAccountId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                    '<a href="javascript:void(0)" class="link_delete" data-id="' + data.OrderAccountId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                    '</td>';
                                html += '</tr>';
                                $('#tbodyOrderAccount').prepend(html);
                            }
                            else $('td#orderAccountName_' + data.OrderAccountId).text(data.OrderAccountName);
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật Tài khoản đặt hàng', 0);
        return false;
    });
});