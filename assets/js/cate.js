$(document).ready(function(){
    $('select#cateTypeIdSearch').change(function(){
        window.location.href = $('input#cateListUrl').val() + '/' + $(this).val();
    });
    $('#btnChooseImage').click(function(){
        var finder = new CKFinder();
        finder.resourceType = 'Images';
        finder.selectActionFunction = function (fileUrl) {
            $('input#cateImage').val(fileUrl);
            $('#imgCate').attr('src', fileUrl).show();
        };
        finder.popup();
    });
    $("#tbodyCate").on("blur", "input#cateName", function(){
        $('input#cateSlug').val(makeSkug($(this).val()));
    });
    $("#tbodyCate").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#cateId').val(id);
        $('select#displayOrder').val($('select#displayOrder_' + id).val());
        $('input#cateName').val($('td#cateName_' + id).text());
        $('input#cateSlug').val($('input#cateSlug_' + id).val());
        $('input#cateNameTQ').val($('td#cateNameTQ_' + id).text());
        $('#cateDesc').val($('td#cateDesc_' + id).text());
        $('input#countNumber').val($('td#countNumber_' + id).text());
        var fileUrl = $('#imgCate_' + id).attr('src');
        $('input#cateImage').val(fileUrl);
        $('#imgCate').attr('src', fileUrl).show();
        scrollTo('select#displayOrder');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#cateForm').trigger("reset");
        $('#imgCate').hide();
        return false;
    });
    $("#tbodyCate").on("click", "a.link_delete", function(){
        if($('input#deleteCate').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deleteCateUrl').val(),
                    data: {
                        CateId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) $('tr#cate_' + id).remove();
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa chuyên mục', 0);
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#updateCate').val() == '1') {
            if (validate()) {
                var form = $('#cateForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1) redirect(true, '');
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật chuyên mục', 0);
        return false;
    });
});
function changeDisplayOrder(select, cateId){
    if($('input#updateCate').val() == '1') {
        $.ajax({
            type: "POST",
            url: $('input#changeDisplayOrderUrl').val(),
            data: {
                CateId: cateId,
                CateTypeId: $('input#cateTypeId').val(),
                DisplayOrder: select.value
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) redirect(true, '');
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
    else showNotification('Bạn không có quyền cập nhật chuyên mục', 0);
}