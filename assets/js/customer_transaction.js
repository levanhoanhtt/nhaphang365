$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('#tbodyTransaction').on('click', 'i.fa', function(){
        var id = $(this).attr('data-id');
        $('.tr' + id).slideToggle();
        if($(this).hasClass('fa-caret-square-o-right')) $(this).removeClass('fa-caret-square-o-right').addClass('fa-caret-square-o-down');
        else $(this).removeClass('fa-caret-square-o-down').addClass('fa-caret-square-o-right');
    });
    $('i.all').click(function(){
        if($(this).hasClass('fa-caret-square-o-right')){
            $(this).removeClass('fa-caret-square-o-right').addClass('fa-caret-square-o-down');
            $('tr.thead').show();
            $('#tbodyTransaction i.fa').removeClass('fa-caret-square-o-right').addClass('fa-caret-square-o-down');
        }
        else{
            $(this).removeClass('fa-caret-square-o-down').addClass('fa-caret-square-o-right');
            $('tr.thead').hide();
            $('#tbodyTransaction i.fa').removeClass('fa-caret-square-o-down').addClass('fa-caret-square-o-right');
        }
    });
});