$(document).ready(function(){
    $("#tbodyTransportType").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#transportTypeId').val(id);
        $('input#transportTypeName').val($('td#transportTypeName_' + id).text());
        $('select#parentTransportTypeId').val($('input#parentTransportTypeId_' + id).val());
        scrollTo('input#transportTypeName');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#transportTypeForm').trigger("reset");
        return false;
    });
    $("#tbodyTransportType").on("click", "a.link_delete", function(){
        if($('input#deleteTransportType').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deleteTransportTypeUrl').val(),
                    data: {
                        TransportTypeId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) $('tr#transportType_' + id).remove();
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa Phương thức giao hàng', 0);
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#updateTransportType').val() == '1') {
            if (validate()) {
                var form = $('#transportTypeForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            form.trigger("reset");
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr id="transportType_' + data.TransportTypeId + '">';
                                html += '<td id="transportTypeName_' + data.TransportTypeId + '">' + data.TransportTypeName + '</td>';
                                html += '<td id="parentTransportTypeName_' + data.TransportTypeId + '">' + data.ParentTransportTypeName + '</td>';
                                html += '<td class="actions">' +
                                    '<a href="javascript:void(0)" class="link_edit" data-id="' + data.TransportTypeId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                    '<a href="javascript:void(0)" class="link_delete" data-id="' + data.TransportTypeId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                    '<input type="text" hidden="hidden" id="parentTransportTypeId_' + data.TransportTypeId + '" value="' + data.ParentTransportTypeId + '">' +
                                    '</td>';
                                html += '</tr>';
                                $('#tbodyTransportType').prepend(html);
                                if(data.ParentTransportTypeId == 0) $('select#parentTransportTypeId').append('<option value="' + data.TransportTypeId + '">' + data.TransportTypeName + '</option>');
                            }
                            else{
                                $('td#transportTypeName_' + data.TransportTypeId).text(data.TransportTypeName);
                                $('td#parentTransportTypeName_' + data.TransportTypeId).text(data.ParentTransportTypeName);
                            }
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật Phương thức giao hàng', 0);
        return false;
    });
});