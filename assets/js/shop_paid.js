$(document).ready(function(){
    if($('#ulReturnStatus').length > 0){
        var curentPathName = window.location.pathname;
        var rootPath = $('#roorPath').val();
        curentPathName = curentPathName.replace(rootPath, '');
        var hostname = window.location.hostname;
        $('#ulReturnStatus li a').each(function() {
            $(this).removeClass('btn-primary');
            $(this).addClass('btn-default');
            var pageLink = $(this).attr("href");
            pageLink = pageLink.replace('https://', '');
            pageLink = pageLink.replace('http://', '');
            pageLink = pageLink.replace(hostname, '');
            pageLink = pageLink.replace(rootPath, '');
            if(pageLink == curentPathName){
                $(this).removeClass('btn-default');
                $(this).addClass('btn-primary');
                return false;
            }
        });
    }
    var shopIdOld = '';
    var shopId = '';
    var rowspan = 1;
    $('#tbodyShopNumber tr').each(function(){
        shopId = $(this).attr('data-id');
        if(shopId != shopIdOld){
            shopIdOld = shopId;
            rowspan = $('#tbodyShopNumber tr[data-id="' + shopId + '"]').length;
            if(rowspan > 1){
                var trFirst = $('#tbodyShopNumber tr[data-id="' + shopId + '"]').first();
                trFirst.find('.tdMerge').attr('rowspan', rowspan);
                $('#tbodyShopNumber tr[data-id="' + shopId + '"] .tdMerge[rowspan="1"]').remove();
            }
        }
    });
    $('#tbodyShopNumber').on('click', '.btnUpdate', function(){
        var id = $(this).attr('data-id');
        var shopNo = $('input#shopNo_' + id).val().trim();
        var shopCost = replaceCost1($('input#shopCost_' + id).val().trim());
        if(shopNo != '' && shopCost > 0) {
            var shopCostAdmin = replaceCost1($('input#shopCostAdmin_' + id).val().trim());
            if(shopCostAdmin == 0) shopCostAdmin = shopCost;
            $.ajax({
                type: "POST",
                url: $('input#updateShopNumberUrl').val(),
                data: {
                    ShopNumberId: id,
                    OrderId: $('input#orderId_' + id).val(),
                    ShopNo: shopNo,
                    ShopCost: shopCost,
                    ShopCostAdmin: shopCostAdmin,
                    ReturnCost: replaceCost1($('input#returnCost_' + id).val()),
                    IsReturn: $('input#isReturn_' + id).val(),
                    OrderAccountId: $('select#orderAccountId_' + id).val(),
                    ShopPaidStatusId: 3,
                    BankAccountId: $('select#bankAccountId_' + id).val(),
                    Reason: $('input#reason_' + id).val(),
                    Comment: $('input#comment_' + id).val().trim(),
                    OrderStatusId: 10,
                    UpdateTypeId: 2
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('tr#shopNumber_' + id).find('td.actions').html(json.data);
                        var shopNumberCount = parseInt($('span#shopNumberCount').text());
                        $('span#shopNumberCount').text(shopNumberCount - 1);
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else showNotification('Số thứ tự và giá không được để trống', 0);
        return false;
    });
    $('#tbodyShopNumber').on('click', '.btnUpdateReturn', function(){
        var id = $(this).attr('data-id');
        var shopNo = $('input#shopNo_' + id).val().trim();
        var shopCost = replaceCost1($('input#shopCost_' + id).val().trim());
        var returnCost = replaceCost1($('input#returnCost_' + id).val());
        if(shopNo != '' && shopCost > 0 && returnCost > 0) {
            var shopCostAdmin = replaceCost1($('input#shopCostAdmin_' + id).val().trim());
            if(shopCostAdmin == 0) shopCostAdmin = shopCost;
            $.ajax({
                type: "POST",
                url: $('input#updateShopNumberUrl').val(),
                data: {
                    ShopNumberId: id,
                    OrderId: $('input#orderId_' + id).val(),
                    ShopNo: shopNo,
                    ShopCost: shopCost,
                    ShopCostAdmin: shopCostAdmin,
                    ReturnCost: returnCost,
                    IsReturn: 1,
                    OrderAccountId: $('select#orderAccountId_' + id).val(),
                    ShopPaidStatusId: $('input#shopPaidStatusId_' + id).val(),
                    BankAccountId: $('select#bankAccountId_' + id).val(),
                    Reason: $('input#reason_' + id).val(),
                    Comment: $('input#comment_' + id).val().trim(),
                    OrderStatusId: 10,
                    UpdateTypeId: 3
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) $('tr#shopNumber_' + id).find('td.actions').html('');
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else showNotification('Số thứ tự và giá không được để trống', 0);
        return false;
    });
    $('#tbodyShopNumber').on('click', '.btnDelete', function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#deleteShopNumberUrl').val(),
                data: {
                    ShopNumberId: id,
                    OrderId: $('input#orderId_' + id).val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
});
