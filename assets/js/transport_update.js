$(document).ready(function(){
    $('input#cbCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    if($('input#statusId').val() == '2'){
        $('input#cbCheck').iCheck('disable');
        $('input#statusId').val('3');
    }
    $('input#cbCheck').on('ifToggled', function(e){
        if(e.currentTarget.checked) $('input#statusId').val('2');
        else $('input#statusId').val('1');
    });
    $('input.cost').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: ','
    });
    var transportFees = getTransportFees();
    $('#transportForm').on('keyup', 'input.cost1', setPaidVN);
    $('#transportForm').on('keyup', 'input.cost2', function(){
        var value = $(this).val();
        $(this).val(formatDecimal(value));
    });
    $('#transportForm').on('keyup', 'input#weight', function(){
        var weight = replaceCost1($(this).val());
        if(weight > 0) setWeightCost(parseInt($('select#warehouseId').val()), weight, transportFees);
        setPaidVN();
    });
    $('select#warehouseId').change(function(){
        var weight = replaceCost1($('input#weight').val());
        if(weight > 0) setWeightCost(parseInt($(this).val()), weight, transportFees);
        setPaidVN();
    });
    $('input#submit').click(function () {
    //$(document).on('submit','#transportForm',function () {
        if (validateCost1()) {
            $('input#submit').prop('disabled', true);
            var form = $('#transportForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(false, $('input#transportListUrl').val());
                    else $('input#submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('input#submit').prop('disabled', false);
                }
            });
        }
       return false;
    });
});

function setWeightCost(warehouseId, weight, transportFees){
    for (var i = 0; i < transportFees.length; i++) {
        if (warehouseId == transportFees[i].id && weight >= transportFees[i].min && weight < transportFees[i].max) {
            $('input#weightCost').val(transportFees[i].cost);
            break;
        }
    }
}

function setPaidVN(){
    var weight = replaceCost1($('input#weight').val());
    if(weight < 1) weight = 1;
    var paidVN = weight * replaceCost($('input#weightCost').val()) + replaceCost($('input#otherCost').val());
    $('input#paidVN').val(Math.ceil(paidVN));
    $('input#paidVN').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: ','
    });
}

function getTransportFees() {
    var retVal = [];
    var transportFee = {};
    $('input.transportFee').each(function () {
        transportFee = {
            id: parseInt($(this).attr('data-id')),
            min: parseInt($(this).attr('data-min')),
            max: parseInt($(this).attr('data-max')),
            cost: $(this).val()
        };
        retVal.push(transportFee);
    });
    return retVal;
}