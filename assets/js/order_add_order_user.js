$(document).ready(function(){
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $("#tbodyOrder").on("click", "a.link_update", function(){
        var id = $(this).attr('data-id');
        var orderUserId = $('select#orderUserId_' + id).val();
        if(orderUserId != '0'){
            var careStaffId = $('input#careStaffId_' + id).val();
            $.ajax({
                type: "POST",
                url: $('input#updateOrderStaffUrl').val(),
                data: {
                    OrderId: id,
                    CareStaffId: careStaffId,
                    OrderUserId: orderUserId,
                    OldCareStaffId: careStaffId,
                    CustomerId: $('input#customerId_' + id).val(),
                    OrderCode: $('#tdOrderCode_' + id).text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('select#orderUserId_' + id).prop('disabled', true);
                        $('tr#order_' + id + ' td.actions').html('');
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else showNotification('Vui lòng chọn Nhân viên Đặt hàng', 0);
        return false;
    });
});