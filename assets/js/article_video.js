$(document).ready(function(){
    $("#tbodyArticle").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#articleId').val(id);
        $('select#displayOrder').val($('select#displayOrder_' + id).val());
        $('input#articleTitle').val($('td#articleTitle_' + id).text());
        $('input#articleContent').val($('td#articleContent_' + id).text());
        scrollTo('select#displayOrder');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#articleForm').trigger("reset");
        return false;
    });
    $("#tbodyArticle").on("click", "a.link_delete", function(){
        if (confirm('Bạn có thực sự muốn xóa ?')) {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    ArticleId: id,
                    StatusId: 0,
                    ArticleTypeName: 'Video'
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) $('tr#article_' + id).remove();
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('a#link_update').click(function(){
        if(validate()) {
            var form = $('#articleForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(true, '');

                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});
function changeDisplayOrder(select, articleId){
    $.ajax({
        type: "POST",
        url: $('input#changeDisplayOrderUrl').val(),
        data: {
            ArticleId: articleId,
            ArticleTypeId: $('input#articleTypeId').val(),
            DisplayOrder: select.value
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if(json.code == 1) redirect(true, '');
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}