$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    var curentPathName = window.location.pathname;
    var rootPath = $('#roorPath').val();
    curentPathName = curentPathName.replace(rootPath, '');
    var hostname = window.location.hostname;
    $('#ulOrderStatus li a').each(function() {
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-default');
        var pageLink = $(this).attr("href");
        pageLink = pageLink.replace('https://', '');
        pageLink = pageLink.replace('http://', '');
        pageLink = pageLink.replace(hostname, '');
        pageLink = pageLink.replace(rootPath, '');
        if(pageLink == curentPathName){
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
            return false;
        }
    });
    $("#tbodyOrder").on("click", "a.link_status", function(){
        var id = $(this).attr('data-id');
        var statusId = $(this).attr('data-status');
        if(statusId != $('input#statusId_' + id).val()) {
            $.ajax({
                type: "POST",
                url: $('input#changeStatusUrl').val(),
                data: {
                    OrderId: id,
                    OrderStatusId: statusId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('td#statusName_' + id).html(json.data);
                        $('input#statusId_' + id).val(statusId);
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });
});