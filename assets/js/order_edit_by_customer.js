$(document).ready(function(){
    $('.match-height').matchHeight();
    if($('input#canEdit').val() == '0'){
        $('select#warehouseId').prop('disabled', true);
        $('.tbodyProduct input, .tbodyProduct textarea').prop('disabled', true);
        $('.tbodyProduct textarea.confirm').prop('disabled', false);
        $('.btn-add-product').hide();
    }

    //global variable========================================
    var noProductImage = $('input#noProductImage').val();
    var shipTQType = parseInt($('input#shipTQType').val());
    var orderStatusId = parseInt($('input#orderStatusId').val());

    if(orderStatusId > 3){
        $('select#careStaffId1') .prop('disabled', true);
        $('#btnUpdateStaff').remove();
        $('.box-staff').removeClass('box-staff');
        $('.tbodyProduct i.link_delete_product').remove();
    }

    if($('i.shipTQReason').length > 0) $('i.shipTQReason').tooltip();

    //update staff=============================================
    $('#btnUpdateStaff').click(function(){
        var careStaffId = $('select#careStaffId1').val();
        if($('input#careStaffId').val() != careStaffId){
            $.ajax({
                type: "POST",
                url: $('input#updateOrderStaffUrl').val(),
                data: {
                    OrderId: $('input#orderId').val(),
                    CareStaffId: careStaffId,
                    OrderUserId: 0,
                    OldCareStaffId: $('input#careStaffId').val(),
                    CustomerId: $('input#customerId').val(),
                    OrderCode: $('#tdOrderCode').text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) $('input#careStaffId').val(careStaffId);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        else showNotification('Vui lòng chọn CSKH khác', 0);
    });

    //save order===============================================
    $('.btnSubmitOrder').click(function(){
        saveOrder(1, noProductImage, shipTQType, false, function(){
            orderStatusId = 1;
        });
    });
    //comfirm order============================================
    $('.btnComfirmOrder').click(function(){
        saveOrder(4, noProductImage, shipTQType, false, function(){
            orderStatusId = 4;
        });
    });

    //delete product normal===================================
    deleteProduct(orderStatusId, shipTQType);

    //comfirm product===================================
    $('.tbodyProduct').on('click', '.btn-confirm-product', function () {
        if (confirm('Bạn đã chắc chắn ?')) {
            var tr = $(this).parent().parent().parent();
            var productId = parseInt(tr.attr('data-product-id'));
            var productStatusId = parseInt(tr.attr('data-status'));
            if (productId > 0 && (productStatusId == 3 || productStatusId == 4)) {
                var id = $(this).attr('data-id');
                var statusId = 0;
                if (productStatusId == 3) {
                    if (id == '2') statusId = 5;
                    else statusId = 7;
                }
                else {
                    if (id == '2') statusId = 6;
                    else statusId = 8;
                }
                var removeReason = tr.find('i.reason').attr('data-original-title');
                var removeConfirm = tr.find('textarea.confirm').val().trim();
                var color = tr.find('input.productColor').val().trim();
                var size = tr.find('input.productSize').val().trim();
                var quantityOld = replaceCost(tr.find('input.soluong').val().trim());
                var quantityNew = replaceCost(tr.find('input.soluong1').val().trim());
                var costOld = replaceCost1(tr.find('input.gia').val().trim());
                var costNew = replaceCost1(tr.find('.tdCost2 input.cost4').val().trim());
                var shipTQ = 0;
                var shopCode = tr.attr('data-shop');
                var tbodyProductId = '#tbodyProduct_' + shopCode;
                var data_product = tr.attr('data-product');
                var trFirst = $(tbodyProductId + ' tr[data-product="' + data_product + '"]').first();
                var productLink = trFirst.find('.tdProductName .linkproductName').text();
                if ($(tbodyProductId + ' tr[data-status="2"]').length == 1) shipTQ = replaceCost1($(tbodyProductId).find('.shipTQ').val().trim());
                $.ajax({
                    type: "POST",
                    url: $('input#changeStatusProductUrl').val(),
                    data: {
                        UserId: $('input#careStaffId').val(),
                        RoleId: 1,
                        CustomerId: $('input#customerId').val(),
                        OrderId: $('input#orderId').val(),
                        OrderCode: $('#tdOrderCode').text(),
                        ProductId: productId,
                        ProductStatusId: statusId,
                        RemoveReason: removeReason,
                        RemoveConfirm: removeConfirm,
                        ProductLink: productLink,
                        Color: color,
                        Size: size,
                        Quantity: quantityOld,
                        QuantityNew: quantityNew,
                        Cost: costOld,
                        CostNew: costNew,
                        ShipTQ: shipTQ,
                        ShopCode: shopCode,
                        ExchangeRate: replaceCost($('input#exchangeRate').val()),
                        ServicePercent: parseInt($('#servicePercent').val())
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) {
                            tr.attr('data-status', statusId);
                            for (var i = 1; i < 9; i++) tr.removeClass('tr_product_status_' + i);
                            tr.addClass('tr_product_status_' + statusId);
                            tr.find('.span-btn').remove();
                            tr.find('textarea.confirm').prop('disabled', true);
                            var html = '';
                            if (statusId == 5 || statusId == 6) {
                                html = '<span class="label1">Giá đã ' + ((statusId == 5) ? 'tăng' : 'giảm') + '</span><span class="label2">Giá cũ</span>';
                                html += '<input type="text" class="form-control cost1 gia cost4 hmdrequired hmdrequiredCost1" disabled data-field="Giá sản phẩm" value="' + costNew + '">';
                                html += '<input type="text" class="form-control cost4" disabled value="' + costOld + '">';
                                tr.find('.tdCost2').html(html);
                                html = '<span class="label1">SL mới</span><span class="label2">SL cũ</span>';
                                html += '<input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" disabled data-field="Số lượng sản phẩm" value="' + quantityNew + '">';
                                html += '<input type="text" class="form-control cost soluong1" data-field="Số lượng sản phẩm" disabled value="' + quantityOld + '">';
                                tr.find('.tdQuantity2').html(html);
                                tr.find('input.thanhtien').val(quantityNew * costNew);
                            }
                            else if (statusId == 7 || statusId == 8) {
                                html = '<span class="label1">Giá không được duyệt</span><span class="label2">Giá cũ</span>';
                                html += '<input type="text" class="form-control cost4" disabled value="' + costNew + '">';
                                html += '<input type="text" class="form-control cost1 gia cost4 hmdrequired hmdrequiredCost1" disabled data-field="Giá sản phẩm" value="' + costOld + '">';
                                tr.find('.tdCost2').html(html);
                                html = '<span class="label1">SL mới</span><span class="label2">SL cũ</span>';
                                html += '<input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" disabled data-field="Số lượng sản phẩm" value="0">';
                                html += '<input type="text" class="form-control cost soluong1" disabled data-field="Số lượng sản phẩm" value="' + quantityOld + '">';
                                tr.find('.tdQuantity2').html(html);
                                tr.find('input.thanhtien').val('0');
                                if(shipTQ > 0){
                                    var rowspan = $('#tbodyProduct_' + shopCode + ' .tr_product').length;
                                    var height = rowspan * 105 / 2;
                                    html = '<span class="label1">Giá mới</span><span class="label2" style="top: ' + height + 'px">Giá cũ</span>';
                                    html += '<input type="text" value="0" class="form-control cost1 shipTQ" disabled style="height: ' + height + 'px;">';
                                    html += '<input type="text" value="' + formatDecimal(shipTQ.toString()) + '" class="form-control cost1 shipTQ1" disabled style="height: ' + height + 'px;">';
                                    tr.find('.tdShipTQ2').removeClass('tdShipTQ').removeClass('tdShipTQ2').addClass('tdShipTQ3').html(html);
                                    $(tbodyProductId + ' .td_sum_ship').text('0');
                                }
                            }
                            setTotalPrice(replaceCost($('input#exchangeRate').val()), getServiceFees(), shipTQType);
                            setProductSum($(tbodyProductId));
                        }
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
    });
    //comfirm shop======================================
    $('.timeline-header').on('click', '.btn-confirm-shop', function(){
        if (confirm('Bạn đã chắc chắn ?')) {
            var shopCode = $(this).attr('data-shop');
            var shopStatusId = parseInt($('input#shopStatusId_' + shopCode).val());
            var id = $(this).attr('data-id');
            var statusId = 0;
            if (shopStatusId == 3) {
                if (id == '2') statusId = 5;
                else statusId = 7;
            }
            else {
                if (id == '2') statusId = 6;
                else statusId = 8;
            }
            var tbodyProductId = '#tbodyProduct_' + shopCode;
            var shipTQOld = replaceCost1($(tbodyProductId + ' input.shipTQ').val());
            var shipTQNew = replaceCost1($(tbodyProductId + ' input.shipTQ1').val());
            var exchangeRate = replaceCost($('input#exchangeRate').val());
            var differenceShipTQ = 0;
            if (statusId == 5 || statusId == 6) {
                differenceShipTQ = shipTQNew - shipTQOld;
                if(differenceShipTQ < 0) differenceShipTQ = -differenceShipTQ;
            }
            else if (statusId == 7 || statusId == 8) {
                differenceShipTQ += shipTQOld;
                $(tbodyProductId + ' .tr_product').each(function () {
                    differenceShipTQ += replaceCost1($(this).find('input.thanhtien').val());
                });
            }
            var differenceShipVN = Math.ceil(differenceShipTQ * exchangeRate);
            differenceShipVN += Math.ceil(differenceShipVN * parseInt($('#servicePercent').val()) / 100);
            $.ajax({
                type: "POST",
                url: $('input#changeStatusShopUrl').val(),
                data: {
                    UserId: $('input#careStaffId').val(),
                    RoleId: 1,
                    CustomerId: $('input#customerId').val(),
                    OrderId: $('input#orderId').val(),
                    OrderCode: $('#tdOrderCode').text(),
                    ShopCode: shopCode,
                    ShopStatusId: statusId,
                    ShipTQ: shipTQOld,
                    ShipTQNew: shipTQNew,
                    PaidTQ: differenceShipTQ,
                    ExchangeRate: exchangeRate,
                    PaidVN: differenceShipVN,
                    ShipTQReason: $('i#shipTQReason_' + shopCode).attr('data-original-title'),
                    ShipTQConfirm: $('input#shipTQConfirm_' + shopCode).val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('input#shopStatusId_' + shopCode).val(statusId);
                        $('input#shipTQConfirm_' + shopCode).prop('disabled', true);
                        $('#spanBtnShop_' + shopCode).remove();
                        var tr = $(tbodyProductId + ' .tr_product').first();
                        var rowspan = $(tbodyProductId + ' .tr_product').length;
                        var height = rowspan * 105 / 2;
                        var html = '';
                        if (statusId == 5 || statusId == 6) {
                            shipTQNew = formatDecimal(shipTQNew.toString());
                            html = '<span class="label1">Giá đã ' + ((statusId == 5) ? 'tăng' : 'giảm') + '</span><span class="label2" style="top: ' + height + 'px">Giá cũ</span>';
                            html += '<input type="text" value="' + shipTQNew + '" class="form-control cost1 shipTQ" disabled style="height: ' + height + 'px;">';
                            html += '<input type="text" value="' + formatDecimal(shipTQOld.toString()) + '" class="form-control cost1 shipTQ1" disabled style="height: ' + height + 'px;">';
                            tr.find('.tdShipTQ3').html(html);
                            $(tbodyProductId + ' .td_sum_ship').text(shipTQNew);
                        }
                        else if (statusId == 7 || statusId == 8) {
                            html += '<span class="label1">Giá mới</span><span class="label2" style="top: ' + height + 'px">Giá cũ</span>';
                            html += '<input type="text" value="0" class="form-control cost1 shipTQ" disabled style="height: ' + height + 'px;">';
                            html += '<input type="text" value="' + formatDecimal(shipTQOld.toString()) + '" class="form-control cost1 shipTQ1" disabled style="height: ' + height + 'px;">';
                            tr.find('.tdShipTQ3').html(html);
                            $(tbodyProductId + ' .td_sum_ship').text('0');
                            var quantity = 0;
                            $(tbodyProductId + ' .tr_product').each(function(){
                                tr = $(this);
                                tr.find('input.thanhtien').val('0');
                                if(tr.find('.tdQuantity').length > 0){
                                    quantity = tr.find('input.soluong').val();
                                    html = '<span class="label1">SL mới</span><span class="label2">SL cũ</span>';
                                    html += '<input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" disabled data-field="Số lượng sản phẩm" value="0">';
                                    html += '<input type="text" class="form-control cost soluong1" data-field="Số lượng sản phẩm" disabled value="' + quantity + '">';
                                    console.log(html);
                                    tr.find('.tdQuantity').addClass('tdQuantity2').removeClass('tdQuantity').html(html);
                                }
                                else{
                                    quantity = tr.find('input.soluong').val();
                                    tr.find('input.soluong').val('0');
                                    tr.find('input.soluong1').val(quantity);
                                }
                            });
                        }
                        setTotalPrice(exchangeRate, getServiceFees(), shipTQType);
                        setProductSum($(tbodyProductId));
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });

    //complaint=========================================
    //add complaint modal=====================================
    $('.tbodyProduct').on('click', 'i.link_complaint', function() {
        var tr = $(this).parent().parent();
        var productId = parseInt(tr.attr('data-product-id'));
        if(productId > 0) {
            var shopCode = tr.attr('data-shop');
            var data_product = tr.attr('data-product');
            var tbodyProductId = '#tbodyProduct_' + shopCode;
            var trFirst = $(tbodyProductId + ' tr[data-product="' + data_product + '"]').first();
            var productLink = trFirst.find('.tdProductName .linkproductName').text();
            $('a#productLinkComplaint').text(productLink);
            $('a#productLinkComplaint').attr('href', productLink);
            $('td#productColorComplaint').text(tr.find('input.productColor').val());
            $('td#productSizeComplaint').text(tr.find('input.productSize').val());
            $('td#productQuantityComplaint').text(tr.find('input.soluong').val());
            $('td#productCostComplaint').text(tr.find('input.gia').val());
            $('input#productIdComplaint').val(productId);
            $('#modalAddComplaint').modal('show');
        }
    });
    //image complaint======================================
    $('#btnUploadImage1Complaint').click(function(){
        var finder = new CKFinder();
        finder.resourceType = 'Products';
        finder.selectActionFunction = function(fileUrl) {
            $('#ulProduct1Complaint').append('<li><a href="' + fileUrl + '" target="_blank"><img src="' + fileUrl + '"></a><i class="fa fa-times"></i></li>');
        };
        finder.popup();
    });
    $('#btnUploadImage2Complaint').click(function(){
        var finder = new CKFinder();
        finder.resourceType = 'Products';
        finder.selectActionFunction = function(fileUrl) {
            $('#ulProduct2Complaint').append('<li><a href="' + fileUrl + '" target="_blank"><img src="' + fileUrl + '"></a><i class="fa fa-times"></i></li>');
        };
        finder.popup();
    });
    $('.ulProductComplaint').on('click', 'li i', function(){
        $(this).parent('li').remove();
    });
    //add complaint ===============================================
    $('#btnAddComplaint').click(function(){
        var complaintTypes = [];
        $('input.cbComplaintType').each(function(){
            if($(this).prop('checked')) complaintTypes.push($(this).val());
        });
        if(complaintTypes.length == 0) showNotification('Vui lòng chọn Nội dung khiếu nại', 0);
        else{
            var productImages = [];
            var waybillCodeImages = [];
            $('#ulProduct1Complaint li img').each(function(){
                productImages.push($(this).attr('src'));
            });
            $('#ulProduct2Complaint li img').each(function(){
                waybillCodeImages.push($(this).attr('src'));
            });
            var productId = $('input#productIdComplaint').val();
            $.ajax({
                type: "POST",
                url: $('input#addComplaintUrl').val(),
                data: {
                    OrderId: $('input#orderId').val(),
                    CustomerId: $('input#customerId').val(),
                    CareStaffId: $('input#careStaffId').val(),
                    ProductId: productId,
                    ComplaintStatusId: 1,
                    ComplaintTypes: JSON.stringify(complaintTypes),
                    ProductImage: JSON.stringify(productImages),
                    WaybillCodeImage: JSON.stringify(waybillCodeImages),
                    Comment: $('#commentComplaint').val().trim(),
                    OrderUserId: $('input#orderUserId').val(),
                    ComplaintOwnerId: 1,
                    ComplaintSolutions: '',
                    ExchangeRate: replaceCost($('input#exchangeRate').val()),
                    ExchangeCostTQ: 0,
                    ReduceCostTQ: 0,
                    OrderCode: $('#tdOrderCode').text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        $('input.cbComplaintType').each(function(){
                            if($(this).prop('checked')) $(this).prop('checked', false);
                        });
                        $('#ulProduct1Complaint').html('');
                        $('#ulProduct2Complaint').html('');
                        $('#commentComplaint').val('');
                        $('#modalAddComplaint').modal('hide');
                        $('.tbodyProduct .tr_product[data-product-id="' + productId + '"] .tdProductName').append('<span class="complaint label label-default" data-id="' + json.data + '" onclick="showCustomerComplaint(' + json.data + ', 4, true);">Khiếu nại (Chưa xử lý)</span>');
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
    // view complaint===================================
    if($('input#complaintId').val() != '0') showCustomerComplaint($('input#complaintId').val());
    $('span.complaint').click(function(){
        showCustomerComplaint($(this).attr('data-id'));
    });
    //update complaint==================================
    $('#btnUpdateComplaint').click(function(){
        var curentMsgComplaint = $('#curentMsgComplaint').val().trim();
        if(curentMsgComplaint == ''){
            showNotification('Bạn chưa phản hồi khiếu nại', 0);
            return false;
        }
        var complaintId = parseInt($('input#complaintId').val());
        if(complaintId > 0) {
            var complaintSolutions = [];
            var complaintStatusId = 6;
            var isAccept = 0;
            $('#divComplaintSolution div.icheckbox_square-blue').each(function(){
                if($(this).hasClass('checked')) complaintSolutions.push($(this).find('input.cbComplaintSolution').val());
            });
            $('#complaintCurentMsg div.iradio_square-blue').each(function () {
                if ($(this).hasClass('checked')) complaintStatusId = parseInt($(this).find('input.cbComplaintAgree').val());
            });
            if(complaintStatusId == 7) isAccept = 1;
            $.ajax({
                type: "POST",
                url: $('input#updateComplaintUrl').val(),
                data: {
                    ComplaintId: complaintId,
                    OrderId: $('input#orderId').val(),
                    ProductId: $('input#productIdComplaint2').val(),
                    CustomerId: $('input#customerId').val(),
                    CareStaffId: $('input#careStaffId').val(),
                    ComplaintStatusId: complaintStatusId,
                    ComplaintOwnerId: 1,
                    ExchangeRate: replaceCost($('input#exchangeRate').val()),
                    ComplaintSolutions: JSON.stringify(complaintSolutions),
                    ExchangeCostTQ: $('input#exchangeCostTQ').val(),
                    ReduceCostTQ: $('input#reduceCostTQ').val(),
                    UserId: $('input#userLoginId').val(),
                    Message: curentMsgComplaint,
                    ComplaintMsgTypeId: 8,
                    IsAccept: isAccept
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('#curentMsgComplaint').val('');
                        var divClass = ($('#listComplaintMsgs div.form-group').length % 2 == 0) ? 'div-left' : 'div-right';
                        var html = '<div class="form-group ' + divClass + '">';
                        html += '<label>Khách hàng phản hồi: </label>';
                        html += '<textarea class="form-control" rows="2" disabled>' + curentMsgComplaint + '</textarea></div>';
                        $('#listComplaintMsgs').append(html);
                        html = 'Khiếu nại (';
                        if(complaintStatusId == 7){
                            $('#complaintCurentMsg').hide();
                            $('#btnUpdateComplaint').hide();
                            html += 'Khách đồng ý)';
                        }
                        else html += 'Khách không đồng ý)';
                        $('span.complaint[data-id="' + complaintId + '"]').text(html);
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
});

function saveOrder(orderStatusId, noProductImage, shipTQType, isInterval, fnSuccess){
    if(validate() && validateCost() && validateCost1()) {
        var listProducts = getListProducts(noProductImage, false, shipTQType);
        $.ajax({
            type: "POST",
            url: $('input#updateOrderUrl').val(),
            data: {
                CustomerId: $('input#customerId').val(),
                FullName: $('input#fullName').val().trim(),
                PhoneNumber: $('input#phoneNumber').val().trim(),
                Email: $('input#email').val().trim(),
                Address: $('input#address').val().trim(),
                CareStaffId: $('input#careStaffId').val(),
                WarehouseId: $('select#warehouseId').val(),
                ExchangeRate: replaceCost($('input#exchangeRate').val()),
                OrderStatusId: orderStatusId,
                ServicePercent: $('#servicePercent').val(),
                IsFixPercent: $('input#isFixPercent').val(),
                OtherCost: replaceCost($('input#costFee').val()),
                OrderUserId: $('input#orderUserId').val(),
                Comment: $('#aOrderComment').attr('data-original-title'),
                ProductJson: JSON.stringify(listProducts),
                IsFromAdmin: 1,
                OrderId: $('input#orderId').val(),
                OrderCode: $('#tdOrderCode').text()
            },
            success: function (response) {
                if(!isInterval) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) {
                        $('.btnSubmitOrder, .btnComfirmOrder').prop('disabled', true);
                        if (orderStatusId == 1) $('#btnCurent').text('Cần check');
                        else if (orderStatusId == 4) $('#btnCurent').text('Chờ duyệt');
                        $('input#orderStatusId').val(orderStatusId);
                        fnSuccess();
                    }
                }
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    }
}

function showCustomerComplaint(id){
    $('input.cbComplaintType2').iCheck('uncheck');
    $('input.cbComplaintSolution').iCheck('uncheck');
    $('#divComplaintSolutionMoney .form-group').hide();
    $('#divComplaintSolutionMoney input').val('0');
    $('#curentMsgComplaint').val('');
    $.ajax({
        type: "POST",
        url: $('input#getComplaintDetailUrl').val(),
        data: {
            ComplaintId: id
        },
        success: function (response) {
            var json = JSON.parse(response);
            if(json.code == 1) {
                $('#customerNameComplaint').text($('input#fullName').val().trim());
                var data = json.data.Product;
                $('input#productIdComplaint2').val(data.ProductId);
                $('a#productLinkComplaint2').attr('href', data.ProductLink);
                $('a#productLinkComplaint2').text(data.ProductLink);
                $('td#productColorComplaint2').text(data.Color);
                $('td#productSizeComplaint2').text(data.Size);
                $('td#productCostComplaint2').text(data.Cost);
                $('td#productQuantityComplaint2').text(data.Quantity);
                data = json.data.Complaint;
                $('input#complaintId').val(data.ComplaintId);
                $('#crDateTimeComplaint').text(data.CrDateTime);
                $('#commentComplaint2').val(data.Comment);
                $('#spanCrDateTimeComplaint').text('Đã gửi lúc ' + data.CrDateTime + ' - ' + $('input#fullName').val().trim());
                var i, html = '', image = '';
                var imagePath = $('input#imagePath').val();
                var complaintTypes = data.ComplaintTypes;
                if(complaintTypes != ''){
                    complaintTypes = JSON.parse(complaintTypes);
                    for(i = 0; i < complaintTypes.length; i++) $('input.cbComplaintType2[value="' + complaintTypes[i] + '"]').iCheck('check');
                }
                var productImages = data.ProductImage;
                if(productImages != ''){
                    html = '';
                    productImages = JSON.parse(productImages);
                    for(i = 0; i < productImages.length; i++){
                        image = imagePath + productImages[i];
                        html += '<li><a href="' + image + '" target="_blank"><img src="' + image + '"></a></li>';
                    }
                    $('#ulProduct1Complaint2').html(html);
                }
                var waybillCodeImages = data.WaybillCodeImage;
                if(waybillCodeImages != ''){
                    html = '';
                    waybillCodeImages = JSON.parse(waybillCodeImages);
                    for(i = 0; i < waybillCodeImages.length; i++){
                        image = imagePath + waybillCodeImages[i];
                        html += '<li><a href="' + image + '" target="_blank"><img src="' + image + '"></a></li>';
                    }
                    $('#ulProduct2Complaint2').html(html);
                }
                var complaintStatusId = data.ComplaintStatusId;
                if(complaintStatusId == 7 || complaintStatusId == 1){
                    $('#complaintCurentMsg').hide();
                    $('#btnUpdateComplaint').hide();
                }
                var complaintSolutions = data.ComplaintSolutions;
                if(complaintSolutions != ''){
                    complaintSolutions = JSON.parse(complaintSolutions);
                    for(i = 0; i < complaintSolutions.length; i++){
                        $('input.cbComplaintSolution[value="' + complaintSolutions[i] + '"]').iCheck('check');
                        if(complaintSolutions[i] == 1){
                            $('input#exchangeCostTQ').val(data.ExchangeCostTQ);
                            $('#complaintSolution1').show();
                        }
                        else if(complaintSolutions[i] == 4){
                            $('input#reduceCostTQ').val(data.ReduceCostTQ);
                            $('#complaintSolution4').show();
                        }
                    }
                }
                var listComplaintMsgs = json.data.ComplaintMsgs;
                if(listComplaintMsgs.length > 0){
                    html = '';
                    var divClass = '', labelUser = '';
                    for(i = 0; i < listComplaintMsgs.length; i++){
                        divClass = (i % 2 == 0) ? 'div-left' : 'div-right';
                        if(listComplaintMsgs[i].ComplaintMsgTypeId == 1 || listComplaintMsgs[i].ComplaintMsgTypeId == 3 || listComplaintMsgs[i].ComplaintMsgTypeId == 5) labelUser = 'Chăm sóc trả lời';
                        else if(listComplaintMsgs[i].ComplaintMsgTypeId == 2 || listComplaintMsgs[i].ComplaintMsgTypeId == 4 || listComplaintMsgs[i].ComplaintMsgTypeId == 6) labelUser = 'Đặt hàng trả lời';
                        else if(listComplaintMsgs[i].ComplaintMsgTypeId == 8) labelUser = 'Khách hàng phản hồi';
                        else labelUser = 'Quản lý duyệt';
                        html += '<div class="form-group ' + divClass + '">';
                        html += '<label>' + labelUser + ': </label>';
                        if(listComplaintMsgs[i].ComplaintMsgTypeId == 7){
                            html += '<div class="complaintAgree">';
                            html += '<label><input type="radio" class="iCheck1" disabled' + ((listComplaintMsgs[i].IsAccept == 1) ? ' checked' : '') + '> Duyệt</label>';
                            html += '<label><input type="radio" class="iCheck1" disabled' + ((listComplaintMsgs[i].IsAccept == 0) ? ' checked' : '') + '> Không Duyệt</label></div>';
                        }
                        else if(listComplaintMsgs[i].ComplaintMsgTypeId == 8){
                            html += '<div class="complaintAgree">';
                            html += '<label><input type="radio" class="iCheck1" disabled' + ((listComplaintMsgs[i].IsAccept == 1) ? ' checked' : '') + '> Đồng ý</label>';
                            html += '<label><input type="radio" class="iCheck1" disabled' + ((listComplaintMsgs[i].IsAccept == 0) ? ' checked' : '') + '> Không Đồng ý</label></div>';
                        }
                        html += '<span class="padding15">Đã gửi lúc ' + listComplaintMsgs[i].CrDateTime + ' - ' + listComplaintMsgs[i].FullName + '</span>';
                        html += '<textarea class="form-control" rows="2" disabled>' + listComplaintMsgs[i].Message + '</textarea></div>';
                    }
                    $('#listComplaintMsgs').html(html);
                    $('input.iCheck1').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%'
                    });
                }
                formatCostPart('cost2');
                $('#modalUpdateComplaint').modal('show');
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}