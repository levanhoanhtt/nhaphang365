$(document).ready(function () {
    CKEDITOR.replace('ArticleContent', {
        language: 'vi',
        height: 500
    });
    if ($('#articleLead').length > 0) {
        CKEDITOR.replace('ArticleLead', {
            language: 'vi',
            toolbar: 'ShortToolbar',
            height: 200
        });
    }
    if ($('.datepicker').length > 0) {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    }
    $('.btnImage').click(function () {
        var finder = new CKFinder();
        finder.resourceType = 'Images';
        finder.selectActionFunction = function (fileUrl) {
            $('input#articleImage').val(fileUrl);
            $('#imgArticle').attr('src', fileUrl);
        };
        finder.popup();
    });
    $("#articleForm").on("blur", "input#articleTitle", function(){
        $('input#articleSlug').val(makeSkug($(this).val()));
    });
    $(document).on('submit', '#articleForm', function () {
        if($('select#categoryId').val() != null) $('input#categoryIds').val($('select#categoryId').val().toString());
        else $('input#categoryIds').val('');
        if (validate()) {
            var articleContent = CKEDITOR.instances['ArticleContent'].getData().trim();
            if (checkEmptyEditor(articleContent)) {
                var form = $('#articleForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if(json.code == 1 && $('input#articleId').val() == '0') redirect(false, $('input#editArticleUrl').val() + '/' + json.data);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
            else showNotification('Nội dung không được bỏ trống', 0);
        }
        return false;
    });
});
