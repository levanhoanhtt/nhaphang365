$(document).ready(function(){
    $('.match-height').matchHeight();
    if($('input#canEdit').val() == '0'){
        $('select#warehouseId').prop('disabled', true);
        $('input#costFee').prop('disabled', true);
        $('input#exchangeRate').prop('disabled', true);
        $('#servicePercent').prop('disabled', true);
        $('.tbodyProduct input, .tbodyProduct textarea').prop('disabled', true);
        $('.btn-add-product').hide();
        $('input#cbIsFixPercent').iCheck('disable');
        $('.timeline-body tr').each(function(){
            $(this).find('th').last().remove();
            $(this).find('td').last().remove();
        });
    }

    //global variable========================================
    var orderStatusId = parseInt($('input#orderStatusId').val());
    var noProductImage = $('input#noProductImage').val();
    var shipTQType = parseInt($('input#shipTQType').val());

    //comfirm order============================================
    $('.btnConfirmOk').click(function(){
        changeOrderStatus(9, function(){
            orderStatusId = 9;
        });
    });
    //not comfirm order============================================
    $('.btnConfirmNotOk').click(function(){
        changeOrderStatus(7, function(){
            orderStatusId = 7;
        });
    });

    //change shop category========================================
    changeShopCategory(orderStatusId);
});

function changeOrderStatus(orderStatusId, fnSuccess){
    var flag = true;
    if(orderStatusId == 8) flag = confirm('Bạn có thực sự muốn hủy đơn hàng ?');
    if(flag){
        $('.btnConfirmOk, .btnConfirmNotOk').prop('disabled', true);
        $.ajax({
            type: "POST",
            url: $('input#changeStatusOrderUrl').val(),
            data: {
                OrderId: $('input#orderId').val(),
                OrderStatusId: orderStatusId,
                CustomerId: $('input#customerId').val(),
                CareStaffId: $('input#careStaffId').val(),
                OrderCode: $('#tdOrderCode').text()
            },
            success: function (response) {
                var json = $.parseJSON(response);
                showNotification(json.message, json.code);
                if (json.code == 1) {
                    $('.btnConfirmOk, .btnConfirmNotOk').prop('disabled', true);
                    var statusName = '';
                    if(orderStatusId == 7) statusName = 'Không được duyệt';
                    else if(orderStatusId == 9) statusName = 'KT đã duyệt';
                    $('#btnCurent').text(statusName);
                    $('input#orderStatusId').val(orderStatusId);
                    fnSuccess();
                }
                else $('.btnConfirmOk, .btnConfirmNotOk').prop('disabled', false);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                $('.btnConfirmOk, .btnConfirmNotOk').prop('disabled', false);
            }
        });
    }
}