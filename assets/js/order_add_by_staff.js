$(document).ready(function () {
    if ($(window).width() < 500) $('div#logo').removeClass('pull-left');
    else $('div#logo').addClass('pull-left');
    $(window).resize(function () {
        if ($(window).width() < 500) $('div#logo').removeClass('pull-left');
        else $('div#logo').addClass('pull-left');
    });
    $('input#crDate').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });

    //global variable========================================
    var noProductImage = $('input#noProductImage').val();
    var shipTQType = parseInt($('input#shipTQType').val());

    //chon KH==============================================
    $('select#customerId').change(function(){
        var customerId = $(this).val();
        $('input#customerId').val(customerId);
        if(customerId != '0'){
            var inputUser = $('input#customer_' + customerId);
            $('input#fullName').val(inputUser.attr('data-name'));
            $('input#phoneNumber').val(inputUser.attr('data-phone'));
            $('input#email').val(inputUser.attr('data-email'));
            $('input#address').val(inputUser.val());
        }
    });

    //save order===============================================
    $('#btnSaveOrder').click(function(){
        saveOrderDraff(noProductImage, replaceCost($('input#exchangeRate').val()), shipTQType, true);
    });
    $('#btnSubmitOrder').click(function(){
        saveOrder(3, noProductImage, shipTQType);
    });
    //comfirm order============================================
    $('#btnComfirmOrder').click(function(){
        saveOrder(4, noProductImage, shipTQType);
    });
    //save draff to exist order================================
    $('#btnMergeOrederDraff').click(function(){
        if(validate() && validateCost() && validateCost1()) {
            $.ajax({
                type: "POST",
                url: $('input#getListOrderUrl').val(),
                data: {
                    CustomerId: $('input#customerId').val(),
                    OrderStatusIds: '1,2,3'
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    if (json.code == 1) {
                        var html = '<option value="0">Vui lòng chọn</option>';
                        var data = json.data;
                        for (var i = 0; i < data.length; i++) {
                            html += '<option value="' + data[i].OrderId + '">' + data[i].OrderCode + '</option>';
                        }
                        $('select#mergeOrderId1').html(html);
                        $('#modalMergeOrderDraff').modal('show');
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
    $('#btnMergeOrder1').click(function(){
        var orderId = parseInt($('select#mergeOrderId1').val());
        if(orderId > 0){
            var listProducts = getListProducts(noProductImage, false, shipTQType);
            if (listProducts.length > 0) {
                $('#btnMergeOrder1').prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: $('input#mergeOrderDraffUrl').val(),
                    data: {
                        OrderId: orderId,
                        ProductJson: JSON.stringify(listProducts),
                        CustomerName: $('input#fullName').val().trim()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) redirect(false, $('input#orderEditUrl').val() + '/' + orderId);
                        else $('#btnMergeOrder1').prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        $('#btnMergeOrder1').prop('disabled', false);
                    }
                });
            }
            else showNotification('Xin vui lòng thêm link sản phẩm để gộp đơn hàng', 0);
        }
    });
    //import order==============================================
    $('#btnImportOrder').click(function(){
        $('#modalImportExcel').modal('show');
        $('input#fileExcelUrl').val('');
    });
    $('#btnUploadExcel').click(function(){
        var finder = new CKFinder();
        finder.resourceType = 'Files';
        finder.selectActionFunction = function(fileUrl) {
            $('input#fileExcelUrl').val(fileUrl);
        };
        finder.popup();
    });
    $('#btnImportExcel').click(function(){
        var fileExcelUrl = $('input#fileExcelUrl').val();
        if(fileExcelUrl != ''){
            $('#btnImportExcel').prop('disabled', true);
            $('.imgLoading').show();
            $.ajax({
                type: "POST",
                url: $('input#importOrderUrl').val(),
                data: {
                    FileUrl: fileExcelUrl
                },
                success: function (response) {
                    $('.imgLoading').hide();
                    $('#modalImportExcel').modal('hide');
                    $('#btnImportExcel').prop('disabled', false);
                    var json = $.parseJSON(response);
                    if(json.code == 1){
                        var data = json.data;
                        $('input#customerId').val('0');
                        $('input#fullName').val(data.FullName);
                        $('input#phoneNumber').val(data.PhoneNumber);
                        $('input#email').val(data.Email);
                        $('input#address').val(data.Address);
                        var exchangeRate = replaceCost(data.ExchangeRate);
                        $('input#exchangeRate').val(exchangeRate);
                        bindProducts(data.Products, data.ShipTQs, exchangeRate, getServiceFees(), noProductImage);
                    }
                    else showNotification(json.message, json.code);
                },
                error: function (response) {
                    $('.imgLoading').hide();
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('#btnImportExcel').prop('disabled', false);
                }
            });
        }
        else showNotification('Vui lòng chọn file Excel đơn hàng', 0);
    });
});

function saveOrder(orderStatusId, noProductImage, shipTQType){
    if(validate() && validateCost() && validateCost1()) {
        var listProducts = getListProducts(noProductImage, false, shipTQType);
        if (listProducts.length > 0) {
            $('#btnSubmitOrder, #btnComfirmOrder').prop('disabled', true);
            $.ajax({
                type: "POST",
                url: $('input#updateOrderUrl').val(),
                data: {
                    CustomerId: $('input#customerId').val(),
                    FullName: $('input#fullName').val().trim(),
                    PhoneNumber: $('input#phoneNumber').val().trim(),
                    Email: $('input#email').val().trim(),
                    Address: $('input#address').val().trim(),
                    CareStaffId: $('select#careStaffId').val(),
                    WarehouseId: $('select#warehouseId').val(),
                    ExchangeRate: replaceCost($('input#exchangeRate').val()),
                    OrderStatusId: orderStatusId,
                    ServicePercent: $('#servicePercent').val(),
                    IsFixPercent: $('input#isFixPercent').val(),
                    OtherCost: replaceCost($('input#costFee').val()),
                    Comment: $('textarea#comment').val().trim(),
                    OrderUserId: 0,
                    ProductJson: JSON.stringify(listProducts),
                    IsFromAdmin: 1,
                    OrderId: 0,
                    CrDate: $('input#crDate').val().trim(),
                    CrTime: $('input#crTime').val().trim()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1) redirect(false, $('input#orderEditUrl').val() + '/' + json.data);
                    else $('#btnSubmitOrder, #btnComfirmOrder').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('#btnSubmitOrder, #btnComfirmOrder').prop('disabled', false);
                }
            });
        }
        else showNotification('Xin vui lòng thêm link sản phẩm để gửi đơn hàng', 0);
    }
}

function bindProducts(listProducts, listShipTQs, exchangeRate, serviceFees, noProductImage){
    $('ul.timeline').html('');
    $('.box-order-shop').hide();
    var html = '';
    var htmlShop = '';
    var shopCount = 0;
    var cateHtml = $('select#categoryIdHidden').html();
    var shopCode = '';
    var products = [];
    var row = 0, rowImage = 0, rowSpan = 0;
    var productLink = '', productLinkOld = '';
    for (var shopId in listProducts){
        shopCount++;
        shopCode = 'HV_' + shopId;
        htmlShop += '<li title="' + shopCode + '" data-id="' + shopCode + '">' + shopId + '</li>';
        html += '<li id="li_' + shopCode + '">';
        html += '<i class="fa fa-shopping-basket"></i>';
        html += '<div class="timeline-item"><div class="timeline-header"><ul class="list-inline">';
        html += '<li><span class="text-bold">Mã shop: </span><a href="javascript:void(0)" id="shopUrl_' + shopCode + '" target="_blank">' + shopCode + '</a></li>';
        html += '<li><span class="text-bold">Tên shop: </span><a href="javascript:void(0)" target="_blank"><span id="shopName_' + shopCode + '">' + shopCode + '</span></a></li>';
        html += '<li><select class="form-control" name="CategoryId_' + shopCode + '" id="categoryId_' + shopCode + '">' + cateHtml + '</select></li></ul></div>';
        html += '<div class="timeline-body"><div class="no-padding divTable">';//table-responsive
        html += '<table class="table table-hover"><thead><tr><th style="width: 82px;">Ảnh</th><th style="width: 250px;">Link sản phẩm</th><th>Màu sắc</th><th>Kích thước</th><th style="width: 85px;">Số<br/>lượng</th><th style="width: 100px;">Giá (Tệ)</th><th style="width: 120px;">Tổng (Tệ)</th><th style="width: 50px;">Ship TQ</th><th style="width: 200px;">Ghi chú</th><th style="padding-right: 15px;"></th></tr></thead>';
        html += '<tbody class="tbodyProduct" id="tbodyProduct_' + shopCode + '">';
        products = listProducts[shopId];
        row = 0;
        rowImage = 0;
        productLink = '';
        productLinkOld = '';
        rowSpan = products.length;
        for(var i = 0; i < rowSpan; i++){
            rowImage++;
            productLink = products[i].ProductLink;
            if(productLink != productLinkOld) row++;
            productLinkOld = productLink;
            html += '<tr data-product="' + row + '" data-shop="' + shopCode + '" data-product-id="0" data-status="2" class="tr_product tr_product_status_2">';
            html += '<td><img class="productImage" id="productImage_' + rowImage + '_' + shopCode + '" src="' + products[i].ProductImage + '" data-id="' + rowImage + '" data-shop="' + shopCode + '"></td>';
            html += '<td class="tdProductName" rowspan="1"><a target="_blank" href="' + productLink + '" class="linkproductName">' + productLink + '</a><textarea rows="3" class="form-control productName"></textarea></td>';
            html += '<td class="tdAttr"><input type="text" class="form-control productColor" value="' + products[i].Color + '"><i class="fa fa-plus link_add_product"></i></td>';
            html += '<td class="tdAttr"><input type="text" class="form-control productSize" value="' + products[i].Size + '"><i class="fa fa-plus link_add_product"></i></td>';
            html += '<td class="tdQuantity"><input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="' + products[i].Quantity + '"></td>';
            html += '<td class="tdCost"><input type="text" class="form-control cost1 gia hmdrequired hmdrequiredCost1" data-field="Giá sản phẩm" value="' + products[i].Cost + '" ></td>';
            html += '<td><input type="text" class="form-control cost1 thanhtien" value="' + (products[i].Quantity * products[i].Cost) + '" disabled></td>';
            if(rowImage == 1) html += '<td class="tdShipTQ" rowspan="' + rowSpan + '"><input type="text" value="' + listShipTQs[shopId] + '" class="form-control cost1 shipTQ" style="height: ' + (rowSpan * 82) + 'px;"></td>';
            html += '<td><textarea rows="3" class="form-control comment">' + products[i].Comment + '</textarea></td>';
            html += '<td><i class="fa fa-trash-o link_delete"></i></td>';
            html += '</tr>';
        }
        html += '</tbody></table>';
        html += '<p><button class="btn btn-info btn-add-product" data-id="' + shopCode + '">Thêm sản phẩm</button></p>';
        html += '</div></div></div></li>';
    }
    if(html != '') {
        $('ul.timeline').html(html);
        mergeByLink(1);
        setTotalPrice(exchangeRate, serviceFees, 1);
        commonAction(exchangeRate, serviceFees, 1, noProductImage, 82);
        $('.box-order-shop').html('<ul class="list-group">' + htmlShop + '</ul>');
        $('.box-order-shop').css('height', (22 * shopCount) + 'px');
        $('.box-order-shop').show();
        setToolTip();
    }
}