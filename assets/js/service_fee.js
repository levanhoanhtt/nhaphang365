$(document).ready(function(){
    $("#tbodyServiceFee").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#serviceFeeId').val(id);
        $('input#beginCost').val($('td#beginCost_' + id).text());
        $('input#endCost').val($('td#endCost_' + id).text());
        $('input#percent').val($('td#percent_' + id).text());
        scrollTo('input#beginCost');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#serviceFeeForm').trigger("reset");
        return false;
    });
    $("#tbodyServiceFee").on("click", "a.link_delete", function(){
        if($('input#deleteServiceFee').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deleteServiceFeeUrl').val(),
                    data: {
                        ServiceFeeId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) $('tr#serviceFee_' + id).remove();
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa phí dịch vụ', 0);
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#updateServiceFee').val() == '1') {
            if (validate()) {
                var form = $('#serviceFeeForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            form.trigger("reset");
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr id="serviceFee_' + data.ServiceFeeId + '">';
                                html += '<td id="beginCost_' + data.ServiceFeeId + '">' + data.BeginCost + '</td>';
                                html += '<td id="endCost_' + data.ServiceFeeId + '">' + data.EndCost + '</td>';
                                html += '<td id="percent_' + data.ServiceFeeId + '">' + data.Percent + '</td>';
                                html += '<td class="actions">' +
                                    '<a href="javascript:void(0)" class="link_edit" data-id="' + data.ServiceFeeId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                    '<a href="javascript:void(0)" class="link_delete" data-id="' + data.ServiceFeeId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                    '</td>';
                                html += '</tr>';
                                $('#tbodyServiceFee').prepend(html);
                            }
                            else{
                                $('td#beginCost_' + data.ServiceFeeId).text(data.BeginCost);
                                $('td#endCost_' + data.ServiceFeeId).text(data.EndCost);
                                $('td#percent_' + data.ServiceFeeId).text(data.Percent);
                            }
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật phí dịch vụ', 0);
        return false;
    });
});
