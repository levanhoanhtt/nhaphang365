$(document).ready(function() {
    $('select#transactionRangeId option[value="2"]').hide();
    if($('input#transactionTypeId').val() == '1') $('select#transactionRangeId option[value="4"]').hide();
    else $('select#transactionRangeId option[value="3"]').hide();
    if($('input.cbStatusId').length > 0){
        $('input.cbStatusId').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
        if($('input#roleLoginId').val() == '3' || $('input#roleLoginId').val() == '6'){
            if($('input#statusId').val() == '2') $('input.cbStatusId').iCheck('disable');
            else {
                $('input.cbStatusId').on('ifChecked', function (e) {
                    $('input#statusId').val(e.currentTarget.value);
                });
            }
        }
        else{
            $('input.cbStatusId').iCheck('disable');
            $('select#warehouseId, select#moneySourceId, input#paidCost, input#exchangeRate, input#paidVN, #comment, #comfirm').prop('disabled', true);
        }
    }
    formatCost();
    $('.aCurrency').click(function(){
        $('span#curentCurrency').text($(this).text());
        $('#divCurrency').removeClass('open');
        var id = $(this).attr('data-id');
        $('input#currencyId').val(id);
        if(id == '1'){
            $('input#exchangeRate').val('1');
            $('input#paidTQ').val('0');
            $('input#paidVN').val($('input#paidCost').val());
            $('.divTQ').hide();
        }
        else{
            var paidCost = $('input#paidCost').val();
            var exchangeRate = $('input#exchangeRateTQ').val();
            $('input#paidTQ').val(paidCost);
            $('input#exchangeRate').val(exchangeRate);
            $('input#paidVN').val(Math.ceil(replaceCost1(paidCost) * replaceCost(exchangeRate)));
            formatPaidVN();
            $('.divTQ').show();
        }
        return false;
    });
    $('#transactionForm').on('keyup', 'input#paidCost', function(){
        formatCostPart('cost1');
        if($('input#currencyId').val() == '1'){
            $('input#paidTQ').val('0');
            $('input#paidVN').val($(this).val());
        }
        else{
            $('input#paidTQ').val($(this).val());
            $('input#paidVN').val(Math.ceil(replaceCost1($(this).val()) * replaceCost($('input#exchangeRate').val())));
        }
        formatPaidVN();
    });
    $('#transactionForm').on('keyup', 'input#exchangeRate', function(){
        $('input#exchangeRateTQ').val($(this).val());
        $('input#paidVN').val(Math.ceil(replaceCost1($('input#paidCost').val()) * replaceCost($(this).val())));
        formatPaidVN();
    });

    if($('select#customerId').val() != '0') getListOrder(parseInt($('select#customerId').val()), $('select#orderId').val());
    $('select#customerId').change(function(){
        getListOrder(parseInt($(this).val()), 0);
    });

    if($('input#transactionTypeId').val() == '2'){
        $('select#orderId').change(function(){
            if($(this).val() != '0'){
                $.ajax({
                    type: "POST",
                    url: $('input#getListComplaintByCustomerUrl').val(),
                    data: {
                        OrderId: $(this).val(),
                        CustomerId: $('select#customerId').val()
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1) {
                            var html = '<option value="0">Chưa rõ</option>';
                            var data = json.data;
                            for (var i = 0; i < data.length; i++) html += '<option value="' + data[i].ComplaintId + '">Khiếu nại #' + data[i].ComplaintId + '</option>';
                            $('select#complaintId').html(html);
                        }
                    },
                    error: function (response) {
                        $('select#complaintId').html('<option value="0">Chưa rõ</option>');
                    }
                });
            }
            else $('select#complaintId').html('<option value="0">Chưa rõ</option>');
        });
    }

    $('input#submit').click(function () {
    //$(document).on('submit','#transactionForm',function () {
        if (validateCost1()) {
            $('input#submit').prop('disabled', true);
            var transactionId = parseInt($('input#transactionId').val());
            var statusId = parseInt($('input#statusId').val());
            if(statusId == 2 && $('input#roleLoginId').val() == '6') statusId = 4;
            var form = $('#transactionForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: {
                    CustomerId: $('select#customerId').val(),
                    OrderId: $('select#orderId').val(),
                    ComplaintId: $('select#complaintId').val(),
                    StatusId: statusId,
                    TransactionTypeId: $('input#transactionTypeId').val(),
                    TransactionRangeId: $('select#transactionRangeId').val(),
                    WarehouseId: $('select#warehouseId').val(),
                    MoneySourceId: $('select#moneySourceId').val(),
                    Comment: $('#comment').val().trim(),
                    Confirm: $('#comfirm').val().trim(),
                    PaidTQ: replaceCost1($('input#paidTQ').val()),
                    ExchangeRate: replaceCost($('input#exchangeRate').val()),
                    PaidVN: replaceCost($('input#paidVN').val()),
                    TransactionId: transactionId
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1){
                        if(transactionId > 0) redirect(false, $('input#transactionEditUrl').val() + '/' + json.data);
                        else{
                            if($('select#customerId').val() != '0'){
                                if($('input#transactionTypeId').val() == 1) redirect(false, $('input#transactionInvoiceUrl').val());
                                else redirect(false, $('input#transactionExpenseUrl').val());
                            }
                            else redirect(false, $('input#transactionNoNameUrl').val());
                        }
                    }
                    else $('input#submit').prop('disabled', false);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    $('input#submit').prop('disabled', false);
                }
            });
        }
        return false;
    });
});

function getListOrder(customerId, orderId){
    if(customerId > 0){
        $.ajax({
            type: "POST",
            url: $('input#getListOrderUrl').val(),
            data: {
                CustomerId: customerId
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if(json.code == 1) {
                    var html = '<option value="0">Chưa rõ</option>';
                    var data = json.data;
                    for (var i = 0; i < data.length; i++) html += '<option value="' + data[i].OrderId + '">' + data[i].OrderCode + '</option>';
                    $('select#orderId').html(html);
                    $('select#orderId').val(orderId);
                }
            },
            error: function (response) {
                $('select#orderId').html('<option value="0">Chưa rõ</option>');
            }
        });
    }
    else $('select#orderId').html('<option value="0">Chưa rõ</option>');
}

function formatPaidVN(){
    $('input#paidVN').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: ','
    });
}