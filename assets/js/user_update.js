$(document).ready(function(){
    $('input.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    if($('select#roleId').length > 0) $('select#roleId option[value="4"]').hide();
    if($('input#phoneNumber1').length > 0) $('div.iradio_square-blue').first().attr('aria-checked', 'true');
    $('.chooseImage').click(function(){
        var finder = new CKFinder();
        finder.resourceType = 'Users';
        finder.selectActionFunction = function(fileUrl) {
            $('input#avatar').val(fileUrl);
            $('img#imgAvatar').attr('src', fileUrl);
        };
        finder.popup();
    });
    $('a#generatorPass').click(function(){
        var pass = randomIntFromInterval(100000000, 999999999);
        $('input#newPass').val(pass);
        $('input#rePass').val(pass);
        return false;
    });
    $(document).on('submit','#userForm',function (){
        if(validate()) {
            if ($('input#newPass').val() != $('input#rePass').val()) {
                showNotification('Mật khẩu không trùng', 0);
                return false;
            }
            var flag = true;
            var phone = '';
            if($('input#phoneNumber1').length > 0){
                var phones = '';
                $('div.iradio_square-blue').each(function(){
                    var id = $(this).find('input.iCheck').attr('data-id');
                    phone = $('input#phoneNumber' + id).val();
                    if($(this).attr('aria-checked') == 'true'){
                        if(phone == ''){
                            $('input#phoneNumber').val('');
                            showNotification('Di động chính không được bỏ trống', 0);
                            flag = false;
                            return false;
                        }
                        else $('input#phoneNumber').val(phone);
                    }
                    else if(phone != '') phones += phone + '|';
                });
                $('input#phoneNumbers').val(phones);
            }
            if(!flag) return false;
            if($('#roleId').val() == '4') {
                phone = $('input#phoneNumber').val();
                if (phone.indexOf(' ') >= 0) {
                    showNotification('Số điện thoại không được có khoảng trằng', 0);
                    return false;
                }
                if (phone.length == 10 || phone.length == 11) {
                    var filter = /^[0-9-+]+$/;
                    if (!filter.test(phone)) {
                        showNotification('Số điện thoại không hợp lệ', 0);
                        return false;
                    }
                }
                else {
                    showNotification('Số điện thoại chỉ gồm 10 hoặc 11 số', 0);
                    return false;
                }
            }
            if($('input#userName').length > 0 && $('input#userName').val().trim().indexOf(' ') >= 0){
                showNotification('Tên đăng nhập không được có khoảng trằng', 0);
                $('input#userName').focus();
                return false;
            }
            var form = $('#userForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1 && $('input#userId').val() == '0') redirect(false, $('input#userEditUrl').val() + '/' + json.data);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});

function randomIntFromInterval(min,max){
    return Math.floor(Math.random()*(max-min+1)+min);
}