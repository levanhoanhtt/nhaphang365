$(document).ready(function(){
    $("#tbodyTransportUser").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#transportUserId').val(id);
        $('input#transportUserName').val($('td#transportUserName_' + id).text());
        scrollTo('input#transportUserName');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#transportUserForm').trigger("reset");
        return false;
    });
    $("#tbodyTransportUser").on("click", "a.link_delete", function(){
        if($('input#deleteTransportUser').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deleteTransportUserUrl').val(),
                    data: {
                        TransportUserId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) $('tr#transportUser_' + id).remove();
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa Nhân viên giao hàng', 0);
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#updateTransportUser').val() == '1') {
            if (validate()) {
                var form = $('#transportUserForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            form.trigger("reset");
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr id="transportUser_' + data.TransportUserId + '">';
                                html += '<td id="transportUserName_' + data.TransportUserId + '">' + data.TransportUserName + '</td>';
                                html += '<td class="actions">' +
                                    '<a href="javascript:void(0)" class="link_edit" data-id="' + data.TransportUserId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                    '<a href="javascript:void(0)" class="link_delete" data-id="' + data.TransportUserId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                    '</td>';
                                html += '</tr>';
                                $('#tbodyTransportUser').prepend(html);
                            }
                            else $('td#transportUserName_' + data.TransportUserId).text(data.TransportUserName);
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật Nhân viên giao hàng', 0);
        return false;
    });
});