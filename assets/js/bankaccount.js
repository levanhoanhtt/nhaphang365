$(document).ready(function(){
    $("#tbodyBankAccount").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#bankAccountId').val(id);
        $('input#bankAccountName').val($('td#bankAccountName_' + id).text());
        scrollTo('input#bankAccountName');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#bankAccountForm').trigger("reset");
        return false;
    });
    $("#tbodyBankAccount").on("click", "a.link_delete", function(){
        if($('input#deleteBankAccount').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deleteBankAccountUrl').val(),
                    data: {
                        BankAccountId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) $('tr#bankAccount_' + id).remove();
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa Tài khoản đặt hàng', 0);
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#updateBankAccount').val() == '1') {
            if (validate()) {
                var form = $('#bankAccountForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            form.trigger("reset");
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr id="bankAccount_' + data.BankAccountId + '">';
                                html += '<td id="bankAccountName_' + data.BankAccountId + '">' + data.BankAccountName + '</td>';
                                html += '<td class="actions">' +
                                    '<a href="javascript:void(0)" class="link_edit" data-id="' + data.BankAccountId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                    '<a href="javascript:void(0)" class="link_delete" data-id="' + data.BankAccountId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                    '</td>';
                                html += '</tr>';
                                $('#tbodyBankAccount').prepend(html);
                            }
                            else $('td#bankAccountName_' + data.BankAccountId).text(data.BankAccountName);
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật Tài khoản đặt hàng', 0);
        return false;
    });
});
