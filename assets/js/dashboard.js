$(document).ready(function() {
    $('.match-height').matchHeight();
    var roleLoginId = parseInt($('input#roleLoginId').val());
    //statistic order
    $.ajax({
        type: "POST",
        url: $('input#statisticOrderUrl').val(),
        data: {
            OrderStatusIds: '1,4',
            IsStatisticAll: 0
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1){
                var data = json.data;
                if(roleLoginId == 1){
                    $('span#countOrder_1').text(data.Count_1);
                    $('span#countOrder_4').text(data.Count_4);
                }
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
    //statistic complaint
    $.ajax({
        type: "POST",
        url: $('input#statisticComplaintUrl').val(),
        data: {
            ComplaintStatusIds: '1,4',
            IsStatisticAll: 0
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1){
                var data = json.data;
                if(roleLoginId == 1){
                    $('span#countComplaint_1').text(data.Count_1);
                    $('span#countComplaint_3').text(data.Count_3);
                }
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
    //statistic notification
    $.ajax({
        type: "POST",
        url: $('input#statisticNotificationUrl').val(),
        data: {
            IsStatisticAll: 0
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if(json.code == 1){
                var data = json.data;
                if(roleLoginId == 1) $('span#countNotification_1').text(data.Count_1);
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
    //revenue staff
    //if(roleLoginId == 1) {
        var now = new Date();
        var curentMonth = now.getMonth() + 1;
        if (curentMonth < 10) curentMonth = '0' + curentMonth;
        var curentMonthYear = curentMonth + '/' + now.getFullYear();
        $.ajax({
            type: "POST",
            url: $('input#orderRevenueUrl').val(),
            data: {
                StatisticTypeId: 1,
                OrderStatusId: 6,
                OrderBeginDate: '01/' + curentMonthYear,
                OrderEndDate: '31/' + curentMonthYear
            },
            success: function (response) {
                var json = $.parseJSON(response);
                if (json.code == 1) {
                    var data = json.data;
                    var html = '';
                    var userLoginId = parseInt($('input#userLoginId').val());
                    for(var i = 0; i < data.length; i++){
                        if(data[i].StaffId == userLoginId){
                            html += '<div class="col-lg-3 col-xs-6"><div class="box box-primary"><div class="box-header with-border"><h3 class="box-title">' + data[i].FullName + '</h3></div>';
                            html += '<div class="box-body"><span class="count">' + data[i].TotalCost + '</span></div></div></div>';
                        }
                        else{
                            html += '<div class="col-lg-3 col-xs-6"><div class="box box-default"><div class="box-header with-border"><h3 class="box-title">' + data[i].FullName + '</h3></div>';
                            html += '<div class="box-body"><span class="count">' + data[i].TotalCost + '</span></div></div></div>';
                        }
                    }
                    $('div#staffRevenue').html(html);
                }
                else showNotification(json.message, json.code);
            },
            error: function (response) {
                showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
            }
        });
    //}
});