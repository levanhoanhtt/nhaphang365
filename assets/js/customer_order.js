$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    var curentPathName = window.location.pathname;
    var rootPath = $('#roorPath').val();
    curentPathName = curentPathName.replace(rootPath, '');
    var hostname = window.location.hostname;
    $('#ulOrderStatus li a').each(function() {
        $(this).removeClass('btn-primary');
        $(this).addClass('btn-default');
        var pageLink = $(this).attr("href");
        pageLink = pageLink.replace('https://', '');
        pageLink = pageLink.replace('http://', '');
        pageLink = pageLink.replace(hostname, '');
        pageLink = pageLink.replace(rootPath, '');
        if(pageLink == curentPathName){
            $(this).removeClass('btn-default');
            $(this).addClass('btn-primary');
            return false;
        }
    });
    $('table i.fa-clone').click(function(){
        var id = $(this).attr('data-id');
        if(confirm('Bạn chắc chắn muốn tạo đơn hàng mới như đơn hàng ' + $('#orderCode_' + id).text() + ' ?')){
            $.ajax({
                type: "POST",
                url: $('input#cloneOrderUrl').val(),
                data: {
                    OrderId: id
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if(json.code == 1) redirect(false, $('input#listOrderUrl').val());
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
    });
});