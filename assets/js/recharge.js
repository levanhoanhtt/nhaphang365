$(document).ready(function() {
    if($('.datepicker').length > 0) {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    }
    var form = $('#rechargeForm');
    if(form.length > 0){
        form.on('keyup', '.cost', function(){
            var value = $(this).val();
            $(this).val(formatDecimal(value));
        });
        $('input#submit').click(function(){
            if($('select#bankAccountId').val() == '0'){
                showNotification('Vui lòng chọn Tài khoản ngân hàng cần nạp', 0);
                return false;
            }
            var paidTQ = replaceCost1($('input#paidTQ').val());
            var exchangeRate = replaceCost($('input#exchangeRate').val());
            if(paidTQ > 0 && exchangeRate > 0){
                $('input#submit').prop('disabled', true);
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        showNotification(json.message, json.code);
                        if (json.code == 1) redirect(false, $('input#rechargeUrl').val());
                        else $('input#submit').prop('disabled', false);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                        $('input#submit').prop('disabled', false);
                    }
                });
            }
            else showNotification('Số tệ và tỉ giá phải lớn hơn 0', 0);
            return false;
        });
    }
});