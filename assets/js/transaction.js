$(document).ready(function() {
    if($('.datepicker').length > 0) {
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    }
    $("#tbodyTransaction").on("click", "a.link_delete", function(){
        if($('input#changeStatus').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) changeStatus($(this).attr('data-id'), 0);
        }
        else showNotification('Bạn không có quyền xóa ' + $('input#transactionTypeName').val(), 0);
        return false;
    });
    $("#tbodyTransaction").on("click", "a.link_status", function(){
        if($('input#changeStatus').val() == '1'){
            var id = $(this).attr('data-id');
            var statusId = $(this).attr('data-status');
            if(statusId != $('input#statusId_' + id).val()) changeStatus(id, statusId);
        }
        else showNotification('Bạn không có quyền cập nhật trạng thái ' + $('input#transactionTypeName').val(), 0);
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });    
});

function changeStatus(id, statusId){
    $.ajax({
        type: "POST",
        url: $('input#changeStatusUrl').val(),
        data: {
            TransactionId: id,
            StatusId: statusId,
            OrderId: $('input#orderId_' + id).val(),
            CustomerId: $('input#customerId_' + id).val(),
            PaidVN: $('input#paidVN_' + id).val(),
            Balance: 0,
            TransactionTypeId: $('input#transactionTypeId').val(),
            TransactionTypeName: $('input#transactionTypeName').val()
        },
        success: function (response) {
            var json = $.parseJSON(response);
            showNotification(json.message, json.code);
            if (json.code == 1){
                if(statusId == 0) $('tr#transaction_' + id).remove();
                else{
                    if($('input#isChangeStatus').val() == '1') {
                        $('td#statusName_' + id).html(json.data.StatusName);
                        $('input#statusId_' + id).val(statusId);
                        if(statusId == 2) $('tr#transaction_' + id + ' td.actions').html('');
                    }
                    else $('tr#transaction_' + id).remove();
                }
            }
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}