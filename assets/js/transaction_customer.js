$(document).ready(function() {
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
    });
    $('select#customerId').change(function(){
        if($(this).val() != '0') window.location.href = $('input#transactionCustomerUrl').val() + '/' + $(this).val();
    });
    $(document).on('submit','#userForm',function () {
        if (validate()) {
            if ($('input#newPass').val() != $('input#rePass').val()) {
                showNotification('Mật khẩu không trùng', 0);
                return false;
            }
            var form = $('#userForm');
            $.ajax({
                type: "POST",
                url: form.attr('action'),
                data: form.serialize(),
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
            return false;
        }
    });
    $('#btnOrder').click(function(){
        $('#paggingOrder .box-tools').html('');
        $('#paggingOrder').hide();
        getListOrder(1, true);
    });
    $('#tbodyTransaction').on('click', 'i.fa', function(){
        var id = $(this).attr('data-id');
        $('.tr' + id).slideToggle();
        if($(this).hasClass('fa-caret-square-o-right')) $(this).removeClass('fa-caret-square-o-right').addClass('fa-caret-square-o-down');
        else $(this).removeClass('fa-caret-square-o-down').addClass('fa-caret-square-o-right');
    });
    $('i.all').click(function(){
        if($(this).hasClass('fa-caret-square-o-right')){
            $(this).removeClass('fa-caret-square-o-right').addClass('fa-caret-square-o-down');
            $('tr.thead').show();
            $('#tbodyTransaction i.fa').removeClass('fa-caret-square-o-right').addClass('fa-caret-square-o-down');
        }
        else{
            $(this).removeClass('fa-caret-square-o-down').addClass('fa-caret-square-o-right');
            $('tr.thead').hide();
            $('#tbodyTransaction i.fa').removeClass('fa-caret-square-o-down').addClass('fa-caret-square-o-right');
        }
    });
});

function paggingOrder(pageId){
    getListOrder(pageId, false);
}

function getListOrder(pageId, isOpenModal){
    $.ajax({
        type: "POST",
        url: $('input#getListOrderUrl').val(),
        data: {
            CustomerId: $('input#customerId').val(),
            PageId: pageId
        },
        success: function (response) {
            var json = $.parseJSON(response);
            if (json.code == 1) {
                var data = json.data;
                if(data.PaggingHtml != ''){
                    $('#paggingOrder .box-tools').html(data.PaggingHtml);
                    $('#paggingOrder').show();
                }
                var html = '';
                data = data.ListOrders;
                for (var i = 0; i < data.length; i++){
                    html += '<tr><td>' + (i + 1) + '</td>';
                    html += '<td>' + data[i].OrderLinkHtml + '</td>';
                    html += '<td>' + data[i].CrDateTime + '</td>';
                    html += '<td>' + data[i].OrderStatusHtml + '</td>';
                    html += '<td>' + data[i].TotalCost + '</td></tr>';
                }
                $('#tbodyOrder').html(html);
                if(isOpenModal) $('#modalOrder').modal('show');
            }
            else showNotification(json.message, json.code);
        },
        error: function (response) {
            showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
        }
    });
}