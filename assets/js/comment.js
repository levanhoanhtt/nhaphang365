$(document).ready(function() {
    $('select#commentStarId option').each(function(){
        if($(this).val() != '0') $(this).text($(this).val() + ' - ' + $(this).text());
    });
    $("#tbodyComment").on("click", "a.link_delete", function(){
        if($('input#deleteComment').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')){
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#changeStatusUrl').val(),
                    data: {
                        CommentId: id,
                        StatusId: 0
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) $('tr#comment_' + id).remove();
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa Bình luận - Đánh giá', 0);
        return false;
    });
    $("#tbodyComment").on("click", "a.link_status", function(){
        if($('input#changeStatus').val() == '1'){
            var id = $(this).attr('data-id');
            var statusId = $(this).attr('data-status');
            if(statusId != $('input#statusId_' + id).val()) {
                $.ajax({
                    type: "POST",
                    url: $('input#changeStatusUrl').val(),
                    data: {
                        CommentId: id,
                        StatusId: statusId
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1){
                            $('td#statusName_' + id).html(json.data.StatusName);
                            $('input#statusId_' + id).val(statusId);
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật trạng thái Bình luận - Đánh giá', 0);
        $('#btnGroup_' + id).removeClass('open');
        return false;
    });
});