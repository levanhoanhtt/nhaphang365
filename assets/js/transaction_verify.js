$(document).ready(function(){
    $('.tbodyOrderTransaction').on('click', '.link_order', function(){
        var type = parseInt($(this).attr('data-type'));
        var roleId = parseInt($('input#roleLoginId').val());
        var statusId = 0;
        if(roleId == 3 || roleId == 7){
            if(type == 1) statusId = 7;
            else if(type == 2) statusId = 5;
        }
        else if(roleId == 6){
            if(type == 1) statusId = 7;
            else if(type == 2) statusId = 9;
        }
        var orderId = parseInt($(this).attr('data-id'));
        if(orderId > 0 && statusId > 0){
            $.ajax({
                type: "POST",
                url: $('input#changeOrderStatusUrl').val(),
                data: {
                    OrderId: orderId,
                    OrderStatusId: statusId,
                    CustomerId: $('input#customerId1_' + orderId).val(),
                    CareStaffId: $('input#careStaffId_' + orderId).val(),
                    OrderCode: $('#tdOrderCode_' + orderId).text()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('tr#order_' + orderId).find('td.actions').html(json.data);
                        $('#tdOrderStatus_' + orderId).html(json.data);
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
    $('.tbodyOrderTransaction').on('click', '.link_transaction', function(){
        var type = parseInt($(this).attr('data-type'));
        var roleId = parseInt($('input#roleLoginId').val());
        var statusId = 0;
        if(roleId == 3 || roleId == 7){
            if(type == 1) statusId = 3;
            else if(type == 2) statusId = 2;
        }
        else if(roleId == 6){
            if(type == 1) statusId = 3;
            else if(type == 2) statusId = 4;
        }
        var transactionId = parseInt($(this).attr('data-id'));
        if(transactionId > 0 && statusId > 0){
            $.ajax({
                type: "POST",
                url: $('input#changeTransactionStatusUrl').val(),
                data: {
                    TransactionId: transactionId,
                    StatusId: statusId,
                    OrderId: $('input#orderId_' + transactionId).val(),
                    CustomerId: $('input#customerId_' + transactionId).val(),
                    PaidVN: $('input#paidVN_' + transactionId).val(),
                    Balance: 0,
                    TransactionTypeId: $('input#transactionTypeId_' + transactionId).val()
                },
                success: function (response) {
                    var json = $.parseJSON(response);
                    showNotification(json.message, json.code);
                    if (json.code == 1){
                        $('tr#transaction_' + transactionId).find('td.actions').html(json.data.StatusName);
                        $('#tdTransactionStatus_' + transactionId).html(json.data.StatusName);
                    }
                },
                error: function (response) {
                    showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                }
            });
        }
        return false;
    });
});