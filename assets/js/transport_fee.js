$(document).ready(function(){
    $('input#weightCost').priceFormat({
        prefix: '',
        centsLimit: 0,
        thousandsSeparator: ','
    });
    $("#tbodyTransportFee").on("click", "a.link_edit", function(){
        var id = $(this).attr('data-id');
        $('input#transportFeeId').val(id);
        $('select#warehouseId').val($('input#warehouseId_' + id).val());
        $('input#beginWeight').val($('td#beginWeight_' + id).text());
        $('input#endWeight').val($('td#endWeight_' + id).text());
        $('input#weightCost').val($('td#weightCost_' + id).text());
        scrollTo('select#warehouseId');
        return false;
    });
    $('a#link_cancel').click(function(){
        $('#transportFeeForm').trigger("reset");
        return false;
    });
    $("#tbodyTransportFee").on("click", "a.link_delete", function(){
        if($('input#deleteTransportFee').val() == '1') {
            if (confirm('Bạn có thực sự muốn xóa ?')) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "POST",
                    url: $('input#deleteTransportFeeUrl').val(),
                    data: {
                        TransportFeeId: id
                    },
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if (json.code == 1) $('tr#transportFee_' + id).remove();
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền xóa phí vận chuyển', 0);
        return false;
    });
    $('a#link_update').click(function(){
        if($('input#updateTransportFee').val() == '1') {
            if (validate()) {
                var form = $('#transportFeeForm');
                $.ajax({
                    type: "POST",
                    url: form.attr('action'),
                    data: form.serialize(),
                    success: function (response) {
                        var json = $.parseJSON(response);
                        if(json.code == 1){
                            form.trigger("reset");
                            var data = json.data;
                            if(data.IsAdd == 1){
                                var html = '<tr id="transportFee_' + data.TransportFeeId + '">';
                                html += '<td id="warehouseName_' + data.TransportFeeId + '">' + data.WarehouseName + '</td>';
                                html += '<td id="beginWeight_' + data.TransportFeeId + '">' + data.BeginWeight + '</td>';
                                html += '<td id="endWeight_' + data.TransportFeeId + '">' + data.EndWeight + '</td>';
                                html += '<td id="weightCost_' + data.TransportFeeId + '">' + data.WeightCost + '</td>';
                                html += '<td class="actions">' +
                                    '<a href="javascript:void(0)" class="link_edit" data-id="' + data.TransportFeeId + '" title="Sửa"><i class="fa fa-pencil"></i></a>' +
                                    '<a href="javascript:void(0)" class="link_delete" data-id="' + data.TransportFeeId + '" title="Xóa"><i class="fa fa-trash-o"></i></a>' +
                                    '<input type="text" hidden="hidden" id="warehouseId_' + data.TransportFeeId + '" value="' + data.WarehouseName + '">' +
                                    '</td>';
                                html += '</tr>';
                                $('#tbodyTransportFee').prepend(html);
                            }
                            else{
                                $('td#warehouseName_' + data.TransportFeeId).text(data.WarehouseName);
                                $('td#beginWeight_' + data.TransportFeeId).text(data.BeginWeight);
                                $('td#endWeight_' + data.TransportFeeId).text(data.EndWeight);
                                $('td#weightCost_' + data.TransportFeeId).text(data.WeightCost);
                            }
                        }
                        showNotification(json.message, json.code);
                    },
                    error: function (response) {
                        showNotification('Có lỗi xảy ra trong quá trình thực hiện', 0);
                    }
                });
            }
        }
        else showNotification('Bạn không có quyền cập nhật phí vận chuyển', 0);
        return false;
    });
});
