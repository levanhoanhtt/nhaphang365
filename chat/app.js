var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server),
    request = require('request');

server.listen(process.env.PORT || 3000, function(){
    console.log('Chat System NhapHang365 listening on port 3000!');
});

app.get('/', function(req, res){
    res.send('Chat System NhapHang365');
});

var staffOnlines = [];
var customerOnlines = [];
io.sockets.on('connection', function(socket){

    //Chat Start
    socket.on('chat begin', function (data) {
        data.msg = 'Chào anh/chị, Nhập hàng 365 có thể hỗ trợ được gì cho anh/chị ạ ?';
        io.emit('chat begin', data);
    });

    //New Message
    socket.on('new message', function (data) {
        request.post({
                url:'http://nhaphang365.vn/chat/insert',
                form: {
                    CustomerId: data.customerId,
                    StaffId: data.staffId,
                    Message: data.msg,
                    FileUrl: data.fileUrl,
                    IsCustomerSend: data.isCustomerSend
                }
            },
            function(err, httpResponse, body){
                if (err) io.emit('error message', data);
                else{
                    var json = JSON.parse(body);
                    if(json.code == 1){
                        data.crDateTime = json.data.CrDateTime;
                        data.msg = json.data.Message;
                        data.fileUrl = json.data.FileUrl;
                        io.emit('new message', data);
                    }
                    else io.emit('error message', data);
                }
            }
        );
    });

    //user state
    socket.on('user state', function (data) {
        if(data.state == 1) { //online
            if(data.roleId == 1 && staffOnlines.indexOf(data.userId) == -1) staffOnlines.push(data.userId);
            else if(data.roleId == 4 && customerOnlines.indexOf(data.userId) == -1) customerOnlines.push(data.userId);
        }
        else{ //offline
            var index = -1;
            if(data.roleId == 1){
                index = staffOnlines.indexOf(data.userId);
                if(index >= 0) staffOnlines.splice(index, 1);
            }
            else if(data.roleId == 4){
                index = customerOnlines.indexOf(data.userId);
                if(index >= 0) customerOnlines.splice(index, 1);
            }
        }
        io.emit('user state', data);
    });

    socket.on('user online', function (data) {
        if(data.roleId == 1) io.emit('staff online', staffOnlines);
        else if(data.roleId == 4) io.emit('customer online', customerOnlines);
    });
});