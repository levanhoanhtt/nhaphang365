<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtrackingcrawls extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "trackingcrawls";
        $this->_primary_key = "TrackingCrawlId";
    }

    public function getCount($postData){
        $query = "1 = 1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM trackingcrawls WHERE 1 = 1" . $this->buildQuery($postData, true);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $orderBy = false){
        $query = '';
        if(isset($postData['Tracking']) && !empty($postData['Tracking'])) $query.=" AND Tracking LIKE '%{$postData['Tracking']}'";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND DateVN >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND DateVN <= '{$postData['EndDate']}'";
        if($orderBy) $query .= " ORDER BY DateTQ DESC";
        return $query;
    }
}