<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransporttypes extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transporttypes";
        $this->_primary_key = "TransportTypeId";
    }

    public function getHierachy(){
        $retVal = array();
        $listTransportTypes = $this->getBy(array('StatusId' => STATUS_ACTIVED));
        foreach($listTransportTypes as $tt1){
            if($tt1['ParentTransportTypeId'] == 0){
                $retVal[] = $tt1;
                foreach($listTransportTypes as $tt2){
                    if($tt2['ParentTransportTypeId'] == $tt1['TransportTypeId']) $retVal[] = $tt2;
                }
            }
        }
        return $retVal;
    }

    public function checkHasChild($transportTypeId){
        $transportTypes = $this->getBy(array('ParentTransportTypeId' => $transportTypeId, 'StatusId' => STATUS_ACTIVED), false, 'TransportTypeId');
        return empty($transportTypes);
    }

    public function getSelectHtml($listTransportTypes, $transportTypeId = 0, $isAll = false, $txtAll = 'Phương thức giao hàng'){
        $retVal = '<select name="TransportTypeId" id="transportTypeId" class="form-control">';
        if($isAll) $retVal .= '<option value="0">'.$txtAll.'</option>';
        foreach($listTransportTypes as $tt){
            if($tt['ParentTransportTypeId'] == 0){
                $retVal .= '<optgroup label="' . $tt['TransportTypeName'] . '"><option value="' . $tt['TransportTypeId'] . '"' . (($tt['TransportTypeId'] == $transportTypeId) ? ' selected="selected"' : '') . '>' . $tt['TransportTypeName'] . '</option>';
                foreach($listTransportTypes as $tt2) {
                    if ($tt2['ParentTransportTypeId'] == $tt['TransportTypeId']) $retVal .= '<option value="' . $tt2['TransportTypeId'] . '"' . (($tt2['TransportTypeId'] == $transportTypeId) ? ' selected="selected"' : '') . '>' . $tt2['TransportTypeName'] . '</option>';
                }
                $retVal .= '</optgroup>';
            }
        }
        $retVal .= '</select>';
        return $retVal;
    }
}
