<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcomplaints extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "complaints";
        $this->_primary_key = "ComplaintId";
    }

    public function getCount($postData){
        $query = "ComplaintStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM complaints WHERE ComplaintStatusId > 0" . $this->buildQuery($postData) . ' ORDER BY OrderId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['OrderId']) && $postData['OrderId'] > 0) $query.=" AND OrderId=".$postData['OrderId'];
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND CustomerId=".$postData['CustomerId'];
        if(isset($postData['ComplaintStatusId']) && $postData['ComplaintStatusId'] > 0) $query.=" AND ComplaintStatusId=".$postData['ComplaintStatusId'];
        if(isset($postData['CareStaffId']) && $postData['CareStaffId'] > 0) $query.=" AND CareStaffId=".$postData['CareStaffId'];
        if(isset($postData['OrderUserId']) && $postData['OrderUserId'] >= 0) $query.=" AND OrderUserId=".$postData['OrderUserId'];
        if(isset($postData['UserHandelId']) && $postData['UserHandelId'] > 0) $query.=" AND (CareStaffId={$postData['UserHandelId']} OR OrderUserId={$postData['UserHandelId']})";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND CrDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND CrDateTime <= '{$postData['EndDate']}'";
        return $query;
    }

    public function statisticCounts($postData, $complaintStatusStatistics = array(), $isStatisticAll = false){
        $retVal = array();
        $retVal[0] = 0;//total
        $complaintStatus = $this->Mconstants->complaintStatus;
        foreach($complaintStatus as $i => $v) $retVal[$i] = 0;
        if(!empty($complaintStatusStatistics)) $complaintStatusStatistics = array_keys($complaintStatusStatistics);
        $flag = false;
        $query = "SELECT ";
        foreach($complaintStatus as $i => $v){
            if(in_array($i, $complaintStatusStatistics)){
                $flag = true;
                $query .= "COUNT(CASE WHEN ComplaintStatusId = {$i} THEN 1 END) AS Count{$i},";
            }
        }
        if($isStatisticAll){
            $flag = true;
            $query .= "COUNT(CASE WHEN ComplaintStatusId > 0 THEN 1 END) AS Total FROM complaints WHERE 1=1";
        }
        else{
            $query = substr($query, 0, strlen($query) - 1);
            $query .= " FROM complaints WHERE 1=1";
        }
        $query.=$this->buildQuery($postData);
        if($flag) {
            $counts = $this->getByQuery($query);
            if (!empty($counts)) {
                $counts = $counts[0];
                foreach ($counts as $label => $value) {
                    if ($label == 'Total') $retVal[0] = $value;
                    foreach ($complaintStatus as $i => $v) {
                        if ($label == 'Count' . $i) $retVal[$i] = $value;
                    }
                }
            }
        }
        return $retVal;
    }

    public function update($complaintId, $postData, $crUserId, $orderCode, $postDataMsg = array()){
        $this->load->model('Mnotifications');
        $crDateTime = getCurentDateTime();
        $this->db->trans_begin();
        $complaintId = $this->save($postData, $complaintId);
        if($complaintId > 0) {
            if(!empty($postDataMsg)){
                $this->load->model('Mcomplaintmsgs');
                $this->Mcomplaintmsgs->save($postDataMsg);
            }
            if($postData['ComplaintStatusId'] == 1){
                $notificationData = array(
                    'CustomerId' => $postData['CustomerId'],
                    'UserId' => $postData['CareStaffId'],
                    'RoleId' => 1,
                    'OrderId' => $postData['OrderId'],
                    'ProductId' => $postData['ProductId'],
                    'ComplaintId' => $complaintId,
                    'Message' => "Đơn hàng {$orderCode} khách có khiếu nại.",
                    'IsCustomerSend' => 1,
                    'NotificationStatusId' => 1,
                    'LinkTo' => 'order/edit/'.$postData['OrderId'].'/'.$complaintId,
                    'CrDateTime' => $crDateTime
                );
                $this->Mnotifications->save($notificationData);
            }
            elseif ($postData['ComplaintStatusId'] == 5) {
                $notificationData = array(
                    array(
                        'CustomerId' => $postData['CustomerId'],
                        'UserId' => $postData['CareStaffId'],
                        'RoleId' => 1,
                        'OrderId' => $postData['OrderId'],
                        'ProductId' => $postData['ProductId'],
                        'ComplaintId' => $complaintId,
                        'Message' => "Khiếu nại của đơn hàng {$orderCode} đã được giải quyết.",
                        'IsCustomerSend' => 0,
                        'NotificationStatusId' => 1,
                        'LinkTo' => 'chi-tiet-don-hang-' . $postData['OrderId'] . '--khieu-nai-' . $complaintId,
                        'CrDateTime' => $crDateTime
                    ),
                    array(
                        'CustomerId' => $postData['CustomerId'],
                        'UserId' => $postData['CareStaffId'],
                        'RoleId' => 1,
                        'OrderId' => $postData['OrderId'],
                        'ProductId' => $postData['ProductId'],
                        'ComplaintId' => $complaintId,
                        'Message' => "Khiếu nại của đơn hàng {$orderCode} đã được duyệt.",
                        'IsCustomerSend' => 1,
                        'NotificationStatusId' => 1,
                        'LinkTo' => 'order/edit/'.$postData['OrderId'].'/'.$complaintId,
                        'CrDateTime' => $crDateTime
                    )
                );
                $this->db->insert_batch('notifications', $notificationData);
            }
            /*if($postData['ComplaintStatusId'] == 7){
                $paidTQ = $postData['ExchangeCostTQ'] + $postData['ReduceCostTQ'];
                if($paidTQ > 0){
                    $this->load->model('Mtransactions');
                    $paidVN = $paidTQ * $postData['ExchangeRate'];
                    $crDateTime = getCurentDateTime();
                    $transactionData = array(
                        'CustomerId' => $postData['CustomerId'],
                        'OrderId' => $postData['OrderId'],
                        'ComplaintId' => $complaintId,
                        'StatusId' => 1,
                        'TransactionTypeId' => 2,
                        'TransactionRangeId' => 3,
                        'WarehouseId' => 1,
                        'MoneySourceId' => 1,
                        'Comment' => '',
                        'Confirm' => '',
                        'PaidTQ' => $paidTQ,
                        'ExchangeRate' => $postData['ExchangeRate'],
                        'PaidVN' => $paidVN,
                        'PaidDateTime' => $crDateTime,
                        'Balance' => 0,
                        'CrUserId' => $crUserId,
                        'CrDateTime' => $crDateTime
                    );
                    $this->Mtransactions->save($transactionData);
                }
            }*/
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $complaintId;
        }
    }
}
