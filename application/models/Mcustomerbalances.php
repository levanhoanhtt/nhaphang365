<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcustomerbalances extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "customerbalances";
        $this->_primary_key = "CustomerBalanceId";
    }

    public function getCurentBalance($customerId){
        return $this->getFieldValue(array('CustomerId' => $customerId, 'IsLast' => 1), 'Balance', 0);
    }

    public function insert($postData){
        $this->db->trans_begin();
        $this->db->update('customerbalances', array('IsLast' => 0), array('CustomerId' => $postData['CustomerId'], 'IsLast' => 1));
        $this->save($postData);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }
}
