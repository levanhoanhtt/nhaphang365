<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mbankaccounts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "bankaccounts";
        $this->_primary_key = "BankAccountId";
    }
}
