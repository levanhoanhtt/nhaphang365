<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransportfeelogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transportfeelogs";
        $this->_primary_key = "TransportFeeLogId";
    }

    public function insert($transportFeeId, $updateUserId, $updateDateTime){
        $this->db->query('INSERT INTO transportfeelogs(TransportFeeId,WarehouseId,BeginWeight,EndWeight,WeightCost,StatusId,CrUserId,CrDateTime,UpdateUserId,UpdateDateTime)
                                                SELECT TransportFeeId,WarehouseId,BeginWeight,EndWeight,WeightCost,StatusId,CrUserId,CrDateTime, ?, ? FROM transportfees WHERE TransportFeeId = ?',
                        array($updateUserId, $updateDateTime, $transportFeeId));

    }
}
