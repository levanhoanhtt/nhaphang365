<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marticlecates extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "articlecates";
        $this->_primary_key = "ArticleCateId";
    }

    public function getListCateIds($articleId){
        $retVal = array();
        $acs = $this->getBy(array('ArticleId' => $articleId));
        foreach($acs as $ac) $retVal[] = $ac['CateId'];
        return $retVal;
    }
}