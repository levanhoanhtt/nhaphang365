<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transactions";
        $this->_primary_key = "TransactionId";
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM transactions WHERE StatusId > 0" . $this->buildQuery($postData, true);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function getListVerify($roleId){
        $retVal = array();
        $query = '';
        if($roleId == 3 || $roleId == 7) $query = "SELECT * FROM transactions WHERE StatusId IN(1, 4) AND CustomerId > 0";
        elseif($roleId == 6) $query = "SELECT * FROM transactions WHERE StatusId = 1 AND CustomerId > 0";
        if(!empty($query)) $retVal = $this->getByQuery($query);
        return $retVal;
    }

    private function buildQuery($postData, $orderBy = false){
        $query = '';
        if(isset($postData['TransactionCode']) && $postData['TransactionCode'] > 0) $query.=" AND TransactionId=".$postData['TransactionCode'];
        if(isset($postData['CustomerId']) && $postData['CustomerId'] >= 0) $query.=" AND CustomerId=".$postData['CustomerId'];
        if(isset($postData['OrderId']) && $postData['OrderId'] >= 0) $query.=" AND OrderId=".$postData['OrderId'];
        if(isset($postData['ComplaintId']) && $postData['ComplaintId'] >= 0) $query.=" AND ComplaintId=".$postData['ComplaintId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['TransactionTypeId']) && $postData['TransactionTypeId'] > 0) $query.=" AND TransactionTypeId=".$postData['TransactionTypeId'];
        if(isset($postData['TransactionRangeId']) && $postData['TransactionRangeId'] > 0) $query.=" AND TransactionRangeId=".$postData['TransactionRangeId'];
        if(isset($postData['WarehouseId']) && $postData['WarehouseId'] > 0) $query.=" AND WarehouseId=".$postData['WarehouseId'];
        if(isset($postData['MoneySourceId']) && $postData['MoneySourceId'] > 0) $query.=" AND MoneySourceId=".$postData['MoneySourceId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND PaidDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND PaidDateTime <= '{$postData['EndDate']}'";
        if($orderBy) $query .= " ORDER BY PaidDateTime DESC";
        return $query;
    }

    public function update($postData, $transactionId, $isUpdateField = false){
        $updateDateTime = ($transactionId > 0) ?  $postData['UpdateDateTime'] : $postData['CrDateTime'];
        $this->db->trans_begin();
        if($transactionId > 0){
            $this->load->model('Mtransactionlogs');
            $this->Mtransactionlogs->insert($transactionId, $postData['UpdateUserId'], $updateDateTime);
        }
        $fieldNull = array();
        if(!$isUpdateField) $fieldNull = array('Comment', 'Confirm', 'PaidTQ', 'UpdateUserId', 'UpdateDateTime');
        if(isset($postData['StatusId'])) {
            if ($postData['CustomerId'] > 0 && $postData['PaidVN'] > 0 && $postData['StatusId'] == STATUS_ACTIVED) $postData['Balance'] = $this->getNewBalance($postData['CustomerId'], $postData['TransactionTypeId'], $postData['PaidVN']);
            else $postData['Balance'] = 0;
        }
        $transactionId = $this->save($postData, $transactionId, $fieldNull);
        if(isset($postData['StatusId'])) {
            if ($transactionId > 0 && $postData['CustomerId'] > 0 && $postData['PaidVN'] > 0 && $postData['StatusId'] == STATUS_ACTIVED) {
                $balanceData = array(
                    'CustomerId' => $postData['CustomerId'],
                    'TransactionId' => $transactionId,
                    'Balance' => $postData['Balance'],
                    'IsLast' => 1,
                    'CrDateTime' => $updateDateTime
                );
                $this->load->model('Mcustomerbalances');
                $this->Mcustomerbalances->insert($balanceData);
                if ($postData['TransactionTypeId'] == 1) {
                    $this->load->model('Morders');
                    $notificationData = array(
                        array(
                            'CustomerId' => $postData['CustomerId'],
                            'UserId' => isset($postData['UpdateUserId']) ? $postData['UpdateUserId'] : $postData['CrUserId'],
                            'RoleId' => 3,
                            'OrderId' => $postData['OrderId'],
                            'ProductId' => 0,
                            'ComplaintId' => 0,
                            'Message' => 'Hệ thống đã nhận được khoản tiền ' . priceFormat($postData['PaidVN']) . ' VNĐ.',
                            'IsCustomerSend' => 0,
                            'NotificationStatusId' => 1,
                            'LinkTo' => 'customer/transaction',
                            'CrDateTime' => $updateDateTime
                        )
                    );
                    $careStaffId = $this->Morders->getFieldValue(array('OrderId' => $postData['OrderId']), 'CareStaffId');
                    if ($careStaffId > 0) {
                        $notificationData[] = array(
                            'CustomerId' => $postData['CustomerId'],
                            'UserId' => $careStaffId,
                            'RoleId' => 1,
                            'OrderId' => $postData['OrderId'],
                            'ProductId' => 0,
                            'ComplaintId' => 0,
                            'Message' => "Phiếu thu #{$transactionId} gắn với khách {$this->Musers->getFieldValue(array('UserId' => $postData['CustomerId']), 'FullName')} đã được duyệt.",
                            'IsCustomerSend' => 1,
                            'NotificationStatusId' => 1,
                            'LinkTo' => 'transaction/edit/' . $transactionId,
                            'CrDateTime' => $updateDateTime
                        );
                    }
                    $this->db->insert_batch('notifications', $notificationData);
                }
            }
        }
        else{
            if ($transactionId > 0) $this->db->update('customerbalances', array('Balance' => $postData['Balance']), array('TransactionId' => $transactionId));
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return $transactionId;
        }
    }

    //them ghi no
    public function addDebit($orderData, $orderId, $orderCode){
        $this->load->model('Morders');
        $this->load->model('Mproducts');
        $this->load->model('Mcustomerbalances');
        $this->db->trans_begin();
        $customerId = 0;
        $exchangeRate = 0;
        $servicePercent = 0;
        $otherCost = 0;
        if(isset($orderData['CustomerId']) && $orderData['CustomerId'] > 0) $customerId = $orderData['CustomerId'];
        if(isset($orderData['ExchangeRate']) && $orderData['ExchangeRate'] > 0) $exchangeRate = $orderData['ExchangeRate'];
        if($customerId == 0){
            $order = $this->Morders->get($orderId, true, '', 'CustomerId,ExchangeRate,ServicePercent,OtherCost');
            if($order){
                $customerId = $order['CustomerId'];
                $exchangeRate = $order['ExchangeRate'];
                $servicePercent = $order['ServicePercent'];
                $otherCost = $order['OtherCost'];
            }
        }
        else{
            $otherCost = $orderData['OtherCost'];
            $servicePercent = $orderData['ServicePercent'];
        }
        if($customerId > 0) {
            if(isset($postData['UpdateDateTime'])) $crDateTime = $postData['UpdateDateTime'];
            else $crDateTime = getCurentDateTime();
            if(isset($orderData['UpdateUserId'])) $crUserId = $orderData['UpdateUserId'];
            elseif(isset($orderData['CrUserId'])) $crUserId = $orderData['CrUserId'];
            else $crUserId = 0;
            $totalCost = $otherCost;
            $productInfos = $this->Mproducts->getListInfo($orderId);
            $paidVN  = $exchangeRate * ($productInfos['PaidTQ'] + $productInfos['ShipTQ']);
            $serviceCost = round($paidVN * $servicePercent/100);
            $totalCost += $paidVN + $serviceCost;
            $paidVN = $totalCost;
            $balance = $this->getNewBalance($customerId, 3,  $paidVN);
            $transactionData = array(
                'CustomerId' => $customerId,
                'OrderId' => $orderId,
                'ComplaintId' => 0,
                'StatusId' => 2,
                'TransactionTypeId' => 3,
                'TransactionRangeId' => 1,
                'WarehouseId' => 1,
                'MoneySourceId' => 1,
                'Comment' => 'Ghi nợ đơn hàng '.$orderCode,
                'Confirm' => '',
                'PaidTQ' => 0,
                'ExchangeRate' => $exchangeRate,
                'PaidVN' => $paidVN,
                'PaidDateTime' => $crDateTime,
                'Balance' => $balance,
                'CrUserId' => $crUserId,
                'CrDateTime' => $crDateTime
            );
            $transactionId = $this->save($transactionData);
            if($transactionId > 0){
                $balanceData = array(
                    'CustomerId' => $customerId,
                    'TransactionId' => $transactionId,
                    'Balance' => $balance,
                    'IsLast' => 1,
                    'CrDateTime' => $crDateTime
                );
                $this->Mcustomerbalances->insert($balanceData);
                //thong bao cho khach
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function getNewBalance($customerId, $transactionTypeId,  $paidVN){
        $curentBalance = $this->Mcustomerbalances->getCurentBalance($customerId);
        if($transactionTypeId == 1) $balance = $curentBalance + $paidVN; //phieu thu
        else if($transactionTypeId == 2) $balance = $curentBalance - $paidVN; //phieu chi +
        else if($transactionTypeId == 3) $balance = $curentBalance - $paidVN; //ghi no
        else $balance = 0;
        return $balance;
    }
}