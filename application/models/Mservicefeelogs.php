<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mservicefeelogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "servicefeelogs";
        $this->_primary_key = "ServiceFeeLogId";
    }

    public function insert($serviceFeeId, $updateUserId, $updateDateTime){
        $this->db->query('INSERT INTO servicefeelogs(ServiceFeeId, BeginCost, EndCost, Percent, StatusId, CrUserId, CrDateTime, UpdateUserId, UpdateDateTime)
                                              SELECT ServiceFeeId, BeginCost, EndCost, Percent, StatusId, CrUserId, CrDateTime, ?, ? FROM servicefees WHERE ServiceFeeId = ?',
                        array($updateUserId, $updateDateTime, $serviceFeeId));

    }
}
