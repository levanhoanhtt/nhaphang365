<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morderaccounts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orderaccounts";
        $this->_primary_key = "OrderAccountId";
    }
}