<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morderdraffs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orderdraffs";
        $this->_primary_key = "OrderDraffId";
    }

    public function update($postData){
        if($postData['UserId'] > 0) $where = array('UserId' => $postData['UserId']);
        else $where = array('IpAddress' => $postData['IpAddress']);
        $orderDraffId = $this->getFieldValue($where, 'OrderDraffId', 0);
        $orderDraffId = $this->save($postData, $orderDraffId, array('FullName', 'PhoneNumber', 'Email', 'Address', 'Comment', 'ProductJson'));
        if($orderDraffId > 0) return true;
        return false;
        /*$this->db->trans_begin();
        $this->deleteMultiple($where);
        $this->save($postData, 0, array('FullName', 'PhoneNumber', 'Email', 'Address', 'Comment', 'ProductJson'));
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }*/
    }

    public function getByUser($userId, $ipAddress){
        if($userId > 0) $where = array('UserId' => $userId);
        else $where = array('IpAddress' => $ipAddress);
        return $this->getBy($where, true);
    }
}
