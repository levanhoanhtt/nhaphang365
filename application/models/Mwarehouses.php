<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mwarehouses extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "warehouses";
        $this->_primary_key = "WarehouseId";
    }
}
