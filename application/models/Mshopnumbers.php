<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mshopnumbers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "shopnumbers";
        $this->_primary_key = "ShopNumberId";
    }

    public function getCount($postData){
        $query = "ShopPaidStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM shopnumbers WHERE ShopPaidStatusId > 0" . $this->buildQuery($postData, true);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $orderBy = false){
        $query = '';
        if(isset($postData['ShopNo']) && !empty($postData['ShopNo'])) $query.=" AND ShopNo='{$postData['ShopNo']}'";
        if(isset($postData['OrderId']) && $postData['OrderId'] > 0) $query.=" AND OrderId=".$postData['OrderId'];
        if(isset($postData['ShopPaidStatusId']) && $postData['ShopPaidStatusId'] > 0) $query.=" AND ShopPaidStatusId=".$postData['ShopPaidStatusId'];
        if(isset($postData['IsReturn']) && $postData['IsReturn'] >= 0) $query.=" AND ReturnCost > 0 AND IsReturn=".$postData['IsReturn'];
        if(isset($postData['NoIsReturn']) && $postData['NoIsReturn'] == 1) $query.=" AND IsReturn = 0";
        if(isset($postData['OrderAccountId']) && $postData['OrderAccountId'] > 0) $query.=" AND OrderAccountId=".$postData['OrderAccountId'];
        if(isset($postData['OrderUserId']) && $postData['OrderUserId'] > 0) $query.=" AND OrderId IN(SELECT OrderId FROM orders WHERE OrderUserId = {$postData['OrderUserId']})";
        if($orderBy) $query .= " ORDER BY ShopId DESC";
        return $query;
    }

    public function getByGroup($orderId){
        $retVal = array();
        $listShopNumbers = $this->getBy(array('OrderId' => $orderId, 'ShopPaidStatusId >' => 0));
        foreach($listShopNumbers as $sn) $retVal[$sn['ShopId']][] = $sn;
        return $retVal;
    }

    public function insert($shopNumberInserts, $orderStatusId){
        $this->db->trans_begin();
        if(!empty($shopNumberInserts)) {
            $this->db->insert_batch('shopnumbers', $shopNumberInserts);
            if($orderStatusId == 5){
                $this->load->model('Morders');
                $this->Morders->updateField(array(
                    'OrderStatusId' => 10,
                    'UpdateUserId' => $shopNumberInserts[0]['CrUserId'],
                    'UpdateDateTime' => $shopNumberInserts[0]['CrDateTime']
                ), $shopNumberInserts[0]['OrderId']);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function update($postData, $shopNumberId, $orderStatusId, $updateTypeId){
        $this->load->model('Mrecharges');
        $this->db->trans_begin();
        //log

        $this->save($postData, $shopNumberId);
        if($updateTypeId == 2){ //cap nhat o trnag thanh toan
            if ($postData['ShopPaidStatusId'] == 3 && $postData['BankAccountId'] > 0) {
                $rechargeData = array(
                    'BankAccountId' => $postData['BankAccountId'],
                    'ShopNumberId' => $shopNumberId,
                    'PaidTQ' => $postData['ShopCostAdmin'] > 0 ? $postData['ShopCostAdmin'] : $postData['ShopCost'],
                    'ExchangeRate' => 0,
                    'RechargeTypeId' => 3,
                    'Balance' => 0,
                    'RechargeDate' => $postData['UpdateDateTime'],
                    'Comment' => 'Thanh toán STT '.$postData['ShopNo'],
                    'CrUserId' => $postData['UpdateUserId'],
                    'CrDateTime' => $postData['UpdateDateTime']
                );
                $this->Mrecharges->insert($rechargeData);
            }
        }
        elseif($updateTypeId == 3){ //cap nhat doi tien
            if($postData['BankAccountId'] > 0) {
                $rechargeData = array(
                    'BankAccountId' => $postData['BankAccountId'],
                    'ShopNumberId' => $shopNumberId,
                    'PaidTQ' => $postData['ReturnCost'],
                    'ExchangeRate' => 0,
                    'RechargeTypeId' => 1,
                    'Balance' => 0,
                    'RechargeDate' => $postData['UpdateDateTime'],
                    'Comment' => 'Đòi lại tiền STT '.$postData['ShopNo'],
                    'CrUserId' => $postData['UpdateUserId'],
                    'CrDateTime' => $postData['UpdateDateTime']
                );
                $this->Mrecharges->insert($rechargeData);
            }
        }
        if($orderStatusId == 5){
            $this->load->model('Morders');
            $this->Morders->updateField(array(
                'OrderStatusId' => 10,
                'UpdateUserId' => $postData['UpdateUserId'],
                'UpdateDateTime' => $postData['UpdateDateTime']
            ), $postData['OrderId']);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function getShopCount($orderId){
        $shopCounts = $this->getByQuery('SELECT DISTINCT ShopId FROM shopnumbers WHERE OrderId = ?', array($orderId));
        return count($shopCounts);
    }

    public function statisticReturnCounts($postData){
        $retVal = array(0, 0);
        $query = "SELECT COUNT(CASE WHEN IsReturn = 0 THEN 1 END) AS Count0,COUNT(CASE WHEN IsReturn = 1 THEN 1 END) AS Count1 FROM shopnumbers WHERE ReturnCost > 0";
        $query .= $this->buildQuery($postData);
        $counts = $this->getByQuery($query);
        if (!empty($counts)) {
            $counts = $counts[0];
            foreach ($counts as $label => $value) {
                for($i = 0; $i <= 1; $i++){
                    if ($label == 'Count' . $i) $retVal[$i] = $value;
                }
            }
        }
        return $retVal;
    }
}