<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Morders extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "orders";
        $this->_primary_key = "OrderId";
    }

    public function update($postData, $orderId, $listProducts){
        $this->load->model('Morderdraffs');
        $this->load->model('Mproducts');
        $this->load->model('Mtransactions');
        $this->load->model('Mshops');
        $this->load->model('Mnotifications');
        if($orderId == 0){
            $isInsert = true;
            $crUserId = $postData['CrUserId'];
            $crDateTime = $postData['CrDateTime'];
        }
        else{
            $isInsert = false;
            $crUserId = $postData['UpdateUserId'];
            $crDateTime = $postData['UpdateDateTime'];
        }
        $this->db->trans_begin();
        if($postData['CustomerId'] == 0){
            $customer = $this->Musers->checkExist(0, "", $postData['Email'], $postData['PhoneNumber']);
            if($customer) $customerId = $customer['UserId'];
            else $customerId = 0;
            if($customerId == 0) {
                $userData = array(
                    'FullName' => $postData['FullName'],
                    'PhoneNumber' => $postData['PhoneNumber'],
                    'Email' => $postData['Email'],
                    'Address' => $postData['Address'],
                    'UserName' => $postData['PhoneNumber'],
                    'UserPass' => md5('123456'),
                    'RoleId' => 4,
                    'GenderId' => 1,
                    'StatusId' => STATUS_ACTIVED,
                    'ProvinceId' => 24,
                    'CrDateTime' => $crDateTime
                );
                $customerId = $this->Musers->save($userData, 0);
            }
            if($customerId > 0) $postData['CustomerId'] = $customerId;
        }
        if($postData['CustomerId'] > 0) {
            $customerName = $postData['FullName'];
            unset($postData['FullName']);
            unset($postData['IpAddress']);
            if(isset($postData['CrUserId']) && $postData['CrUserId'] == 0) $postData['CrUserId'] = $postData['CustomerId'];
            if($postData['OrderStatusId'] == 4) $postData['CustomerOkDate'] = $crDateTime;
            $orderCode = $postData['OrderCode'];
            if(!$isInsert){
                unset($postData['OrderCode']);
                $this->db->update('shops', array('ShopStatusId' => 0, 'UpdateUserId' => $crUserId, 'UpdateDateTime' => $crDateTime), array('OrderId' => $orderId));
                //log
            }
            $orderId = $this->save($postData, $orderId, array('Comment', 'CustomerOkDate', 'OrderDate', 'UpdateUserId', 'UpdateDateTime'));
            if ($orderId > 0) {
                if($crUserId == 0) $crUserId = $postData['CustomerId'];
                if($isInsert){
                    $orderCode = 'D' . ddMMyyyy($crDateTime, "dm") . ($orderId + 10000);
                    $this->db->update('orders', array('OrderCode' => $orderCode), array('OrderId' => $orderId));
                    $this->Morderdraffs->deleteMultiple(array('UserId' => $crUserId));
                }
                else{
                    if($postData['OrderStatusId'] == 3){
                        $notificationData = array(
                            'CustomerId' => $postData['CustomerId'],
                            'UserId' => $postData['UpdateUserId'],
                            'RoleId' => 1,
                            'OrderId' => $orderId,
                            'ProductId' => 0,
                            'ComplaintId' => 0,
                            'Message' => "Đơn hàng {$orderCode} nhân viên đã check xong, bạn vui lòng kiểm tra.",
                            'IsCustomerSend' => 0,
                            'NotificationStatusId' => 1,
                            'LinkTo' => 'chi-tiet-don-hang-'.$orderId,
                            'CrDateTime' => $crDateTime
                        );
                        $this->Mnotifications->save($notificationData);
                    }
                }
                if($postData['OrderStatusId'] == 1 || $postData['OrderStatusId'] == 4){
                    $message = ($postData['OrderStatusId'] == 1) ? "Khách hàng {$customerName} đã gửi đơn hàng {$orderCode} cần check." : "Đơn hàng {$orderCode} đã được chốt.";
                    $notificationData = array(
                        'CustomerId' => $postData['CustomerId'],
                        'UserId' => $postData['CareStaffId'],
                        'RoleId' => 1,
                        'OrderId' => $orderId,
                        'ProductId' => 0,
                        'ComplaintId' => 0,
                        'Message' => $message,
                        'IsCustomerSend' => 1,
                        'NotificationStatusId' => 1,
                        'LinkTo' => 'order/edit/'.$orderId,
                        'CrDateTime' => $crDateTime
                    );
                    $this->Mnotifications->save($notificationData);
                }
                $labelUnsets = array('ShopCode', 'ShopName', 'ShopUrl', 'CategoryId', 'ShipTQ', 'ShipTQOldNew', 'ShopStatusId', 'IsTransportSlow', 'ShopComment', 'ShopFeedback', 'ShipTQReason', 'ShipTQConfirm');
                $shopIds = array();
                $productInserts = array();
                $productUpdates = array();
                $i = 0;
                foreach($listProducts as $p){
                    if(!empty($p['ShopCode'])){
                        if(!isset($shopIds[$p['ShopCode']])){
                            $shopId = $this->Mshops->update($p, $orderId, $crUserId, $crDateTime);
                            $shopIds[$p['ShopCode']] = $shopId;
                        }
                        $p['ShopId'] = $shopIds[$p['ShopCode']];
                    }
                    else{
                        $i++;
                        $p['ShopCode'] = 'HV_'.$i;
                        $p['ShopName'] = 'HV_'.$i;
                        $p['ShopUrl'] = 'javascript:void(0)';
                        $p['ShopId'] = $this->Mshops->update($p, $orderId, $crUserId, $crDateTime);
                    }
                    $p['OrderId'] = $orderId;
                    $p['Quantity'] = replacePrice($p['Quantity']);
                    $p['Cost'] = replacePrice($p['Cost']);
                    foreach($labelUnsets as $l) unset($p[$l]);
                    if($p['ProductId'] > 0){
                        $p['UpdateUserId'] = $crUserId;
                        $p['UpdateDateTime'] = $crDateTime;
                        $productUpdates[]=$p;
                    }
                    else{
                        $p['CrUserId'] = $crUserId;
                        $p['CrDateTime'] = $crDateTime;
                        unset($p['ProductId']);
                        $productInserts[] = $p;
                    }
                }
                if(!empty($productInserts)) $this->db->insert_batch('products', $productInserts);
                if(!empty($productUpdates)) $this->db->update_batch('products', $productUpdates, 'ProductId');
                if($postData['OrderStatusId'] == 5) $this->Mtransactions->addDebit($postData, $orderId, $orderCode);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $orderId;
        }
    }

    public function updateField($postData, $orderId, $notificationData = array(), $customerId = 0, $orderCode = ''){
        $this->db->trans_begin();
        //logs..
        $this->save($postData, $orderId);
        if(isset($postData['OrderStatusId'])){
            if($postData['OrderStatusId'] == 5) {
                //them ghi no
                $this->load->model('Mtransactions');
                $this->Mtransactions->addDebit($postData, $orderId, $orderCode);
            }
            elseif($postData['OrderStatusId'] == 8) {
                $this->load->model('Mtransactions');
                $this->load->model('Mcustomerbalances');
                $transaction = $this->Mtransactions->getBy(array('OrderId' => $orderId, 'CustomerId' => $customerId, 'StatusId' => STATUS_ACTIVED, 'TransactionRangeId' => 1, 'TransactionTypeId' => 3), true);
                if($transaction){
                    $updateDateTime = $postData['UpdateDateTime'];
                    $balance = $this->Mtransactions->getNewBalance($customerId, 1, $transaction['PaidVN']);
                    $transaction['TransactionTypeId'] = 1;
                    $transaction['PaidDateTime'] = $updateDateTime;
                    $transaction['Balance'] = $balance;
                    $transaction['Comment'] = 'Hoàn tiền đơn hàng '.$orderCode;
                    $transaction['CrUserId'] = $postData['UpdateUserId'];
                    $transaction['CrDateTime'] = $updateDateTime;
                    unset($transaction['TransactionId']);
                    unset($transaction['UpdateUserId']);
                    unset($transaction['UpdateDateTime']);
                    $transactionId = $this->Mtransactions->save($transaction);
                    $balanceData = array(
                        'CustomerId' => $customerId,
                        'TransactionId' => $transactionId,
                        'Balance' => $balance,
                        'IsLast' => 1,
                        'CrDateTime' => $updateDateTime
                    );
                    $this->Mcustomerbalances->insert($balanceData);
                }
            }
        }
        if(!empty($notificationData)){
            $this->load->model('Mnotifications');
            $this->Mnotifications->save($notificationData);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function cloneOrder($orderId, $postData){
        $orderCode = '';
        $shopCodeOlds = array();
        $shopCodeNews = array();
        $this->db->trans_begin();
        $orderCloneId = $this->save($postData, 0, array('Comment', 'CustomerOkDate', 'OrderDate', 'UpdateUserId', 'UpdateDateTime'));
        if($orderCloneId > 0){
            $query = "INSERT INTO products(OrderId,ProductLink,ProductImage,Color,Size,Quantity,Cost,ProductStatusId,ShopId,Comment,Feedback,QuantityOldNew,CostOldNew,RemoveReason,RemoveConfirm,CrUserId,CrDateTime,UpdateUserId,UpdateDateTime)
                                    SELECT {$orderCloneId},ProductLink,ProductImage,Color,Size,Quantity,Cost,2,ShopId,NULL,NULL,NULL,NULL,NULL,NULL,CrUserId,NOW(),NULL,NULL
                                    FROM products WHERE OrderId = {$orderId} AND ProductStatusId = 2";
            $this->db->query($query);
            $query = "INSERT INTO shops(ShopCode,ShopName,ShopUrl,OrderId,CategoryId,ShipTQ,ShipTQOldNew,ShopStatusId,IsTransportSlow,Comment,Feedback,ShipTQReason,ShipTQConfirm,CrUserId,CrDateTime,UpdateUserId,UpdateDateTime)
                                 SELECT ShopCode,ShopName,ShopUrl,{$orderCloneId},CategoryId,ShipTQ,0,2,0,Comment,Feedback,NULL,NULL,CrUserId,NOW(),NULL,NULL
                                 FROM shops WHERE OrderId = {$orderId} AND ShopStatusId = 2";
            $this->db->query($query);
            $orderCode = 'D' . ddMMyyyy($postData['CrDateTime'], "dm") . ($orderCloneId + 10000);
            $this->db->update('orders', array('OrderCode' => $orderCode), array('OrderId' => $orderCloneId));
            $this->load->model('Mshops');
            $shopCodes = $this->Mshops->getBy(array('OrderId' => $orderId, 'ShopStatusId' => 2), false, '', 'ShopId, ShopCode');
            foreach($shopCodes as $sc) $shopCodeOlds[$sc['ShopCode']] = $sc['ShopId'];
            $shopCodes = $this->Mshops->getBy(array('OrderId' => $orderCloneId), false, '', 'ShopId, ShopCode');
            foreach($shopCodes as $sc) $shopCodeNews[$sc['ShopCode']] = $sc['ShopId'];
            foreach($shopCodeOlds as $shopCode => $shopId){
                if(isset($shopCodeNews[$shopCode])) $this->db->query("UPDATE products SET ShopId = {$shopCodeNews[$shopCode]} WHERE OrderId ={$orderCloneId} AND ShopId = {$shopId}");
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return '';
        }
        else{
            $this->db->trans_commit();
            return $orderCode;
        }
    }

    public function merge($orderId, $mergeOrderId, $updateUserId, $updateDateTime, $notificationData = array()){
        $this->db->trans_begin();
        //update shop
        $this->db->update('shops', array('OrderId' => $orderId), array('OrderId' => $mergeOrderId));
        $this->db->update('shops', array('ShopCode' => 'HV_' . $orderId, 'ShopName' => 'HV_' . $orderId), array('ShopCode' => 'HV_' . $mergeOrderId));
        //update product
        $this->db->update('products', array('OrderId' => $orderId, 'UpdateUserId' => $updateUserId, 'UpdateDateTime' => $updateDateTime), array('OrderId' => $mergeOrderId));
        //update order
        $this->updateField(array('OrderStatusId' => 0, 'UpdateUserId' => $updateUserId, 'UpdateDateTime' => $updateDateTime), $mergeOrderId);
        $this->updateField(array('OrderStatusId' => 1, 'UpdateUserId' => $updateUserId, 'UpdateDateTime' => $updateDateTime), $orderId);
        if(!empty($notificationData)) {
            $this->load->model('Mnotifications');
            $this->Mnotifications->save($notificationData);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function mergeDraff($orderId, $listProducts, $updateUserId, $updateDateTime, $notificationData = array()){
        $this->load->model('Morderdraffs');
        $this->load->model('Mproducts');
        $this->load->model('Mshops');
        $labelUnsets = array('ProductId', 'ShopCode', 'ShopName', 'ShopUrl', 'CategoryId', 'ShipTQ', 'ShipTQOldNew', 'ShopStatusId', 'IsTransportSlow', 'ShopComment', 'ShopFeedback', 'ShipTQReason', 'ShipTQConfirm');
        $shopIds = array();
        $productInserts = array();
        $this->db->trans_begin();
        foreach($listProducts as $p){
            if(!empty($p['ShopCode'])){
                if(!isset($shopIds[$p['ShopCode']])){
                    $shopId = $this->Mshops->update($p, $orderId, $updateUserId, $updateDateTime);
                    $shopIds[$p['ShopCode']] = $shopId;
                }
                $p['ShopId'] = $shopIds[$p['ShopCode']];
            }
            else{
                $p['ShopCode'] = 'HV_'.$orderId;
                $p['ShopName'] = 'HV_'.$orderId;
                $p['ShopUrl'] = 'javascript:void(0)';
                $p['ShopId'] = $this->Mshops->update($p, $orderId, $updateUserId, $updateDateTime);
            }
            $p['OrderId'] = $orderId;
            $p['Quantity'] = replacePrice($p['Quantity']);
            $p['Cost'] = replacePrice($p['Cost']);
            foreach($labelUnsets as $l) unset($p[$l]);
            $p['CrUserId'] = $updateUserId;
            $p['CrDateTime'] = $updateDateTime;
            $productInserts[] = $p;
        }
        if(!empty($productInserts)) $this->db->insert_batch('products', $productInserts);
        $this->updateField(array('OrderStatusId' => 1, 'UpdateUserId' => $updateUserId, 'UpdateDateTime' => $updateDateTime), $orderId);
        if(!empty($notificationData)) {
            $this->load->model('Mnotifications');
            $this->Mnotifications->save($notificationData);
        }
        $this->load->model('Morderdraffs');
        $this->Morderdraffs->deleteMultiple(array('UserId' => $updateUserId));
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function getCount($postData){
        $query = "OrderStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $select = '*'){
        $query = "SELECT {$select} FROM orders WHERE OrderStatusId > 0" . $this->buildQuery($postData) . " ORDER BY CrDateTime DESC";
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    public function getListVerify($roleId){
        $retVal = array();
        $query = '';
        if($roleId == 3 || $roleId == 7) $query = "SELECT * FROM orders WHERE OrderStatusId IN(4,9) ORDER BY CustomerOkDate DESC";
        elseif($roleId == 6) $query = "SELECT * FROM orders WHERE OrderStatusId = 4 ORDER BY CustomerOkDate DESC";
        if(!empty($query)) $retVal = $this->getByQuery($query);
        return $retVal;
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['OrderCode']) && !empty($postData['OrderCode'])) $query.=" AND OrderCode='{$postData['OrderCode']}'";
        if(isset($postData['ProductLink']) && !empty($postData['ProductLink'])) $query.=" AND OrderId IN(SELECT OrderId FROM products WHERE ProductLink = '{$postData['ProductLink']}')";
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND CustomerId=".$postData['CustomerId'];
        if(isset($postData['CareStaffId']) && $postData['CareStaffId'] > 0) $query.=" AND CareStaffId=".$postData['CareStaffId'];
        if(isset($postData['OrderUserId']) && $postData['OrderUserId'] > 0) $query.=" AND OrderUserId=".$postData['OrderUserId'];
        if(isset($postData['NoOrderUserId'])) $query.=" AND OrderUserId=0";
        if(isset($postData['UserHandelId']) && $postData['UserHandelId'] > 0) $query.=" AND (CareStaffId={$postData['UserHandelId']} OR OrderUserId={$postData['UserHandelId']})";
        if(isset($postData['OrderStatusId']) && $postData['OrderStatusId'] > 0) $query.=" AND OrderStatusId=".$postData['OrderStatusId'];
        if(isset($postData['OrderStatusIds']) && !empty($postData['OrderStatusIds'])) $query.=" AND OrderStatusId IN({$postData['OrderStatusIds']})";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND ((CustomerOkDate IS NOT NULL AND CustomerOkDate >= '{$postData['BeginDate']}') OR (CustomerOkDate IS NULL AND CrDateTime >= '{$postData['BeginDate']}'))";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND ((CustomerOkDate IS NOT NULL AND CustomerOkDate <= '{$postData['EndDate']}') OR (CustomerOkDate IS NULL AND CrDateTime <= '{$postData['EndDate']}'))";
        if(isset($postData['Tracking']) && !empty($postData['Tracking'])) $query.=" AND OrderId IN(SELECT OrderId FROM shoptrackings WHERE Tracking = '{$postData['Tracking']}')";
        if(isset($postData['TrackingStatusId']) && $postData['TrackingStatusId'] > 0) $query.=" AND TrackingStatusId=".$postData['TrackingStatusId'];
        if(isset($postData['OrderBeginDate']) && !empty($postData['OrderBeginDate'])) $query .= " AND OrderDate >= '{$postData['OrderBeginDate']}'";
        if(isset($postData['OrderEndDate']) && !empty($postData['OrderEndDate'])) $query .= " AND OrderDate <= '{$postData['OrderEndDate']}'";
        if(isset($postData['ShopNo']) && !empty($postData['ShopNo'])) $query.=" AND OrderId IN(SELECT OrderId FROM shopnumbers WHERE ShopNo = '{$postData['ShopNo']}')";
        if(isset($postData['ComplaintStatusId']) && $postData['ComplaintStatusId'] > 0) $query.=" AND OrderId IN(SELECT OrderId FROM complaints WHERE ComplaintStatusId={$postData['ComplaintStatusId']})";
        return $query;
    }

    public function statisticCounts($postData, $orderStatusStatistics = array(), $isStatisticAll = false){
        $retVal = array();
        $retVal[0] = 0;//total
        $orderStatus = $this->Mconstants->orderStatus;
        foreach($orderStatus as $i => $v) $retVal[$i] = 0;
        if(!empty($orderStatusStatistics)) $orderStatusStatistics = array_keys($orderStatusStatistics);
        $flag = false;
        $query = "SELECT ";
        foreach($orderStatus as $i => $v){
            if(in_array($i, $orderStatusStatistics)){
                $flag = true;
                $query .= "COUNT(CASE WHEN OrderStatusId = {$i} THEN 1 END) AS Count{$i},";
            }
        }
        if($isStatisticAll){
            $flag = true;
            $query .= "COUNT(CASE WHEN OrderStatusId > 0 THEN 1 END) AS Total FROM orders WHERE 1=1";
        }
        else{
            $query = substr($query, 0, strlen($query) - 1);
            $query .= " FROM orders WHERE 1=1";
        }
        $query.=$this->buildQuery($postData);
        if($flag) {
            $counts = $this->getByQuery($query);
            if (!empty($counts)) {
                $counts = $counts[0];
                foreach ($counts as $label => $value) {
                    if ($label == 'Total') $retVal[0] = $value;
                    foreach ($orderStatus as $i => $v) {
                        if ($label == 'Count' . $i) $retVal[$i] = $value;
                    }
                }
            }
        }
        return $retVal;
    }

    public function getSumCost($o, $flag = true){
        $retVal = 0;
        if($flag) $retVal += $o['OtherCost'];
        $productInfos = $this->Mproducts->getListInfo($o['OrderId']);
        $paidVN  = $o['ExchangeRate'] * ($productInfos['PaidTQ'] + $productInfos['ShipTQ']);
        $serviceCost = 0;
        if($flag) $serviceCost = round($paidVN * $o['ServicePercent']/100);
        $retVal += $paidVN + $serviceCost;
        return $retVal;
    }
}