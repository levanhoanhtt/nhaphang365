<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransactionlogs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transactionlogs";
        $this->_primary_key = "TransactionLogId";
    }

    public function insert($transactionId, $updateUserId, $updateDateTime){
        $this->db->query('INSERT INTO transactionlogs(TransactionId, CustomerId, OrderId, ComplaintId, StatusId, TransactionTypeId, TransactionRangeId, WarehouseId, MoneySourceId, Comment, Confirm, PaidTQ, ExchangeRate, PaidVN, PaidDateTime, Balance, CrUserId, CrDateTime, UpdateUserId, UpdateDateTime)
                                               SELECT TransactionId, CustomerId, OrderId, ComplaintId, StatusId, TransactionTypeId, TransactionRangeId, WarehouseId, MoneySourceId, Comment, Confirm, PaidTQ, ExchangeRate, PaidVN, PaidDateTime, Balance, CrUserId, CrDateTime, ?, ? FROM transactions WHERE TransactionId = ?',
            array($updateUserId, $updateDateTime, $transactionId));

    }
}