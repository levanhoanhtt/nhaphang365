<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mchats extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "chats";
        $this->_primary_key = "ChatId";
    }

    public function getCount($postData){
        $query = "1 = 1" . $this->buildQuery($postData, false);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $from){
        $query = "SELECT * FROM chats WHERE 1 = 1" . $this->buildQuery($postData);
        if($perPage > 0) $query .= " LIMIT {$from}, {$perPage}";
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $orderBy = true){
        $query = '';
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND CustomerId=".$postData['CustomerId'];
        if(isset($postData['StaffId']) && $postData['StaffId'] > 0) $query.=" AND StaffId=".$postData['StaffId'];
        if(isset($postData['IsCustomerSend']) && $postData['IsCustomerSend'] >= 0) $query.=" AND IsCustomerSend=".$postData['IsCustomerSend'];
        if(isset($postData['IsCustomerRead']) && $postData['IsCustomerRead'] >= 0) $query.=" AND IsCustomerRead=".$postData['IsCustomerRead'];
        if(isset($postData['IsStaffRead']) && $postData['IsStaffRead'] >= 0) $query.=" AND IsStaffRead=".$postData['IsStaffRead'];
        if($orderBy) $query .= " ORDER BY ChatId DESC";
        return $query;
    }

    public function updateReadMessage($customerId, $staffId, $isStaff = true){
        if($isStaff) $query = "UPDATE chats SET IsStaffRead = 1 WHERE CustomerId = ? AND StaffId = ? AND IsStaffRead = 0";
        else $query = "UPDATE chats SET IsCustomerRead = 1 WHERE CustomerId = ? AND StaffId = ? AND IsCustomerRead = 0";
        $this->db->query($query, array($customerId, $staffId));
    }

    public function getLastedChat($userId, $isUnRead = false, $isStaff = true){
        if($isStaff){
            if($isUnRead) $query = "SELECT * FROM chats WHERE ChatId IN(SELECT MAX(ChatId) FROM chats WHERE StaffId = ? AND IsCustomerSend = 1 AND IsStaffRead = 0 GROUP BY CustomerId, StaffId) ORDER BY ChatId DESC";
            else $query = "SELECT * FROM chats WHERE ChatId IN(SELECT MAX(ChatId) FROM chats WHERE StaffId = ? GROUP BY CustomerId, StaffId) ORDER BY ChatId DESC";
        }
        else{
            if($isUnRead) $query = "SELECT * FROM chats WHERE ChatId IN(SELECT MAX(ChatId) FROM chats WHERE CustomerId = ? AND IsCustomerSend = 0 AND IsCustomerRead = 0 GROUP BY CustomerId, StaffId) ORDER BY ChatId DESC";
            else $query = "SELECT * FROM chats WHERE ChatId IN(SELECT MAX(ChatId) FROM chats WHERE CustomerId = ? GROUP BY CustomerId, StaffId) ORDER BY ChatId DESC";
        }
        return $this->getByQuery($query, array($userId));
    }

    public function getListCountUnRead($userId, $isStaff = true){
        $retVal = array();
        if($isStaff) {
            $counts = $this->getByQuery('SELECT CustomerId, COUNT(ChatId) AS CountMsg FROM chats WHERE StaffId = ? AND IsCustomerSend = 1 AND IsStaffRead = 0 GROUP BY CustomerId', array($userId));
            foreach ($counts as $c) $retVal[$c['CustomerId']] = $c['CountMsg'];
        }
        else{
            $counts = $this->getByQuery('SELECT StaffId, COUNT(ChatId) AS CountMsg FROM chats WHERE CustomerId = ? AND IsCustomerSend = 0 AND IsCustomerRead = 0 GROUP BY StaffId', array($userId));
            foreach ($counts as $c) $retVal[$c['StaffId']] = $c['CountMsg'];
        }
        return $retVal;
    }
}