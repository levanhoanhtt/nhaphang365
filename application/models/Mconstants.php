<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mconstants extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public $roles = array(
        1 => 'Nhân Viên CSKH',
        2 => 'Nhân Viên Vận chuyển',
        3 => 'Quản lý chung',
        4 => 'Khách hàng',
        5 => 'Nhân viên Đặt hàng',
        6 => 'Kế toán',
        7 => 'Quản lý CSKH',
        8 => 'Quản lý Đặt hàng',
        9 => 'Quản lý Vận chuyển'
    );

    public $orderStatus = array(
        1 => 'Cần check', //KHÁCH HÀNG TẠO ĐƠN HÀNG. (KH - ĐƯỢC SỬA)
        2 => 'Đang check', //KHÁCH HÀNG ĐÃ GỬI ĐƠN HÀNG .( NHÂN VIÊN CSKH CHECK ĐƠN )
        3 => 'Đã check', //KHÁCH HÀNG NHẬN ĐƯỢC ĐƠN HÀNG PHẢN HỒI TỪ CSKH. (KH - ĐƯỢC SỬA)
        4 => 'Chờ duyệt', // KHÁCH HÀNG BẤM CHỐT ĐƠN HÀNG. - Chốt đơn
        9 => 'KT đã duyệt',
        5 => 'Đặt hàng', //KHÁCH HÀNG Đã CHUYỂN TIỀN , QL ĐA DUYỆT
        10 => 'Đang đặt hàng', //chỉ cần 1 shop có xuất hiện 1 STT là sẽ được chuyển sang phần đang đặt hàng
        6 => 'Đã đặt hàng', // ĐẶT HÀNG ĐÃ ĐẶT HÀNG THÀNH CÔNG. - đơn đó đã thanh toán xong hết
        7 => 'Không được duyệt', //chờ duyệt nhưng ko dc
        8 => 'Hủy đơn hàng' //QL hủy đơn
    );

    public $status = array(
        2 => 'Đã duyệt',
        1 => 'Chưa duyệt',
        3 => 'Không được duyệt',
        //4 => 'KT đã duyet
    );

    public $productStatus = array(
        1 => 'Hết hàng',
        2 => 'Có hàng',
        3 => 'Chờ duyệt tăng giá',
        4 => 'Chờ duyệt giảm giá',
        5 => 'Đã duyệt tăng giá',
        6 => 'Đã duyệt giảm giá',
        7 => 'Không duyệt tăng giá',
        8 => 'Không duyệt giảm giá'
    );

    public $complaintStatus = array(
        1 => 'Chưa xử lý',
        2 => 'Đặt hàng xử lý',
        3 => 'Chờ duyệt',
        4 => 'Khiếu nại không duyệt',
        5 => 'Đã duyệt',
        6 => 'Khách không đồng ý',
        7 => 'Khách đồng ý',
        8 => 'Chăm sóc xử lý'//'Khiếu nại ĐH gửi CS' //CS xu ly
    );

    public $complaintTypes = array(
        1 => 'Thiếu hàng',
        2 => 'Hàng lỗi hỏng',
        3 => 'Sai tùy chọn (màu, size)',
        4 => 'Khác'
    );

    public $complaintOwners = array(
        1 => 'Chăm sóc xử lý',
        2 => 'Đặt hàng xử lý',
        3 => 'Quản lý duyệt'
    );

    public $complaintSolutions = array(
        1 => 'Đền hàng',
        2 => 'Đòi tiền',
        3 => 'Đổi trả',
        4 => 'Giảm trừ'
    );

    public $complaintMsgTypes = array(
        1 => 'CS gửi ĐH',
        2 => 'ĐH gửi CS',
        3 => 'CS gửi QL',
        4 => 'ĐH gửi QL',
        5 => 'CS gửi KH',
        6 => 'ĐH gửi KH',
        7 => 'QL duyệt',
        8 => 'KH gửi'
    );

    public $trackingStatus = array(
        1 => 'Chưa có Mã vận đơn',
        2 => 'Đã có Mã vận đơn'
    );

    public $shopPaidStatus = array(
        1 => 'Chưa thanh toán',
        2 => 'Cần thanh toán',
        3 => 'Đã thanh toán'
    );

    public $shopStatus = array(
        2 => 'Có hàng',
        3 => 'Chờ duyệt tăng giá',
        4 => 'Chờ duyệt giảm giá',
        5 => 'Đã duyệt tăng giá',
        6 => 'Đã duyệt giảm giá',
        7 => 'Không duyệt tăng giá',
        8 => 'Không duyệt giảm giá'
    );

    public $notificationStatus = array(
        1 => 'Chưa xem',
        2 => 'Đã xem'
    );

    public $transactionRanges = array(
        1 => 'Đơn hàng',
        2 => 'Vận chuyển',
        3 => 'Giảm trừ Doanh Thu', //xoa, báo hết hàng, khiếu nại, giam gia
        4 => 'Tăng Doanh Thu' //tăng giá
    );

    public $transactionTypes = array(
        1 => 'Phiếu thu',
        2 => 'Phiếu chi',
        3 => 'Ghi nợ'
    );

    public $rechargeTypes = array(
        1 => 'Đòi lại tiền',
        2 => 'Nạp tệ',
        3 => 'Thanh toán'
    );

    public $transportReasons = array(
        1 => 'Phụ phí ngoài',
        2 => 'Xe ba gác',
        3 => 'Vào bến',
        4 => 'Ship NT'
    );

    public $commentStars = array(
        1 => 'Khá tồi',
        2 => 'Không tốt',
        3 => 'Tốt',
        4 => 'Rất tốt',
        5 => 'Xuất sắc'
    );

    public $genders = array(
        1 => 'Nam',
        2 => 'Nữ'
    );

    /*public $articleTypes = array(
        1 => 'Page cố định',
        2 => 'Bài viết'
    );
    public $categoryTypes = array(
        1 => 'Chuyên mục shop',
        2 => 'Chuyên mục Bài viết',
        3 => 'Sản phẩm Tao bao',
        4 => 'Sản phẩm Tmail',
        5 => 'Sản phẩm 1688',
    );*/

    public $labelCss = array(
        1 => 'label label-default',
        2 => 'label label-success',
        3 => 'label label-warning',
        4 => 'label label-danger',
        5 => 'label label-default',
        6 => 'label label-success',
        7 => 'label label-warning',
        8 => 'label label-danger',
        9 => 'label label-default',
        10 => 'label label-success',
        11 => 'label label-warning',
        12 => 'label label-danger'

    );

    public function selectConstants($key, $selectName, $itemId = 0, $isAll = false, $txtAll = 'Tất cả'){
        $obj = $this->$key;
        if($obj) {
            echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'">';
            if($isAll) echo '<option value="0">'.$txtAll.'</option>';
            foreach($obj as $i => $v){
                if($itemId == $i) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$v.'</option>';
            }
            echo "</select>";
        }
    }

    public function selectObject($listObj, $objKey, $objValue, $selectName, $objId = 0, $isAll = false, $txtAll = "", $selectClass = '', $attrSelect = ''){
        $id = str_replace('[]', '', lcfirst($selectName));
        echo '<select class="form-control'.$selectClass.'" name="'.$selectName.'" id="'.$id.'"'.$attrSelect.'>';
        if($isAll){
            if(empty($txtAll)) echo '<option value="0">Tất cả</option>';
            else echo '<option value="0">'.$txtAll.'</option>';
        }
        $isSelectMutiple = is_array($objId);
        foreach($listObj as $obj){
            $selected = '';
            if(!$isSelectMutiple) {
                if ($obj[$objKey] == $objId) $selected = ' selected="selected"';
            }
            elseif(in_array($obj[$objKey], $objId)) $selected = ' selected="selected"';
            echo '<option value="'.$obj[$objKey].'"'.$selected.'>'.$obj[$objValue].'</option>';
        }
        echo '</select>';
    }

    public function selectNumber($start, $end, $selectName, $itemId = 0, $asc = false, $attrSelect = ''){
        echo '<select class="form-control" name="'.$selectName.'" id="'.lcfirst($selectName).'"'.$attrSelect.'>';
        if($asc){
            for($i = $start; $i <= $end; $i++){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        else{
            for($i = $end; $i >= $start; $i--){
                if($i == $itemId) $selected = ' selected="selected"';
                else $selected = '';
                echo '<option value="'.$i.'"'.$selected.'>'.$i.'</option>';
            }
        }
        echo '</select>';
    }

    public function getObjectValue($listObj, $objKey, $objValue, $objKeyReturn){
        foreach($listObj as $obj){
            if($obj[$objKey] == $objValue) return $obj[$objKeyReturn];
        }
        return '';
    }
}