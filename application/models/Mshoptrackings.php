<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mshoptrackings extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "shoptrackings";
        $this->_primary_key = "ShopTrackingId";
    }

    public function getByGroup($orderId){
        $retVal = array();
        $listShopTrackings = $this->getBy(array('OrderId' => $orderId));
        foreach($listShopTrackings as $st) $retVal[$st['ShopId']][] = $st;
        return $retVal;
    }

    public function update($postData, $shopTrackingId){
        $this->db->trans_begin();
        if($shopTrackingId > 0){
            //log
        }
        $shopTrackingId = $this->save($postData, $shopTrackingId, array('UpdateUserId', 'UpdateDateTime'));
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $shopTrackingId;
        }
    }

    public function getShopCount($orderId){
        $shopCounts = $this->getByQuery('SELECT DISTINCT ShopId FROM shoptrackings WHERE OrderId = ?', array($orderId));
        return count($shopCounts);
    }
}