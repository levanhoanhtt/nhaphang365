<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransports extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transports";
        $this->_primary_key = "TransportId";
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM transports WHERE StatusId > 0" . $this->buildQuery($postData, true);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $orderBy = false){
        $query = '';
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND CustomerId=".$postData['CustomerId'];
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['WarehouseId']) && $postData['WarehouseId'] > 0) $query.=" AND WarehouseId=".$postData['WarehouseId'];
        if(isset($postData['MoneySourceId']) && $postData['MoneySourceId'] > 0) $query.=" AND MoneySourceId=".$postData['MoneySourceId'];
        if(isset($postData['TransportReasonId']) && $postData['TransportReasonId'] > 0) $query.=" AND TransportReasonId=".$postData['TransportReasonId'];
        if(isset($postData['TransportUserId']) && $postData['TransportUserId'] > 0) $query.=" AND TransportUserId=".$postData['TransportUserId'];
        if(isset($postData['TransportTypeId']) && $postData['TransportTypeId'] > 0) $query.=" AND TransportTypeId=".$postData['TransportTypeId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND PaidDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND PaidDateTime <= '{$postData['EndDate']}'";
        if($orderBy) $query .= " ORDER BY TransportId DESC";
        return $query;
    }

    public function update($postData, $transportId){
        $flag = $transportId > 0;
        $updateDateTime = $flag ?  $postData['UpdateDateTime'] : $postData['CrDateTime'];
        $this->load->model('Mtransactions');
        $this->load->model('Mcustomerbalances');
        $this->load->model('Mnotifications');
        $isAddTransaction = true;
        if($postData['StatusId'] == 3 && $flag){
            $postData['StatusId'] = 2;
            $isAddTransaction = false;
        }
        $this->db->trans_begin();
        $fieldNull = array();
        if(!$flag) $fieldNull = array('Comment', 'UpdateUserId', 'UpdateDateTime');
        $transportId = $this->save($postData, $transportId, $fieldNull);
        if($flag){ //update staus
            if($postData['StatusId'] == 2 && $isAddTransaction){
                $transactionTypeId = 1;
                $transactionData = array(
                    'CustomerId' => $postData['CustomerId'],
                    'OrderId' => 0,
                    'ComplaintId' => 0,
                    'StatusId' => 2,
                    'TransactionTypeId' => $transactionTypeId,
                    'TransactionRangeId' => 2,
                    'WarehouseId' => $postData['WarehouseId'],
                    'MoneySourceId' => $postData['MoneySourceId'],
                    'Comment' => 'Thanh toán tiền vận chuyển #' . $transportId,
                    'Confirm' => '',
                    'PaidTQ' => 0,
                    'ExchangeRate' => 1,
                    'PaidVN' => $postData['PaidVN'],
                    'PaidDateTime' => $updateDateTime,
                    'Balance' => $this->Mtransactions->getNewBalance($postData['CustomerId'], $transactionTypeId, $postData['PaidVN']),
                    'CrUserId' => isset($postData['CrUserId']) ? $postData['CrUserId'] : $postData['UpdateUserId'],
                    'CrDateTime' => $updateDateTime
                );
                $this->Mtransactions->update($transactionData, 0);
                $notificationData = array(
                    'CustomerId' => $postData['CustomerId'],
                    'UserId' => isset($postData['UpdateUserId']) ? $postData['UpdateUserId'] : $postData['CrUserId'],
                    'RoleId' => 3,
                    'OrderId' => 0,
                    'ProductId' => 0,
                    'ComplaintId' => 0,
                    'Message' => 'Hệ thống đã nhận được khoản tiền '.priceFormat($postData['PaidVN']).' VNĐ.',
                    'IsCustomerSend' => 0,
                    'NotificationStatusId' => 1,
                    'LinkTo' => 'customer/transaction',
                    'CrDateTime' => $updateDateTime
                );
                $this->Mnotifications->save($notificationData);
            }
        }
        else{ //add moi
            if($postData['StatusId'] == 1){
                $transactionTypeId = 3;
                $transactionData = array(
                    'CustomerId' => $postData['CustomerId'],
                    'OrderId' => 0,
                    'ComplaintId' => 0,
                    'StatusId' => 2,
                    'TransactionTypeId' => $transactionTypeId,
                    'TransactionRangeId' => 2,
                    'WarehouseId' => $postData['WarehouseId'],
                    'MoneySourceId' => $postData['MoneySourceId'],
                    'Comment' => 'Ghi nợ tiền vận chuyển #' . $transportId,
                    'Confirm' => '',
                    'PaidTQ' => 0,
                    'ExchangeRate' => 1,
                    'PaidVN' => $postData['PaidVN'],
                    'PaidDateTime' => $updateDateTime,
                    'Balance' => $this->Mtransactions->getNewBalance($postData['CustomerId'], $transactionTypeId, $postData['PaidVN']),
                    'CrUserId' => $postData['CrUserId'],
                    'CrDateTime' => $postData['CrDateTime']
                );
                $this->Mtransactions->update($transactionData, 0);
            }
            elseif($postData['StatusId'] == 2){
                $curentBalance = $this->Mcustomerbalances->getCurentBalance($postData['CustomerId']);
                $transactionData = array(
                    array(
                        'CustomerId' => $postData['CustomerId'],
                        'OrderId' => 0,
                        'ComplaintId' => 0,
                        'StatusId' => 2,
                        'TransactionTypeId' => 3,
                        'TransactionRangeId' => 2,
                        'WarehouseId' => $postData['WarehouseId'],
                        'MoneySourceId' => $postData['MoneySourceId'],
                        'Comment' => 'Ghi nợ tiền vận chuyển #' . $transportId,
                        'Confirm' => '',
                        'PaidTQ' => 0,
                        'ExchangeRate' => 1,
                        'PaidVN' => $postData['PaidVN'],
                        'PaidDateTime' => $updateDateTime,
                        'Balance' => $curentBalance - $postData['PaidVN'],
                        'CrUserId' => $postData['CrUserId'],
                        'CrDateTime' => $postData['CrDateTime']
                    ),
                    array(
                        'CustomerId' => $postData['CustomerId'],
                        'OrderId' => 0,
                        'ComplaintId' => 0,
                        'StatusId' => 2,
                        'TransactionTypeId' => 1,
                        'TransactionRangeId' => 2,
                        'WarehouseId' => $postData['WarehouseId'],
                        'MoneySourceId' => $postData['MoneySourceId'],
                        'Comment' => 'Thanh toàn tiền vận chuyển #' . $transportId,
                        'Confirm' => '',
                        'PaidTQ' => 0,
                        'ExchangeRate' => 1,
                        'PaidVN' => $postData['PaidVN'],
                        'PaidDateTime' => $updateDateTime,
                        'Balance' => $curentBalance,
                        'CrUserId' => $postData['CrUserId'],
                        'CrDateTime' => $postData['CrDateTime']
                    )
                );
                $this->db->insert_batch('transactions', $transactionData);
                $notificationData = array(
                    'CustomerId' => $postData['CustomerId'],
                    'UserId' => isset($postData['UpdateUserId']) ? $postData['UpdateUserId'] : $postData['CrUserId'],
                    'RoleId' => 3,
                    'OrderId' => 0,
                    'ProductId' => 0,
                    'ComplaintId' => 0,
                    'Message' => 'Hệ thống đã nhận được khoản tiền '.priceFormat($postData['PaidVN']).' VNĐ.',
                    'IsCustomerSend' => 0,
                    'NotificationStatusId' => 1,
                    'LinkTo' => 'customer/transaction',
                    'CrDateTime' => $updateDateTime
                );
                $this->Mnotifications->save($notificationData);
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return $transportId;
        }
    }
}