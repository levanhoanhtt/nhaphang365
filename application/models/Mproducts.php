<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mproducts extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "products";
        $this->_primary_key = "ProductId";
    }

    public function update($postData, $productId, $notificationData = array(), $transactionData = array(), $staffs, $isChangeBalance = false){
        $this->db->trans_begin();
        $this->db->where('ProductId', $productId);
        if(!isset($postData['RemoveConfirm']) || empty($postData['RemoveConfirm'])){
            $this->db->set('RemoveConfirm', null);
            unset($postData['RemoveConfirm']);
        }
        $this->db->update('products', $postData);
        $statusFlag = in_array($postData['ProductStatusId'], array(1, 5, 6, 7, 8));
        $notificationInserts = array($notificationData);
        $userIdOld = $notificationData['UserId'];
        if(isset($staffs['CareStaffId']) && $staffs['CareStaffId'] > 0 && $staffs['CareStaffId'] != $userIdOld){
            $notificationData['UserId'] = $staffs['CareStaffId'];
            $notificationData['RoleId'] = 1;
            $notificationData['IsCustomerSend'] = 1;
            $notificationInserts[] = $notificationData;
        }
        if(isset($staffs['OrderUserId']) && $staffs['OrderUserId'] > 0 && $staffs['OrderUserId'] != $userIdOld){
            $notificationData['UserId'] = $staffs['OrderUserId'];
            $notificationData['RoleId'] = 5;
            $notificationData['IsCustomerSend'] = 1;
            $notificationInserts[] = $notificationData;
        }
        if($statusFlag){
            $notificationData['UserId'] = 9;
            $notificationData['RoleId'] = 6;
            $notificationData['IsCustomerSend'] = 1;
            $notificationInserts[] = $notificationData;
        }
        $this->db->insert_batch('notifications', $notificationInserts);
        if($statusFlag && !empty($transactionData)){
            $this->load->model('Mtransactions');
            $transactionId = $this->Mtransactions->update($transactionData, 0);
            if($isChangeBalance && $transactionId > 0){
                $this->load->model('Mcustomerbalances');
                $balanceData = array(
                    'CustomerId' => $transactionData['CustomerId'],
                    'TransactionId' => $transactionId,
                    'Balance' => $transactionData['Balance'],
                    'IsLast' => 1,
                    'CrDateTime' => $transactionData['CrDateTime']
                );
                $this->Mcustomerbalances->insert($balanceData);
            }
        }
        if(in_array($postData['ProductStatusId'], array(1, 7, 8))){
            $shopId = $this->getFieldValue(array('ProductId' => $productId), 'ShopId', 0);
            if($shopId > 0) {
                $count = $this->getCountProductActive($shopId);
                if($count == 0) {
                    //save logs
                    $this->db->query('UPDATE shops SET ShipTQOldNew = ShipTQ, ShipTQ = 0, ShopStatusId = 7, ShipTQReason = ?, UpdateUserId = ?, UpdateDateTime = NOW() WHERE ShopId = ?', array('Shop hết sản phẩm', $notificationData['CustomerId'], $shopId));
                }
                //update shop tracking
                $this->load->model('Mshops');
                $this->load->model('Mshoptrackings');
                $orderId = $notificationData['OrderId'];
                $shopCount1 = $this->Mshops->getShopCount($orderId);
                $shopCount2 = $this->Mshoptrackings->getShopCount($orderId);
                if($shopCount1 == $shopCount2) {
                    $this->load->model('Morders');
                    $this->Morders->updateField(array('TrackingStatusId' => 2), $orderId);
                }
            }
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function getSumCost($orderId){
        $costs = $this->getByQuery('SELECT SUM(Quantity * Cost) AS SumCost FROM products WHERE OrderId = ? AND ProductStatusId = ?', array($orderId, STATUS_ACTIVED));
        if(!empty($costs)) return $costs[0]['SumCost'];
        return 0;
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM products WHERE ProductStatusId > 4" . $this->buildQuery($postData) . " ORDER BY UpdateDateTime DESC";
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['OrderCode']) && $postData['OrderCode'] > 0) $query.=" AND OrderId=".$postData['OrderCode'];
        if(isset($postData['ProductStatusId']) && $postData['ProductStatusId'] > 0) $query.=" AND ProductStatusId=".$postData['ProductStatusId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND UpdateDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND UpdateDateTime <= '{$postData['EndDate']}'";
        return $query;
    }

    public function getListInfo($orderId){
        $shopData = $this->getByQuery('SELECT SUM(ShipTQ) AS SumShipTQ, COUNT(ShopId) AS ShopCount FROM shops WHERE OrderId = ? AND ShopStatusId > 0', array($orderId));
        if(empty($shopData)) $shopData = array('SumShipTQ' => 0, 'ShopCount' => 0);
        else{
            $shopData = $shopData[0];
            if(empty($shopData['SumShipTQ'])) $shopData['SumShipTQ'] = 0;
            if(empty($shopData['ShopCount'])) $shopData['ShopCount'] = 0;
        }
        $productData = $this->getByQuery('SELECT SUM(Quantity * Cost) AS PaidTQ, SUM(Quantity) AS ProductCount FROM products WHERE OrderId = ? AND ProductStatusId > 0', array($orderId));
        if(empty($productData)) $productData = array('PaidTQ' => 0, 'ProductCount' => 0);
        else{
            $productData = $productData[0];
            if(empty($productData['PaidTQ'])) $productData['PaidTQ'] = 0;
            if(empty($productData['ProductCount'])) $productData['ProductCount'] = 0;
        }
        return array(
            'ShipTQ' => $shopData['SumShipTQ'],
            'ShopCount' => $shopData['ShopCount'],
            'PaidTQ' => $productData['PaidTQ'],
            'ProductCount' => $productData['ProductCount']
        );
    }

    private function getCountProductActive($shopId){
        $shopCounts = $this->getByQuery('SELECT DISTINCT ProductId FROM products WHERE ShopId = ? AND ProductStatusId NOT IN(0, 1, 7, 8)', array($shopId));
        return count($shopCounts);
    }
}
