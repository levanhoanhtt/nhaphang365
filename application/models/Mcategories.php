<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcategories extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "categories";
        $this->_primary_key = "CategoryId";
    }

    public function getListChild($parentCategoryId, $select = ''){
        return $this->getBy(array('StatusId' => STATUS_ACTIVED, 'ParentCategoryId' => $parentCategoryId), false, 'DisplayOrder', $select, 0, 0, 'asc');
    }

    public function getByCategoryTypeId($categoryTypeId){
        $retVal = array();
        if(is_array($categoryTypeId)) $retVal = $this->getByQuery('SELECT * FROM categories WHERE CategoryTypeId IN('.implode(',', $categoryTypeId).') ORDER BY DisplayOrder ASC');
        else $retVal = $this->getBy(array('CategoryTypeId' => $categoryTypeId), false, 'DisplayOrder', '', 0, 0, 'asc');
        return $retVal;
    }

    public function update($postData, $categoryId){
        $this->db->trans_begin();
        $this->db->set('DisplayOrder', 'DisplayOrder+1', false);
        $where = array('StatusId' => STATUS_ACTIVED, 'CategoryTypeId' => $postData['CategoryTypeId'], 'DisplayOrder>=' => $postData['DisplayOrder']);
        if($postData['ParentCategoryId'] > 0) $where['ParentCategoryId'] = $postData['ParentCategoryId'];
        $this->db->where($where);
        $this->db->update('categories');
        if($categoryId > 0 && count($postData) == 4) $this->save($postData, $categoryId);
        else $categoryId = $this->save($postData, $categoryId, array('CategoryNameTQ', 'CategoryImage', 'UpdateUserId', 'UpdateDateTime'));
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return $categoryId;
        }
    }

    public function checkHasChild($categoryId){
        $cates = $this->getBy(array('ParentCategoryId' => $categoryId, 'StatusId' => STATUS_ACTIVED), false, 'CategoryId');
        return empty($cates);
    }
}
