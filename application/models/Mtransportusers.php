<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransportusers extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transportusers";
        $this->_primary_key = "TransportUserId";
    }
}
