<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcategoryarticles extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "categoryarticles";
        $this->_primary_key = "CategoryArticleId";
    }

    public function getListCategoryIds($articleId){
        $retVal = array();
        $acs = $this->getBy(array('ArticleId' => $articleId));
        foreach($acs as $ac) $retVal[] = $ac['CategoryId'];
        return $retVal;
    }
}