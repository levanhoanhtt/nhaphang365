<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mnotifications extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "notifications";
        $this->_primary_key = "NotificationId";
    }

    public function getCount($postData){
        $query = "NotificationStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM notifications WHERE NotificationStatusId > 0" . $this->buildQuery($postData).' ORDER BY NotificationId DESC';
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData){
        $query = '';
        if(isset($postData['CustomerId']) && $postData['CustomerId'] > 0) $query.=" AND CustomerId=".$postData['CustomerId'];
        if(isset($postData['UserId']) && $postData['UserId'] > 0) $query.=" AND UserId IN(0, {$postData['UserId']})";
        if(isset($postData['RoleId']) && $postData['RoleId'] > 0) $query.=" AND RoleId=".$postData['RoleId'];
        if(isset($postData['OrderId']) && $postData['OrderId'] > 0) $query.=" AND OrderId=".$postData['OrderId'];
        if(isset($postData['ProductId']) && $postData['ProductId'] > 0) $query.=" AND ProductId=".$postData['ProductId'];
        if(isset($postData['ComplaintId']) && $postData['ComplaintId'] > 0) $query.=" AND ComplaintId=".$postData['ComplaintId'];
        if(isset($postData['IsCustomerSend']) && $postData['IsCustomerSend'] >= 0) $query.=" AND IsCustomerSend=".$postData['IsCustomerSend'];
        if(isset($postData['NotificationStatusId']) && $postData['NotificationStatusId'] > 0) $query.=" AND NotificationStatusId=".$postData['NotificationStatusId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query.=" AND CrDateTime>='{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query.=" AND CrDateTime<='{$postData['EndDate']}'";
        return $query;
    }

    public function changeAllStaus($userId, $roleId, $notificationStatusId){
        if($roleId == 4) $this->db->update('notifications', array('NotificationStatusId' => $notificationStatusId), array('NotificationStatusId > ' => 0, 'NotificationStatusId != ' => $notificationStatusId));
        else $this->db->query('UPDATE notifications SET NotificationStatusId = ? WHERE NotificationStatusId > 0 AND NotificationStatusId != ? AND ((UserId = 0 AND RoleId = ?) OR UserId = ?)', array($notificationStatusId, $notificationStatusId, $roleId, $userId));
    }

    public function statisticCounts($postData, $isStatisticAll = false){
        $retVal = array();
        $retVal[0] = 0;//total
        $notificationStatus = $this->Mconstants->notificationStatus;
        foreach($notificationStatus as $i => $v) $retVal[$i] = 0;
        $flag = false;
        $query = "SELECT ";
        foreach($notificationStatus as $i => $v){
            $flag = true;
            $query .= "COUNT(CASE WHEN NotificationStatusId = {$i} THEN 1 END) AS Count{$i},";
        }
        if($isStatisticAll){
            $flag = true;
            $query .= "COUNT(CASE WHEN NotificationStatusId > 0 THEN 1 END) AS Total FROM notifications WHERE 1=1";
        }
        else{
            $query = substr($query, 0, strlen($query) - 1);
            $query .= " FROM notifications WHERE 1=1";
        }
        $query.=$this->buildQuery($postData);
        if($flag) {
            $counts = $this->getByQuery($query);
            if (!empty($counts)) {
                $counts = $counts[0];
                foreach ($counts as $label => $value) {
                    if ($label == 'Total') $retVal[0] = $value;
                    foreach ($notificationStatus as $i => $v) {
                        if ($label == 'Count' . $i) $retVal[$i] = $value;
                    }
                }
            }
        }
        return $retVal;
    }
}
