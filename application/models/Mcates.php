<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcates extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "cates";
        $this->_primary_key = "CateId";
    }

    public function getList(){
        return $this->get(0, false, "DisplayOrder", '', 0, 0, 'asc');
    }

    public function update($postData, $cateId){
        $this->db->trans_begin();
        $this->db->set('DisplayOrder', 'DisplayOrder+1', false);
        $this->db->where('DisplayOrder>=', $postData['DisplayOrder']);
        $this->db->where('CateTypeId=', $postData['CateTypeId']);
        $this->db->update('cates');
        $cateId = $this->save($postData, $cateId);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $cateId;
        }
    }
}
