<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mservicefees extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "servicefees";
        $this->_primary_key = "ServiceFeeId";
    }

    function update($serviceFeeId, $postData, $isUpdateField = false){
        $fieldNull = array();
        if(!$isUpdateField) $fieldNull = array('UpdateUserId', 'UpdateDateTime');
        $this->load->model('Mservicefeelogs');
        $this->db->trans_begin();
        if($serviceFeeId > 0){
            $this->Mservicefeelogs->insert($serviceFeeId, $postData['UpdateUserId'], $postData['UpdateDateTime']);
            $serviceFeeId = $this->save($postData, $serviceFeeId, $fieldNull);
        }
        else $serviceFeeId = $this->save($postData, 0, $fieldNull);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $serviceFeeId;
        }
    }
}
