<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mcomplaintmsgs extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "complaintmsgs";
        $this->_primary_key = "ComplaintMsgId";
    }
}
