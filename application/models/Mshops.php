<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mshops extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "shops";
        $this->_primary_key = "ShopId";
    }

    public function getCount($postData){
        $query = "ShopStatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM shops WHERE ShopStatusId > 0" . $this->buildQuery($postData, true);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $orderBy = false){
        $query = '';
        if(isset($postData['OrderId']) && $postData['OrderId'] > 0) $query.=" AND OrderId=".$postData['OrderId'];
        if(isset($postData['ShopCode']) && !empty($postData['ShopCode'])) $query.=" AND ShopCode='{$postData['ShopCode']}'";
        if(isset($postData['CategoryId']) && $postData['CategoryId'] > 0) $query.=" AND CategoryId=".$postData['CategoryId'];
        if(isset($postData['ShopStatusId']) && $postData['ShopStatusId'] > 0) $query.=" AND ShopStatusId=".$postData['ShopStatusId'];
        if(isset($postData['IsTransportSlow']) && $postData['IsTransportSlow'] >= 0) $query.=" AND IsTransportSlow=".$postData['IsTransportSlow'];
        if(isset($postData['OrderAccountId']) && $postData['OrderAccountId'] > 0) $query.=" AND OrderAccountId IN(SELECT OrderAccountId FROM shopnumbers WHERE OrderAccountId = {$postData['OrderAccountId']})";
        if(isset($postData['OrderUserId']) && $postData['OrderUserId'] > 0) $query.=" AND OrderId IN(SELECT OrderId FROM orders WHERE OrderUserId = {$postData['OrderUserId']})";
        if($orderBy) $query .= " ORDER BY OrderId DESC";
        return $query;
    }

    public function update($p, $orderId, $crUserId, $crDateTime){
        $shopData = array(
            'ShopCode' => $p['ShopCode'],
            'ShopName' => $p['ShopName'],
            'ShopUrl' => $p['ShopUrl'],
            'OrderId' => $orderId,
            'CategoryId' => $p['CategoryId'],
            'ShipTQ' => replacePrice($p['ShipTQ']),
            'ShipTQOldNew' => 0,
            'ShopStatusId' => 2,
            'IsTransportSlow' => 0,
            'Comment' => $p['ShopComment'],
            'Feedback' => $p['ShopFeedback']
        );
        $shopId = $this->getFieldValue(array('OrderId' => $orderId, 'ShopCode' => $p['ShopCode']), 'ShopId', 0);
        if($shopId > 0){
            $shopData['UpdateUserId'] = $crUserId;
            $shopData['UpdateDateTime'] = $crDateTime;
            //save logs
        }
        else{
            $shopData['CrUserId'] = $crUserId;
            $shopData['CrDateTime'] = $crDateTime;
        }
        return $this->save($shopData, $shopId, array('Comment', 'Feedback', 'ShipTQReason', 'ShipTQConfirm', 'UpdateUserId', 'UpdateDateTime'));
    }

    public function updateField($postData, $shopId){
        $this->db->trans_begin();
        $this->save($postData, $shopId);
        //save logs

        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function changeShipTQ($shopData, $shopId, $notificationData = array(), $transactionData = array(), $staffs, $isChangeBalance = false){
        $this->db->trans_begin();
        $this->save($shopData, $shopId);
        //save logs

        $statusFlag = in_array($shopData['ShopStatusId'], array(5, 6, 7, 8));
        $notificationInsers = array($notificationData);
        $userIdOld = $notificationData['UserId'];
        if(isset($staffs['CareStaffId']) && $staffs['CareStaffId'] > 0 && $staffs['CareStaffId'] != $userIdOld){
            $notificationData['UserId'] = $staffs['CareStaffId'];
            $notificationData['RoleId'] = 1;
            $notificationData['IsCustomerSend'] = 1;
            $notificationInserts[] = $notificationData;
        }
        if(isset($staffs['OrderUserId']) && $staffs['OrderUserId'] > 0 && $staffs['OrderUserId'] != $userIdOld){
            $notificationData['UserId'] = $staffs['OrderUserId'];
            $notificationData['RoleId'] = 5;
            $notificationData['IsCustomerSend'] = 1;
            $notificationInserts[] = $notificationData;
        }
        if($statusFlag){
            $notificationData['UserId'] = 9;
            $notificationData['RoleId'] = 6;
            $notificationData['IsCustomerSend'] = 1;
            $notificationInsers[] = $notificationData;
        }
        $this->db->insert_batch('notifications', $notificationInsers);
        if($statusFlag && !empty($transactionData)){
            $this->load->model('Mtransactions');
            $transactionId = $this->Mtransactions->update($transactionData, 0);
            if($isChangeBalance && $transactionId > 0){
                $this->load->model('Mcustomerbalances');
                $balanceData = array(
                    'CustomerId' => $transactionData['CustomerId'],
                    'TransactionId' => $transactionId,
                    'Balance' => $transactionData['Balance'],
                    'IsLast' => 1,
                    'CrDateTime' => $transactionData['CrDateTime']
                );
                $this->Mcustomerbalances->insert($balanceData);
            }
        }
        if($shopData['ShopStatusId'] == 7 || $shopData['ShopStatusId'] == 8){
            $this->db->query('UPDATE products SET QuantityOldNew = Quantity, Quantity = 0, ProductStatusId = 1, UpdateUserId = ?, UpdateDateTime = NOW()  WHERE OrderId = ? AND ShopId = ?', array($notificationData['CustomerId'], $notificationData['OrderId'], $shopId));
            //save logs
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }

    public function getShopCount($orderId){
        $shopCounts = $this->getByQuery('SELECT DISTINCT ShopId FROM shops WHERE OrderId = ? AND ShopStatusId NOT IN(0, 7, 8)', array($orderId));
        return count($shopCounts);
    }

    public function getSumCost($shopId){
        $retVal = 0;
        $sumCosts = $this->getByQuery('SELECT SUM(Quantity * Cost) AS SumCost FROM products WHERE ShopId = ? UNION ALL SELECT ShipTQ FROM shops WHERE ShopId = ?', array($shopId, $shopId));
        foreach($sumCosts as $sc) $retVal += $sc['SumCost'];
        return $retVal;
    }
}