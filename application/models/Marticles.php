<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marticles extends MY_Model
{

    function __construct()
    {
        parent::__construct();
        $this->_table_name = "articles";
        $this->_primary_key = "ArticleId";
    }

    public function getCount($postData){
        $query = "StatusId > 0" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1, $orderBy = ''){
        $query = "SELECT * FROM articles WHERE StatusId > 0" . $this->buildQuery($postData, $orderBy);
        if ($perPage > 0) {
            $from = ($page - 1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $orderBy = ''){
        $query = '';
        if(isset($postData['ArticleTitle']) && !empty($postData['ArticleTitle'])) $query.=" AND ArticleTitle LIKE '%{$postData['ArticleTitle']}%'";
        if(isset($postData['StatusId']) && $postData['StatusId'] > 0) $query.=" AND StatusId=".$postData['StatusId'];
        if(isset($postData['ArticleTypeId']) && $postData['ArticleTypeId'] > 0) $query.=" AND ArticleTypeId=".$postData['ArticleTypeId'];
        if(isset($postData['CategoryId']) && $postData['CategoryId'] > 0) $query.=" AND ArticleId IN(SELECT ArticleId FROM categoryarticles WHERE CategoryId={$postData['CategoryId']})";
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND PublishDateTime >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND PublishDateTime <= '{$postData['EndDate']}'";
        if($orderBy == 'DisplayOrder') $query .= ' ORDER BY DisplayOrder ASC';
        elseif(!empty($orderBy)) $query .= " ORDER BY {$orderBy} DESC";
        return $query;
    }

    public function update($postData, $articleId = 0, $isUpdateDisplayOrder = 0, $categoryIds = array()){
        $this->db->trans_begin();
        if($postData['ArticleTypeId'] == 3 && $articleId > 0) $this->db->delete('categoryarticles', array('ArticleId' => $articleId));
        if($isUpdateDisplayOrder == 1) {
            $this->db->set('DisplayOrder', 'DisplayOrder+1', false);
            $this->db->where(array('StatusId' => STATUS_ACTIVED, 'ArticleTypeId' => $postData['ArticleTypeId'], 'DisplayOrder>=' => $postData['DisplayOrder']));
            $this->db->update('articles');
        }
        $articleId = $this->save($postData, $articleId, array('ArticleImage', 'UpdateUserId', 'UpdateDateTime'));
        if($articleId > 0 && !empty($categoryIds)){
            $categoryArticles = array();
            foreach($categoryIds as $categoryId){
                if($categoryId > 0) $categoryArticles[] = array('CategoryId' => $categoryId, 'ArticleId' => $articleId);
            }
            if(!empty($categoryArticles)) $this->db->insert_batch('categoryarticles', $categoryArticles);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $articleId;
        }
    }
}
