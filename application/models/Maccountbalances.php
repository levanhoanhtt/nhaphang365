<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maccountbalances extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "accountbalances";
        $this->_primary_key = "AccountBalanceId";
    }

    public function getCurentBalance($bankAccountId){
        return $this->getFieldValue(array('BankAccountId' => $bankAccountId, 'IsLast' => 1), 'Balance', 0);
    }

    public function insert($postData){
        $this->db->trans_begin();
        $this->db->update('accountbalances', array('IsLast' => 0), array('BankAccountId' => $postData['BankAccountId'], 'IsLast' => 1));
        $this->save($postData);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return true;
        }
    }
}
