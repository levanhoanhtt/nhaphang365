<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mrecharges extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "recharges";
        $this->_primary_key = "RechargeId";
    }

    public function getCount($postData){
        $query = "1=1" . $this->buildQuery($postData);
        return $this->countRows($query);
    }

    public function search($postData, $perPage = 0, $page = 1){
        $query = "SELECT * FROM recharges WHERE 1=1" . $this->buildQuery($postData, true);
        if($perPage > 0) {
            $from = ($page-1) * $perPage;
            $query .= " LIMIT {$from}, {$perPage}";
        }
        return $this->getByQuery($query);
    }

    private function buildQuery($postData, $orderBy = false){
        $query = '';
        if(isset($postData['BankAccountId']) && $postData['BankAccountId'] > 0) $query.=" AND BankAccountId=".$postData['BankAccountId'];
        if(isset($postData['RechargeTypeId']) && $postData['RechargeTypeId'] > 0) $query.=" AND RechargeTypeId=".$postData['RechargeTypeId'];
        if(isset($postData['BeginDate']) && !empty($postData['BeginDate'])) $query .= " AND RechargeDate >= '{$postData['BeginDate']}'";
        if(isset($postData['EndDate']) && !empty($postData['EndDate'])) $query .= " AND RechargeDate <= '{$postData['EndDate']}'";
        if($orderBy) $query .= " ORDER BY RechargeId DESC";
        return $query;
    }

    public function insert($postData){
        $this->load->model('Maccountbalances');
        $this->db->trans_begin();
        $postData['Balance'] = $this->getNewBalance($postData['BankAccountId'], $postData['RechargeTypeId'],  $postData['PaidTQ']);
        $rechargeId = $this->save($postData, 0, array('Comment'));
        if($rechargeId > 0){
            $balanceData = array(
                'BankAccountId' => $postData['BankAccountId'],
                'RechargeId' => $rechargeId,
                'Balance' => $postData['Balance'],
                'IsLast' => 1,
                'CrDateTime' => $postData['CrDateTime']
            );
            $this->Maccountbalances->insert($balanceData);
        }
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return false;
        }
        else{
            $this->db->trans_commit();
            return $rechargeId;
        }
    }

    private function getNewBalance($bankAccountId, $rechargeTypeId,  $paidTQ){
        $curentBalance = $this->Maccountbalances->getCurentBalance($bankAccountId);
        if($rechargeTypeId == 1) $balance = $curentBalance + $paidTQ; //doi tien
        else if($rechargeTypeId == 2) $balance = $curentBalance + $paidTQ; //nap te
        else if($rechargeTypeId == 3) $balance = $curentBalance - $paidTQ; //thanh toan
        else $balance = 0;
        return $balance;
    }
}
