<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mactions extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "actions";
        $this->_primary_key = "ActionId";
    }

    public function getByUserId($userId, $roleId = 0){
        return $this->getBy(array('StatusId' => STATUS_ACTIVED), false, 'DisplayOrder');
    }

    public function checkAccess($listActions, $actionUrl){
        /*foreach($listActions as $action){
            if($action['ActionUrl'] == $actionUrl) return true;
        }
        return false;*/
        return true;
    }

    public function getHierachy(){
        $retVal = array();
        $listActions = $this->getBy(array('StatusId' => 2), false, 'DisplayOrder');
        $listActions1 = $listActions2 = $listActions3 = array();
        foreach($listActions as $a){
            if($a['ActionLevel'] == 1) $listActions1[]=$a;
            elseif($a['ActionLevel'] == 2) $listActions2[]=$a;
            elseif($a['ActionLevel'] == 3) $listActions3[]=$a;
        }
        foreach($listActions1 as $a1){
            $retVal[] = $a1;
            foreach ($listActions2 as $a2) {
                if($a2['ParentActionId'] == $a1['ActionId']){
                    $retVal[]=$a2;
                    foreach ($listActions3 as $a3) {
                        if($a3['ParentActionId'] == $a2['ActionId']) $retVal[]=$a3;
                    }
                }
            }
        }
        return $retVal;
    }
}
