<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mtransportfees extends MY_Model {

    function __construct() {
        parent::__construct();
        $this->_table_name = "transportfees";
        $this->_primary_key = "TransportFeeId";
    }

    function update($transportFeeId, $postData, $isUpdateField = false){
        $fieldNull = array();
        if(!$isUpdateField) $fieldNull = array('UpdateUserId', 'UpdateDateTime');
        $this->load->model('Mtransportfeelogs');
        $this->db->trans_begin();
        if($transportFeeId > 0){
            $this->Mtransportfeelogs->insert($transportFeeId, $postData['UpdateUserId'], $postData['UpdateDateTime']);
            $transportFeeId = $this->save($postData, $transportFeeId, $fieldNull);
        }
        else $transportFeeId = $this->save($postData, 0, $fieldNull);
        if ($this->db->trans_status() === false){
            $this->db->trans_rollback();
            return 0;
        }
        else{
            $this->db->trans_commit();
            return $transportFeeId;
        }
    }
}
