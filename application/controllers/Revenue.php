<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Revenue extends MY_Controller {

	public function index(){

	}

	public function staff(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Doanh thu của CSKH',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/revenue_staff.js'))
				)
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'revenue/staff')) {
				$data['listStaffs'] = $this->Musers->getListByRole(1);
				$this->load->view('revenue/staff', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}
}
