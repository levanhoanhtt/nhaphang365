<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	public function index($orderStatusId = 0){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Đơn hàng',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/order_list.js'))
				)
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'order')) {
				$roleId = $user['RoleId'];
				$data['roleId'] = $roleId;
				$listUsers = $this->Musers->getListByRole(array(1, 4, 5, 8));
				$listCustomers = $listStaffs = array();
				foreach($listUsers as $u){
					if($u['RoleId'] == 4) $listCustomers[]=$u;
					else $listStaffs[]=$u;
				}
				$data['listCustomers'] = $listCustomers;
				$data['listStaffs'] = $listStaffs;
				$postData = $this->arrayFromPost(array('CustomerId', 'CareStaffId', 'OrderUserId', 'UserHandelId', 'ProductLink', 'OrderCode', 'Tracking', 'ShopNo', 'ComplaintStatusId', 'TrackingStatusId', 'BeginDate', 'EndDate'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$data['orderStatusId'] = 0;
				if($orderStatusId > 0 && $orderStatusId < 11){
					$postData['OrderStatusId'] = $orderStatusId;
					$data['orderStatusId'] = $orderStatusId;
				}
				if($roleId == 1) $postData['CareStaffId'] = $user['UserId'];
				elseif($roleId == 5 || $roleId == 8){
					if($roleId == 5 || !isset($postData['OrderUserId'])) $postData['OrderUserId'] = $user['UserId'];
					if(!in_array($orderStatusId, array(5, 6, 10))){
						unset($postData['OrderStatusId']);
						$postData['OrderStatusIds'] = '5,6,10';
					}
					$beginDateStr = '2017-06-19 00:00:00';
					if(empty($postData['BeginDate'])) $postData['BeginDate'] = $beginDateStr;
					else{
						$beginDate = new DateTime($beginDateStr);
						$endDate = new DateTime($postData['BeginDate']);
						$diff = date_diff($beginDate, $endDate);
						$diff = $diff->format('%R%a');
						if($diff < 0) $postData['BeginDate'] = $beginDateStr;;
					}
				}
				$this->load->model('Morders');
				$statisticData = $postData;
				unset($statisticData['OrderStatusId']);
				$data['statisticOrderCounts'] = $this->Morders->statisticCounts($statisticData, $this->Mconstants->orderStatus, true);
				$rowCount = $this->Morders->getCount($postData);
				$data['listOrders'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listOrders'] = $this->Morders->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('order/list', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function edit($orderId = 0, $complaintId = 0, $productIdScroll = 0, $shopCodeScroll = '', $fromExcel = 0, $actExcelId = 0){
		if($orderId > 0) {
			if($fromExcel == 1 && $actExcelId > 0) $user = $this->Musers->get($actExcelId);
			else $user = $this->session->userdata('user');
			if ($user) {
				$js =  array();
				$data = $this->commonData($user, 'Chi tiết Đơn hàng');
				$roleId = $user['RoleId'];
				if($roleId == 4) $flag = true;
				else $flag = $this->Mactions->checkAccess($data['listActions'], 'order/view');
				if ($flag) {
					$this->loadModel(array('Morders', 'Mwarehouses', 'Mservicefees', 'Mproducts', 'Mshops', 'Mcategories', 'Mmoneysources', 'Morderaccounts', 'Mshopnumbers', 'Mshoptrackings'));
					$order = $this->Morders->get($orderId);
					$view = 'edit_by_staff';
					if(!is_numeric($productIdScroll) || $productIdScroll < 0) $productIdScroll = 0;
					$data['productIdScroll'] = $productIdScroll;
					$data['shopCodeScroll'] = trim($shopCodeScroll);
					if ($order && $order['OrderStatusId'] > 0) {
						$js = array('js/jquery.price_format.min.js', 'vendor/plugins/jquery.matchHeight.js', 'ckfinder/ckfinder.js', 'js/order_function.js?20170729');
						$canEdit = false;
						$isAccess = false;
						$listOrderAccounts = array();
						$whereStatus = array('StatusId' => STATUS_ACTIVED);
						//$roleId = 8;
						if($roleId == 1 || $roleId == 7){ //CSKH
							$view = 'edit_by_staff';
							$js[] = 'vendor/plugins/jwerty/jwerty.js';
							$js[]= 'js/order_edit_by_staff.js?20170731';
							$isAccess = true;
							if($roleId == 7) {
								//$canEdit = true;
								$canEdit = $order['OrderStatusId'] == 1 || $order['OrderStatusId'] == 2 || $order['OrderStatusId'] == 3;
								//$isAccess = true;
							}
							elseif($order['CareStaffId'] == $user['UserId']){
								$canEdit = $order['OrderStatusId'] == 1 || $order['OrderStatusId'] == 2 || $order['OrderStatusId'] == 3;
								//$isAccess = true;
							}
							if ($isAccess && $order['OrderStatusId'] == 1){
								$notificationData = array(
									'CustomerId' => $order['CustomerId'],
									'UserId' => $user['UserId'],
									'RoleId' => $roleId,
									'OrderId' => $orderId,
									'ProductId' => 0,
									'ComplaintId' => 0,
									'Message' => "Đơn hàng {$order['OrderCode']} đã được nhân viên {$user['FullName']} check.",
									'IsCustomerSend' => 0,
									'NotificationStatusId' => 1,
									'LinkTo' => 'chi-tiet-don-hang-'.$orderId,
									'CrDateTime' => getCurentDateTime()
								);
								$flag = $this->Morders->updateField(array('OrderStatusId' => 2, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime()), $orderId, $notificationData, $order['CustomerId'], $order['OrderCode']);
								if($flag) $order['OrderStatusId'] = 2;
							}
						}
						elseif($roleId == 3){ //Quan ly
							$view = 'edit_by_admin';
							$js[]= 'js/order_edit_by_admin.js?20170724';
							$canEdit = $order['OrderStatusId'] <= 4;
							$isAccess = true;
							$listUsers = $this->Musers->getListByRole(array(1, 5, 8));
							$listCareStaffs = $listOrderUsers = array();
							foreach($listUsers as $u){
								if($u['RoleId'] == 1) $listCareStaffs[]=$u;
								elseif($u['RoleId'] == 5 || $u['RoleId'] == 8) $listOrderUsers[]=$u;
							}
							$data['listCareStaffs'] = $listCareStaffs;
							$data['listOrderUsers'] = $listOrderUsers;
						}
						elseif($roleId == 4){ //Khach hang
							$view = 'edit_by_custoner';
							$js[]= 'js/order_edit_by_customer.js?20170630';
							if($order['CustomerId'] == $user['UserId']) $canEdit = $order['OrderStatusId'] == 3;
							if($order['CustomerId'] == $user['UserId']) $isAccess = true;
							$data['isCustomerPanel'] = true;
							$data['listCareStaffs'] = $this->Musers->getListByRole(1);
						}
						elseif($roleId == 5 || $roleId == 8){//dat hang
							$view = 'edit_by_order';
							$js[]= 'js/order_edit_by_order.js?20170727';
							if(in_array($order['OrderStatusId'], array(5, 6, 10))){
								if ($roleId == 5) {
									if ($order['OrderUserId'] == $user['UserId']) {
										$canEdit = true;
										$isAccess = true;
									}
								}
								else {
									$canEdit = true;
									$isAccess = true;
								}
							}
							if($isAccess) $listOrderAccounts = $this->Morderaccounts->getBy($whereStatus);
						}
						elseif($roleId == 6) {//ke toan
							$view = 'edit_by_accountant';
							$js[]= 'js/order_edit_by_accountant.js?20170724';
							$canEdit = false;
							$isAccess = true;
						}
						if($isAccess) {
							$data['canEdit'] = $canEdit;
							$data['listOrderAccounts'] = $listOrderAccounts;
							$data['title'] = 'Chi tiết Đơn hàng ' . $order['OrderCode'];
							$data['orderId'] = $orderId;
							$data['order'] = $order;
							$data['exchangeRate'] = $order['ExchangeRate'];
							$data['careStaffName'] = '';
							$data['orderUserName'] = '';
							if ($roleId != 3) {
								$data['careStaffName'] = $this->Musers->getFieldValue(array('UserId' => $order['CareStaffId']), 'FullName');
								if ($order['OrderUserId'] > 0) $data['orderUserName'] = $this->Musers->getFieldValue(array('UserId' => $order['OrderUserId']), 'FullName');
							}
							$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
							$data['listServiceFees'] = $this->Mservicefees->getBy($whereStatus);
							$data['customerName'] = $this->Musers->getFieldValue(array('UserId' => $order['CustomerId']), 'FullName');
							$data['listCategories'] = $this->Mcategories->getByCategoryTypeId(1);
							$data['listMoneySources'] = array();
							if ($order['OrderStatusId'] == 5 || $order['OrderStatusId'] == 6) $data['listMoneySources'] = $this->Mmoneysources->getBy($whereStatus);
							$listProducts = $this->Mproducts->getBy(array('OrderId' => $orderId, 'ProductStatusId > ' => 0), false, 'ShopId');
							$listShops = array();
							$listShopNumbers = array();
							$listShopTrackings = array();
							if (!empty($listProducts)) {
								//if ($roleId == 5 || $roleId == 8) {
								if(in_array($order['OrderStatusId'], array(5, 6, 10))){
									$listShopNumbers = $this->Mshopnumbers->getByGroup($orderId);
									$listShopTrackings = $this->Mshoptrackings->getByGroup($orderId);
								}
								$shops = $this->Mshops->getBy(array('OrderId' => $orderId, 'ShopStatusId >' => 0));
								foreach ($shops as $s) {
									if (isset($listShopNumbers[$s['ShopId']])) {
										$listShopNumbers[$s['ShopCode']] = $listShopNumbers[$s['ShopId']];
										unset($listShopNumbers[$s['ShopId']]);
									}
									else $listShopNumbers[$s['ShopCode']] = array();
									if (isset($listShopTrackings[$s['ShopId']])) {
										$listShopTrackings[$s['ShopCode']] = $listShopTrackings[$s['ShopId']];
										unset($listShopTrackings[$s['ShopId']]);
									}
									else $listShopTrackings[$s['ShopCode']] = array();
								}
								$shopFields = array('ShopCode', 'ShopName', 'ShopUrl', 'CategoryId', 'ShipTQ', 'ShipTQOldNew', 'ShopStatusId', 'IsTransportSlow', 'ShipTQReason', 'ShipTQConfirm');
								foreach ($listProducts as $p) {
									if ($p['ShopId'] > 0) {
										foreach ($shops as $s) {
											if ($s['ShopId'] == $p['ShopId']) {
												foreach ($shopFields as $sf) $p[$sf] = $s[$sf];
												$p['ShopComment'] = $s['Comment'];
												$p['ShopFeedback'] = $s['Feedback'];
												$listShops[$s['ShopCode']][] = $p;
												break;
											}
										}
									}
								}
							}
							$data['listShops'] = $listShops;
							$data['listShopNumbers'] = $listShopNumbers;
							$data['listShopTrackings'] = $listShopTrackings;
							$data['complaintId'] = 0;
							$this->load->model('Mcomplaints');
							$listComplaints = $this->Mcomplaints->search(array('OrderId' => $orderId), PHP_INT_MAX, 1);
							$data['listComplaints'] = $listComplaints;
							if (!is_numeric($complaintId) || $complaintId < 0) $complaintId = 0;
							$flag = $this->Mconstants->getObjectValue($listComplaints, 'ComplaintId', $complaintId, 'ComplaintId');
							if ($flag > 0) $data['complaintId'] = $complaintId;
						}
						else {
							$view = 'edit_by_staff';
							$data['orderId'] = 0;
							$data['txtError'] = "Không tìm thấy đơn hàng";
						}
					}
					else {
						$view = 'edit_by_staff';
						$data['orderId'] = 0;
						$data['txtError'] = "Không tìm thấy đơn hàng";
					}
					$data['scriptFooter'] = array('js' => $js);
					$this->load->view('order/'.$view, $data);
				}
				else $this->load->view('user/permission', $data);
			}
			else redirect('user');
		}
		else redirect('order');
	}

	public function add(){
		$user = $this->session->userdata('user');
		if ($user) {
			$data = $this->commonData($user,
				'Thêm Đơn hàng',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/jquery.price_format.min.js', 'ckfinder/ckfinder.js', 'js/order_function.js?20170729', 'js/order_add_by_staff.js?20170703'))
				)
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'order/add')) {
				$data['canEdit'] = true;
				$data['productIdScroll'] = 0;
				$data['shopCodeScroll'] = '';
				$this->loadModel(array('Mwarehouses', 'Mservicefees', 'Mconfigs', 'Morderdraffs', 'Mcategories'));
				$whereStatus = array('StatusId' => STATUS_ACTIVED);
				$data['listCustomers'] = $this->Musers->getListByRole(4);
				$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
				$data['listServiceFees'] = $this->Mservicefees->getBy($whereStatus);
				$data['listCategories'] = $this->Mcategories->getByCategoryTypeId(1);
				$orderDraff = $this->Morderdraffs->getByUser($user['UserId'], $this->input->ip_address());;
				if (!$orderDraff) {
					$orderDraff = array(
						'FullName' => '',
						'PhoneNumber' => '',
						'Email' => '',
						'Address' => '',
						'CareStaffId' => '',
						'WarehouseId' => '',
						'Comment' => '',
						'ProductJson' => '[]'
					);
					$data['exchangeRate'] = $this->Mconfigs->getFieldValue(array('ConfigCode' => 'EXCHAGE_RATE_CN'), 'ConfigValue', 3456);
				}
				else $data['exchangeRate'] = $orderDraff['ExchangeRate'];
				$data['orderDraff'] = $orderDraff;
				$listProducts = json_decode($orderDraff['ProductJson'], true);
				$listShops = array();
				foreach((array)$listProducts as $p){
					if(!empty($p['ShopCode'])) $listShops[$p['ShopCode']][] = $p;
					else $listShops['NoShopHMD'][] = $p;
				}
				$data['listShops'] = $listShops;
				$this->load->view('order/add', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function addOrderUser(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Cập nhật Đặt hàng cho Đơn hàng',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/order_add_order_user.js'))
				)
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'order/addOrderUser')) {
				$postData = $this->arrayFromPost(array('OrderCode', 'BeginDate', 'EndDate'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$postData['OrderStatusId'] = 5;
				$postData['NoOrderUserId'] = 0;
				$beginDateStr = '2017-06-19 00:00:00';
				if(empty($postData['BeginDate'])) $postData['BeginDate'] = $beginDateStr;
				else{
					$beginDate = new DateTime($beginDateStr);
					$endDate = new DateTime($postData['BeginDate']);
					$diff = date_diff($beginDate, $endDate);
					$diff = $diff->format('%R%a');
					if($diff < 0) $postData['BeginDate'] = $beginDateStr;;
				}
				$this->load->model('Morders');
				$rowCount = $this->Morders->getCount($postData);
				$data['listOrders'] = array();
				if($rowCount > 0){
					$data['listOrderUsers'] = $this->Musers->getListByRole(array(5, 8));
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listOrders'] = $this->Morders->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('order/add_order_user', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function saveDraff(){
		$postData = $this->arrayFromPost(array('UserId', 'FullName', 'PhoneNumber', 'Email', 'Address', 'CareStaffId', 'WarehouseId', 'ExchangeRate', 'Comment', 'ProductJson'));
		$postData['ProductJson'] = replaceFileUrl($postData['ProductJson'], PRODUCT_PATH);
		$postData['IpAddress'] = $this->input->ip_address();
		$postData['UserAgent'] = $this->input->user_agent();
		$postData['CrDateTime'] = getCurentDateTime();
		$this->load->model('Morderdraffs');
		$flag = $this->Morderdraffs->update($postData);
		if($flag) echo json_encode(array('code' => 1, 'message' => 'Lưu đơn hàng thành công'));
		else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function update(){
		$isFromAdmin = $this->input->post('IsFromAdmin');
		$flag = true;
		$userId = 0;
		$user = $this->session->userdata('user');
		if($user) $userId = $user['UserId'];
		elseif($isFromAdmin == 1) $flag = false;
		if($flag) {
			$postData = $this->arrayFromPost(array('OrderCode', 'CustomerId', 'FullName', 'PhoneNumber', 'Email', 'Address', 'CareStaffId', 'WarehouseId', 'ExchangeRate', 'OrderStatusId', 'ServicePercent', 'IsFixPercent', 'OtherCost', 'Comment', 'OrderUserId'));
			$postData['OtherCost'] = replacePrice($postData['OtherCost']);
			$postData['TrackingStatusId'] = 1;
			$postData['IpAddress'] = $this->input->ip_address();
			$orderId = $this->input->post('OrderId');
			$updateDateTime = getCurentDateTime();
			if ($orderId == 0) {
				if ($isFromAdmin == 1) $postData['CrUserId'] = $userId;
				else $postData['CrUserId'] = $postData['CustomerId'];
				$crDateTime = ddMMyyyyToDate(trim($this->input->post('CrDate')));
				if(empty($crDateTime)) $crDateTime = $updateDateTime;
				else $crDateTime .= ' ' . trim($this->input->post('CrTime'));
				$postData['CrDateTime'] = $crDateTime;
			}
			else {
				$postData['UpdateUserId'] = $userId;
				$postData['UpdateDateTime'] = $updateDateTime;
			}
			if($postData['OrderStatusId'] == 5){
				$postData['OrderDate'] = $updateDateTime;
				$postData['IsFixPercent'] = 1;
			}
			$listProducts = array();
			$productJson = $this->input->post('ProductJson');
			if (!empty($productJson)) {
				$productJson = replaceFileUrl($productJson, PRODUCT_PATH);
				$listProducts = json_decode($productJson, true);
			}
			$this->load->model('Morders');
			$orderId = $this->Morders->update($postData, $orderId, $listProducts);
			if ($orderId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Đơn hàng thành công", 'data' => $orderId));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->session->userdata('user');
		if ($user) {
			$orderId = $this->input->post('OrderId');
			$orderStatusId = $this->input->post('OrderStatusId');
			if($orderId > 0 && $orderStatusId > 0 && $orderStatusId < 11){
				$crDateTime = getCurentDateTime();
				$customerId = $this->input->post('CustomerId');
				$orderCode = $this->input->post('OrderCode');
				$notificationData = array();
				if($orderStatusId == 8){
					$notificationData = array(
						'CustomerId' => $customerId,
						'UserId' => $user['UserId'],
						'RoleId' => $user['RoleId'],
						'OrderId' => $orderId,
						'ProductId' => 0,
						'ComplaintId' => 0,
						'Message' => "Hoàn tiền đơn hàng {$orderCode}.",
						'IsCustomerSend' => 0,
						'NotificationStatusId' => 1,
						'LinkTo' => 'chi-tiet-don-hang-'.$orderId,
						'CrDateTime' => $crDateTime
					);
				}
				elseif($orderStatusId == 9){
					$notificationData = array(
						'CustomerId' => $customerId,
						'UserId' => $this->input->post('CareStaffId'),
						'RoleId' => 1,
						'OrderId' => $orderId,
						'ProductId' => 0,
						'ComplaintId' => 0,
						'Message' => "Đơn hàng {$orderCode} kế toán đã duyệt.",
						'IsCustomerSend' => 1,
						'NotificationStatusId' => 1,
						'LinkTo' => 'order/edit/'.$orderId,
						'CrDateTime' => $crDateTime
					);
				}
				$this->load->model('Morders');
				$postData = array('OrderStatusId' => $orderStatusId, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => $crDateTime);
				if($orderStatusId == 5){
					$postData['OrderDate'] = $crDateTime;
					$postData['IsFixPercent'] = 1;
				}
				$flag = $this->Morders->updateField($postData, $orderId, $notificationData, $customerId, $orderCode);
				if($flag){
					$orderStatusName = '<span class="'.$this->Mconstants->labelCss[$orderStatusId].'">'.$this->Mconstants->orderStatus[$orderStatusId].'</span>';
					echo json_encode(array('code' => 1, 'message' => "Cập nhật đơn hàng thành công", 'data' => $orderStatusName));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function cloneOrder(){
		$user = $this->session->userdata('user');
		if ($user) {
			$orderId = $this->input->post('OrderId');
			if($orderId > 0){
				$this->loadModel(array('Morders', 'Mconfigs', 'Mproducts', 'Mservicefees'));
				$order = $this->Morders->get($orderId);
				if($order){
					$exchangeRate = $this->Mconfigs->getFieldValue(array('ConfigCode' => 'EXCHAGE_RATE_CN'), 'ConfigValue', 3456);
					$postData = array_merge($order, array(
						'ExchangeRate' => $exchangeRate,
						'OrderStatusId' => 1,
						'OtherCost' => 0,
						'CrUserId' => $user['UserId'],
						'CrDateTime' => getCurentDateTime()
					));
					unset($postData['OrderId']);
					unset($postData['CustomerOkDate']);
					unset($postData['OrderDate']);
					unset($postData['UpdateUserId']);
					unset($postData['UpdateDateTime']);
					$orderCode = $this->Morders->cloneOrder($orderId, $postData);
					if(!empty($orderCode)) echo json_encode(array('code' => 1, 'message' => "Tạo Đơn hàng {$orderCode} thành công"));
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Đơn hàng không tồn tại"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateStaff(){
		$user = $this->session->userdata('user');
		if ($user && ($user['RoleId'] == 3 || $user['RoleId'] == 4 || $user['RoleId'] == 8)) {
			$orderId = $this->input->post('OrderId');
			if($orderId > 0){
				$postData = $this->arrayFromPost(array('CareStaffId', 'OrderUserId'));
				$postData['UpdateUserId'] = $user['UserId'];
				$updateDateTime = getCurentDateTime();
				$postData['UpdateDateTime'] = $updateDateTime;
				$this->load->model('Morders');
				$flag = $this->Morders->updateField($postData, $orderId);
				if($flag){
					$oldCareStaffId = $this->input->post('OldCareStaffId');
					if($postData['CareStaffId'] != $oldCareStaffId){
						$customerId = $this->input->post('CustomerId');
						$orderCode = $this->input->post('OrderCode');
						$notificationData = array();
						if($oldCareStaffId > 0){
							$notificationData[] = array(
								'CustomerId' => $customerId,
								'UserId' => $oldCareStaffId,
								'RoleId' => 1,
								'OrderId' => $orderId,
								'ProductId' => 0,
								'ComplaintId' => 0,
								'Message' => 'Khách hàng đã lựa chọn nhân viên khác check đơn '.$orderCode,
								'IsCustomerSend' => 1,
								'NotificationStatusId' => 1,
								'LinkTo' => 'order/edit/'.$orderId,
								'CrDateTime' => $updateDateTime
							);
						}
						if($postData['CareStaffId'] > 0){
							$notificationData[] = array(
								'CustomerId' => $customerId,
								'UserId' => $postData['CareStaffId'],
								'RoleId' => 1,
								'OrderId' => $orderId,
								'ProductId' => 0,
								'ComplaintId' => 0,
								'Message' => "Khách hàng {$this->Musers->getFieldValue(array('UserId' => $customerId), 'FullName')} đã gửi đơn hàng {$orderCode} cần check.",
								'IsCustomerSend' => 1,
								'NotificationStatusId' => 1,
								'LinkTo' => 'order/edit/'.$orderId,
								'CrDateTime' => $updateDateTime
							);
						}
						$this->db->insert_batch('notifications', $notificationData);
					}
					echo json_encode(array('code' => 1, 'message' => "Cập nhật nhân viên thành công"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function merge(){
		$user = $this->session->userdata('user');
		if ($user) {
			$orderId = $this->input->post('OrderId');
			$mergeOrderId = $this->input->post('MergeOrderId');//bo
			if($orderId > 0 && $mergeOrderId > 0 && $orderId != $mergeOrderId){
				$updateDateTime = getCurentDateTime();
				$notificationData = array(
					'CustomerId' => $this->input->post('CustomerId'),
					'UserId' => $this->input->post('CareStaffId'),
					'RoleId' => 1,
					'OrderId' => $orderId,
					'ProductId' => 0,
					'ComplaintId' => 0,
					'Message' => "Khách hàng {$this->input->post('CustomerName')} đã gửi đơn hàng {$this->input->post('OrderCode')} cần check.",
					'IsCustomerSend' => 1,
					'NotificationStatusId' => 1,
					'LinkTo' => 'order/edit/'.$orderId,
					'CrDateTime' => $updateDateTime
				);
				$this->load->model('Morders');
				$flag = $this->Morders->merge($orderId, $mergeOrderId, $user['UserId'], $updateDateTime, $notificationData);
				if($flag) echo json_encode(array('code' => 1, 'message' => "Gộp đơn hàng thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function mergeDraff(){
		$user = $this->session->userdata('user');
		if ($user) {
			$orderId = $this->input->post('OrderId');
			$listProducts = array();
			$productJson = $this->input->post('ProductJson');
			if (!empty($productJson)) {
				$productJson = replaceFileUrl($productJson, PRODUCT_PATH);
				$listProducts = json_decode($productJson, true);
			}
			if($orderId > 0 && !empty($listProducts)){
				$updateDateTime = getCurentDateTime();
				$this->load->model('Morders');
				$order = $this->Morders->get($orderId, true, '', 'CustomerId, CareStaffId, OrderCode');
				if($order) {
					$notificationData = array(
						'CustomerId' => $order['CustomerId'],
						'UserId' => $order['CareStaffId'],
						'RoleId' => 1,
						'OrderId' => $orderId,
						'ProductId' => 0,
						'ComplaintId' => 0,
						'Message' => "Khách hàng {$this->input->post('CustomerName')} đã gửi đơn hàng {$order['OrderCode']} cần check.",
						'IsCustomerSend' => 1,
						'NotificationStatusId' => 1,
						'LinkTo' => 'order/edit/' . $orderId,
						'CrDateTime' => $updateDateTime
					);
				}
				else $notificationData = array();
				$flag = $this->Morders->mergeDraff($orderId, $listProducts, $user['UserId'], $updateDateTime, $notificationData);
				if($flag) echo json_encode(array('code' => 1, 'message' => "Gộp đơn hàng thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function import(){
		$fileUrl = trim($this->input->post('FileUrl'));
		if (!empty($fileUrl)) {
			if(ROOT_PATH != '/') $fileUrl = str_replace(ROOT_PATH, '', $fileUrl);
			$fileUrl = FCPATH . $fileUrl;
			$this->load->library('excel');
			$inputFileType = PHPExcel_IOFactory::identify($fileUrl);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objReader->setReadDataOnly(true);
			$objPHPExcel = $objReader->load($fileUrl);
			$objWorksheet  = $objPHPExcel->setActiveSheetIndex(0);
			$data = array(
				'FullName' => trim($objWorksheet->getCellByColumnAndRow(3, 4)->getValue()),
				'PhoneNumber' => trim($objWorksheet->getCellByColumnAndRow(3, 5)->getValue()),
				'Email' => trim($objWorksheet->getCellByColumnAndRow(3, 6)->getValue()),
				'Address' => trim($objWorksheet->getCellByColumnAndRow(3, 7)->getValue()),
				'ExchangeRate' => trim($objWorksheet->getCellByColumnAndRow(11, 1)->getValue())
			);
			$products = array();
			$shipTQs = array();
			$productLinkOld = '';
			for($row = 11; $row < 100; $row++){
				$productLink = trim($objWorksheet->getCellByColumnAndRow(1, $row)->getValue());
				if(empty($productLink)) $productLink = $productLinkOld;
				else $productLinkOld = $productLink;
				$shipTQ = trim($objWorksheet->getCellByColumnAndRow(7, $row)->getValue());
				if(empty($shipTQ)) $shipTQ = 0;
				$shopId = trim($objWorksheet->getCellByColumnAndRow(11, $row)->getValue());
				if(empty($shopId)) $shopId = 1;
				if(!isset($shipTQs[$shopId])) $shipTQs[$shopId] = $shipTQ;
				elseif($shipTQs[$shopId] == 0) $shipTQs[$shopId] = $shipTQ;
				if($productLink != 'Tổng'){
					$quantity = trim($objWorksheet->getCellByColumnAndRow(5, $row)->getValue());
					$cost = trim($objWorksheet->getCellByColumnAndRow(6, $row)->getValue());
					if(!empty($quantity) && !empty($cost) && $quantity * $cost > 0) {
						$products[$shopId][] = array(
							'ProductLink' => $productLink,
							'Color' => trim($objWorksheet->getCellByColumnAndRow(2, $row)->getValue()),
							'Size' => trim($objWorksheet->getCellByColumnAndRow(3, $row)->getValue()),
							'ProductImage' => trim($objWorksheet->getCellByColumnAndRow(4, $row)->getValue()),
							'Quantity' => $quantity,
							'Cost' => $cost,
							'Comment' => trim($objWorksheet->getCellByColumnAndRow(9, $row)->getValue())
						);
					}
				}
				else break;
			}
			$data['Products'] = $products;
			$data['ShipTQs'] = $shipTQs;
			echo json_encode(array('code' => 1, 'data' => $data));
			$objPHPExcel->disconnectWorksheets();
			unset($objPHPExcel);
		}
		else echo json_encode(array('code' => -1, 'message' => "Vui lòng chọn file Excel đơn hàng"));
	}

	public function export($orderId = 0){
		$user = $this->session->userdata('user');
		if($orderId > 0 && $user){
			$this->loadModel(array('Morders', 'Mproducts', 'Mshops'));
			$order = $this->Morders->get($orderId);
			if($order){
				$fileUrl = FCPATH . 'assets/uploads/don-dat-hang.xls';
				$this->load->library('excel');
				$objReader = PHPExcel_IOFactory::createReader('Excel5');
				$objPHPExcel = $objReader->load($fileUrl);
				$objPHPExcel->setActiveSheetIndex(0);
				$sheet = $objPHPExcel->getActiveSheet();
				$orderCode = $this->Musers->getFieldValue(array('UserId' => $order['CustomerId']), 'FullName') . ' - '.$orderId;
				$orderUrl = base_url('order/edit/'.$orderId.'/0/0/shopcode/1/'.$user['UserId']);
				$listProducts = $this->Mproducts->getBy(array('OrderId' => $orderId, 'ProductStatusId > ' => 0), false, 'ShopId');
				$listShops = $this->Mshops->getBy(array('OrderId' => $orderId, 'ShopStatusId >' => 0));
				$shipTQs = array();
				$i = 0;
				$productPath = base_url(PRODUCT_PATH).'/';
				$noProductImage = $productPath.NO_PRODUCT;
				foreach($listProducts as $p){
					$i++;
					$j = $i + 1;
					$shipTQ = 0;
					if(!isset($shipTQs[$p['ShopId']])){
						$shipTQ = $this->Mconstants->getObjectValue($listShops, 'ShopId', $p['ShopId'], 'ShipTQ');
						$shipTQs[$p['ShopId']] = 1;
					}
					$productImage = getFileUrl($productPath, $p['ProductImage'], $noProductImage);
					$sheet->setCellValue('A'.$j, $i);
					$sheet->setCellValue('B'.$j, $p['ProductLink']);
					$sheet->getCell('B'.$j)->getHyperlink()->setUrl($p['ProductLink']);
					$sheet->setCellValue('C'.$j, $p['Color']);
					$sheet->setCellValue('D'.$j, $p['Size']);
					$sheet->setCellValue('E'.$j, $productImage);
					$sheet->getCell('E'.$j)->getHyperlink()->setUrl($productImage);
					$sheet->setCellValue('F'.$j, $p['Quantity']);
					$sheet->setCellValue('G'.$j, $p['Cost']);
					$sheet->setCellValue('H'.$j, $shipTQ);
					$sheet->setCellValue('I'.$j, $p['Quantity'] * $p['Cost'] + $shipTQ);
					$sheet->setCellValue('J'.$j, $p['Comment']);
					$sheet->setCellValue('K'.$j, $this->Mconstants->getObjectValue($listShops, 'ShopId', $p['ShopId'], 'ShopCode'));
					$sheet->setCellValue('O'.$j, $order['Address']);
					$sheet->setCellValue('P'.$j, $orderCode);
					$sheet->getCell('P'.$j)->getHyperlink()->setUrl($orderUrl);
				}
				$cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
				$cellIterator->setIterateOnlyExistingCells(true);
				foreach($cellIterator as $cell) $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
				$filename = "Don_dat_hang_{$orderCode}.xls";
				header('Content-Type: application/vnd.ms-excel');
				header('Content-Disposition: attachment;filename="'.$filename.'"');
				header('Cache-Control: max-age=0');
				$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
				$objWriter->save('php://output');
				$objPHPExcel->disconnectWorksheets();
				unset($objPHPExcel);
			}
			else echo "<script>window.close();</script>";
		}
		else echo "<script>window.close();</script>";
	}

	public function getList(){
		$user = $this->session->userdata('user');
		if($user){
			$postData = $this->arrayFromPost(array('CustomerId', 'OrderStatusId', 'OrderStatusIds'));
			if($user['RoleId'] == 1) $postData['CareStaffId'] = $user['UserId'];
			elseif($user['RoleId'] == 5) $postData['OrderUserId'] = $user['UserId'];
			$this->load->model('Morders');
			$listOrderIds = $this->Morders->search($postData, PHP_INT_MAX, 1, 'OrderId,OrderCode');
			echo json_encode(array('code' => 1, 'data' => $listOrderIds));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	//tai chinh cua khach
	public function getList02(){
		$user = $this->session->userdata('user');
		$customerId = $this->input->post('CustomerId');
		if($user && $customerId > 0){
			$postData = array('CustomerId' => $customerId);
			if($user['RoleId'] == 1) $postData['CareStaffId'] = $user['UserId'];
			elseif($user['RoleId'] == 5) $postData['OrderUserId'] = $user['UserId'];
			$this->loadModel(array('Morders', 'Mproducts'));
			$rowCount = $this->Morders->getCount($postData);
			$listOrders = array();
			$paggingHtml = '';
			if($rowCount > 0){
				$perPage = 20;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$paggingHtml = getPaggingHtml($page, $pageCount, 'paggingOrder');
				$listOrderRaws = $this->Morders->search($postData, $perPage, $page);
				$orderStatus = $this->Mconstants->orderStatus;
				$labelCss = $this->Mconstants->labelCss;
				foreach($listOrderRaws as $o){
					$o['OrderLinkHtml'] = '<a href="'.base_url('order/edit/'.$o['OrderId']).'" target="_blank">'.$o['OrderCode'].'</a>';
					$o['CrDateTime'] = ddMMyyyy($o['CrDateTime'], 'd/m/Y H:i');
					$o['OrderStatusHtml'] = '<span class="'.$labelCss[$o['OrderStatusId']].'">'.$orderStatus[$o['OrderStatusId']].'</span>';
					$totalCost = $this->Morders->getSumCost($o);
					$o['TotalCost'] = priceFormat($totalCost);
					$listOrders[]=$o;
				}
			}
			echo json_encode(array('code' => 1, 'data' => array('ListOrders' => $listOrders, 'PaggingHtml' => $paggingHtml)));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	//thong ke tai chinh
	public function getList03(){
		$user = $this->session->userdata('user');
		$statisticTypeId = $this->input->post('StatisticTypeId');
		if ($user && $statisticTypeId > 0) {
			$postData = $this->arrayFromPost(array('CustomerId', 'CareStaffId', 'OrderUserId', 'OrderStatusId', 'OrderBeginDate', 'OrderEndDate'));
			if(!empty($postData['OrderBeginDate'])) $postData['OrderBeginDate'] = ddMMyyyyToDate($postData['OrderBeginDate']);
			if(!empty($postData['OrderEndDate'])) $postData['OrderEndDate'] = ddMMyyyyToDate($postData['OrderEndDate']);
			$this->loadModel(array('Morders', 'Mproducts'));
			$listOrderRaws = $this->Morders->search($postData, PHP_INT_MAX, 1);
			$listOrders = array();
			$orderStatus = $this->Mconstants->orderStatus;
			$labelCss = $this->Mconstants->labelCss;
			foreach($listOrderRaws as $o){
				$totalCost = $this->Morders->getSumCost($o, false);
				$o['TotalCost'] = $totalCost;
				$listOrders[]=$o;
			}
			$data = array();
			if($statisticTypeId == 1){ // thong ke theo tung NV CSKH - o trang dashboard
				$revenues = array();
				$staffNames = array();
				$listStaffs = $this->Musers->getListByRole(1);
				foreach($listStaffs as $st){
					$revenues[$st['UserId']] = 0;
					$staffNames[$st['UserId']] = $st['FullName'];
					foreach($listOrders as $o){
						if(!in_array($o['OrderId'], array(462, 483, 491, 492, 495, 496, 497, 501, 504, 508, 516, 520, 521, 522, 526, 531, 546, 547, 548, 549, 568, 570, 572, 574, 575, 594, 596, 597, 616, 617, 621, 632, 644, 666))) {
							if ($st['UserId'] == $o['CareStaffId']) $revenues[$st['UserId']] += $o['TotalCost'];
						}
					}
				}
				arsort($revenues);
				foreach($revenues as $staffId => $totalCost){
					$data[]=array(
						'StaffId' => $staffId,
						'FullName' => $staffNames[$staffId],
						'TotalCost' => priceFormat($totalCost)
					);
				}
			}
			else $data = $listOrders;
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function statistics(){
		$user = $this->session->userdata('user');
		$orderStatusIds = trim($this->input->post('OrderStatusIds'));
		if(!empty($orderStatusIds)) $orderStatusIds = explode(',', $orderStatusIds);
		if($user && !empty($orderStatusIds)){
			$where = array();
			if($user['RoleId'] == 1) $where = array('CareStaffId' => $user['UserId']);
			elseif($user['RoleId'] == 5) $where = array('OrderUserId' => $user['UserId']);
			$orderStatusStatistics = array();
			foreach($orderStatusIds as $id) $orderStatusStatistics[$id] = '';
			$isStatisticAll = ($this->input->post('IsStatisticAll') == 1) ? true : false;
			$this->load->model('Morders');
			$statistics = $this->Morders->statisticCounts($where, $orderStatusStatistics, $isStatisticAll);
			$data = array();
			foreach($statistics as $i => $v) $data['Count_'.$i] = $v;
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}