<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transporttype extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Phương thức giao hàng',
				array('scriptFooter' => array('js' => 'js/transport_type.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'transporttype')) {
				$data['updateTransportType'] = $this->Mactions->checkAccess($listActions, 'transporttype/update');
				$data['deleteTransportType'] = $this->Mactions->checkAccess($listActions, 'transporttype/delete');
				$this->load->model('Mtransporttypes');
				$data['listTransportTypes'] = $this->Mtransporttypes->getHierachy();
				$this->load->view('setting/transporttype', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('TransportTypeName', 'ParentTransportTypeId'));
		if (!empty($postData['TransportTypeName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$transportTypeId = $this->input->post('TransportTypeId');
			$this->load->model('Mtransporttypes');
			$flag = $this->Mtransporttypes->save($postData, $transportTypeId);
			if ($flag > 0) {
				$postData['TransportTypeId'] = $flag;
				$postData['ParentTransportTypeName'] = $this->Mtransporttypes->getFieldValue(array('TransportTypeId' => $postData['ParentTransportTypeId']), 'TransportTypeName');
				$postData['IsAdd'] = ($transportTypeId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Phương thức giao hàng thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$transportTypeId = $this->input->post('TransportTypeId');
		if ($transportTypeId > 0) {
			$this->load->model('Mtransporttypes');
			$flag = $this->Mtransporttypes->checkHasChild($transportTypeId);
			if($flag) {
				$flag = $this->Mtransporttypes->changeStatus(0, $transportTypeId);
				if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Phương thức giao hàng thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Phương thức giao hàng này có con"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
