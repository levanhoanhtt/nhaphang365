<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transportfee extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Phí vận chuyển',
				array('scriptFooter' => array('js' => array('js/jquery.price_format.min.js', 'js/transport_fee.js')))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'transportfee')) {
				$data['updateTransportFee'] = $this->Mactions->checkAccess($listActions, 'transportfee/update');
				$data['deleteTransportFee'] = $this->Mactions->checkAccess($listActions, 'transportfee/delete');
				$this->loadModel(array('Mwarehouses', 'Mtransportfees'));
				$whereStatus = array('StatusId' => STATUS_ACTIVED);
				$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
				$data['listTransportFees'] = $this->Mtransportfees->getBy($whereStatus, false, 'WarehouseId');
				$this->load->view('setting/transportfee', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$user = $this->session->userdata('user');
		if($user) {
			$postData = $this->arrayFromPost(array('WarehouseId', 'BeginWeight', 'EndWeight', 'WeightCost'));
			$weightCost = $postData['WeightCost'];
			$postData['WeightCost'] = replacePrice($weightCost);
			if ($postData['EndWeight'] > 0 && $postData['WeightCost'] > 0) {
				$postData['StatusId'] = STATUS_ACTIVED;
				$transportFeeId = $this->input->post('TransportFeeId');
				if($transportFeeId == 0) {
					$postData['CrUserId'] = $user['UserId'];
					$postData['CrDateTime'] = getCurentDateTime();
				}
				else{
					$postData['UpdateUserId'] = $user['UserId'];
					$postData['UpdateDateTime'] = getCurentDateTime();
				}
				$this->loadModel(array('Mwarehouses', 'Mtransportfees'));
				$flag = $this->Mtransportfees->update($transportFeeId, $postData);
				if ($flag > 0) {
					$postData['TransportFeeId'] = $flag;
					$postData['WarehouseName'] = $this->Mwarehouses->getFieldValue(array('WarehouseId' => $postData['WarehouseId']), 'WarehouseName');
					$postData['WeightCost'] = $weightCost;
					$postData['IsAdd'] = ($transportFeeId > 0) ? 0 : 1;
					echo json_encode(array('code' => 1, 'message' => "Cập nhật phí vận chuyển thành công", 'data' => $postData));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$user = $this->session->userdata('user');
		if($user) {
			$transportFeeId = $this->input->post('TransportFeeId');
			if ($transportFeeId > 0) {
				$this->load->model('Mtransportfees');
				$flag = $this->Mtransportfees->update($transportFeeId, array('StatusId' => 0, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime()), true);
				if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa phí vận chuyển thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
