<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user && $user['RoleId'] != 4) redirect('order/add');
		else {
			$this->loadModel(array('Mwarehouses', 'Mservicefees', 'Mconfigs', 'Morderdraffs', 'Mcategories'));
			$whereStatus = array('StatusId' => STATUS_ACTIVED);
			$data = array(
				'title' => 'Tạo đơn hàng',
				'scriptFooter' => array('js' => array('js/jquery.price_format.min.js', 'ckfinder/ckfinder.js', 'js/order_function.js?20170729', 'js/order_add_by_customer.js?20170630')),
				'listCustomerCareStaff' => $this->Musers->getListByRole(1),
				'listWarehouses' => $this->Mwarehouses->getBy($whereStatus),
				'listServiceFees' => $this->Mservicefees->getBy($whereStatus),
				'listCategories' => $this->Mcategories->getByCategoryTypeId(1),
				'isCustomerPanel' => false,
				'canEdit' => true,
				'productIdScroll' => 0,
				'shopCodeScroll' => 0
			);
			$data['exchangeRate'] = $this->Mconfigs->getFieldValue(array('ConfigCode' => 'EXCHAGE_RATE_CN'), 'ConfigValue', 3456);
			$userId = 0;
			if ($user && $user['RoleId'] == 4) $userId = $user['UserId'];
			$orderDraff = $this->Morderdraffs->getByUser($userId, $this->input->ip_address());
			if (!$orderDraff) {
				$orderDraff = array(
					'FullName' => '',
					'PhoneNumber' => '',
					'Email' => '',
					'Address' => '',
					'CareStaffId' => '',
					'WarehouseId' => '',
					'Comment' => '',
					'ProductJson' => '[]'
				);
				if ($userId > 0) {
					$orderDraff = array_merge($orderDraff, array(
						'FullName' => $user['FullName'],
						'PhoneNumber' => $user['PhoneNumber'],
						'Email' => $user['Email'],
						'Address' => $user['Address']
					));
					if (!empty($user['Configs'])) {
						$configs = json_decode($user['Configs'], true);
						if (isset($configs['CareStaffId'])) $orderDraff['CareStaffId'] = $configs['CareStaffId'];
					}
				}
			}
			else{
				if($userId > 0 && empty($orderDraff['FullName']) && empty($orderDraff['PhoneNumber']) && empty($orderDraff['Email']) && empty($orderDraff['Address'])){
					$orderDraff = array_merge($orderDraff, array(
						'FullName' => $user['FullName'],
						'PhoneNumber' => $user['PhoneNumber'],
						'Email' => $user['Email'],
						'Address' => $user['Address']
					));
					if (!empty($user['Configs'])) {
						$configs = json_decode($user['Configs'], true);
						if (isset($configs['CareStaffId'])) $orderDraff['CareStaffId'] = $configs['CareStaffId'];
					}
				}
			}
			$data['orderDraff'] = $orderDraff;
			$listProducts = json_decode($orderDraff['ProductJson'], true);
			$listShops = array();
			foreach((array)$listProducts as $p){
				if(!empty($p['ShopCode'])) $listShops[$p['ShopCode']][] = $p;
				else $listShops['NoShopHMD'][] = $p;
			}
			$data['listShops'] = $listShops;
			$this->load->view('customer/index', $data);
		}
	}

	public function profile(){
		$user = $this->session->userdata('user');
		if($user) {
			if ($user['RoleId'] == 4) {
				$data = array(
					'title' => 'Thông tin tài khoản',
					'user' => $user,
					'scriptFooter' =>array('js' => array('ckfinder/ckfinder.js', 'js/user_profile.js?20170715')),
					'isCustomerPanel' => true
				);
				$this->load->model('Mprovinces');
				$data['listProvinces'] = $this->Mprovinces->getList();
				$data['listCustomerCareStaff'] = $this->Musers->getListByRole(1);
				$data['bankAccountNumber'] = $data['bankName1'] = $data['bankName2'] = $data['bankAccountName'] = '';
				$data['fullName0'] = $data['fullName1'] = $data['fullName2'] = $data['phoneNumber1'] = $data['phoneNumber2'] = $data['address1'] = $data['address2'] = '';
				$data['provinceId1'] = $data['provinceId2'] = 24;
				if(!empty($user['Configs'])){
					$configs = json_decode($user['Configs'], true);
					if(isset($configs['BankAccountNumber'])) $data['bankAccountNumber'] = $configs['BankAccountNumber'];
					if(isset($configs['BankName1'])) $data['bankName1'] = $configs['BankName1'];
					if(isset($configs['BankName2'])) $data['bankName2'] = $configs['BankName2'];
					if(isset($configs['BankAccountName'])) $data['bankAccountName'] = $configs['BankAccountName'];
					if(isset($configs['FullName0'])) $data['fullName0'] = $configs['FullName0'];
					if(isset($configs['FullName1'])) $data['fullName1'] = $configs['FullName1'];
					if(isset($configs['FullName2'])) $data['fullName2'] = $configs['FullName2'];
					if(isset($configs['PhoneNumber1'])) $data['phoneNumber1'] = $configs['PhoneNumber1'];
					if(isset($configs['PhoneNumber2'])) $data['phoneNumber2'] = $configs['PhoneNumber2'];
					if(isset($configs['Address1'])) $data['address1'] = $configs['Address1'];
					if(isset($configs['Address2'])) $data['address2'] = $configs['Address2'];
					if(isset($configs['ProvinceId1'])) $data['provinceId1'] = $configs['ProvinceId1'];
					if(isset($configs['ProvinceId2'])) $data['provinceId2'] = $configs['ProvinceId2'];
				}
				$this->load->view('customer/profile', $data);
			}
			else redirect('user/dashboard');
		}
		else redirect('user');
	}

	public function notification($notificationStatusId = 0){
		$user = $this->session->userdata('user');
		if($user) {
			if ($user['RoleId'] == 4) {
				$data = array(
					'title' => 'Thông báo',
					'user' => $user,
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' =>array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/notification.js')),
					'isCustomerPanel' => true
				);
				$postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
				$postData['CustomerId'] = $user['UserId'];
				$postData['IsCustomerSend'] = 0;
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$data['notificationStatusId'] = 0;
				if($notificationStatusId == 1 || $notificationStatusId == 2){
					$postData['NotificationStatusId'] = $notificationStatusId;
					$data['notificationStatusId'] = $notificationStatusId;
				}
				$this->load->model('Mnotifications');
				$data['statisticNotificationCounts'] = $this->Mnotifications->statisticCounts($postData, true);
				$rowCount = $this->Mnotifications->getCount($postData);
				$data['listNotifications'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listNotifications'] = $this->Mnotifications->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('customer/notification', $data);
			}
			else redirect('user/dashboard');
		}
		else redirect('user');
	}

	public function transaction(){
		$user = $this->session->userdata('user');
		if($user) {
			if ($user['RoleId'] == 4) {
				$data = array(
					'title' => 'Chi tiết giao dich',
					'user' => $user,
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' =>array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/customer_transaction.js')),
					'isCustomerPanel' => true
				);
				$postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$postData['CustomerId'] = $user['UserId'];
				$this->loadModel(array('Mcustomerbalances', 'Mtransactions', 'Morders'));
				$data['customerBalance'] = priceFormat($this->Mcustomerbalances->getCurentBalance($user['UserId']));
				$rowCount = $this->Mtransactions->getCount($postData);
				$data['listTransactions'] = array();
				if($rowCount > 0){
					$perPage = PHP_INT_MAX;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listTransactions'] = $this->Mtransactions->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('customer/transaction', $data);
			}
			else redirect('user/dashboard');
		}
		else redirect('user');
	}

	public function order($orderStatusId = 0){
		$user = $this->session->userdata('user');
		if($user) {
			if ($user['RoleId'] == 4) {
				$this->loadModel(array('Morders', 'Mproducts'));
				$data = array(
					'title' => 'Danh sách Đơn hàng',
					'user' => $user,
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' =>array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/customer_order.js')),
					'isCustomerPanel' => true
				);
				$postData = $this->arrayFromPost(array('ProductLink', 'OrderCode', 'WaybillCode', 'ComplaintStatusId', 'BeginDate', 'EndDate'));
				$postData['CustomerId'] = $user['UserId'];
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$data['orderStatusId'] = 0;
				if($orderStatusId > 0 && $orderStatusId <= count($this->Mconstants->orderStatus)){
					$postData['OrderStatusId'] = $orderStatusId;
					$data['orderStatusId'] = $orderStatusId;
				}
				$data['statisticOrderCounts'] = $this->Morders->statisticCounts(array('CustomerId' => $user['UserId']), $this->Mconstants->orderStatus, true);
				$rowCount = $this->Morders->getCount($postData);
				$data['listOrders'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listOrders'] = $this->Morders->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('customer/order', $data);
			}
			else redirect('user/dashboard');
		}
		else redirect('user');
	}

	public function complaint($complaintStatusId = 0){
		$user = $this->session->userdata('user');
		if($user) {
			if ($user['RoleId'] == 4) {
				$this->loadModel(array('Morders', 'Mproducts'));
				$data = array(
					'title' => 'Danh sách Khiếu nại',
					'user' => $user,
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' =>array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/complaint_list.js')),
					'isCustomerPanel' => true
				);
				$postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
				$postData['CustomerId'] = $user['UserId'];
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$data['complaintStatusId'] = 0;
				if($complaintStatusId > 0 && $complaintStatusId < count($this->Mconstants->complaintStatus)){
					$postData['ComplaintStatusId'] = $complaintStatusId;
					$data['complaintStatusId'] = $complaintStatusId;
				}
				$this->loadModel(array('Mcomplaints', 'Morders'));
				$data['statisticComplaintCounts'] = $this->Mcomplaints->statisticCounts($postData, $this->Mconstants->complaintStatus, true);
				$rowCount = $this->Mcomplaints->getCount($postData);
				$data['listComplaints'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listComplaints'] = $this->Mcomplaints->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('customer/complaint', $data);
			}
			else redirect('user/dashboard');
		}
		else redirect('user');
	}

	public function contact(){
		$user = $this->session->userdata('user');
		if($user) {
			if ($user['RoleId'] == 4) {
				$this->load->model('Marticles');
				$data = array(
					'title' => 'Liên hệ',
					'user' => $user,
					'isCustomerPanel' => true,
					'contactText' => $this->Marticles->getFieldValue(array('ArticleId' => 2), 'ArticleContent')
				);
				$this->load->view('customer/contact', $data);
			}
			else redirect('user/dashboard');
		}
		else redirect('user');
	}

	public function chat(){
		$user = $this->session->userdata('user');
		if($user) {
			if ($user['RoleId'] == 4) {
				$data = array(
					'title' => 'Chat với Nhân viên CSKH',
					'user' => $user,
					'scriptFooter' =>array('js' => 'ckfinder/ckfinder.js'),
					'isCustomerPanel' => true
				);
				$this->load->view('customer/chat', $data);
			}
			else redirect('user/chat');
		}
		else redirect('user');
	}

	public function getBalance(){
		$customerId = $this->input->post('CustomerId');
		if($customerId > 0){
			$this->load->model('Mcustomerbalances');
			echo priceFormat($this->Mcustomerbalances->getCurentBalance($customerId));
		}
		else echo 0;
	}
}
