<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trackingcrawl extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Mã vận đơn lấy về',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/tracking_crawl.js'))
				)
			);
			if($this->Mactions->checkAccess($data['listActions'], 'trackingcrawl')) {
				$postData = $this->arrayFromPost(array('Tracking', 'BeginDate', 'EndDate'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$this->load->model('Mtrackingcrawls');
				$rowCount = $this->Mtrackingcrawls->getCount($postData);
				$data['listTrackingCrawls'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listTrackingCrawls'] = $this->Mtrackingcrawls->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('order/tracking_crawl', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}
}
