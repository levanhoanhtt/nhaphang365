<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction extends MY_Controller {

	public function index(){
		redirect('user/dashboard');
	}
	//phieu thu (no name) chua duyet
	public function noName(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Phiếu thu NoName chưa duyệt',
				array('scriptFooter' => array('js' => 'js/transaction.js?20170715'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'transaction/noName')) {
				$data['deleteTransaction'] = $this->Mactions->checkAccess($listActions, 'transaction/deleteNoName');
				$data['changeStatus'] = $this->Mactions->checkAccess($listActions, 'transaction/editInvoice');
				$this->load->model('Mtransactions');
				$postData = array('StatusId' => 1, 'TransactionTypeId' => 1);
				$rowCount = $this->Mtransactions->getCount($postData);
				$data['listTransactions'] = array();
				$data['listCustomers'] = array();
				if($rowCount > 0){
					$data['listCustomers'] = $this->Musers->getListByRole(4);
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listTransactions'] = $this->Mtransactions->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('transaction/noname', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}
	//phieu thu
	public function invoice(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Phiếu thu',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/transaction.js?20170715'))
				)
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'transaction/invoice')) {
				$data['changeStatus'] = $this->Mactions->checkAccess($listActions, 'transaction/editInvoice');
				$data['listCustomers'] = $this->Musers->getListByRole(4);
				$this->loadModel(array('Mtransactions', 'Mwarehouses', 'Mmoneysources', 'Morders'));
				$whereStatus = array('StatusId' => STATUS_ACTIVED);
				$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
				$data['listMoneySources'] = $this->Mmoneysources->getBy($whereStatus);
				$postData = $this->arrayFromPost(array('TransactionCode', 'CustomerId', 'StatusId', 'WarehouseId', 'MoneySourceId', 'TransactionRangeId', 'BeginDate', 'EndDate'));
				$postData['TransactionCode'] = str_replace('#', '', $postData['TransactionCode']);
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				if(empty($postData['CustomerId'])) unset($postData['CustomerId']);
				$postData['TransactionTypeId'] = 1;
				$rowCount = $this->Mtransactions->getCount($postData);
				$data['listTransactions'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listTransactions'] = $this->Mtransactions->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('transaction/invoice', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}
	//phieu chi
	public function expense(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Phiếu chi',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/transaction.js?20170715'))
				)
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'transaction/expense')) {
				$data['changeStatus'] = $this->Mactions->checkAccess($listActions, 'transaction/editExpense');
				$data['listCustomers'] = $this->Musers->getListByRole(4);
				$this->loadModel(array('Mtransactions', 'Mwarehouses', 'Mmoneysources', 'Morders'));
				$whereStatus = array('StatusId' => STATUS_ACTIVED);
				$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
				$data['listMoneySources'] = $this->Mmoneysources->getBy($whereStatus);
				$postData = $this->arrayFromPost(array('TransactionCode', 'CustomerId', 'StatusId', 'WarehouseId', 'MoneySourceId', 'TransactionRangeId', 'BeginDate', 'EndDate'));
				$postData['TransactionCode'] = str_replace('#', '', $postData['TransactionCode']);
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				if(empty($postData['CustomerId'])) unset($postData['CustomerId']);
				$postData['TransactionTypeId'] = 2;
				$rowCount = $this->Mtransactions->getCount($postData);
				$data['listTransactions'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listTransactions'] = $this->Mtransactions->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('transaction/expense', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function add($transactionTypeName = ''){
		$user = $this->session->userdata('user');
		if($user && in_array($transactionTypeName, array('invoice', 'expense'))) {
			$data = $this->commonData($user,
				'Thêm Phiếu '. (($transactionTypeName == 'invoice') ? 'thu' : 'chi'),
				array('scriptFooter' => array('js' => array('js/jquery.price_format.min.js', 'js/transaction_update.js?20170624')))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'transaction/add/'.$transactionTypeName)) {
				//$isInvoice = ($transactionTypeName == 'invoice') ? false : true;
				//$data['isInvoice'] = $isInvoice;
				$data['listCustomers'] = $this->Musers->getListByRole(4);
				$this->loadModel(array('Mwarehouses', 'Mmoneysources', 'Mconfigs'));
				$data['exchangeRate'] = $this->Mconfigs->getFieldValue(array('ConfigCode' => 'EXCHAGE_RATE_CN'), 'ConfigValue', 3456);
				$whereStatus = array('StatusId' => STATUS_ACTIVED);
				$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
				$data['listMoneySources'] = $this->Mmoneysources->getBy($whereStatus);
				$this->load->view('transaction/add_'.$transactionTypeName, $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function edit($transactionId = 0){
		if($transactionId > 0) {
			$user = $this->session->userdata('user');
			if ($user) {
				$data = $this->commonData($user,
					'Cập nhật Phiếu ',
					array('scriptFooter' => array('js' => array('js/jquery.price_format.min.js', 'js/transaction_update.js?20170624')))
				);
				$this->loadModel(array('Mtransactions', 'Mwarehouses', 'Mmoneysources'));
				$transaction = $this->Mtransactions->get($transactionId);
				if ($transaction) {
					$actionName = $transaction['TransactionTypeId'] == 1 ? 'transaction/viewInvoice' : 'transaction/viewExpense';
					$listActions = $data['listActions'];
					if ($this->Mactions->checkAccess($listActions, $actionName)) {
						$canAsignCustomer = false;
						if($transaction['StatusId'] == STATUS_ACTIVED){
							$canEdit = false;
							if($transaction['CustomerId'] == 0) $canAsignCustomer = true;
						}
						else {
							$actionName = $transaction['TransactionTypeId'] == 1 ? 'transaction/editInvoice' : 'transaction/editExpense';
							$canEdit = $this->Mactions->checkAccess($listActions, $actionName);
							if(!$canEdit && $transaction['CustomerId'] == 0) $canAsignCustomer = true;
						}
						$data['canEdit'] = $canEdit;
						$data['canAsignCustomer'] = $canAsignCustomer;
						$data['title'] .= ($transaction['TransactionTypeId'] == 1 ? 'thu ' : 'chi ').'#'.$transactionId;
						$data['transactionId'] = $transactionId;
						$data['transaction'] = $transaction;
						$data['listCustomers'] = $this->Musers->getListByRole(4);
						$whereStatus = array('StatusId' => STATUS_ACTIVED);
						$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
						$data['listMoneySources'] = $this->Mmoneysources->getBy($whereStatus);
						$data['crFullName'] = $this->Musers->getFieldValue(array('UserId' => $transaction['CrUserId']), 'FullName');
						$updateFullName = '';
						if($transaction['UpdateUserId'] > 0) $updateFullName = $this->Musers->getFieldValue(array('UserId' => $transaction['UpdateUserId']), 'FullName');
						$data['updateFullName'] = $updateFullName;
						$this->load->view('transaction/edit_'.($transaction['TransactionTypeId'] == 1 ? 'invoice' : 'expense'), $data);
					}
					else $this->load->view('user/permission', $data);
				}
				else {
					$data['transactionId'] = 0;
					$data['txtError'] = "Không tìm thấy Phiếu thu/ chi";
					$this->load->view('transaction/edit_invoice', $data);
				}
			}
			else redirect('user');
		}
		else redirect('transaction');
	}

	public function customer($customerId = 0){
		$user = $this->session->userdata('user');
		if($user) {
			$data = $this->commonData($user,
				'Tài chính Khách hàng',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/transaction_customer.js?20170715')),
					'customerId' => $customerId
				)
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'transaction/customer')) {
				$customer = false;
				if($customerId > 0) $customer = $this->Musers->get($customerId);
				$data['customer'] = $customer;
				if($customer){
					$postData = $this->arrayFromPost(array('TransactionCode', 'BeginDate', 'EndDate'));
					$postData['TransactionCode'] = str_replace('#', '', $postData['TransactionCode']);
					if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
					if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
					$postData['CustomerId'] = $customerId;
					$this->loadModel(array('Mprovinces', 'Mtransactions', 'Mcustomerbalances', 'Morders'));
					$data['customerBalance'] = priceFormat($this->Mcustomerbalances->getCurentBalance($customerId));
					$data['listCustomers'] = $this->Musers->getListByRole(4);
					$data['listProvinces'] = $this->Mprovinces->getList();
					$data['listTransactions'] = array();
					$rowCount = $this->Mtransactions->getCount($postData);
					if ($rowCount > 0) {
						$perPage = PHP_INT_MAX;
						$pageCount = ceil($rowCount / $perPage);
						$page = $this->input->post('PageId');
						if(!is_numeric($page) || $page < 1) $page = 1;
						$data['listTransactions'] = $this->Mtransactions->search($postData, $perPage, $page);
						$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
					}
				}
				else{
					$data['customerId'] = 0;
					$data['txtError'] = 'Khách hàng không tồn tại';
				}
				$this->load->view('transaction/customer', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('transaction');
	}

	public function customer2($customerId = 0){
		$user = $this->session->userdata('user');
		if($user) {
			$data = $this->commonData($user,
				'Tài chính Khách hàng',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/transaction_customer2.js')),
					'customerId' => $customerId
				)
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'transaction/customer')) {
				$customer = false;
				if($customerId > 0) $customer = $this->Musers->get($customerId);
				$data['customer'] = $customer;
				if($customer){
					$postData = $this->arrayFromPost(array('TransactionCode', 'TransactionRangeId', 'BeginDate', 'EndDate'));
					$postData['TransactionCode'] = str_replace('#', '', $postData['TransactionCode']);
					if (!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
					if (!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
					$postData['CustomerId'] = $customerId;
					$this->loadModel(array('Mprovinces', 'Mtransactions', 'Mcustomerbalances', 'Morders'));
					$data['customerBalance'] = priceFormat($this->Mcustomerbalances->getCurentBalance($customerId));
					$data['listCustomers'] = $this->Musers->getListByRole(4);
					$data['listProvinces'] = $this->Mprovinces->getList();
					$data['listTransactions'] = array();
					$rowCount = $this->Mtransactions->getCount($postData);
					if ($rowCount > 0) {
						$perPage = PHP_INT_MAX;
						$pageCount = ceil($rowCount / $perPage);
						$page = $this->input->post('PageId');
						if(!is_numeric($page) || $page < 1) $page = 1;
						$data['listTransactions'] = $this->Mtransactions->search($postData, $perPage, $page);
						$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
					}
				}
				else{
					$data['customerId'] = 0;
					$data['txtError'] = 'Khách hàng không tồn tại';
				}
				$this->load->view('transaction/customer2', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('transaction');
	}

	public function verify(){
		$user = $this->session->userdata('user');
		if ($user) {
			$data = $this->commonData($user,
				'Danh sách duyệt Đơn hàng + Tài chính',
				array('scriptFooter' => array('js' => 'js/transaction_verify.js?20170615'))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'transaction/verify')) {
			//if($user['RoleId'] == 3 || $user['RoleId'] == 6){
				$this->loadModel(array('Morders', 'Mtransactions'));
				$data['listOrders'] = $this->Morders->getListVerify($user['RoleId']);
				$data['listTransactions'] = $this->Mtransactions->getListVerify($user['RoleId']);
				if($user['RoleId'] == 6) $this->load->view('transaction/verify', $data);
				else $this->load->view('transaction/admin_verify', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function changeStatus(){
		$user = $this->session->userdata('user');
		if ($user) {
			$transactionId = $this->input->post('TransactionId');
			$statusId = $this->input->post('StatusId');
			if ($transactionId > 0 && $statusId >= 0 && $statusId >= 0 && $statusId < 5) {
				$this->loadModel(array('Mcustomerbalances', 'Mtransactions'));
				$postData = array(
					'CustomerId' => $this->input->post('CustomerId'),
					'OrderId' => $this->input->post('OrderId'),
					'TransactionTypeId' => $this->input->post('TransactionTypeId'),
					'PaidVN' => $this->input->post('PaidVN'),
					'Balance' => $this->input->post('Balance'),
					'StatusId' => $statusId,
					'UpdateUserId' => $user['UserId'],
					'UpdateDateTime' => getCurentDateTime()
				);
				if($statusId == 2) $postData['Balance'] = $this->Mtransactions->getNewBalance($postData['CustomerId'], $postData['TransactionTypeId'], $postData['PaidVN']);
				$flag = $this->Mtransactions->update($postData, $transactionId, true);
				if ($flag) {
					$statusName = "";
					if ($statusId == 0) $txtSuccess = "Xóa {$this->input->post('TransactionTypeName')} thành công";
					else {
						$txtSuccess = "Đổi trạng thái thành công";
						if($statusId == 4) $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">KT đã duyệt</span>';
						else $statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
					}
					echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function update(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('CustomerId', 'OrderId', 'ComplaintId', 'StatusId', 'TransactionTypeId', 'TransactionRangeId', 'WarehouseId', 'MoneySourceId', 'Comment', 'Confirm', 'PaidTQ', 'ExchangeRate', 'PaidVN'));
			$postData['PaidTQ'] = replacePrice($postData['PaidTQ']);
			if($postData['ExchangeRate'] == 1) unset($postData['PaidTQ']);
			$postData['ExchangeRate'] = replacePrice($postData['ExchangeRate']);
			$postData['PaidVN'] = replacePrice($postData['PaidVN']);
			$updateDateTime = getCurentDateTime();
			$transactionId = $this->input->post('TransactionId');
			if($transactionId > 0){
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = $updateDateTime;
			}
			else{
				$postData['PaidDateTime'] = $updateDateTime;
				$postData['CrUserId'] = $user['UserId'];
				$postData['CrDateTime'] = $updateDateTime;
			}
			$this->loadModel(array('Mcustomerbalances', 'Mtransactions'));
			/*if($postData['CustomerId'] > 0 && $postData['PaidVN'] > 0 && $postData['StatusId'] == STATUS_ACTIVED) $postData['Balance'] = $this->Mtransactions->getNewBalance($postData['CustomerId'], $postData['TransactionTypeId'], $postData['PaidVN']);
			else $postData['Balance'] = 0;*/
			$transactionId = $this->Mtransactions->update($postData, $transactionId);
			if ($transactionId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật {$this->Mconstants->transactionTypes[$postData['TransactionTypeId']]} thành công", 'data' => $transactionId));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateBalance(){
		$user = $this->session->userdata('user');
		if ($user) {
			$transactionId = $this->input->post('TransactionId');
			if($transactionId > 0) {
				$postData = $this->arrayFromPost(array('PaidDateTime', 'Balance'));
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = getCurentDateTime();;
				$this->load->model('Mtransactions');
				$transactionId = $this->Mtransactions->update($postData, $transactionId, true);
				if ($transactionId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật {$this->input->post('TransactionTypeName')} thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}