<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Complaint extends MY_Controller {

	public function index($complaintStatusId = 0){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Khiếu nại',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/complaint_list.js'))
				)
			);
			$listActions = $data['listActions'];
			if ($this->Mactions->checkAccess($listActions, 'complaint')) {
				$data['deleteComplaint'] = $this->Mactions->checkAccess($listActions, 'complaint/delete');
				$listUserAlls = $this->Musers->getListByRole(0, true);
				$listUsers = array();
				$listCustomers = array();
				foreach($listUserAlls as $u){
					if($u['RoleId'] == 4) $listCustomers[]=$u;
					elseif($u['RoleId'] == 1){
						if($user['RoleId'] == 5 || $user['RoleId'] == 8) $listUsers[]=$u;
					}
					elseif($u['RoleId'] == 5 || $u['RoleId'] == 8){
						if($user['RoleId'] == 1) $listUsers[]=$u;
					}
					if($user['RoleId'] == 3 && $u['RoleId'] != 4) $listUsers[]=$u;
				}
				$data['listUsers'] = $listUsers;
				$data['listCustomers'] = $listCustomers;
				$postData = $this->arrayFromPost(array('CareStaffId', 'OrderUserId', 'UserHandelId', 'CustomerId', 'BeginDate', 'EndDate'));
				if($user['RoleId'] == 1) $postData['CareStaffId'] = $user['UserId'];
				elseif($user['RoleId'] == 5) $postData['OrderUserId'] = $user['UserId'];
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				if(empty($postData['OrderUserId']) || $postData['OrderUserId'] == 0) $postData['OrderUserId'] = -1;
				$data['complaintStatusId'] = 0;
				if($complaintStatusId > 0 && $complaintStatusId < count($this->Mconstants->complaintStatus)){
					$postData['ComplaintStatusId'] = $complaintStatusId;
					$data['complaintStatusId'] = $complaintStatusId;
				}
				$this->loadModel(array('Mcomplaints', 'Morders'));
				$data['statisticComplaintCounts'] = $this->Mcomplaints->statisticCounts($postData, $this->Mconstants->complaintStatus, true);
				$rowCount = $this->Mcomplaints->getCount($postData);
				$data['listComplaints'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listComplaints'] = $this->Mcomplaints->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('order/complaintlist', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function add(){
		$user = $this->session->userdata('user');
		if($user) {
			$postData = $this->arrayFromPost(array('OrderId', 'ProductId', 'ComplaintStatusId', 'ComplaintTypes', 'CustomerId', 'CareStaffId', 'OrderUserId', 'ProductImage', 'WaybillCodeImage', 'Comment', 'ComplaintOwnerId', 'ComplaintSolutions', 'ExchangeRate', 'ExchangeCostTQ', 'ReduceCostTQ'));
			if ($postData['OrderId'] > 0 && $postData['ProductId'] > 0 && $postData['CareStaffId'] > 0 && $postData['CustomerId'] > 0) {
				$postData['ProductImage'] = replaceFileUrl($postData['ProductImage'], PRODUCT_PATH);
				$postData['WaybillCodeImage'] = replaceFileUrl($postData['WaybillCodeImage'], PRODUCT_PATH);
				$postData['CrDateTime'] = getCurentDateTime();
				$this->load->model('Mcomplaints');
				$complaintId = $this->Mcomplaints->update(0, $postData, $user['UserId'], $this->input->post('OrderCode'));
				if ($complaintId > 0) echo json_encode(array('code' => 1, 'message' => "Thêm khiếu nại thành công", 'data' => $complaintId));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function update(){
		$user = $this->session->userdata('user');
		if($user) {
			$complaintId = $this->input->post('ComplaintId');
			if ($complaintId > 0) {
				$postData = $this->arrayFromPost(array('OrderId', 'ProductId', 'CustomerId', 'CareStaffId', 'ComplaintStatusId', 'ComplaintOwnerId', 'ComplaintSolutions', 'ExchangeRate', 'ExchangeCostTQ', 'ReduceCostTQ'));
				$postDataMsg = $this->arrayFromPost(array('UserId', 'Message', 'ComplaintMsgTypeId', 'IsAccept'));
				$postDataMsg['ComplaintId'] = $complaintId;
				$crDateTime = getCurentDateTime();
				$postDataMsg['CrDateTime'] = $crDateTime;
				$this->load->model('Mcomplaints');
				$flag = $this->Mcomplaints->update($complaintId, $postData, $user['UserId'], $this->input->post('OrderCode'), $postDataMsg);
				if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật khiếu nại thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function delete(){
		$complaintId = $this->input->post('ComplaintId');
		if($complaintId > 0){
			$this->load->model('Mcomplaints');
			$flag = $this->Mcomplaints->changeStatus(0, $complaintId, 'ComplaintStatusId');
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Khiếu nại thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Xóa Khiếu nại thất bại"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getDetail(){
		$complaintId = $this->input->post('ComplaintId');
		if($complaintId > 0){
			$this->loadModel(array('Mcomplaints', 'Mproducts', 'Mcomplaintmsgs'));
			$complaint = $this->Mcomplaints->get($complaintId);
			if($complaint){
				$product = $this->Mproducts->get($complaint['ProductId'], true, "", "ProductId, ProductLink, ProductImage, Color, Size, Quantity, Cost");
				if($product){
					$complaint['CrDateTime'] = ddMMyyyy($complaint['CrDateTime'], 'H:i d/m/Y');
					$product['Sum'] = priceFormat($product['Quantity'] * $product['Cost'], false);
					$product['Cost'] = priceFormat($product['Cost'], false);
					$listComplaintMsgs = $this->Mcomplaintmsgs->getBy(array('ComplaintId' => $complaintId));
					$complaintMsgs = array();
					$fullNames = array();
					foreach($listComplaintMsgs as $cm){
						$cm['CrDateTime'] = ddMMyyyy($cm['CrDateTime'], 'H:i d/m/Y');
						$userId = $cm['UserId'];
						if(!isset($fullNames[$userId])) $fullNames[$userId] = $this->Musers->getFieldValue(array('UserId' => $userId), 'FullName');
						$cm['FullName'] = $fullNames[$userId];
						$complaintMsgs[]=$cm;
					}
					echo json_encode(array('code' => 1, 'data' => array('Complaint' => $complaint, 'Product' => $product, 'ComplaintMsgs' => $complaintMsgs)));
				}
				else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy sản phẩm"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Không tìm thấy khiếu nại"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getListComplaintByCustomer(){
		if ($this->session->userdata('user')) {
			$orderId = $this->input->post('OrderId');
			$customerId = $this->input->post('CustomerId');
			if($orderId > 0 && $customerId > 0){
				$this->load->model('Mcomplaints');
				$listComplaintIds = $this->Mcomplaints->getBy(array('CustomerId' => $customerId, 'OrderId' => $orderId, 'ComplaintStatusId >' => 0), false, "", "ComplaintId");
				echo json_encode(array('code' => 1, 'data' => $listComplaintIds));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function statistics(){
		$user = $this->session->userdata('user');
		$complaintStatusIds = trim($this->input->post('ComplaintStatusIds'));
		if(!empty($complaintStatusIds)) $complaintStatusIds = explode(',', $complaintStatusIds);
		if($user && !empty($complaintStatusIds)){
			$where = array();
			if($user['RoleId'] == 1) $where = array('CareStaffId' => $user['UserId']);
			elseif($user['RoleId'] == 5) $where = array('OrderUserId' => $user['UserId']);
			$complaintStatusStatistics = array();
			foreach($complaintStatusIds as $id) $complaintStatusStatistics[$id] = '';
			$isStatisticAll = ($this->input->post('IsStatisticAll') == 1) ? true : false;
			$this->load->model('Mcomplaints');
			$statistics = $this->Mcomplaints->statisticCounts($where, $complaintStatusStatistics, $isStatisticAll);
			$data = array();
			foreach($statistics as $i => $v) $data['Count_'.$i] = $v;
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
