<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transportuser extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Nhân viên giao hàng',
				array('scriptFooter' => array('js' => 'js/transport_user.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'transportuser')) {
				$data['updateTransportUser'] = $this->Mactions->checkAccess($listActions, 'transportuser/update');
				$data['deleteTransportUser'] = $this->Mactions->checkAccess($listActions, 'transportuser/delete');
				$this->load->model('Mtransportusers');
				$data['listTransportUsers'] = $this->Mtransportusers->getBy(array('StatusId' => STATUS_ACTIVED));
				$this->load->view('setting/transportuser', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('TransportUserName'));
		if (!empty($postData['TransportUserName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$transportUserId = $this->input->post('TransportUserId');
			$this->load->model('Mtransportusers');
			$flag = $this->Mtransportusers->save($postData, $transportUserId);
			if ($flag > 0) {
				$postData['TransportUserId'] = $flag;
				$postData['IsAdd'] = ($transportUserId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Nhân viên giao hàng thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$transportUserId = $this->input->post('TransportUserId');
		if ($transportUserId > 0) {
			$this->load->model('Mtransportusers');
			$flag = $this->Mtransportusers->changeStatus(0, $transportUserId);
			if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Nhân viên giao hàng thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
