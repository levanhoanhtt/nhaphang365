<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Useraction extends MY_Controller {

	public function index($userId = 0){
		$user = $this->session->userdata('user');
		if($user){
			if($userId > 0) {
				$data = $this->commonData($user,
					'Cấp quyên truy cập',
					array('scriptFooter' => array('js' => 'js/page_user_action.js'))
				);
				if ($this->Mactions->checkAccess($data['listActions'], 'useraction')) {
					$userEdit = $this->Musers->get($userId, true, "", "FullName, RoleId");
					$data['fullName'] = '';
					if($userEdit && $userEdit['RoleId'] != 4) {
						$data['fullName'] = $userEdit['FullName'];
						$data['userId'] = $userId;
						$data['listActiveActions'] = $this->Mactions->getHierachy();
						$this->db->reconnect();
						$data['listUserActions'] = $this->Mactions->getByUserId($userId);
						if ($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
						if ($this->session->flashdata('txtError')) $data['txtError'] = $this->session->flashdata('txtError');
					}
					else{
						$data['userId'] = 0;
						$data['listActiveActions'] = $data['listUserActions'] = array();
						$data['txtError'] = "Không tìm thấy nhân viên";
					}
					$this->load->view('user/useraction', $data);
				}
				else $this->load->view('user/permission', $data);
			}
			else redirect('user/profile');
		}
		else redirect('user');
	}

	public function update($userId = 0){
		if($userId > 0) {
			$user = $this->session->userdata('user');
			if ($user) {
				$roleId = $this->Musers->get_any_field(array('UserId' => $userId), 'RoleId');
				if($roleId != 4) {
					$listActiveActions = $this->Mactions->get_by(array('StatusId' => STATUS_ACTIVED));
					$valueData = array();
					$crDateTime = getCurentDateTime();
					foreach ($listActiveActions as $act) {
						if ($this->input->post('Checkbox' . $act['ActionId']) == 'on'){
							$valueData[] = array('UserId' => $userId, 'ActionId' => $act['ActionId'], 'CrUserId' => $user['UserId'], 'CrDateTime' => $crDateTime);
						}
					}
					$this->load->model('Museractions');
					$flag = $this->Museractions->updateBatch($userId, $valueData);
					if($flag) $this->session->set_flashdata('txtSuccess', "Cấp quyên truy cập thành công");
					else $this->session->set_flashdata('txtError', "Có lỗi xảy ra trong quá trình thực hiện");
					redirect('useraction/'.$userId);
				}
				else redirect('useraction');
			}
			else redirect('user');
		}
		else redirect('user/profile');
	}
}
