<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends MY_Controller{

    public function index(){
        $this->loadModel(array('Mconfigs', 'Mcustomerbalances', 'Msliders', 'Mcategories', 'Marticles'));
        $configs = $this->session->userdata('configs');
        if (!$configs) {
            $configs = $this->Mconfigs->getListMap();
            $this->session->set_userdata('configs', $configs);
        }
        $siteName = (isset($configs['SITE_NAME']) ? $configs['SITE_NAME'] : 'Nhập hàng 365');
        $data = array(
            'title' => 'Trang chủ - ' . $siteName,
            'userName' => '',
            'userPass' => '',
            'userId' => 0,
            'fullName' => $siteName,
            'userAvatar' => NO_IMAGE,
            //'balance' => 0,
            'listSliders' => $this->Msliders->getList(),
            'listProducts' => $this->Mcategories->getByCategoryTypeId(array(3, 4, 5)),
            'listArticleFooters' => $this->Marticles->search(array('ArticleTypeId' => 2), 3, 1, 'PublishDateTime')
        );
        $user = $this->session->userdata('user');
        if ($user) {
            $data['userId'] = $user['UserId'];
            $data['fullName'] = $user['FullName'];
            $data['userAvatar'] = empty($user['Avatar']) ? NO_IMAGE : $user['Avatar'];
            //$data['balance'] = priceFormat($this->Mcustomerbalances->getCurentBalance($user['UserId']));
        }
        else {
            $this->load->helper('cookie');
            $data['userName'] = $this->input->cookie('userName', true);
            $data['userPass'] = $this->input->cookie('userPass', true);
        }
        $this->load->view('site/index', $data);
    }

    public function article($articleSlug = '', $articleId = 0){
        if($articleId > 0 && !empty($articleSlug)){
            $this->loadModel(array('Mconfigs', 'Marticles'));
            $article = $this->Marticles->get($articleId);
            if($article){
                if($article['ArticleSlug'] == $articleSlug){
                    $configs = $this->session->userdata('configs');
                    if(!$configs){
                        $configs = $this->Mconfigs->getListMap();
                        $this->session->set_userdata('configs', $configs);
                    }
                    $siteName = (isset($configs['SITE_NAME']) ? $configs['SITE_NAME'] : 'Nhập hàng 365');
                    $data = array(
                        'title' => $article['ArticleTitle'] . ' - '. $siteName,
                        'userId' => 0,
                        'userName' => '',
                        'userPass' => '',
                        'fullName' => $siteName,
                        'userAvatar' => NO_IMAGE,
                        'article' => $article,
                        'listArticleFooters' => $this->Marticles->search(array('ArticleTypeId' => 2), 3, 1, 'PublishDateTime')
                    );
                    $data['listArticleSidebars'] = $data['listArticleFooters'];
                    $user = $this->session->userdata('user');
                    if($user){
                        $data['userId'] = $user['UserId'];
                        $data['fullName'] = $user['FullName'];
                        $data['userAvatar'] = empty($user['Avatar']) ? NO_IMAGE : $user['Avatar'];
                    }
                    else {
                        $this->load->helper('cookie');
                        $data['userName'] = $this->input->cookie('userName', true);
                        $data['userPass'] = $this->input->cookie('userPass', true);
                    }
                    $this->load->view('site/article', $data);
                }
                else redirect(base_url($article['ArticleSlug'].'-p'.$articleId));
            }
            else redirect(base_url());
        }
        else redirect(base_url());
    }

    public function category($categorySlug = '', $categoryId = 0, $pageId = 1){
        if($categoryId > 0 && !empty($categorySlug)){
            $this->loadModel(array('Mconfigs', 'Mcategories', 'Marticles'));
            $category = $this->Mcategories->get($categoryId);
            if($category && $category['CategoryTypeId'] == 2){
                if($category['CategoryNameTQ'] == $categorySlug){
                    if(!is_numeric($pageId)) $pageId = 1;
                    if($pageId < 1) $pageId = 1;
                    $configs = $this->session->userdata('configs');
                    if(!$configs){
                        $configs = $this->Mconfigs->getListMap();
                        $this->session->set_userdata('configs', $configs);
                    }
                    $siteName = (isset($configs['SITE_NAME']) ? $configs['SITE_NAME'] : 'Nhập hàng 365');
                    $perPage = 10;
                    $articleCount = $this->Marticles->getCount(array('ArticleTypeId' => 2, 'CategoryId' => $categoryId));
                    $data = array(
                        'title' => $category['CategoryName'] . ' - '. $siteName,
                        'userId' => 0,
                        'userName' => '',
                        'userPass' => '',
                        'fullName' => $siteName,
                        'userAvatar' => NO_IMAGE,
                        'categoryUrl' => base_url($categorySlug.'-c'.$categoryId).'-trang-',
                        'pageId' => $pageId,
                        'pageCount' => ceil($articleCount / $perPage),
                        'listArticles' => $this->Marticles->search(array('ArticleTypeId' => 2, 'CategoryId' => $categoryId), $perPage, $pageId, 'PublishDateTime'),
                        'listArticleFooters' => $this->Marticles->search(array('ArticleTypeId' => 2), 3, 1, 'PublishDateTime'),
                        'listCategorySidebars' => $this->Mcategories->getListChild(1)
                    );
                    $data['listArticleSidebars'] = $data['listArticleFooters'];
                    $user = $this->session->userdata('user');
                    if($user){
                        $data['userId'] = $user['UserId'];
                        $data['fullName'] = $user['FullName'];
                        $data['userAvatar'] = empty($user['Avatar']) ? NO_IMAGE : $user['Avatar'];
                    }
                    else {
                        $this->load->helper('cookie');
                        $data['userName'] = $this->input->cookie('userName', true);
                        $data['userPass'] = $this->input->cookie('userPass', true);
                    }
                    $this->load->view('site/category', $data);
                }
                else redirect(base_url($category['CategoryNameTQ'].'-c'.$categoryId));
            }
            else redirect(base_url());
        }
        else redirect(base_url());
    }

    public function getListProvinces(){
        $this->load->model('Mprovinces');
        echo json_encode($this->Mprovinces->getList());
    }
}