<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MY_Controller {

	public function index($notificationStatusId = 0){
		$user = $this->session->userdata('user');
		if($user) {
			$data = $this->commonData($user,
				'Danh sách Thông báo',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/notification.js'))
				)
			);
			$postData = $this->arrayFromPost(array('BeginDate', 'EndDate'));
			$postData['UserId'] = $user['UserId'];
			$postData['RoleId'] = $user['RoleId'];
			if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
			if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
			$data['notificationStatusId'] = 0;
			if($notificationStatusId == 1 || $notificationStatusId == 2){
				$postData['NotificationStatusId'] = $notificationStatusId;
				$data['notificationStatusId'] = $notificationStatusId;
			}
			$this->load->model('Mnotifications');
			$data['statisticNotificationCounts'] = $this->Mnotifications->statisticCounts($postData, true);
			$rowCount = $this->Mnotifications->getCount($postData);
			$data['listNotifications'] = array();
			if($rowCount > 0){
				$perPage = 20;
				$pageCount = ceil($rowCount / $perPage);
				$page = $this->input->post('PageId');
				if(!is_numeric($page) || $page < 1) $page = 1;
				$data['listNotifications'] = $this->Mnotifications->search($postData, $perPage, $page);
				$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
			}
			$this->load->view('user/notification', $data);
		}
		else redirect('user');
	}

	public function changeStatus(){
		$user = $this->session->userdata('user');
		if ($user) {
			$notificationId = $this->input->post('NotificationId');
			$notificationStatusId = $this->input->post('NotificationStatusId');
			if($notificationId > 0 && $notificationStatusId >= 0 && $notificationStatusId <= 2){
				$this->load->model('Mnotifications');
				$flag = $this->Mnotifications->changeStatus($notificationStatusId, $notificationId, 'NotificationStatusId');
				if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật thông báo thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeAllStaus(){
		$user = $this->session->userdata('user');
		$notificationStatusId = $this->input->post('NotificationStatusId');
		if ($user && $notificationStatusId >= 0 && $notificationStatusId <= 2) {
			$this->load->model('Mnotifications');
			$this->Mnotifications->changeAllStaus($user['UserId'], $user['RoleId'], $notificationStatusId);
			echo json_encode(array('code' => 1, 'message' => "Cập nhật thông báo thành công"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getListForHeader(){
		$userId = $this->input->post('UserId');
		$roleId = $this->input->post('RoleId');
		if($userId > 0 && $roleId > 0){
			$this->load->model('Mnotifications');
			if($roleId == 4){
				$listNotifications = $this->Mnotifications->search(array('CustomerId' => $userId, 'IsCustomerSend' => 0, 'NotificationStatusId' => 1), PHP_INT_MAX, 1);
				$data = array();
				foreach($listNotifications as $n){
					$n['CrTime'] = ddMMyyyy($n['CrDateTime'], 'H:i:s');
					$n['CrDate'] = ddMMyyyy($n['CrDateTime'], 'd/m/Y');
					if(!empty($n['LinkTo'])) $n['LinkTo'] = base_url($n['LinkTo']);
					$data[]=$n;
				}
				echo json_encode(array('code' => 1, 'data' => $data));
			}
			else{
				$listNotifications = $this->Mnotifications->search(array('UserId' => $userId, 'RoleId' => $roleId, 'IsCustomerSend' => 1, 'NotificationStatusId' => 1), PHP_INT_MAX, 1);
				$data = array();
				$customers = array();
				foreach($listNotifications as $n){
					$n['CrTime'] = ddMMyyyy($n['CrDateTime'], 'H:i:s');
					$n['CrDate'] = ddMMyyyy($n['CrDateTime'], 'd/m/Y');
					if(!empty($n['LinkTo'])) $n['LinkTo'] = base_url($n['LinkTo']);
					if(!isset($customers[$n['CustomerId']])) {
						$customer = $this->Musers->get($n['CustomerId'], true, "", "FullName, Avatar");
						$customers[$n['CustomerId']] = $customer;
					}
					else $customer = $customers[$n['CustomerId']];
					if($customer) {
						$n['CustomerName'] = $customer['FullName'];
						$n['CustomerAvatar'] = empty($customer['Avatar']) ? NO_IMAGE : $customer['Avatar'];
					}
					else{
						$n['CustomerName'] = '';
						$n['CustomerAvatar'] = NO_IMAGE;
					}
					$data[]=$n;
				}
				echo json_encode(array('code' => 1, 'data' => $data));
			}
		}
		else echo json_encode(array('code' => -1));
	}

	public function statistics(){
		$user = $this->session->userdata('user');
		if($user){
			$where = array();
			if($user['RoleId'] == 4) $where = array('CustomerId' => $user['UserId']);
			else $where = array('UserId' => $user['UserId']);
			$isStatisticAll = ($this->input->post('IsStatisticAll') == 1) ? true : false;
			$this->load->model('Mnotifications');
			$statistics = $this->Mnotifications->statisticCounts($where, $isStatisticAll);
			$data = array();
			foreach($statistics as $i => $v) $data['Count_'.$i] = $v;
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
