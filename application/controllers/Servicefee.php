<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicefee extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Phí dịch vụ',
				array('scriptFooter' => array('js' => 'js/service_fee.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'servicefee')) {
				$data['updateServiceFee'] = $this->Mactions->checkAccess($listActions, 'servicefee/update');
				$data['deleteServiceFee'] = $this->Mactions->checkAccess($listActions, 'servicefee/delete');
				$this->load->model('Mservicefees');
				$data['listServiceFees'] = $this->Mservicefees->getBy(array('StatusId' => STATUS_ACTIVED));
				$this->load->view('setting/servicefee', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$user = $this->session->userdata('user');
		if($user) {
			$postData = $this->arrayFromPost(array('BeginCost', "EndCost", 'Percent'));
			if ($postData['EndCost'] > 0 && $postData['Percent'] > 0) {
				$postData['StatusId'] = STATUS_ACTIVED;
				$serviceFeeId = $this->input->post('ServiceFeeId');
				if($serviceFeeId == 0) {
					$postData['CrUserId'] = $user['UserId'];
					$postData['CrDateTime'] = getCurentDateTime();
				}
				else{
					$postData['UpdateUserId'] = $user['UserId'];
					$postData['UpdateDateTime'] = getCurentDateTime();
				}
				$this->load->model('Mservicefees');
				$flag = $this->Mservicefees->update($serviceFeeId, $postData);
				if ($flag > 0) {
					$postData['ServiceFeeId'] = $flag;
					$postData['IsAdd'] = ($serviceFeeId > 0) ? 0 : 1;
					echo json_encode(array('code' => 1, 'message' => "Cập nhật phí dịch vụ thành công", 'data' => $postData));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$user = $this->session->userdata('user');
		if($user) {
			$serviceFeeId = $this->input->post('ServiceFeeId');
			if ($serviceFeeId > 0) {
				$this->load->model('Mservicefees');
				$flag = $this->Mservicefees->update($serviceFeeId, array('StatusId' => 0, 'UpdateUserId' => $user['UserId'], 'UpdateDateTime' => getCurentDateTime()), true);
				if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa phí dịch vụ thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
