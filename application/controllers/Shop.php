<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shop extends MY_Controller {

	public function paid(){
		$user = $this->session->userdata('user');
		if ($user) {
			$data = $this->commonData($user,
				'Shop cần thanh toán',
				array('scriptFooter' => array('js' => 'js/shop_paid.js?20170727'))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'shop/paid')) {
				$this->loadModel(array('Morders', 'Morderaccounts', 'Mshops', 'Mshopnumbers', 'Mbankaccounts'));
				$data['listOrders'] = $this->Morders->search(array('OrderStatusIds' => '5,6,10'), PHP_INT_MAX, 1, 'OrderId, OrderCode, OrderDate');
				$data['listOrderAccounts'] = $this->Morderaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$data['listBankAccounts'] = $this->Mbankaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$data['listOrderUsers'] = $this->Musers->getListByRole(array(5, 8));
				$postData = $this->arrayFromPost(array('ShopNo', 'OrderId', 'OrderAccountId', 'OrderUserId'));
				$postData['ShopPaidStatusId'] = 2;
				$postData['NoIsReturn'] = 1;
				$data['listShopnNumbers'] = array();
				$rowCount = $this->Mshopnumbers->getCount($postData);
				$data['shopNumberCount'] = $rowCount;
				if ($rowCount > 0) {
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listShopnNumbers'] = $this->Mshopnumbers->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('shop/paid', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function paidDone(){
		$user = $this->session->userdata('user');
		if ($user) {
			$data = $this->commonData($user,
				'Shop đã thanh toán',
				array('scriptFooter' => array('js' => 'js/shop_paid.js?20170727'))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'shop/paid')) {
				$this->loadModel(array('Morders', 'Morderaccounts', 'Mshops', 'Mshopnumbers'));
				$data['listOrders'] = $this->Morders->search(array('OrderStatusIds' => '5,6,10'), PHP_INT_MAX, 1, 'OrderId, OrderCode, OrderDate');
				$data['listOrderAccounts'] = $this->Morderaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$data['listOrderUsers'] = $this->Musers->getListByRole(array(5, 8));
				$postData = $this->arrayFromPost(array('ShopNo', 'OrderId', 'OrderAccountId', 'OrderUserId'));
				$postData['ShopPaidStatusId'] = 3;
				$data['listShopnNumbers'] = array();
				$rowCount = $this->Mshopnumbers->getCount($postData);
				if ($rowCount > 0) {
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listShopnNumbers'] = $this->Mshopnumbers->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('shop/paid_done', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function isReturn($isReturn = 0){
		$user = $this->session->userdata('user');
		if ($user) {
			$data = $this->commonData($user,
				'Đòi tiền',
				array('scriptFooter' => array('js' => 'js/shop_paid.js?20170727'))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'shop/isreturn/0')) {
				$this->loadModel(array('Morders', 'Morderaccounts', 'Mshops', 'Mshopnumbers', 'Mbankaccounts'));
				$data['listOrders'] = $this->Morders->search(array('OrderStatusIds' => '5,6,10'), PHP_INT_MAX, 1, 'OrderId, OrderCode, OrderDate');
				$data['listOrderAccounts'] = $this->Morderaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$data['listBankAccounts'] = $this->Mbankaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$data['listOrderUsers'] = $this->Musers->getListByRole(array(5, 8));
				$postData = $this->arrayFromPost(array('ShopNo', 'OrderId', 'OrderAccountId', 'OrderUserId'));
				if(!is_numeric($isReturn)) $isReturn = 0;
				if($isReturn != 0 && $isReturn != 1) $isReturn = 0;
				$data['isReturn'] = $isReturn;
				$data['title'] = $isReturn == 0 ? 'Chưa đòi tiền' : 'Đã đòi tiền';
				$data['statisticReturns'] = $this->Mshopnumbers->statisticReturnCounts($postData);
				$postData['IsReturn'] = $isReturn;
				$data['listShopnNumbers'] = array();
				$rowCount = $this->Mshopnumbers->getCount($postData);
				if ($rowCount > 0) {
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listShopnNumbers'] = $this->Mshopnumbers->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('shop/return', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function getShopNumber(){
		$user = $this->session->userdata('user');
		if ($user) {
			$orderId = $this->input->post('OrderId');
			if($orderId > 0){
				$this->loadModel(array('Mshops', 'Mshopnumbers'));
				$listShopNumbers = $this->Mshopnumbers->getBy(array('OrderId' => $orderId), false, 'ShopId');
				$data = array();
				if(!empty($listShopNumbers)){
					$listShops = $this->Mshops->getBy(array('OrderId' => $orderId, 'ShopStatusId >' => 0), false, '', 'ShopId, ShopCode');
					foreach($listShopNumbers as $sn){
						$shopCode = $this->Mconstants->getObjectValue($listShops, 'ShopId', $sn['ShopId'], 'ShopCode');
						$data[$shopCode][] = $sn;
					}
				}
				echo json_encode(array('code' => 1, 'data' => $data));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function deleteShopNumber(){
		$user = $this->session->userdata('user');
		if ($user) {
			$shopNumberId = $this->input->post('ShopNumberId');
			$orderId = $this->input->post('OrderId');
			if($shopNumberId > 0 && $orderId > 0){
				$this->load->model('Mshopnumbers');
				$flag = $this->Mshopnumbers->changeStatus(0, $shopNumberId, 'ShopPaidStatusId');
				if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Số thứ tự thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateShopNumber(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('OrderId', 'ShopNo', 'ShopCost', 'ShopCostAdmin', 'ReturnCost', 'IsReturn', 'OrderAccountId', 'ShopPaidStatusId', 'BankAccountId', 'Reason', 'Comment'));
			$shopNumberId = $this->input->post('ShopNumberId');
			$postData['ShopCost'] = replacePrice($postData['ShopCost']);
			if($shopNumberId > 0 && $postData['OrderId'] > 0 && !empty($postData['ShopNo']) && $postData['ShopCost'] > 0){
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = getCurentDateTime();
				if($user['RoleId'] == 8) $postData['ShopCostAdmin'] = replacePrice($postData['ShopCostAdmin']);
				else unset($postData['ShopCostAdmin']);
				$this->load->model('Mshopnumbers');
				$flag = $this->Mshopnumbers->update($postData, $shopNumberId, $this->input->post('OrderStatusId'), $this->input->post('UpdateTypeId'));
				if ($flag){
					$statusName = '<span class="' . $this->Mconstants->labelCss[$postData['ShopPaidStatusId']] . '">' . $this->Mconstants->shopPaidStatus[$postData['ShopPaidStatusId']] . '</span>';
					echo json_encode(array('code' => 1, 'message' => "Cập nhật Số thứ tự thành công", 'data' => $statusName));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function insertShopNumber(){
		$user = $this->session->userdata('user');
		if ($user) {
			$orderId = $this->input->post('OrderId');
			$shopNumberJson = $this->input->post('ShopNumberJson');
			if($orderId > 0 && !empty($shopNumberJson)){
				$shopNumbers = json_decode($shopNumberJson, true);
				if(!empty($shopNumbers)){
					$this->loadModel(array('Mshops', 'Mshopnumbers'));
					$listShops = $this->Mshops->getBy(array('OrderId' => $orderId, 'ShopStatusId >' => 0), false, '', 'ShopId, ShopCode');
					$crDateTime = getCurentDateTime();
					$shopNumberInserts = array();
					foreach($shopNumbers as $sn){
						$shopId = $this->Mconstants->getObjectValue($listShops, 'ShopCode', $sn['ShopCode'], 'ShopId');
						if($shopId > 0){
							$shopNumberInserts[] = array(
								'ShopId' => $shopId,
								'OrderId' => $orderId,
								'ShopNo' => $sn['ShopNo'],
								'ShopCost' =>  replacePrice($sn['ShopCost']),
								'ShopCostAdmin' =>  replacePrice($sn['ShopCostAdmin']),
								'ReturnCost' =>  replacePrice($sn['ReturnCost']),
								'IsReturn' => $sn['IsReturn'],
								'OrderAccountId' => $sn['OrderAccountId'],
								'ShopPaidStatusId' => 2,
								'Reason' => $sn['Reason'],
								'Comment' => $sn['Comment'],
								'CrUserId' => $user['UserId'],
								'CrDateTime' => $crDateTime
							);
						}
					}
					if(!empty($shopNumberInserts)){
						$flag = $this->Mshopnumbers->insert($shopNumberInserts, $this->input->post('OrderStatusId'));
						if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật Số thứ tự thành công"));
						else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
					}
					else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function deleteShopTracking(){
		$user = $this->session->userdata('user');
		if ($user) {
			$shopTrackingId = $this->input->post('ShopTrackingId');
			$orderId = $this->input->post('OrderId');
			if($shopTrackingId > 0 && $orderId > 0){
				$this->loadModel(array('Mshops', 'Mshoptrackings'));
				$this->Mshoptrackings->delete($shopTrackingId);
				$shopCount1 = $this->Mshops->getShopCount($orderId);
				if($shopCount1 > 0){
					$shopCount2 = $this->Mshoptrackings->getShopCount($orderId);
					$trackingStatusId = 1;
					if($shopCount1 == $shopCount2) $trackingStatusId = 2;
					$this->load->model('Morders');
					$this->Morders->updateField(array('TrackingStatusId' => $trackingStatusId), $orderId);
				}
				echo json_encode(array('code' => 1, 'message' => "Xóa Mã vận đơn thành công"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function updateShopTracking(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('OrderId', 'Tracking', 'IsReturn', 'Comment'));
			$shopCode = $this->input->post('ShopCode');
			$orderId = $postData['OrderId'];
			if(!empty($shopCode) && $orderId > 0 && !empty($postData['Tracking']) ){
				$this->loadModel(array('Mshops', 'Mshoptrackings'));
				$shopId = $this->Mshops->getFieldValue(array('ShopCode' => $shopCode, 'OrderId' => $orderId), 'ShopId', 0);
				if($shopId > 0) {
					$postData['ShopId'] = $shopId;
					$shopTrackingId = $this->input->post('ShopTrackingId');
					if ($shopTrackingId > 0) {
						$postData['UpdateUserId'] = $user['UserId'];
						$postData['UpdateDateTime'] = getCurentDateTime();
					}
					else {
						$postData['CrUserId'] = $user['UserId'];
						$postData['CrDateTime'] = getCurentDateTime();
					}
					$flag = $this->Mshoptrackings->update($postData, $shopTrackingId);
					if ($flag > 0) {
						$shopCount1 = $this->Mshops->getShopCount($orderId);
						if($shopCount1 > 0){
							$shopCount2 = $this->Mshoptrackings->getShopCount($orderId);
							if($shopCount1 == $shopCount2) {
								$this->load->model('Morders');
								$this->Morders->updateField(array('TrackingStatusId' => 2), $orderId);
							}
						}
						$postData['ShopTrackingId'] = $flag;
						$postData['IsAdd'] = ($shopTrackingId > 0) ? 0 : 1;
						echo json_encode(array('code' => 1, 'message' => "Cập nhật Mã vận đơn thành công", 'data' => $postData));
					}
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeIsTransportSlow(){
		$user = $this->session->userdata('user');
		if ($user) {
			$orderId = $this->input->post('OrderId');
			$shopCode = trim($this->input->post('ShopCode'));
			if($orderId > 0 && !empty($shopCode)){
				$this->load->model('Mshops');
				$shopId = $this->Mshops->getFieldValue(array('ShopCode' => $shopCode, 'OrderId' => $orderId), 'ShopId', 0);
				if($shopId > 0) {
					$flag = $this->Mshops->updateField(array(
						'IsTransportSlow' => $this->input->post('IsTransportSlow'),
						'UpdateUserId' => $user['UserId'],
						'UpdateDateTime' => getCurentDateTime()
					), $shopId);
					if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật shop {$shopCode} thành công"));
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->session->userdata('user');
		if ($user) {
			$notificationData = $this->arrayFromPost(array('CustomerId', 'UserId', 'RoleId', 'OrderId'));
			$postData = $this->arrayFromPost(array('OrderCode', 'ShopCode', 'ShopStatusId', 'ShipTQ', 'ShipTQNew', 'PaidTQ', 'ExchangeRate', 'PaidVN', 'ShipTQReason', 'ShipTQConfirm'));
			$postData['ShipTQ'] = replacePrice($postData['ShipTQ']);
			$postData['ShipTQNew'] = replacePrice($postData['ShipTQNew']);
			$postData['PaidTQ'] = replacePrice($postData['PaidTQ']);
			$postData['PaidVN'] = replacePrice($postData['PaidVN']);
			if($postData['PaidTQ'] < 0) $postData['PaidTQ'] = -$postData['PaidTQ'];
			if($postData['PaidVN'] < 0) $postData['PaidVN'] = -$postData['PaidVN'];
			$shopStatusId = $postData['ShopStatusId'];
			$shopCode = $postData['ShopCode'];
			$orderId = $notificationData['OrderId'];
			$customerId = $notificationData['CustomerId'];
			if($customerId > 0 && $orderId > 0 && $shopStatusId > 0 && $postData['PaidVN'] > 0 && !empty($shopCode) && !empty($postData['ShipTQReason'])){
				$this->load->model('Mshops');
				$shopId = $this->Mshops->getFieldValue(array('ShopCode' => $shopCode, 'OrderId' => $orderId), 'ShopId', 0);
				if($shopId > 0) {
					$crDateTime = getCurentDateTime();
					$notificationData['NotificationStatusId'] = 1;
					$notificationData['ProductId'] = 0;
					$notificationData['ComplaintId'] = 0;
					$notificationData['CrDateTime'] = $crDateTime;
					$message = '';
					$transactionData = array();
					$isChangeBalance = false;
					$this->loadModel(array('Mtransactions', 'Mcustomerbalances', 'Mshops'));
					if($shopStatusId == 3 || $shopStatusId == 4){
						$notificationData['IsCustomerSend'] = 0;
						$notificationData['LinkTo'] = 'chi-tiet-don-hang-'.$orderId.'-shop-'.$shopCode;
						$message = $postData['ShipTQReason'];
					}
					elseif($shopStatusId == 5){
						$notificationData['IsCustomerSend'] = 1;
						$message = "Đơn hàng {$postData['OrderCode']} được khách hàng chấp nhận tăng giá shop {$shopCode} - Xác nhận: ";
						$message .= '"'.$postData['ShipTQConfirm'].'"';
						$transactionTypeId = 2;
						$transactionData = array(
							'CustomerId' => $customerId,
							'OrderId' => $orderId,
							'ComplaintId' => 0,
							'StatusId' => 2,
							'TransactionTypeId' => $transactionTypeId,
							'TransactionRangeId' => 4,
							'WarehouseId' => 1,
							'MoneySourceId' => 1,
							'Comment' => $message,
							'PaidTQ' => $postData['PaidTQ'],
							'ExchangeRate' => $postData['ExchangeRate'],
							'PaidVN' => $postData['PaidVN'],
							'PaidDateTime' => $crDateTime,
							'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $postData['PaidVN']),
							'CrUserId' => $user['UserId'],
							'CrDateTime' => $crDateTime
						);
						$isChangeBalance = true;
					}
					elseif($shopStatusId == 6){
						$notificationData['IsCustomerSend'] = 1;
						$message = "Đơn hàng {$postData['OrderCode']} được khách hàng chấp nhận giảm giá shop {$shopCode} - Xác nhận: ";
						$message .= '"'.$postData['ShipTQConfirm'].'"';
						$transactionTypeId = 1;
						$transactionData = array(
							'CustomerId' => $customerId,
							'OrderId' => $orderId,
							'ComplaintId' => 0,
							'StatusId' => 2,
							'TransactionTypeId' => $transactionTypeId,
							'TransactionRangeId' => 3,
							'WarehouseId' => 1,
							'MoneySourceId' => 1,
							'Comment' => $message,
							'PaidTQ' => $postData['PaidTQ'],
							'ExchangeRate' => $postData['ExchangeRate'],
							'PaidVN' => $postData['PaidVN'],
							'PaidDateTime' => $crDateTime,
							'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $postData['PaidVN']),
							'CrUserId' => $user['UserId'],
							'CrDateTime' => $crDateTime
						);
						$isChangeBalance = true;
					}
					elseif($shopStatusId == 7){
						$notificationData['IsCustomerSend'] = 1;
						$message = "Đơn hàng {$postData['OrderCode']} khách hàng KHÔNG chấp nhận tăng giá shop {$shopCode} - Xác nhận: ";
						$message .= '"'.$postData['ShipTQConfirm'].'"';
						$transactionTypeId = 1;
						$transactionData = array(
							'CustomerId' => $customerId,
							'OrderId' => $orderId,
							'ComplaintId' => 0,
							'StatusId' => 2,
							'TransactionTypeId' => $transactionTypeId,
							'TransactionRangeId' => 3,
							'WarehouseId' => 1,
							'MoneySourceId' => 1,
							'Comment' => $message,
							'PaidTQ' => $postData['PaidTQ'],
							'ExchangeRate' => $postData['ExchangeRate'],
							'PaidVN' => $postData['PaidVN'],
							'PaidDateTime' => $crDateTime,
							'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $postData['PaidVN']),
							'CrUserId' => $user['UserId'],
							'CrDateTime' => $crDateTime
						);
						$isChangeBalance = true;
					}
					elseif($shopStatusId == 8){
						$notificationData['IsCustomerSend'] = 1;
						$message = "Đơn hàng {$postData['OrderCode']} khách hàng KHÔNG chấp nhận giảm giá shop {$shopCode} - Xác nhận: ";
						$message .= '"'.$postData['ShipTQConfirm'].'"';
						$transactionTypeId = 1;
						$transactionData = array(
							'CustomerId' => $customerId,
							'OrderId' => $orderId,
							'ComplaintId' => 0,
							'StatusId' => 2,
							'TransactionTypeId' => $transactionTypeId,
							'TransactionRangeId' => 3,
							'WarehouseId' => 1,
							'MoneySourceId' => 1,
							'Comment' => $message,
							'PaidTQ' => $postData['PaidTQ'],
							'ExchangeRate' => $postData['ExchangeRate'],
							'PaidVN' => $postData['PaidVN'],
							'PaidDateTime' => $crDateTime,
							'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $postData['PaidVN']),
							'CrUserId' => $user['UserId'],
							'CrDateTime' => $crDateTime
						);
						$isChangeBalance = true;
					}
					if($notificationData['IsCustomerSend'] == 1) $notificationData['LinkTo'] = 'order/edit/'.$orderId.'/0/0'.$shopCode;
					$notificationData['Message'] = $message;
					$shopUpdate = array(
						'ShopStatusId' => $shopStatusId,
						'ShipTQReason' => $postData['ShipTQReason'],
						'UpdateUserId' => $user['UserId'],
						'UpdateDateTime' => $crDateTime
					);
					if($shopStatusId == 3 || $shopStatusId == 4) $shopUpdate['ShipTQOldNew'] = $postData['ShipTQNew'];
					elseif($shopStatusId == 5 || $shopStatusId == 6){
						$shopUpdate['ShipTQ'] = $postData['ShipTQNew'];
						$shopUpdate['ShipTQOldNew'] = $postData['ShipTQ'];
					}
					elseif($shopStatusId == 7 || $shopStatusId == 8){
						$shopUpdate['ShipTQ'] = 0;
						$shopUpdate['ShipTQOldNew'] = $postData['ShipTQ'];
					}
					if($user['RoleId'] == 4) $shopUpdate['ShipTQConfirm'] = $postData['ShipTQConfirm'];
					$staffs = $this->arrayFromPost(array('CareStaffId', 'OrderUserId'));
					$flag = $this->Mshops->changeShipTQ($shopUpdate, $shopId, $notificationData, $transactionData, $staffs, $isChangeBalance);
					if($flag) echo json_encode(array('code' => 1, 'message' => "Thay đổi giá shop thành công"));
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeCategory(){
		$user = $this->session->userdata('user');
		if ($user) {
			$orderId = $this->input->post('OrderId');
			$shopCode = trim($this->input->post('ShopCode'));
			$categoryId = $this->input->post('CategoryId');
			if($orderId > 0 && !empty($shopCode) && $categoryId > 0){
				$this->load->model('Mshops');
				$shopId = $this->Mshops->getFieldValue(array('ShopCode' => $shopCode, 'OrderId' => $orderId), 'ShopId', 0);
				if($shopId > 0) {
					$flag = $this->Mshops->updateField(array(
						'CategoryId' => $categoryId,
						'UpdateUserId' => $user['UserId'],
						'UpdateDateTime' => getCurentDateTime()
					), $shopId);
					if($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật chuyên mục shop {$shopCode} thành công"));
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}