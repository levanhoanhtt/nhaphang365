<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bankaccount extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Tài khoản ngân hàng',
				array('scriptFooter' => array('js' => 'js/bankaccount.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'bankaccount')) {
				$data['updateBankAccount'] = $this->Mactions->checkAccess($listActions, 'bankaccount/update');
				$data['deleteBankAccount'] = $this->Mactions->checkAccess($listActions, 'bankaccount/delete');
				$this->load->model('Mbankaccounts');
				$data['listBankAccounts'] = $this->Mbankaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$this->load->view('setting/bankaccount', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('BankAccountName'));
		if(!empty($postData['BankAccountName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$BankAccountId = $this->input->post('BankAccountId');
			$this->load->model('Mbankaccounts');
			$flag = $this->Mbankaccounts->save($postData, $BankAccountId);
			if ($flag > 0) {
				$postData['BankAccountId'] = $flag;
				$postData['IsAdd'] = ($BankAccountId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Tài khoản ngân hàng thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$BankAccountId = $this->input->post('BankAccountId');
		if($BankAccountId > 0){
			$this->load->model('Mbankaccounts');
			$flag = $this->Mbankaccounts->changeStatus(0, $BankAccountId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Tài khoản ngân hàng thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
