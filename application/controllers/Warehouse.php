<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Warehouse extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Kho',
				array('scriptFooter' => array('js' => 'js/warehouse.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'warehouse')) {
				$data['updateWarehouse'] = $this->Mactions->checkAccess($listActions, 'warehouse/update');
				$data['deleteWarehouse'] = $this->Mactions->checkAccess($listActions, 'warehouse/delete');
				$this->load->model('Mwarehouses');
				$data['listWarehouses'] = $this->Mwarehouses->getBy(array('StatusId' => STATUS_ACTIVED));
				$this->load->view('setting/warehouse', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('WarehouseName'));
		if(!empty($postData['WarehouseName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$warehouseId = $this->input->post('WarehouseId');
			$this->load->model('Mwarehouses');
			$flag = $this->Mwarehouses->save($postData, $warehouseId);
			if ($flag > 0) {
				$postData['WarehouseId'] = $flag;
				$postData['IsAdd'] = ($warehouseId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật kho thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$warehouseId = $this->input->post('WarehouseId');
		if($warehouseId > 0){
			$this->load->model('Mwarehouses');
			$flag = $this->Mwarehouses->changeStatus(0, $warehouseId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa kho thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
