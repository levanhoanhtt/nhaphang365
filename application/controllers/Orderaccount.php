<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orderaccount extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Tài khoản đặt hàng',
				array('scriptFooter' => array('js' => 'js/orderaccount.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'orderaccount')) {
				$data['updateOrderAccount'] = $this->Mactions->checkAccess($listActions, 'orderaccount/update');
				$data['deleteOrderAccount'] = $this->Mactions->checkAccess($listActions, 'orderaccount/delete');
				$this->load->model('Morderaccounts');
				$data['listOrderAccounts'] = $this->Morderaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$this->load->view('setting/orderaccount', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('OrderAccountName'));
		if(!empty($postData['OrderAccountName'])) {
			$postData['StatusId'] = STATUS_ACTIVED;
			$orderaccountId = $this->input->post('OrderAccountId');
			$this->load->model('Morderaccounts');
			$flag = $this->Morderaccounts->save($postData, $orderaccountId);
			if ($flag > 0) {
				$postData['OrderAccountId'] = $flag;
				$postData['IsAdd'] = ($orderaccountId > 0) ? 0 : 1;
				echo json_encode(array('code' => 1, 'message' => "Cập nhật Tài khoản đặt hàng thành công", 'data' => $postData));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	
	public function delete(){
		$orderaccountId = $this->input->post('OrderAccountId');
		if($orderaccountId > 0){
			$this->load->model('Morderaccounts');
			$flag = $this->Morderaccounts->changeStatus(0, $orderaccountId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa Tài khoản đặt hàng thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
