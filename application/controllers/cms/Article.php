<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách bài viết',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/article_list.js'))
				)
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'cms/article')) {
				$data['deleteArticle'] = $this->Mactions->checkAccess($listActions, 'cms/article/delete');
				$data['changeStatus'] = $this->Mactions->checkAccess($listActions, 'cms/article/edit');
				$postData = $this->arrayFromPost(array('ArticleTitle', 'StatusId', 'BeginDate', 'EndDate'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$postData['ArticleTypeId'] = 2;
				$this->load->model('Marticles');
				$data['listArticles'] = array();
				$rowCount = $this->Marticles->getCount($postData);
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listArticles'] = $this->Marticles->search($postData, $perPage, $page, 'DisplayOrder');
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('article/post', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function page(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user, 'Danh sách trang tĩnh');
			if($this->Mactions->checkAccess($data['listActions'], 'cms/article/page')) {
				$postData = $this->arrayFromPost(array('ArticleTitle'));
				$postData['StatusId'] = STATUS_ACTIVED;
				$postData['ArticleTypeId'] = 1;
				$this->load->model('Marticles');
				$data['listArticles'] = $this->Marticles->search($postData, PHP_INT_MAX);
				$this->load->view('article/page', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function video(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Video trang chủ',
				array('scriptFooter' => array('js' => 'js/article_video.js'))
			);
			if($this->Mactions->checkAccess($data['listActions'], 'cms/article/video')) {
				$this->load->model('Marticles');
				$data['listArticles'] = $this->Marticles->search(array('StatusId' => STATUS_ACTIVED, 'ArticleTypeId' => 2), PHP_INT_MAX, 1, 'DisplayOrder');
				$this->load->view('article/video', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function add(){
		$user = $this->session->userdata('user');
		if ($user) {
			$data = $this->commonData($user,
				'Thêm bài viết',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'js/article_update.js'))
				)
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'cms/article/add')) {
				$this->load->model('Mcategories');
				$data['listCategories'] = $this->Mcategories->getByCategoryTypeId(2);
				$this->load->view('article/add', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function edit($articleId){
		if($articleId > 0){
			$user = $this->session->userdata('user');
			if ($user) {
				$data = $this->commonData($user,
					'Cập nhật nội dung',
					array(
						'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
						'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'ckeditor/ckeditor.js', 'ckfinder/ckfinder.js', 'js/article_update.js'))
					)
				);
				if ($this->Mactions->checkAccess($data['listActions'], 'cms/article/edit')) {
					$this->load->model('Marticles');
					$article = $this->Marticles->get($articleId);
					$view = 'edit_page';
					if ($article) {
						$data['articleId'] = $articleId;
						$data['article'] = $article;
						/*if($article['ArticleTypeId'] == 1){
							$data['articleId'] = 0;
							$data['txtError'] = "Không tìm thấy trang";
						}*/
						if($article['ArticleTypeId'] == 2){
							$this->loadModel(array('Mcategories', 'Mcategoryarticles'));
							$data['listCategories'] = $this->Mcategories->getByCategoryTypeId(2);
							$data['categoryIds'] = $this->Mcategoryarticles->getListCategoryIds($articleId);
							$view = 'edit_post';
						}
					}
					else {
						$data['articleId'] = 0;
						$data['txtError'] = "Không tìm thấy trang";
					}
					$this->load->view('article/'.$view, $data);
				}
				else $this->load->view('user/permission', $data);
			}
			else redirect('user');
		}
		else redirect('cms/article');
	}

	public function update(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('ArticleTitle', 'ArticleSlug', 'ArticleLead', 'ArticleContent', 'StatusId', 'ArticleTypeId', 'DisplayOrder', 'ArticleImage', 'PublishDateTime'));
			$postData['ArticleImage'] = replaceFileUrl($postData['ArticleImage']);
			if(!empty($postData['PublishDateTime'])) $postData['PublishDateTime'] = ddMMyyyyToDate($postData['PublishDateTime']);
			$updateDateTime = getCurentDateTime();
			$articleId = $this->input->post('ArticleId');
			if($articleId > 0){
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = $updateDateTime;
			}
			else{
				if(empty($postData['PublishDateTime'])) $postData['PublishDateTime'] = $updateDateTime;
				$postData['CrUserId'] = $user['UserId'];
				$postData['CrDateTime'] = $updateDateTime;
			}
			$categoryIds = explode(',', trim($this->input->post('CategoryIds')));
			$this->load->model('Marticles');
			$articleId = $this->Marticles->update($postData, $articleId, $this->input->post('IsUpdateDisplayOrder'), $categoryIds);
			if ($articleId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật nội dung thành công", 'data' => $articleId));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeDisplayOrder(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('ArticleTypeId', 'DisplayOrder'));
			$articleId = $this->input->post('ArticleId');
			if($articleId > 0 && $postData['ArticleTypeId'] > 0 && $postData['DisplayOrder'] > 0){
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = getCurentDateTime();
				$this->load->model('Marticles');
				$flag = $this->Marticles->update($postData, $articleId, 1);
				if($flag) echo json_encode(array('code' => 1, 'message' => "Thay đổi thứ tự thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
	public function changeStatus(){
		$articleId = $this->input->post('ArticleId');
		$statusId = $this->input->post('StatusId');
		if($articleId > 0 && $statusId >= 0 && $statusId < 4){
			$this->load->model('Marticles');
			$flag = $this->Marticles->changeStatus($statusId, $articleId);
			if ($flag){
				if($statusId == 0) echo json_encode(array('code' => 1, 'message' => "Xóa {$this->input->post('ArticleTypeName')} thành công"));
				else echo json_encode(array('code' => 1, 'message' => "Thay đổi trạng thái {$this->input->post('ArticleTypeName')} thành công", 'data' => array('StatusName' => '<span class="'.$this->Mconstants->labelCss[$statusId].'">'.$this->Mconstants->status[$statusId].'</span>')));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getArticleContent(){
		$articleId = $this->input->post('ArticleId');
		if($articleId > 0){
			$this->load->model('Marticles');
			$articleContent = $this->Marticles->getFieldValue(array('ArticleId' => $articleId), 'ArticleContent');
			if(empty($articleContent)) echo json_encode(array('code' => 0));
			else echo json_encode(array('code' => 1, 'data' => $articleContent));
		}
		else echo json_encode(array('code' => -1));
	}
}