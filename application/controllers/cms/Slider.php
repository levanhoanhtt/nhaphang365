<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Slider',
				array('scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/slider.js')))
			);
			if($this->Mactions->checkAccess($data['listActions'], 'cms/slider')) {
				$this->load->model('Msliders');
				$data['listSliders'] = $this->Msliders->getList();
				$this->load->view('cms/slider', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('SliderName', 'SliderImage', 'SliderLink', 'DisplayOrder', 'SliderTypeId'));
		$sliderImage = $postData['SliderImage'];
		if(!empty($sliderImage)) {
			$postData['SliderImage'] = replaceFileUrl($sliderImage);
			$sliderId = $this->input->post('SliderId');
			$this->load->model('Msliders');
			$flag = $this->Msliders->update($postData, $sliderId);
			if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật ảnh thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeDisplayOrder(){
		$postData = $this->arrayFromPost(array('DisplayOrder', 'SliderTypeId'));
		$sliderId = $this->input->post('SliderId');
		if($sliderId > 0 && $postData['SliderTypeId'] > 0 && $postData['DisplayOrder'] > 0){
			$this->load->model('Msliders');
			$flag = $this->Msliders->update($postData, $sliderId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Thay đổi thứ tự thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function delete(){
		$sliderId = $this->input->post('SliderId');
		if($sliderId > 0){
			$this->load->model('Msliders');
			$flag = $this->Msliders->delete($sliderId);
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa ảnh thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

}
