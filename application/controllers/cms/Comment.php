<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Bình luận - Đánh giá',
				array('scriptFooter' => array('js' => 'js/comment.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'cms/comment')) {
				$data['deleteComment'] = $this->Mactions->checkAccess($listActions, 'cms/comment/delete');
				$data['changeStatus'] = $this->Mactions->checkAccess($listActions, 'cms/comment/edit');
				$this->load->model('Mcomments');
				$postData = $this->arrayFromPost(array('StatusId', 'CommentStarId', 'IpAddress'));
				$rowCount = $this->Mcomments->getCount($postData);
				$data['listComments'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listComments'] = $this->Mcomments->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('comment/list', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function add(){
		$user = $this->session->userdata('user');
		if ($user) {
			$data = $this->commonData($user,
				'Thên Bình luận - Đánh giá',
				array('scriptFooter' => array('js' => 'js/comment_update.js'))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'cms/comment/edit')) {
				$data['listCustomers'] = $this->Musers->getListByRole();
				$this->load->view('comment/add', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function edit($commentId){
		if($commentId > 0) {
			$user = $this->session->userdata('user');
			if ($user) {
				$data = $this->commonData($user,
					'Cập nhật Bình luận - Đánh giá',
					array('scriptFooter' => array('js' => 'js/comment_update.js'))
				);
				if ($this->Mactions->checkAccess($data['listActions'], 'cms/comment/edit')) {
					$this->load->model('Mcomments');
					$comment = $this->Mcomments->get($commentId);
					if ($comment) {
						$data['commentId'] = $commentId;
						$data['comment'] = $comment;
						$data['listCustomers'] = $this->Musers->getListByRole();
						$this->load->view('comment/edit', $data);
					}
					else {
						$data['commentId'] = 0;
						$data['txtError'] = "Không tìm thấy Bình luận - Đánh giá";
						$this->load->view('comment/edit', $data);
					}
				}
				else $this->load->view('user/permission', $data);
			}
			else redirect('user');
		}
		else redirect('comment');
	}

	public function update(){
		$postData = $this->arrayFromPost(array('ArticleId', 'Comment', 'StatusId', 'CommentStarId', 'FullName', 'CrUserId'));
		if(!empty($postData['Comment'])){
			$commentId = $this->input->post('CommentId');
			if($postData['ArticleId'] == 0 || empty($postData['ArticleId'])) unset($postData['ArticleId']);
			if($commentId == 0) {
				$postData['UserAgent'] = $this->input->user_agent();
				$postData['IpAddress'] = $this->input->ip_address();
				$postData['CrDateTime'] = getCurentDateTime();
			}
			$this->load->model('Mcomments');
			$flag = $this->Mcomments->save($postData, $commentId, array('ArticleId', 'FullName'));
			if ($flag > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật Bình luận - Đánh giá thành công", 'data' => $flag));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$commentId = $this->input->post('CommentId');
		$statusId = $this->input->post('StatusId');
		if($commentId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
			$this->load->model('Mcomments');
			$flag = $this->Mcomments->changeStatus($statusId, $commentId);
			if($flag) {
				$txtSuccess = "";
				$statusName = "";
				if($statusId == 0) $txtSuccess = "Xóa Bình luận - Đánh giá thành công";
				else{
					$txtSuccess = "Đổi trạng thái thành công";
					$statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
				}
				echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getList(){
		$this->load->model('Mcomments');
		$listComments = $this->Mcomments->search(array('StatusId' => STATUS_ACTIVED), 20, 1);
		$data = array();
		$customers = array();
		foreach($listComments as $c){
			if($c['CrUserId'] > 0){
				if(!isset($customers[$c['CrUserId']])) $customers[$c['CrUserId']] = $this->Musers->get($c['CrUserId'], true, '', 'FullName,Avatar');
				$customer = $customers[$c['CrUserId']];
				$c['FullName'] = $customer['FullName'];
				$c['Avatar'] = empty($customer['Avatar']) ? NO_IMAGE : $customer['Avatar'];
			}
			else $c['Avatar'] = NO_IMAGE;
			$data[] = $c;
		}
		echo json_encode(array('code' => 1, 'data' => $data));
	}
}
