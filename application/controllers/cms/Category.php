<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Chuyên mục bài viết',
				array('scriptFooter' => array('js' => 'js/category_article.js'))
			);
			if($this->Mactions->checkAccess($data['listActions'], 'cms/category')) {
				$this->load->model('Mcategories');
				$data['listCategories'] = $this->Mcategories->getByCategoryTypeId(2);
				$this->load->view('category/article', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function product($categoryTypeId = 3){
		if(!in_array($categoryTypeId, array(3, 4, 5))) $categoryTypeId = 3;
		$user = $this->session->userdata('user');
		if ($user) {
			$data = $this->commonData($user,
				'Danh sách Sản phẩm',
				array('scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/category_product.js')))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'cms/category/product')) {
				$data['categoryTypeId'] = $categoryTypeId;
				$this->load->model('Mcategories');
				$data['listCategories'] = $this->Mcategories->getByCategoryTypeId($categoryTypeId);
				$this->load->view('category/product', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function shop(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Chuyên mục sản phẩm',
				array('scriptFooter' => array('js' => 'js/category_shop.js'))
			);
			if($this->Mactions->checkAccess($data['listActions'], 'cms/category/shop')) {
				$this->load->model('Mcategories');
				$data['listCategories'] = $this->Mcategories->getByCategoryTypeId(1);
				$this->load->view('category/shop', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('CategoryName', 'CategoryNameTQ', 'ParentCategoryId', 'CategoryLevel', 'StatusId', 'CategoryImage', 'CategoryTypeId', 'DisplayOrder'));
			if(!empty($postData['CategoryImage'])) $postData['CategoryImage'] = replaceFileUrl($postData['CategoryImage']);
			$updateDateTime = getCurentDateTime();
			$categoryId = $this->input->post('CategoryId');
			if($categoryId > 0){
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = $updateDateTime;
			}
			else{
				$postData['CrUserId'] = $user['UserId'];
				$postData['CrDateTime'] = $updateDateTime;
			}
			if($postData['CategoryTypeId'] == 2){
				if(!empty($postData['CategoryNameTQ'])) $postData['CategoryNameTQ'] = createSlug($postData['CategoryNameTQ']);
				else $postData['CategoryNameTQ'] = createSlug($postData['CategoryName']);
			}
			$this->load->model('Mcategories');
			$flag = $this->Mcategories->update($postData, $categoryId);
			if ($flag) echo json_encode(array('code' => 1, 'message' => "Cập nhật chuyên mục thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeDisplayOrder(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('CategoryTypeId', 'ParentCategoryId', 'DisplayOrder'));
			$categoryId = $this->input->post('CategoryId');
			if($categoryId > 0 && $postData['DisplayOrder'] > 0){
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = getCurentDateTime();
				$this->load->model('Mcategories');
				$flag = $this->Mcategories->update($postData, $categoryId);
				if($flag) echo json_encode(array('code' => 1, 'message' => "Thay đổi thứ tự thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function delete(){
		$categoryId = $this->input->post('CategoryId');
		if($categoryId > 0){
			$this->load->model('Mcategories');
			if($this->input->post('IsCheck') == 1) $flag = $this->Mcategories->checkHasChild($categoryId);
			else $flag = true;
			if(!$flag) echo json_encode(array('code' => 0, 'message' => "Chuyên mục này có con"));
			else {
				$flag = $this->Mcategories->changeStatus(0, $categoryId);
				if ($flag) echo json_encode(array('code' => 1, 'message' => "Xóa chuyên mục thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}
