<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recharge extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Tài khoản Tệ',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/recharge.js?20170718'))
				)
			);
			if($this->Mactions->checkAccess($data['listActions'], 'recharge')) {
				$this->loadModel(array('Mbankaccounts', 'Maccountbalances', 'Mrecharges'));
				$data['listBankAccounts'] = $this->Mbankaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$data['listAccountBalances'] = $this->Maccountbalances->getBy(array('IsLast' => 1));
				$postData = $this->arrayFromPost(array('BankAccountId', 'RechargeTypeId', 'BeginDate', 'EndDate'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$rowCount = $this->Mrecharges->getCount($postData);
				$data['listRecharges'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listRecharges'] = $this->Mrecharges->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('recharge/list', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function add(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Nạp Tệ',
				array('scriptFooter' => array('js' => 'js/recharge.js?20170718'))
			);
			if($this->Mactions->checkAccess($data['listActions'], 'recharge/add')) {
				$this->loadModel(array('Mbankaccounts', 'Mrecharges'));
				$data['listBankAccounts'] = $this->Mbankaccounts->getBy(array('StatusId' => STATUS_ACTIVED));
				$this->load->view('recharge/add', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('BankAccountId', 'ShopNumberId', 'PaidTQ', 'ExchangeRate', 'RechargeTypeId', 'Comment'));
			$postData['PaidTQ'] = replacePrice($postData['PaidTQ']);
			$postData['ExchangeRate'] = replacePrice($postData['ExchangeRate']);
			if($postData['PaidTQ'] > 0 && $postData['ExchangeRate'] > 0){
				$postData['CrUserId'] = $user['UserId'];
				$crDateTime = getCurentDateTime();
				$postData['CrDateTime'] = $crDateTime;
				$postData['RechargeDate'] = $crDateTime;
				$this->load->model('Mrecharges');
				$rechargeId = $this->Mrecharges->insert($postData);
				if($rechargeId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật {$this->Mconstants->rechargeTypes[$postData['RechargeTypeId']]} thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}