<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	public function index(){
		if(!$this->session->userdata('user')){
			$data = array('title' => 'Đăng nhập');
			if ($this->session->flashdata('txtSuccess')) $data['txtSuccess'] = $this->session->flashdata('txtSuccess');
			$this->load->helper('cookie');
			$data['userName'] = $this->input->cookie('userName', true);
			$data['userPass'] = $this->input->cookie('userPass', true);
			$this->load->view('user/login', $data);
		}
		else redirect('user/dashboard');
	}

	public function logout(){
		$user = $this->session->userdata('user');
		$fields = array('user', 'configs');
		foreach($fields as $field) $this->session->unset_userdata($field);
		if($user){
			if($user['RoleId'] == 1 || $user['RoleId'] == 4) $this->load->view('user/logout', array('userId' => $user['UserId'], 'roleId' => $user['RoleId']));
			else redirect('user');
		}
		else redirect('user');
	}

	public function forgotPass(){
		$this->load->view('user/forgotpass', array('title' => 'Quên mật khẩu'));
	}

	public function sendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$message = "Xin chào {$user['FullName']}<br/>Xin vào link ".base_url('user/changePass/'.$token).' để đổi mật khẩu.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'dathang86@gmail.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Nhập hàng 365';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Lấy lại mật khẩu', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					$this->load->view('user/forgotpass', array('title' => 'Quên mật khẩu', 'txtSuccess' => 'Kiểm tra email và làm theo hướng dẫn'));
				}
			}
			else $this->load->view('user/forgotpass', array('title' => 'Quên mật khẩu', 'txtError' => 'Người dùng không tốn tại hoặc chưa kích hoạt!'));
		}
		else $this->load->view('user/forgotpass', array('title' => 'Quên mật khẩu', 'txtError' => 'Email không được bỏ trống!'));
	}

	public function changePass($token = ''){
		$data = array('title' => 'Đổi mật khẩu', 'token' => $token);
		$isWrongToken = true;
		if(!empty($token)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Token' => $token), true, "", "UserId");
			if($user){
				if($this->input->post('UserPass')) {
					$postData = $this->arrayFromPost(array('UserPass', 'RePass'));
					if (!empty($postData['UserPass']) && $postData['UserPass'] == $postData['RePass']) {
						$this->Musers->save(array('UserPass' => md5($postData['UserPass']), 'Token' => ''), $user['UserId'], array('Token'));
						$this->session->set_flashdata('txtSuccess', "Đổi mật khẩu thành công");
						redirect('user');
						exit();
					}
					else $data['txtError'] = "Mật khẩu không trùng";
				}
			}
			else {
				$data['txtError'] = "Mã Token không dúng";
				$isWrongToken = false;
			}
		}
		else {
			$data['txtError'] = "Mã Token không dúng";
			$isWrongToken = false;
		}
		$data['isWrongToken'] = $isWrongToken;
		$this->load->view('user/changepass', $data);
	}

	public function register(){
		if($this->session->userdata('user')) redirect('user/dashboard');
		else{
			$this->load->model('Mprovinces');
			$data = array(
				'title' => 'Đăng ký thành viên',
				'listProvinces' => $this->Mprovinces->getList()
			);
			$this->load->view('user/register', $data);
		}
	}

	public function dashboard(){
		$user = $this->session->userdata('user');
		if($user){
			if($user['RoleId'] == 4) redirect('customer/order');
			else {
				$data = $this->commonData($user,
					'Dashboard',
					array('scriptFooter' => array('js' => array('vendor/plugins/jquery.matchHeight.js', 'js/dashboard.js?20170712')))
				);
				$view = 'customer_care';
				if($user['RoleId'] == 1) $view = 'customer_care'; //CSKH
				$this->load->view('dashboard/'.$view, $data);
			}
		}
		else redirect('user');
	}

	public function permission(){
		$this->load->view('user/permission');
	}

	public function profile(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Trang cá nhân - '.$user['FullName'],
				array('scriptFooter' =>array('js' => array('ckfinder/ckfinder.js', 'js/user_profile.js?20170715')))
			);
			$this->load->model('Mprovinces');
			$data['listProvinces'] = $this->Mprovinces->getList();
			$this->load->view('user/profile', $data);
		}
		else redirect('user');
	}

	public function updateProfile(){
		$user = $this->session->userdata('user');
		if($user) {
			$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'NewPass', 'FullName', 'Email', 'GenderId', 'ProvinceId', 'Address', 'PhoneNumber', 'Avatar', 'CareStaffId'));
			$flag = false;
			if (!empty($postData['NewPass'])) {
				if ($user['UserPass'] == md5($postData['UserPass'])) {
					$flag = true;
					$postData['UserPass'] = md5($postData['NewPass']);
					unset($postData['NewPass']);
				}
				else echo json_encode(array('code' => -1, 'message' => "Mật khảu cũ không đúng"));
			}
			else {
				$flag = true;
				unset($postData['UserPass']);
				unset($postData['NewPass']);
			}
			if ($flag) {
				if ($this->Musers->checkExist($user['UserId'], $postData['UserName'], $postData['Email'], $postData['PhoneNumber'])) {
					echo json_encode(array('code' => -1, 'message' => "Tên đăng nhập hoặc Số điện thoại đã tồn tại trong hệ thống"));
				}
				else {
					$postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
					$postData['CrUserId'] = $user['UserId'];
					$postData['CrDateTime'] = getCurentDateTime();
					$postConfigs = $this->arrayFromPost(array('FullName1', 'FullName2', 'FullName3', 'PhoneNumber1', 'PhoneNumber2', 'PhoneNumber3', 'Address1', 'Address2', 'Address3', 'ProvinceId1', 'ProvinceId2', 'ProvinceId3', 'ContactDefaultIndex'));
					if(empty($user['Configs'])) $configs = array();
					else $configs = json_decode($user['Configs'], true);
					$configs = array_merge($configs, $this->arrayFromPost(array('BankAccountNumber', 'BankName1', 'BankAccountName', 'BankName2')));
					for($i = 1; $i < 4; $i++){
						if($i == $postConfigs['ContactDefaultIndex']){
							unset($configs['PhoneNumber'.$i]);
							unset($configs['Address'.$i]);
							unset($configs['ProvinceId'.$i]);
							$configs['FullName0'] = $postConfigs['FullName' . $i];
						}
						else {
							if(!empty($postConfigs['Address' . $i])){
								if($postConfigs['ContactDefaultIndex'] < 3) $j = ($i > 1) ? ($i - 1) : 1;
								else $j = $i;
								$configs['FullName' . $j] = $postConfigs['FullName' . $i];
								$configs['PhoneNumber' . $j] = $postConfigs['PhoneNumber' . $i];
								$configs['Address' . $j] = $postConfigs['Address' . $i];
								$configs['ProvinceId' . $j] = $postConfigs['ProvinceId' . $i];
							}
						}
					}
					$postData['Configs'] = json_encode($configs);
					$flag = $this->Musers->save($postData, $user['UserId'], array('Avatar', 'Configs'));
					if ($flag) {
						$user = array_merge($user, $postData);
						$this->session->set_userdata('user', $user);
						echo json_encode(array('code' => 1, 'message' => "Cập nhật thông tin cá nhân thành công"));
					}
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function customer(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Khách hàng',
				array('scriptFooter' => array('js' => 'js/user_list.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'user/customer')) {
				$data['deleteUser'] = $this->Mactions->checkAccess($listActions, 'user/deleteCustomer');
				$data['changeStatus'] = $this->Mactions->checkAccess($listActions, 'user/editCustomer');
				$postData = $this->arrayFromPost(array('FullName', 'Email', 'PhoneNumber'));
				$postData['RoleId'] = 4;
				$postData['UserSearchId'] = $user['UserId'];
				$postData['UserRoleId'] = $user['RoleId'];
				if($user['RoleId'] == 3) $flag = true;
				else $flag = !(empty($postData['FullName']) && empty($postData['Email']) && empty($postData['PhoneNumber']));
				if($flag) $rowCount = $this->Musers->getCount($postData);
				else $rowCount = 0;
				$data['listUsers'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listUsers'] = $this->Musers->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('user/customer', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function staff(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Nhân viên',
				array('scriptFooter' => array('js' => 'js/user_list.js'))
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'user/staff')) {
				$data['deleteUser'] = $this->Mactions->checkAccess($listActions, 'user/deleteStaff');
				$data['changeStatus'] = $this->Mactions->checkAccess($listActions, 'user/editStaff');
				$this->load->model('Mprovinces');
				$data['listProvinces'] = $this->Mprovinces->getList();
				$postData = $this->arrayFromPost(array('UserName', 'FullName', 'Email', 'PhoneNumber', 'RoleId', 'StatusId', 'GenderId', 'ProvinceId'));
				if($postData['RoleId'] == '' || $postData['RoleId'] == 0) $postData['RoleId'] = 10;
				$rowCount = $this->Musers->getCount($postData);
				$data['listUsers'] = array();
				if($rowCount > 0){
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listUsers'] = $this->Musers->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('user/staff', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function changeStatus(){
		$userId = $this->input->post('UserId');
		$statusId = $this->input->post('StatusId');
		if($userId > 0 && $statusId >= 0 && $statusId <= count($this->Mconstants->status)) {
			$flag = $this->Musers->changeStatus($statusId, $userId);
			if($flag) {
				$txtSuccess = "";
				$statusName = "";
				if($statusId == 0) $txtSuccess = "Xóa {$this->input->post('UserTypeName')} thành công";
				else{
					$txtSuccess = "Đổi trạng thái thành công";
					$statusName = '<span class="' . $this->Mconstants->labelCss[$statusId] . '">' . $this->Mconstants->status[$statusId] . '</span>';
				}
				echo json_encode(array('code' => 1, 'message' => $txtSuccess, 'data' => array('StatusName' => $statusName)));
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function edit($userId = 0){
		if($userId > 0) {
			$user = $this->session->userdata('user');
			if ($user) {
				$data = $this->commonData($user,
					'Cập nhật người dùng',
					array('scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/user_update.js?20170715')))
				);
				$userEdit = $this->Musers->get($userId);
				if ($userEdit) {
					$isStaff = ($userEdit['RoleId'] == 4) ? false : true;
					$actionName = $isStaff ? 'user/viewStaff' : 'user/viewCustomer';
					$listActions = $data['listActions'];
					if ($this->Mactions->checkAccess($listActions, $actionName)) {
						$actionName = $isStaff ? 'user/editStaff' : 'user/editCustomer';
						$data['canEdit'] = $this->Mactions->checkAccess($listActions, $actionName);
						$data['isStaff'] = $isStaff;
						$data['title'] = $isStaff ? 'Cập nhật nhân viên' : 'Cập nhật khách hàng';
						$data['userId'] = $userId;
						$data['userEdit'] = $userEdit;
						$phoneLabels = array('PhoneNumber1', 'PhoneNumber2');
						foreach($phoneLabels as $label) $data[$label] = '';
						if(!$isStaff && !empty($userEdit['Configs'])){
							$configs = json_decode($userEdit['Configs'], true);
							foreach($phoneLabels as $label){
								if(isset($configs[$label])) $data[$label] = $configs[$label];
							}
						}
						$this->load->model('Mprovinces');
						$data['listProvinces'] = $this->Mprovinces->getList();
						$this->load->view('user/edit_'.($isStaff ? 'staff' : 'customer'), $data);
					}
					else $this->load->view('user/permission', $data);
				}
				else {
					$data['userId'] = 0;
					$data['txtError'] = "Không tìm thấy người dùng";
					$this->load->view('user/edit_staff', $data);
				}
			}
			else redirect('user');
		}
		else redirect('user/profile');
	}

	public function add($userTypeName = ''){
		$user = $this->session->userdata('user');
		if($user && in_array($userTypeName, array('customer', 'staff'))) {
			$data = $this->commonData($user,
				'Thêm '. (($userTypeName == 'customer') ? 'Khách hàng' : 'Nhân viên'),
				array('scriptFooter' => array('js' => array('ckfinder/ckfinder.js', 'js/user_update.js?20170715')))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'user/add/'.$userTypeName)) {
				$isStaff = ($userTypeName == 'customer') ? false : true;
				$data['isStaff'] = $isStaff;
				$this->load->model('Mprovinces');
				$data['listProvinces'] = $this->Mprovinces->getList();
				$this->load->view('user/add_'.$userTypeName, $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function chat(){
		$user = $this->session->userdata('user');
		if($user) {
			$data = $this->commonData($user,
				'Chat với Khách hàng',
				array('scriptFooter' => array('js' => 'ckfinder/ckfinder.js'))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'user/chat')) {
				if($user['RoleId'] == 3 || $user['RoleId'] == 7){
					$data['listStaffs'] = $this->Musers->getListByRole(1);
					$this->load->view('user/chat_by_admin', $data);
				}
				else $this->load->view('user/chat', $data);
			}
			else $this->load->view('user/permission', $data);
		}
	}
	//api
	public function saveUser(){
		$user = $this->session->userdata('user');
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'FullName', 'Email', 'RoleId', 'GenderId', 'StatusId', 'ProvinceId', 'Address', 'PhoneNumber', 'Avatar', 'Comment'));
		$userId = $this->input->post('UserId');
		if(empty($postData['UserName'])) $postData['UserName'] = $postData['PhoneNumber'];
		if(!empty($postData['UserName']) && !empty($postData['FullName']) && $postData['RoleId'] > 0) {
			if ($this->Musers->checkExist($userId, $postData['UserName'], $postData['Email'], $postData['PhoneNumber'])) {
				echo json_encode(array('code' => -1, 'message' => "Tên đăng nhập hoặc Số điện thoại đã tồn tại trong hệ thống"));
			}
			else {
				$postData['Avatar'] = replaceFileUrl($postData['Avatar'], USER_PATH);
				$userPass = $postData['UserPass'];
				if ($userId == 0) $postData['UserPass'] = md5($userPass);
				else {
					unset($postData['UserPass']);
					$newPass = trim($this->input->post('NewPass'));
					if (!empty($newPass)) $postData['UserPass'] = md5($newPass);
				}
				$postData['CrUserId'] = ($user) ? $user['UserId'] : 0;
				$postData['CrDateTime'] = getCurentDateTime();
				$phones = trim($this->input->post('PhoneNumbers'));
				if (!empty($phones)) {
					$configs = array();
					if ($userId > 0) {
						$configStr = $this->Musers->getFieldValue(array('UserId' => $userId), 'Configs');
						if (!empty($configStr)) $configs = json_decode($configStr, true);
					}
					$phones = explode('|', $phones);
					$i = 0;
					foreach ($phones as $phone) {
						if (!empty($phone)) {
							$i++;
							$configs['PhoneNumber' . $i] = $phone;
						}
					}
					if ($i > 0) $postData['Configs'] = json_encode($configs);
				}
				$userId = $this->Musers->save($postData, $userId, array('Avatar', 'Configs', 'Comment'));
				if ($userId > 0) {
					if ($this->input->post('IsSendPass') == 'on') {
						$message = "Xin chào {$postData['FullName']}<br/>Thông tin đăng nhập của bạn là:<br/>Địa chỉ: " . base_url() . "<br/>Tên đăng nhập: {$postData['UserName']}<br/>Mật khẩu: {$userPass}";
						$configs = $this->session->userdata('configs');
						if (!$configs) $configs = array();
						$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'nhaphang365@gmail.com';
						$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Nhập hàng 365';
						$this->sendMail($emailFrom, $companyName, $postData['Email'], 'Thông tin đăng nhập', $message);
					}
					echo json_encode(array('code' => 1, 'message' => "Cập nhật {$this->input->post('UserTypeName')} thành công", 'data' => $userId));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function checkLogin(){
		$postData = $this->arrayFromPost(array('UserName', 'UserPass', 'IsRemember', 'IsGetBalance', 'IsGetConfigs'));
		$userName = $postData['UserName'];
		$userPass = $postData['UserPass'];
		$user = $this->Musers->login($userName, $userPass);
		if($user){
			if(empty($user['Avatar'])) $user['Avatar'] = NO_IMAGE;
			$this->session->set_userdata('user', $user);
			if($postData['IsGetConfigs'] == 1){
				$this->load->model('Mconfigs');
				$configs = $this->Mconfigs->getListMap();
				$this->session->set_userdata('configs', $configs);
			}
			$balance = 0;
			if($postData['IsGetBalance'] == 1){
				$this->load->model('Mcustomerbalances');
				$balance = priceFormat($this->Mcustomerbalances->getCurentBalance($user['UserId']));
			}
			if($postData['IsRemember'] == 'on'){
				$this->load->helper('cookie');
				$this->input->set_cookie(array('name' => 'userName', 'value' => $userName, 'expire' => '86400'));
				$this->input->set_cookie(array('name' => 'userPass', 'value' => $userPass, 'expire' => '86400'));
			}
			echo json_encode(array('code' => 1, 'message' => "Đăng nhập thành công", 'data' => array('User' => $user, 'Balance' => $balance)));
		}
		else echo json_encode(array('code' => 0, 'message' => "Đăng nhập không thành công"));
	}

	public function userLogout(){
		$this->session->unset_userdata('user');
		echo json_encode(array('FullName' => 'Nhập hàng 365', 'Avatar' => NO_IMAGE));
	}

	public function requestSendToken(){
		$email = trim($this->input->post('Email'));
		if(!empty($email)){
			$user = $this->Musers->getBy(array('StatusId' => STATUS_ACTIVED, 'Email' => $email), true, "", "UserId,FullName");
			if($user){
				$token = bin2hex(mcrypt_create_iv(10, MCRYPT_DEV_RANDOM));
				$message = "Xin chào {$user['FullName']}<br/>Xin vào link ".base_url('user/changePass/'.$token).' để đổi mật khẩu.';
				$configs = $this->session->userdata('configs');
				if(!$configs) $configs = array();
				$emailFrom = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'dathang86@gmail.com';
				$companyName = isset($configs['COMPANY_NAME']) ? $configs['COMPANY_NAME'] : 'Nhập hàng 365';
				$flag = $this->sendMail($emailFrom, $companyName, $email, 'Lấy lại mật khẩu', $message);
				if($flag){
					$this->Musers->save(array('Token' => $token), $user['UserId']);
					echo json_encode(array('code' => 1, 'message' => "Kiểm tra email và làm theo hướng dẫn"));
				}
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => 0, 'message' => "Người dùng không tốn tại hoặc chưa kích hoạt"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Email không được bỏ trống"));
	}

	public function getList(){
		$roleId = $this->input->post('RoleId');
		$listUsers = $this->Musers->getListByRole(intval($roleId));
		$data = array();
		foreach($listUsers as $u){
			if(empty($u['Avatar'])) $u['Avatar'] = NO_IMAGE;
			$data[] = $u;
		}
		echo json_encode(array('code' => 1, 'data' => $data));
	}
}