<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	public function delete(){
		$user = $this->session->userdata('user');
		$productId = $this->input->post('ProductId');
		if ($user && $productId > 0) {
			$this->load->model('Mproducts');
			$flag = $this->Mproducts->changeStatus(0, $productId, 'ProductStatusId');
			if($flag) echo json_encode(array('code' => 1, 'message' => "Xóa sản phẩm thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->session->userdata('user');
		if ($user) {
			$productId = $this->input->post('ProductId');
			$productStatusId = $this->input->post('ProductStatusId');
			$removeReason = trim($this->input->post('RemoveReason'));
			if ($productId > 0 && $productStatusId > 0 && !empty($removeReason)) {
				$crDateTime = getCurentDateTime();
				$notificationData = $this->arrayFromPost(array('CustomerId', 'UserId', 'RoleId', 'OrderId'));
				$notificationData['NotificationStatusId'] = 1;
				$notificationData['ProductId'] = $productId;
				$notificationData['ComplaintId'] = 0;
				$notificationData['CrDateTime'] = $crDateTime;
				$productData = $this->arrayFromPost(array('ProductLink', 'Color', 'Size', 'Quantity', 'QuantityNew', 'Cost', 'CostNew', 'ShipTQ', 'ExchangeRate', 'ServicePercent'));
				$productData['Quantity'] = replacePrice($productData['Quantity']);
				$productData['QuantityNew'] = replacePrice($productData['QuantityNew']);
				$productData['Cost'] = replacePrice($productData['Cost']);
				$productData['CostNew'] = replacePrice($productData['CostNew']);
				$productData['ShipTQ'] = replacePrice($productData['ShipTQ']);
				$paidTQ = $productData['Quantity'] * $productData['Cost'] + $productData['ShipTQ'];
				$paidTQNew = $productData['QuantityNew'] * $productData['CostNew'] + $productData['ShipTQ'];
				$paidVN = ceil($paidTQ * $productData['ExchangeRate']);
				$transactionData = array();
				$isChangeBalance = false;
				//$shopCode = trim($this->input->post('ShopCode'));
				$orderId = $notificationData['OrderId'];
				$customerId = $notificationData['CustomerId'];
				$message = "Đơn hàng {$this->input->post('OrderCode')} ";
				$removeConfirm = trim($this->input->post('RemoveConfirm'));
				$this->loadModel(array('Mtransactions', 'Mcustomerbalances', 'Mproducts'));
				if($productStatusId == 1){
					$notificationData['IsCustomerSend'] = 0;
					$notificationData['LinkTo'] = 'chi-tiet-don-hang-'.$orderId.'-sp-'.$productId;
					$message .= 'của quý khách có link sản phẩm hết hàng, quý khách xin vui lòng kiểm tra.';
					$transactionTypeId = 1;
					$paidVN = $paidVN + ceil($paidVN * $productData['ServicePercent'] / 100);
					$transactionData = array(
						'CustomerId' => $customerId,
						'OrderId' => $orderId,
						'ComplaintId' => 0,
						'StatusId' => 2,
						'TransactionTypeId' => $transactionTypeId,
						'TransactionRangeId' => 3,
						'WarehouseId' => 1,
						'MoneySourceId' => 1,
						'Comment' => $message,
						'PaidTQ' => $paidTQ,
						'ExchangeRate' => $productData['ExchangeRate'],
						'PaidVN' => $paidVN,
						'PaidDateTime' => $crDateTime,
						'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $paidVN),
						'CrUserId' => $user['UserId'],
						'CrDateTime' => $crDateTime
					);
					$isChangeBalance = true;
				}
				elseif($productStatusId == 3 || $productStatusId == 4){
					$notificationData['IsCustomerSend'] = 0;
					$notificationData['LinkTo'] = 'chi-tiet-don-hang-'.$orderId.'-sp-'.$productId;
					//$message .= 'của quý khách có link sản phẩm tăng giá với lý do "'.$removeReason.'", quý khách xin vui lòng xác nhận.';
					$message = $removeReason;
				}
				elseif($productStatusId == 5){
					$notificationData['IsCustomerSend'] = 1;
					$notificationData['LinkTo'] = 'order/edit/'.$orderId.'/0/'.$productId;
					$message .= 'được khách hàng chấp nhận tăng giá - Xác nhận: "'.$removeConfirm.'"';
					$paidTQDifference = $paidTQNew - $paidTQ;
					$paidVNDifference = ceil($paidTQDifference * $productData['ExchangeRate']);
					$paidVN = $paidVNDifference + ceil($paidVNDifference * $productData['ServicePercent'] / 100);
					$transactionTypeId = 2;
					$transactionData = array(
						'CustomerId' => $customerId,
						'OrderId' => $orderId,
						'ComplaintId' => 0,
						'StatusId' => 2,
						'TransactionTypeId' => $transactionTypeId,
						'TransactionRangeId' => 4,
						'WarehouseId' => 1,
						'MoneySourceId' => 1,
						'Comment' => $message,
						'PaidTQ' => $paidTQDifference,
						'ExchangeRate' => $productData['ExchangeRate'],
						'PaidVN' => $paidVN,
						'PaidDateTime' => $crDateTime,
						'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $paidVN),
						'CrUserId' => $user['UserId'],
						'CrDateTime' => $crDateTime
					);
					$isChangeBalance = true;
				}
				elseif($productStatusId == 6){
					$notificationData['IsCustomerSend'] = 1;
					$notificationData['LinkTo'] = 'order/edit/'.$orderId.'/0/'.$productId;
					$message .= 'được khách hàng chấp nhận giảm giá - Xác nhận: "'.$removeConfirm.'"';
					$paidTQDifference = $paidTQ - $paidTQNew;
					$paidVNDifference = ceil($paidTQDifference * $productData['ExchangeRate']);
					$paidVN = $paidVNDifference + ceil($paidVNDifference * $productData['ServicePercent'] / 100);
					$transactionTypeId = 1;
					$transactionData = array(
						'CustomerId' => $customerId,
						'OrderId' => $orderId,
						'ComplaintId' => 0,
						'StatusId' => 2,
						'TransactionTypeId' => $transactionTypeId,
						'TransactionRangeId' => 3,
						'WarehouseId' => 1,
						'MoneySourceId' => 1,
						'Comment' => $message,
						'PaidTQ' => $paidTQDifference,
						'ExchangeRate' => $productData['ExchangeRate'],
						'PaidVN' => $paidVN,
						'PaidDateTime' => $crDateTime,
						'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $paidVN),
						'CrUserId' => $user['UserId'],
						'CrDateTime' => $crDateTime
					);
					$isChangeBalance = true;
				}
				elseif($productStatusId == 7){
					$notificationData['IsCustomerSend'] = 1;
					$notificationData['LinkTo'] = 'order/edit/'.$orderId.'/0/'.$productId;
					$message .= 'khách hàng KHÔNG chấp nhận tăng giá - Xác nhận: "'.$removeConfirm.'"';
					$paidTQDifference = $productData['Quantity'] * $productData['Cost'] + $productData['ShipTQ'];
					$paidVNDifference = ceil($paidTQDifference * $productData['ExchangeRate']);
					$paidVN = $paidVNDifference + ceil($paidVNDifference * $productData['ServicePercent'] / 100);
					$transactionTypeId = 1;
					$transactionData = array(
						'CustomerId' => $customerId,
						'OrderId' => $orderId,
						'ComplaintId' => 0,
						'StatusId' => 2,
						'TransactionTypeId' => $transactionTypeId,
						'TransactionRangeId' => 3,
						'WarehouseId' => 1,
						'MoneySourceId' => 1,
						'Comment' => $message,
						'PaidTQ' => $paidTQDifference,
						'ExchangeRate' => $productData['ExchangeRate'],
						'PaidVN' => $paidVN,
						'PaidDateTime' => $crDateTime,
						'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $paidVN),
						'CrUserId' => $user['UserId'],
						'CrDateTime' => $crDateTime
					);
					$isChangeBalance = true;
				}
				elseif($productStatusId == 8){
					$notificationData['IsCustomerSend'] = 1;
					$notificationData['LinkTo'] = 'order/edit/'.$orderId.'/0/'.$productId;
					$message .= 'khách hàng KHÔNG chấp nhận giảm giá - Xác nhận: "'.$removeConfirm.'"';
					$paidTQDifference = $productData['Quantity'] * $productData['Cost'] + $productData['ShipTQ'];
					$paidVNDifference = ceil($paidTQDifference * $productData['ExchangeRate']);
					$paidVN = $paidVNDifference + ceil($paidVNDifference * $productData['ServicePercent'] / 100);
					$transactionTypeId = 1;
					$transactionData = array(
						'CustomerId' => $customerId,
						'OrderId' => $orderId,
						'ComplaintId' => 0,
						'StatusId' => 2,
						'TransactionTypeId' => $transactionTypeId,
						'TransactionRangeId' => 3,
						'WarehouseId' => 1,
						'MoneySourceId' => 1,
						'Comment' => $message,
						'PaidTQ' => $paidTQDifference,
						'ExchangeRate' => $productData['ExchangeRate'],
						'PaidVN' => $paidVN,
						'PaidDateTime' => $crDateTime,
						'Balance' => $this->Mtransactions->getNewBalance($customerId, $transactionTypeId,  $paidVN),
						'CrUserId' => $user['UserId'],
						'CrDateTime' => $crDateTime
					);
					$isChangeBalance = true;
				}
				$notificationData['Message'] = $message;
				$productUpdate = array(
					'ProductStatusId' => $productStatusId,
					'RemoveReason' => $removeReason,
					'UpdateUserId' => $user['UserId'],
					'UpdateDateTime' => $crDateTime
				);
				if($productStatusId == 1){
					$productUpdate['Quantity'] = 0;
					$productUpdate['QuantityOldNew'] = $productData['Quantity'];
				}
				elseif($productStatusId == 3 || $productStatusId == 4){
					$productUpdate['CostOldNew'] = $productData['CostNew'];
					$productUpdate['QuantityOldNew'] = $productData['QuantityNew'];
				}
				elseif($productStatusId == 5 || $productStatusId == 6){
					$productUpdate['CostOldNew'] = $productData['Cost'];
					$productUpdate['Cost'] = $productData['CostNew'];
					$productUpdate['QuantityOldNew'] = $productData['Quantity'];
					$productUpdate['Quantity'] = $productData['QuantityNew'];
				}
				elseif($productStatusId == 7 || $productStatusId == 8){
					$productUpdate['Quantity'] = 0;
					$productUpdate['QuantityOldNew'] = $productData['Quantity'];
					$productData['CostOldNew'] = $productData['CostNew'];
				}
				if($user['RoleId'] == 4) $productUpdate['RemoveConfirm'] = $removeConfirm;
				$staffs = $this->arrayFromPost(array('CareStaffId', 'OrderUserId'));
				$flag = $this->Mproducts->update($productUpdate, $productId, $notificationData, $transactionData, $staffs, $isChangeBalance);
				if($flag) echo json_encode(array('code' => 1, 'message' => "Thay đổi giá sản phẩm thành công"));
				else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function redirect(){
		$productNameTQ = isset($_GET['ProductNameTQ']) ? trim($_GET['ProductNameTQ']) : '';
		if(!empty($productNameTQ)){
			$productNameTQ = urlencode($productNameTQ);
			$shopId = isset($_GET['ShopId']) ? intval($_GET['ShopId']) : 1;
			if ($shopId < 1 || $shopId > 3) $shopId = 1;
			$url = '';
			if($shopId == 1) $url = 'https://world.taobao.com/search/search.htm?_input_charset=utf-8&q=' . $productNameTQ;
			else if($shopId == 2) $url = 'https://list.tmall.com/search_product.htm?_input_charset=utf-8&q=' . $productNameTQ;
			else if($shopId == 3) $url = 'https://s.1688.com/selloffer/offer_search.htm?_input_charset=utf-8&keywords=' . $productNameTQ;
			redirect($url);
		}
		else {
			$productName = isset($_GET['ProductName']) ? trim($_GET['ProductName']) : '';
			if (!empty($productName)) {
				$shopId = isset($_GET['ShopId']) ? intval($_GET['ShopId']) : 1;
				if ($shopId < 1 || $shopId > 3) $shopId = 1;
				$this->load->view('product/redirect', array('productName' => $productName, 'shopId' => $shopId));
			}
			else echo "<script>window.close();</script>";
		}
	}

	private function getShopUrl($siteName, $shopCode){
		$retVal = 'javascript:void(0)';
		if(!empty($shopCode)){
			if($siteName == 'TAOBAO') $retVal = "https://{$shopCode}.world.taobao.com/";
			elseif($siteName == 'TMALL') $retVal = "https://{$shopCode}.world.tmall.com/";
			elseif($siteName == '1688') $retVal = "https://{$shopCode}.1688.com/";
		}
		return $retVal;
	}

	private function getProductImage($productImage){
		$productImage = urldecode($productImage);
		$productImage = str_replace(array('.150x150.jpg', '_290x290.jpg', '400x400.jpg', '.jpg_150x150q90.jpg', '.jpg_150x150.jpg'), '.jpg', $productImage);
		$productImage = str_replace('//', '', $productImage);
		if(strrpos($productImage, 'img', -strlen($productImage)) !== false) $productImage = 'http://'.$productImage;
		elseif(strrpos($productImage, 'gd', -strlen($productImage)) !== false) $productImage = 'http://'.$productImage;
		return $productImage;
	}
	//api extension
	//add to cart
	public function add(){
		header('Access-Control-Allow-Origin: *');
		$productData = $this->arrayFromPost(array('comment', 'image_origin', 'link_origin', 'price_origin', 'price_promotion', 'property', 'quantity', 'shop_id', 'shop_name', 'stock', 'title_origin', 'site', 'error'));
		if($productData['error'] == 1){
			if(!is_numeric($productData['price_origin']) || $productData['price_origin'] < 0) $productData['price_origin'] = 0;
			if(!is_numeric($productData['price_promotion']) || $productData['price_promotion'] < 0) $productData['price_promotion'] = 0;
			if(!is_numeric($productData['quantity']) || $productData['quantity'] < 0) $productData['quantity'] = 0;
		}
		if(!empty($productData['link_origin'])) {
			$user = $this->session->userdata('user');
			$this->loadModel(array('Morderdraffs', 'Mconfigs'));
			$ipAdress = $this->input->ip_address();
			$userId = 0;
			if ($user) $userId = $user['UserId'];
			$orderDraff = $this->Morderdraffs->getByUser($userId, $ipAdress);
			if (!$orderDraff) {
				if ($user) {
					if($user['RoleId'] == 4) {
						$orderDraff = array(
							'UserId' => $userId,
							'FullName' => $user['FullName'],
							'PhoneNumber' => $user['PhoneNumber'],
							'Email' => $user['Email'],
							'Address' => $user['Address'],
							'CareStaffId' => '',
							'WarehouseId' => '',
							'Comment' => '',
							'ProductJson' => '[]',
							'IpAddress' => $ipAdress,
							'UserAgent' => $this->input->user_agent(),
							'CrDateTime' => getCurentDateTime()
						);
						if (!empty($user['Configs'])) {
							$userConfigs = json_decode($user['Configs'], true);
							if (isset($userConfigs['CareStaffId'])) $orderDraff['CareStaffId'] = $userConfigs['CareStaffId'];
						}
					}
					else{
						$orderDraff = array(
							'UserId' => $userId,
							'FullName' => '',
							'PhoneNumber' => '',
							'Email' => '',
							'Address' => '',
							'CareStaffId' => '',
							'WarehouseId' => '',
							'Comment' => '',
							'ProductJson' => '[]',
							'IpAddress' => $ipAdress,
							'UserAgent' => $this->input->user_agent(),
							'CrDateTime' => getCurentDateTime()
						);
					}
				}
				else {
					$orderDraff = array(
						'UserId' => $userId,
						'FullName' => '',
						'PhoneNumber' => '',
						'Email' => '',
						'Address' => '',
						'CareStaffId' => '',
						'WarehouseId' => '',
						'Comment' => '',
						'ProductJson' => '[]',
						'IpAddress' => $ipAdress,
						'UserAgent' => $this->input->user_agent(),
						'CrDateTime' => getCurentDateTime()
					);
				}
			}
			else {
				$orderDraff['UserId'] = $userId;
				$orderDraff['UserAgent'] = $this->input->user_agent();
				$orderDraff['CrDateTime'] = getCurentDateTime();
			}
			$orderDraff['ExchangeRate'] = $this->Mconfigs->getFieldValue(array('ConfigCode' => 'EXCHAGE_RATE_CN'), 'ConfigValue', 3456);
			if(!empty($orderDraff['ProductJson'])) $listProducts = json_decode($orderDraff['ProductJson'], true);
			else $listProducts = array();
			$color = $size = '';
			if(!empty($productData['property'])){
				$property = explode(';', $productData['property']);
				$i = 0;
				$pr1 = $pr2 = '';
				foreach($property as $pr){
					if(!empty($pr)){
						$i++;
						if($i == 1) $pr1 = $pr;
						elseif($i == 2) $pr2 = $pr;
						else break;
					}
				}
				if(!empty($pr1) && !empty($pr2)){
					$color = $pr2;
					$size = $pr1;
				}
				elseif(!empty($pr1)) $color = $pr1;
			}
			$product = array(
				'ShopCode' => $productData['shop_id'],
				'ShopName' => $productData['shop_name'],
				'ShopUrl' => $this->getShopUrl($productData['site'], $productData['shop_id']),
                'CategoryId' => 0,
				'IsTransportSlow' => 0,
                'ShopComment' => '',
                'ShopFeedback' => '',
				'ProductImage' => $this->getProductImage($productData['image_origin']),
				'ProductLink' => $productData['link_origin'],
				'Color' => $color,
				'Size' => $size,
				'Quantity' => $productData['quantity'],
				'Cost' => ($productData['price_promotion'] > 0) ? $productData['price_promotion'] : $productData['price_origin'],
				'ShipTQ' => 0,
				'ProductStatusId' => 2,
				'Comment' => $productData['comment'],
				'FeedBack' => ''
			);
			$index = -1;
			foreach($listProducts as $i => $p){
				if($p['ProductLink'] == $productData['link_origin'] && $p['Color'] == $color && $p['Size'] == $size){
					$index = $i;
					break;
				}
			}
			if($index >= 0){
				$quantity = $listProducts[$index]['Quantity'] + $productData['quantity'];
				if($quantity > $productData['stock']) $quantity = $productData['stock'];
				$product['Quantity'] = $quantity;
				$listProducts[$index] = $product;
			}
			else $listProducts[]= $product;
			$orderDraff['ProductJson'] = json_encode($listProducts);
			$flag = $this->Morderdraffs->update($orderDraff);
			if($flag){
				$count = 0;
				foreach($listProducts as $p){
					if(!empty($p['ProductLink']) && $p['Quantity'] > 0 && $p['Cost'] > 0) $count++;
				}
				$data = array(
					'title' => $productData['title_origin'],
					'price' => $product['Cost'],
					'quantity' => $product['Quantity'],
					'count' => $count
				);
				echo json_encode(array('status' => 1, 'message' => 'Thêm vào đơn hàng thành công', 'data' => $data));
			}
			else echo json_encode(array('status' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('status' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getExchange(){
		header('Access-Control-Allow-Origin: *');
		$this->load->model('Mconfigs');
		echo $this->Mconfigs->getFieldValue(array('ConfigCode' => 'EXCHAGE_RATE_CN'), 'ConfigValue', 3456);
	}

	public function getProductCount(){
		header('Access-Control-Allow-Origin: *');
		$this->load->model('Morderdraffs');
		$user = $this->session->userdata('user');
		$userId = 0;
		if ($user) $userId = $user['UserId'];
		$orderDraff = $this->Morderdraffs->getByUser($userId, $this->input->ip_address());
		if($orderDraff && !empty($orderDraff['ProductJson'])){
			$listProducts = json_decode($orderDraff['ProductJson'], true);
			$count = 0;
			foreach((array)$listProducts as $p){
				if(!empty($p['ProductLink']) && $p['Quantity'] > 0 && $p['Cost'] > 0) $count++;
			}
			echo $count;
		}
		else echo 0;
	}
}