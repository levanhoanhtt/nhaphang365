<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transport extends MY_Controller {

	public function index(){
		$user = $this->session->userdata('user');
		if($user){
			$data = $this->commonData($user,
				'Danh sách Phiếu thu Vận chuyển',
				array(
					'scriptHeader' => array('css' => 'vendor/plugins/datepicker/datepicker3.css'),
					'scriptFooter' => array('js' => array('vendor/plugins/datepicker/bootstrap-datepicker.js', 'js/transport.js'))
				)
			);
			$listActions = $data['listActions'];
			if($this->Mactions->checkAccess($listActions, 'transport')) {
				$data['changeStatus'] = true;// $this->Mactions->checkAccess($listActions, 'transport');
				$this->loadModel(array('Mtransports', 'Mwarehouses', 'Mtransportusers', 'Mtransporttypes'));
				$postData = $this->arrayFromPost(array('CustomerId', 'StatusId', 'WarehouseId', 'MoneySourceId', 'TransportUserId', 'TransportTypeId', 'BeginDate', 'EndDate'));
				if(!empty($postData['BeginDate'])) $postData['BeginDate'] = ddMMyyyyToDate($postData['BeginDate']);
				if(!empty($postData['EndDate'])) $postData['EndDate'] = ddMMyyyyToDate($postData['EndDate']);
				$whereStatus = array('StatusId' => STATUS_ACTIVED);
				$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
				$data['listTransportUsers'] = $this->Mtransportusers->getBy($whereStatus);
				$data['listTransportTypes'] = $this->Mtransporttypes->getHierachy();
				$data['listTransports'] = array();
				$data['listCustomers'] = array();
				$rowCount = $this->Mtransports->getCount($postData);
				if($rowCount > 0){
					$data['listCustomers'] = $this->Musers->getListByRole(4);
					$perPage = 20;
					$pageCount = ceil($rowCount / $perPage);
					$page = $this->input->post('PageId');
					if(!is_numeric($page) || $page < 1) $page = 1;
					$data['listTransports'] = $this->Mtransports->search($postData, $perPage, $page);
					$data['paggingHtml'] = getPaggingHtml($page, $pageCount);
				}
				$this->load->view('transport/list', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function edit($transportId = 0){
		if($transportId > 0) {
			$user = $this->session->userdata('user');
			if ($user) {
				$data = $this->commonData($user,
					'Cập nhật Phiếu thu Vận chuyển',
					array('scriptFooter' => array('js' => array('js/jquery.price_format.min.js', 'js/transport_update.js?20170624')))
				);
				$this->loadModel(array('Mtransports', 'Mwarehouses', 'Mtransportusers', 'Mtransporttypes'));
				$transport = $this->Mtransports->get($transportId);
				if ($transport) {
					if ($this->Mactions->checkAccess($data['listActions'], 'transport/edit')) {
						$data['transportId'] = $transportId;
						$data['transport'] = $transport;
						$data['customerName'] = $this->Musers->getFieldValue(array('UserId' => $transport['CustomerId']), 'FullName');
						$data['warehouseName'] = $this->Mwarehouses->getFieldValue(array('WarehouseId' => $transport['WarehouseId']), 'WarehouseName');
						$data['listTransportUsers'] = $this->Mtransportusers->getBy(array('StatusId' => STATUS_ACTIVED));
						$data['listTransportTypes'] = $this->Mtransporttypes->getHierachy();
					}
					else $this->load->view('user/permission', $data);
				}
				else {
					$data['transportId'] = 0;
					$data['txtError'] = "Không tìm thấy Phiếu thu Vận chuyển";
				}
				$this->load->view('transport/edit', $data);
			}
			else redirect('user');
		}
		else redirect('transportprocess');
	}

	public function add(){
		$user = $this->session->userdata('user');
		if($user) {
			$data = $this->commonData($user,
				'Thêm Phiếu thu Vận chuyển',
				array('scriptFooter' => array('js' => array('js/jquery.price_format.min.js', 'js/transport_update.js?20170624')))
			);
			if ($this->Mactions->checkAccess($data['listActions'], 'transport/add')) {
				$data['listCustomers'] = $this->Musers->getListByRole(4);
				$this->loadModel(array('Mwarehouses', 'Mtransportfees', 'Mtransportusers', 'Mtransporttypes'));
				$whereStatus = array('StatusId' => STATUS_ACTIVED);
				$data['listWarehouses'] = $this->Mwarehouses->getBy($whereStatus);
				$data['listTransportFees'] = $this->Mtransportfees->getBy($whereStatus);
				$data['listTransportUsers'] = $this->Mtransportusers->getBy($whereStatus);
				$data['listTransportTypes'] = $this->Mtransporttypes->getHierachy();
				$this->load->view('transport/add', $data);
			}
			else $this->load->view('user/permission', $data);
		}
		else redirect('user');
	}

	public function update(){
		$user = $this->session->userdata('user');
		if ($user) {
			$postData = $this->arrayFromPost(array('CustomerId', 'StatusId', 'WarehouseId', 'PackageCount', 'Weight', 'WeightCost', 'OtherCost', 'TransportReasonId', 'PaidVN', 'TransportUserId', 'TransportTypeId', 'Comment'));
			$postData['MoneySourceId'] = 4;
			$postData['PackageCount'] = replacePrice($postData['PackageCount']);
			$postData['Weight'] = replacePrice($postData['Weight']);
			$postData['WeightCost'] = replacePrice($postData['WeightCost']);
			$postData['OtherCost'] = replacePrice($postData['OtherCost']);
			$postData['PaidVN'] = replacePrice($postData['PaidVN']);
			$updateDateTime = getCurentDateTime();
			$transportId = $this->input->post('TransportId');
			if($transportId > 0) {
				$postData['UpdateUserId'] = $user['UserId'];
				$postData['UpdateDateTime'] = $updateDateTime;
			}
			else{
				$postData['PaidDateTime'] = $updateDateTime;
				$postData['CrUserId'] = $user['UserId'];
				$postData['CrDateTime'] = $updateDateTime;
			}
			$this->load->model('Mtransports');
			$transportId = $this->Mtransports->update($postData, $transportId);
			if ($transportId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật phiếu thu vận chuyển thành công"));
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function changeStatus(){
		$user = $this->session->userdata('user');
		if ($user) {
			$transportId = $this->input->post('TransportId');
			if($transportId > 0){
				$this->load->model('Mtransports');
				$transport = $this->Mtransports->get($transportId);
				if($transport && $transport['StatusId'] == 1){
					$transport['StatusId'] = 2;
					$transport['UpdateUserId'] = $user['UserId'];
					$transport['UpdateDateTime'] = getCurentDateTime();
					$transportId = $this->Mtransports->update($transport, $transportId);
					if ($transportId > 0) echo json_encode(array('code' => 1, 'message' => "Cập nhật phiếu thu vận chuyển thành công"));
					else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
				}
				else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
			}
			else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}
}