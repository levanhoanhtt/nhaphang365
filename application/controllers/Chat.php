<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends MY_Controller {

	public function insert(){
		header('Access-Control-Allow-Origin: *');
		$postData = $this->arrayFromPost(array('CustomerId', 'StaffId', 'Message', 'FileUrl', 'IsCustomerSend'));
		if($postData['CustomerId'] > 0 && $postData['StaffId'] > 0){
			$postData['IsCustomerRead'] = 0;
			$postData['IsStaffRead'] = 0;
			$ipAddress = $this->input->ip_address();
			$postData['IpAddress'] = $ipAddress;
			$crDateTime = getCurentDateTime();
			$postData['CrDateTime'] = $crDateTime;
			$postData['Message'] = $this->makeLink($postData['Message']);
			if(!empty($postData['FileUrl'])) $postData['FileUrl'] = replaceFileUrl($postData['FileUrl'], PRODUCT_PATH);
			$this->load->model('Mchats');
			$chatId = $this->Mchats->save($postData);
			if($chatId > 0){
				echo json_encode(array('code' => 1, 'data' => array(
						'ChatId' => $chatId,
						'Message' => $postData['Message'],
						'FileUrl' => $postData['FileUrl'],
						'IpAddress' => $ipAddress,
						'CrDateTime' => ddMMyyyy($crDateTime, 'd/m/Y H:i:s'))
					)
				);
			}
			else echo json_encode(array('code' => 0, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	private function makeLink($text){
		if($num_found = preg_match_all('~[a-z]+://\S+~', $text, $out)){
			$links = array();
			foreach($out[0] as $link){
				if(!in_array($link, $links)) $links[]=$link;
			}
			foreach($links as $link) $text = str_replace($link, '<a href="'.$link.'" target="_blank">'.$link.'</a>', $text);
		}
		return $text;
	}

	public function getListCustomer(){
		$user = $this->session->userdata('user');
		$staffId = $this->input->post('StaffId');
		if($user && $staffId > 0) {
			$data = array();
			$listCustomers = $this->Musers->getListByRole(4);
			$listCustomerIds = array();
			$roleId = $user['RoleId'];
			if($roleId == 1) $listCustomerIds = $this->Musers->getCustomerIdsForStaff($user['UserId']);
			foreach($listCustomers as $c){
				if($roleId == 3) $flag = true;
				elseif($roleId == 1){
					$flag = false;
					if(in_array($c['UserId'], $listCustomerIds)) $flag = true;
				}
				if(empty($c['Avatar'])) $c['Avatar'] = NO_IMAGE;
				if(!$flag){
					$c['Email'] = '';
					$c['PhoneNumber'] = '';
				}
				$data[] = $c;
			}
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getList(){
		$postData = $this->arrayFromPost(array('CustomerId', 'StaffId'));
		if($postData['CustomerId'] > 0 && $postData['StaffId'] > 0){
			$this->load->model('Mchats');
			$limit = $this->input->post('Limit');
			$start = $this->input->post('Start');
			if (!is_numeric($limit) || $limit <= 0) $limit = 20;
			if (!is_numeric($start) || $start < 0) $start = 0;
			$listChats = $this->Mchats->search($postData, $limit, $start);
			$data = array();
			foreach ($listChats as $c) {
				$c['CrDateTime'] = ddMMyyyy($c['CrDateTime'], 'd/m/Y H:i:s');
				$data[] = $c;
			}
			echo json_encode(array('code' => 1, 'data' => array_reverse($data)));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	//chat o header
	public function getListChatUnRead(){
		$userId = $this->input->post('UserId');
		if($userId > 0){
			$isStaff = ($this->input->post('IsStaff') == 1) ? true : false;
			$this->load->model('Mchats');
			$listChats = $this->Mchats->getLastedChat($userId, true, $isStaff);
			$data = array();
			foreach($listChats as $c){
				$user = false;
				if($isStaff) {
					if ($c['CustomerId'] > 0) $user = $this->Musers->get($c['CustomerId'], true, "", "FullName, Avatar");
				}
				else{
					if($c['StaffId'] > 0) $user = $this->Musers->get($c['StaffId'], true, "", "FullName, Avatar");
				}
				if($user) {
					$c['FullName'] = $user['FullName'];
					$c['Avatar'] = empty($user['Avatar']) ? NO_IMAGE : $user['Avatar'];
				}
				else{
					$c['FullName'] = 'No Name';
					$c['Avatar'] = NO_IMAGE;
				}
				$c['CrTime'] = ddMMyyyy($c['CrDateTime'], 'H:i:s');
				$c['CrDate'] = ddMMyyyy($c['CrDateTime'], 'd/m/Y');
				$data[]=$c;
			}
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getCountChatUnReadByStaff(){
		$customerId = $this->input->post('CustomerId');
		$staffId = $this->input->post('StaffId');
		if($customerId > 0 && $staffId > 0){
			$this->load->model('Mchats');
			echo $this->Mchats->getCount(array('CustomerId' => $customerId, 'StaffId' => $staffId, 'IsCustomerSend' => 1, 'IsStaffRead' => 0));
		}
		else echo 0;
	}

	public function updateCountChatUnRead(){
		$customerId = $this->input->post('CustomerId');
		$staffId = $this->input->post('StaffId');
		if($customerId > 0 && $staffId > 0){
			$this->load->model('Mchats');
			$isStaff = ($this->input->post('IsStaff') == 1) ? true : false;
			$this->Mchats->updateReadMessage($customerId, $staffId, $isStaff);
			echo 1;
		}
		else echo 0;
	}

	public function getListCustomerChat(){
		$user = $this->session->userdata('user');
		$staffId = $this->input->post('StaffId');
		if($user && $staffId > 0) {
			$this->load->model('Mchats');
			$listChats = $this->Mchats->getLastedChat($staffId);
			$data = array();
			$customerIds = array();
			$listCustomers = $this->Musers->getListByRole(4);
			$listCustomerIds = array();
			$roleId = $user['RoleId'];
			if($roleId == 1) $listCustomerIds = $this->Musers->getCustomerIdsForStaff($user['UserId']);
			$searchCustomerName = strtolower(trim($this->input->post('CustomerName')));
			if(!empty($listChats)) {
				$listCountUnRead = $this->Mchats->getListCountUnRead($staffId);
				foreach ($listChats as $c) {
					$c['CountMsg'] = isset($listCountUnRead[$c['CustomerId']]) ? $listCountUnRead[$c['CustomerId']] : 0;
					$customer = false;
					foreach($listCustomers as $c1){
						if($c1['UserId'] == $c['CustomerId']){
							$customer = $c1;
							break;
						}
					}
					if($customer) {
						if($roleId == 3) $flag = true;
						else {
							$flag = false;
							if (in_array($c['CustomerId'], $listCustomerIds)) $flag = true;
						}
						$c['CustomerName'] = $customer['FullName'];
						$c['CustomerAvatar'] = empty($customer['Avatar']) ? NO_IMAGE : $customer['Avatar'];
						$c['CustomerEmail'] = ($flag) ? $customer['Email'] : '';
						$c['CustomerPhone'] = ($flag) ? $customer['PhoneNumber'] : '';
						$c['CustomerTypeId'] = 2;
						$customerIds[] = $customer['UserId'];
					}
					else{
						$c['CustomerName'] = 'No Name';
						$c['CustomerAvatar'] = NO_IMAGE;
						$c['CustomerEmail'] = '';
						$c['CustomerPhone'] = '';
						$c['CustomerTypeId'] = 2;
					}
					if(!empty($searchCustomerName)){
						if(strpos(strtolower($c['CustomerName']), $searchCustomerName) !== false) $data[] = $c;
					}
					else $data[] = $c;
				}
			}
			foreach($listCustomers as $c){
				if(!in_array($c['UserId'], $customerIds)){
					if($roleId == 3) $flag = true;
					elseif($roleId == 1){
						$flag = false;
						if(in_array($c['UserId'], $listCustomerIds)) $flag = true;
					}
					$c1 =  array(
						'StaffId' => $staffId,
						'CustomerId' => $c['UserId'],
						'CustomerName' => $c['FullName'],
						'CustomerAvatar' => empty($c['Avatar']) ? NO_IMAGE : $c['Avatar'],
						'CustomerEmail' => ($flag) ? $c['Email'] : '',
						'CustomerPhone' => ($flag) ? $c['PhoneNumber'] : '',
						'CountMsg' => 0,
						'IsCustomerSend' => 1,
						'Message' => '',
						'CustomerTypeId' => 3
					);
					if(!empty($searchCustomerName)){
						if(strpos(strtolower($c['FullName']), $searchCustomerName) !== false) $data[] = $c1;
					}
					else $data[] = $c1;
					$customerIds[] = $c['UserId'];
				}
			}
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
	}

	public function getListStaffChat(){
		$data = array();
		$listStaffs = $this->Musers->getListByRole(1);
		$customerId = $this->input->post('CustomerId');
		if($customerId > 0){
			$this->load->model('Mchats');
			$listChats = $this->Mchats->getLastedChat($customerId, false, false);
			$staffIds = array();
			$searchStaffName = trim($this->input->post('StaffName'));
			if(!empty($listChats)) {
				$listCountUnRead = $this->Mchats->getListCountUnRead($customerId, false);
				foreach ($listChats as $c) {
					$c['CountMsg'] = isset($listCountUnRead[$c['StaffId']]) ? $listCountUnRead[$c['StaffId']] : 0;
					$staff = false;
					foreach($listStaffs as $sf){
						if($c['StaffId'] == $sf['UserId']){
							$staff = $sf;
							break;
						}
					}
					if($staff) {
						$c['StaffName'] = $staff['FullName'];
						$c['StaffAvatar'] = empty($staff['Avatar']) ? NO_IMAGE : $staff['Avatar'];
						$staffIds[] = $staff['UserId'];
					}
					else{
						$c['StaffName'] = 'No Name';
						$c['StaffAvatar'] = NO_IMAGE;
					}
					if(!empty($searchStaffName)){
						if(strpos(strtolower($c['StaffName']), $searchStaffName) !== false) $data[] = $c;
					}
					else $data[] = $c;
				}
			}
			foreach($listStaffs as $sf){
				if(!in_array($sf['UserId'], $staffIds)){
					$c =  array(
						'StaffId' => $sf['UserId'],
						'CustomerId' => $customerId,
						'StaffName' => $sf['FullName'],
						'StaffAvatar' => empty($sf['Avatar']) ? NO_IMAGE : $sf['Avatar'],
						'CountMsg' => 0,
						'IsCustomerSend' => 0,
						'Message' => ''
					);
					if(!empty($searchStaffName)){
						if(strpos(strtolower($c['StaffName']), $searchStaffName) !== false) $data[] = $c;
					}
					else $data[] = $c;
					$staffIds[] = $sf['UserId'];
				}
			}
			echo json_encode(array('code' => 1, 'data' => $data));
		}
		//else echo json_encode(array('code' => -1, 'message' => "Có lỗi xảy ra trong quá trình thực hiện"));
		else{
			foreach($listStaffs as $sf){
				$data[] = array(
					'StaffId' => $sf['UserId'],
					'CustomerId' => $customerId,
					'StaffName' => $sf['FullName'],
					'StaffAvatar' => empty($sf['Avatar']) ? NO_IMAGE : $sf['Avatar'],
					'CountMsg' => 0,
					'IsCustomerSend' => 0,
					'Message' => ''
				);
			}
			echo json_encode(array('code' => 1, 'data' => $data));
		}
	}
}
