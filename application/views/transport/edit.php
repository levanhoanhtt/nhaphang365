<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($transportId > 0){ ?>
                    <?php echo form_open('transport/update', array('id' => 'transportForm')); ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Thời gian</label>
                                <input type="text" name="PaidDateTime" class="form-control" disabled value="<?php echo ddMMyyyy($transport['PaidDateTime'], 'd/m/Y H:i'); ?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Khách hàng</label>
                                <input type="text" class="form-control" disabled value="<?php echo $customerName; ?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Chi nhánh</label>
                                <input type="text" class="form-control" disabled value="<?php echo $warehouseName; ?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Phương thức giao hàng</label>
                                <?php echo $this->Mtransporttypes->getSelectHtml($listTransportTypes, $transport['TransportTypeId']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Nhân viên giao hàng</label>
                                <?php $this->Mconstants->selectObject($listTransportUsers, 'TransportUserId', 'TransportUserName', 'TransportUserId', $transport['TransportUserId'], true, 'Nhân viên giao hàng', ' select2'); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Tổng kiện hàng bàn giao</label>
                                <input type="text" id="packageCount" class="form-control hmdrequiredCost1 cost" disabled value="<?php echo $transport['PackageCount']; ?>" data-field="Tổng kiện hàng bàn giao">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Tổng kg</label>
                                <input type="text" id="weight" class="form-control hmdrequiredCost1 cost2" disabled value="<?php echo $transport['Weight']; ?>" data-field="Tổng KG">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Đơn giá/ 1 kg</label>
                                <input type="text" id="weightCost" class="form-control hmdrequiredCost1 cost cost1" disabled value="<?php echo priceFormat($transport['WeightCost']); ?>" data-field="Đơn giá">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Phát sinh</label>
                                <input type="text" id="otherCost" class="form-control cost cost1" disabled value="<?php echo priceFormat($transport['OtherCost']); ?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Lý do</label>
                                <?php $this->Mconstants->selectConstants('transportReasons', 'TransportReasonId', $transport['TransportReasonId']); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Số tiền</label>
                                <input type="text" id="paidVN" class="form-control hmdrequiredCost1 cost" disabled value="<?php echo priceFormat($transport['PaidVN']); ?>" data-field="Số tiền">
                            </div>
                        </div>
                        <div class="col-sm-3" style="margin-top: 30px;">
                            <div class="form-group">
                                <input type="checkbox" id="cbCheck"<?php if($transport['StatusId'] == 2) echo ' checked="checked"'; ?>> Đã thu tiền
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ghi chú</label>
                        <textarea rows="2" name="Comment" class="form-control"><?php echo $transport['Comment']; ?></textarea>
                    </div>
                    <div class="form-group text-right">
                        <input type="text" id="statusId" name="StatusId" hidden="hidden" value="<?php echo $transport['StatusId']; ?>">
                        <input type="text" id="transportId" name="TransportId" hidden="hidden" value="<?php echo $transportId; ?>">
                        <input type="text" name="CustomerId" hidden="hidden" value="<?php echo $transport['CustomerId']; ?>">
                        <input type="text" name="WarehouseId" hidden="hidden" value="<?php echo $transport['WarehouseId']; ?>">
                        <input type="text" name="PackageCount" hidden="hidden" value="<?php echo $transport['PackageCount']; ?>">
                        <input type="text" name="Weight" hidden="hidden" value="<?php echo $transport['Weight']; ?>">
                        <input type="text" name="WeightCost" hidden="hidden" value="<?php echo $transport['WeightCost']; ?>">
                        <input type="text" name="OtherCost" hidden="hidden" value="<?php echo $transport['OtherCost']; ?>">
                        <input type="text" name="PaidVN" hidden="hidden" value="<?php echo $transport['PaidVN']; ?>">
                        <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                        <input type="text" id="transportListUrl" hidden="hidden" value="<?php echo base_url('transport'); ?>">
                    </div>
                    <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>