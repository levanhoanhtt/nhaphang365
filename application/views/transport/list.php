<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('transport'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', set_value('CustomerId'), true, 'Khách hàng', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId'), true, 'Trạng thái'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listWarehouses, "WarehouseId", "WarehouseName", "WarehouseId", set_value('WarehouseId'), true, 'Chi nhánh'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listTransportUsers, 'TransportUserId', 'TransportUserName', 'TransportUserId', set_value('TransportUserId'), true, 'Nhân viên giao hàng', ' select2'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php echo $this->Mtransporttypes->getSelectHtml($listTransportTypes, set_value('TransportTypeId'), true); ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml('<a href="'.base_url('transport/add').'" class="btn btn-primary">Thêm mới</a>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Khách hàng</th>
                                <th>Thông tin</th>
                                <th>Số tiền</th>
                                <th>Chi nhánh</th>
                                <th>Nhân viên</th>
                                <th>Ghi chú</th>
                                <th>Trạng thái</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransport">
                            <?php $i = 0;
                            $status = array(1 => 'Chưa thu tiền', 2 => 'Đã thu tiền');
                            $labelCss = $this->Mconstants->labelCss;
                            $transportReasons = $this->Mconstants->transportReasons;
                            foreach($listTransports as $t){
                                $i++; ?>
                                <tr id="transport_<?php echo $t['TransportId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td>
                                        <?php $customerName = $this->Mconstants->getObjectValue($listCustomers, 'UserId', $t['CustomerId'], 'FullName');
                                        echo empty($customerName) ? 'NoName' : '<a href="'.base_url('transaction/customer/'.$t['CustomerId']).'">'.$customerName.'</a>'; ?>
                                        <br/><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?>
                                    </td>
                                    <td>
                                        <p>Tổng kiện hàng bàn giao: <?php echo $t['PackageCount']; ?></p>
                                        <p>Tổng kg: <?php echo priceFormat($t['Weight'], true); ?></p>
                                        <p>Đơn giá/ 1 kg: <?php echo priceFormat($t['WeightCost']); ?></p>
                                        <?php if($t['OtherCost'] > 0){ ?>
                                            <p>Phát sinh: <?php echo priceFormat($t['OtherCost']); ?></p>
                                            <p>Lý do: <?php echo $transportReasons[$t['TransportReasonId']]; ?></p>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo priceFormat($t['PaidVN']); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listWarehouses, 'WarehouseId', $t['WarehouseId'], 'WarehouseName'); ?></td>
                                    <td>
                                        <?php echo $this->Mconstants->getObjectValue($listTransportUsers, 'TransportUserId', $t['TransportUserId'], 'TransportUserName'); ?><br/>
                                        <span class="label label-default"><?php echo $this->Mconstants->getObjectValue($listTransportTypes, 'TransportTypeId', $t['TransportTypeId'], 'TransportTypeName'); ?></span>
                                    </td>
                                    <td><?php echo $t['Comment']; ?></td>
                                    <td id="statusName_<?php echo $t['TransportId']; ?>"><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                    <td class="actions">
                                        <?php if($t['StatusId'] != STATUS_ACTIVED) { ?>
                                            <a href="javascript:void(0)" class="link_status" data-id="<?php echo $t['TransportId']; ?>" title="Đã thu tiền"><i class="fa fa-check"></i></a>
                                        <?php } ?>
                                        <a href="<?php echo base_url('transport/edit/'.$t['TransportId']) ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeStatus" value="<?php echo $changeStatus ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('transport/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>