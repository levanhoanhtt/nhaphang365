<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php echo form_open('transport/update', array('id' => 'transportForm')); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Thời gian</label>
                            <input type="text" name="PaidDateTime" class="form-control" disabled value="<?php echo date('d/m/Y H:i'); ?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Khách hàng</label>
                            <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', set_value('CustomerId'), false, '', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Chi nhánh</label>
                            <?php $this->Mconstants->selectObject($listWarehouses, "WarehouseId", "WarehouseName", "WarehouseId", set_value('WarehouseId')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Phương thức giao hàng</label>
                            <?php echo $this->Mtransporttypes->getSelectHtml($listTransportTypes); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Nhân viên giao hàng</label>
                            <?php $this->Mconstants->selectObject($listTransportUsers, 'TransportUserId', 'TransportUserName', 'TransportUserId', set_value('TransportUserId'), true, 'Nhân viên giao hàng', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Tổng kiện hàng bàn giao</label>
                            <input type="text" name="PackageCount" id="packageCount" class="form-control hmdrequiredCost1 cost" value="0" data-field="Tổng kiện hàng bàn giao">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Tổng kg</label>
                            <input type="text" name="Weight" id="weight" class="form-control hmdrequiredCost1 cost2" value="0" data-field="Tổng KG">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Đơn giá/ 1 kg</label>
                            <input type="text" name="WeightCost" id="weightCost" class="form-control hmdrequiredCost1 cost cost1" value="0" data-field="Đơn giá">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Phát sinh</label>
                            <input type="text" name="OtherCost" id="otherCost" class="form-control cost cost1" value="0">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Lý do</label>
                            <?php $this->Mconstants->selectConstants('transportReasons', 'TransportReasonId', set_value('TransportReasonId')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số tiền</label>
                            <input type="text" name="PaidVN" id="paidVN" class="form-control hmdrequiredCost1 cost" value="0" data-field="Số tiền">
                        </div>
                    </div>
                    <div class="col-sm-3" style="margin-top: 30px;">
                        <div class="form-group">
                            <input type="checkbox" id="cbCheck"> Đã thu tiền
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Ghi chú</label>
                    <textarea rows="2" name="Comment" class="form-control"></textarea>
                </div>
                <div class="form-group text-right">
                    <input type="text" id="statusId" name="StatusId" hidden="hidden" value="1">
                    <input type="text" id="transportId" name="TransportId" hidden="hidden" value="0">
                    <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                    <input type="text" id="transportListUrl" hidden="hidden" value="<?php echo base_url('transport'); ?>">
                    <?php foreach($listTransportFees as $tf){ ?>
                        <input type="text" hidden="hidden" class="transportFee" data-id="<?php echo $tf['WarehouseId']; ?>" data-min="<?php echo $tf['BeginWeight']; ?>" data-max="<?php echo $tf['EndWeight']; ?>" value="<?php echo priceFormat($tf['WeightCost']); ?>">
                    <?php } ?>
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>