<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <select class="form-control" id="categoryTypeIdSearch">
                                        <option value="3"<?php if($categoryTypeId == 3) echo ' selected="selected"'; ?>>Sản phẩm Tao bao</option>
                                        <option value="4"<?php if($categoryTypeId == 4) echo ' selected="selected"'; ?>>Sản phẩm Tmail</option>
                                        <option value="5"<?php if($categoryTypeId == 5) echo ' selected="selected"'; ?>>Sản phẩm 1688</option>
                                    </select>
                                    <input type="text" hidden="hidden" id="categoryProductUrl" value="<?php echo base_url('cms/category/product'); ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <style>
                        #tbodyCategory img{max-height: 100px;}
                        #tbodyCategory #btnChooseImage{display: block;margin-bottom: 5px;}
                    </style>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Thứ tự</th>
                                <th>Ảnh</th>
                                <th>Tên Tiếng Việt</th>
                                <th>Tên Trung Quốc</th>
                                <th>Công cụ</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCategory">
                            <?php foreach($listCategories as $c){ ?>
                                <tr id="category_<?php echo $c['CategoryId']; ?>">
                                    <td>
                                        <?php $attrSelect = ' onchange="changeDisplayOrder(this, \'' . $c['CategoryId'] . '\')" data-id="'.$c['CategoryId'].'"';
                                        $this->Mconstants->selectNumber(1, 100, 'DisplayOrder_'.$c['CategoryId'], $c['DisplayOrder'], true, $attrSelect); ?>
                                    </td>
                                    <td><img src="<?php echo IMAGE_PATH.$c['CategoryImage']; ?>" id="imgCategory_<?php echo $c['CategoryId']; ?>"></td>
                                    <td id="categoryName_<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryName']; ?></td>
                                    <td id="categoryNameTQ_<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryNameTQ']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $c['CategoryId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $c['CategoryId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('cms/category/update', array('id' => 'categoryForm')); ?>
                                <td><?php $this->Mconstants->selectNumber(1, 100, 'DisplayOrder', 1, true); ?></td>
                                <td>
                                    <button type="button" class="btn btn-default" id="btnChooseImage">Chọn ảnh</button>
                                    <img src="<?php echo IMAGE_PATH.NO_PRODUCT; ?>" id="imgCategory" style="display: none;">
                                </td>
                                <td><input type="text" class="form-control hmdrequired" id="categoryName" name="CategoryName" value="" data-field="Tên Tiếng Việt"></td>
                                <td><input type="text" class="form-control hmdrequired" id="categoryNameTQ" name="CategoryNameTQ" value="" data-field="Tên Trung Quốc"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="CategoryId" id="categoryId" value="0" hidden="hidden">
                                    <input type="text" class="hmdrequired" name="CategoryImage" id="categoryImage" value="" hidden="hidden" data-field="Ảnh sản phẩm">
                                    <input type="text" name="CategoryTypeId" id="categoryTypeId" value="<?php echo $categoryTypeId; ?>" hidden="hidden">
                                    <input type="text" name="ParentCategoryId" id="parentCategoryId" value="0" hidden="hidden">
                                    <input type="text" name="CategoryLevel" id="categoryLevel" value="1" hidden="hidden">
                                    <input type="text" name="StatusId" value="2" hidden="hidden">
                                    <input type="text" hidden="hidden" id="changeDisplayOrderUrl" value="<?php echo base_url('cms/category/changeDisplayOrder'); ?>">
                                    <input type="text" id="deleteCategoryUrl" value="<?php echo base_url('cms/category/delete'); ?>" hidden="hidden">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>