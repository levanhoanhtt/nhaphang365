<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Thứ tự</th>
                                <th>Chuyên mục</th>
                                <th>Slug</th>
                                <th>Chuyên mục cha</th>
                                <th>Công cụ</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyCategory">
                            <?php foreach($listCategories as $c){ ?>
                                <tr id="category_<?php echo $c['CategoryId']; ?>">
                                    <td>
                                        <?php $attrSelect = ' onchange="changeDisplayOrder(this, \'' . $c['CategoryId'] . '\')" data-id="'.$c['CategoryId'].'"';
                                        $this->Mconstants->selectNumber(1, 100, 'DisplayOrder_'.$c['CategoryId'], $c['DisplayOrder'], true, $attrSelect); ?>
                                    </td>
                                    <td id="categoryName_<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryName']; ?></td>
                                    <td id="categoryNameTQ_<?php echo $c['CategoryId']; ?>"><?php echo $c['CategoryNameTQ']; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listCategories, 'CategoryId', $c['ParentCategoryId'], 'CategoryName'); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $c['CategoryId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $c['CategoryId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" id="parentCategoryId_<?php echo $c['CategoryId']; ?>" value="<?php echo $c['ParentCategoryId']; ?>" hidden="hidden">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('cms/category/update', array('id' => 'categoryForm')); ?>
                                <td><?php $this->Mconstants->selectNumber(1, 100, 'DisplayOrder', 1, true); ?></td>
                                <td><input type="text" class="form-control hmdrequired" id="categoryName" name="CategoryName" value="" data-field="Tên chuyên mục"></td>
                                <td><input type="text" class="form-control" id="categoryNameTQ" name="CategoryNameTQ" value=""></td>
                                <td><?php echo $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'ParentCategoryId', 0, true, 'Không có'); ?></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="CategoryId" id="categoryId" value="0" hidden="hidden">
                                    <input type="text" name="CategoryNameTQ" value="" hidden="hidden">
                                    <input type="text" name="CategoryImage" value="" hidden="hidden">
                                    <input type="text" name="CategoryTypeId" id="categoryTypeId" value="2" hidden="hidden">
                                    <input type="text" name="CategoryLevel" value="1" hidden="hidden">
                                    <input type="text" name="StatusId" value="2" hidden="hidden">
                                    <input type="text" hidden="hidden" id="changeDisplayOrderUrl" value="<?php echo base_url('cms/category/changeDisplayOrder'); ?>">
                                    <input type="text" id="deleteCategoryUrl" value="<?php echo base_url('cms/category/delete'); ?>" hidden="hidden">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>