<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <?php $status = $this->Mconstants->status;
            $status[4] = 'KT đã duyệt'; ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('transaction/invoice'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="TransactionCode" class="form-control" value="<?php echo set_value('TransactionCode'); ?>" placeholder="ID phiếu">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', set_value('CustomerId'), true, 'Khách hàng', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php //$this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId'), true, 'Trạng thái'); ?>
                                <select class="form-control" name="StatusId" id="statusId">
                                    <option value="0">Trạng thái</option>
                                    <?php foreach($status as $i => $v){ ?>
                                        <option value="<?php echo $i; ?>"><?php echo $v; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listWarehouses, "WarehouseId", "WarehouseName", "WarehouseId", set_value('WarehouseId'), true, 'Chi nhánh'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listMoneySources, "MoneySourceId", "MoneySourceName", "MoneySourceId", set_value('MoneySourceId'), true, 'Nguồn tiền'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('transactionRanges', 'TransactionRangeId', set_value('TransactionRangeId'), true, 'Khoản mục'); ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml('<a href="'.base_url('transaction/add/invoice').'" class="btn btn-primary">Thêm mới</a>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã</th>
                                <th>Khách hàng</th>
                                <th>Thời gian</th>
                                <th>Mã đơn</th>
                                <th>Số tiền</th>
                                <th>Khoản mục</th>
                                <th>Nguồn tiền</th>
                                <th>Chi nhánh</th>
                                <th>Nhân viên</th>
                                <th>Ghi chú</th>
                                <th>Trạng thái</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransaction">
                            <?php $i = 0;
                            $isChangeStatus = $user['RoleId'] == 3 || $user['RoleId'] == 6;
                            $labelCss = $this->Mconstants->labelCss;
                            $status1 = $status;
                            if($user['RoleId'] == 3) unset($status1[4]);
                            else unset($status1[2]);
                            $fullNames = array();
                            $orderCodes = array();
                            foreach($listTransactions as $t){
                                $i++;
                                if(!isset($fullNames[$t['CrUserId']])) $fullNames[$t['CrUserId']] = $this->Musers->getFieldValue(array('UserId' => $t['CrUserId']), 'FullName');
                                ?>
                                <tr id="transaction_<?php echo $t['TransactionId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('transaction/edit/'.$t['TransactionId']); ?>">#<?php echo $t['TransactionId']; ?></a></td>
                                    <td>
                                        <?php $customerName = $this->Mconstants->getObjectValue($listCustomers, 'UserId', $t['CustomerId'], 'FullName');
                                        echo empty($customerName) ? 'NoName' : '<a href="'.base_url('transaction/customer/'.$t['CustomerId']).'">'.$customerName.'</a>'; ?>
                                    </td>
                                    <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td>
                                        <?php if($t['OrderId'] > 0) {
                                            if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');; ?>
                                            <a href="<?php echo base_url('order/edit/'.$t['OrderId']); ?>"><?php echo $orderCodes[$t['OrderId']]; ?></a>
                                        <?php } ?>
                                    </td>
                                    <td><?php echo priceFormat($t['PaidVN']); ?></td>
                                    <td><?php echo $this->Mconstants->transactionRanges[$t['TransactionRangeId']]; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listMoneySources, 'MoneySourceId', $t['MoneySourceId'], 'MoneySourceName'); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listWarehouses, 'WarehouseId', $t['WarehouseId'], 'WarehouseName'); ?></td>
                                    <td><a href="<?php echo base_url('user/edit/'.$t['CrUserId']); ?>"><?php echo $fullNames[$t['CrUserId']]; ?></a></td>
                                    <td><?php echo $t['Comment']; ?></td>
                                    <td id="statusName_<?php echo $t['TransactionId']; ?>"><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                    <td class="actions">
                                        <?php if($t['StatusId'] != STATUS_ACTIVED && $isChangeStatus) {
                                            if($user['RoleId'] == 3){ ?>
                                                <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $t['TransactionId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                            <?php } ?>
                                        <div class="btn-group" id="btnGroup_<?php echo $t['TransactionId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status1 as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $t['TransactionId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <?php } ?>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['StatusId']; ?>">
                                        <input type="text" hidden="hidden" id="orderId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['OrderId']; ?>">
                                        <input type="text" hidden="hidden" id="customerId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['CustomerId']; ?>">
                                        <input type="text" hidden="hidden" id="paidVN_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['PaidVN']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeStatus" value="<?php echo $changeStatus ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('transaction/changeStatus'); ?>">
                    <input type="text" hidden="hidden" id="transactionTypeId" value="1">
                    <input type="text" hidden="hidden" id="transactionTypeName" value="Phiếu thu">
                    <input type="text" hidden="hidden" id="isChangeStatus" value="1">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>