<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div style="display: none;">
                    <?php echo form_open('transaction/noName'); ?>
                    <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                    <?php echo form_close(); ?>
                </div>
                <div class="box box-success">
                    <?php sectionTitleHtml('<a href="'.base_url('transaction/add/invoice').'" class="btn btn-primary">Thêm mới</a>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã</th>
                                <th>Gắn vào khách</th>
                                <th>Số tiền</th>
                                <th>Thời gian</th>
                                <th>Ghi chú</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransaction">
                            <?php $i = 0;
                            $isChangeStatus = $user['RoleId'] == 3 || $user['RoleId'] == 6;
                            $status = $this->Mconstants->status;
                            foreach($listTransactions as $t){
                                $i++; ?>
                                <tr id="transaction_<?php echo $t['TransactionId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('transaction/edit/'.$t['TransactionId']); ?>">#<?php echo $t['TransactionId']; ?></a></td>
                                    <td>
                                        <?php $customerName = $this->Mconstants->getObjectValue($listCustomers, 'UserId', $t['CustomerId'], 'FullName');
                                        echo empty($customerName) ? 'NoName' : '<a href="'.base_url('transaction/customer/'.$t['CustomerId']).'">'.$customerName.'</a>'; ?>
                                    </td>
                                    <td><?php echo priceFormat($t['PaidVN']); ?></td>
                                    <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo $t['Comment']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $t['TransactionId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <?php if($t['StatusId'] != STATUS_ACTIVED && $isChangeStatus) { ?>
                                        <div class="btn-group" id="btnGroup_<?php echo $t['TransactionId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $t['TransactionId']; ?>" data-status="<?php echo $j; ?>" data-order="<?php echo $t['OrderId']; ?>" data-customer="<?php echo $t['CustomerId']; ?>" data-paid="<?php echo $t['PaidVN']; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <?php } ?>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['StatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="deleteTransaction" value="<?php echo $deleteTransaction ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="changeStatus" value="<?php echo $changeStatus ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('transaction/changeStatus'); ?>">
                    <input type="text" hidden="hidden" id="transactionTypeId" value="1">
                    <input type="text" hidden="hidden" id="transactionTypeName" value="Phiếu thu">
                    <input type="text" hidden="hidden" id="isChangeStatus" value="0">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>