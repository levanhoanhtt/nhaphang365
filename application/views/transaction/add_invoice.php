<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php echo form_open('transaction/update', array('id' => 'transactionForm')); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Thời gian</label>
                            <input type="text" name="PaidDateTime" class="form-control" disabled value="<?php echo date('d/m/Y H:i'); ?>">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Khách hàng</label>
                            <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', set_value('CustomerId'), true, 'NoName', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Chi nhánh</label>
                            <?php $this->Mconstants->selectObject($listWarehouses, "WarehouseId", "WarehouseName", "WarehouseId", set_value('WarehouseId')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Khoản mục</label>
                            <?php $this->Mconstants->selectConstants('transactionRanges', 'TransactionRangeId', set_value('TransactionRangeId')); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Đơn hàng</label>
                            <select class="form-control select2" name="OrderId" id="orderId">
                                <option value="0">Chưa rõ</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Nguồn tiền</label>
                            <?php $this->Mconstants->selectObject($listMoneySources, "MoneySourceId", "MoneySourceName", "MoneySourceId", set_value('MoneySourceId')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Số tiền</label>
                            <div class="input-group">
                                <input type="text" id="paidCost" class="form-control hmdrequiredCost1 cost1" value="0" data-field="Số tiền">
                                <div class="input-group-btn" id="divCurrency">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span id="curentCurrency">VNĐ</span>
                                        <span class="fa fa-caret-down"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)" class="aCurrency" data-id="1">VNĐ</a></li>
                                        <li><a href="javascript:void(0)" class="aCurrency" data-id="2">Tệ</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group divTQ" style="display: none;">
                            <label class="control-label">Tỉ giá</label>
                            <input type="text" name="ExchangeRate" id="exchangeRate" class="form-control cost" value="1">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group divTQ" style="display: none;">
                            <label class="control-label">Số tiền (VNĐ)</label>
                            <input type="text" name="PaidVN" id="paidVN" class="form-control cost" value="0">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Ghi chú</label>
                    <textarea rows="2" name="Comment" id="comment" class="form-control"></textarea>
                </div>
                <div class="form-group text-right">
                    <input type="text" name="TransactionId" id="transactionId" hidden="hidden" value="0">
                    <input type="text" name="PaidTQ" id="paidTQ" hidden="hidden" value="0">
                    <input type="text" id="statusId" hidden="hidden" value="1">
                    <input type="text" name="TransactionTypeId" id="transactionTypeId" hidden="hidden" value="1">
                    <input type="text" name="ComplaintId" hidden="hidden" value="0">
                    <input type="text" id="exchangeRateTQ" hidden="hidden" value="<?php echo priceFormat($exchangeRate); ?>">
                    <input type="text" id="currencyId" hidden="hidden" value="1">
                    <input type="text" id="comfirm" hidden="hidden" value="">
                    <input type="text" id="getListOrderUrl" hidden="hidden" value="<?php echo base_url('order/getList'); ?>">
                    <input type="text" id="transactionEditUrl" hidden="hidden" value="<?php echo base_url('transaction/edit'); ?>">
                    <input type="text" id="transactionNoNameUrl" hidden="hidden" value="<?php echo base_url('transaction/noName'); ?>">
                    <input type="text" id="transactionInvoiceUrl" hidden="hidden" value="<?php echo base_url('transaction/invoice'); ?>">
                    <input type="text" id="transactionExpenseUrl" hidden="hidden" value="<?php echo base_url('transaction/expense'); ?>">
                    <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>