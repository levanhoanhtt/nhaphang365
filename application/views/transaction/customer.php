<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <?php if($customerId > 0){ ?>
                <section class="content">
                    <div class="box box-default">
                        <?php sectionTitleHtml('Tìm kiếm'); ?>
                        <div class="box-body row-margin">
                            <?php echo form_open('transaction/customer/'.$customerId); ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', $customerId, true, 'Khách hàng', ' select2'); ?>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="TransactionCode" class="form-control" value="<?php echo set_value('TransactionCode'); ?>" placeholder="ID phiếu">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <ul class="list-inline">
                                        <li><input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm"></li>
                                        <li><button type="button" class="btn btn-primary" id="btnOrder">Danh sách đơn hàng</button></li>
                                    </ul>
                                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                    <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerId; ?>">
                                    <input type="text" id="getListOrderUrl" hidden="hidden" value="<?php echo base_url('order/getList02'); ?>">
                                    <input type="text" id="transactionCustomerUrl" hidden="hidden" value="<?php echo base_url('transaction/customer'); ?>">
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="modalOrder">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Danh sách đơn hàng</h4>
                                </div>
                                <div class="modal-body box box-success">
                                    <div class="box-header with-border" id="paggingOrder" style="display: none;">
                                        <h3 class="box-title"></h3>
                                        <div class="box-tools pull-right"></div>
                                    </div>
                                    <div class="box-body table-responsive no-padding divTable">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Đơn hàng</th>
                                                <th>Ngày tạo</th>
                                                <th>Trạng thái</th>
                                                <th>Tổng tiền</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyOrder"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <style>
                    #userForm .form-group{padding: 0 5px;}
                    #spanBalance{font-weight: bold;color: red;font-size: 22px;}
                    .divTable i.fa{cursor: pointer;}
                    #tbodyTransaction tr.thead{background-color:#f5f5f5;}
                </style>
                <section class="content">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="box box-success">
                                <?php sectionTitleHtml('Thông tin Khách hàng'); ?>
                                <?php if($customer){ ?>
                                    <?php echo form_open('user/saveUser', array('id' => 'userForm')); ?>
                                    <div class="form-group">
                                        <label class="control-label">Tên khách hàng <span class="required">*</span></label>
                                        <input type="text" name="FullName" class="form-control hmdrequired" value="<?php echo $customer['FullName']; ?>" data-field="Tên khách hàng">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email/Facebook</label>
                                        <input type="text" name="Email" class="form-control" value="<?php echo $customer['Email']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Di động</label>
                                        <input type="text" name="PhoneNumber" class="form-control" value="<?php echo $customer['PhoneNumber']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Tỉnh/ Thành phố</label>
                                        <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $customer['ProvinceId'], false, '', ' select2'); ?>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Địa chỉ <span class="required">*</span></label>
                                        <input type="text" name="Address" class="form-control hmdrequired" value="<?php echo $customer['Address']; ?>" data-field="Địa chỉ">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ghi chú</label>
                                        <input type="text" name="Comment" class="form-control" value="<?php echo $customer['Comment']; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Mật khẩu mới</label>
                                        <input type="password" id="newPass" name="NewPass" class="form-control" value="">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Gõ lại Mật khẩu</label>
                                        <input type="password" id="rePass" name="RePass" class="form-control" value="">
                                    </div>
                                    <div class="form-group" style="padding-bottom: 5px;">
                                        <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                                        <input type="text" name="UserId" id="userId" hidden="hidden" value="<?php echo $customer['UserId']; ?>">
                                        <input type="text" name="GenderId" hidden="hidden" value="<?php echo $customer['GenderId']; ?>">
                                        <input type="text" name="StatusId" hidden="hidden" value="<?php echo $customer['StatusId']; ?>">
                                        <input type="text" name="RoleId" hidden="hidden" value="4">
                                        <input type="text" hidden="hidden" name="UserTypeName" value="khách hàng">
                                    </div>
                                    <?php echo form_close(); ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="box box-success">
                                <?php sectionTitleHtml('Tổng dư nợ <span id="spanBalance">'.$customerBalance.'</span> VNĐ', isset($paggingHtml) ? $paggingHtml : ''); ?>
                                <div class="box-body table-responsive no-padding divTable">
                                    <table class="table table-hover table-bordered">
                                        <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th><i class="fa fa-caret-square-o-right all"></i></th>
                                            <th>Thời gian</th>
                                            <th>Đơn hàng</th>
                                            <th>Vận chuyển</th>
                                            <th>Đã thanh toán</th>
                                            <th>Ghi chú</th>
                                            <th>Ghi nợ</th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyTransaction">
                                        <?php $i = 0;
                                        $status = $this->Mconstants->status;
                                        $status[4] = 'KT đã duyệt';
                                        $labelCss = $this->Mconstants->labelCss;
                                        $transactionRanges = $this->Mconstants->transactionRanges;
                                        $orderCodes = array();
                                        foreach($listTransactions as $t){
                                            $balance = $t['Balance'];
                                            $listChildTransactionOrders = array(); // don hang
                                            $listChildTransactionTransports = array(); // van chuyen
                                            foreach($listTransactions as $t1){
                                                if($t1['OrderId'] == $t['OrderId'] && $t1['TransactionId'] != $t['TransactionId'] && ($t1['TransactionRangeId'] == 3 || $t1['TransactionRangeId'] == 4)){
                                                    if($t['TransactionRangeId'] == 1 && $t['StatusId'] == 2) $listChildTransactionOrders[]=$t1;
                                                    elseif($t['TransactionRangeId'] == 2 && $t['StatusId'] == 2) $listChildTransactionTransports[]=$t1;
                                                }
                                            }
                                            if($t['TransactionRangeId'] == 1 && $t['StatusId'] == 2){ //don hang
                                                $i++;
                                                if(empty($listChildTransactionOrders)){ ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td></td>
                                                        <td>
                                                            <?php if($t['OrderId'] > 0){
                                                                if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                                echo '<a href="'.base_url("order/edit/".$t['OrderId']).'">'.$orderCodes[$t['OrderId']].'</a><br/>';
                                                            }
                                                            echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?>
                                                        </td>
                                                        <td class="text-center"><?php if($t['TransactionTypeId'] == 3) echo priceFormat($t['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                        <td></td>
                                                        <td class="text-center"><?php if($t['TransactionTypeId'] == 1 || $t['TransactionTypeId'] == 2) echo priceFormat($t['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                        <td><?php echo $t['Comment']; ?></td>
                                                        <td><?php echo priceFormat($balance); ?></td>
                                                    </tr>
                                                <?php } else{
                                                    if($t['TransactionTypeId'] == 3) {
                                                        $orderPaid = $t['PaidVN'];
                                                        $totalPaid = 0;
                                                    }
                                                    else{
                                                        $orderPaid = 0;
                                                        $totalPaid = $t['PaidVN'];
                                                    }
                                                    foreach($listChildTransactionOrders as $t1){
                                                        $balance = $t1['Balance'];
                                                        if($t1['TransactionRangeId'] == 3){
                                                            if($t['TransactionTypeId'] == 3) $orderPaid -= $t1['PaidVN'];
                                                            elseif($t['TransactionTypeId'] == 1) $totalPaid += $t1['PaidVN'];
                                                            elseif($t['TransactionTypeId'] == 2) $totalPaid -= $t1['PaidVN'];
                                                        }
                                                        elseif($t1['TransactionRangeId'] == 4){
                                                            if($t['TransactionTypeId'] == 3) $orderPaid += $t1['PaidVN'];
                                                            elseif($t['TransactionTypeId'] == 1) $totalPaid -= $t1['PaidVN'];
                                                            elseif($t['TransactionTypeId'] == 2) $totalPaid += $t1['PaidVN'];
                                                        }
                                                    } ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><i class="fa fa-caret-square-o-right" data-id="<?php echo $t['TransactionId']; ?>"></i></td>
                                                        <td>
                                                            <?php if($t['OrderId'] > 0){
                                                                if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                                echo '<a href="'.base_url("order/edit/".$t['OrderId']).'">'.$orderCodes[$t['OrderId']].'</a><br/>';
                                                            }
                                                            echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?>
                                                        </td>
                                                        <td class="text-center"><?php if($orderPaid > 0) echo priceFormat($orderPaid).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                        <td></td>
                                                        <td class="text-center"><?php if($totalPaid > 0) echo priceFormat($totalPaid).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                        <td><?php echo $t['Comment']; ?></td>
                                                        <td><?php echo priceFormat($balance); ?></td>
                                                    </tr>
                                                    <tr class="thead tr<?php echo $t['TransactionId']; ?>" style="display: none;">
                                                        <td colspan="8">
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                <tr>
                                                                    <th>Thời gian</th>
                                                                    <th>Số tiền</th>
                                                                    <th>Loại</th>
                                                                    <th>Trạng thái</th>
                                                                    <th>Ghi chú</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                                    <td class="text-center"><?php echo priceFormat($t['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                                    <td>Đơn hàng</td>
                                                                    <td><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                                                    <td><?php echo $t['Comment']; ?></td>
                                                                </tr>
                                                                <?php foreach($listChildTransactionOrders as $t1){ ?>
                                                                    <tr>
                                                                        <td><?php echo ddMMyyyy($t1['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                                        <td class="text-center"><?php echo priceFormat($t1['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t1['TransactionId']).'">#'.$t1['TransactionId'].'</a>'; ?></td>
                                                                        <td><?php echo $transactionRanges[$t1['TransactionRangeId']] ?></td>
                                                                        <td><span class="<?php echo $labelCss[$t1['StatusId']]; ?>"><?php echo $status[$t1['StatusId']]; ?></span></td>
                                                                        <td><?php echo $t1['Comment']; ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                <?php }
                                            }
                                            elseif($t['TransactionRangeId'] == 2 && $t['StatusId'] == 2) { //van chuyen
                                                $i++;
                                                if(empty($listChildTransactionTransports)){ ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td></td>
                                                        <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                        <td></td>
                                                        <td class="text-center"><?php if($t['TransactionTypeId'] == 3) echo priceFormat($t['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                        <td class="text-center"><?php if($t['TransactionTypeId'] == 1 || $t['TransactionTypeId'] == 2) echo priceFormat($t['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                        <td><?php echo $t['Comment']; ?></td>
                                                        <td><?php echo priceFormat($balance); ?></td>
                                                    </tr>
                                                <?php } else{
                                                    if($t['TransactionTypeId'] == 3) {
                                                        $transportPaid = $t['PaidVN'];
                                                        $totalPaid = 0;
                                                    }
                                                    else{
                                                        $transportPaid = 0;
                                                        $totalPaid = $t['PaidVN'];
                                                    }
                                                    foreach($listChildTransactionTransports as $t1){
                                                        $balance = $t1['Balance'];
                                                        if($t1['TransactionRangeId'] == 3){
                                                            if($t['TransactionTypeId'] == 3) $transportPaid -= $t1['PaidVN'];
                                                            elseif($t['TransactionTypeId'] == 1) $totalPaid += $t1['PaidVN'];
                                                            elseif($t['TransactionTypeId'] == 2) $totalPaid -= $t1['PaidVN'];
                                                        }
                                                        elseif($t1['TransactionRangeId'] == 4){
                                                            if($t['TransactionTypeId'] == 3) $transportPaid += $t1['PaidVN'];
                                                            elseif($t['TransactionTypeId'] == 1) $totalPaid -= $t1['PaidVN'];
                                                            elseif($t['TransactionTypeId'] == 2) $totalPaid += $t1['PaidVN'];
                                                        }
                                                    } ?>
                                                    <tr>
                                                        <td><?php echo $i; ?></td>
                                                        <td><i class="fa fa-caret-square-o-right" data-id="<?php echo $t['TransactionId']; ?>"></i></td>
                                                        <td>
                                                            <?php if($t['OrderId'] > 0){
                                                                if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                                echo '<a href="'.base_url("order/edit/".$t['OrderId']).'">'.$orderCodes[$t['OrderId']].'</a><br/>';
                                                            }
                                                            echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?>
                                                        </td>
                                                        <td></td>
                                                        <td class="text-center"><?php if($transportPaid > 0) echo priceFormat($transportPaid).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                        <td class="text-center"><?php if($totalPaid > 0) echo priceFormat($totalPaid).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                        <td><?php echo $t['Comment']; ?></td>
                                                        <td><?php echo priceFormat($balance); ?></td>
                                                    </tr>
                                                    <tr class="thead tr<?php echo $t['TransactionId']; ?>" style="display: none;">
                                                        <td colspan="8">
                                                            <table class="table table-bordered">
                                                                <thead>
                                                                <tr>
                                                                    <th>Thời gian</th>
                                                                    <th>Số tiền</th>
                                                                    <th>Loại</th>
                                                                    <th>Trạng thái</th>
                                                                    <th>Ghi chú</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody>
                                                                <tr>
                                                                    <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                                    <td class="text-center"><?php echo priceFormat($t['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                                                    <td>Vận chuyển</td>
                                                                    <td><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                                                    <td><?php echo $t['Comment']; ?></td>
                                                                </tr>
                                                                <?php foreach($listChildTransactionTransports as $t1){ ?>
                                                                    <tr>
                                                                        <td><?php echo ddMMyyyy($t1['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                                        <td class="text-center"><?php echo priceFormat($t1['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t1['TransactionId']).'">#'.$t1['TransactionId'].'</a>'; ?></td>
                                                                        <td><?php echo $transactionRanges[$t1['TransactionRangeId']] ?></td>
                                                                        <td><span class="<?php echo $labelCss[$t1['StatusId']]; ?>"><?php echo $status[$t1['StatusId']]; ?></span></td>
                                                                        <td><?php echo $t1['Comment']; ?></td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                <?php }
                                            }
                                        } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php } else { ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>