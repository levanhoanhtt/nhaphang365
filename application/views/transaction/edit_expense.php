<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php //$this->load->view('includes/breadcrumb'); ?>
            <section class="content-header">
                <h1 style="display: inline;"><?php echo $title; ?></h1>
                <?php if($transactionId > 0){ ?>
                    <ul class="list-inline" style="display: inline;margin-left: 20px;">
                        <?php $status = $this->Mconstants->status;
                        $status[4] = 'KT đã duyệt';
                        foreach($status as $i => $v){ ?>
                            <li>
                                <label><input type="radio" name="StatusId" class="iCheck cbStatusId" value="<?php echo $i; ?>"<?php if($i == $transaction['StatusId']) echo ' checked'; ?>> <?php echo $v; ?></label>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url('user/dashboard'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                </ol>
            </section>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($transactionId > 0){ ?>
                    <?php echo form_open('transaction/update', array('id' => 'transactionForm')); ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Thời gian</label>
                                <input type="text" name="PaidDateTime" class="form-control" disabled value="<?php echo ddMMyyyy($transaction['PaidDateTime'], 'd/m/Y H:i'); ?>">
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Khách hàng<?php if($transaction['CustomerId'] == 0) echo ' <span class="label label-warning">NoName</span> gắn cho' ?></label>
                                <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', $transaction['CustomerId'], $transaction['CustomerId'] == 0, 'NoName', ' select2'); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Chi nhánh</label>
                                <?php $this->Mconstants->selectObject($listWarehouses, "WarehouseId", "WarehouseName", "WarehouseId", $transaction['WarehouseId']); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Nguồn tiền</label>
                                <?php $this->Mconstants->selectObject($listMoneySources, "MoneySourceId", "MoneySourceName", "MoneySourceId", set_value('MoneySourceId')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Khoản mục</label>
                                <?php $this->Mconstants->selectConstants('transactionRanges', 'TransactionRangeId', $transaction['TransactionRangeId']); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Đơn hàng</label>
                                <select class="form-control select2" name="OrderId" id="orderId">
                                    <option value="0">Chưa rõ</option>
                                    <?php if($transaction['OrderId'] > 0) echo "<option value='{$transaction['OrderId']}' selected='selected'>Đơn đặt hàng #{$transaction['OrderId']}</option>"; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Khiếu nại</label>
                                <select class="form-control select2" name="ComplaintId" id="complaintId">
                                    <option value="0">Chưa rõ</option>
                                    <?php if($transaction['ComplaintId'] > 0) echo "<option value='{$transaction['ComplaintId']}' selected='selected'>Khiếu nại #{$transaction['ComplaintId']}</option>"; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Số tiền</label>
                                <div class="input-group">
                                    <input type="text" id="paidCost" class="form-control hmdrequiredCost1 cost1" value="<?php echo ($transaction['ExchangeRate'] == 1) ? priceFormat($transaction['PaidVN']) : priceFormat($transaction['PaidTQ'], true); ?>" data-field="Số tiền">
                                    <div class="input-group-btn" id="divCurrency">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <span id="curentCurrency"><?php echo ($transaction['ExchangeRate'] == 1) ? 'VNĐ' : 'Tệ'; ?></span>
                                            <span class="fa fa-caret-down"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="javascript:void(0)" class="aCurrency" data-id="1">VNĐ</a></li>
                                            <li><a href="javascript:void(0)" class="aCurrency" data-id="2">Tệ</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group divTQ"<?php if($transaction['ExchangeRate'] == 1) echo ' style="display: none;"'; ?>>
                                <label class="control-label">Tỉ giá</label>
                                <input type="text" name="ExchangeRate" id="exchangeRate" class="form-control cost" value="<?php echo priceFormat($transaction['ExchangeRate']); ?>">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group divTQ"<?php if($transaction['ExchangeRate'] == 1) echo ' style="display: none;"'; ?>>
                                <label class="control-label">Số tiền (VNĐ)</label>
                                <input type="text" name="PaidVN" id="paidVN" class="form-control cost" value="<?php echo priceFormat($transaction['PaidVN']); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Ghi chú <span class="normal"> bởi <b><?php echo $crFullName; ?></b> lúc <b><?php echo ddMMyyyy($transaction['CrDateTime'], 'd/m/Y H:i'); ?></b></span></label>
                        <textarea rows="2" name="Comment" id="comment" class="form-control"><?php echo $transaction['Comment']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Xác nhận
                            <?php if(!empty($updateFullName)){ ?>
                                <span class="normal"> bởi <b><?php echo $updateFullName; ?></b> lúc <b><?php echo ddMMyyyy($transaction['UpdateDateTime'], 'd/m/Y H:i'); ?></b></span>
                            <?php } ?>
                        </label>
                        <textarea rows="2" name="Confirm" id="comfirm" class="form-control"><?php echo $transaction['Confirm']; ?></textarea>
                    </div>
                    <div class="form-group text-right">
                        <input type="text" name="TransactionId" id="transactionId" hidden="hidden" value="<?php echo $transactionId; ?>">
                        <input type="text" name="PaidTQ" id="paidTQ" hidden="hidden" value="<?php echo $transaction['PaidTQ']; ?>">
                        <input type="text" name="StatusId" id="statusId" hidden="hidden" value="<?php echo $transaction['StatusId']; ?>">
                        <input type="text" name="TransactionTypeId" id="transactionTypeId" hidden="hidden" value="<?php echo $transaction['TransactionTypeId']; ?>">
                        <input type="text" id="exchangeRateTQ" hidden="hidden" value="<?php echo priceFormat($transaction['ExchangeRate']); ?>">
                        <input type="text" id="currencyId" hidden="hidden" value="<?php echo ($transaction['ExchangeRate'] == 1) ? 1 : 2; ?>">
                        <input type="text" id="getListOrderUrl" hidden="hidden" value="<?php echo base_url('order/getList'); ?>">
                        <input type="text" id="getListComplaintByCustomerUrl" hidden="hidden" value="<?php echo base_url('complaint/getListComplaintByCustomer'); ?>">
                        <input type="text" id="transactionEditUrl" hidden="hidden" value="<?php echo base_url('transaction/edit'); ?>">
                        <?php if($canEdit) { ?>
                            <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                        <?php } elseif($canAsignCustomer) { ?>
                            <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Gắn khách vào phiếu">
                        <?php } ?>
                    </div>
                    <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>