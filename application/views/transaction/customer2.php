<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <?php if($customerId > 0){ ?>
                <section class="content">
                    <div class="box box-default">
                        <?php sectionTitleHtml('Tìm kiếm'); ?>
                        <div class="box-body row-margin">
                            <?php echo form_open('transaction/customer/'.$customerId); ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', $customerId, true, 'Khách hàng', ' select2'); ?>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <input type="text" name="TransactionCode" class="form-control" value="<?php echo set_value('TransactionCode'); ?>" placeholder="ID phiếu">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <ul class="list-inline">
                                        <li><input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm"></li>
                                        <li><button type="button" class="btn btn-primary" id="btnOrder">Danh sách đơn hàng</button></li>
                                    </ul>
                                    <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                    <input type="text" hidden="hidden" id="customerId" value="<?php echo $customerId; ?>">
                                    <input type="text" id="getListOrderUrl" hidden="hidden" value="<?php echo base_url('order/getList02'); ?>">
                                    <input type="text" id="transactionCustomerUrl" hidden="hidden" value="<?php echo base_url('transaction/customer2'); ?>">
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                    <div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-labelledby="modalOrder">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Danh sách đơn hàng</h4>
                                </div>
                                <div class="modal-body box box-success">
                                    <div class="box-header with-border" id="paggingOrder" style="display: none;">
                                        <h3 class="box-title"></h3>
                                        <div class="box-tools pull-right"></div>
                                    </div>
                                    <div class="box-body table-responsive no-padding divTable">
                                        <table class="table table-hover table-bordered">
                                            <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Đơn hàng</th>
                                                <th>Ngày tạo</th>
                                                <th>Trạng thái</th>
                                                <th>Tổng tiền</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tbodyOrder"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <style>
                    #spanBalance{font-weight: bold;color: red;font-size: 22px;}
                </style>
                <section class="content">
                    <div class="box box-success">
                        <?php sectionTitleHtml('Tổng dư nợ <span id="spanBalance">'.$customerBalance.'</span> VNĐ', isset($paggingHtml) ? $paggingHtml : ''); ?>
                        <div class="box-body table-responsive no-padding divTable">
                            <table class="table table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Thời gian</th>
                                    <th>Đơn hàng</th>
                                    <th>Số tiền</th>
                                    <th>Loại</th>
                                    <th>Khoản mục</th>
                                    <th>Trạng thái</th>
                                    <th>Ghi chú</th>
                                    <th>Ghi nợ</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody id="tbodyTransaction">
                                <?php $i = 0;
                                $status = $this->Mconstants->status;
                                $status[4] = 'KT đã duyệt';
                                $labelCss = $this->Mconstants->labelCss;
                                $transactionRanges = $this->Mconstants->transactionRanges;
                                $transactionTypes = $this->Mconstants->transactionTypes;
                                $orderCodes = array();
                                foreach($listTransactions as $t){
                                    $i++; ?>
                                    <tr id="transaction_<?php echo $t['TransactionId']; ?>">
                                        <td><?php echo $i; ?></td>
                                        <td><input type="text" class="form-control" id="paidDateTime_<?php echo $t['TransactionId']; ?>" value="<?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?>"></td>
                                        <td>
                                            <?php if($t['OrderId'] > 0){
                                                if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                echo '<a href="'.base_url("order/edit/".$t['OrderId']).'">'.$orderCodes[$t['OrderId']].'</a><br/>';
                                            } ?>
                                        </td>
                                        <td class="text-center"><?php echo priceFormat($t['PaidVN']).'<br/><a target="_blank" href="'.base_url('transaction/edit/'.$t['TransactionId']).'">#'.$t['TransactionId'].'</a>'; ?></td>
                                        <td id="transactionTypeName_<?php echo $t['TransactionId']; ?>"><?php echo $transactionTypes[$t['TransactionTypeId']] ?></td>
                                        <td><?php echo $transactionRanges[$t['TransactionRangeId']] ?></td>
                                        <td><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                        <td><?php echo $t['Comment']; ?></td>
                                        <td><input type="text" class="form-control balance" id="balance_<?php echo $t['TransactionId']; ?>" value="<?php echo priceFormat($t['Balance']); ?>"></td>
                                        <td class="actions">
                                            <a href="javascript:void(0)" class="link_update" data-id="<?php echo $t['TransactionId']; ?>" title="Cập nhật"><i class="fa fa-save"></i></a>
                                            <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $t['TransactionId']; ?>" title="Thôi"><i class="fa fa-times"></i></a>
                                            <input type="text" hidden="hidden" id="orderId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['OrderId']; ?>">
                                            <input type="text" hidden="hidden" id="transactionTypeId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['TransactionTypeId']; ?>">
                                            <input type="text" hidden="hidden" id="customerId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['CustomerId']; ?>">
                                            <input type="text" hidden="hidden" id="paidVN_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['PaidVN']; ?>">
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('transaction/changeStatus'); ?>">
                        <input type="text" hidden="hidden" id="updateBalanceUrl" value="<?php echo base_url('transaction/updateBalance'); ?>">
                    </div>
                </section>
            <?php } else { ?>
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            <?php } ?>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>