<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <style>.tbodyOrderTransaction tr.thead{font-weight: bold;}</style>
            <section class="content">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Kế toán đã duyệt</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Kế toán chưa duyệt</a></li>
                    </ul>
                    <div class="tab-content">
                        <?php $orderStatus = $this->Mconstants->orderStatus;
                        //$orderStatus[9] = 'KT đã duyệt';
                        $labelCss = $this->Mconstants->labelCss;
                        $roles = $this->Mconstants->roles;
                        $transactionTypes = $this->Mconstants->transactionTypes;
                        $status = $this->Mconstants->status;
                        $status[4] = 'KT đã duyệt';
                        $fullNames = array();
                        $orderCodes = array();
                        $listTransactionIds = array(); ?>
                        <div class="tab-pane active" id="tab_1">
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Ngày tạo đơn</th>
                                        <th>Ngày chốt đơn</th>
                                        <th>Trạng thái</th>
                                        <th>Khách hàng</th>
                                        <th style="width: 30%;">Địa chỉ nhận hàng</th>
                                        <th>Nhân viên xử lý</th>
                                        <th>Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody class="tbodyOrderTransaction">
                                    <?php $i = 0;
                                    foreach($listOrders as $o) {
                                        if ($o['OrderStatusId'] == 9) {
                                            $i++;
                                            if (!isset($fullNames[$o['CustomerId']])) $fullNames[$o['CustomerId']] = $this->Musers->getFieldValue(array('UserId' => $o['CustomerId']), 'FullName');
                                            if ($o['CareStaffId'] > 0 && !isset($fullNames[$o['CareStaffId']])) $fullNames[$o['CareStaffId']] = $this->Musers->getFieldValue(array('UserId' => $o['CareStaffId']), 'FullName');
                                            if ($o['OrderUserId'] > 0 && !isset($fullNames[$o['OrderUserId']])) $fullNames[$o['OrderUserId']] = $this->Musers->getFieldValue(array('UserId' => $o['OrderUserId']), 'FullName');
                                            $j = 0;
                                            $listChildTransactions = array();
                                            foreach ($listTransactions as $t) {
                                                if ($t['OrderId'] == $o['OrderId']) {
                                                    $listChildTransactions[] = $t;
                                                    $listTransactionIds[] = $t['TransactionId'];
                                                }
                                            } ?>
                                            <tr id="order_<?php echo $o['OrderId']; ?>">
                                                <td><?php echo $i; ?></td>
                                                <td id="tdOrderCode_<?php echo $o['OrderId']; ?>"><a href="<?php echo base_url('order/edit/' . $o['OrderId']); ?>"><?php echo $o['OrderCode']; ?></a></td>
                                                <td><?php echo ddMMyyyy($o['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                                <td><?php echo ddMMyyyy($o['CustomerOkDate'], 'd/m/Y H:i'); ?></td>
                                                <td id="tdOrderStatus_<?php echo $o['OrderId']; ?>"><span class="<?php echo $labelCss[$o['OrderStatusId']]; ?>"><?php echo $orderStatus[$o['OrderStatusId']]; ?></span></td>
                                                <td><a href="<?php echo base_url('transaction/customer/' . $o['CustomerId']); ?>"><?php echo isset($fullNames[$o['CustomerId']]) ? $fullNames[$o['CustomerId']] : ''; ?></a></td>
                                                <td>
                                                    <p><?php echo $o['Address'] ?></p>
                                                    <p>SĐT: <?php echo $o['PhoneNumber']; ?></p>
                                                    <p>Email: <?php echo $o['Email']; ?></p>
                                                </td>
                                                <td>
                                                    <?php if (isset($fullNames[$o['CareStaffId']]) && isset($fullNames[$o['OrderUserId']])) {
                                                        echo '<a href="' . base_url('user/edit/' . $o['CareStaffId']) . '" target="_blank">' . $fullNames[$o['CareStaffId']] . '</a><br/>(Nhân Viên CSKH)<br/>';
                                                        echo '<a href="' . base_url('user/edit/' . $o['OrderUserId']) . '" target="_blank">' . $fullNames[$o['OrderUserId']] . '</a><br/>(Nhân viên Đặt hàng)';
                                                    }
                                                    elseif (isset($fullNames[$o['CareStaffId']])) echo '<a href="' . base_url('user/edit/' . $o['CareStaffId']) . '" target="_blank">' . $fullNames[$o['CareStaffId']] . '</a><br/>(Nhân Viên CSKH)';
                                                    elseif (isset($fullNames[$o['OrderUserId']])) echo '<a href="' . base_url('user/edit/' . $o['OrderUserId']) . '" target="_blank">' . $fullNames[$o['OrderUserId']] . '</a><br/>(Nhân viên Đặt hàng)'; ?>
                                                </td>
                                                <td class="actions">
                                                    <button type="button" class="btn btn-primary btn-sm link_order" data-id="<?php echo $o['OrderId']; ?>" data-type="2">Duyệt</button>
                                                    <button type="button" class="btn btn-default btn-sm link_order" data-id="<?php echo $o['OrderId']; ?>" data-type="1">Không Duyệt</button>
                                                    <input type="hidden" id="customerId1_<?php echo $o['OrderId']; ?>" value="<?php echo $o['CustomerId']; ?>">
                                                    <input type="hidden" id="careStaffId_<?php echo $o['OrderId']; ?>" value="<?php echo $o['CareStaffId']; ?>">
                                                </td>
                                            </tr>
                                            <?php if (!empty($listChildTransactions)) { ?>
                                                <tr class="thead">
                                                    <td>STT</td>
                                                    <td>Mã phiếu</td>
                                                    <td>Loại</td>
                                                    <td>Gán vào khách</td>
                                                    <td>Trạng thái</td>
                                                    <td>Số tiền</td>
                                                    <td>Ghi chú</td>
                                                    <td>Thời gian</td>
                                                    <td>Hành động</td>
                                                </tr>
                                                <?php foreach ($listChildTransactions as $t) {
                                                    $j++;
                                                    if (!isset($fullNames[$t['CustomerId']])) $fullNames[$t['CustomerId']] = $this->Musers->getFieldValue(array('UserId' => $t['CustomerId']), 'FullName'); ?>
                                                    <tr id="transaction_<?php echo $t['TransactionId']; ?>">
                                                        <td><?php echo $i . '.' . $j; ?></td>
                                                        <td><a href="<?php echo base_url('transaction/edit/' . $t['TransactionId']); ?>">Mã phiếu #<?php echo $t['TransactionId']; ?></a></td>
                                                        <td><span class="<?php echo $labelCss[$t['TransactionTypeId']]; ?>"><?php echo $transactionTypes[$t['TransactionTypeId']]; ?></span></td>
                                                        <td><a href="<?php echo base_url('transaction/customer/' . $t['CustomerId']); ?>"><?php echo isset($fullNames[$t['CustomerId']]) ? $fullNames[$t['CustomerId']] : ''; ?></a></td>
                                                        <td id="tdTransactionStatus_<?php echo $t['TransactionId']; ?>"><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                                        <td><?php echo priceFormat($t['PaidVN']); ?></td>
                                                        <td><?php echo $t['Comment']; ?></td>
                                                        <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                        <td class="actions">
                                                            <button type="button" class="btn btn-primary btn-sm link_transaction" data-id="<?php echo $t['TransactionId']; ?>" data-type="2">Duyệt</button>
                                                            <button type="button" class="btn btn-default btn-sm link_transaction" data-id="<?php echo $t['TransactionId']; ?>" data-type="1">Không Duyệt</button>
                                                            <input type="hidden" id="customerId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['CustomerId']; ?>">
                                                            <input type="hidden" id="orderId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['OrderId']; ?>">
                                                            <input type="hidden" id="paidVN_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['PaidVN']; ?>">
                                                            <input type="hidden" id="transactionTypeId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['TransactionTypeId']; ?>">
                                                        </td>
                                                    </tr>
                                                <?php }
                                            }
                                        }
                                    }
                                    $listChildTransactions = array();
                                    foreach($listTransactions as $t){
                                        if($t['StatusId'] == 4 && !in_array($t['TransactionId'], $listTransactionIds)) $listChildTransactions[]=$t;
                                    }
                                    if(!empty($listChildTransactions)){ ?>
                                        <tr class="thead">
                                            <td>STT</td>
                                            <td>Mã phiếu</td>
                                            <td>Loại</td>
                                            <td>Gán vào khách</td>
                                            <td>Trạng thái</td>
                                            <td>Số tiền</td>
                                            <td>Ghi chú</td>
                                            <td>Thời gian</td>
                                            <td>Hành động</td>
                                        </tr>
                                        <?php $i++; $j = 0;
                                        foreach($listChildTransactions as $t){
                                            $j++;
                                            if(!isset($fullNames[$t['CustomerId']])) $fullNames[$t['CustomerId']] = $this->Musers->getFieldValue(array('UserId' => $t['CustomerId']), 'FullName'); ?>
                                            <tr id="transaction_<?php echo $t['TransactionId']; ?>">
                                                <td><?php echo $i.'.'.$j; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('transaction/edit/'.$t['TransactionId']); ?>">Mã phiếu #<?php echo $t['TransactionId']; ?></a><br/>
                                                    <a href="<?php echo base_url('order/edit/'.$t['OrderId']); ?>">
                                                        <?php if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                        echo $orderCodes[$t['OrderId']]; ?>
                                                    </a>
                                                </td>
                                                <td><span class="<?php echo $labelCss[$t['TransactionTypeId']]; ?>"><?php echo $transactionTypes[$t['TransactionTypeId']]; ?></span></td>
                                                <td><a href="<?php echo base_url('transaction/customer/'.$t['CustomerId']); ?>"><?php echo isset($fullNames[$t['CustomerId']]) ? $fullNames[$t['CustomerId']] : ''; ?></a></td>
                                                <td id="tdTransactionStatus_<?php echo $t['TransactionId']; ?>"><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                                <td><?php echo priceFormat($t['PaidVN']); ?></td>
                                                <td><?php echo $t['Comment']; ?></td>
                                                <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                <td class="actions">
                                                    <button type="button" class="btn btn-primary btn-sm link_transaction" data-id="<?php echo $t['TransactionId']; ?>" data-type="2">Duyệt</button>
                                                    <button type="button" class="btn btn-default btn-sm link_transaction" data-id="<?php echo $t['TransactionId']; ?>" data-type="1">Không Duyệt</button>
                                                    <input type="hidden" id="customerId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['CustomerId']; ?>">
                                                    <input type="hidden" id="orderId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['OrderId']; ?>">
                                                    <input type="hidden" id="paidVN_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['PaidVN']; ?>">
                                                    <input type="hidden" id="transactionTypeId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['TransactionTypeId']; ?>">
                                                </td>
                                            </tr>
                                        <?php }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Mã đơn hàng</th>
                                        <th>Ngày tạo đơn</th>
                                        <th>Ngày chốt đơn</th>
                                        <th>Trạng thái</th>
                                        <th>Khách hàng</th>
                                        <th style="width: 30%;">Địa chỉ nhận hàng</th>
                                        <th>Nhân viên xử lý</th>
                                        <th>Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody class="tbodyOrderTransaction">
                                    <?php $i = 0;
                                    foreach($listOrders as $o) {
                                        if ($o['OrderStatusId'] == 4) {
                                            $i++;
                                            if (!isset($fullNames[$o['CustomerId']])) $fullNames[$o['CustomerId']] = $this->Musers->getFieldValue(array('UserId' => $o['CustomerId']), 'FullName');
                                            if ($o['CareStaffId'] > 0 && !isset($fullNames[$o['CareStaffId']])) $fullNames[$o['CareStaffId']] = $this->Musers->getFieldValue(array('UserId' => $o['CareStaffId']), 'FullName');
                                            if ($o['OrderUserId'] > 0 && !isset($fullNames[$o['OrderUserId']])) $fullNames[$o['OrderUserId']] = $this->Musers->getFieldValue(array('UserId' => $o['OrderUserId']), 'FullName');
                                            $j = 0;
                                            $listChildTransactions = array();
                                            foreach ($listTransactions as $t) {
                                                if ($t['OrderId'] == $o['OrderId']) {
                                                    $listChildTransactions[] = $t;
                                                    $listTransactionIds[] = $t['TransactionId'];
                                                }
                                            } ?>
                                            <tr id="order_<?php echo $o['OrderId']; ?>">
                                                <td><?php echo $i; ?></td>
                                                <td id="tdOrderCode_<?php echo $o['OrderId']; ?>"><a href="<?php echo base_url('order/edit/' . $o['OrderId']); ?>"><?php echo $o['OrderCode']; ?></a></td>
                                                <td><?php echo ddMMyyyy($o['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                                <td><?php echo ddMMyyyy($o['CustomerOkDate'], 'd/m/Y H:i'); ?></td>
                                                <td id="tdOrderStatus_<?php echo $o['OrderId']; ?>"><span class="<?php echo $labelCss[$o['OrderStatusId']]; ?>"><?php echo $orderStatus[$o['OrderStatusId']]; ?></span></td>
                                                <td><a href="<?php echo base_url('transaction/customer/' . $o['CustomerId']); ?>"><?php echo isset($fullNames[$o['CustomerId']]) ? $fullNames[$o['CustomerId']] : ''; ?></a></td>
                                                <td>
                                                    <p><?php echo $o['Address'] ?></p>
                                                    <p>SĐT: <?php echo $o['PhoneNumber']; ?></p>
                                                    <p>Email: <?php echo $o['Email']; ?></p>
                                                </td>
                                                <td>
                                                    <?php if (isset($fullNames[$o['CareStaffId']]) && isset($fullNames[$o['OrderUserId']])) {
                                                        echo '<a href="' . base_url('user/edit/' . $o['CareStaffId']) . '" target="_blank">' . $fullNames[$o['CareStaffId']] . '</a><br/>(Nhân Viên CSKH)<br/>';
                                                        echo '<a href="' . base_url('user/edit/' . $o['OrderUserId']) . '" target="_blank">' . $fullNames[$o['OrderUserId']] . '</a><br/>(Nhân viên Đặt hàng)';
                                                    }
                                                    elseif (isset($fullNames[$o['CareStaffId']])) echo '<a href="' . base_url('user/edit/' . $o['CareStaffId']) . '" target="_blank">' . $fullNames[$o['CareStaffId']] . '</a><br/>(Nhân Viên CSKH)';
                                                    elseif (isset($fullNames[$o['OrderUserId']])) echo '<a href="' . base_url('user/edit/' . $o['OrderUserId']) . '" target="_blank">' . $fullNames[$o['OrderUserId']] . '</a><br/>(Nhân viên Đặt hàng)'; ?>
                                                </td>
                                                <td class="actions">
                                                    <button type="button" class="btn btn-primary btn-sm link_order" data-id="<?php echo $o['OrderId']; ?>" data-type="2">Duyệt</button>
                                                    <button type="button" class="btn btn-default btn-sm link_order" data-id="<?php echo $o['OrderId']; ?>" data-type="1">Không Duyệt</button>
                                                    <input type="hidden" id="customerId1_<?php echo $o['OrderId']; ?>" value="<?php echo $o['CustomerId']; ?>">
                                                    <input type="hidden" id="careStaffId_<?php echo $o['OrderId']; ?>" value="<?php echo $o['CareStaffId']; ?>">
                                                </td>
                                            </tr>
                                            <?php if (!empty($listChildTransactions)) { ?>
                                                <tr class="thead">
                                                    <td>STT</td>
                                                    <td>Mã phiếu</td>
                                                    <td>Loại</td>
                                                    <td>Gán vào khách</td>
                                                    <td>Trạng thái</td>
                                                    <td>Số tiền</td>
                                                    <td>Ghi chú</td>
                                                    <td>Thời gian</td>
                                                    <td>Hành động</td>
                                                </tr>
                                                <?php foreach ($listChildTransactions as $t) {
                                                    $j++;
                                                    if (!isset($fullNames[$t['CustomerId']])) $fullNames[$t['CustomerId']] = $this->Musers->getFieldValue(array('UserId' => $t['CustomerId']), 'FullName'); ?>
                                                    <tr id="transaction_<?php echo $t['TransactionId']; ?>">
                                                        <td><?php echo $i . '.' . $j; ?></td>
                                                        <td><a href="<?php echo base_url('transaction/edit/' . $t['TransactionId']); ?>">Mã phiếu #<?php echo $t['TransactionId']; ?></a></td>
                                                        <td><span class="<?php echo $labelCss[$t['TransactionTypeId']]; ?>"><?php echo $transactionTypes[$t['TransactionTypeId']]; ?></span></td>
                                                        <td><a href="<?php echo base_url('transaction/customer/' . $t['CustomerId']); ?>"><?php echo isset($fullNames[$t['CustomerId']]) ? $fullNames[$t['CustomerId']] : ''; ?></a></td>
                                                        <td id="tdTransactionStatus_<?php echo $t['TransactionId']; ?>"><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                                        <td><?php echo priceFormat($t['PaidVN']); ?></td>
                                                        <td><?php echo $t['Comment']; ?></td>
                                                        <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                        <td class="actions">
                                                            <button type="button" class="btn btn-primary btn-sm link_transaction" data-id="<?php echo $t['TransactionId']; ?>" data-type="2">Duyệt</button>
                                                            <button type="button" class="btn btn-default btn-sm link_transaction" data-id="<?php echo $t['TransactionId']; ?>" data-type="1">Không Duyệt</button>
                                                            <input type="hidden" id="customerId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['CustomerId']; ?>">
                                                            <input type="hidden" id="orderId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['OrderId']; ?>">
                                                            <input type="hidden" id="paidVN_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['PaidVN']; ?>">
                                                            <input type="hidden" id="transactionTypeId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['TransactionTypeId']; ?>">
                                                        </td>
                                                    </tr>
                                                <?php }
                                            }
                                        }
                                    }
                                    $listChildTransactions = array();
                                    foreach($listTransactions as $t){
                                        if($t['StatusId'] == 1 && !in_array($t['TransactionId'], $listTransactionIds)) $listChildTransactions[]=$t;
                                    }
                                    if(!empty($listChildTransactions)){ ?>
                                        <tr class="thead">
                                            <td>STT</td>
                                            <td>Mã phiếu</td>
                                            <td>Loại</td>
                                            <td>Gán vào khách</td>
                                            <td>Trạng thái</td>
                                            <td>Số tiền</td>
                                            <td>Ghi chú</td>
                                            <td>Thời gian</td>
                                            <td>Hành động</td>
                                        </tr>
                                        <?php $i++; $j = 0;
                                        foreach($listChildTransactions as $t){
                                            $j++;
                                            if(!isset($fullNames[$t['CustomerId']])) $fullNames[$t['CustomerId']] = $this->Musers->getFieldValue(array('UserId' => $t['CustomerId']), 'FullName'); ?>
                                            <tr id="transaction_<?php echo $t['TransactionId']; ?>">
                                                <td><?php echo $i.'.'.$j; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url('transaction/edit/'.$t['TransactionId']); ?>">Mã phiếu #<?php echo $t['TransactionId']; ?></a><br/>
                                                    <a href="<?php echo base_url('order/edit/'.$t['OrderId']); ?>">
                                                        <?php if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                        echo $orderCodes[$t['OrderId']]; ?>
                                                    </a>
                                                </td>
                                                <td><span class="<?php echo $labelCss[$t['TransactionTypeId']]; ?>"><?php echo $transactionTypes[$t['TransactionTypeId']]; ?></span></td>
                                                <td><a href="<?php echo base_url('transaction/customer/'.$t['CustomerId']); ?>"><?php echo isset($fullNames[$t['CustomerId']]) ? $fullNames[$t['CustomerId']] : ''; ?></a></td>
                                                <td id="tdTransactionStatus_<?php echo $t['TransactionId']; ?>"><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                                <td><?php echo priceFormat($t['PaidVN']); ?></td>
                                                <td><?php echo $t['Comment']; ?></td>
                                                <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                <td class="actions">
                                                    <button type="button" class="btn btn-primary btn-sm link_transaction" data-id="<?php echo $t['TransactionId']; ?>" data-type="2">Duyệt</button>
                                                    <button type="button" class="btn btn-default btn-sm link_transaction" data-id="<?php echo $t['TransactionId']; ?>" data-type="1">Không Duyệt</button>
                                                    <input type="hidden" id="customerId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['CustomerId']; ?>">
                                                    <input type="hidden" id="orderId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['OrderId']; ?>">
                                                    <input type="hidden" id="paidVN_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['PaidVN']; ?>">
                                                    <input type="hidden" id="transactionTypeId_<?php echo $t['TransactionId']; ?>" value="<?php echo $t['TransactionTypeId']; ?>">
                                                </td>
                                            </tr>
                                        <?php }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="changeOrderStatusUrl" value="<?php echo base_url('order/changeStatus'); ?>">
                <input type="text" hidden="hidden" id="changeTransactionStatusUrl" value="<?php echo base_url('transaction/changeStatus'); ?>">
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>