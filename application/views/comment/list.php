<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('cms/comment'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId'), true, 'Trạng thái'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('commentStars', 'CommentStarId', set_value('CommentStarId'), true, 'Xếp hạng'); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="IpAddress" class="form-control" value="<?php echo set_value('IpAddress'); ?>" placeholder="Ip Address">
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title . '<a href="'.base_url("cms/comment/add").'" class="btn btn-primary margin-left-10">Thêm mới</a>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Bình luận - Đánh giá</th>
                                <th>Khách hàng</th>
                                <th>Xếp hạng</th>
                                <th>Ip Address</th>
                                <th>Trạng thái</th>
                                <th>Ngày tạo</th>
                                <th style="width: 100px;">Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyComment">
                            <?php $i = 0;
                            $fullNames = array();
                            $status = $this->Mconstants->status;
                            $labelCss = $this->Mconstants->labelCss;
                            foreach($listComments as $c){
                                $i++;
                                $fullName = '';
                                if($c['CrUserId'] > 0) {
                                    if (!isset($fullNames[$c['CrUserId']])) $fullNames[$c['CrUserId']] = $this->Musers->getFieldValue(array('UserId' => $c['CrUserId']), 'FullName');
                                } ?>
                                <tr id="comment_<?php echo $c['CommentId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('cms/comment/edit/'.$c['CommentId']); ?>"><?php echo $c['Comment']; ?></a></td>
                                    <td>
                                        <?php if($c['CrUserId'] > 0) {
                                            if (!isset($fullNames[$c['CrUserId']])) $fullNames[$c['CrUserId']] = $this->Musers->getFieldValue(array('UserId' => $c['CrUserId']), 'FullName');
                                            echo $fullNames[$c['CrUserId']];
                                        }
                                        else echo $c['FullName']; ?>
                                    </td>
                                    <td><?php echo $c['CommentStarId']; ?></td>
                                    <td><?php echo $c['IpAddress']; ?></td>
                                    <td id="statusName_<?php echo $c['CommentId']; ?>"><span class="<?php echo $labelCss[$c['StatusId']]; ?>"><?php echo $status[$c['StatusId']]; ?></span></td>
                                    <td><?php echo ddMMyyyy($c['CrDateTime'], 'd/m/Y H:i:s'); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $c['CommentId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <div class="btn-group" id="btnGroup_<?php echo $c['CommentId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $c['CommentId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $c['CommentId']; ?>" value="<?php echo $c['StatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="deleteComment" value="<?php echo $deleteComment ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="changeStatus" value="<?php echo $changeStatus ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('cms/comment/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>