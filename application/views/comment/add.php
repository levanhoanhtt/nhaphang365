<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php echo form_open('cms/comment/update', array('id' => 'commentForm')); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Khách hàng</label>
                            <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CrUserId', set_value('CrUserId'), true, 'Tự điền', ' select2'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Khách hàng</label>
                            <input type="text" class="form-control" name="FullName" id="fullName" value="<?php echo set_value('FullName'); ?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Xếp hạng</label>
                            <?php $this->Mconstants->selectConstants('commentStars', 'CommentStarId', set_value('CommentStarId')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Trạng thái</label>
                            <?php $this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId')); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Ip Address</label>
                            <input type="text" class="form-control" value="<?php echo $this->input->ip_address(); ?>" disabled>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label class="control-label">User Agent</label>
                            <input type="text" class="form-control" value="<?php echo $this->input->user_agent(); ?>" disabled>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Bình luận - Đánh giá <span class="required">*</span></label>
                    <textarea rows="5" name="Comment" class="form-control hmdrequired" data-field="Bình luận - Đánh giá"><?php echo set_value('Comment'); ?></textarea>
                </div>
                <div class="form-group text-right">
                    <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                    <input type="text" name="CommentId" id="commentId" hidden="hidden" value="0">
                    <input type="text" name="ArticleId" hidden="hidden" value="0">
                    <input type="text" id="commentEditUrl" hidden="hidden" value="<?php echo base_url('cms/comment/edit'); ?>">
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>