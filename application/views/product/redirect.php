<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Chuyển hướng sang web khác...</title>
    <base href="<?php echo base_url();?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php $this->load->view('includes/favicon'); ?>
    <meta name="robots" content="noindex, follow">
</head>
<body>
<p style="text-align: center;">Xin vui lòng chờ trong giây lát...</p>
<input type="text" hidden="hidden" id="productName" value="<?php echo $productName; ?>">
<input type="text" hidden="hidden" id="shopId" value="<?php echo $shopId; ?>">
<script type="text/javascript" src="assets/front/v1/js/jquery.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        var productName = $('input#productName').val();
        if(productName != ''){
            $.ajax({
                type: "POST",
                url: 'https://translate.yandex.net/api/v1.5/tr.json/translate',
                data: {
                    key: 'trnsl.1.1.20170121T075612Z.de658b4e19b99506.e593951de9fe57cc926f39a1c4ebe30ff6b08472',
                    text: productName,
                    lang: 'vi-zh'
                },
                success: function (response) {
                    if(response.code == 200 && response.text.length > 0){
                        var text = encodeURIComponent(response.text[0]);
                        var url = '';
                        var shopId = parseInt($('input#shopId').val());
                        if(shopId == 1) url = 'https://world.taobao.com/search/search.htm?_input_charset=utf-8&q=' + text;
                        else if(shopId == 2) url = 'https://list.tmall.com/search_product.htm?_input_charset=utf-8&q=' + text;
                        else if(shopId == 3) url = 'https://s.1688.com/selloffer/offer_search.htm?_input_charset=utf-8&keywords=' + text;
                        window.location.href = url;
                    }
                    else window.close();
                },
                error: function (response) {
                    window.close();
                }
            });
        }
        else window.close();
    });
</script>
</body>
</html>