<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <ul class="list-inline" id="ulComplaintStatus">
                            <li><a href="<?php echo base_url('complaint'); ?>" class="btn btn-primary">Tất cả (<?php echo $statisticComplaintCounts[0]; ?>)</a></li>
                            <?php $complaintStatus = $this->Mconstants->complaintStatus;
                            foreach($complaintStatus as $i => $v){ ?>
                                <li><a href="<?php echo base_url('complaint/'.$i); ?>" class="btn btn-default"><?php echo $v; ?> (<?php echo $statisticComplaintCounts[$i]; ?>)</a></li>
                            <?php } ?>
                        </ul>
                        <?php echo ($complaintStatusId > 0) ? form_open('complaint/'.$complaintStatusId) : form_open('complaint'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', set_value('CustomerId'), true, 'Khách hàng', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php if($user['RoleId'] == 1) $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'OrderUserId', set_value('OrderUserId'), true, 'Nhân viên Đặt hàng', ' select2');
                                elseif($user['RoleId'] == 5) $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'CareStaffId', set_value('CareStaffId'), true, 'Nhân viên CSKH', ' select2');
                                elseif($user['RoleId'] == 3) $this->Mconstants->selectObject($listUsers, 'UserId', 'FullName', 'UserHandelId', set_value('UserHandelId'), true, 'Nhân viên xử lý', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Đơn hàng</th>
                                <th>Khiếu nại</th>
                                <th>Khách hàng</th>
                                <th>Ngày tạo</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyComplaint">
                            <?php $i = 0;
                            $complaintStatus = $this->Mconstants->complaintStatus;
                            $labelCss = $this->Mconstants->labelCss;
                            $orderCodes = array();
                            foreach($listComplaints as $c){
                                $i++; ?>
                                <tr id="complaint_<?php echo $c['ComplaintId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td>
                                        <a href="<?php echo base_url('order/edit/'.$c['OrderId']); ?>">
                                            <?php if(!isset($orderCodes[$c['OrderId']])) $orderCodes[$c['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $c['OrderId']), 'OrderCode');
                                            echo $orderCodes[$c['OrderId']]; ?>
                                        </a>
                                    </td>
                                    <td><a href="<?php echo base_url('order/edit/'.$c['OrderId'].'/'.$c['ComplaintId']); ?>"><span class="<?php echo $labelCss[$c['ComplaintStatusId']]; ?>">Khiếu nại #<?php echo $c['ComplaintId']; ?>(<?php echo $complaintStatus[$c['ComplaintStatusId']]; ?>)</span></a></td>
                                    <td><a href="<?php echo base_url('transaction/customer/'.$c['CustomerId']); ?>"><?php echo $this->Mconstants->getObjectValue($listCustomers, 'UserId', $c['CustomerId'], 'FullName'); ?></a></td>
                                    <td><?php echo ddMMyyyy($c['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_history" data-id="<?php echo $c['ComplaintId']; ?>" title="Lịch sử khiếu nại"><i class="fa fa-history"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $c['ComplaintId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="deleteComplaint" value="<?php echo $deleteComplaint ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="deleteComplaintUrl" value="<?php echo base_url('complaint/delete'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>