<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php //sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <ul class="list-inline" id="ulOrderStatus">
                            <li><a href="<?php echo base_url('order'); ?>" class="btn btn-primary">Tất cả (<?php echo $statisticOrderCounts[0]; ?>)</a></li>
                            <?php $orderStatus = $this->Mconstants->orderStatus;
                            foreach($orderStatus as $i => $v){ ?>
                                <li><a href="<?php echo base_url('order/'.$i); ?>" class="btn btn-default"><?php echo $v; ?> (<?php echo $statisticOrderCounts[$i]; ?>)</a></li>
                            <?php } ?>
                        </ul>
                        <?php echo ($orderStatusId > 0) ? form_open('order/'.$orderStatusId) : form_open('order'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listCustomers, 'UserId', 'FullName', 'CustomerId', set_value('CustomerId'), true, 'Khách hàng', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $selectName = ''; $attrSelect = ''; $selectClass = '';
                                if($roleId == 1){
                                    $attrSelect = ' disabled="disabled"';
                                    $selectName = 'CareStaffId';
                                }
                                elseif( $roleId == 5 || $roleId == 8){
                                    if( $roleId == 5) $attrSelect = ' disabled="disabled"';
                                    $selectName = 'OrderUserId';
                                }
                                else{
                                    $selectName = 'UserHandelId';
                                    $selectClass = ' select2';
                                }
                                $this->Mconstants->selectObject($listStaffs, 'UserId', 'FullName', $selectName, set_value($selectName), true, 'Nhân viên xử lý', $selectClass, $attrSelect); ?>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="ProductLink" class="form-control" value="<?php echo set_value('ProductLink'); ?>" placeholder="Link sản phẩm">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="OrderCode" class="form-control" value="<?php echo set_value('OrderCode'); ?>" placeholder="Mã đơn hàng">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="Tracking" class="form-control" value="<?php echo set_value('Tracking'); ?>" placeholder="Mã vận đơn">
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="ShopNo" class="form-control" value="<?php echo set_value('ShopNo'); ?>" placeholder="Số thứ tự">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('complaintStatus', 'ComplaintStatusId', set_value('ComplaintStatusId'), true, 'Trạng thái Khiếu nại'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('trackingStatus', 'TrackingStatusId', set_value('TrackingStatusId'), true, 'Trạng thái Mã vận đơn'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã đơn hàng</th>
                                <th>Ngày tạo đơn</th>
                                <th>Ngày chốt đơn</th>
                                <th>Trạng thái</th>
                                <th>Khách hàng</th>
                                <th>Địa chỉ nhận hàng</th>
                                <th>Nhân viên xử lý</th>
                                <?php if($roleId == 3) echo '<th>Hành động</th>'; ?>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php $i = 0;
                            $orderStatus = $this->Mconstants->orderStatus;
                            $labelCss = $this->Mconstants->labelCss;
                            $roles = $this->Mconstants->roles;
                            foreach($listOrders as $o){
                                $customer = false;
                                $careStaff = false;
                                $orderStaff = false;
                                foreach($listCustomers as $c){
                                    if($c['UserId'] == $o['CustomerId']){
                                        $customer = $c;
                                        break;
                                    }
                                }
                                foreach($listStaffs as $s){
                                    if($s['UserId'] == $o['CareStaffId']){
                                        $careStaff = $s;
                                        break;
                                    }
                                }
                                if($o['OrderUserId'] > 0){
                                    foreach($listStaffs as $s){
                                        if($s['UserId'] == $o['OrderUserId']){
                                            $orderStaff = $s;
                                            break;
                                        }
                                    }
                                }
                                if($customer){
                                    $i++; ?>
                                    <tr id="order_<?php echo $o['OrderId']; ?>">
                                        <td><?php echo $i; ?></td>
                                        <td><a href="<?php echo base_url('order/edit/'.$o['OrderId']); ?>"><?php echo $o['OrderCode']; ?></a></td>
                                        <td><?php echo ddMMyyyy($o['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                        <td><?php echo ddMMyyyy($o['CustomerOkDate'], 'd/m/Y H:i'); ?></td>
                                        <td id="statusName_<?php echo $o['OrderId']; ?>"><span class="<?php echo $labelCss[$o['OrderStatusId']]; ?>"><?php echo $orderStatus[$o['OrderStatusId']]; ?></span></td>
                                        <td><a href="<?php echo base_url('transaction/customer/'.$o['CustomerId']); ?>"><?php echo $customer['FullName']; ?></a></td>
                                        <td>
                                            <p><?php echo $o['Address'] ?></p>
                                            <p>SĐT: <?php echo $o['PhoneNumber']; ?></p>
                                            <p>Email: <?php echo $o['Email']; ?></p>
                                        </td>
                                        <td>
                                            <?php if($careStaff && $orderStaff){
                                                echo '<a href="'.base_url('user/edit/'.$o['CareStaffId']).'" target="_blank">' . $careStaff['FullName']. '</a><br/>(Nhân Viên CSKH)<br/>';
                                                echo '<a href="'.base_url('user/edit/'.$o['OrderUserId']).'" target="_blank">' . $orderStaff['FullName']. '</a><br/>(Nhân viên Đặt hàng)';
                                            }
                                            elseif($careStaff) echo '<a href="'.base_url('user/edit/'.$o['CareStaffId']).'" target="_blank">' . $careStaff['FullName']. '</a><br/>(Nhân Viên CSKH)';
                                            elseif($orderStaff) echo '<a href="'.base_url('user/edit/'.$o['OrderUserId']).'" target="_blank">' . $orderStaff['FullName']. '</a><br/>(Nhân viên Đặt hàng)'; ?>
                                        </td>
                                        <?php if($roleId == 3){ ?>
                                        <td class="actions">
                                            <!--<a href="javascript:void(0)" class="link_delete" data-id="<?php //echo $o['OrderId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>-->
                                            <?php if($o['OrderStatusId'] != 6){ ?>
                                            <div class="btn-group" id="btnGroup_<?php echo $o['OrderId']; ?>">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                                <ul class="dropdown-menu">
                                                    <?php foreach($orderStatus as $j => $v){
                                                        if($j == 5 || $j == 6){ ?>
                                                        <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $o['OrderId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                    <?php } } ?>
                                                </ul>
                                            </div>
                                            <input type="text" hidden="hidden" id="statusId_<?php echo $o['OrderId']; ?>" value="<?php echo $o['OrderStatusId']; ?>">
                                            <?php } ?>
                                        </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('order/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>