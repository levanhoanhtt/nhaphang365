<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('order/addOrderUser'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="OrderCode" class="form-control" value="<?php echo set_value('OrderCode'); ?>" placeholder="Mã đơn hàng">
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã đơn hàng</th>
                                <th>Ngày tạo đơn</th>
                                <th>Ngày chốt đơn</th>
                                <th>Trạng thái</th>
                                <th>Khách hàng</th>
                                <th>Nhân viên đặt hàng</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrder">
                            <?php $i = 0;
                            $fullNames = array();
                            foreach($listOrders as $o){
                                $i++;
                                if(!isset($fullNames[$o['CustomerId']])) $fullNames[$o['CustomerId']] = $this->Musers->getFieldValue(array('UserId' => $o['CustomerId']), 'FullName'); ?>
                                <tr id="order_<?php echo $o['OrderId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td id="tdOrderCode_<?php echo $o['OrderId']; ?>"><a href="<?php echo base_url('order/edit/'.$o['OrderId']); ?>"><?php echo $o['OrderCode']; ?></a></td>
                                    <td><?php echo ddMMyyyy($o['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo ddMMyyyy($o['CustomerOkDate'], 'd/m/Y H:i'); ?></td>
                                    <td><span class="label label-default">Đặt hàng</span></td>
                                    <td><a href="<?php echo base_url('transaction/customer/'.$o['CustomerId']); ?>"><?php echo $fullNames[$o['CustomerId']]; ?></a></td>
                                    <td><?php $this->Mconstants->selectObject($listOrderUsers, "UserId", "FullName", 'OrderUserId_'.$o['OrderId'], $o['OrderUserId'], true, "Chọn nhân viên", " select2"); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_update" data-id="<?php echo $o['OrderId']; ?>" title="Cập nhật"><i class="fa fa-save"></i></a>
                                        <input type="text" hidden="hidden" id="careStaffId_<?php echo $o['OrderId']; ?>" value="<?php echo $o['CareStaffId']; ?>">
                                        <input type="text" hidden="hidden" id="customerId_<?php echo $o['OrderId']; ?>" value="<?php echo $o['CustomerId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="updateOrderStaffUrl" value="<?php echo base_url('order/updateStaff'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>