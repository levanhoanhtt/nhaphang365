<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper order-page">
        <?php if($orderId > 0){
        $shopCodes = array(); ?>
        <div class="container-fluid">
            <?php $noProductImage = IMAGE_PATH.NO_PRODUCT; ?>
            <?php $this->load->view('includes/order/meta'); ?>
            <section class="content">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="box box-primary match-height">
                            <?php sectionTitleHtml('Thông tin Khách hàng'); ?>
                            <div class="box-body no-padding">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td class="line30">Mã đơn</td>
                                        <td class="line30" id="tdOrderCode"><?php echo $order['OrderCode']; ?></td>
                                        <td class="line30">Trạng thái</td>
                                        <td>
                                            <ul class="list-inline">
                                                <li><button class="btn btn-primary btn-sm" id="btnCurent" disabled><?php echo $this->Mconstants->orderStatus[$order['OrderStatusId']]; ?></button></li>
                                                <?php if($order['OrderStatusId'] == 4){ ?>
                                                    <li><button class="btn btn-primary btn-sm btnConfirmOk">Duyệt</button></li>
                                                    <li><button class="btn btn-primary btn-sm btnConfirmNotOk">Không duyệt</button></li>
                                                <?php } ?>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="line30">Tên khách</td>
                                        <td class="line30">
                                            <a href="<?php echo base_url('transaction/customer/'.$order['CustomerId']); ?>"><?php echo $customerName; ?></a>
                                            <a href="mailto:<?php echo $order['Email']; ?>" title="<?php echo $order['Email']; ?>"><i class="fa fa-envelope-o"></i></a>
                                            <input type="text" hidden="hidden" id="customerId" value="<?php echo $order['CustomerId']; ?>">
                                            <input type="text" hidden="hidden" id="orderId" value="<?php echo $orderId; ?>">
                                            <input type="text" hidden="hidden" id="fullName" value="<?php echo $customerName; ?>">
                                            <input type="text" hidden="hidden" id="phoneNumber" value="<?php echo $order['PhoneNumber']; ?>">
                                            <input type="text" hidden="hidden" id="email" value="<?php echo $order['Email']; ?>">
                                            <input type="text" hidden="hidden" id="address" value="<?php echo $order['Address']; ?>">
                                            <input type="text" hidden="hidden" id="careStaffId" value="<?php echo $order['CareStaffId']; ?>">
                                            <input type="text" hidden="hidden" id="orderUserId" value="<?php echo ($order['OrderUserId'] > 0) ? $order['OrderUserId'] : 0; ?>">
                                            <input type="text" hidden="hidden" id="orderStatusId" value="<?php echo $order['OrderStatusId']; ?>">
                                            <input type="text" hidden="hidden" id="isFixPercent" value="<?php echo $order['IsFixPercent']; ?>">
                                        </td>
                                        <td class="line30">Địa chỉ nhận hàng</td>
                                        <td>
                                            <?php echo $customerName; ?> - <?php echo $order['PhoneNumber']; ?><br/>
                                            <?php echo $order['Address']; ?> <i class="fa fa-pencil"></i>
                                            <?php if(!empty($order['OrderDate'])){ ?>
                                                <p class="pull-right">Ngày ĐH: <?php echo ddMMyyyy($order['OrderDate']); ?></p>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="box box-primary match-height">
                            <?php sectionTitleHtml('Danh sách Nhân viên xử lý'); ?>
                            <div class="box-body">
                                <ul class="list-group">
                                    <li><a href="<?php echo base_url('user/edit/'.$order['CareStaffId']); ?>" target="_blank"><i class="fa fa-user"></i> <?php echo $careStaffName; ?> (CSKH)</a></li>
                                    <?php if($order['OrderUserId'] > 0){ ?>
                                        <li><a href="<?php echo base_url('user/edit/'.$order['OrderUserId']); ?>" target="_blank"><i class="fa fa-user"></i> <?php echo $orderUserName; ?> (Đặt hàng)</a></li>
                                    <?php } ?>
                                </ul>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label label-horizontal">Lựa chọn Kho về</label>
                                    <div class="col-sm-7"><?php $this->Mconstants->selectObject($listWarehouses, "WarehouseId", "WarehouseName", "WarehouseId", $order['WarehouseId']); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box box-default">
                    <div class="box-body no-padding">
                        <div class="order-container order-container-staff">
                            <ul class="timeline has-img-105">
                                <?php $productStatus = $this->Mconstants->productStatus;
                                $labelCss = $this->Mconstants->labelCss;
                                $complaintStatus = $this->Mconstants->complaintStatus;
                                foreach($listShops as $shopCode => $listProducts){
                                    $shopCodes[] = $shopCode;
                                    $shopStatusId = $listProducts[0]['ShopStatusId']; ?>
                                    <li id="li_<?php echo $shopCode; ?>">
                                        <i class="fa fa-shopping-basket"></i>
                                        <div class="timeline-item">
                                            <div class="timeline-header">
                                                <ul class="list-inline">
                                                    <?php $shopUrl = $listProducts[0]['ShopUrl']; ?>
                                                    <li><span class="text-bold">Mã shop: </span><a href="<?php echo $shopUrl ?>" id="shopUrl_<?php echo $shopCode; ?>" target="_blank"><?php echo $shopCode; ?></a></li>
                                                    <li><span class="text-bold">Tên shop: </span><a href="<?php echo $shopUrl ?>" target="_blank"><span id="shopName_<?php echo $shopCode; ?>"><?php echo $listProducts[0]['ShopName']; ?></span></a></li>
                                                    <li><?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId_'.$shopCode, $listProducts[0]['CategoryId'], true, "Chọn chuyên mục", " select2"); ?></li>
                                                </ul>
                                            </div>
                                            <div class="timeline-body">
                                                <div class="table-responsive no-padding divTable">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 105px;">Ảnh</th>
                                                            <th style="width: 250px;">Link sản phẩm</th>
                                                            <th style="width: 100px;">Màu sắc</th>
                                                            <th style="width: 100px;">Kích thước</th>
                                                            <th style="width: 100px;">Số lượng</th>
                                                            <th style="width: 100px;">Đơn giá(¥)</th>
                                                            <th style="width: 100px;">Giá(¥)</th>
                                                            <th style="width: 100px;">Ship nội địa</th>
                                                            <th>Ghi chú</th>
                                                            <th style="padding-right: 15px;width: 50px;"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="tbodyProduct" id="tbodyProduct_<?php echo $shopCode; ?>">
                                                        <?php $row = $rowImage = 0; $productLink = $productLinkOld = $productImage = '';
                                                        foreach($listProducts as $p){
                                                            $rowImage++;
                                                            $productLink = $p['ProductLink'];
                                                            $productImage = getFileUrl(PRODUCT_PATH, $p['ProductImage'], $noProductImage);
                                                            $productStatusId = $p['ProductStatusId'];
                                                            if($productLink != $productLinkOld) $row++;
                                                            $productLinkOld = $productLink;
                                                            $complaint = false;
                                                            foreach($listComplaints as $c){
                                                                if($c['ProductId'] == $p['ProductId']){
                                                                    $complaint = $c;
                                                                    break;
                                                                }
                                                            } ?>
                                                            <tr data-product="<?php echo $row; ?>" data-shop="<?php echo $shopCode; ?>" data-product-id="<?php echo $p['ProductId']; ?>" data-status="<?php echo $productStatusId; ?>" class="tr_product tr_product_status_<?php echo $productStatusId; ?>">
                                                                <td><img class="productImage" id="productImage_<?php echo $rowImage; ?>_<?php echo $shopCode; ?>" src="<?php echo $productImage; ?>" data-id="<?php echo $rowImage; ?>" data-shop="<?php echo $shopCode; ?>"></td>
                                                                <td class="tdProductName" rowspan="1">
                                                                    <a target="_blank" href="<?php echo $p['ProductLink']; ?>" class="linkproductName"><?php echo $productLink; ?></a>
                                                                    <textarea rows="3" class="form-control productName"></textarea>
                                                                    <?php if($complaint){ ?>
                                                                        <span class="complaint <?php echo $labelCss[$complaint['ComplaintStatusId']]; ?>" data-id="<?php echo $complaint['ComplaintId']; ?>">Khiếu nại (<?php echo $complaintStatus[$complaint['ComplaintStatusId']]; ?>)</span>
                                                                    <?php } ?>
                                                                </td>
                                                                <td class="tdAttr"><input type="text" class="form-control productColor" value="<?php echo $p['Color']; ?>"><i class="fa fa-plus link_add_product"></i></td>
                                                                <td class="tdAttr"><input type="text" class="form-control productSize" value="<?php echo $p['Size']; ?>"><i class="fa fa-plus link_add_product"></i></td>
                                                                <?php if($productStatusId == 1 || $productStatusId == 7 || $productStatusId == 8){ ?>
                                                                    <td class="tdQuantity2">
                                                                        <span class="label1">SL mới</span>
                                                                        <span class="label2">SL cũ</span>
                                                                        <input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="<?php echo $p['Quantity']; ?>">
                                                                        <input type="text" class="form-control cost soluong1" data-field="Số lượng sản phẩm" value="<?php echo $p['QuantityOldNew']; ?>">
                                                                    </td>
                                                                <?php } elseif($productStatusId == 3 || $productStatusId == 4){ ?>
                                                                    <td class="tdQuantity2">
                                                                        <span class="label1">SL cần KH duyệt</span>
                                                                        <span class="label2">SL cũ</span>
                                                                        <input type="text" class="form-control cost soluong1" data-field="Số lượng sản phẩm" value="<?php echo $p['QuantityOldNew']; ?>">
                                                                        <input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="<?php echo $p['Quantity']; ?>">
                                                                    </td>
                                                                <?php } elseif($productStatusId == 5 || $productStatusId == 6){ ?>
                                                                    <td class="tdQuantity2">
                                                                        <span class="label1">SL mới</span>
                                                                        <span class="label2">SL cũ</span>
                                                                        <input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="<?php echo $p['Quantity']; ?>">
                                                                        <input type="text" class="form-control cost soluong1" data-field="Số lượng sản phẩm" value="<?php echo $p['QuantityOldNew']; ?>">
                                                                    </td>
                                                                <?php } else{ ?>
                                                                    <td class="tdQuantity"><input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="<?php echo $p['Quantity']; ?>"></td>
                                                                <?php } ?>
                                                                <?php if($productStatusId == 3 || $productStatusId == 4){ ?>
                                                                    <td class="tdCost2">
                                                                        <span class="label1">Giá cần KH duyệt</span>
                                                                        <span class="label2">Giá cũ</span>
                                                                        <input type="text" class="form-control cost4" disabled value="<?php echo priceFormat($p['CostOldNew'], true); ?>">
                                                                        <input type="text" class="form-control cost1 gia cost4 hmdrequired hmdrequiredCost1" disabled data-field="Giá sản phẩm" value="<?php echo priceFormat($p['Cost'], true); ?>" >
                                                                    </td>
                                                                <?php } elseif($productStatusId == 5 || $productStatusId == 6){ ?>
                                                                    <td class="tdCost2">
                                                                        <span class="label1">Giá đã <?php echo ($productStatusId == 5) ? 'tăng' : 'giảm'; ?></span>
                                                                        <span class="label2">Giá cũ</span>
                                                                        <input type="text" class="form-control cost1 gia cost4 hmdrequired hmdrequiredCost1" disabled data-field="Giá sản phẩm" value="<?php echo priceFormat($p['Cost'], true); ?>" >
                                                                        <input type="text" class="form-control cost4" disabled value="<?php echo priceFormat($p['CostOldNew'], true); ?>">
                                                                    </td>
                                                                <?php }  elseif($productStatusId == 7 || $productStatusId == 8){ ?>
                                                                    <td class="tdCost2">
                                                                        <span class="label1">Giá không được duyệt</span>
                                                                        <span class="label2">Giá cũ</span>
                                                                        <input type="text" class="form-control cost4" disabled value="<?php echo priceFormat($p['CostOldNew'], true); ?>">
                                                                        <input type="text" class="form-control cost1 gia cost4 hmdrequired hmdrequiredCost1" disabled data-field="Giá sản phẩm" value="<?php echo priceFormat($p['Cost'], true); ?>" >
                                                                    </td>
                                                                <?php } else { ?>
                                                                    <td class="tdCost"><input type="text" class="form-control cost1 gia hmdrequired hmdrequiredCost1" data-field="Giá sản phẩm" value="<?php echo priceFormat($p['Cost'], true); ?>" ></td>
                                                                <?php } ?>
                                                                <td><input type="text" class="form-control cost1 thanhtien" value="<?php echo priceFormat($p['Quantity'] * $p['Cost'], true); ?>" disabled></td>
                                                                <?php if($rowImage == 1){
                                                                    $rowspan = count($listProducts);
                                                                    $height = $rowspan * 105;
                                                                    if($shopStatusId == 3 || $shopStatusId == 4){
                                                                        $height = $height / 2; ?>
                                                                        <td class="tdShipTQ3" rowspan="<?php echo $rowspan; ?>">
                                                                            <span class="label1">Giá cần duyệt</span>
                                                                            <span class="label2" style="top: <?php echo $height; ?>px">Giá cũ</span>
                                                                            <input type="text" value="<?php echo priceFormat($p['ShipTQOldNew'], true); ?>" class="form-control cost1 shipTQ1" disabled style="height: <?php echo $height; ?>px;">
                                                                            <input type="text" value="<?php echo priceFormat($p['ShipTQ'], true); ?>" class="form-control cost1 shipTQ" disabled style="height: <?php echo $height; ?>px;">
                                                                        </td>
                                                                    <?php } elseif($shopStatusId == 5 || $shopStatusId == 6){
                                                                        $height = $height / 2; ?>
                                                                        <td class="tdShipTQ3" rowspan="<?php echo $rowspan; ?>">
                                                                            <span class="label1">Giá đã <?php echo ($shopStatusId == 5) ? 'tăng' : 'giảm'; ?></span>
                                                                            <span class="label2" style="top: <?php echo $height; ?>px">Giá cũ</span>
                                                                            <input type="text" value="<?php echo priceFormat($p['ShipTQ'], true); ?>" class="form-control cost1 shipTQ" disabled style="height: <?php echo $height; ?>px;">
                                                                            <input type="text" value="<?php echo priceFormat($p['ShipTQOldNew'], true); ?>" class="form-control cost1 shipTQ1" disabled style="height: <?php echo $height; ?>px;">
                                                                        </td>
                                                                    <?php } elseif($shopStatusId == 7 || $shopStatusId == 8){
                                                                        $height = $height / 2; ?>
                                                                        <td class="tdShipTQ3" rowspan="<?php echo $rowspan; ?>">
                                                                            <span class="label1">Giá mới</span>
                                                                            <span class="label2" style="top: <?php echo $height; ?>px">Giá cũ</span>
                                                                            <input type="text" value="<?php echo priceFormat($p['ShipTQ'], true); ?>" class="form-control cost1 shipTQ" disabled style="height: <?php echo $height; ?>px;">
                                                                            <input type="text" value="<?php echo priceFormat($p['ShipTQOldNew'], true); ?>" class="form-control cost1 shipTQ1" disabled style="height: <?php echo $height; ?>px;">
                                                                        </td>
                                                                    <?php } else{ ?>
                                                                        <td class="tdShipTQ" rowspan="<?php echo $rowspan; ?>"><input type="text" value="<?php echo priceFormat($p['ShipTQ'], true); ?>" class="form-control cost1 shipTQ" style="height: <?php echo $height; ?>px;"></td>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <td>
                                                                    <?php if($canEdit){ ?>
                                                                        <p><b>Khách</b>: <span class="comment cmt" data-title="<?php echo $p['Comment']; ?>"><?php echo substr ($p['Comment'], 0, 20).'...'; ?></span>
                                                                            <i class="fa fa-pencil" title="<?php echo $p['Comment']; ?>"></i>
                                                                        </p>
                                                                        <b>Hệ thống</b>:<textarea rows="2" class="form-control feedback cmt"><?php echo $p['Feedback']; ?></textarea>
                                                                    <?php } else { ?>
                                                                        <p><b>Khách</b>: <span class="comment cmt"><?php echo substr ($p['Comment'], 0, 20).'...'; ?></span>
                                                                            <i class="fa fa-pencil" title="<?php echo $p['Comment']; ?>"></i>
                                                                        </p>
                                                                        <p><b>Hệ thống</b>: <span class="feedback cmt"><?php echo substr ($p['Feedback'], 0, 20).'...'; ?></span>
                                                                            <i class="fa fa-pencil" title="<?php echo $p['Feedback']; ?>"></i>
                                                                        </p>
                                                                    <?php } ?>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                    <p><button class="btn btn-info btn-add-product" data-id="<?php echo $shopCode; ?>">Thêm sản phẩm</button></p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8"></div>
                        <div class="col-sm-4">
                            <div class="pull-right border1" style="margin-bottom: 20px;">
                                <table class="table no-padding">
                                    <tbody>
                                    <tr>
                                        <td class="first"><b>Tỉ giá</b></td>
                                        <td><input type="text" value="<?php echo priceFormat($exchangeRate); ?>" class="form-control cost" id="exchangeRate"></td>
                                    </tr>
                                    <tr>
                                        <td class="first"><b>Tổng giá sản phẩm (Tệ)</b></td>
                                        <td><input type="text" value="0" class="form-control cost1" disabled="disabled" id="costTQ"></td>
                                    </tr>
                                    <tr>
                                        <td class="first">
                                            <input type="checkbox" class="iCheck" id="cbIsFixPercent"<?php if($order['IsFixPercent'] == 1) echo ' checked'; ?>>
                                            <!--<b>Phí dịch vụ</b> (<input type="text" class="form-control cost"<?php //if($order['IsFixPercent'] == 0) echo ' disabled'; ?> id="servicePercent" value="<?php //echo $order['ServicePercent']; ?>"> %)-->
                                            <b>Phí dịch vụ</b> (<select id="servicePercent"<?php if($order['IsFixPercent'] == 0) echo ' disabled'; ?>>
                                                <?php for($i = 0; $i <= 10; $i++){ ?>
                                                    <option value="<?php echo $i; ?>"<?php if($i == $order['ServicePercent']) echo ' selected="selected"'; ?>><?php echo $i; ?></option>
                                                <?php } ?>
                                            </select> %)
                                        </td>
                                        <td><input type="text" value="0" class="form-control cost" disabled="disabled" id="serviceFee"></td>
                                    </tr>
                                    <tr>
                                        <td class="first"><b>Chi phí phát sinh (VNĐ)</b></td>
                                        <td><input type="text" value="<?php echo priceFormat($order['OtherCost']); ?>" class="form-control cost" id="costFee"></td>
                                    </tr>
                                    <tr>
                                        <td class="first"><b>Tổng giá thanh toán (VNĐ)</b></td>
                                        <td><input type="text" value="0" class="form-control cost" disabled="disabled" id="sumCost"></td>
                                    </tr>
                                    </tbody>
                                </table>
                                <?php if($order['OrderStatusId'] == 4){ ?>
                                    <ul class="list-inline text-right">
                                        <li><button class="btn btn-primary btn-sm btnConfirmOk">Duyệt</button></li>
                                        <li><button class="btn btn-primary btn-sm btnConfirmNotOk">Không duyệt</button></li>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--Begin Modal-->
                <?php $this->load->view('includes/order/modal_complaint'); ?>
                <?php $this->load->view('includes/order/modal'); ?>
                <!--End Modal-->
                <input type="text" hidden="hidden" id="changeStatusOrderUrl" value="<?php echo base_url('order/changeStatus'); ?>">
                <input type="text" hidden="hidden" id="changeCategoryShopUrl" value="<?php echo base_url('shop/changeCategory'); ?>">
                <input type="text" hidden="hidden" id="getProductDetailUrl" value="<?php echo base_url('product/getProductDetail'); ?>">
                <input type="text" hidden="hidden" id="getComplaintDetailUrl" value="<?php echo base_url('complaint/getDetail'); ?>">
                <input type="text" hidden="hidden" id="updateComplaintUrl" value="<?php echo base_url('complaint/update'); ?>">
                <input type="text" hidden="hidden" id="complaintId" value="<?php echo $complaintId; ?>">
                <input type="text" hidden="hidden" id="imagePath" value="<?php echo PRODUCT_PATH; ?>">
                <input type="text" hidden="hidden" id="shipTQType" value="2">
            </section>
        </div>
        <div class="box-order-shop" style="display: none;">
                <ul class="list-group">
                    <?php foreach($shopCodes as $i => $v){ ?>
                        <li title="<?php echo ($v == 'NoShopHMD') ? '' : $v; ?>" data-id="<?php echo $v; ?>"><?php echo $i+1; ?></li>
                    <?php } ?>
                </ul>
            </div>
        <?php } else { ?>
            <div class="container-fluid">
                <section class="content"><?php $this->load->view('includes/notice'); ?></section>
            </div>
        <?php } ?>
    </div>
<?php $this->load->view('includes/footer'); ?>