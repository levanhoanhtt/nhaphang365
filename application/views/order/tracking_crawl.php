<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('trackingcrawl'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="Tracking" class="form-control" value="<?php echo set_value('Tracking'); ?>" placeholder="Mã vận đơn">
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Mã vận đơn</th>
                                <th>Số kiện</th>
                                <th>Loại hàng</th>
                                <th>Phí</th>
                                <th>Ngày TQ</th>
                                <th>Ngày HN</th>
                                <th>Trạng thái</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            foreach($listTrackingCrawls as $tc){
                                $i++; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $tc['Tracking']; ?></td>
                                    <td><?php echo $tc['PackageCount']; ?></td>
                                    <td><?php echo $tc['ProductTypeName']; ?></td>
                                    <td><?php echo $tc['TrackingCost']; ?></td>
                                    <td><?php echo ddMMyyyy($tc['DateTQ'], 'd/m/Y'); ?></td>
                                    <td>
                                        <?php $dateVN = ddMMyyyy($tc['DateVN'], 'd/m/Y');
                                        if($dateVN != '30/11/-0001') echo $dateVN; ?>
                                    </td>
                                    <td><?php echo $tc['StatusName']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>