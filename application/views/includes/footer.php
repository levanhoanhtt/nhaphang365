<?php $siteName = 'Nhập hàng 365';
$phoneCare = '0123456789';
$email = 'nhaphang365@gmail.com';
$facebook = 'https://www.facebook.com/nhaphang365/';
$configs = $this->session->userdata('configs');
if($configs){
    if(isset($configs['SITE_NAME'])) $siteName = $configs['SITE_NAME'];
    if(isset($configs['PHONE_CARE'])) $phoneCare = $configs['PHONE_CARE'];
    if(isset($configs['EMAIL_COMPANY'])) $email = $configs['EMAIL_COMPANY'];
    if(isset($configs['FACEBOOK'])) $facebook = $configs['FACEBOOK'];
} ?>
<footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2017 <a href="#"></a>.</strong> All rights reserved.
            - <strong>CSKH</strong>: <span id="spanSysPhone"><?php echo $phoneCare; ?></span> . <strong>Email: <a id="aSysEmail" href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></strong>.
        <strong>Facebook</strong>: <a href="<?php echo $facebook; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
</footer>

</div>
<input type="text" hidden="hidden" id="roorPath" value="<?php echo ROOT_PATH; ?>">
<input type="text" hidden="hidden" id="siteName" value="<?php echo $siteName; ?>">
<input type="text" hidden="hidden" id="userImagePath" value="<?php echo USER_PATH; ?>">
<input type="text" hidden="hidden" id="chatServerUrl" value="<?php echo CHAT_SERVER; ?>">
<input type="text" hidden="hidden" id="getListNotificationHeaderUrl" value="<?php echo base_url('notification/getListForHeader'); ?>">
<input type="text" hidden="hidden" id="changeStatusNotificationUrl" value="<?php echo base_url('notification/changeStatus'); ?>">
<?php $user = $this->session->userdata('user');
if($user){ ?>
    <input type="text" hidden="hidden" id="userLoginId" value="<?php echo $user['UserId']; ?>">
    <input type="text" hidden="hidden" id="fullNameLoginId" value="<?php echo $user['FullName'] ?>">
    <input type="text" hidden="hidden" id="avatarLoginId" value="<?php echo empty($user['Avatar']) ? NO_IMAGE : $user['Avatar']; ?>">
    <input type="text" hidden="hidden" id="roleLoginId" value="<?php echo $user['RoleId']; ?>">
<?php } else { ?>
    <input type="text" hidden="hidden" id="userLoginId" value="0">
    <input type="text" hidden="hidden" id="fullNameLoginId" value="">
    <input type="text" hidden="hidden" id="avatarLoginId" value="<?php echo NO_IMAGE; ?>">
    <input type="text" hidden="hidden" id="roleLoginId" value="0">
<?php } ?>
<noscript><meta http-equiv="refresh" content="0; url=<?php echo base_url('user/permission'); ?>" /></noscript>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/plugins/pace/pace.min.js"></script>
<script src="assets/vendor/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/plugins/fastclick/fastclick.js"></script>
<script src="assets/vendor/dist/js/app.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.js"></script>
<script src="assets/vendor/plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="assets/vendor/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="assets/js/common.js"></script>
<?php if(isset($scriptFooter)) outputScript($scriptFooter); ?>
<!--Chat-->
<script src="assets/vendor/plugins/socket.io/socket.io.js"></script>
<script src="assets/vendor/plugins/jquery.playSound.js"></script>
<?php if($user){
    if($user['RoleId'] == 4){ ?>
        <script type="text/javascript" src="assets/js/chat_by_customer.js"></script>
        <?php $this->load->view('site/ga'); ?>
    <?php } elseif($user['RoleId'] == 3 || $user['RoleId'] == 7){ ?>
        <script type="text/javascript" src="assets/js/chat_by_admin.js"></script>
    <?php } else{ ?>
        <script type="text/javascript" src="assets/js/chat_by_user.js"></script>
    <?php } ?>
<?php } else { ?>
    <script type="text/javascript" src="assets/js/chat_by_customer.js?20170715"></script>
    <?php $this->load->view('site/ga'); ?>
<?php } ?>
</body>
</html>