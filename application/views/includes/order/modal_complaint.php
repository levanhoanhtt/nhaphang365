<div class="modal fade" id="modalUpdateComplaint" tabindex="-1" role="dialog" aria-labelledby="modalUpdateComplaint">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Chi tiết Khiếu nại</h4>
            </div>
            <div class="modal-body">
                <p>Người tạo: <i>Khách hàng</i>: <b id="customerNameComplaint"></b> - Đã gửi: <b id="crDateTimeComplaint"></b></p>
                <div class="table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Link sản phẩm</th>
                            <th>Màu săc</th>
                            <th>Kích thước</th>
                            <th>Số lượng</th>
                            <th>Giá (tệ)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><pre class="pre"><a id="productLinkComplaint2" href="javascript:void(0)" target="_blank"></a></pre></td>
                            <td id="productColorComplaint2"></td>
                            <td id="productSizeComplaint2"></td>
                            <td id="productQuantityComplaint2"></td>
                            <td id="productCostComplaint2"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label>Nội dung khiếu nại</label>
                            <?php $complaintTypes = $this->Mconstants->complaintTypes;
                            foreach($complaintTypes as $i => $v){ ?>
                                <p>
                                    <input type="checkbox" class="iCheck cbComplaintType" disabled value="<?php echo $i; ?>">
                                    <?php echo $v; ?>
                                </p>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label style="padding-right: 5px;">Ảnh sản phẩm</label><!--<button type="button" class="btn btn-default btn-flat" id="btnUploadImage1Complaint">Upload</button>-->
                        <ul id="ulProduct1Complaint" class="ulProductComplaint"></ul>
                    </div>
                    <div class="col-sm-4">
                        <label style="padding-right: 5px;">Ảnh mã vận đơn</label><!--<button type="button" class="btn btn-default btn-flat" id="btnUploadImage2Complaint">Upload</button>-->
                        <ul id="ulProduct2Complaint" class="ulProductComplaint"></ul>
                    </div>
                </div>
                <div class="form-group">
                    <label>Chi tiết khiếu nại: </label><span id="spanCrDateTimeComplaint" class="padding15"></span>
                    <textarea class="form-control" rows="2" id="commentComplaint" disabled></textarea>
                </div>
                <h4 class="text-bold">Phản hồi từ Đặt Hàng 86</h4>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <br/>
                            <?php $this->Mconstants->selectConstants('complaintOwners', 'ComplaintOwnerId'); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group" id="divComplaintSolution">
                            <label>Phương án xử lý</label>
                            <ul class="list-inline">
                                <?php $complaintSolutions = $this->Mconstants->complaintSolutions;
                                foreach($complaintSolutions as $i => $v){ ?>
                                    <li>
                                        <input type="checkbox" class="iCheck cbComplaintSolution" value="<?php echo $i; ?>">
                                        <?php echo $v; ?>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row" id="divComplaintSolutionMoney">
                    <div class="col-sm-6"></div>
                    <div class="col-sm-6">
                        <div class="form-group" id="complaintSolution1" style="display: none;margin-bottom: 5px;">
                            <label class="col-sm-5 control-label">Số tệ đền hàng: </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control cost2" id="exchangeCostTQ" value="0">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group" id="complaintSolution4" style="display: none;">
                            <label class="col-sm-5 control-label">Số tệ giảm trừ: </label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control cost2" id="reduceCostTQ" value="0">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div id="listComplaintMsgs"></div>
                <div class="form-group" id="complaintCurentMsg">
                    <label id="labelComplaintCurentMsg">Chăm sóc trả lời:</label>
                    <div class="complaintAgree" style="display: none;">
                        <label>
                            <input type="radio" name="cbComplaintAgree" class="iCheck cbComplaintAgree" value="1"> Duyệt
                        </label>
                        <label>
                            <input type="radio" name="cbComplaintAgree" class="iCheck cbComplaintAgree" value="0" checked> Không Duyệt
                        </label>
                    </div>
                    <textarea class="form-control" rows="2" id="curentMsgComplaint"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                <button type="button" class="btn btn-primary" id="btnUpdateComplaint">Gửi quản lý duyệt</button>
                <input type="text" hidden="hidden" id="productIdComplaint" value="0">
                <input type="text" hidden="hidden" id="complaintStatusId" value="0">
                <input type="text" hidden="hidden" id="complaintMsgTypeId" value="0">
            </div>
        </div>
    </div>
</div>