<input type="text" hidden="hidden" id="shopCodeCurent" value="">
<input type="text" hidden="hidden" id="getShopNumberUrl" value="<?php echo base_url('shop/getShopNumber'); ?>">
<input type="text" hidden="hidden" id="insertShopNumberUrl" value="<?php echo base_url('shop/insertShopNumber'); ?>">
<input type="text" hidden="hidden" id="updateShopNumberUrl" value="<?php echo base_url('shop/updateShopNumber'); ?>">
<input type="text" hidden="hidden" id="deleteShopNumberUrl" value="<?php echo base_url('shop/deleteShopNumber'); ?>">
<input type="text" hidden="hidden" id="deleteShopTrackingUrl" value="<?php echo base_url('shop/deleteShopTracking'); ?>">
<input type="text" hidden="hidden" id="changeIsTransportSlowUrl" value="<?php echo base_url('shop/changeIsTransportSlow'); ?>">
<?php foreach($listOrderAccounts as $oa){ ?>
    <input type="text" hidden="hidden" class="orderAccount" data-id="<?php echo $oa['OrderAccountId']; ?>" value="<?php echo $oa['OrderAccountName']; ?>">
<?php } ?>
<div class="modal fade" id="modalShopNumberInfo" tabindex="-1" role="dialog" aria-labelledby="modalShopNumberInfo">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Chi tiết thanh toán Shop</h4>
            </div>
            <div class="modal-body box box-success">
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Tên shop</th>
                            <th>Số thứ tự</th>
                            <th>Tổng thực</th>
                            <th>Tổng</th>
                            <th>Đòi tiền</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyShopNumberInfo"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalShopTracking" tabindex="-1" role="dialog" aria-labelledby="modalShopTracking">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cập nhật Mã vận đơn</h4>
            </div>
            <div class="modal-body box box-success">
                <div class="box-body table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Mã vận đơn</th>
                            <td>Đã về</td>
                            <td>Ghi chú</td>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody id="tbodyShopTracking">
                        <tr>
                            <?php echo form_open('shop/updateShopTracking', array('id' => 'shopTrackingForm')); ?>
                            <td><input type="text" class="form-control hmdrequired" id="tracking" value="" data-field="Mã vận đơn"></td>
                            <td><input type="checkbox" class="cbShop" id="trackingReturn"></td>
                            <td><input type="text" class="form-control" id="trackingComment" value=""></td>
                            <td class="actions">
                                <a href="javascript:void(0)" id="link_update_shopTracking" title="Cập nhật"><i class="fa fa-save"></i></a>
                                <a href="javascript:void(0)" id="link_cancel_shopTracking" title="Thôi"><i class="fa fa-times"></i></a>
                                <input type="text" id="shopTrackingId" value="0" hidden="hidden">
                            </td>
                            <?php echo form_close(); ?>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>