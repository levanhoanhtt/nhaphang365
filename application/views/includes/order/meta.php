<section class="content-header">
    <h1><?php echo $title; ?></h1>
    <ol class="breadcrumb">
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" aria-expanded="false">
                <i class="fa fa-wrench"></i> Thao tác <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">Tìm link</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">Chốt đơn</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)" id="aMergeOreder">Gộp đơn</a></li>
            </ul>
        </li>
        <li><a href="javascript:void(0)" title="<?php echo $order['Comment']; ?>" id="aOrderComment"><i class="fa fa-pencil"></i> Ghi chú đơn hàng</a></li>
    </ol>
</section>