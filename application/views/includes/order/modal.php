<?php $noProductImage = IMAGE_PATH . NO_PRODUCT; ?>
<div class="modal fade" id="modalProductImage" tabindex="-1" role="dialog" aria-labelledby="modalProductImage">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ảnh sản phẩm</h4>
            </div>
            <div class="modal-body">
                <?php if($canEdit){ ?>
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="productImageUrl" placeholder="Link ảnh">
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-info btn-flat" id="btnUploadImage">Upload</button>
                        </span>
                    </div>
                </div>
                <?php } ?>
                <img src="<?php echo $noProductImage;; ?>" id="imgProduct" style="max-height: 500px;max-width: 100%;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                <?php if($canEdit){ ?><button type="button" class="btn btn-primary" id="btnUpdateImage">Cập nhật</button><?php } ?>
                <input type="text" hidden="hidden" id="imageRow" value="0">
                <input type="text" hidden="hidden" id="imageShop" value="">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalImportExcel" tabindex="-1" role="dialog" aria-labelledby="modalImportExcel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Import đơn hàng từ Excel</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" id="fileExcelUrl" placeholder="File Excel" disabled>
                        <span class="input-group-btn">
                            <button type="button" class="btn btn-info btn-flat" id="btnUploadExcel">Upload</button>
                        </span>
                    </div>
                </div>
                <img src="assets/vendor/dist/img/loading.gif" class="imgLoading imgCenter" style="display: none;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                <button type="button" class="btn btn-primary" id="btnImportExcel">Import</button>
                <input type="text" hidden="hidden" id="importOrderUrl" value="<?php echo base_url('order/import'); ?>">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalMergeOrder" tabindex="-1" role="dialog" aria-labelledby="modalMergeOrder">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Gộp đơn hàng</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Chọn đơn gộp cùng: </label>
                    <div class="col-sm-7">
                        <select class="form-control" id="mergeOrderId">
                            <option value="0">Vui lòng chọn</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                <button type="button" class="btn btn-primary" id="btnMergeOrder">Gộp đơn</button>
                <input type="text" id="getListOrderUrl" hidden="hidden" value="<?php echo base_url('order/getList'); ?>">
                <input type="text" hidden="hidden" id="mergeOrderUrl" value="<?php echo base_url('order/merge'); ?>">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalMergeOrderDraff" tabindex="-1" role="dialog" aria-labelledby="modalMergeOrderDraff">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Gộp vào đơn hàng đã có</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-5 control-label">Chọn đơn gộp cùng: </label>
                    <div class="col-sm-7">
                        <select class="form-control" id="mergeOrderId1">
                            <option value="0">Vui lòng chọn</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                <button type="button" class="btn btn-primary" id="btnMergeOrder1">Gộp đơn</button>
                <input type="text" hidden="hidden" id="mergeOrderDraffUrl" value="<?php echo base_url('order/mergeDraff'); ?>">
            </div>
        </div>
    </div>
</div>
<input type="text" hidden="hidden" id="canEdit" value="<?php echo $canEdit ? 1 : 0; ?>">
<input type="text" hidden="hidden" id="productIdScroll" value="<?php echo $productIdScroll; ?>">
<input type="text" hidden="hidden" id="shopCodeScroll" value="<?php echo $shopCodeScroll; ?>">
<input type="text" hidden="hidden" id="noProductImage" value="<?php echo $noProductImage; ?>">
<input type="text" hidden="hidden" id="updateOrderUrl" value="<?php echo base_url('order/update'); ?>">
<?php foreach($listServiceFees as $sf){ ?>
    <input type="text" hidden="hidden" class="serviceFee" data-min="<?php echo $sf['BeginCost'] * 1000000; ?>" data-max="<?php echo $sf['EndCost'] * 1000000; ?>" value="<?php echo $sf['Percent']/100; ?>">
<?php } ?>