<style>
    .table-bordered1{border-top: 1px solid #827777;}
    .table-bordered1 thead tr>th{border: 1px solid #827777;border-bottom: none;}
</style>
<div class="modal fade" id="modalDeleteProduct" tabindex="-1" role="dialog" aria-labelledby="modalDeleteProduct">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tăng/ giảm giá sản phẩm</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Mã shop</th>
                            <th>Link sản phẩm</th>
                            <th>Màu săc</th>
                            <th>Kích thước</th>
                            <th>Ship TQ</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="productShopDelete"></td>
                            <td><pre class="pre"><a id="productLinkDelete" href="javascript:void(0)" target="_blank"></a></pre></td>
                            <td id="productColorDelete"></td>
                            <td id="productSizeDelete"></td>
                            <td id="productShipTQDelete"></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered1">
                        <thead>
                        <tr>
                            <th>Số lượng cũ</th>
                            <th>Đơn giá cũ (tệ)</th>
                            <th>Số lượng mới</th>
                            <th>Đơn giá mới (tệ)</th>
                            <th>Chênh lệch (tệ)</th>
                            <th>Chênh lệch (VNĐ)</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyProductDelete">
                        <tr>
                            <td><input type="text" class="form-control" id="productQuantityDelete" value="0" disabled></td>
                            <td><input type="text" class="form-control" id="productCostDelete" value="0" disabled></td>
                            <td><input type="text" class="form-control cost3" id="newProductQuantity" value="0"></td>
                            <td><input type="text" class="form-control cost3" id="newProductCost" value="0"></td>
                            <td><input type="text" class="form-control cost3" id="differenceCostTQ" value="0" disabled></td>
                            <td><input type="text" class="form-control cost3" id="differenceCostVN" value="0" disabled></td>
                        </tr>
                        </tbody>
                    </table>
                    <div class="form-group">
                        <textarea class="form-control" rows="2" id="removeReason" placeholder="Lí do"></textarea>
                    </div>
                    <div class="alert alert-warning text-center hmd-alert">
                        <strong>Chênh lệch (VNĐ) = Chênh lệch (tệ) * Tỉ giá + Phí dịch vụ</strong>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-primary" id="btnChangeProduct">Thay đổi</button>
                    <input type="text" hidden="hidden" id="productRow" value="0">
                    <input type="text" hidden="hidden" id="productId" value="0">
                    <input type="text" hidden="hidden" id="productShop" value="">
                    <input type="text" hidden="hidden" id="productStatusId" value="0">
                    <input type="text" hidden="hidden" id="imageDelete" value="">
                    <input type="text" hidden="hidden" id="commentDelete" value="">
                    <input type="text" hidden="hidden" id="feedbackDelete" value="">
                    <input type="text" hidden="hidden" id="changeStatusProductUrl" value="<?php echo base_url('product/changeStatus'); ?>">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalChangeShipTQ" tabindex="-1" role="dialog" aria-labelledby="modalChangeShipTQ">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tăng/ giảm phí Ship TQ</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive no-padding divTable">
                    <table class="table table-hover table-bordered1">
                        <thead>
                        <tr>
                            <th>Mã shop</th>
                            <th>Tên shop</th>
                            <th>Số lượng sản phẩm</th>
                            <th>Ship TQ cũ (tệ)</th>
                            <th>Ship TQ mới (tệ)</th>
                            <th>Chênh lệch (tệ)</th>
                            <th>Chênh lệch (VNĐ)</th>
                        </tr>
                        </thead>
                        <tbody id="tbodyChangeShipTQ">
                        <tr>
                            <td><input type="text" class="form-control" id="shipTQShopCode" value="" disabled></td>
                            <td><input type="text" class="form-control" id="shipTQShopName" value="" disabled></td>
                            <td><input type="text" class="form-control" id="shipTQProductCount" value="0" disabled></td>
                            <td><input type="text" class="form-control" id="shipTQOld" value="0" disabled></td>
                            <td><input type="text" class="form-control" id="shipTQNew" value="0"></td>
                            <td><input type="text" class="form-control" id="differenceShipTQ" value="0" disabled></td>
                            <td><input type="text" class="form-control" id="differenceShipVN" value="0" disabled></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="2" id="shipTQReason" placeholder="Lí do"></textarea>
                </div>
                <div class="alert alert-warning text-center hmd-alert">
                    <strong>Chênh lệch (VNĐ) = Chênh lệch (tệ) * Tỉ giá + Phí dịch vụ</strong>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                    <button type="button" class="btn btn-primary" id="btnChangeShipTQ">Thay đổi</button>
                    <input type="text" hidden="hidden" id="shopStatusId" value="0">
                    <input type="text" hidden="hidden" id="changeStatusShopUrl" value="<?php echo base_url('shop/changeStatus'); ?>">
                </div>
            </div>
        </div>
    </div>
</div>