<div class="modal fade" id="modalRevenue" tabindex="-1" role="dialog" aria-labelledby="modalRevenue">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tăng giảm doanh thu</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Khoản mục</label>
                            <?php $this->Mconstants->selectConstants('transactionRanges', 'TransactionRangeId', 3); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Số tiền</label>
                            <div class="input-group">
                                <input type="text" id="paidCostRevenue" class="form-control cost5" value="0">
                                <div class="input-group-btn" id="divCurrency">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <span id="curentCurrency">VNĐ</span>
                                        <span class="fa fa-caret-down"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)" class="aCurrency" data-id="1">VNĐ</a></li>
                                        <li><a href="javascript:void(0)" class="aCurrency" data-id="2">Tệ</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group divTQ" style="display: none;">
                            <label class="control-label">Số tiền (VNĐ)</label>
                            <input type="text" id="paidVNRevenue" class="form-control" value="0">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Chi nhánh</label>
                            <?php $this->Mconstants->selectObject($listWarehouses, "WarehouseId", "WarehouseName", "WarehouseId1", $order['WarehouseId']); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Nguồn tiền</label>
                            <?php $this->Mconstants->selectObject($listMoneySources, "MoneySourceId", "MoneySourceName", "MoneySourceId"); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group divTQ" style="display: none;">
                            <label class="control-label">Tỉ giá</label>
                            <input type="text" id="exchangeRateRevenue" class="form-control" value="1">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Ghi chú</label>
                    <textarea rows="2" id="commentRevenue" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                <button type="button" class="btn btn-primary" id="btnAddRevenue">Cập nhật</button>
                <input type="text" id="currencyId" hidden="hidden" value="1">
                <input type="text" id="paidTQRevenue" hidden="hidden" value="0">
                <input type="text" id="updateTransactionUrl" hidden="hidden" value="<?php echo base_url('transaction/update'); ?>">
            </div>
        </div>
    </div>
</div>