<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url();?>"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!--<meta name="google-site-verification" content="nHSNu27ibibj0Qd5lERaNlSAN-hcV13v_J-sqg0syOE" />-->
    <?php $this->load->view('includes/favicon'); ?>
    <meta name="robots" content="noindex, follow">
    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/vendor/plugins/pnotify/pnotify.custom.css" />
    <link rel="stylesheet" href="assets/vendor/plugins/select2/select2.min.css" />
    <link rel="stylesheet" href="assets/vendor/plugins/iCheck/square/blue.css">
    <?php if(isset($scriptHeader)) outputScript($scriptHeader); ?>
    <link rel="stylesheet" href="assets/vendor/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/vendor/dist/css/style.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">
    <header class="main-header">
        <nav class="navbar navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <form action="<?php echo base_url('product/redirect'); ?>" id="productSearchForm" method="get" target="_blank" accept-charset="utf-8">
                        <select class="form-control" name="ShopId">
                            <option value="1">Taobao</option>
                            <option value="2">Tmall</option>
                            <option value="3">1688</option>
                        </select>
                        <div class="input-group">
                            <input type="text" class="form-control" value="" name="ProductName" placeholder="Nhập tên sản phẩm tiếng Việt">
                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="navbar-custom-menu">
                    <input type="text" hidden="hidden" id="getBalanceUrl" value="<?php echo base_url('customer/getBalance'); ?>">
                    <ul class="nav navbar-nav">
                        <?php if($isCustomerPanel){ ?>
                            <li><a href="javascript:void(0)"><span class="spanBalance">0</span> VNĐ</a></li>
                            <li><a href="javascript:void(0)" id="aBalance">Nạp tiền</a></li>
                            <li class="dropdown user user-menu">
                                <a href="<?php echo base_url('customer/order'); ?>">
                                    <?php $avatar = USER_PATH.((!empty($user['Avatar'])) ? $user['Avatar'] : NO_IMAGE); ?>
                                    <img src="<?php echo $avatar; ?>" alt="<?php echo $user['FullName']; ?>" class="user-image">
                                    <span class="hidden-xs"><?php echo $user['FullName']; ?></span>
                                </a>
                            </li>
                            <li class="dropdown messages-menu">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-danger countNotification">0</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><span class="countNotification">0</span> Thông báo</li>
                                    <li>
                                        <ul class="menu" id="ulNotificationHeader"></ul>
                                    </li>
                                    <li class="footer"><a href="<?php echo base_url('customer/notification'); ?>" id="aViewAllNotification">Xem tất cả thông báo</a></li>
                                </ul>
                            </li>
                            <li class="dropdown messages-menu">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-envelope-o"></i>
                                    <span class="label label-success countMsgChat">0</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header"><span class="countMsgChat">0</span> Tin nhắn từ CSKH</li>
                                    <li>
                                        <input type="text" hidden="hidden" id="getListChatUnReadUrl" value="<?php echo base_url('chat/getListChatUnRead'); ?>">
                                        <ul class="menu" id="ulChatHeader"></ul>
                                    </li>
                                    <li class="footer"><a href="<?php echo base_url('customer/chat'); ?>" id="aViewAllChat">Xem tất cả Tin nhắn</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                            <li><a href="<?php echo base_url('tao-don-hang'); ?>" title="Tạo đơn hàng"><i class="fa fa-shopping-cart"></i></a></li>
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-caret-down"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url('customer/profile'); ?>"><i class="fa fa-user"></i> Thông tin tài khoản</a></li>
                                    <li><a href="<?php echo base_url('customer/notification'); ?>"><i class="fa fa-bell"></i> Thông báo</a></li>
                                    <li><a href="<?php echo base_url('customer/transaction'); ?>"><i class="fa fa-tasks"></i> Chi tiết giao dịch</a></li>
                                    <li><a href="<?php echo base_url('customer/order'); ?>"><i class="fa fa-files-o"></i> Danh sách đơn hàng</a></li>
                                    <li><a href="<?php echo base_url('tao-don-hang'); ?>"><i class="fa fa-plus"></i> Tạo đơn hàng</a></li>
                                    <li><a href="<?php echo base_url('customer/complaint'); ?>"><i class="fa fa-commenting-o"></i> Danh sách khiếu nại</a></li>
                                    <li><a href="<?php echo base_url('customer/contact'); ?>"><i class="fa fa-phone"></i> Liên hệ</a></li>
                                    <li><a href="<?php echo site_url('user/logout'); ?>"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>
                                </ul>
                            </li>
                        <?php } else {
                            $user = $this->session->userdata('user');
                            if($user){ ?>
                                <li><a href="javascript:void(0)"><span class="spanBalance">0</span> VNĐ</a></li>
                                <li><a href="javascript:void(0)" id="aBalance">Nạp tiền</a></li>
                                <li class="dropdown user user-menu">
                                    <a href="<?php echo base_url('customer/order'); ?>">
                                        <?php $avatar = USER_PATH.((!empty($user['Avatar'])) ? $user['Avatar'] : NO_IMAGE); ?>
                                        <img src="<?php echo $avatar; ?>" alt="<?php echo $user['FullName']; ?>" class="user-image">
                                        <span class="hidden-xs"><?php echo $user['FullName']; ?></span>
                                    </a>
                                </li>
                                <li class="dropdown messages-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bell-o"></i>
                                        <span class="label label-danger countNotification">0</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header"><span class="countNotification">0</span> Thông báo</li>
                                        <li>
                                            <ul class="menu" id="ulNotificationHeader"></ul>
                                        </li>
                                        <li class="footer"><a href="<?php echo base_url('customer/notification'); ?>" id="aViewAllNotification">Xem tất cả thông báo</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown messages-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-envelope-o"></i>
                                        <span class="label label-success countMsgChat">0</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header"><span class="countMsgChat">0</span> Tin nhắn từ CSKH</li>
                                        <li>
                                            <input type="text" hidden="hidden" id="getListChatUnReadUrl" value="<?php echo base_url('chat/getListChatUnRead'); ?>">
                                            <ul class="menu" id="ulChatHeader"></ul>
                                        </li>
                                        <li class="footer"><a href="<?php echo base_url('customer/chat'); ?>" id="aViewAllChat">Xem tất cả Tin nhắn</a></li>
                                    </ul>
                                </li>
                                <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-caret-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url('customer/profile'); ?>" target="_blank"><i class="fa fa-user"></i> Thông tin tài khoản</a></li>
                                        <li><a href="<?php echo base_url('customer/notification'); ?>" target="_blank"><i class="fa fa-bell"></i> Thông báo</a></li>
                                        <li><a href="<?php echo base_url('customer/transaction'); ?>" target="_blank"><i class="fa fa-tasks"></i> Chi tiết giao dịch</a></li>
                                        <li><a href="<?php echo base_url('customer/order'); ?>" target="_blank"><i class="fa fa-files-o"></i> Danh sách đơn hàng</a></li>
                                        <li><a href="<?php echo base_url('tao-don-hang'); ?>" target="_blank"><i class="fa fa-plus"></i> Tạo đơn hàng</a></li>
                                        <li><a href="<?php echo base_url('customer/complaint'); ?>"><i class="fa fa-commenting-o"></i> Danh sách khiếu nại</a></li>
                                        <li><a href="<?php echo base_url('customer/contact'); ?>" target="_blank"><i class="fa fa-phone"></i> Liên hệ</a></li>
                                        <li><a href="<?php echo site_url('user/logout'); ?>"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>
                                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modalServiceFee"><i class="fa fa-money"></i> Bảng phí Dịch vụ (1)</a></li>
                                        <li><a href="javascript:void(0)"> <i class="fa fa-money"></i>Bảng phí vận chuyển TQ-VN (2)</a></li>
                                    </ul>
                                </li>
                            <?php } else { ?>
                                <li><a href="<?php echo base_url('user'); ?>" target="_blank">Đăng nhập</a></li>
                                <li><a href="<?php echo base_url('user/register'); ?>" target="_blank">Đăng ký</a></li>
                                <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-caret-down"></i>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#modalServiceFee"><i class="fa fa-money"></i> Bảng phí Dịch vụ (1)</a></li>
                                        <li><a href="javascript:void(0)"> <i class="fa fa-money"></i>Bảng phí vận chuyển TQ-VN (2)</a></li>
                                    </ul>
                                </li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
    <input type="text" hidden="hidden" id="getArticleContentUrl" value="<?php echo base_url('cms/article/getArticleContent'); ?>">
    <div class="modal fade" id="modalBalance" tabindex="-1" role="dialog" aria-labelledby="modalBalance">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Thông tin chuyển khoản</h4>
                </div>
                <div class="modal-body"></div>
                <input type="text" hidden="hidden" id="isGetRechargeText" value="0">
            </div>
        </div>
    </div>