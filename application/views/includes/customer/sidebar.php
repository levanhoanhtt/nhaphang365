<div class="box box-solid">
    <div class="box-body" style="border: 3px solid #d2d6de;margin-bottom: 10px;padding: 0;">
        <?php $this->load->view('includes/customer/menu'); ?>
    </div>
    <div class="chat-box" style="left: 30px;right: 0;">
        <a href="javascript:void(0)" class="maximizechat">
            <div class="chat-status-icon"></div>
        </a>
        <div class="chat-wrapper">
            <?php $this->load->view('includes/chat_by_customer'); ?>
        </div>
    </div>
</div>