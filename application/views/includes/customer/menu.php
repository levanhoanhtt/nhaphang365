<ul class="nav nav-pills nav-stacked" id="navbar-customer">
    <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i> Trang chủ</a></li>
    <li><a href="<?php echo base_url('customer/profile'); ?>"><i class="fa fa-user"></i> Thông tin tài khoản</a></li>
    <li><a href="<?php echo base_url('customer/notification'); ?>"><i class="fa fa-bell"></i> Thông báo</a></li>
    <li><a href="<?php echo base_url('customer/transaction'); ?>"><i class="fa fa-tasks"></i> Chi tiết giao dịch</a></li>
    <li><a href="<?php echo base_url('customer/order'); ?>"><i class="fa fa-files-o"></i> Danh sách đơn hàng</a></li>
    <li><a href="<?php echo base_url('tao-don-hang'); ?>"><i class="fa fa-plus"></i> Tạo đơn hàng</a></li>
    <li><a href="<?php echo base_url('customer/complaint'); ?>"><i class="fa fa-commenting-o"></i> Danh sách khiếu nại</a></li>
    <li><a href="<?php echo base_url('customer/contact'); ?>"><i class="fa fa-phone"></i> Liên hệ</a></li>
    <li><a href="<?php echo site_url('user/logout'); ?>"><i class="fa fa-sign-out"></i> Đăng xuất</a></li>
</ul>