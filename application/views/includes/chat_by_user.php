<div class="box box-primary direct-chat direct-chat-primary direct-chat-contacts-open" id="boxChatCustomer">
    <div class="box-header with-border">
        <h3 class="box-title">Chat với KH</h3>
        <p id="customerPhone"></p>
        <p id="customerEmail"></p>
        <div class="box-tools pull-right">
            <span data-toggle="tooltip" id="countChatUnRead" class="badge bg-light-blue" data-original-title="0 Tin Nhắn chưa đọc">0</span>
            <!--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-toggle="tooltip" data-widget="chat-pane-toggle" data-original-title="Danh sách Khách hàng"><i class="fa fa-user"></i></button>-->
            <a href="<?php echo base_url('user/chat'); ?>" target="_blank" data-toggle="tooltip" data-original-title="Chat ở tab mới" class="btn btn-box-tool"><i class="fa fa-windows"></i></a>
            <button type="button" class="btn btn-box-tool btn-tool-close" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="chat-content">
        <div class="box-body" style="display: block;">
            <div class="direct-chat-messages" id="listChat"></div>
            <div class="direct-chat-contacts">
                <button type="button" class="btn-chat-toggle">Thu gọn</button>
                <input type="text" hidden="hidden" id="imagePath" value="<?php echo PRODUCT_PATH; ?>">
                <input type="text" hidden="hidden" id="getCustomerUrl" value="<?php echo base_url('chat/getListCustomer'); ?>">
                <input type="text" hidden="hidden" id="getChatUrl" value="<?php echo base_url('chat/getList'); ?>">
                <input type="text" hidden="hidden" id="getCountChatUnReadByStaffUrl" value="<?php echo base_url('chat/getCountChatUnReadByStaff'); ?>">
                <input type="text" hidden="hidden" id="updateCountChatUnReadUrl" value="<?php echo base_url('chat/updateCountChatUnRead'); ?>">
                <input type="text" hidden="hidden" id="startChatPagging" value="0">
                <input type="text" hidden="hidden" id="totalChatMsg" value="0">
                <input type="text" hidden="hidden" id="customerId" value="0">
                <input type="text" hidden="hidden" id="customerName" value="">
                <input type="text" hidden="hidden" id="customerAvatar" value="<?php echo NO_IMAGE; ?>">
                <div class="contacts-chat-wapper">
                    <ul class="contacts-list" id="chat-staff-care-list"></ul>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer" style="display: block;">
        <form action="javascript:void(0)" method="post" id="chatForm">
            <div class="input-group">
                <input type="text" placeholder="Tin nhắn..." class="form-control" id="chatMsg" autocomplete="off">
                  <span class="input-group-btn">
                      <button type="submit" class="btn btn-primary btn-flat" data-toggle="tooltip" data-original-title="Gửi"><i class="fa fa-paper-plane"></i></button>
                      <button id="btnChatSendFile" type="button" class="btn btn-default btn-flat" data-toggle="tooltip" data-original-title="Gửi File"><i class="fa fa-paperclip"></i></button>
                  </span>
            </div>
        </form>
    </div>
</div>