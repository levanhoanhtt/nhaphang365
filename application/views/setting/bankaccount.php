<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bbanked">
                            <thead>
                            <tr>
                                <th>Tài khoản</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyBankAccount">
                            <?php
                            foreach($listBankAccounts as $ba){ ?>
                                <tr id="bankAccount_<?php echo $ba['BankAccountId']; ?>">
                                    <td id="bankAccountName_<?php echo $ba['BankAccountId']; ?>"><?php echo $ba['BankAccountName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $ba['BankAccountId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $ba['BankAccountId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('bankaccount/update', array('id' => 'bankAccountForm')); ?>
                                <td><input type="text" class="form-control hmdrequired" id="bankAccountName" name="BankAccountName" value="" data-field="Tài khoản"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="BankAccountId" id="bankAccountId" value="0" hidden="hidden">
                                    <input type="text" id="deleteBankAccountUrl" value="<?php echo base_url('bankaccount/delete'); ?>" hidden="hidden">
                                    <input type="text" hidden="hidden" id="updateBankAccount" value="<?php echo $updateBankAccount ? 1 : 0; ?>">
                                    <input type="text" hidden="hidden" id="deleteBankAccount" value="<?php echo $deleteBankAccount ? 1 : 0; ?>">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
