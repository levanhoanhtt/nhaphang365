<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Tài khoản</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyOrderAccount">
                            <?php
                            foreach($listOrderAccounts as $w){ ?>
                                <tr id="orderAccount_<?php echo $w['OrderAccountId']; ?>">
                                    <td id="orderAccountName_<?php echo $w['OrderAccountId']; ?>"><?php echo $w['OrderAccountName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $w['OrderAccountId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $w['OrderAccountId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('orderaccount/update', array('id' => 'orderAccountForm')); ?>
                                <td><input type="text" class="form-control hmdrequired" id="orderAccountName" name="OrderAccountName" value="" data-field="Tài khoản"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="OrderAccountId" id="orderAccountId" value="0" hidden="hidden">
                                    <input type="text" id="deleteOrderAccountUrl" value="<?php echo base_url('orderaccount/delete'); ?>" hidden="hidden">
                                    <input type="text" hidden="hidden" id="updateOrderAccount" value="<?php echo $updateOrderAccount ? 1 : 0; ?>">
                                    <input type="text" hidden="hidden" id="deleteOrderAccount" value="<?php echo $deleteOrderAccount ? 1 : 0; ?>">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>