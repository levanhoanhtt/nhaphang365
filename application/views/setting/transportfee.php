<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Chi nhánh</th>
                                <th>Từ (kg)</th>
                                <th>Đến (kg)</th>
                                <th>Phí (VNĐ/ 1 kg)</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransportFee">
                            <?php
                            foreach($listTransportFees as $tf){ ?>
                                <tr id="transportFee_<?php echo $tf['TransportFeeId']; ?>">
                                    <td id="warehouseName_<?php echo $tf['TransportFeeId']; ?>"><?php echo $this->Mconstants->getObjectValue($listWarehouses, 'WarehouseId', $tf['WarehouseId'], 'WarehouseName'); ?></td>
                                    <td id="beginWeight_<?php echo $tf['TransportFeeId']; ?>"><?php echo $tf['BeginWeight']; ?></td>
                                    <td id="endWeight_<?php echo $tf['TransportFeeId']; ?>"><?php echo $tf['EndWeight']; ?></td>
                                    <td id="weightCost_<?php echo $tf['TransportFeeId']; ?>"><?php echo priceFormat($tf['WeightCost']); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $tf['TransportFeeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $tf['TransportFeeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="warehouseId_<?php echo $tf['TransportFeeId']; ?>" value="<?php echo $tf['WarehouseId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('transportfee/update', array('id' => 'transportFeeForm')); ?>
                                <td><?php echo $this->Mconstants->selectObject($listWarehouses, 'WarehouseId', 'WarehouseName', 'WarehouseId'); ?></td>
                                <td><input type="number" class="form-control hmdrequired" id="beginWeight" name="BeginWeight" value="" data-field="Kg từ"></td>
                                <td><input type="number" class="form-control hmdrequired" id="endWeight" name="EndWeight" value="" data-field="Kg đến"></td>
                                <td><input type="text" class="form-control hmdrequired" id="weightCost" name="WeightCost" value="" data-field="Phí vận chuyển"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="TransportFeeId" id="transportFeeId" value="0" hidden="hidden">
                                    <input type="text" id="deleteTransportFeeUrl" value="<?php echo base_url('transportfee/delete'); ?>" hidden="hidden">
                                    <input type="text" hidden="hidden" id="updateTransportFee" value="<?php echo $updateTransportFee ? 1 : 0; ?>">
                                    <input type="text" hidden="hidden" id="deleteTransportFee" value="<?php echo $deleteTransportFee ? 1 : 0; ?>">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>