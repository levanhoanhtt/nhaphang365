<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Phương thức giao hàng</th>
                                <th>Phương thức cha</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransportType">
                            <?php $listParentTransportTypes = array();
                            foreach($listTransportTypes as $tt){
                                if($tt['ParentTransportTypeId'] == 0) $listParentTransportTypes[] = $tt; ?>
                                <tr id="transportType_<?php echo $tt['TransportTypeId']; ?>">
                                    <td id="transportTypeName_<?php echo $tt['TransportTypeId']; ?>"><?php echo $tt['TransportTypeName']; ?></td>
                                    <td id="parentTransportTypeName_<?php echo $tt['TransportTypeId']; ?>"><?php echo $this->Mconstants->getObjectValue($listTransportTypes, 'TransportTypeId', $tt['ParentTransportTypeId'], 'TransportTypeName'); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $tt['TransportTypeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $tt['TransportTypeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <input type="text" hidden="hidden" id="parentTransportTypeId_<?php echo $tt['TransportTypeId']; ?>" value="<?php echo $tt['ParentTransportTypeId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('transporttype/update', array('id' => 'transportTypeForm')); ?>
                                <td><input type="text" class="form-control hmdrequired" id="transportTypeName" name="TransportTypeName" value="" data-field="Phương thức giao hàng"></td>
                                <td><?php echo $this->Mconstants->selectObject($listParentTransportTypes, 'TransportTypeId', 'TransportTypeName', 'ParentTransportTypeId', 0, true, 'Không có'); ?></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="TransportTypeId" id="transportTypeId" value="0" hidden="hidden">
                                    <input type="text" id="deleteTransportTypeUrl" value="<?php echo base_url('transporttype/delete'); ?>" hidden="hidden">
                                    <input type="text" hidden="hidden" id="updateTransportType" value="<?php echo $updateTransportType ? 1 : 0; ?>">
                                    <input type="text" hidden="hidden" id="deleteTransportType" value="<?php echo $deleteTransportType ? 1 : 0; ?>">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>