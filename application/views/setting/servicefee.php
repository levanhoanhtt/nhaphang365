<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Từ (Triệu đồng)</th>
                                <th>Đến (Triệu đồng)</th>
                                <th>Phí dịch vụ (%)</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyServiceFee">
                            <?php
                            foreach($listServiceFees as $sf){ ?>
                                <tr id="serviceFee_<?php echo $sf['ServiceFeeId']; ?>">
                                    <td id="beginCost_<?php echo $sf['ServiceFeeId']; ?>"><?php echo $sf['BeginCost']; ?></td>
                                    <td id="endCost_<?php echo $sf['ServiceFeeId']; ?>"><?php echo $sf['EndCost']; ?></td>
                                    <td id="percent_<?php echo $sf['ServiceFeeId']; ?>"><?php echo priceFormat($sf['Percent']); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $sf['ServiceFeeId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $sf['ServiceFeeId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('servicefee/update', array('id' => 'serviceFeeForm')); ?>
                                <td><input type="number" class="form-control hmdrequired" id="beginCost" name="BeginCost" value="" data-field="Đơn hàng từ"></td>
                                <td><input type="number" class="form-control hmdrequired" id="endCost" name="EndCost" value="" data-field="Đơn hàng đến"></td>
                                <td><input type="text" class="form-control hmdrequired" id="percent" name="Percent" value="" data-field="Phí dịch vụ"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ServiceFeeId" id="serviceFeeId" value="0" hidden="hidden">
                                    <input type="text" id="deleteServiceFeeUrl" value="<?php echo base_url('servicefee/delete'); ?>" hidden="hidden">
                                    <input type="text" hidden="hidden" id="updateServiceFee" value="<?php echo $updateServiceFee ? 1 : 0; ?>">
                                    <input type="text" hidden="hidden" id="deleteServiceFee" value="<?php echo $deleteServiceFee ? 1 : 0; ?>">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>