<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nhân viên giao hàng</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyTransportUser">
                            <?php foreach($listTransportUsers as $tu){ ?>
                                <tr id="transportUser_<?php echo $tu['TransportUserId']; ?>">
                                    <td id="transportUserName_<?php echo $tu['TransportUserId']; ?>"><?php echo $tu['TransportUserName']; ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $tu['TransportUserId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $tu['TransportUserId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('transportuser/update', array('id' => 'transportUserForm')); ?>
                                <td><input type="text" class="form-control hmdrequired" id="transportUserName" name="TransportUserName" value="" data-field="Nhân viên giao hàng"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="TransportUserId" id="transportUserId" value="0" hidden="hidden">
                                    <input type="text" id="deleteTransportUserUrl" value="<?php echo base_url('transportuser/delete'); ?>" hidden="hidden">
                                    <input type="text" hidden="hidden" id="updateTransportUser" value="<?php echo $updateTransportUser ? 1 : 0; ?>">
                                    <input type="text" hidden="hidden" id="deleteTransportUser" value="<?php echo $deleteTransportUser ? 1 : 0; ?>">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>