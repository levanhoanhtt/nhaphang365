<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper dashboard-page">
        <div class="container-fluid">
            <?php //$this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="row">
                    <div class="col-lg-3 col-xs-6 match-height">
                        <div class="box box-default">
                            <?php sectionTitleHtml('CẦN CHECK'); ?>
                            <div class="box-body">
                                <a href="<?php echo base_url('order/1'); ?>"><span class="count" id="countOrder_1">0</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6 match-height">
                        <div class="box box-default">
                            <?php sectionTitleHtml('CHỜ DUYỆT'); ?>
                            <div class="box-body">
                                <a href="<?php echo base_url('order/4'); ?>"><span class="count" id="countOrder_4">0</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6 match-height">
                        <div class="box box-default">
                            <?php sectionTitleHtml('KHIẾU NẠI CHƯA DUYỆT'); ?>
                            <div class="box-body">
                                <a href="<?php echo base_url('complaint/3'); ?>"><span class="count" id="countComplaint_3">0</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6 match-height">
                        <div class="box box-default">
                            <?php sectionTitleHtml('KHIẾU NẠI CHƯA XỬ LÝ'); ?>
                            <div class="box-body">
                                <a href="<?php echo base_url('complaint/1'); ?>"><span class="count" id="countComplaint_1">0</span></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6 match-height">
                        <div class="box box-default">
                            <?php sectionTitleHtml('THÔNG BÁO'); ?>
                            <div class="box-body">
                                <a href="<?php echo base_url('notification'); ?>"><span class="count" id="countNotification_1">0</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row lowRevenue" id="staffRevenue"></div>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">Thưởng doanh số</a></li>
                        <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Thưởng thi đua</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">

                        </div>
                        <div class="tab-pane" id="tab_2">

                        </div>
                    </div>
                </div>
                <input type="text" hidden="hidden" id="statisticOrderUrl" value="<?php echo base_url('order/statistics'); ?>">
                <input type="text" hidden="hidden" id="statisticComplaintUrl" value="<?php echo base_url('complaint/statistics'); ?>">
                <input type="text" hidden="hidden" id="statisticNotificationUrl" value="<?php echo base_url('notification/statistics'); ?>">
                <input type="text" hidden="hidden" id="orderRevenueUrl" value="<?php echo base_url('order/getList03'); ?>">
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
