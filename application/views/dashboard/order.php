<?php $this->load->view('includes/header'); ?>
<?php $this->load->view('includes/menu'); ?>
    <div class="content-wrapper" style="min-height: 916px;">
        <div class="container-fluid">
            <?php //$this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <div class="box box-default">
                            <?php sectionTitleHtml('CHƯA CÓ MÃ VẬN ĐƠN'); ?>
                            <div class="box-body">
                                <span class="count"><?php echo 0; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <div class="box box-default">
                            <?php sectionTitleHtml('CẦN ĐẶT'); ?>
                            <div class="box-body">
                                <span class="count"><?php echo 0; ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <div class="box box-default">
                            <?php sectionTitleHtml('TĂNG CHI PHÍ'); ?>
                            <div class="box-body">
                                <span class="count">0</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <div class="box box-default">
                            <?php sectionTitleHtml('ĐÒI TIÊN'); ?>
                            <div class="box-body">
                                <span class="count">0</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <div class="box box-default">
                            <?php sectionTitleHtml('ĐƠN HÀNG THAY ĐỔI CHƯA DUYỆT'); ?>
                            <div class="box-body">
                                <span class="count">0</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xs-6">
                        <div class="box box-default">
                            <?php sectionTitleHtml('KHIẾU NẠI CHƯA XỬ LÝ'); ?>
                            <div class="box-body">
                                <span class="count">0</span>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>
