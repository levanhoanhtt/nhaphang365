<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <style>
                        #tbodySlider img{max-height: 100px;}
                        #tbodySlider #btnChooseImage{display: block;margin-bottom: 5px;}
                    </style>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ảnh</th>
                                <th>Tiêu đề</th>
                                <th style="width: 50%;">Link</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodySlider">
                            <?php foreach($listSliders as $s){ ?>
                                <tr id="slider_<?php echo $s['SliderId']; ?>">
                                    <td>
                                        <?php $attrSelect = ' onchange="changeDisplayOrder(this, \'' . $s['SliderId'] . '\')" data-id="'.$s['SliderId'].'"';
                                        $this->Mconstants->selectNumber(1, 100, 'DisplayOrder_'.$s['SliderId'], $s['DisplayOrder'], true, $attrSelect); ?>
                                    </td>
                                    <td><img src="<?php echo IMAGE_PATH.$s['SliderImage']; ?>" id="imgSlider_<?php echo $s['SliderId']; ?>"></td>
                                    <td id="sliderName_<?php echo $s['SliderId']; ?>"><?php echo $s['SliderName']; ?></td>
                                    <td id="sliderLink_<?php echo $s['SliderId']; ?>"><pre class="pre"><a class="sliderLink" href="<?php echo $s['SliderLink']; ?>" target="_blank"><?php echo $s['SliderLink']; ?></a></pre></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $s['SliderId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $s['SliderId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('cms/slider/update', array('id' => 'sliderForm')); ?>
                                <td><?php $this->Mconstants->selectNumber(1, 100, 'DisplayOrder', 1, true); ?></td>
                                <td>
                                    <button type="button" class="btn btn-default" id="btnChooseImage">Chọn ảnh</button>
                                    <img src="<?php echo IMAGE_PATH.NO_PRODUCT; ?>" id="imgSlider" style="display: none;">
                                </td>
                                <td><input type="text" class="form-control" id="sliderName" name="SliderName" value=""></td>
                                <td><input type="text" class="form-control" id="sliderLink" name="SliderLink" value=""></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="SliderId" id="sliderId" value="0" hidden="hidden">
                                    <input type="text" name="SliderImage" id="sliderImage" value="" hidden="hidden">
                                    <input type="text" name="SliderTypeId" id="sliderTypeId" value="1" hidden="hidden">
                                    <input type="text" hidden="hidden" id="changeDisplayOrderUrl" value="<?php echo base_url('cms/slider/changeDisplayOrder'); ?>">
                                    <input type="text" id="deleteSliderUrl" value="<?php echo base_url('cms/slider/delete'); ?>" hidden="hidden">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>