<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listStaffs, 'UserId', 'FullName', 'StaffId', set_value('StaffId'), true, 'Nhân viên CSKH', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" id="beginDate" value="" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" id="endDate" value="" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="button" id="submit" class="btn btn-primary" value="Tìm kiếm">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Nhân viên</th>
                                <th>Tháng</th>
                                <th>Tổng giá trí đơn hàng</th>
                                <th>Chi tiết</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyRevenue">

                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>