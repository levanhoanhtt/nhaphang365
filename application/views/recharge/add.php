<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php echo form_open('recharge/update', array('id' => 'rechargeForm')); ?>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Thời gian</label>
                            <input type="text" name="RechargeDate" class="form-control" disabled value="<?php echo date('d/m/Y H:i'); ?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Tài khoản ngân hàng</label>
                            <?php $this->Mconstants->selectObject($listBankAccounts, 'BankAccountId', 'BankAccountName', 'BankAccountId', set_value('BankAccountId'), true, 'Tài khoản ngân hàng'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Số tệ <span class="required">*</span></label>
                            <input type="text" name="PaidTQ" id="paidTQ" class="form-control cost" value="0" data-field="Số tệ">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label class="control-label">Tỉ giá <span class="required">*</span></label>
                            <input type="text" name="ExchangeRate" id="exchangeRate" class="form-control cost" value="0">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Ghi chú</label>
                    <textarea rows="2" name="Comment" class="form-control"></textarea>
                </div>
                <div class="form-group text-right">
                    <input type="text" name="RechargeId" hidden="hidden" value="0">
                    <input type="text" name="ShopNumberId" hidden="hidden" value="0">
                    <input type="text" name="RechargeTypeId"  hidden="hidden" value="2">
                    <input type="text" hidden="hidden" id="rechargeUrl" value="<?php echo base_url('recharge'); ?>">
                    <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>