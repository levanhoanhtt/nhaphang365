<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('recharge'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listBankAccounts, 'BankAccountId', 'BankAccountName', 'BankAccountId', set_value('BankAccountId'), true, 'Tài khoản ngân hàng'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('rechargeTypes', 'RechargeTypeId', set_value('RechargeTypeId'), true, 'Loại'); ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml('Số dư hiện tại'); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tài khoản ngân hàng</th>
                                <th>Số tệ</th>
                                <th>Tính đến</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            $bankAccountIds = array();
                            foreach($listAccountBalances as $ab){
                                $i++;
                                $bankAccountIds[]=$ab['BankAccountId']; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listBankAccounts, 'BankAccountId', $ab['BankAccountId'], 'BankAccountName'); ?></td>
                                    <td><?php echo priceFormat($ab['Balance'], true); ?></td>
                                    <td><?php echo ddMMyyyy($ab['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                </tr>
                            <?php }
                            if(count($bankAccountIds) < count($listBankAccounts)){
                                foreach($listBankAccounts as $ba){
                                    if(!in_array($ba['BankAccountId'], $bankAccountIds)){
                                        $i++; ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $ba['BankAccountName']; ?></td>
                                            <td>0</td>
                                            <td><?php echo date('d/m/Y H:i'); ?></td>
                                        </tr>
                                    <?php }
                                }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box box-success">
                    <?php sectionTitleHtml('<a href="'.base_url('recharge/add').'" class="btn btn-primary">Nạp tệ</a>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tài khoản ngân hàng</th>
                                <th>Thời gian</th>
                                <th>Số tệ</th>
                                <th>Tỷ giá</th>
                                <th>Loại</th>
                                <th>Ghi chú</th>
                                <th>Ghi nợ</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            $labelCss = $this->Mconstants->labelCss;
                            $rechargeTypes = $this->Mconstants->rechargeTypes;
                            foreach($listRecharges as $rc){
                                $i++; ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listBankAccounts, 'BankAccountId', $rc['BankAccountId'], 'BankAccountName'); ?></td>
                                    <td><?php echo ddMMyyyy($rc['RechargeDate'], 'd/m/Y H:i'); ?></td>
                                    <td><?php echo priceFormat($rc['PaidTQ'], true); ?></td>
                                    <td><?php echo priceFormat($rc['ExchangeRate']); ?></td>
                                    <td><span class="<?php echo $labelCss[$rc['RechargeTypeId']]; ?>"><?php echo $rechargeTypes[$rc['RechargeTypeId']]; ?></span></td>
                                    <td><?php echo $rc['Comment']; ?></td>
                                    <td><?php echo priceFormat($rc['Balance'], true); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>