<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($userId > 0){ ?>
                <?php echo form_open('user/saveUser', array('id' => 'userForm')); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Họ và tên <span class="required">*</span></label>
                            <input type="text" name="FullName" class="form-control hmdrequired" value="<?php echo $userEdit['FullName']; ?>" data-field="Họ và tên">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tên đăng nhập <span class="required">*</span></label>
                            <input type="text" name="UserName" id="userName" class="form-control hmdrequired" value="<?php echo $userEdit['UserName']; ?>" data-field="Tên đăng nhập">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="text" name="Email" class="form-control" value="<?php echo $userEdit['Email']; ?>">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Giới tính</label>
                            <?php $this->Mconstants->selectConstants('genders', 'GenderId', $userEdit['GenderId']); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Trạng thái</label>
                            <?php $this->Mconstants->selectConstants('status', 'StatusId', $userEdit['StatusId']); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tỉnh/ Thành phố</label>
                            <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', $userEdit['ProvinceId'], false, '', ' select2'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Địa chỉ <span class="required">*</span></label>
                    <input type="text" name="Address" class="form-control hmdrequired" value="<?php echo $userEdit['Address']; ?>" data-field="Địa chỉ">
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <?php $avatar = (empty($userEdit['Avatar']) ? NO_IMAGE : $userEdit['Avatar']); ?>
                            <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 200px;height: 200px;display: block;">
                            <input type="text" hidden="hidden" name="Avatar" id="avatar" value="<?php echo $avatar; ?>">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Di động</label>
                                    <input type="text" name="PhoneNumber" id="phoneNumber" class="form-control" value="<?php echo $userEdit['PhoneNumber']; ?>">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Phân cấp</label>
                                    <?php $this->Mconstants->selectConstants('roles', 'RoleId', $userEdit['RoleId']); ?>
                                </div>
                            </div>
                        </div>
                        <?php if($canEdit){ ?>
                            <div class="form-group text-right">
                                <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                                <input type="text" name="UserId" id="userId" hidden="hidden" value="<?php echo $userId; ?>">
                                <input type="text" name="UserPass" hidden="hidden" value="<?php echo $userEdit['UserPass']; ?>">
                                <input type="text" hidden="hidden" name="UserTypeName" value="<?php echo $isStaff ? 'nhân viên' : 'khách hàng'; ?>">
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>