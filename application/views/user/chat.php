<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper" id="chatToCustomerPage">
        <div class="container-fluid">
            <section class="content-header">
                <h1><?php echo $title; ?></h1>
                <ol class="breadcrumb">
                    <li><input type="checkbox" id="cbOnChat"> <label id="lbOnChat">Online</label></li>
                </ol>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-sm-3" style="padding: 0;">
                        <div class="box box-primary">
                            <div class="box-header">
                                <?php echo form_open('chat/getListCustomerChat', array('id' => 'seachCustomerChatForm', 'class' => "input-group input-group-sm")); ?>
                                <input type="text" class="form-control" id="txtCustomerChatName" placeholder="Tìm kiếm Khách hàng">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <div class="box-body box-comments" id="listChatCustomer"></div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="box box-primary direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title"><?php echo $title; ?></h3>
                            </div>
                            <div class="box-body" style="display: block;height: 355px;">
                                <div class="direct-chat-messages" id="listChat" style="height: 350px;"></div>
                                <div class="direct-chat-contacts">
                                    <input type="text" hidden="hidden" id="imagePath" value="<?php echo PRODUCT_PATH; ?>">
                                    <input type="text" hidden="hidden" id="getChatUrl" value="<?php echo base_url('chat/getList'); ?>">
                                    <input type="text" hidden="hidden" id="updateCountChatUnReadUrl" value="<?php echo base_url('chat/updateCountChatUnRead'); ?>">
                                    <input type="text" hidden="hidden" id="startChatPagging" value="0">
                                    <input type="text" hidden="hidden" id="totalChatMsg" value="0">
                                    <input type="text" hidden="hidden" id="customerId" value="0">
                                    <input type="text" hidden="hidden" id="customerName" value="">
                                    <input type="text" hidden="hidden" id="customerAvatar" value="<?php echo NO_IMAGE; ?>">
                                </div>
                            </div>
                            <div class="box-footer" style="display: block;">
                                <form action="javascript:void(0)" method="post" id="chatForm">
                                    <div class="input-group">
                                        <input type="text" placeholder="Tin nhắn..." class="form-control" id="chatMsg" autocomplete="off">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-primary btn-flat">Gửi</button>
                                            <button id="btnChatSendFile" type="button" class="btn btn-default btn-flat"><i class="fa fa-paperclip"></i></button>
                                        </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2" style="padding: 0;">
                        <div class="box box-primary">
                            <div class="box-header">
                                <?php echo form_open('chat/getListCustomerChat', array('id' => 'seachCustomerChatForm2', 'class' => "input-group input-group-sm")); ?>
                                <input type="text" class="form-control" id="txtCustomerChatName2" placeholder="Tìm kiếm">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <div class="box-body box-comments" id="listChatCustomer2"></div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>