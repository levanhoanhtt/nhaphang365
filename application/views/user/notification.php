<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <div class="box-body row-margin">
                        <ul class="list-inline" id="ulNotificationStatus">
                            <li><a href="<?php echo base_url('notification'); ?>" class="btn btn-primary">Tất cả (<?php echo $statisticNotificationCounts[0]; ?>)</a></li>
                            <li><a href="<?php echo base_url('notification/1'); ?>" class="btn btn-default">Chưa xem (<?php echo $statisticNotificationCounts[1]; ?>)</a></li>
                            <li><a href="<?php echo base_url('notification/2'); ?>" class="btn btn-default">Đã xem (<?php echo $statisticNotificationCounts[2]; ?>)</a></li>
                        </ul>
                        <?php echo ($notificationStatusId > 0) ? form_open('notification/'.$notificationStatusId) : form_open('notification'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('notificationStatus', 'NotificationStatusId', set_value('NotificationStatusId'), true, 'Tất cả'); ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Lọc">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml('<a href="javascript:void(0)" class="btn btn-primary" id="aReadAll">Đã đọc toàn bộ</a>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center">Ngày</th>
                                <th>Khách hàng</th>
                                <th class="text-center">Nội dung</th>
                                <th>Loại</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyNotification">
                            <?php $fullNames = array();
                            foreach($listNotifications as $n){
                                if(!isset($fullNames[$n['CustomerId']])) $fullNames[$n['CustomerId']] = $this->Musers->getFieldValue(array('UserId' => $n['CustomerId']), 'FullName');
                                $class = '';
                                if($n['NotificationStatusId'] == 1 && $n['IsCustomerSend'] == 0) $class = ' class="warning"';
                                else if($n['NotificationStatusId'] == 1 && $n['IsCustomerSend'] == 1) $class = ' class="success"'; ?>
                                <tr id="notification_<?php echo $n['NotificationId']; ?>" <?php echo $class; ?>>
                                    <td class="text-center"><?php echo ddMMyyyy($n['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td><a href="<?php echo base_url('transaction/customer/'.$n['CustomerId']); ?>"><?php echo $fullNames[$n['CustomerId']]; ?></a></td>
                                    <td><a href="<?php echo empty($n['LinkTo']) ? 'javascript:void(0)' : $n['LinkTo']; ?>" class="link_to" data-id="<?php echo $n['NotificationId']; ?>" data-status="<?php echo $n['NotificationStatusId']; ?>" data-check="<?php echo $n['IsCustomerSend']; ?>"><?php echo $n['Message']; ?></a></td>
                                    <td><?php echo ($n['IsCustomerSend'] == 1) ? 'Khách gửi' : 'Hệ thống gửi'; ?></td>
                                    <td class="actions">
                                        <?php if($n['NotificationStatusId'] == 1 && $n['IsCustomerSend'] == 1){ ?>
                                            <a href="javascript:void(0)" title="Đánh dấu đã đọc" class="link_check" data-id="<?php echo $n['NotificationId']; ?>"><i class="fa fa-check"></i></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="changeAllStatusUrl" value="<?php echo base_url('notification/changeAllStaus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>