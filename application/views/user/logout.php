<!DOCTYPE html>
<html lang="vi">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Đăng xuất</title>
    <base href="<?php echo base_url();?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <?php $this->load->view('includes/favicon'); ?>
    <meta name="robots" content="noindex, follow">
</head>
<body>
<p style="text-align: center;">Xin vui lòng chờ trong giây lát...</p>
<input type="text" hidden="hidden" id="userId" value="<?php echo $userId; ?>">
<input type="text" hidden="hidden" id="roleId" value="<?php echo $roleId; ?>">
<input type="text" hidden="hidden" id="loginUrl" value="<?php echo base_url('user'); ?>">
<input type="text" hidden="hidden" id="chatServerUrl" value="<?php echo CHAT_SERVER; ?>">
<input type="text" hidden="hidden" id="siteName" value="Đặt hàng 86">
<noscript><meta http-equiv="refresh" content="0; url=<?php echo base_url('user/permission'); ?>" /></noscript>
<script src="assets/vendor/plugins/jQuery/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="assets/js/common.js"></script>
<script src="assets/vendor/plugins/socket.io/socket.io.js"></script>
<script type="text/javascript" src="assets/js/chat_by_customer.js"></script>
<script type="text/javascript" src="assets/js/user_logout.js"></script>
</body>
</html>