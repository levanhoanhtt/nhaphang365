<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php echo form_open('user/saveUser', array('id' => 'userForm')); ?>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Họ và tên <span class="required">*</span></label>
                            <input type="text" name="FullName" class="form-control hmdrequired" value="" data-field="Họ và tên">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Email/ Facebook</label>
                            <input type="text" name="Email" class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tỉnh/ Thành phố</label>
                            <?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId', 0, false, '', ' select2'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Giới tính</label>
                            <?php $this->Mconstants->selectConstants('genders', 'GenderId'); ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Trạng thái</label>
                            <?php $this->Mconstants->selectConstants('status', 'StatusId'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Di động <input type="radio" data-id="1" name="MainPhoneNumber" class="iCheck" checked="checked"></label>
                            <input type="text" id="phoneNumber1" class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Di động <input type="radio" data-id="2" name="MainPhoneNumber" class="iCheck"></label>
                            <input type="text" id="phoneNumber2" class="form-control" value="">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Di động <input type="radio" data-id="3" name="MainPhoneNumber" class="iCheck"></label>
                            <input type="text" id="phoneNumber3" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Địa chỉ <span class="required">*</span></label>
                    <input type="text" name="Address" class="form-control hmdrequired" value="" data-field="Địa chỉ">
                </div>
                <div class="form-group">
                    <label class="control-label">Ghi chú</label>
                    <input type="text" name="Comment" class="form-control" value="">
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <?php $avatar = (set_value('Avatar')) ? set_value('Avatar') : NO_IMAGE; ?>
                            <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 200px;height: 200px;display: block;">
                            <input type="text" hidden="hidden" name="Avatar" id="avatar" value="<?php echo $avatar; ?>">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Mật khẩu <span class="required">*</span></label>
                                    <input type="text" id="newPass" name="UserPass" class="form-control hmdrequired" value="123456" data-field="Mật khẩu">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Gõ lại Mật khẩu <span class="required">*</span></label>
                                    <input type="text" id="rePass" class="form-control hmdrequired" value="123456" data-field="Mật khẩu">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <a href="javascript:void(0)" class="btn btn-default" id="generatorPass">Sinh Mật khẩu</a>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label"><input type="checkbox" name="IsSendPass" class="iCheck" checked="checked"> Gửi mật khẩu vào email</label>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <input type="text" name="UserId" id="userId" hidden="hidden" value="0">
                            <input type="text" name="RoleId" id="roleId" hidden="hidden" value="4">
                            <input type="text" id="phoneNumber" name="PhoneNumber" hidden="hidden" value="">
                            <input type="text" id="phoneNumbers" name="PhoneNumbers" hidden="hidden" value="">
                            <input type="text" hidden="hidden" name="UserTypeName" value="<?php echo $isStaff ? 'nhân viên' : 'khách hàng'; ?>">
                            <input type="text" id="userEditUrl" hidden="hidden" value="<?php echo base_url('user/edit'); ?>">
                            <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>