<?php $configs = $this->session->userdata('configs');
$hotline = isset($configs['PHONE_CARE']) ? $configs['PHONE_CARE'] : '0984913326';
$email = isset($configs['EMAIL_COMPANY']) ? $configs['EMAIL_COMPANY'] : 'hotro@nhaphang365.vn';
$exchangeRate = isset($configs['EXCHAGE_RATE_CN']) ? $configs['EXCHAGE_RATE_CN'] : '3450'; ?>
<div class="footer-block">
    <div class="row footer-container">
        <div class="footer-block-contact">
            <span class="footer-block-title" style="margin-bottom: 20px">Liên hệ</span>
            <div class="footer-block-content">
                <p class="hot-line">
                    <span>Hotline: </span><?php echo $hotline; ?> <span class="email">Email hỗ trợ: <?php echo $email; ?></span>
                </p>
                <div class="block-address">
                    <span class="address-title">Hà Nội</span>
                    <ul class="address-list">
                        <li>
                            <span class="address">CT4 khu đô thị mới Tứ Hiệp, Thanh Trì</span>
                        </li>
                    </ul>
                </div>
                <!--<div style="display: none" class="block-address">
                    <span class="address-title">Hồ Chí Minh</span>
                    <ul class="address-list">
                        <li>
                            <span class="address">Lầu 1, Số 92 - Đường số 2 - Cư Xá Đô Thành (Vườn Chuối) - P4 - Q3</span>
                        </li>
                    </ul>
                </div>-->
            </div>
        </div>
        <div class="footer-block-account-info" style="display: none;">
            <span class="footer-block-title">Tỷ giá hiện tại</span>

            <div class="exchange-rate" style="float: left; clear: both;">
                <span style="display: block">Trung Quốc: <span style="color: #C44853;font-weight: 500"><?php echo priceFormat($exchangeRate); ?></span></span>
            </div>
        </div>
        <div class="footer-block-blog">
            <span class="footer-block-title">Blog</span>
            <div class="footer-block-content">
                <?php foreach ($listArticleFooters as $af) { ?>
                    <div class="block-post">
                        <span class="post-title"><a href="<?php echo base_url($af['ArticleSlug'].'-p'.$af['ArticleId']); ?>"><?php echo $af['ArticleTitle']; ?></a></span>
                    </div>
                <?php }?>
            </div>
        </div>
        <div class="footer-block-cert">
            <span class="footer-block-title">Được chứng nhận</span>

            <div class="footer-block-content">
                <div class="block-cert">
                    <a href="http://merchant_cert.lab.baokim.vn/chung_nhan/00000/615_NHAPHANG365_COM" target="_blank"><img src="assets/front/v1/images/cert_baokim.png"></a>
                </div>
                <div class="block-cert">
                    <a href="javascript:void(0)" target="_blank"><img src="assets/front/v1/images/dathongbao.png"></a>
                </div>
            </div>
        </div>
        <span class="footer-line"></span>
        <!--<span class="footer-logo-2"><img src="assets/front/v1/images/logo.png" alt=""/></span>-->
    </div>
</div>
<div class="chat-box">
    <a href="javascript:void(0)" class="maximizechat">
        <div class="chat-status-icon"></div>
    </a>
    <div class="chat-wrapper"">
        <?php $this->load->view('includes/chat_by_customer'); ?>
    </div>
</div>
<div class="reveal-overlay" id="register_Modal" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="reveal nh-popup" id="modal-signup" style="display: none; top: 79px;">
        <div class="modal-header">
            <span>Đăng ký tài khoản mới</span>
            <a class="close-button" data-close="" aria-label="Close reveal" type="button">×</a>
        </div>
        <div class="modal-content sgup">
            <?php echo form_open('user/saveUser', array('id' => 'registerForm', 'class' => 'form-signup')); ?>
            <div class="row-input">
                <label class="input-label">
                    <span class="label-name">Họ tên</span>
                    <span class="message"><i class="fa fa-exclamation-triangle name"></i></span>
                </label>
                <input type="text" class="input-name" name="FullName" placeholder="Họ và tên" data-field="Họ và tên">
            </div>
            <div class="row-input">
                <label class="input-label">
                    <span class="label-name">Số điện thoại</span>
                    <span class="message"><i class="fa fa-exclamation-triangle name"></i></span>
                </label>
                <input type="text" class="input-name" name="PhoneNumber" id="phoneNumber"  placeholder="Di động" data-field="Di động">
            </div>
            <div class="row-input">
                <label class="input-label">
                    <span class="label-name">Gmail</span>
                    <span class="message"><i class="fa fa-exclamation-triangle name"></i></span>
                </label>
                <input type="text" class="input-name" name="Email" placeholder="Email/ Facebook">
            </div>
            <div class="row-input">
                <label class="input-label">
                    <span class="label-name">Giới tính</span>
                    <span class="message"><i class="fa fa-exclamation-triangle name"></i></span>
                </label>
                <?php $this->Mconstants->selectConstants('genders', 'GenderId'); ?>
            </div>
            <div class="row-input">
                <label class="input-label">
                    <span class="label-name">Tỉnh/Thành phố</span>
                    <span class="message"><i class="fa fa-exclamation-triangle name"></i></span>
                </label>
                <select class="form-control" name="ProvinceId" id="provinceId"></select>
            </div>
            <div class="row-input">
                <label class="input-label">
                    <span class="label-name">Địa chỉ</span>
                    <span class="message"><i class="fa fa-exclamation-triangle name"></i></span>
                </label>
                <input type="text" class="input-name" name="Address" class="form-control hmdrequired" value="" placeholder="Địa chỉ" data-field="Địa chỉ">
            </div>
            <div class="row-input">
                <label class="input-label">
                    <span class="label-name">Mật khẩu</span>
                    <span class="message"><i class="fa fa-exclamation-triangle name"></i></span>
                </label>
                <input type="password" class="input-name" id="newPass" name="UserPass" placeholder="Mật khẩu" data-field="Mật khẩu">
            </div>
            <div class="row-input">
                <label class="input-label">
                    <span class="label-name">Xác nhận mật khẩu</span>
                    <span class="message"><i class="fa fa-exclamation-triangle password"></i></span>
                </label>
                <input type="password" class="input-pass" id="rePass" name="rePass"  placeholder="Gõ lại Mật khẩu" data-field="Mật khẩu">
                <a class="show-pass"><i class="fa nb-eye"></i></a>
            </div>
            <div class="end-form"></div>
            <div class="row-input">
                <button type="submit" class="btn-submit">Đăng ký</button>
                <input type="hidden" name="UserId" value="0">
                <input type="hidden" name="RoleId" value="4">
                <input type="hidden" name="StatusId" value="<?php echo STATUS_ACTIVED; ?>">
                <input type="hidden" name="Avatar" value="<?php echo NO_IMAGE; ?>">
                <input type="hidden" name="UserTypeName" value="người dùng">
                <input type="hidden" id="getListProvinceUrl" value="<?php echo base_url('site/getListProvinces'); ?>">
            </div>
            <?php echo form_close(); ?>
        </div>

    </div>
</div>
<div class="reveal-overlay" id="login_Modal" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="reveal nh-popup" id="modal-signup2" style="display: none; top: 31px;">
        <div class="modal-header">
            <span>Đăng nhập</span>
            <a class="close-button" data-close="" aria-label="Close reveal" type="button">×</a>
        </div>
        <div class="modal-content">
            <div class="signup2">
                <?php echo form_open('user/checkLogin', array('id' => 'loginForm', 'class' => 'signup2-form')); ?>
                <div class="row-input">
                    <label class="input-label">
                        <span class="label-name">Số điện thoại</span>
                        <span class="message"><i class="fa fa-exclamation-triangle  user-name"></i></span>
                    </label>
                    <input type="text" class="input-name" value="<?php echo $userName; ?>" name="UserName">
                </div>
                <div class="row-input">
                    <label class="input-label">
                        <span class="label-name">Mật khẩu</span>
                        <span class="message"><i class="fa fa-exclamation-triangle  pass-word"></i></span>
                    </label>
                    <input type="password" class="input-pass" value="<?php echo $userPass; ?>" name="UserPass">
                    <a class="show-pass"><i class="fa nb-eye"></i></a>
                    <a href="/quen-mat-khau" class="forgot-pass">Quên mật khẩu?</a>
                </div>
                <div style="clear: both"></div>
                <div class="row-input">
                    <button class="btn-submit" <?php if ($userId > 0) echo ' style="display:none;"'; ?>>
                        Đăng nhập <span class="loading" style="display: none;"><i
                                    class="fa fa-spinner fa-spin"></i></span></button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="redirectProductUrl" value="<?php echo base_url('product/redirect'); ?>">
<input type="hidden" id="userImagePath" value="<?php echo USER_PATH; ?>">
<input type="hidden" id="chatServerUrl" value="<?php echo CHAT_SERVER; ?>">
<input type="hidden" id="getListNotificationHeaderUrl"
       value="<?php echo base_url('notification/getListForHeader'); ?>">
<input type="hidden" id="changeStatusNotificationUrl" value="<?php echo base_url('notification/changeStatus'); ?>">
<?php if ($userId > 0) { ?>
    <input type="hidden" id="userLoginId" value="<?php echo $userId; ?>">
    <input type="hidden" id="fullNameLoginId" value="<?php echo $fullName ?>">
    <input type="hidden" id="avatarLoginId" value="<?php echo empty($userAvatar) ? NO_IMAGE : $userAvatar; ?>">
<?php } else { ?>
    <input type="hidden" id="userLoginId" value="0">
    <input type="hidden" id="fullNameLoginId" value="">
    <input type="hidden" id="avatarLoginId" value="<?php echo NO_IMAGE; ?>">
<?php } ?>
<noscript>
    <meta http-equiv="refresh" content="0; url=<?php echo base_url('user/permission'); ?>"/>
</noscript>
<script src="assets/front/v1/js/jquery.js"></script>
<script src="assets/front/v1/libs/owl-carousel/owl.carousel.min.js"></script>
<script src="assets/front/v1/js/app.js?20171204"></script>
<script type="text/javascript" src="assets/vendor/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="assets/vendor/plugins/iCheck/icheck.min.js"></script>
<script src="assets/vendor/plugins/pnotify/pnotify.custom.js"></script>
<script src="assets/vendor/plugins/lazyload/jquery.lazyload.min.js"></script>
<script type="text/javascript" src="assets/front/v1/js/main-extends.js"></script>
<!--Chat-->
<script src="assets/vendor/plugins/socket.io/socket.io.js"></script>
<script src="assets/vendor/plugins/jquery.playSound.js"></script>
<script type="text/javascript" src="assets/ckfinder/ckfinder.js"></script>
<script type="text/javascript" src="assets/js/chat_by_customer.js"></script>
<?php //$this->load->view('site/ga'); ?>
</body>
</html>