<div class="blog-pages-left">
    <ul class="blog-categories">
        <?php foreach ($listCategorySidebars as $cs) { ?>
            <li class="blog-category">
                <a href="<?php echo base_url($cs['CategoryNameTQ'].'-c'.$cs['CategoryId']); ?>">
                    <span class="blog-icon blog-category-icon"></span>
                    <span class="blog-category-name"><?php echo $cs['CategoryName']; ?></span>
                    <i class="fa fa-angle-right blog-angle-right"></i>
                </a>
            </li>
        <?php } ?>
    </ul>
    <div style="clear: both"></div>
</div>