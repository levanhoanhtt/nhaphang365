<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title><?php echo $title; ?></title>
    <base href="<?php echo base_url(); ?>"/>
    <?php $this->load->view('includes/favicon'); ?>
    <link href="assets/front/v1/libs/owl-carousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="assets/front/v1/libs/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="assets/vendor/plugins/pnotify/pnotify.custom.css"/>
    <link href="assets/front/v1/css/style.css" rel="stylesheet">
    <link href="assets/front/v1/css/nh-custom.css?20171204" rel="stylesheet">
    <link href="assets/front/v1/css/blog.css" rel="stylesheet">
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/59c271d04854b82732ff11cc/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
</head>
<body id="main-body">
<div class="top-header" id="topHead" style="left: 0px;">
    <div class="row top-header-container">
        <a href="<?php echo base_url(); ?>" class="logo-top"><img src="assets/front/v1/images/logo.png" alt="" style="height: 50px;padding-bottom: 5px;margin-top: -3px;"/></a>
        <form action="<?php echo base_url('product/redirect'); ?>" id="productSearchForm" method="get" target="_blank" accept-charset="utf-8">
            <div class="search-box">
                <div class="select-site-container">
                    <div id="dd" class="list-site-container" tabindex="1">
                        <span class="selected-site">Taobao</span>
                        <ul class="list-site notranslate">
                            <li  data-id="1"><a href="javascript:void(0)" title="Taobao(TQ)">Taobao<span style="float: right;">(TQ)</span></a></li>
                            <li  data-id="2"><a href="javascript:void(0)" title="Tmall(TQ)">Tmall<span style="float: right;">(TQ)</span></a></li>
                            <li  data-id="3"><a href="javascript:void(0)" title="1688(TQ)">1688<span style="float: right;">(TQ)</span></a></li>
                        </ul>
                    </div>
                </div>
                <input type="hidden" name="ShopId" id="shopId" value="1">
                <input type="text" class="search-input header-search" name="ProductName" placeholder="Nhập tên sản phẩm tiếng Việt">
                <button type="submit" class="btn-search"></button>
            </div>
        </form>
        <span class="create-order" id="create-order">
             <a href="<?php echo base_url('tao-don-hang'); ?>" class="create-order-link" title="Tạo đơn hàng">Tạo đơn hàng</a>
        </span>
        <span class="order-label">
                <a href="<?php echo base_url('customer/order'); ?>" id="createDeal" data-reveal-id="modal-signup2"
                   class="modal-signup2 list-order-label">Danh sách đơn hàng</a>
        </span>
        <div class="notification-block">
            <div class="notification-block-message">
                <a href="javascript:void(0)" class="notification-block-message-link fa fa-comments"
                   data-dropdown=".notification-message-details" title="Tin nhắn" ></a>
                <span id="count-chat-notify" class="alert badge" style="display: none;">0</span>

                <div class="notification-message-details" style="display: none;">
                    <div class="details-popup-header">
                        <span class="title">Tin nhắn</span>
                    </div>
                    <div class="details-popup-content">
                        <ul class="list-message">
                            <input type="hidden" id="getListChatUnReadUrl" value="<?php echo base_url('chat/getListChatUnRead'); ?>">
                            <a href="javascript:void(0)" id="aViewAllChat" style="display: none;"></a>
                            <ul class="menu" id="ulChatHeader"></ul>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="notification-block-notice">
                <a href="<?php echo base_url('customer/notification'); ?>" id="aViewAllNotification"
                   data-reveal-id="modal-signup2"
                   class="modal-signup2 notification-block-notice-link" title="Thông báo"></a>
            </div>

            <div class="notification-block-message">
                <a href="javascript:void(0)" class="notification-block-feedback-link fa fa-envelope" title="Góp ý"></a>
                <div class="notification-message-details feedback-form" style="right: -12px !important; display: none;">
                    <div class="details-popup-header">
                        <span class="title">Góp ý</span>
                    </div>
                    <div class="details-popup-content-2">
                        <p class="fb_success">Cảm ơn bạn đã đóng góp ý kiến để giúp Nhập Hàng 365 nâng cao chất lượng dịch vụ!</p>
                        <form action="javascript:void(0)" method="post" style="margin: 13px">
                            <div style="">
                                <label>Số điện thoại <span class="fb_mess_phone"></span></label>
                                <input type="text" name="fb_phone" class="fb_phone" value="">
                            </div>
                            <div>
                                <label>Nội dung <span class="fb_mess_content"></span></label>
                                <span class="fb-notes">(Chỉ tiếp nhận các góp ý về chất lượng dịch vụ hoặc hệ thống. Các vấn đề liên quan đơn hàng, đặt hàng xin liên hệ CSKH, các vẫn đề liên quan khiếu nại xin tạo phiếu khiếu nại trong đơn hàng. Cảm ơn quý khách!)</span>
                                <textarea name="fb_content" class="fb_content" style="min-height: 130px"></textarea>
                            </div>
                            <a href="javascript:void(0)" class="btn-feedback">
                                Gửi
                                <span class="loading" style="display: none;"><i class="fa fa-spinner fa-spin"></i></span>
                            </a>
                            <a href="javascript:void(0)" class="btn-feedback-close" style="display: none">Đóng</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--none session authen-->
        <a href="javascript:void(0)"<?php if ($userId > 0) echo ' style="display:none;"'; ?> id="link-modal-sign-up" data-reveal-id="modal-signup" class="modal-signup nb-signup">Đăng ký</a>
        <a href="javascript:void(0)"<?php if ($userId > 0) echo ' style="display:none;"'; ?> id="link-modal-login" data-reveal-id="modal-signup2" class="modal-signup2 nb-signup">Đăng nhập</a>
        <a id="aLogout" data-url="<?php echo base_url('user/userLogout'); ?>" class="modal-signup2 nb-signup" href="javascript:void(0)"<?php if ($userId == 0) echo ' style="display:none;"'; ?>>Đăng xuất</a>
        <a id="aAvartar" href="<?php echo base_url('customer/order'); ?>" href="javascript:void(0)"<?php if ($userId == 0) echo ' style="display:none;"'; ?> class="modal-signup nb-signup">
            <div></div>
            <span class="f_click fullName"><?php echo $fullName; ?></span>
        </a>
    </div>
</div>
<div class="top-navigation-container">
    <div class="row">
        <ul class="top-navigation">
            <li class="menu-item"><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
            <li class="menu-item"><a href="<?php echo base_url('huong-dan-dat-hang-p1'); ?>">Hướng dẫn đặt hàng</a></li>
            <li class="menu-item"><a href="<?php echo base_url('quy-dinh-dat-hang-p2'); ?>">Quy định đặt hàng</a></li>
            <li class="menu-item"><a href="<?php echo base_url('bang-gia-p3'); ?>">Bảng giá</a></li>
            <li class="menu-item"><a href="<?php echo base_url('gioi-thieu-p4'); ?>">Giới thiệu</a></li>
            <li class="menu-item"><a href="<?php echo base_url('blog-c1'); ?>">Blog</a></li>
            <li class="menu-item"><a href="<?php echo base_url('tuyen-dung-p5'); ?>">Tuyển dụng</a></li>
            <li class="menu-item"><a href="<?php echo base_url('lien-he-p6'); ?>">Liên hệ</a></li>
        </ul>
    </div>
    <a href="javascript:void(0)" class="home-icon"></a>
</div>
<!--<div class="nh-description">
    <div class="row nh-description-container">
        <ul class="list-description">
            <li class="description-item-1">
				<span class="description-item-content">
					<span class="description-hight-light-text">DỄ DÀNG</span>
					TẠO VÀ QUẢN LÝ ĐƠN HÀNG, TÌM NGUỒN HÀNG,
					<span class="description-hight-light-text">TƯ VẤN MIỄN PHÍ</span>
				</span>
            </li>
            <li class="description-item-2">
				<span class="description-item-content">
					<span class="description-hight-light-text">ĐẢM BẢO 100% </span>
					HÀNG HÓA, ĐỀN BÙ KHI CÓ SAI SÓT, THẤT LẠC
				</span>
            </li>
            <li class="description-item-3">
				<span class="description-item-content">
					GIAO HÀNG TẬN NƠI,<span class="description-hight-light-text"> NHANH CHÓNG </span>DÙ ĐƠN HÀNG <span class="description-hight-light-text">CHỈ 1 SẢN PHẨM</span>
				</span>
            </li>
        </ul>
    </div>
</div>-->