<?php $this->load->view('site/includes/header'); ?>
<div class="row main-content" id="main-content">
    <style>
        .pagination {
            margin-top: 20px;
            text-align: right;
        }

        .pagination li {
            border: 1px solid #CFD1D2 !important;

        }

        .pagination .active {
            background-color: #EDF0F4;
        }

        .post-title a:hover {
            text-decoration: underline;
        }
    </style>
    <div class="nh-breadcrumb">
        <ul class="nh-breadcrumb-list">
            <ul class="nh-breadcrumb-list">
                <li class="nh-breadcrumb-item"><a href="<?php echo base_url(''); ?>">Trang chủ</a></li>
                <li class="nh-breadcrumb-item"><a href="<?php echo base_url('blog-c1'); ?>">Blog</a></li>
            </ul>
        </ul>
    </div>
    <span class="nh-divide"></span>

    <div style="clear: both"></div>
    <div class="blog-pages">
        <?php $this->load->view('site/includes/sidebar-left'); ?>
        <div class="blog-pages-right">
            <div class="blog-list">
                <?php foreach($listArticles as $a){ ?>
                    <div class="blog-item">
                        <a href="<?php echo base_url($a['ArticleSlug'].'-p'.$a['ArticleId']); ?>">
                            <img src="<?php echo IMAGE_PATH.$a['ArticleImage']; ?>" class="blog-img" alt="<?php echo $a['ArticleTitle']; ?>">
                        </a>
                        <div class="blog-title"><a href="<?php echo base_url($a['ArticleSlug'].'-p'.$a['ArticleId']); ?>"><?php echo $a['ArticleTitle']; ?></a></div>
                        <div class="blog-content"><?php echo $a['ArticleLead']; ?></a></div>
                        <div style="clear: both"></div>
                    </div>
                <?php } ?>
                <?php if($pageCount > 1){
                    echo '<ul class="pagination">';
                    if($pageId > 2){
                        echo '<li class="prev"><a href="'.$categoryUrl.($pageId - 1).'"><span><<</span></a></li>';
                        if($pageId == 1) echo '<li class="active"><a href="javascript:vopid(0)">1</a></li>';
                        else echo '<li><a href="'.$categoryUrl.'1">1</a></li>';
                    }
                    $start = ($pageId > 2) ? ($pageId - 1) : 1;
                    if($start > 3) echo '<li><a href="javascript:vopid(0)">...</a></li>';
                    for($i = $start; $i < $pageId + 3 && $i <= $pageCount; $i++) {
                        if($i == $pageId) echo '<li class="active"><a href="javascript:vopid(0)">'.$i.'</a></li>';
                        else echo '<li><a href="'.$categoryUrl.$i.'">'.$i.'</a></li>';
                    }
                    if ($pageId + 3 < $pageCount) {
                        echo '<li><a href="javascript:vopid(0)">...</a></li>';
                        echo '<li><a href="javascript:void(0)">'.$categoryUrl.$pageCount.'</a></li>';
                    }
                    if ($pageId < $pageCount) echo '<li class="last"><a href="'.$categoryUrl.$pageCount.'"><span>>></span></a></li>';
                    echo '</ul>';
                } ?>
            </div>
            <?php $this->load->view('site/includes/sidebar-right'); ?>
        </div>
        <div style="clear: both"></div>
    </div>
</div>
<?php $this->load->view('site/includes/footer'); ?>