<?php $this->load->view('site/includes/header'); ?>
<div id="home-slide" class="orbit home-slide" role="region" data-orbit="xjg8hf-orbit">
    <div class="owl-carousel">
        <?php foreach($listSliders as $s){ ?>
            <div class="slider-item">
                <a href="<?php echo empty($s['SliderLink']) ? 'javascript:void(0)' : $s['SliderLink']; ?>"><img class="home-slide-img" src="<?php echo IMAGE_PATH.$s['SliderImage']; ?>" alt="<?php echo $s['SliderName']; ?>"></a>
            </div>
        <?php } ?>
    </div>
</div>
<div class="row main-content" id="main-content">
    <span class="homepage-divide"></span>
    <ul class="order-step">
        <li class="step-create-order">
            <span class="step-text text-above">Tạo</span>
            <span class="step-text text-below">Đơn hàng</span>
        </li>
        <li class="step-send-order">
            <span class="step-text text-above">Gửi</span>
            <span class="step-text text-below">Đơn hàng</span>
        </li>
        <li class="step-check-order">
            <span class="step-text text-above">NH365</span>
            <span class="step-text text-below">Báo giá</span>
        </li>
        <li class="step-make-payment-order">
            <span class="step-text text-above">Chốt đơn</span>
            <span class="step-text text-below">hàng</span>
        </li>
        <li class="step-confirm-order">
            <span class="step-text text-above">Thanh toán</span>
        </li>
        <li class="step-delivery-order">
            <span class="step-text text-above">Nhận hàng</span>
            <span class="step-text text-below">tại nhà</span>
        </li>
    </ul>
    <div class="origin-block" style="padding-bottom: 20px; margin-bottom: 20px; display: none">
        <span style="color: #D33319; font-size: 16px; font-weight: bold;">THÔNG BÁO:</span>
        Do tình trạng tắc biên nên hàng hoá sẽ về chậm hơn bình thường 2-3 ngày. Rất mong quý khách hàng thông cảm!
    </div>
    <div class="origin-block">
        <div class="origin-header">
			<span class="origin-title">
				<i>Đặt hàng từ</i>
				<a href="http://taobao.com/" target="_blank"><strong>Taobao.com</strong></a>
			</span>
        </div>
        <ul class="list-category owl-carousel">
            <?php foreach ($listProducts as $p) {
                if($p['CategoryTypeId'] == 3){ ?>
                    <li class="owl-item">
                        <a href="https://world.taobao.com/search/search.htm?_input_charset=utf-8&q=<?php echo urlencode($p['CategoryNameTQ']); ?>" class="image-link" target="_blank">
                            <img src="<?php echo IMAGE_PATH .  $p['CategoryImage']; ?>" alt="<?php echo $p['CategoryName']; ?>">
                            <span class="category-title"><?php echo $p['CategoryName']; ?></span>
                        </a>
                        <a class="view-more" href="https://world.taobao.com/search/search.htm?_input_charset=utf-8&q=<?php echo urlencode($p['CategoryNameTQ']); ?>" target="_blank">xem thêm</a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
        <div class="search-block">
            <form action="<?php echo base_url('product/redirect'); ?>" method="get" target="_blank" accept-charset="utf-8">
                <input type="hidden" class="origin-keyword" name="ShopId" value="1">
                <input type="text" class="origin-keyword" name="ProductName" placeholder="Nhập tên sản phẩm tiếng Việt để tìm kiếm từ Taobao.com">
                <button type="submit" class="origin-search-btn"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>
    <div class="origin-block">
        <div class="origin-header">
			<span class="origin-title">
				<i>Đặt hàng từ</i>
				<a href="https://www.tmall.com/" target="_blank"><strong>Tmall.com</strong></a>
			</span>
        </div>
        <ul class="list-category owl-carousel">
            <?php foreach ($listProducts as $p) {
                if($p['CategoryTypeId'] == 4){ ?>
                    <li class="owl-item">
                        <a href="https://list.tmall.com/search_product.htm?_input_charset=utf-8&q=<?php echo urlencode($p['CategoryNameTQ']); ?>" class="image-link" target="_blank">
                            <img src="<?php echo IMAGE_PATH .  $p['CategoryImage']; ?>" alt="<?php echo $p['CategoryName']; ?>">
                            <span class="category-title"><?php echo $p['CategoryName']; ?></span>
                        </a>
                        <a class="view-more" href="https://list.tmall.com/search_product.htm?_input_charset=utf-8&q=<?php echo urlencode($p['CategoryNameTQ']); ?>" target="_blank">xem thêm</a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
        <div class="search-block">
            <form action="<?php echo base_url('product/redirect'); ?>" method="get" target="_blank" accept-charset="utf-8">
                <input type="hidden" class="origin-keyword" name="ShopId" value="2">
                <input type="text" class="origin-keyword" name="ProductName" placeholder="Nhập tên sản phẩm tiếng Việt để tìm kiếm từ Tmall.com">
                <button type = "submit" class="origin-search-btn"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>
    <div class="origin-block">
        <div class="origin-header">
			<span class="origin-title">
				<i>Đặt hàng từ</i>
				<a href="http://1688.com/" target="_blank"><strong>1688.com</strong></a>
			</span>
        </div>
        <ul class="list-category owl-carousel">
            <?php foreach ($listProducts as $p) {
                if($p['CategoryTypeId'] == 5){ ?>
                    <li class="owl-item">
                        <a href="https://s.1688.com/selloffer/offer_search.htm?_input_charset=utf-8&keywords=<?php echo urlencode($p['CategoryNameTQ']); ?>" class="image-link" target="_blank">
                            <img src="<?php echo IMAGE_PATH .  $p['CategoryImage']; ?>" alt="<?php echo $p['CategoryName']; ?>">
                            <span class="category-title"><?php echo $p['CategoryName']; ?></span>
                        </a>
                        <a class="view-more" href="https://s.1688.com/selloffer/offer_search.htm?_input_charset=utf-8&keywords=<?php echo urlencode($p['CategoryNameTQ']); ?>" target="_blank">xem thêm</a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
        <div class="search-block">
            <form action="<?php echo base_url('product/redirect'); ?>" method="get" target="_blank" accept-charset="utf-8">
                <input type="hidden" class="origin-keyword" name="ShopId" value="3">
                <input type="text" class="origin-keyword" name="ProductName" placeholder="Nhập tên sản phẩm tiếng Việt để tìm kiếm từ 1688.com">
                <button type = "submit" class="origin-search-btn"><i class="fa fa-search"></i></button>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('site/includes/footer'); ?>