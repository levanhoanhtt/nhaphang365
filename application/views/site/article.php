<?php $this->load->view('site/includes/header'); ?>
<div class="row main-content" id="main-content">
    <div class="nh-breadcrumb">
        <ul class="nh-breadcrumb-list">
            <li class="nh-breadcrumb-item"><a href="<?php echo base_url(''); ?>">Trang chủ</a></li>
            <!--<li class="nh-breadcrumb-item"><a href="/blog">Blog</a></li>-->
        </ul>
    </div>
    <span class="nh-divide"></span>
    <div style="clear: both"></div>
    <div class="blog-pages">
        <div class="blog-pages-right blog-page" style="width: 100%;">
            <div class="blog-list" style="width: 100% !important;padding-right: 246px !important;min-height: 600px;">
                <h1 style="text-align: center;margin:0 10px 15px 10px;"><?php echo $article['ArticleTitle']; ?></h1>
                <div class="contentx"><?php echo $article['ArticleContent']; ?></div>
            </div>
            <style>
                .block-post {
                    padding-top: 10px;
                    padding-right: 10px;
                }
                .post-title a {
                    color: #4f4f4f;
                    font-weight: 500;
                    width: 100%;
                    line-height: 20px;
                    display: block;
                }

                .post-title a:hover {
                    text-decoration: underline;
                }

                .neo {
                    position: fixed;
                    width: 195px;
                    top:46px;
                }
                .block-neo {
                    overflow: hidden;
                }
                .btn-blog-order, .install-extension-btn{
                    color: #434343 !important;
                }
                .btn-blog-order:hover, .install-extension-btn:hover{
                    color: #ca341f !important;
                }
                .install-extension-btn{
                    display: block;
                    width: 164px;
                    height: 35px;
                    text-align: center;
                    line-height: 35px;
                    border: 1px solid #ceceda;
                    margin-top: 5px;
                    box-shadow: 2px 2px 2px #888;
                    font-size: 13px;
                }
                .more-option-text{
                    display: block;
                    width: 164px;
                    font-style: italic;
                    font-size: 14px;
                    color: #555;
                    text-align: center;
                    font-weight: 500;
                    margin-top: 5px;
                }
            </style>
            <?php $this->load->view('site/includes/sidebar-right'); ?>
        </div>
        <div style="clear: both"></div>
    </div>
</div>
<?php $this->load->view('site/includes/footer'); ?>