<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('shop/paidDone'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="ShopNo" class="form-control" value="<?php echo set_value('ShopNo'); ?>" placeholder="Số thứ tự">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listOrders, 'OrderId', 'OrderCode', 'OrderId', set_value('OrderId'), true, 'Đơn hàng', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listOrderAccounts, 'OrderAccountId', 'OrderAccountName', 'OrderAccountId', set_value('OrderAccountId'), true, 'Tài khoản đặt hàng', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listOrderUsers, 'UserId', 'FullName', 'OrderUserId', set_value('OrderUserId'), true, 'Nhân viên đặt hàng', ' select2'); ?>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <style>
                    #tbodyShopNumber .tdPadding{text-align: center;vertical-align: middle;}
                </style>
                <div class="box box-success">
                    <?php sectionTitleHtml($title, isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Số thự tự</th>
                                <th>Ngày đặt</th>
                                <th>Đơn hàng</th>
                                <th>Tổng</th>
                                <th>Giá thanh toán</th>
                                <th>Giá thanh toán 2</th>
                                <th>Tài khoản</th>
                                <th>Ghi chú</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyShopNumber">
                            <?php $shopCodes = array();
                            $sumCosts = array();
                            foreach($listShopnNumbers as $sn){
                                $shopId = $sn['ShopId'];
                                if(!isset($shopCodes[$shopId])) $shopCodes[$shopId] = $this->Mshops->getFieldValue(array('ShopId' => $shopId), 'ShopCode');
                                if(!isset($sumCosts[$shopId])) $sumCosts[$shopId] = $this->Mshops->getSumCost($shopId); ?>
                                <tr id="shopNumber_<?php echo $sn['ShopNumberId']; ?>" data-id="<?php echo $shopId; ?>">
                                    <td><?php echo $sn['ShopNo']; ?></td>
                                    <td class="tdPadding tdMerge" rowspan="1"><?php echo ddMMyyyy($this->Mconstants->getObjectValue($listOrders, 'OrderId', $sn['OrderId'], 'OrderDate')); ?></td>
                                    <td class="tdPadding tdMerge" rowspan="1"><a href="<?php echo base_url('order/edit/'.$sn['OrderId'].'/0/0/'.$shopCodes[$shopId]); ?>" target="_blank"><?php echo $this->Mconstants->getObjectValue($listOrders, 'OrderId', $sn['OrderId'], 'OrderCode'); ?> - <?php echo $shopCodes[$shopId]; ?></a></td>
                                    <td class="tdPadding tdMerge" rowspan="1"><?php echo priceFormat($sumCosts[$shopId], true); ?></td>
                                    <td><?php echo priceFormat($sn['ShopCost'], true); ?></td>
                                    <td><?php echo priceFormat($sn['ShopCostAdmin'], true); ?></td>
                                    <td><?php echo $this->Mconstants->getObjectValue($listOrderAccounts, 'OrderAccountId', $sn['OrderAccountId'], 'OrderAccountName'); ?></td>
                                    <td><?php echo $sn['Comment']; ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>