<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('shop/paid'); ?>
                        <div class="row">
                            <div class="col-sm-3">
                                <input type="text" name="ShopNo" class="form-control" value="<?php echo set_value('ShopNo'); ?>" placeholder="Số thứ tự">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listOrders, 'OrderId', 'OrderCode', 'OrderId', set_value('OrderId'), true, 'Đơn hàng', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listOrderAccounts, 'OrderAccountId', 'OrderAccountName', 'OrderAccountId', set_value('OrderAccountId'), true, 'Tài khoản đặt hàng', ' select2'); ?>
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectObject($listOrderUsers, 'UserId', 'FullName', 'OrderUserId', set_value('OrderUserId'), true, 'Nhân viên đặt hàng', ' select2'); ?>
                            </div>
                        </div>
                        <div class="form-group text-right">
                            <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            <input type="text" hidden="hidden" name="PageId" id="pageId" value="">
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <style>
                    #shopNumberCount{font-weight: bold;color: red;font-size: 22px;}
                    #tbodyShopNumber td{padding: 0 !important;}
                    #tbodyShopNumber td input{border: none !important;}
                    #tbodyShopNumber .tdPadding{text-align: center;vertical-align: middle;}
                </style>
                <div class="box box-success">
                    <?php sectionTitleHtml('<span id="shopNumberCount">'.priceFormat($shopNumberCount).'</span> Số thứ tự cần thanh toán', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>Số thự tự</th>
                                <th>Ngày đặt</th>
                                <th>Đơn hàng</th>
                                <th>Tổng</th>
                                <th>Giá thanh toán</th>
                                <th>Giá thanh toán 2</th>
                                <th>Tài khoản</th>
                                <th>Ngân hàng</th>
                                <th>Ghi chú</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyShopNumber">
                            <?php $shopCodes = array();
                            $sumCosts = array();
                            foreach($listShopnNumbers as $sn){
                                $shopId = $sn['ShopId'];
                                if(!isset($shopCodes[$shopId])) $shopCodes[$shopId] = $this->Mshops->getFieldValue(array('ShopId' => $shopId), 'ShopCode');
                                if(!isset($sumCosts[$shopId])) $sumCosts[$shopId] = $this->Mshops->getSumCost($shopId); ?>
                                <tr id="shopNumber_<?php echo $sn['ShopNumberId']; ?>" data-id="<?php echo $shopId; ?>">
                                    <td><input type="text" class="form-control" id="shopNo_<?php echo $sn['ShopNumberId']; ?>" value="<?php echo $sn['ShopNo']; ?>"></td>
                                    <td class="tdPadding tdMerge" rowspan="1"><?php echo ddMMyyyy($this->Mconstants->getObjectValue($listOrders, 'OrderId', $sn['OrderId'], 'OrderDate')); ?></td>
                                    <td class="tdPadding tdMerge" rowspan="1"><a href="<?php echo base_url('order/edit/'.$sn['OrderId'].'/0/0/'.$shopCodes[$shopId]); ?>" target="_blank"><?php echo $this->Mconstants->getObjectValue($listOrders, 'OrderId', $sn['OrderId'], 'OrderCode'); ?> - <?php echo $shopCodes[$shopId]; ?></a></td>
                                    <td class="tdPadding tdMerge" rowspan="1"><?php echo priceFormat($sumCosts[$shopId], true); ?></td>
                                    <td><input type="text" class="form-control shopCost" id="shopCost_<?php echo $sn['ShopNumberId']; ?>" value="<?php echo priceFormat($sn['ShopCost'], true); ?>"></td>
                                    <td><input type="text" class="form-control shopCost" id="shopCostAdmin_<?php echo $sn['ShopNumberId']; ?>" value="<?php echo priceFormat($sn['ShopCostAdmin'], true); ?>"></td>
                                    <td><?php echo $this->Mconstants->selectObject($listOrderAccounts, 'OrderAccountId', 'OrderAccountName', 'OrderAccountId_'.$sn['ShopNumberId'], $sn['OrderAccountId']); ?></td>
                                    <td><?php echo $this->Mconstants->selectObject($listBankAccounts, 'BankAccountId', 'BankAccountName', 'BankAccountId_'.$sn['ShopNumberId'], $sn['BankAccountId']); ?></td>
                                    <td><input type="text" class="form-control" id="comment_<?php echo $sn['ShopNumberId']; ?>" value="<?php echo $sn['Comment']; ?>"></td>
                                    <td class="actions tdPadding">
                                        <button class="btn btn-primary btn-sm btnUpdate" data-id="<?php echo $sn['ShopNumberId']; ?>">Thanh toán</button>
                                        <button class="btn btn-danger btn-sm btnDelete" data-id="<?php echo $sn['ShopNumberId']; ?>">Xóa</button>
                                        <input type="text" hidden="hidden" id="orderId_<?php echo $sn['ShopNumberId']; ?>" value="<?php echo $sn['OrderId']; ?>">
                                        <input type="text" hidden="hidden" id="returnCost_<?php echo $sn['ShopNumberId']; ?>" value="<?php echo $sn['ReturnCost']; ?>">
                                        <input type="text" hidden="hidden" id="isReturn_<?php echo $sn['ShopNumberId']; ?>" value="<?php echo $sn['IsReturn']; ?>">
                                        <input type="text" hidden="hidden" id="reason_<?php echo $sn['ShopNumberId']; ?>" value="<?php echo $sn['Reason']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="updateShopNumberUrl" value="<?php echo base_url('shop/updateShopNumber'); ?>">
                    <input type="text" hidden="hidden" id="deleteShopNumberUrl" value="<?php echo base_url('shop/deleteShopNumber'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>