<?php $this->load->view('includes/customer/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-sm-3">
                        <?php $this->load->view('includes/customer/sidebar'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="box box-default padding15">
                            <?php echo form_open('user/updateProfile', array('id' => 'profileForm')); ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php $avatar = (empty($user['Avatar']) ? NO_IMAGE : $user['Avatar']); ?>
                                        <img src="<?php echo USER_PATH.$avatar; ?>" class="chooseImage" id="imgAvatar" style="width: 150px;height: 150px;display: block;">
                                        <input type="text" hidden="hidden" name="Avatar" id="avatar" value="<?php echo $avatar; ?>">
                                    </div>
                                </div>
                                <div class="col-sm-9">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2 text-left">Họ và tên <span class="required">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="FullName" class="form-control hmdrequired" value="<?php echo $user['FullName']; ?>" data-field="Họ và tên">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2 text-left">Di động <span class="required">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="PhoneNumber" id="phoneNumber" class="form-control hmdrequired" value="<?php echo $user['PhoneNumber']; ?>" data-field="Di động">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2 text-left">Email/ FB</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="Email" class="form-control" value="<?php echo $user['Email']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box box-default">
                                <div class="box-header">
                                    <h3 class="box-title">Thông tin bổ sung</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Giới tính</label>
                                                <?php $this->Mconstants->selectConstants('genders', 'GenderId', $user['GenderId']); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Chăm sóc khách hàng</label>
                                                <?php $this->Mconstants->selectObject($listCustomerCareStaff, 'UserId', 'FullName', 'CareStaffId', $user['CareStaffId'], true, 'Ngẫu nhiên', ' select2'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Số tài khoản ngân hàng</label>
                                                <input type="text" name="BankAccountNumber" class="form-control" value="<?php echo $bankAccountNumber; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Tên ngân hàng, chi nhánh</label>
                                                <input type="text" name="BankName1" class="form-control" value="<?php echo $bankName1; ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Chủ tài khoản</label>
                                                <input type="text" name="BankAccountName" class="form-control" value="<?php echo $bankAccountName; ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Ngân hàng kinh doanh</label>
                                                <input type="text" name="BankName2" class="form-control" value="<?php echo $bankName2; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <input type="text" hidden="hidden" name="ProvinceId" id="provinceId" value="<?php echo $user['ProvinceId']; ?>">
                                    <input type="text" hidden="hidden" name="Address" class="hmdrequired" id="address" value="<?php echo $user['Address']; ?>" data-field="Địa chỉ">
                                </div>
                            </div>
                            <div class="box box-default">
                                <div class="box-header">
                                    <h3 class="box-title">Địa chỉ giao hàng</h3>
                                </div>
                                <div class="box-body table-responsive no-padding divTable">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th style="width: 20%">Họ tên</th>
                                            <th style="width: 20%">Số điện thoại</th>
                                            <th>Địa chỉ nhận hàng</th>
                                            <th>Tỉnh/ Thành phố</th>
                                            <th style="width: 10%"></th>
                                        </tr>
                                        </thead>
                                        <tbody id="tbodyContact">
                                        <?php for($i=1;$i<=3;$i++){ ?>
                                           <tr id="contact_<?php echo $i; ?>"<?php if($i==1) echo ' class="bg-info"'; ?>>
                                               <td><input type="text" id="fullName<?php echo $i; ?>" name="FullName<?php echo $i; ?>" class="form-control" value="<?php echo ($i == 1) ? $fullName0 : (($i == 2) ? $fullName1 : $fullName2); ?>"></td>
                                               <td><input type="text" id="phoneNumber<?php echo $i; ?>" name="PhoneNumber<?php echo $i; ?>" class="form-control phoneNumberContact<?php if($i==1) echo ' phoneNumberCurent'; ?>" value="<?php echo ($i == 1) ? $user['PhoneNumber'] : (($i == 2) ? $phoneNumber1 : $phoneNumber2); ?>"></td>
                                               <td><input type="text" id="address<?php echo $i; ?>" name="Address<?php echo $i; ?>" class="form-control addressContact<?php if($i==1) echo ' addressCurent'; ?>" value="<?php echo ($i == 1) ? $user['Address'] : (($i == 2) ? $address1 : $address2); ?>"></td>
                                               <td><?php $this->Mconstants->selectObject($listProvinces, 'ProvinceId', 'ProvinceName', 'ProvinceId'.$i, ($i == 1) ? $user['ProvinceId'] : (($i == 2) ? $provinceId1 : $provinceId2), false, '', ' select2'.(($i==1) ? ' provinceCurent' : '')); ?></td>
                                               <td>
                                                   <input type="radio" name="ContactDefaultIndex" value="<?php echo $i; ?>" class="iCheck"<?php if($i==1) echo ' checked="checked"'; ?>>
                                                   <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $i; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                               </td>
                                           </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="box-footer">
                                    <button class="btn btn-info pull-right" type="button" id="btnAddContact">Thêm địa chỉ</button>
                                </div>
                            </div>
                            <div class="box box-default">
                                <div class="box-header">
                                    <h3 class="box-title">Thông tin đăng nhập</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Mật khẩu cũ</label>
                                                <input type="password" name="UserPass" id="userPass" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Mật khẩu mới</label>
                                                <input type="password" id="newPass" name="NewPass" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Gõ lại Mật khẩu</label>
                                                <input type="password" id="rePass" name="RePass" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                                <input type="text" name="UserName" id="userName" hidden="hidden" value="<?php echo $user['UserName']; ?>">
                                <input type="text" id="roleId" hidden="hidden" value="<?php echo $user['RoleId']; ?>">
                            </div>
                            <?php echo form_close(); ?>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>