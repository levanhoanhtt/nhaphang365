<?php $this->load->view('includes/customer/header'); ?>
    <div class="content-wrapper order-page">
        <?php $shopCodes = array(); ?>
        <div class="container-fluid">
            <section class="content">
                <?php $noProductImage = IMAGE_PATH . NO_PRODUCT; ?>
                <div class="box box-default">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div id="logo" class="pull-left">
                                    <img class="img-responsive" src="assets/vendor/dist/img/logo.png" alt="logo" width="150">
                                </div>
                                <fieldset>
                                    <legend>Thông tin khách hàng</legend>
                                    <div class="border1">
                                        <table class="table no-padding">
                                            <tbody>
                                            <tr>
                                                <td class="first">Họ tên</td>
                                                <td><input type="text" value="<?php echo $orderDraff['FullName']; ?>" id="fullName" class="form-control hmdrequired" data-field="Họ tên"></td>
                                            </tr>
                                            <tr>
                                                <td class="first">Di động</td>
                                                <td class="tdPhone"><input type="text" value="<?php echo $orderDraff['PhoneNumber']; ?>" id="phoneNumber" class="form-control hmdrequired" data-field="Di động"></td>
                                            </tr>
                                            <tr>
                                                <td class="first">Email</td>
                                                <td><input type="text" value="<?php echo $orderDraff['Email']; ?>" id="email" class="form-control"></td>
                                            </tr>
                                            <tr>
                                                <td class="first">Địa chỉ</td>
                                                <td><input type="text" value="<?php echo $orderDraff['Address']; ?>" id="address" class="form-control hmdrequired" data-field="Địa chỉ"></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-7">
                                <fieldset>
                                    <legend>
                                        Thông tin thanh toán
                                        <span class="pull-right" style="font-size: 15px; font-style: italic; line-height: 32px;">Tỷ giá: <i class="text-bold"><?php echo priceFormat($exchangeRate); ?></i></span>
                                    </legend>
                                    <div class="row">
                                        <div class="col-md-6 border1">
                                            <table class="table no-padding">
                                                <tbody>
                                                <tr>
                                                    <td class="first"><b>Tổng giá sản phẩm (Tệ)</b></td>
                                                    <td><input type="text" value="0" class="form-control cost1" disabled="disabled" id="costTQ"></td>
                                                </tr>
                                                <tr>
                                                    <td class="first"><b>Tổng giá sản phẩm (VNĐ)</b></td>
                                                    <td><input type="text" value="0" class="form-control cost" disabled="disabled" id="costVN"></td>
                                                </tr>
                                                <tr>
                                                    <td class="first"><b>Phí dịch vụ</b></td>
                                                    <td><input type="text" value="0" class="form-control cost" disabled="disabled" id="serviceFee">

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="first"><b>Chi phí phát sinh (VNĐ)</b></td>
                                                    <td><input type="text" value="0" class="form-control cost" disabled="disabled" id="costFee"></td>
                                                </tr>
                                                <tr>
                                                    <td class="first"><b>Tổng giá thanh toán (VNĐ)</b></td>
                                                    <td><input type="text" value="0" class="form-control cost" disabled="disabled" id="sumCost"></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="col-md-6">
                                            <table class="table no-padding">
                                                <tbody>
                                                <tr>
                                                    <td colspan="2">
                                                        <a href="javascript:void(0)" class="btn btn-info" id="aImportExcel" style="margin-bottom: 5px;">Excel Check hàng</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <textarea class="form-control" id="comment" rows="4" placeholder="Ghi chú đơn hàng"><?php echo $orderDraff['Comment']; ?></textarea>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="first">Nhân viên CSKH</td>
                                                    <td><?php $this->Mconstants->selectObject($listCustomerCareStaff, "UserId", "FullName", "CareStaffId", $orderDraff['CareStaffId'], true, "Chọn Nhân viên", " select2"); ?></td>
                                                </tr>
                                                <tr>
                                                    <td class="first">Kho</td>
                                                    <td><?php $this->Mconstants->selectObject($listWarehouses, "WarehouseId", "WarehouseName", "WarehouseId", $orderDraff['WarehouseId']); ?></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="alert alert-warning hmd-alert">
                            <strong>Công thức tính giá: Giá (về tận nhà HN) = (Giá web + phí vận chuyển TQ)*tỷ giá + phí dịch vụ (1) + phí vận chuyển TQ-VN (2)</strong>
                            <span class="pull-right">Hỗ trợ giao hàng free ship nội thành Hà Nội - Sài Gòn</span>
                        </div>
                        <div class="order-container">
                            <ul class="timeline has-img-82">
                                <?php $listNoShopProducts = array();
                                foreach($listShops as $shopCode => $listProducts){
                                    if($shopCode == 'NoShopHMD') $listNoShopProducts = $listProducts;
                                    else{
                                        $shopCodes[] = $shopCode; ?>
                                        <li id="li_<?php echo $shopCode; ?>">
                                            <i class="fa fa-shopping-basket"></i>
                                            <div class="timeline-item">
                                                <div class="timeline-header">
                                                    <ul class="list-inline">
                                                        <?php $shopUrl = $listProducts[0]['ShopUrl']; ?>
                                                        <li><span class="text-bold">Mã shop: </span><a href="<?php echo $shopUrl ?>" id="shopUrl_<?php echo $shopCode; ?>" target="_blank"><?php echo $shopCode; ?></a></li>
                                                        <li><span class="text-bold">Tên shop: </span><a href="<?php echo $shopUrl ?>" target="_blank"><span id="shopName_<?php echo $shopCode; ?>"><?php echo $listProducts[0]['ShopName']; ?></span></a></li>
                                                        <li><?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId_'.$shopCode, $listProducts[0]['CategoryId'], true, "Chọn chuyên mục", " select2"); ?></li>
                                                    </ul>
                                                </div>
                                                <div class="timeline-body">
                                                    <div class="table-responsive no-padding divTable">
                                                        <table class="table">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 105px;">Ảnh</th>
                                                                <th style="width: 250px;">Link sản phẩm</th>
                                                                <th style="width: 100px;">Màu sắc</th>
                                                                <th style="width: 100px;">Kích thước</th>
                                                                <th style="width: 100px;">Số lượng</th>
                                                                <th style="width: 100px;">Đơn giá(¥)</th>
                                                                <th style="width: 100px;">Giá(¥)</th>
                                                                <th style="width: 100px;">Ship nội địa</th>
                                                                <th>Ghi chú</th>
                                                                <th style="padding-right: 15px;width: 50px;"></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="tbodyProduct" id="tbodyProduct_<?php echo $shopCode; ?>">
                                                            <?php $row = $rowImage = 0; $productLink = $productLinkOld = $productImage = '';
                                                            foreach($listProducts as $p){
                                                                $rowImage++;
                                                                $productLink = $p['ProductLink'];
                                                                $productImage = getFileUrl(PRODUCT_PATH, $p['ProductImage'], $noProductImage);
                                                                if($productLink != $productLinkOld) $row++;
                                                                $productLinkOld = $productLink; ?>
                                                                <tr data-product="<?php echo $row; ?>" data-shop="<?php echo $shopCode; ?>" data-product-id="0" data-status="2" class="tr_product tr_product_status_2">
                                                                    <td><img class="productImage" id="productImage_<?php echo $rowImage; ?>_<?php echo $shopCode; ?>" src="<?php echo $productImage; ?>" data-id="<?php echo $rowImage; ?>" data-shop="<?php echo $shopCode; ?>"></td>
                                                                    <td class="tdProductName" rowspan="1">
                                                                        <a target="_blank" href="<?php echo $p['ProductLink']; ?>" class="linkproductName"><?php echo $productLink; ?></a>
                                                                        <textarea rows="3" class="form-control productName"></textarea>
                                                                    </td>
                                                                    <td class="tdAttr"><input type="text" class="form-control productColor" value="<?php echo $p['Color']; ?>"><i class="fa fa-plus link_add_product"></i></td>
                                                                    <td class="tdAttr"><input type="text" class="form-control productSize" value="<?php echo $p['Size']; ?>"><i class="fa fa-plus link_add_product"></i></td>
                                                                    <td><input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="<?php echo $p['Quantity']; ?>"></td>
                                                                    <td class="tdCost"><input type="text" class="form-control cost1 gia hmdrequired hmdrequiredCost1" data-field="Giá sản phẩm" value="<?php echo priceFormat($p['Cost'], true); ?>" ></td>
                                                                    <td><input type="text" class="form-control cost1 thanhtien" value="<?php echo priceFormat($p['Quantity'] * $p['Cost'], true); ?>" disabled></td>
                                                                    <?php if($rowImage == 1){
                                                                        $rowspan = count($listProducts); ?>
                                                                        <td class="tdShipTQ" rowspan="<?php echo $rowspan; ?>"><input type="text" disabled value="?" class="form-control cost1 shipTQ" style="height: <?php echo $rowspan * 82; ?>px;"></td>
                                                                    <?php } ?>
                                                                    <td><textarea rows="3" class="form-control comment cmt"><?php echo $p['Comment']; ?></textarea></td>
                                                                    <td><i class="fa fa-trash-o link_delete"></i></td>
                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                        <p><button class="btn btn-info btn-add-product" data-id="<?php echo $shopCode; ?>">Thêm sản phẩm</button></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    <?php }
                                }
                                if(!empty($listNoShopProducts)){
                                    $shopCode = 'NoShopHMD';
                                    $shopCodes[]= $shopCode; ?>
                                    <li id="li_<?php echo $shopCode; ?>">
                                        <i class="fa fa-shopping-basket"></i>
                                        <div class="timeline-item">
                                            <div class="timeline-header">
                                                <ul class="list-inline">
                                                    <li><span class="text-bold">Mã shop: </span></li>
                                                    <li><span class="text-bold">Tên shop: </span><li><span class="text-bold">Tên shop: </span><span id="shopName_<?php echo $shopCode; ?>"></span></li></li>
                                                    <li><?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId_'.$shopCode, $listProducts[0]['CategoryId'], true, "Chọn chuyên mục", " select2"); ?></li>
                                                </ul>
                                            </div>
                                            <div class="timeline-body">
                                                <div class="table-responsive no-padding divTable">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 105px;">Ảnh</th>
                                                            <th style="width: 250px;">Link sản phẩm</th>
                                                            <th style="width: 100px;">Màu sắc</th>
                                                            <th style="width: 100px;">Kích thước</th>
                                                            <th style="width: 100px;">Số lượng</th>
                                                            <th style="width: 100px;">Đơn giá(¥)</th>
                                                            <th style="width: 100px;">Giá(¥)</th>
                                                            <th style="width: 100px;">Ship nội địa</th>
                                                            <th>Ghi chú</th>
                                                            <th style="padding-right: 15px;width: 50px;"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="tbodyProduct" id="tbodyProduct_<?php echo $shopCode; ?>">
                                                        <?php $row = $rowImage = 0; $productLink = $productLinkOld = $productImage = '';
                                                        foreach($listProducts as $p){
                                                            $rowImage++;
                                                            $productLink = $p['ProductLink'];
                                                            $productImage = getFileUrl(PRODUCT_PATH, $p['ProductImage'], $noProductImage);
                                                            if($productLink != $productLinkOld) $row++;
                                                            $productLinkOld = $productLink; ?>
                                                            <tr data-product="<?php echo $row; ?>" data-shop="<?php echo $shopCode; ?>" data-product-id="0" data-status="2" class="tr_product tr_product_status_2">
                                                                <td><img class="productImage" id="productImage_<?php echo $rowImage; ?>_<?php echo $shopCode; ?>" src="<?php echo $productImage; ?>" data-id="<?php echo $rowImage; ?>" data-shop="<?php echo $shopCode; ?>"></td>
                                                                <td class="tdProductName" rowspan="1">
                                                                    <a target="_blank" href="<?php echo $p['ProductLink']; ?>" class="linkproductName"><?php echo $productLink; ?></a>
                                                                    <textarea rows="3" class="form-control productName"></textarea>
                                                                </td>
                                                                <td class="tdAttr"><input type="text" class="form-control productColor" value="<?php echo $p['Color']; ?>"><i class="fa fa-plus link_add_product"></i></td>
                                                                <td class="tdAttr"><input type="text" class="form-control productSize" value="<?php echo $p['Size']; ?>"><i class="fa fa-plus link_add_product"></i></td>
                                                                <td><input type="text" class="form-control cost soluong hmdrequired hmdrequiredCost" data-field="Số lượng sản phẩm" value="<?php echo $p['Quantity']; ?>"></td>
                                                                <td class="tdCost"><input type="text" class="form-control cost1 gia hmdrequired hmdrequiredCost1" data-field="Giá sản phẩm" value="<?php echo priceFormat($p['Cost'], true); ?>" ></td>
                                                                <td><input type="text" class="form-control cost1 thanhtien" value="<?php echo priceFormat($p['Quantity'] * $p['Cost'], true); ?>" disabled></td>
                                                                <?php if($rowImage == 1){
                                                                    $rowspan = count($listProducts); ?>
                                                                    <td class="tdShipTQ" rowspan="<?php echo $rowspan; ?>"><input type="text" disabled value="?" class="form-control cost1 shipTQ" style="height: <?php echo $rowspan * 82; ?>px;"></td>
                                                                <?php } ?>
                                                                <td><textarea rows="3" class="form-control comment"><?php echo $p['Comment']; ?></textarea></td>
                                                                <td><i class="fa fa-trash-o link_delete"></i></td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                    <p><button class="btn btn-info btn-add-product" data-id="<?php echo $shopCode; ?>">Thêm sản phẩm</button></p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                    <div class="box-footer">
                        <ul class="list-inline">
                            <li><button class="btn btn-primary" id="btnSaveOrder">Lưu đơn hàng</button></li>
                            <li><button class="btn btn-primary" id="btnSubmitOrder">Gửi đơn hàng</button></li>
                            <li><button class="btn btn-primary" id="btnMergeOrederDraff">Gộp vào đơn đã có</button></li>
                        </ul>
                    </div>
                </div>
                <!--Begin Modal-->
                <div class="modal fade" id="modalServiceFee" tabindex="-1" role="dialog" aria-labelledby="modalServiceFee">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Bảng phí dịch vụ</h4>
                            </div>
                            <div class="modal-body">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Tổng đơn hàng</th>
                                        <th>Phí dịch vu</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($listServiceFees as $sf) { ?>
                                        <tr>
                                            <td>
                                                <?php if ($sf['EndCost'] == 10000) echo "Từ {$sf['BeginCost']} triệu trở lên";
                                                else echo "Từ {$sf['BeginCost']} - {$sf['EndCost']} triệu"; ?>
                                            </td>
                                            <td><?php echo priceFormat($sf['Percent']); ?> %</td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="modalLoginMsg" tabindex="-1" role="dialog" aria-labelledby="modalLoginMsg">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Thêm đơn hàng thành công</h4>
                            </div>
                            <div class="modal-body">
                                <p class="text-center">Vui lòng <a href="<?php echo base_url('user'); ?>">đăng nhập</a> để xem đơn hàng của bạn</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $this->load->view('includes/order/modal'); ?>
                <!--End Modal-->
                <input type="text" hidden="hidden" id="saveOrderDraffUrl" value="<?php echo base_url('order/saveDraff'); ?>">
                <input type="text" hidden="hidden" id="listOrderUrl" value="<?php echo base_url('customer/order'); ?>">
                <input type="text" hidden="hidden" id="exchangeRate" value="<?php echo $exchangeRate; ?>">
                <input type="text" hidden="hidden" id="servicePercent" value="0" style="display: none;">
                <input type="text" hidden="hidden" id="isFixPercent" value="0">
                <input type="text" hidden="hidden" id="shipTQType" value="0">
                <div class="chat-box">
                    <a href="javascript:void(0)" class="maximizechat">
                        <div class="chat-status-icon"></div>
                    </a>
                    <div class="chat-wrapper">
                        <?php $this->load->view('includes/chat_by_customer'); ?>
                    </div>
                </div>
            </section>
        </div>
        <div class="box-order-shop" style="display: none;">
            <ul class="list-group">
                <?php foreach($shopCodes as $i => $v){ ?>
                    <li title="<?php echo ($v == 'NoShopHMD') ? '' : $v; ?>" data-id="<?php echo $v; ?>"><?php echo $i+1; ?></li>
                <?php } ?>
            </ul>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>