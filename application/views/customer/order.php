<?php $this->load->view('includes/customer/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-sm-3">
                        <?php $this->load->view('includes/customer/sidebar'); ?>
                    </div>
                    <div class="col-sm-9">
                        <ul class="list-inline" id="ulOrderStatus">
                            <li><a href="<?php echo base_url('customer/order'); ?>" class="btn btn-primary">Tất cả (<?php echo $statisticOrderCounts[0]; ?>)</a></li>
                            <?php $orderStatus = $this->Mconstants->orderStatus;
                            foreach($orderStatus as $i => $v){ ?>
                                <li><a href="<?php echo base_url('customer/order/'.$i); ?>" class="btn btn-default"><?php echo $v; ?> (<?php echo $statisticOrderCounts[$i]; ?>)</a></li>
                            <?php } ?>
                        </ul>
                        <div class="box box-default">
                            <div class="box-body row-margin">
                                <?php echo ($orderStatusId > 0) ? form_open('customer/order/'.$orderStatusId) : form_open('customer/order'); ?>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <input type="text" name="OrderCode" class="form-control" value="<?php echo set_value('OrderCode'); ?>" placeholder="Mã đơn hàng">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" name="WaybillCode" class="form-control" value="<?php echo set_value('WaybillCode'); ?>" placeholder="Mã vận đơn">
                                    </div>
                                    <div class="col-sm-6">
                                        <input type="text" name="ProductLink" class="form-control" value="<?php echo set_value('ProductLink'); ?>" placeholder="Link sản phẩm">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <?php $this->Mconstants->selectConstants('complaintStatus', 'ComplaintStatusId', set_value('ComplaintStatusId'), true, 'Trạng thái Khiếu nại'); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Lọc">
                                        <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <style>
                            table i.fa-clone{
                                cursor: pointer;
                                color: red;
                                padding-left: 5px;
                            }
                        </style>
                        <div class="box box-default">
                            <?php if(isset($paggingHtml) && !empty($paggingHtml)) sectionTitleHtml('', $paggingHtml); ?>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mã đơn hàng</th>
                                        <th>Ngày tạo đơn</th>
                                        <th>Ngày chốt đơn</th>
                                        <th>Trạng thái</th>
                                        <th>Tổng shop</th>
                                        <th>Tổng sản phẩm</th>
                                        <th>Tổng tiền (VNĐ)</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $orderStatus = $this->Mconstants->orderStatus;
                                    $labelCss = $this->Mconstants->labelCss;
                                    foreach($listOrders as $o){
                                        $totalCost = $o['OtherCost'];
                                        $productInfos = $this->Mproducts->getListInfo($o['OrderId']);
                                        $paidVN  = $o['ExchangeRate'] * ($productInfos['PaidTQ'] + $productInfos['ShipTQ']);
                                        $serviceCost = round($paidVN * $o['ServicePercent']/100);
                                        $totalCost += $paidVN + $serviceCost; ?>
                                        <tr>
                                            <td>
                                                <a href="<?php echo base_url('chi-tiet-don-hang-'.$o['OrderId']); ?>" id="orderCode_<?php echo $o['OrderId']; ?>"><?php echo $o['OrderCode']; ?></a>
                                                <?php if($o['OrderStatusId'] == 6) echo '<i class="fa fa-clone" title="Nhân bản đơn hàng" data-id="'.$o['OrderId'].'"></i>'; ?>
                                            </td>
                                            <td><?php echo ddMMyyyy($o['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                            <td><?php echo ddMMyyyy($o['CustomerOkDate'], 'd/m/Y H:i'); ?></td>
                                            <td><span class="<?php echo $labelCss[$o['OrderStatusId']]; ?>"><?php echo $orderStatus[$o['OrderStatusId']]; ?></span></td>
                                            <td><?php echo $productInfos['ShopCount']; ?>
                                            </td>
                                            <td><?php echo $productInfos['ProductCount']; ?>
                                            <td><?php echo priceFormat($totalCost); ?></td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                            <input type="text" hidden="hidden" id="cloneOrderUrl" value="<?php echo base_url('order/cloneOrder'); ?>">
                            <input type="text" hidden="hidden" id="listOrderUrl" value="<?php echo base_url('customer/order/1'); ?>">
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>