<?php $this->load->view('includes/customer/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-sm-3">
                        <?php $this->load->view('includes/customer/sidebar'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="box box-default">
                            <div class="box-body row-margin">
                                <?php echo form_open('customer/transaction'); ?>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                            <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                            <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Lọc">
                                        <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="submit" name="export" class="btn btn-primary" value="Xuất Excel">
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <style>
                            #spanBalance{font-weight: bold;color: red;font-size: 22px;}
                            .divTable i.fa{cursor: pointer;}
                            #tbodyTransaction tr.thead{background-color:#f5f5f5;}
                        </style>
                        <div class="box box-default">
                            <?php sectionTitleHtml('Tổng dư nợ <span id="spanBalance">'.$customerBalance.'</span> VNĐ', isset($paggingHtml) ? $paggingHtml : ''); ?>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th><i class="fa fa-caret-square-o-right all"></i></th>
                                        <th>Thời gian</th>
                                        <th>Đơn hàng</th>
                                        <th>Vận chuyển</th>
                                        <th>Đã thanh toán</th>
                                        <th>Ghi chú</th>
                                        <th>Ghi nợ</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyTransaction">
                                    <?php $i = 0;
                                    $status = $this->Mconstants->status;
                                    $status[4] = 'KT đã duyệt';
                                    $labelCss = $this->Mconstants->labelCss;
                                    $transactionRanges = $this->Mconstants->transactionRanges;
                                    $orderCodes = array();
                                    foreach($listTransactions as $t){
                                        $balance = $t['Balance'];
                                        $listChildTransactionOrders = array(); // don hang
                                        $listChildTransactionTransports = array(); // van chuyen
                                        foreach($listTransactions as $t1){
                                            if($t1['OrderId'] == $t['OrderId'] && $t1['TransactionId'] != $t['TransactionId'] && ($t1['TransactionRangeId'] == 3 || $t1['TransactionRangeId'] == 4)){
                                                if($t['TransactionRangeId'] == 1 && $t['StatusId'] == 2) $listChildTransactionOrders[]=$t1;
                                                elseif($t['TransactionRangeId'] == 2 && $t['StatusId'] == 2) $listChildTransactionTransports[]=$t1;
                                            }
                                        }
                                        if($t['TransactionRangeId'] == 1 && $t['StatusId'] == 2){ //don hang
                                            $i++;
                                            if(empty($listChildTransactionOrders)){ ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td></td>
                                                    <td>
                                                        <?php if($t['OrderId'] > 0){
                                                            if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                            echo '<a href="'.base_url("order/edit/".$t['OrderId']).'">'.$orderCodes[$t['OrderId']].'</a><br/>';
                                                        }
                                                        echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?>
                                                    </td>
                                                    <td class="text-center"><?php if($t['TransactionTypeId'] == 3) echo priceFormat($t['PaidVN']); ?></td>
                                                    <td></td>
                                                    <td class="text-center"><?php if($t['TransactionTypeId'] == 1 || $t['TransactionTypeId'] == 2) echo priceFormat($t['PaidVN']); ?></td>
                                                    <td><?php echo $t['Comment']; ?></td>
                                                    <td><?php echo priceFormat($balance); ?></td>
                                                </tr>
                                            <?php } else{
                                                if($t['TransactionTypeId'] == 3) {
                                                    $orderPaid = $t['PaidVN'];
                                                    $totalPaid = 0;
                                                }
                                                else{
                                                    $orderPaid = 0;
                                                    $totalPaid = $t['PaidVN'];
                                                }
                                                foreach($listChildTransactionOrders as $t1){
                                                    $balance = $t1['Balance'];
                                                    if($t1['TransactionRangeId'] == 3){
                                                        if($t['TransactionTypeId'] == 3) $orderPaid -= $t1['PaidVN'];
                                                        elseif($t['TransactionTypeId'] == 1) $totalPaid += $t1['PaidVN'];
                                                        elseif($t['TransactionTypeId'] == 2) $totalPaid -= $t1['PaidVN'];
                                                    }
                                                    elseif($t1['TransactionRangeId'] == 4){
                                                        if($t['TransactionTypeId'] == 3) $orderPaid += $t1['PaidVN'];
                                                        elseif($t['TransactionTypeId'] == 1) $totalPaid -= $t1['PaidVN'];
                                                        elseif($t['TransactionTypeId'] == 2) $totalPaid += $t1['PaidVN'];
                                                    }
                                                } ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><i class="fa fa-caret-square-o-right" data-id="<?php echo $t['TransactionId']; ?>"></i></td>
                                                    <td>
                                                        <?php if($t['OrderId'] > 0){
                                                            if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                            echo '<a href="'.base_url("order/edit/".$t['OrderId']).'">'.$orderCodes[$t['OrderId']].'</a><br/>';
                                                        }
                                                        echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?>
                                                    </td>
                                                    <td class="text-center"><?php if($orderPaid > 0) echo priceFormat($orderPaid); ?></td>
                                                    <td></td>
                                                    <td class="text-center"><?php if($totalPaid > 0) echo priceFormat($totalPaid); ?></td>
                                                    <td><?php echo $t['Comment']; ?></td>
                                                    <td><?php echo priceFormat($balance); ?></td>
                                                </tr>
                                                <tr class="thead tr<?php echo $t['TransactionId']; ?>" style="display: none;">
                                                    <td colspan="8">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Thời gian</th>
                                                                <th>Số tiền</th>
                                                                <th>Loại</th>
                                                                <th>Trạng thái</th>
                                                                <th>Ghi chú</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                                <td class="text-center"><?php echo priceFormat($t['PaidVN']); ?></td>
                                                                <td>Đơn hàng</td>
                                                                <td><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                                                <td><?php echo $t['Comment']; ?></td>
                                                            </tr>
                                                            <?php foreach($listChildTransactionOrders as $t1){ ?>
                                                                <tr>
                                                                    <td><?php echo ddMMyyyy($t1['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                                    <td class="text-center"><?php echo priceFormat($t1['PaidVN']); ?></td>
                                                                    <td><?php echo $transactionRanges[$t1['TransactionRangeId']] ?></td>
                                                                    <td><span class="<?php echo $labelCss[$t1['StatusId']]; ?>"><?php echo $status[$t1['StatusId']]; ?></span></td>
                                                                    <td><?php echo $t1['Comment']; ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php }
                                        }
                                        elseif($t['TransactionRangeId'] == 2 && $t['StatusId'] == 2) { //van chuyen
                                            $i++;
                                            if(empty($listChildTransactionTransports)){ ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td></td>
                                                    <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                    <td></td>
                                                    <td class="text-center"><?php if($t['TransactionTypeId'] == 3) echo priceFormat($t['PaidVN']); ?></td>
                                                    <td class="text-center"><?php if($t['TransactionTypeId'] == 1 || $t['TransactionTypeId'] == 2) echo priceFormat($t['PaidVN']); ?></td>
                                                    <td><?php echo $t['Comment']; ?></td>
                                                    <td><?php echo priceFormat($balance); ?></td>
                                                </tr>
                                            <?php } else{
                                                if($t['TransactionTypeId'] == 3) {
                                                    $transportPaid = $t['PaidVN'];
                                                    $totalPaid = 0;
                                                }
                                                else{
                                                    $transportPaid = 0;
                                                    $totalPaid = $t['PaidVN'];
                                                }
                                                foreach($listChildTransactionTransports as $t1){
                                                    $balance = $t1['Balance'];
                                                    if($t1['TransactionRangeId'] == 3){
                                                        if($t['TransactionTypeId'] == 3) $transportPaid -= $t1['PaidVN'];
                                                        elseif($t['TransactionTypeId'] == 1) $totalPaid += $t1['PaidVN'];
                                                        elseif($t['TransactionTypeId'] == 2) $totalPaid -= $t1['PaidVN'];
                                                    }
                                                    elseif($t1['TransactionRangeId'] == 4){
                                                        if($t['TransactionTypeId'] == 3) $transportPaid += $t1['PaidVN'];
                                                        elseif($t['TransactionTypeId'] == 1) $totalPaid -= $t1['PaidVN'];
                                                        elseif($t['TransactionTypeId'] == 2) $totalPaid += $t1['PaidVN'];
                                                    }
                                                } ?>
                                                <tr>
                                                    <td><?php echo $i; ?></td>
                                                    <td><i class="fa fa-caret-square-o-right" data-id="<?php echo $t['TransactionId']; ?>"></i></td>
                                                    <td>
                                                        <?php if($t['OrderId'] > 0){
                                                            if(!isset($orderCodes[$t['OrderId']])) $orderCodes[$t['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $t['OrderId']), 'OrderCode');
                                                            echo '<a href="'.base_url("order/edit/".$t['OrderId']).'">'.$orderCodes[$t['OrderId']].'</a><br/>';
                                                        }
                                                        echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?>
                                                    </td>
                                                    <td></td>
                                                    <td class="text-center"><?php if($transportPaid > 0) echo priceFormat($transportPaid); ?></td>
                                                    <td class="text-center"><?php if($totalPaid > 0) echo priceFormat($totalPaid); ?></td>
                                                    <td><?php echo $t['Comment']; ?></td>
                                                    <td><?php echo priceFormat($balance); ?></td>
                                                </tr>
                                                <tr class="thead tr<?php echo $t['TransactionId']; ?>" style="display: none;">
                                                    <td colspan="8">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Thời gian</th>
                                                                <th>Số tiền</th>
                                                                <th>Loại</th>
                                                                <th>Trạng thái</th>
                                                                <th>Ghi chú</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td><?php echo ddMMyyyy($t['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                                <td class="text-center"><?php echo priceFormat($t['PaidVN']); ?></td>
                                                                <td>Vận chuyển</td>
                                                                <td><span class="<?php echo $labelCss[$t['StatusId']]; ?>"><?php echo $status[$t['StatusId']]; ?></span></td>
                                                                <td><?php echo $t['Comment']; ?></td>
                                                            </tr>
                                                            <?php foreach($listChildTransactionTransports as $t1){ ?>
                                                                <tr>
                                                                    <td><?php echo ddMMyyyy($t1['PaidDateTime'], 'd/m/Y H:i'); ?></td>
                                                                    <td class="text-center"><?php echo priceFormat($t1['PaidVN']); ?></td>
                                                                    <td><?php echo $transactionRanges[$t1['TransactionRangeId']] ?></td>
                                                                    <td><span class="<?php echo $labelCss[$t1['StatusId']]; ?>"><?php echo $status[$t1['StatusId']]; ?></span></td>
                                                                    <td><?php echo $t1['Comment']; ?></td>
                                                                </tr>
                                                            <?php } ?>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            <?php }
                                        }
                                    } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>