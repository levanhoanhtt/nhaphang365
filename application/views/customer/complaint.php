<?php $this->load->view('includes/customer/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-sm-3">
                        <?php $this->load->view('includes/customer/sidebar'); ?>
                    </div>
                    <div class="col-sm-9">
                        <ul class="list-inline" id="ulComplaintStatus">
                            <li><a href="<?php echo base_url('complaint'); ?>" class="btn btn-primary">Tất cả (<?php echo $statisticComplaintCounts[0]; ?>)</a></li>
                            <?php $complaintStatus = $this->Mconstants->complaintStatus;
                            foreach($complaintStatus as $i => $v){ ?>
                                <li><a href="<?php echo base_url('complaint/'.$i); ?>" class="btn btn-default"><?php echo $v; ?> (<?php echo $statisticComplaintCounts[$i]; ?>)</a></li>
                            <?php } ?>
                        </ul>
                        <div class="box box-default">
                            <div class="box-body row-margin">
                                <?php echo ($complaintStatusId > 0) ? form_open('customer/complaint/'.$complaintStatusId) : form_open('customer/complaint'); ?>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                            <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Lọc">
                                        <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                                    </div>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <div class="box box-default">
                            <?php if(isset($paggingHtml) && !empty($paggingHtml)) sectionTitleHtml('', $paggingHtml); ?>
                            <div class="box-body table-responsive no-padding divTable">
                                <table class="table table-hover table-bordered">
                                    <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Khiếu nại</th>
                                        <th>Đơn hàng</th>
                                        <th>Ngày tạo</th>
                                        <th>Hành động</th>
                                    </tr>
                                    </thead>
                                    <tbody id="tbodyComplaint">
                                    <?php $i = 0;
                                    $complaintStatus = $this->Mconstants->complaintStatus;
                                    $labelCss = $this->Mconstants->labelCss;
                                    $orderCodes = array();
                                    foreach($listComplaints as $c){
                                        $i++; ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><a href="<?php echo base_url('chi-tiet-don-hang-'.$c['OrderId'].'-khieu-nai-'.$c['ComplaintId']); ?>"><span class="<?php echo $labelCss[$c['ComplaintStatusId']]; ?>">Khiếu nại #<?php echo $c['ComplaintId']; ?>(<?php echo $complaintStatus[$c['ComplaintStatusId']]; ?>)</span></a></td>
                                            <td>
                                                <a href="<?php echo base_url('chi-tiet-don-hang-'.$c['OrderId']); ?>">
                                                    <?php if(!isset($orderCodes[$c['OrderId']])) $orderCodes[$c['OrderId']] = $this->Morders->getFieldValue(array('OrderId' => $c['OrderId']), 'OrderCode');
                                                    echo $orderCodes[$c['OrderId']]; ?>
                                                </a>
                                            </td>
                                            <td><?php echo ddMMyyyy($c['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                            <td class="actions">
                                                <a href="javascript:void(0)" class="link_history" data-id="<?php echo $c['ComplaintId']; ?>" title="Lịch sử khiếu nại"><i class="fa fa-history"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>