<?php $this->load->view('includes/customer/header'); ?>
    <div class="content-wrapper" id="chatToStaffPage">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="box box-primary">
                            <div class="box-header">
                                <?php echo form_open('chat/getListStaffChat', array('id' => 'seachStaffChatForm', 'class' => "input-group input-group-sm")); ?>
                                <input type="text" class="form-control" id="txtStaffChatName" placeholder="Tìm kiếm CSKH">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                            <div class="box-body box-comments" id="listChatStaff"></div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="box box-primary direct-chat direct-chat-primary">
                            <div class="box-header with-border">
                                <h3 class="box-title">Chat với CSKH</h3>
                            </div>
                            <div class="box-body" style="display: block;">
                                <div class="direct-chat-messages" id="listChat" style="height: 350px;"></div>
                                <div class="direct-chat-contacts">
                                    <input type="text" hidden="hidden" id="imagePath" value="<?php echo PRODUCT_PATH; ?>">
                                    <input type="text" hidden="hidden" id="getListStaffUrl" value="<?php echo base_url('user/getList'); ?>">
                                    <input type="text" hidden="hidden" id="getChatUrl" value="<?php echo base_url('chat/getList'); ?>">
                                    <input type="text" hidden="hidden" id="updateCountChatUnReadUrl" value="<?php echo base_url('chat/updateCountChatUnRead'); ?>">
                                    <input type="text" hidden="hidden" id="startChatPagging" value="0">
                                    <input type="text" hidden="hidden" id="totalChatMsg" value="0">
                                    <input type="text" hidden="hidden" id="staffId" value="0">
                                    <input type="text" hidden="hidden" id="staffName" value="">
                                    <input type="text" hidden="hidden" id="staffAvatar" value="<?php echo NO_IMAGE; ?>">
                                </div>
                            </div>
                            <div class="box-footer" style="display: block;">
                                <form action="javascript:void(0)" method="post" id="chatForm">
                                    <div class="input-group">
                                        <input type="text" placeholder="Tin nhắn..." class="form-control" id="chatMsg" autocomplete="off">
                                          <span class="input-group-btn">
                                              <button type="submit" class="btn btn-primary btn-flat">Gửi</button>
                                              <button id="btnChatSendFile" type="button" class="btn btn-default btn-flat"><i class="fa fa-paperclip"></i></button>
                                          </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>