<?php $this->load->view('includes/customer/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-sm-3">
                        <?php $this->load->view('includes/customer/sidebar'); ?>
                    </div>
                    <div class="col-sm-9">
                        <div class="box box-default">
                            <div class="box-header">
                                <h3 class="box-title text-bold">Liên hệ</h3>
                            </div>
                            <div class="box-body">
                                <?php echo $contactText; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>