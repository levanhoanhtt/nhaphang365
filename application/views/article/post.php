<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('cms/article'); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" name="ArticleTitle" class="form-control" value="<?php echo set_value('ArticleTitle'); ?>" placeholder="Tiêu đề">
                            </div>
                            <div class="col-sm-3">
                                <?php $this->Mconstants->selectConstants('status', 'StatusId', set_value('StatusId'), true, 'Trạng thái'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="BeginDate" value="<?php echo set_value('BeginDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                    <input type="text" class="form-control datepicker" name="EndDate" value="<?php echo set_value('EndDate'); ?>" autocomplete="off">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                                <input type="text" hidden="hidden" name="PageId" id="pageId" value="<?php echo set_value('PageId'); ?>">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml('<a href="'.base_url('cms/article/add').'" class="btn btn-primary">Thêm mới</a>', isset($paggingHtml) ? $paggingHtml : ''); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <th>Link</th>
                                <th>Trạng thái</th>
                                <th>Ngày xuất bản</th>
                                <th>Người cập nhật</th>
                                <th>Ngày cập nhật</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="tbodyArticle">
                            <?php $i = 0;
                            $labelCss = $this->Mconstants->labelCss;
                            $status = $this->Mconstants->status;
                            $fullNames = array();
                            foreach($listArticles as $a){
                                $i++;
                                $updateUserId = ($a['UpdateUserId'] > 0) ? $a['UpdateUserId'] : $a['CrUserId'];
                                if(!isset($fullNames[$updateUserId])) $fullNames[$updateUserId] = $this->Musers->getFieldValue(array('UserId' => $updateUserId), 'FullName'); ?>
                                <tr id="article_<?php echo $a['ArticleId']; ?>">
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('cms/article//edit/'.$a['ArticleId']); ?>"><?php echo $a['ArticleTitle']; ?></a></td>
                                    <td><a href="<?php echo base_url($a['ArticleSlug']); ?>" target="_blank"><?php echo $a['ArticleSlug']; ?></a></td>
                                    <td id="statusName_<?php echo $a['ArticleId']; ?>"><span class="<?php echo $labelCss[$a['StatusId']]; ?>"><?php echo $status[$a['StatusId']]; ?></span></td>
                                    <td><?php echo ddMMyyyy($a['PublishDateTime']); ?></td>
                                    <td><a href="<?php echo base_url('user/edit/'.$updateUserId); ?>"><?php echo $fullNames[$updateUserId]; ?></a></td>
                                    <td><?php echo ($a['UpdateUserId'] > 0) ? ddMMyyyy($a['UpdateDateTime'], 'd/m/Y H:i') : ddMMyyyy($a['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $a['ArticleId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                        <div class="btn-group" id="btnGroup_<?php echo $a['ArticleId']; ?>">
                                            <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-check"></i><span class="caret"></span> </button>
                                            <ul class="dropdown-menu">
                                                <?php foreach($status as $j => $v){ ?>
                                                    <li><a href="javascript:void(0)" class="link_status" data-id="<?php echo $a['ArticleId']; ?>" data-status="<?php echo $j; ?>"><?php echo $v; ?></a></li>
                                                <?php }  ?>
                                            </ul>
                                        </div>
                                        <input type="text" hidden="hidden" id="statusId_<?php echo $a['ArticleId']; ?>" value="<?php echo $a['StatusId']; ?>">
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <input type="text" hidden="hidden" id="deleteArticle" value="<?php echo $deleteArticle ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="changeStatus" value="<?php echo $changeStatus ? 1 : 0; ?>">
                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('cms/article/changeStatus'); ?>">
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>