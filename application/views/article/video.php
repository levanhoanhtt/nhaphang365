<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-success">
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <th>Video Youtube Link</th>
                                <th>Hành động</th>
                            </tr>
                            </thead>
                            <tbody id="tbodyArticle">
                            <?php foreach($listArticles as $a){ ?>
                                <tr id="article_<?php echo $a['ArticleId']; ?>">
                                    <td>
                                        <?php $attrSelect = ' onchange="changeDisplayOrder(this, \'' . $a['ArticleId'] . '\')" data-id="'.$a['ArticleId'].'"';
                                        $this->Mconstants->selectNumber(1, 100, 'DisplayOrder_'.$a['ArticleId'], $a['DisplayOrder'], true, $attrSelect); ?>
                                    </td>
                                    <td id="articleTitle_<?php echo $a['ArticleId']; ?>"><?php echo $a['ArticleTitle']; ?></td>
                                    <td id="articleContent_<?php echo $a['ArticleId']; ?>"><a href="<?php echo $a['ArticleContent']; ?>" target="_blank"><?php echo $a['ArticleContent']; ?></a></td>
                                    <td class="actions">
                                        <a href="javascript:void(0)" class="link_edit" data-id="<?php echo $a['ArticleId']; ?>" title="Sửa"><i class="fa fa-pencil"></i></a>
                                        <a href="javascript:void(0)" class="link_delete" data-id="<?php echo $a['ArticleId']; ?>" title="Xóa"><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <?php echo form_open('cms/article/update', array('id' => 'articleForm')); ?>
                                <td><?php $this->Mconstants->selectNumber(1, 100, 'DisplayOrder', 1, true); ?></td>
                                <td><input type="text" class="form-control hmdrequired" id="articleTitle" name="ArticleTitle" value="" data-field="Tiêu đề"></td>
                                <td><input type="text" class="form-control hmdrequired" id="articleContent" name="ArticleContent" value="" data-field="Video Youtube Link"></td>
                                <td class="actions">
                                    <a href="javascript:void(0)" id="link_update" title="Cập nhật"><i class="fa fa-save"></i></a>
                                    <a href="javascript:void(0)" id="link_cancel" title="Thôi"><i class="fa fa-times"></i></a>
                                    <input type="text" name="ArticleId" id="articleId" value="0" hidden="hidden">
                                    <input type="text" name="ArticleSlug" hidden="hidden" value="">
                                    <input type="text" name="ArticleLead" hidden="hidden" value="">
                                    <input type="text" name="StatusId" hidden="hidden" value="2">
                                    <input type="text" name="ArticleTypeId" id="articleTypeId" hidden="hidden" value="2">
                                    <input type="text" name="ArticleImage" hidden="hidden" value="">
                                    <input type="text" name="PublishDateTime" hidden="hidden" value="">
                                    <input type="text" name="IsUpdateDisplayOrder" hidden="hidden" value="1">
                                    <input type="text" hidden="hidden" id="changeDisplayOrderUrl" value="<?php echo base_url('cms/article/changeDisplayOrder'); ?>">
                                    <input type="text" hidden="hidden" id="changeStatusUrl" value="<?php echo base_url('cms/article/changeStatus'); ?>">
                                </td>
                                <?php echo form_close(); ?>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>