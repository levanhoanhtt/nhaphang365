<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($articleId > 0){ ?>
                     <?php echo form_open('cms/article/update', array('id' => 'articleForm')); ?>
                     <div class="row">
                         <div class="col-sm-8">
                             <div class="form-group">
                                 <label class="control-label">Tiêu đề <span class="required">*</span></label>
                                 <input type="text" class="form-control hmdrequired" name="ArticleTitle" id="articleTitle" value="<?php echo $article['ArticleTitle']; ?>" data-field="Tiêu đề">
                             </div>
                             <div class="form-group">
                                 <div class="input-group">
                                     <div class="input-group-btn">
                                         <button type="button" class="btn btn-default"><?php echo base_url('/'); ?></button>
                                     </div>
                                     <input type="text" class="form-control" name="ArticleSlug" id="articleSlug" value="<?php echo $article['ArticleSlug']; ?>">
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="control-label">Trích dẫn</label>
                                 <textarea name="ArticleLead" id="articleLead"><?php echo $article['ArticleLead']; ?></textarea>
                             </div>
                             <div class="form-group">
                                 <textarea name="ArticleContent"><?php echo $article['ArticleContent']; ?></textarea>
                             </div>
                         </div>
                         <div class="col-sm-4">
                             <div class="form-group">
                                 <label class="control-label">Ngày xuất bản <span class="required">*</span></label>
                                 <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                     <input type="text" class="form-control datepicker hmdrequired" name="PublishDateTime" value="<?php echo ddMMyyyy($article['PublishDateTime']); ?>" autocomplete="off" data-field="Ngày xuất bản">
                                 </div>
                             </div>
                             <div class="form-group">
                                 <label class="col-sm-3 control-label" style="line-height: 34px;">Trạng thái</label>
                                 <div class="col-sm-9">
                                     <?php $this->Mconstants->selectConstants('status', 'StatusId', $article['StatusId']); ?>
                                 </div>
                                 <div class="clearfix"></div>
                             </div>
                             <div class="form-group">
                                 <label class="control-label">Chuyên mục <span class="required">*</span></label>
                                 <?php $this->Mconstants->selectObject($listCategories, 'CategoryId', 'CategoryName', 'CategoryId', $categoryIds, false, '', ' select2', ' multiple="multiple" data-placeholder="Chọn chuyên mục" style="width: 100%;"'); ?>
                             </div>
                             <div class="form-group">
                                 <label class="control-label" style="width: 100%;">Ảnh đại diện <button type="button" class="btn btn-box-tool btnImage"><i class="fa fa-upload"></i> Chọn hình</button></label>
                                 <img src="<?php echo IMAGE_PATH.$article['ArticleImage']; ?>" id="imgArticle" class="btnImage" style="width: 100%;cursor: pointer;">
                             </div>
                             <div class="form-group text-right">
                                 <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                                 <input type="text" name="ArticleId" id="articleId" hidden="hidden" value="<?php echo $article['ArticleId']; ?>">
                                 <input type="text" name="ArticleTypeId" hidden="hidden" value="2">
                                 <input type="text" name="DisplayOrder" hidden="hidden" value="1">
                                 <input type="text" name="ArticleImage" id="articleImage" hidden="hidden" value="<?php echo IMAGE_PATH.$article['ArticleImage']; ?>">
                                 <input type="text" name="CategoryIds" id="categoryIds" class="hmdrequired" hidden="hidden" value="<?php echo implode(',', $categoryIds); ?>" data-field="Chuyên mục">
                                 <input type="text" name="IsUpdateDisplayOrder" hidden="hidden" value="0">
                                 <input type="text" hidden="hidden" id="editArticleUrl" value="<?php echo base_url('cms/article/edit'); ?>">
                             </div>
                         </div>
                     </div>
                    <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>