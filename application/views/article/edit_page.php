<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <?php $this->load->view('includes/notice'); ?>
                <?php if($articleId > 0){ ?>
                    <?php echo form_open('cms/article/update', array('id' => 'articleForm')); ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Tiêu đề</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="ArticleTitle" value="<?php echo $article['ArticleTitle']; ?>" readonly="readonly">
                        </div>
                        <div class="col-sm-1 text-right">
                            <input class="btn btn-primary" id="submit" type="submit" name="submit" value="Cập nhật">
                            <input type="text" name="ArticleId" id="articleId" hidden="hidden" value="<?php echo $articleId; ?>">
                            <input type="text" name="StatusId" hidden="hidden" value="2">
                            <input type="text" name="ArticleTypeId" hidden="hidden" value="1">
                            <input type="text" name="DisplayOrder" hidden="hidden" value="1">
                            <input type="text" name="ArticleImage" hidden="hidden" value="">
                            <input type="text" name="PublishDateTime" hidden="hidden" value="">
                            <input type="text" name="IsUpdateDisplayOrder" hidden="hidden" value="0">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-btn">
                                <button type="button" class="btn btn-default"><?php echo base_url('/'); ?></button>
                            </div>
                            <input type="text" class="form-control" name="ArticleSlug" value="<?php echo $article['ArticleSlug']; ?>" readonly="readonly">
                        </div>
                    </div>
                    <div class="form-group">
                        <textarea name="ArticleContent"><?php echo $article['ArticleContent']; ?></textarea>
                    </div>
                    <?php echo form_close(); ?>
                <?php } ?>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>