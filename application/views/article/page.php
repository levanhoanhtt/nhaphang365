<?php $this->load->view('includes/header'); ?>
    <div class="content-wrapper">
        <div class="container-fluid">
            <?php $this->load->view('includes/breadcrumb'); ?>
            <section class="content">
                <div class="box box-default">
                    <?php sectionTitleHtml('Tìm kiếm'); ?>
                    <div class="box-body row-margin">
                        <?php echo form_open('cms/article/page'); ?>
                        <div class="row">
                            <div class="col-sm-9">
                                <input type="text" name="ArticleTitle" class="form-control" value="<?php echo set_value('ArticleTitle'); ?>" placeholder="Tiêu đề">
                            </div>
                            <div class="col-sm-3">
                                <input type="submit" id="submit" name="submit" class="btn btn-primary" value="Tìm kiếm">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="box box-success">
                    <?php sectionTitleHtml($title); ?>
                    <div class="box-body table-responsive no-padding divTable">
                        <table class="table table-hover table-bordered">
                            <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tiêu đề</th>
                                <th>Link</th>
                                <th>Người cập nhật</th>
                                <th>Ngày cập nhật</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0;
                            $fullNames = array();
                            foreach($listArticles as $a){
                                $i++;
                                $updateUserId = ($a['UpdateUserId'] > 0) ? $a['UpdateUserId'] : $a['CrUserId'];
                                if(!isset($fullNames[$updateUserId])) $fullNames[$updateUserId] = $this->Musers->getFieldValue(array('UserId' => $updateUserId), 'FullName'); ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><a href="<?php echo base_url('cms/article//edit/'.$a['ArticleId']); ?>"><?php echo $a['ArticleTitle']; ?></a></td>
                                    <td><a href="<?php echo base_url($a['ArticleSlug']); ?>" target="_blank"><?php echo $a['ArticleSlug']; ?></a></td>
                                    <td><a href="<?php echo base_url('user/edit/'.$updateUserId); ?>"><?php echo $fullNames[$updateUserId]; ?></a></td>
                                    <td><?php echo ($a['UpdateUserId'] > 0) ? ddMMyyyy($a['UpdateDateTime'], 'd/m/Y H:i') : ddMMyyyy($a['CrDateTime'], 'd/m/Y H:i'); ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
<?php $this->load->view('includes/footer'); ?>