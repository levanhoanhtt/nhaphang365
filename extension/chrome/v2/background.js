chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
        switch (request.action) {
            case "getExchangeRate":
            case "addToCart":
            case "getProductCount":
                sendAjaxRequest(request, sender);
                break;
            default : break;
        }
    }
);
chrome.browserAction.onClicked.addListener(function (tab) {
    window.open('http://nhaphang365.vn/', '_blank');
});
function sendAjaxRequest(request, sender) {
    $.ajax({
        url: request.url,
        data: request.data == undefined ? {} : request.data,
        method: request.method == undefined ? 'GET' : request.method,
        contentType: 'application/x-www-form-urlencoded',
        xhrFields: {
            withCredentials: true
        },
        success: function (d) {
            chrome.tabs.sendMessage(sender.tab.id, {action: request.callback, response: d}, function (response) {});
        },
        error: function (xhr, textStatus, error) {}
    });
}